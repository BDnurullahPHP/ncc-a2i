module.exports = function(grunt) {	
	cssmin: {
	  	target: {
		    files: [{
				expand: true,
				cwd: 'public/assets/css',
				src: ['*.css', '!*.min.css'],
				dest: 'public/assets/dist/css',
				ext: 'sss.min.css'
		    }]
	  	}
	}

	grunt.loadNpmTasks('grunt-contrib-cssmin');
	grunt.registerTask('default', ['cssmin']);
};