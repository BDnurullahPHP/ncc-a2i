// var app = require('express')();
// var server = require('http').Server(app);
// var io = require('socket.io')(server);
// var redis = require('redis');

// server.listen(8890);
// io.on('connection', function (socket) {

//     console.log("new client connected");
//     var redisClient = redis.createClient();
//     redisClient.subscribe('message');

//     redisClient.on("message", function(channel, message) {
//         console.log("new message in queue "+ message + " " + channel);
//         socket.emit(channel, message);
//     });

//     socket.on('disconnect', function() {
//         redisClient.quit();
//     });

// });

var server = require('http').Server();
var io = require('socket.io')(server);

var Redis = require('ioredis');
var redis = new Redis();

redis.subscribe('globalNotificationChannel');

redis.on('message', function(channel, message) {
    message = JSON.parse(message);
    console.log('message: '+message);

    io.emit(channel + ':' + message.event, message.data);
});

server.listen(3000, function(){
    console.log('listening on http://localhost:3000');
});