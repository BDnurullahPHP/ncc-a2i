<?php

/**
 * Custom config file for payment API.
 *
 * @author    Abdul awal <abdulawal@atilimited.net>
 */
return [

    // Payment mode
    'dev' => false, // true for devlopment false for production

    // SMS user name
    'url' => 'https://securepay.sslcommerz.com/gwprocess/v3/process.php',

    // Store id
    'store_id' => 'ncc001live',

    // Test url
    'test_url' => 'https://sandbox.sslcommerz.com/gwprocess/v3/process.php',

    // Test store id
    'test_store_id' => 'testbox',

];
