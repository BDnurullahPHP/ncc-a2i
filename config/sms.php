<?php

/**
 * Custom config file for SMS.
 *
 * @author    Abdul awal <abdulawal@atilimited.net>
 */
return [

    // SMS user name
    'user' => 'ncc',

    // SMS password
    'password' => 'NCC@ati2016',

    // SMS Stakeholder ID(For english text only)
    'sid' => 'NCCNonBrand',

    // SMS Stakeholder ID(For bangla text only)
    'sidbn' => 'NCCNONBRANDBD',

    // URL
    'url' => 'http://sms.sslwireless.com/pushapi/dynamic/server.php',
];
