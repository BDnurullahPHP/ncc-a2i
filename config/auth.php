<?php

return [
    'multi' => [
        'admin' => [
            'driver' => 'eloquent',
            'model'  => App\User::class,
            'table'  => 'sa_users',
        ],
        'user' => [
            'driver' => 'eloquent',
            'model'  => App\Models\TenderApplicant::class,
            'table'  => 'tender_applicant',
        ],
    ],
    // 'multi-auth' => [
    //     'admin' => [
    //         'driver' => 'eloquent',
    //         'model' => App\User::class,
    //     ],
    //     'user' => [
    //         'driver' => 'eloquent',
    //         'model'  => App\Models\TenderApplicant::class,
    //     ],
    // ],
    'password' => [
        'email'  => 'emails.password',
        'table'  => 'sa_password_resets',
        'expire' => 60,
    ],

];
