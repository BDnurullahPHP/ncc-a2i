//var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

// elixir(function(mix) {
//     mix.sass('app.scss');
// });

/*
 |------------------------
 | Minify css
 |------------------------
 |
 */
var gulp 		= require('gulp');
var concat 		= require('gulp-concat');
var minifyCSS 	= require('gulp-clean-css');
var rename 		= require('gulp-rename');
var uglify 		= require('gulp-uglify');

// Portal CSS
var portal_css 	= [
					'./public/assets/css/bootstrap.min.css',
					'./public/assets/css/portal.css',
					'./public/assets/css/megamenu/yamm.css',
					'./public/assets/css/megamenu/demo.css',
					'./public/assets/plugins/font-awesome/css/font-awesome.min.css',
					'./public/assets/plugins/jquery-ui-1.12.0/jquery-ui.min.css',
					'./public/assets/css/animate.css',
					'./public/assets/plugins/iCheck/square/green.css',
					'./public/assets/plugins/sweetalert-master/dist/sweetalert.css'
				];

gulp.task('portal-minify-css',function(){
	return gulp.src(portal_css)
			.pipe(minifyCSS())
			.pipe(concat('apms.min.css'))
	        .pipe(gulp.dest('public/assets/dist/css'));
});

// Portal custom style
gulp.task('portal-minify-style',function(){
	return gulp.src('./public/assets/portal/style.css')
			.pipe(minifyCSS())
			.pipe(concat('portal-style.min.css'))
	        .pipe(gulp.dest('public/assets/dist/css'));
});

// Portal javascript
var portal_js 	= [
					'./public/assets/portal/bootstrap/js/bootstrap.min.js',
					'./public/assets/portal/js/plugins.js',
					'./public/assets/plugins/jquery-ui-1.12.0/jquery-ui.min.js',
					'./public/assets/plugins/jquery-validation/dist/jquery.validate.min.js',
					'./public/assets/plugins/jquery-validation/dist/additional-methods.min.js',
					'./public/assets/plugins/jquery-validation/dist/localization/messages_bn_BD.min.js',
					'./public/assets/plugins/iCheck/icheck.min.js',
					'./public/assets/plugins/sweetalert-master/dist/sweetalert.min.js',
					'./public/assets/portal/js/jquery.bootstrap.newsbox.min.js',
					'./public/assets/plugins/input-mask/jquery.inputmask.js',
					'./public/assets/plugins/input-mask/jquery.inputmask.date.extensions.js',
					'./public/assets/plugins/input-mask/jquery.inputmask.extensions.js'
				];

gulp.task('portal-minify-js',function(){
	return gulp.src(portal_js)
          .pipe(uglify())
          .pipe(concat("apms.min.js"))
          .pipe(gulp.dest('public/assets/dist/js'));
});

// Portal custom javascript
gulp.task('portal-minify-script',function(){
	return gulp.src('./public/assets/portal/js/custom.js')
          .pipe(uglify())
          .pipe(concat("portal-script.min.js"))
          .pipe(gulp.dest('public/assets/dist/js'));
});