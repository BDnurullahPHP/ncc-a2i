var FormWizard = function() {
    return {
        init: function() {
            if (jQuery().bootstrapWizard) {
                var valid = false;
                var r = $("#submit_form");
                var mf = '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';
                var ml = '</div>';

                var o = function(e, r, t) {
                        var i = r.find("li").length,
                            o = t + 1;
                        $(".step-title", $("#form_wizard_1")).text("Step " + (t + 1) + " of " + i), jQuery("li", $("#form_wizard_1")).removeClass("done");
                        for (var n = r.find("li"), s = 0; t > s; s++) jQuery(n[s]).addClass("done");
                        1 == o ? $("#form_wizard_1").find(".button-previous").hide() : $("#form_wizard_1").find(".button-previous").show(), o >= i ? ($("#form_wizard_1").find(".button-next").hide(), $("#form_wizard_1").find(".button-submit").show()) : ($("#form_wizard_1").find(".button-next").show(), $("#form_wizard_1").find(".button-submit").hide()), App.scrollTo($(".page-title"))
                    }, 
                    vp = function() {
                        var tab = $('.tab-content').find('.tab-pane.active');
                        var p_sl = $('#tab1').find('.portlet.selected').length;
                        var p_td = tab.find('#pre_tender_list');
                        var r_td = tab.find('#is_it_retender');

                        if (typeof r_td == 'undefined' || !r_td.is(':checked')) {
                            if (tab.find('#project_id').val() == '') {
                                valid = false;
                                tab.closest('.tab-content').find('.form_messsage').html(mf+'দরপত্রের জন্য প্রকল্প নির্বাচন করুন'+ml);
                            }
                            else if(p_sl == 0) {
                                valid = false;
                                tab.closest('.tab-content').find('.form_messsage').html(mf+'দরপত্রের তথ্য যোগ করুন'+ml);
                            } else {
                                valid = true;
                                tab.closest('.tab-content').find('.form_messsage').html('');
                            }
                        } else {
                            if(r_td.is(':checked') && p_td.val() == '' ) {
                                valid = false;
                                tab.closest('.tab-content').find('.form_messsage').html(mf+'দরপত্রের তথ্য যোগ করুন'+ml);
                            }
                            else if(p_sl == 0) {
                                valid = false;
                                tab.closest('.tab-content').find('.form_messsage').html(mf+'দরপত্রের তথ্য যোগ করুন'+ml);
                            } else {
                                valid = true;
                                tab.closest('.tab-content').find('.form_messsage').html('');
                            }
                        }

                            
                    }, 
                    vt = function() {
                        valid = false;

                        $.validator.addMethod("greaterThan", function(value, element, params) {
                            if (!/Invalid|NaN/.test(new Date(value))) {
                                return new Date(value) >= new Date($(params).val());
                            }

                            return isNaN(value) && isNaN($(params).val()) || (Number(value) > Number($(params).val())); 

                        },'Must be greater than {0}.');

                        $('#tender_form').validate({
                            errorElement: 'span',
                            errorClass: 'error-help-block',
                            //focusInvalid: false, // do not focus the last invalid input
                            ignore: [],
                            errorPlacement: function (error, element) {
                                // if (element.attr("type") == "radio") {
                                //     error.insertAfter($(element).parents('div'));
                                // } else {
                                //     error.insertAfter($(element));
                                // }
                                if(element.attr("name") == "TENDER_LOCATION") error.appendTo("#error_msg1")
                                else if (element.attr("name") == "TENDER_COPY_LOCATION") error.appendTo("#error_msg2")
                                else error.insertAfter($(element))
                            },
                            submitHandler: function (form, event) {  
                                event.preventDefault();
                                if (form) {
                                    valid = true;
                                    gs();
                                }
                            }
                        });

                        $('.tender-required').each(function () {
                            $(this).rules('add', {
                                required: true,
                            });
                        });
                        $('.greater-than-first').each(function () {
                            $(this).rules('add', {
                                required: true,
                                greaterThan: '.start-date-from',
                                messages: {
                                    required: "শেষের তারিখ শুরুর তারিখ থেকে বড় হতে হবে"
                                }
                            });
                        });
                    }, 
                    gs = function() {
                        var sc = $('#tab1').find('.portlet.selected');
                        var dt = '<form id="schedule_form" class="form-horizontal" action="#" method="post">';
                        dt += '<div class="form-group no-margin" id="schedule_data_collection">';

                        sc.each(function(index, el) {
                            var ht = $(this).find('.caption').text();
                            var sth = sc.find('table>thead').prop('outerHTML');
                            var str = $(this).find('table>tbody>tr.selected');

                            dt += '<div class="col-md-12 col-sm-12 col-xs-12 data_single_row"><h4>'+ht.trim()+'</h4><table class="table table-striped table-bordered table-hover">';
                            dt += sth;
                            dt += '<tbody>';
                            str.each(function(index, el) {
                                dt += $(this).prop('outerHTML');
                            });
                            dt += '</tbody>';
                            dt += '</table></div>';
                        });
                        dt += '</div></form>';

                        $('#tab3').html(dt);
                    }, 
                    vs = function() {
                        valid = false;
                        $('#schedule_form').validate({
                            errorElement: 'span',
                            errorClass: 'error-help-block',
                            //focusInvalid: false, // do not focus the last invalid input
                            ignore: [],
                            submitHandler: function (form, event) {  
                                event.preventDefault();
                                if (form) {
                                    valid = true;
                                    gr();
                                    gc();
                                }
                            }
                        });

                        $('#schedule_form').find('.schedule-required').each(function () {
                            $(this).rules('add', {
                                required: true, number: true,
                            });
                        });
                    }, 
                    gr = function() {
                        var loader = $('#tab1').find('img.form_loader').prop('outerHTML');
                        $('#tab4').html(loader);
                        $('#tab4').find('img.form_loader').css('display', 'block');
                        var tender_id = $('#tab1').find('#project_id').parent('div').find('input.tender_id');
                        var trl = $('#schedule_data_collection').find('.data_single_row');
                        var con = [];

                        trl.each(function(index, el) {
                            var cat = $(this).find('table>tbody').find('tr').first().find('input.project_category').val();
                            con.push(cat);
                        });

                        if ( $('#form_wizard_1').hasClass('form_wizard_edit') && tender_id.length > 0 ) {
                            var param = {category: con, tender_id: tender_id.val()};
                        } else {
                            var param = {category: con};
                        }

                        $.ajax({
                            url: cat_condition_url,
                            type: 'GET',
                            async: false,
                            dataType: 'html',
                            data: param,
                        })
                        .done(function(res) {
                            $('#tab4').html(res);
                            $(function() {
                                $('.condition_text').redactor();
                            });
                        });
                    }, 
                    gc = function() {
                        var tpn     = $('#tab1').find('#project_id option:selected').text();
                        var tn      = $('#tender_form').find('#tender_no').val();
                        var tm      = $('#tender_form').find('#tender_method option:selected').text();
                        var tsl     = $('#tender_form').find('#tender_location option:selected');
                        var tcl     = $('#tender_form').find('#tender_copy_location option:selected');
                        var tt      = $('#tender_form').find('#tender_title').val();
                        var td      = $('#tender_form').find('#tender_dt').val();
                        var tpd     = $('#tender_form').find('#tender_publish_dt').val();
                        
                        var tlsd    = $('#tender_form').find('.tender_seling_range').val();
                        var tlstf   = $('#tender_form').find('.tender_last_seling_time_from').val();
                        var tlstt   = $('#tender_form').find('.tender_last_seling_time_to').val();
                        var tlrd    = $('#tender_form').find('.tender_last_receive_dt').val();
                        var tod     = $('#tender_form').find('.tender_opening_dt').val();
                        // var noi     = $('#tender_form').find('#no_of_installment').val();
                        // var dbi     = $('#tender_form').find('#days_betn_inst').val();

                        $('#tc_project_name').find('p').text(tpn);
                        $('#tc_number').find('p').text(tn);
                        $('#tc_method').find('p').text(tm);
                        $('#tc_name').find('p').text(tt);
                        $('#tc_date').find('p').text(td);
                        $('#tc_publish_date').find('p').text(tpd);
                        $('#tc_last_selling_date').find('p').text(tlsd);
                        $('#tc_last_selling_time').find('p').text(tlstf+' - '+tlstt);
                        $('#tc_last_receive_date').find('p').text(tlrd);
                        $('#tc_opening_date').find('p').text(tod);
                        // $('#tc_no_of_installment').find('p').text(noi);
                        // $('#tc_days_btn_installment').find('p').text(dbi);
                        $('#tc_location').find('ul').html('');
                        $('#tc_copy_location').find('ul').html('');

                        tsl.each(function(index, el) {
                            $('#tc_location').find('ul').append('<li>'+$(this).text()+'</li>');
                        });
                        tcl.each(function(index, el) {
                            $('#tc_copy_location').find('ul').append('<li>'+$(this).text()+'</li>');
                        });

                        var cs = $('#schedule_data_collection').find('.data_single_row');
                        var odt = '';
                        var trl = $('#tender_rules_data').find('.data_single_row');

                        cs.each(function(index, el) {
                            var tblt = $(this).find('h4').text();
                            var tblh = $(this).find('table>thead').prop('outerHTML');
                            var tblr = $(this).find('table>tbody>tr');
                            odt += '<div class="col-md-12 col-sm-12 col-xs-12 data_single_row">';
                                odt += '<h4>'+tblt+'</h4>';
                                odt += '<table class="table table-striped table-bordered table-hover">';
                                    odt += tblh;
                                    odt += '<tbody>';
                                    tblr.each(function(index, el) {
                                        odt += '<tr>';
                                        if ($(this).find('input.schedule_id').length > 0 && $(this).find('input.schedule_id').val() != '') {
                                            odt += '<input type="hidden" class="schedule_id"';
                                            odt += 'value="'+$(this).find('input.schedule_id').val()+'">';
                                        }
                                        var tbld = $(this).find('td.data_td');
                                        tbld.each(function(index, el) {
                                            if (index == 0) {
                                                odt += '<td class="pr_ssf_no text-center">';
                                                    odt += $(this).html();
                                                odt += '</td>';
                                            }
                                            if (index == 1) {
                                                odt += '<td class="position text-center">';
                                                    odt += $(this).html();
                                                odt += '</td>';
                                            }
                                            if (index == 2) {
                                                odt += '<td class="pr_measurment text-center">';
                                                    odt += $(this).html();
                                                odt += '</td>';
                                            }
                                            if (index == 3) {
                                                odt += '<td class="min_bid_amount text-center">';
                                                    odt += '<input type="hidden" class="uom"';
                                                    odt += 'value="'+$(this).find('input.uom').val()+'">';
                                                    odt += '<p>'+$(this).find('input.min_bid_amount').val()+'</p>';
                                                odt += '</td>';
                                            }
                                            if (index == 4) {
                                                odt += '<td class="monthly_rent tender_particular text-center">';
                                                    odt += '<input type="hidden" class="uom"';
                                                    odt += 'value="'+$(this).find('input.uom').val()+'">';
                                                    odt += '<input type="hidden" class="particular"';
                                                    odt += 'value="'+$(this).find('input.particular').val()+'">';
                                                    if ($(this).find('input.particular_id').length > 0) {
                                                        odt += '<input type="hidden" class="tp_id"';
                                                        odt += 'value="'+$(this).find('input.particular_id').val()+'">';
                                                    }
                                                    odt += '<p>'+$(this).find('input.schedule_price').val()+'</p>';
                                                odt += '</td>';
                                            }
                                            if (index == 5) {
                                                odt += '<td class="schedule_price tender_particular text-center">';
                                                    odt += '<input type="hidden" class="uom"';
                                                    odt += 'value="'+$(this).find('input.uom').val()+'">';
                                                    odt += '<input type="hidden" class="particular"';
                                                    odt += 'value="'+$(this).find('input.particular').val()+'">';
                                                    if ($(this).find('input.particular_id').length > 0) {
                                                        odt += '<input type="hidden" class="tp_id"';
                                                        odt += 'value="'+$(this).find('input.particular_id').val()+'">';
                                                    }
                                                    odt += '<p>'+$(this).find('input.schedule_price').val()+'</p>';
                                                odt += '</td>';
                                            }
                                            if (index == 6) {
                                                odt += '<td class="guarantee tender_particular text-center">';
                                                    odt += '<input type="hidden" class="uom"';
                                                    odt += 'value="'+$(this).find('input.uom').val()+'">';
                                                    odt += '<input type="hidden" class="particular"';
                                                    odt += 'value="'+$(this).find('input.particular').val()+'">';
                                                    if ($(this).find('input.particular_id').length > 0) {
                                                        odt += '<input type="hidden" class="tp_id"';
                                                        odt += 'value="'+$(this).find('input.particular_id').val()+'">';
                                                    }
                                                    odt += '<p>'+$(this).find('input.guarantee').val()+'</p>';
                                                odt += '</td>';
                                            }
                                        });
                                        odt += '</tr>';
                                    });
                                    odt += '</tbody>';
                                odt += '</table>';
                            odt += '</div>';
                        });
                        $('#pro_dtls_sc_overview').html(odt);

                        var str = '';
                        trl.each(function(index, el) {
                            var rlh = $(this).find('h4').text();
                            var con = $(this).find('textarea').val();
                            str += '<div class="col-md-12 col-sm-12 col-xs-12 data_single_row">';
                                str += '<h4>'+rlh+'</h4>';
                                str += con;
                            str += '</div>'
                        });
                        $('#te_rules_overview').html(str);
                    };

                $("#form_wizard_1").bootstrapWizard({
                    nextSelector: ".button-next",
                    previousSelector: ".button-previous",
                    onTabClick: function(e, r, t, i) {
                        //return void o(e, r, t+1);
                        return false;
                    },
                    onNext: function(e, a, n) {
                        if (n == 1) {
                            vp();
                        }
                        else if (n == 2) {
                            vt();
                            $("#tender_form").submit();
                        }
                        else if (n == 3) {
                            vs();
                            $("#schedule_form").submit();
                        }

                        if (valid) {
                            return void o(e, a, n);
                        } else {
                            return false;
                        }
                    },
                    onPrevious: function(e, r, a) {
                        return o(e, r, a);
                    },
                    onTabShow: function(e, r, t) {
                        var i = r.find("li").length,
                            a = t + 1,
                            o = a / i * 100;
                        $("#form_wizard_1").find(".progress-bar").css({
                            width: o + "%"
                        })
                    }
                }), $("#form_wizard_1").find(".button-previous").hide(), $("#form_wizard_1 .button-submit").click(function() {

                    swal({   
                        title: "Are you sure?",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Yes, save it!",
                        cancelButtonText: "No, cancel",
                        closeOnConfirm: true, //If false then confirm message will enable
                        closeOnCancel: true
                    }, function(isConfirm){
                        if (isConfirm) {

                            var token               = $('#project_confirm').find('input[name=_token]').val();
                            var tender_no           = $('#tender_form').find('#tender_no').val();
                            var tender_method       = $('#tender_form').find('#tender_method').val();
                            var tender_title        = $('#tender_form').find('#tender_title').val();
                            var tender_desc         = $('#tender_form').find('#tender_desc').val();
                            var tender_dt           = $('#tender_form').find('#tender_dt').val();
                            var tender_publish_dt   = $('#tender_form').find('#tender_publish_dt').val();
                            
                            var sc_last_se_date     = $('#tender_form').find('.tender_seling_range').val();
                            var sc_last_se_tfrom    = $('#tender_form').find('.tender_last_seling_time_from').val();
                            var sc_last_se_tto      = $('#tender_form').find('.tender_last_seling_time_to').val();
                            var te_last_receive_dt  = $('#tender_form').find('.tender_last_receive_dt').val();
                            var te_opening_dt       = $('#tender_form').find('.tender_opening_dt').val();
                            // var no_of_installment   = $('#tender_form').find('#no_of_installment').val();
                            // var days_betn_inst      = $('#tender_form').find('#days_betn_inst').val();
                            var te_location         = $('#tender_form').find('#tender_location option:selected');
                            var te_cp_location      = $('#tender_form').find('#tender_copy_location option:selected');
                            var te_rules            = $('#tender_rules_data').find('.data_single_row');
                            //var is_active           = $('input[name=IS_ACTIVE]:checked', '#tender_form').val()

                            var project_id          = $('#tab1').find('#project_id').val();
                            var tender_schedule     = $('#pro_dtls_sc_overview').find('.data_single_row');
                            var tender_id           = $('#tab1').find('#project_id').parent('div').find('input.tender_id');
                            var re_render           = $('#tab1').find('#pre_tender_list');
                            var re_render_chk       = $('#tab1').find('#is_it_retender');
                            var date_id             = $('#tender_form').find('input.date_id');

                            if ( $('#form_wizard_1').hasClass('form_wizard_edit') && tender_id.length > 0 ) {
                                var data_post_url = baseUrl+'admin/tender/'+'tender-update/'+tender_id.val();
                            } else {
                                var data_post_url = tender_create_url;
                            }

                            var form_data = new FormData();
                            form_data.append("_token", token);
                            form_data.append("tender_no", tender_no);
                            form_data.append("tender_method", tender_method);
                            form_data.append("tender_title", tender_title);
                            form_data.append("tender_desc", tender_desc);
                            form_data.append("tender_dt", tender_dt);
                            form_data.append("tender_publish_dt", tender_publish_dt);
                            form_data.append("sc_last_selling_date", sc_last_se_date);
                            form_data.append("sc_last_selling_tfrom", sc_last_se_tfrom);
                            form_data.append("sc_last_selling_tto", sc_last_se_tto);
                            form_data.append("te_last_receive_dt", te_last_receive_dt);
                            form_data.append("te_opening_dt", te_opening_dt);
                            // form_data.append("no_of_installment", no_of_installment);
                            // form_data.append("days_betn_inst", days_betn_inst);
                            // form_data.append("is_active", is_active);

                            form_data.append("project_id", project_id);

                            if ( tender_id.length > 0 ) {
                                form_data.append("tender_id", tender_id.val());
                            }
                            if ( typeof re_render_chk !== 'undefined' && re_render_chk.is(':checked') && re_render.val() != '' ) {
                                form_data.append("re_tender", re_render.val());
                            }
                            if ( date_id.length > 0 ) {
                                form_data.append("date_id", date_id.val());
                            }

                            te_location.each(function(index, el) {
                                form_data.append('tender_location['+index+'][location]', $(this).val());
                                form_data.append('tender_location['+index+'][type]', 1);
                                if ( typeof $(this).data('location') !== 'undefined' ) {
                                    form_data.append('tender_location['+index+'][location_id]', $(this).data('location'));
                                }
                            });
                            te_cp_location.each(function(index, el) {
                                form_data.append('tender_copy_location['+index+'][location]', $(this).val());
                                form_data.append('tender_copy_location['+index+'][type]', 2);
                                if ( typeof $(this).data('location') !== 'undefined' ) {
                                    form_data.append('tender_copy_location['+index+'][location_id]', $(this).data('location'));
                                }
                            });

                            te_rules.each(function(index, el) {
                                if ($(this).find('input.te_con_id').length > 0) {
                                    form_data.append('tender_rules['+index+'][con_id]', $(this).find('input.te_con_id').val());
                                }
                                form_data.append('tender_rules['+index+'][category]', $(this).find('input.condition_cat').val());
                                form_data.append('tender_rules['+index+'][rules]', $(this).find('textarea').val());
                            });

                            tender_schedule.each(function(index, el) {

                                var pro_dtls_row = $(this).find('table>tbody>tr');

                                pro_dtls_row.each(function(key, elm) {
                                    if ($(this).find('input.schedule_id').length > 0 && $(this).find('input.schedule_id').val() != '') {
                                        form_data.append('tender_schedule['+index+']['+key+'][schedule_id]', $(this).find('input.schedule_id').val());
                                    }
                                    form_data.append('tender_schedule['+index+']['+key+'][pr_details]', $(this).find('td.pr_ssf_no input.project_details').val());
                                    form_data.append('tender_schedule['+index+']['+key+'][pr_category]', $(this).find('td.pr_ssf_no input.project_category').val());
                                    form_data.append('tender_schedule['+index+']['+key+'][pr_type]', $(this).find('td.pr_ssf_no input.project_type').val());
                                    form_data.append('tender_schedule['+index+']['+key+'][lt_money]', $(this).find('td.min_bid_amount p').text());

                                    var tender_particular = $(this).find('td.tender_particular');

                                    tender_particular.each(function(inx, element) {
                                        if ($(this).find('input.tp_id').length > 0) {
                                            form_data.append('tender_schedule['+index+']['+key+'][tender_particular]['+inx+'][tp_id]', $(this).find('input.tp_id').val());
                                        }
                                        form_data.append('tender_schedule['+index+']['+key+'][tender_particular]['+inx+'][particular_id]', $(this).find('input.particular').val());
                                        form_data.append('tender_schedule['+index+']['+key+'][tender_particular]['+inx+'][particular_amt]', $(this).find('p').text());
                                        form_data.append('tender_schedule['+index+']['+key+'][tender_particular]['+inx+'][uom]', $(this).find('input.uom').val());
                                    });

                                });
                                
                            });
                            
                            // Post tender form data
                            $.ajax({
                                url: data_post_url,
                                dataType: 'json',
                                cache: false,
                                contentType: false,
                                processData: false,
                                async: false,
                                data: form_data,
                                type: 'post'
                            })
                            .done(function(res) {
                                if (res) {
                                    if (res.flug==1) { // For data save response
                                        //swal("Saved", "Successfully saved", "success");
                                        $('.form-wizard').find('ul.nav-pills li:last-child').removeClass('active').addClass('done');
                                        $('.tab-content').find('.form_messsage').html('<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'+res.message+'</div>');
                                        $("#common_table").dataTable().fnDestroy();
                                        //$('#tender_add_content').modal('hide');
                                        tableManaged();
                                    } else { // For error response
                                        $('.tab-content').find('.form_messsage').html('<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'+res.message+'</div>');
                                    }
                                } else { // For problem with getting response
                                    $('.tab-content').find('.form_messsage').html('<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>অনুগ্রহ করে একটু পরে আবার চেষ্টা করুন</div>');
                                }
                            });
                        }
                    });

                    $('html, body').animate({
                        scrollTop: $("#form_wizard_1").offset().top
                    }, 500);

                }).hide();
            }
        }
    }
}();

$('#tender_add_content').on('show.bs.modal', function (e) {

    var modal_body = $(this).find('.modal-content');
    modal_body.find('#body-content').html('');
    modal_body.find('img.loader').show();

});

$('#tender_add_content').on('shown.bs.modal', function (e) {

    var modal_body  = $(this).find('.modal-content');
    var button      = $(e.relatedTarget);
    var action      = button.data('form');

    $.ajax({
        url: action,
        type: 'GET',
        dataType: 'html',
        data: false,
    })
    .done(function(res) {
        modal_body.find('img.loader').hide();
        modal_body.find('#body-content').html(res);
        var mt_title = modal_body.find('#body-content').find('.modal_top_title').text();

        if ( typeof mt_title != 'undefined' && mt_title != '' ) {
            modal_body.find('.modal-title').text(mt_title);
        }

        // Form wizard
        FormWizard.init();

        // For edit tender
        if ( $('#form_wizard_1').hasClass('form_wizard_edit') ) {
            $('#project_id').trigger('change');
        }
        // Redactor js editor
        $(function() {
            $('.reactor_basic').redactor();
        });
        // Select2 multiselect
        $(".multi_select").select2();
    });
});

//=======================Re tender=====================
$(document.body).on('change', '#is_it_retender', function(event) {
    var retender = $(this).val();
    $('#content_body').html('');
    if ( $(this).is(':checked') ) {
        $("#tender_list").removeClass("hidden").find('select').addClass('form-required');
        $("#project_list").addClass("hidden").find('select').removeClass('form-required');
    } else {
        $("#tender_list").addClass("hidden").find('select').removeClass('form-required');
        $("#project_list").removeClass("hidden").find('select').addClass('form-required');
    }
});

$(document.body).on('change', '#pre_tender_list', function() {
    var re_tender = $(this).val();
    if (re_tender != '') {
        $('img.form_loader').show();

        $.ajax({
            url: project_category_url,
            type: 'GET',
            dataType: 'json',
            data: {re_tender: re_tender},
        })
        .done(function(res) {
            $('img.form_loader').hide();
            if (res.flug == 1) {
                $('.tab-content').find('.form_messsage').html('');
                $('#content_body').html(res.data);
                $('#content_body').find('.category_items').find('.tools a').trigger('click');
            } else {
                $('.tab-content').find('.form_messsage').html('<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'+res.message+'</div>')
            }
        });
    }
});
//=====================End re-tender=================

$(document.body).on('change', '#project_id', function(event) {
    var project = $(this).val();
    $('#content_body').html('');
    var tender = $(this).parent('div').find('input.tender_id').val();

    if (project != '') {
        $('img.form_loader').show();

        if (typeof tender != 'undefined') {
            var param = {project: project, tender: tender};
        } else {
            var param = {project: project};
        }

        $.ajax({
            url: project_category_url,
            type: 'GET',
            dataType: 'json',
            data: param,
        })
        .done(function(res) {
            $('img.form_loader').hide();
            if (res.flug == 1) {
                $('.tab-content').find('.form_messsage').html('');
                $('#content_body').html(res.data);
                // For edit tender
                if ( $('#form_wizard_1').hasClass('form_wizard_edit') ) {
                    $('#content_body').find('.category_items').find('.tools a').trigger('click');
                } else {
                    $('#content_body').find('.category_items').first().find('.tools a').trigger('click');
                }
                
            } else {
                $('.tab-content').find('.form_messsage').html('<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'+res.message+'</div>')
            }
        });
    }
});

$(document.body).on('click', '.tools a', function(event) {
    var project_id = $(this).parent('div').find('.project_id').val();
    var category_id = $(this).parent('div').find('.category_id').val();
    var dtls_content = $(this).closest('.portlet').find('.portlet-body .project_details_body');
    var content_lenght = dtls_content.contents().length;
    var tender = $('#tab1').find('#project_id').parent('div').find('input.tender_id').val();

    if (content_lenght == 0) {
        $('img.box_loader').css('display', 'block');

        if (typeof tender != 'undefined') {
            var param = {project_id: project_id, category_id: category_id, tender: tender};
        } else {
            var param = {project_id: project_id, category_id: category_id};
        }

        $.ajax({
            url: project_details_url,
            type: 'GET',
            dataType: 'html',
            data: param,
        })
        .done(function(res) {
            $('img.box_loader').hide();
            dtls_content.html(res);
        });
    }
    
});

$(document.body).on('change', '.check_all', function(event) {
    $(this).closest('table').find('tbody').find('input:checkbox').prop('checked', this.checked);
    if($(this).closest('table').find('tbody').find('input:checkbox').prop('checked') == true){
        $(this).closest('.portlet').addClass('selected');
        $(this).closest('table').find('tbody tr').addClass('selected');
    } else {
        $(this).closest('.portlet').removeClass('selected');
        $(this).closest('table').find('tbody tr').removeClass('selected');
    }
});

$(document.body).on('change', '.table-striped>tbody>tr input:checkbox', function(event) {
    var chk = $(this);
    if ($(this).hasClass('remove_schedule') && $(this).prop('checked') == false) {
        var url = $(this).data('url');
        swal({   
            title: "Are you sure?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            cancelButtonText: "No, cancel",
            closeOnConfirm: false, //If false then confirm message will enable
            closeOnCancel: true
        }, function(isConfirm){
            if (isConfirm) {
                $.ajax({
                    url: url,
                    type: 'GET',
                    dataType: 'html',
                    data: false,
                })
                .done(function(res) {
                    makeSelection(chk);
                    swal("Deleted", res, "success");
                });
            } else {
                chk.prop('checked', true);
            }
        });
    } else {
        makeSelection(chk);
    }
});

function makeSelection(chk) {
    if (chk.prop('checked') == true) {
        chk.closest('tr').addClass('selected');
    } else {
        chk.closest('tr').removeClass('selected');
    }

    if (chk.closest('tbody').find('tr.selected').length == 0) {
        chk.closest('.portlet').removeClass('selected');
    } else {
        chk.closest('.portlet').addClass('selected');
    }
}

// Tender primary select
var tenderPrimarySelect = function(){
    return{
        init: function(){

            $(document.body).on('click', 'a.tab_next', function(event) {
                var is_valid = 0;
                $('.validField').each(function () {
                    if ($(this).val() == "") {
                        $(this).css("border", "1px solid red");
                        is_valid = 1;
                    } else {
                        $(this).css("border", "1px solid #ccc");
                    }
                });
                event.preventDefault();
                if(is_valid == 0){
                    $('.tab-content a[href="#tab_2"]').tab('show');
                }
            });
            $(document.body).on('click', 'a.tab_previous', function(event) {
                event.preventDefault();
                $('.tab-content a[href="#tab_1"]').tab('show');
            });
            //start a new session
            $(document).on("change", "#installment_id", function() {
                $("#installment tbody").html('');
                var bid = $('#applicant_installment').find('input.applicant_bid_amount').val();
                var no = parseInt( $(this).val() );
                    for (var i = 1; i <= no; i++) {
                    $("#installment tbody").append('<tr><td class="ins_number">'+ i +'</td><td><input type="text" class="form-control validField date_picker ins_date" name="date_'+i+'" placeholder="Ex.20-07-2016"></td><td><input type="text" class="form-control form-required amount" readonly="readonly" name="amount_'+i+'" value="'+(bid/no).toFixed(2)+'"></td></tr>');
                }
            });
            $(document.body).on('click', '.tender_installment_save', function(event) {
                var submit_btn  = $(this);
                //submit_btn.addClass('btn_loader').removeClass('btn-success');
                var data_post_url = $(this).data('action');
                var data_print_url = $(this).data('print');
                $('#applicant_installment').validate({
                    errorElement: 'span',
                    errorClass: 'error-help-block',
                    //focusInvalid: false, // do not focus the last invalid input
                    ignore: [],
                    submitHandler: function (form, event) {  
                        event.preventDefault();
                        if (form) {
                            submit_btn.addClass('btn_loader').removeClass('btn-success green');
                            
                            var data_each = $('table#installment>tbody>tr');
                            var token = $('#applicant_installment').find('input[name=_token]').val();
                            var applicant = $('#applicant_installment').find('input[name=applicant_id]').val();
                            var schedule = $('#applicant_installment').find('input[name=schedule_id]').val();
                            var remarks = $('#applicant_installment').find('textarea[name=INITIAL_REMARKS]').val();
                            var punishment = $('#applicant_installment').find('input[name=punishment]').val();
                            var email = $('#applicant_installment').find('input[name=applicant_email]').val();
                            
                            var sharok_no = $('#applicant_installment').find('input[name=sharok_no]').val();
                            var sharok_date = $('#applicant_installment').find('input[name=sharok_date]').val();
                            var letter_subject = $('#applicant_installment').find('input[name=letter_subject]').val();
                            var letter_sources = $('#applicant_installment').find('input[name=letter_sources]').val();
                            var letter_body = $('#applicant_installment').find('textarea[name=letter_body]').val();
                            var applicant_info = $('#applicant_installment').find('textarea[name=applicant_info]').val();
                            var officer_info = $('#applicant_installment').find('textarea[name=officer_info]').val();
                            var distribution = $('#applicant_installment').find('textarea[name=distribution]').val();
                                
                            var form_data = new FormData();
                            form_data.append("_token", token);
                            form_data.append("applicant", applicant);
                            form_data.append("schedule", schedule);
                            form_data.append("remarks", remarks);
                            form_data.append("punishment", punishment);
                            form_data.append("email", email);
                            form_data.append("sharok_no", sharok_no);
                            form_data.append("sharok_date", sharok_date);
                            form_data.append("letter_subject", letter_subject);
                            form_data.append("letter_sources", letter_sources);
                            form_data.append("letter_body", letter_body);
                            form_data.append("applicant_info", applicant_info);
                            form_data.append("officer_info", officer_info);
                            form_data.append("distribution", distribution);
                            
                            data_each.each(function(index, el) {
                                form_data.append('installment['+index+'][number]', $(this).find('td.ins_number').text());
                                form_data.append('installment['+index+'][date]', $(this).find('input.ins_date').val());
                                form_data.append('installment['+index+'][amount]', $(this).find('input.amount').val());
                            });

                            $.ajax({
                                url: data_post_url,
                                dataType: 'json',
                                cache: false,
                                contentType: false,
                                processData: false,
                                async: false,
                                data: form_data,
                                type: 'post'
                            })
                            .done(function(res) {
                                //submit_btn.removeClass('btn_loader').addClass('btn-success');
                                $(".form_messsage").html('<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'+res.message+'</div>');
                                //$(".tender_installment_save").closest('form')[0].reset();
                                $("#common_table").dataTable().fnDestroy();
                                window.open(data_print_url, '_blank');
                                listDataTable();
                                submit_btn.removeClass('btn_loader').addClass('btn-success green');
                            });
                        }
                    }
                });

                $('.form-required').each(function () {
                    $(this).rules('add', {
                        required: true,
                    });
                });
            });
            
        }
    };
}();

// Tender final list
var tenderFinalList = function(){
    return{
        init: function(){

            $(document.body).on('click', '.finally_tender_save', function(event) {
                var submit_btn  = $(this);
                //submit_btn.addClass('btn_loader').removeClass('btn-success');
                var data_post_url = $(this).data('action');
                var data_print_url = $(this).data('print');
                $('#final_letter').validate({
                    errorElement: 'span',
                    errorClass: 'error-help-block',
                    //focusInvalid: false, // do not focus the last invalid input
                    ignore: [],
                    submitHandler: function (form, event) {  
                        event.preventDefault();
                        if (form) {
                            submit_btn.addClass('btn_loader').removeClass('btn-success green');
                            
                            var token = $('#final_letter').find('input[name=_token]').val();
                            var applicant = $('#final_letter').find('input[name=applicant_id]').val();
                            var schedule = $('#final_letter').find('input[name=schedule_id]').val();
                            var email = $('#final_letter').find('input[name=applicant_email]').val();
                                
                                
                            var sharok_no = $('#final_letter').find('input[name=sharok_no]').val();
                            var sharok_date = $('#final_letter').find('input[name=sharok_date]').val();
                            var letter_subject = $('#final_letter').find('input[name=letter_subject]').val();
                            var letter_body = $('#final_letter').find('textarea[name=letter_body]').val();
                            var letter_conditions = $('#final_letter').find('textarea[name=letter_conditions]').val();
                            var applicant_info = $('#final_letter').find('textarea[name=applicant_info]').val();
                            var officer_info = $('#final_letter').find('textarea[name=officer_info]').val();
                            var distribution = $('#final_letter').find('textarea[name=distribution]').val();
                                
                            var form_data = new FormData();
                            form_data.append("_token", token);
                            form_data.append("applicant", applicant);
                            form_data.append("schedule", schedule);
                            form_data.append("email", email);
                            
                            form_data.append("sharok_no", sharok_no);
                            form_data.append("sharok_date", sharok_date);
                            form_data.append("letter_subject", letter_subject);
                            form_data.append("letter_body", letter_body);
                            form_data.append("letter_conditions", letter_conditions);
                            form_data.append("applicant_info", applicant_info);
                            form_data.append("officer_info", officer_info);
                            form_data.append("distribution", distribution);

                            $.ajax({
                                url: data_post_url,
                                dataType: 'json',
                                cache: false,
                                contentType: false,
                                processData: false,
                                async: false,
                                data: form_data,
                                type: 'post'
                            })
                            .done(function(res) {
                                $(".form_messsage").html('<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'+res.message+'</div>');
                                //$(".finally_tender_save").closest('form')[0].reset();
                                if (res.flug == 1) {
                                    $("#common_table").dataTable().fnDestroy();
                                    window.open(data_print_url, '_blank');
                                    listDataTable();
                                }
                                submit_btn.removeClass('btn_loader').addClass('btn-success green');
                            });
                        }
                    }
                });

                $('.form-required').each(function () {
                    $(this).rules('add', {
                        required: true,
                    });
                });
            });

        }
    };
}();

// Security return
var tenderSecurityReturn = function(){
    return{
        init: function(){

            $(document.body).on('click', '.refund_save', function(event) {
                var submit_btn  = $(this);
                var data_post_url = $(this).data('action');
                var data_print_url = $(this).data('print');
                $('#notification_show').html('');
                $('#refund_letter').validate({
                    errorElement: 'span',
                    errorClass: 'error-help-block',
                    //focusInvalid: false, // do not focus the last invalid input
                    ignore: [],
                    submitHandler: function (form, event) {  
                        event.preventDefault();
                        if (form) {
                            submit_btn.addClass('btn_loader').removeClass('btn-success green');
                            
                            $.ajax({
                                url: data_post_url,
                                dataType: 'json',
                                async: false,
                                data: $('#refund_letter').serialize(),
                                type: 'post'
                            })
                            .done(function(res) {
                                $('#notification_show').html('<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'+res.message+'</div>');
                                //$(".finally_tender_save").closest('form')[0].reset();
                                if (res.flag == 1) {
                                    $('table.datatable_ajax').dataTable().fnDestroy();
                                    listDataTable();
                                    window.open(data_print_url, '_blank');
                                }
                                submit_btn.removeClass('btn_loader').addClass('btn-success green');
                            });
                        }
                    }
                });

                $('.form-required').each(function () {
                    $(this).rules('add', {
                        required: true,
                    });
                });
            });

        }
    };
}();