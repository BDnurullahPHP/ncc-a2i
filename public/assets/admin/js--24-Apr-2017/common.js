/* ========================================
 * Author: Abdul Awal <abdulawal@atilimited.net>
 * ========================================
 */

// get current time
var currentTime = function(){
    var h = '';
    var m = '';
    var dt = new Date();
    if (dt.getHours() > 12) {
        h = parseInt(dt.getHours()) - 12;
        m = ' PM';
    } else {
        h = dt.getHours();
        m = ' AM';
    }

    return numPad(h, 2) + ":" + numPad(dt.getMinutes(), 2) + ":" + numPad(dt.getSeconds(), 2) + m;
}

// Get current date
var currentDate = function(){
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth()+1; //January is 0!
    var yyyy = today.getFullYear();

    if(dd<10) {
        dd='0'+dd
    } 

    if(mm<10) {
        mm='0'+mm
    }

    return dd+'-'+mm+'-'+yyyy;
}

// Add 0 before a number if < 10
var numPad = function (str, max) {
    //return ('0' + str).slice(-2);
    str = str.toString();
    return str.length < max ? numPad("0" + str, max) : str;
}

// Common jquery ajax datatable
var tableManaged = function() {

    var source_data  = $('#common_table').data('source');

    // begin data table
    oTable2 = $('#common_table').dataTable(dataTableSettings(source_data));
}

// Common datatable for onchange search
var dataTableManaged = function() {

    $(document.body).on('change', '.list_select', function(event) {
        listDataTable();
    });

}

// Common datatable
var listDataTable = function() {

    $('#result_content').hide();
    var list_val = $('select.list_select').val();
    var name = $('select.list_select').attr('name');
    var source_data = $('table.datatable_ajax').data('source');

    if (list_val != '') {
        $('#result_content').show();
        $("table.datatable_ajax").dataTable().fnDestroy();
        
        // begin data table
        oTable2 = $('table.datatable_ajax').dataTable(dataTableSettings(source_data, list_val));
    }

}

var dataTableSettings = function(source, param = ''){
    var obj = {
        "processing": true,
        "serverSide": true,
        "searching": false,
        "searchable": false,
        "pagingType": "full_numbers",
        'pageLength': 10,
        "aLengthMenu": [
            [10, 15, 20],
            [10, 15, 20] // change per page values here
        ],
        // Load data for the table's content from an Ajax source
        "ajax": {
            //"type": "GET",
            "url": source,
        },
        //Set column definition initialisation properties.
        "columnDefs": [
            {"targets": [0],"orderable": false,},
            {"targets": [ -1 ], "orderable": false,},
            {"targets": [ -2 ], "orderable": false,}
        ]
    };

    if (param != '') {
        obj.ajax.data = function (d){
                        d.group = param;
                    }
    }

    return obj;
}

// Common operations
var basicOperations = function() {

    // Jquery UI datepicker
    $(document.body).on('focus', '.date_picker_default', function(event) {
        $(this).datepicker({
            dateFormat: "dd-mm-yy",
            changeMonth: true,
            changeYear: true
        });
    });

    // Jquery UI datepicker
    $(document.body).on('focus', '.date_picker', function(event) {
        $(this).datepicker({
            dateFormat: "dd-mm-yy",
            minDate: 0,
            changeMonth: true,
            changeYear: true
        });
    });

    // Jquery date rangevpicker(date range)
    $(document.body).on('focus', '.date_range_picker', function(event) {
        $(this).daterangepicker({
            timePicker: false,
            locale: {
                format: 'DD-MM-YYYY'
            }
        });
    });
    // Jquery date rangevpicker(date time)
    $(document.body).on('focus', '.date_time_picker', function(event) {
        $(this).daterangepicker({
            timePicker: true,
            minDate: currentDate(),
            timePickerIncrement: 30,
            singleDatePicker: true,
            locale: {
                format: 'DD-MM-YYYY h:mm A'
            }
        });
    });
    // Jquery date rangevpicker(time)
    $(document.body).on('focus', '.time_picker', function(event) {
        $(this).timepicker();
    });

    // Allow only number
    $(document.body).on('blur', '.allow_numeric', function(event) {
        this.value = this.value.replace(/[^0-9\.]/g,''); 
    });
    
    // Modal calback
    $('#modal_add_content, #modal_big_content').on('show.bs.modal', function (e) {

        var modal_body = $(this).find('.modal-content');
        modal_body.find('#body-content').html('');
        modal_body.find('img.loader').show();
        modal_body.find('.modal-title').text('');

    });

    // Modal content add dialogue
    $('#modal_add_content, #modal_big_content').on('shown.bs.modal', function (e) {

        var modal_body  = $(this).find('.modal-content');
        var button      = $(e.relatedTarget);
        var action      = button.data('form');

        $.ajax({
            url: action,
            type: 'GET',
            dataType: 'html',
            data: false,
        })
        .done(function(res) {
            modal_body.find('img.loader').hide();
            modal_body.find('#body-content').html(res);
            var mt_title = modal_body.find('#body-content').find('.modal_top_title').text();

            if ( typeof mt_title != 'undefined' && mt_title != '' ) {
                modal_body.find('.modal-title').text(mt_title);
            }

            // Bootstrap tooltip
            if ($('.help_tooltip').length > 0) {
                $(function () {
                    $('.help_tooltip').tooltip();
                });
            }
            // Redactor js editor
            if ($('.reactor_basic').length > 0) {
                $(function() {
                    $('.reactor_basic').redactor();
                });
            }
            // Select2 multiselect
            if ($('.multi_select').length > 0) {
                $(".multi_select").select2();
            }
        });

        // Use Ctrl+S to save data
        // $(document).bind('keydown', function(e) {
        //     if(e.ctrlKey && (e.which == 83)) {
        //         e.preventDefault();
        //         alert('Ctrl+S');
        //         return false;
        //     }
        // });

    });

    // Since confModal is essentially a nested modal it's enforceFocus method
    // must be no-op'd or the following error results 
    // "Uncaught RangeError: Maximum call stack size exceeded"
    // But then when the nested modal is hidden we reset modal.enforceFocus
    var enforceModalFocusFn = $.fn.modal.Constructor.prototype.enforceFocus;
    $.fn.modal.Constructor.prototype.enforceFocus = function() {};

    // Delete post
    $(document.body).on('click', 'a.delete_post', function(event) {
        event.preventDefault();
        
        var parent_row  = $(this).closest('tr');
        var get_url     = $(this).attr('href').split('=');
        
        swal({   
            title: "Are you sure?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            cancelButtonText: "No, cancel",
            closeOnConfirm: false, //If false then confirm message will enable
            closeOnCancel: true
        }, function(isConfirm){
            if (isConfirm) {

                $.ajax({
                    url: get_url,
                    type: 'GET',
                    dataType: 'html',
                    data: false,
                })
                .done(function(res) {
                    parent_row.remove();
                    swal("Deleted", res, "success");
                });

            }
        });
    });

    // Status change
    $(document.body).on('click', 'a.change_status', function(event) {
        event.preventDefault();
        
        var ithis   = $(this);
        var get_url = $(this).attr('href').split('=');
        var iactive = $(this).data('isactive');
        
        swal({   
            title: "Are you sure?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, change it!",
            cancelButtonText: "No, cancel",
            closeOnConfirm: false, //If false then confirm message will enable
            closeOnCancel: true
        }, function(isConfirm){
            if (isConfirm) {

                $.ajax({
                    url: get_url,
                    type: 'GET',
                    dataType: 'html',
                    data: false,
                })
                .done(function(res) {
                    if (iactive==1)
                        ithis.removeClass('label-warning').addClass('label-success').data('isactive', 0).text('সক্রিয়');
                    else
                        ithis.removeClass('label-success').addClass('label-warning').data('isactive', 1).text('নিষ্ক্রিয়');

                    swal("Changed", res, "success");
                });

            }
        });
    });

    // Form reset
    $(document.body).on('click', '.reset_form', function(event) {
        $(this).closest('form')[0].reset();
    });

    // For sidebar active/inactive selection
    var url = window.location;
    $('li.nav-item').each(function(index, el) {
        var link = $(this).find('a.nav-link').attr('href').split('=');
        if (url.href == link) {
            $(this).addClass('open active');
            $(this).closest('ul.sub-menu').parent('li.nav-item').addClass('open active');
        }
    });

}

// Common form validation
var commonFormValidation = function() {

    $(document.body).on('click', '.form_submit', function() {

        var submit_btn  = $(this);
        var form        = submit_btn.closest('form');
        var action_url  = submit_btn.data('action');

        // To validate filesize
        $.validator.addMethod('filesize', function(value, element, param) {
            return this.optional(element) || (element.files[0].size <= param) 
        }); 

        // Compare two value
        $.validator.addMethod("greaterThan", function(value, element, params) {
            if (!/Invalid|NaN/.test(new Date(value))) {
                return new Date(value) >= new Date($(params).val());
            }
            return isNaN(value) && isNaN($(params).val()) || (Number(value) > Number($(params).val())); 

        },'Must be greater than {0}.');

        // Check Bangladeshi mobile no
        $.validator.addMethod("mobileBD", function(value, element) {
            return this.optional(element) || value === "NA" || 
                value.match(/^(?:\+?88)?01[15-9]\d{8}$/);
        }, "অনুগ্রহ করে সঠিক নম্বর টাইপ করুন'");

        form.validate({
            errorElement: 'span', //default input error message container
            errorClass: 'error-help-block', // default input error message class
            //focusInvalid: false, // do not focus the last invalid input
            ignore: [],
            highlight: function (element) { // hightlight error inputs
                $(element).closest('.form-group').addClass('form-error'); // set error class to the control group
            },
            unhighlight: function (element) { // revert the change done by hightlight
                $(element).closest('.form-group').removeClass('form-error'); // set error class to the control group
            },
            success: function (label) {
                label.closest('.form-group').removeClass('form-error'); // set success class to the control group
            },
            submitHandler: function (form, event) {  
                event.preventDefault();
                if (form) {
                    dataPost(action_url, submit_btn);
                }
            }
        });

        // Form validation rules
        $('.form-required').each(function () {
            $(this).rules('add', {
                required: true,
                // messages: {
                //     required: "Enter something else"
                // }
            });
        });
        $('.form-email').each(function () {
            $(this).rules('add', {
                required: true, email: true,
            });
        });
        $('.form-password').each(function () {
            $(this).rules('add', {
                required: true, minlength: 6,
            });
        });
        $('.update-form-password').each(function () {
            $(this).rules('add', {
                required: false, minlength: 6,
            });
        });
        $('.update-form-repassword').each(function () {
            $(this).rules('add', {
                required: false, equalTo: "#password", minlength: 6,
            });
        });
        $('.greater-than-first').each(function () {
            $(this).rules('add', {
                required: true,
                greaterThan: '.start-date-from',
                messages: {
                    required: "শেষের তারিখ শুরুর তারিখ থেকে বড় হতে হবে"
                }
            });
        });
        $('.form-attachment').each(function () {
            $(this).rules('add', {
                required: true,
                extension: "jpg|jpeg|png|pdf",
                filesize: 1048576,
                messages: {
                    required: "সংযুক্তির ধরন JPG, PNG বা পিডিএফ এবং 1 MB চেয়ে কম হতে হবে", 
                    filesize: "সংযুক্তির ধরন 1 MB চেয়ে কম হতে হবে"
                }
            });
        });

    });

    // Data post ajax common function, like insert or update
    function dataPost(action_url, submit_btn) {
        $.ajax({
            url: action_url,
            dataType: 'json',
            data: submit_btn.closest('form').serialize(),
            type: 'post'
        }).done(function(res) {
            if (res) {
                if (res.flug==1) { // For data save response
                    //submit_btn.closest('form')[0].reset();
                    submit_btn.closest('form').find('.form_messsage').html('<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'+res.message+'</div>');
                    $("#common_table").dataTable().fnDestroy();
                    tableManaged();
                } else { // For error response
                    submit_btn.closest('form').find('.form_messsage').html('<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'+res.message+'</div>');
                }
            } else { // For problem with getting response
                submit_btn.closest('form').find('.form_messsage').html('<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>অনুগ্রহ করে একটু পরে আবার চেষ্টা করুন</div>');
            }
        });
    }

}

$(document).ready(function() {
    tableManaged();
    dataTableManaged();
    basicOperations();
    commonFormValidation();
});

//var numbers = {'০': 0, '১': 1, '২': 2, '৩': 3, '৪': 4, '৫': 5, '৬': 6, '৭': 7, '৮': 8, '৯': 9};
var numbers = {0: '০', 1: '১', 2: '২', 3: '৩', 4: '৪', 5: '৫', 6: '৬', 7: '৭', 8: '৮', 9: '৯'};

// English to bangla converter
function englishToBangla(input) {
    var output = [];
    for (var i = 0; i < input.length; ++i) {
        if (numbers.hasOwnProperty(input[i])) {
            output.push(numbers[input[i]]);
        } else {
            output.push(input[i]);
        }
    }
    return output.join('');
}