var paymentForm = function() {

	var basicOperations = function() {
		// Bank selection
	    $(document.body).on('change', '#bank_selection', function(event) {
	        var bank = $(this).val();
	        if (bank != '') {
	            $.ajax({
	                url: baseUrl+'get-bank-branch',
	                type: 'GET',
	                dataType: 'html',
	                data: {bank: bank},
	            })
	            .done(function(res) {
	                $('#branch_selection').val(res);
	            });
	        }
	    });

	    $(document.body).on('change', '#payment_method', function(event) {
	        var method = $(this).val();
	        if ( method == 1 ) {
	            $('#payment_form').find('.bank_pay_order').show();
	            $('#payment_form').find('.cash_order').hide();
	        } else if(  method == 3 ) {
	            $('#payment_form').find('.bank_pay_order').hide();
	            $('#payment_form').find('.cash_order').show();
	        } else {
	            $('#payment_form').find('.bank_pay_order').hide();
	            $('#payment_form').find('.cash_order').hide();
	        }
	    });
	}

	var paymentFormValidation = function() {

		$(document.body).on('click', '.payment_form_submit', function() {

	        var submit_btn  = $(this);
	        var form        = submit_btn.closest('form');
	        var action_url  = submit_btn.data('action');

	        $.validator.addMethod('filesize', function(value, element, param) {
	            return this.optional(element) || (element.files[0].size <= param) 
	        });

	        form.validate({
	            errorElement: 'span', //default input error message container
	            errorClass: 'help-block', // default input error message class
	            //focusInvalid: false, // do not focus the last invalid input
	            //ignore: "",
	            rules: {
	                PAYMENT_METHOD: {
	                    required: true,
	                },
	                B_DRAFT_NO: {
	                    required: function(element) {
	                        return ($('#payment_method').val() == 1);
	                    }
	                },
	                BANK_ID: {
	                    required: function(element) {
	                        return ($('#payment_method').val() == 1);
	                    }
	                },
	                B_DRAFT_DATE: {
	                    required: function(element) {
	                        return ($('#payment_method').val() == 1);
	                    }
	                },
	                CHALAN_NO: {
	                    required: function(element) {
	                        return ($('#payment_method').val() == 3);
	                    }
	                },
	                CASH_DATE: {
	                    required: function(element) {
	                        return ($('#payment_method').val() == 3);
	                    }
	                },
	                BANK_ATTACHMENT: {
	                    required: function(element) {
	                        return ($('#payment_method').val() == 1);
	                    },
	                    extension: "jpg|jpeg|png",
	                    filesize: 1048576,
	                    // messages: {
	                    //     required: "সংযুক্তির ধরন jpg, jpeg, png এবং 1 MB চেয়ে কম হতে হবে", 
	                    //     filesize: "সংযুক্তির ধরন 1 MB চেয়ে কম হতে হবে"
	                    // }
	                },
	                CASH_ATTACHMENT: {
	                    required: false,
	                    extension: "jpg|jpeg|png",
	                    filesize: 1048576,
	                    // messages: {
	                    //     required: "সংযুক্তির ধরন jpg, jpeg, png এবং 1 MB চেয়ে কম হতে হবে", 
	                    //     filesize: "সংযুক্তির ধরন 1 MB চেয়ে কম হতে হবে"
	                    // }
	                },
	            },
	            // messages: {
	            //     "name": "Please type your name",
	            //     "password": "Please put your password",
	            // },
	            errorPlacement: function (error, element) {
	                if(element.attr("name") == "B_DRAFT_DATE") error.appendTo("#error_msg1")
	                if(element.attr("name") == "CASH_DATE") error.appendTo("#error_msg2")
	                else error.insertAfter($(element))
	            },
	            highlight: function (element) { // hightlight error inputs
	                $(element).closest('.form-group').addClass('form-error'); // set error class to the control group
	            },
	            unhighlight: function (element) { // revert the change done by hightlight
	                $(element).closest('.form-group').removeClass('form-error'); // set error class to the control group
	            },
	            success: function (label) {
	                label.closest('.form-group').removeClass('form-error'); // set success class to the control group
	            },
	            submitHandler: function (form, event) {  
	                event.preventDefault();
	                if (form) {
	                	submit_btn.addClass('btn_loader').removeClass('btn-success green');
	                    if ($('#payment_method').val() == 1) {
	                    	dataPost(action_url, submit_btn);
	                    } else if($('#payment_method').val() == 2) {
	                    	$('#payment_gw').find('.payment_post_form').trigger('click');
	                    } else if($('#payment_method').val() == 3) {
	                    	dataPost(action_url, submit_btn);
	                    }
	                }
	            }
	        });
	    });

		// Data post ajax common function, like insert or update
	    function dataPost(action_url, submit_btn) {

	    	var token           = submit_btn.closest('form').find('input[name=_token]').val();
	    	var total_amount  	= submit_btn.closest('form').find('#TOTAL_AMOUNT').val();
	    	var payment_method  = submit_btn.closest('form').find('#payment_method').val();
	    	var bank_druft  	= submit_btn.closest('form').find('#B_DRAFT_NO').val();
	    	var bank_id  		= submit_btn.closest('form').find('#bank_selection').val();
	    	var bank_branch		= submit_btn.closest('form').find('#branch_selection').val();
	    	var bank_date		= submit_btn.closest('form').find('#B_DRAFT_DATE').val();
	    	var bank_attachment = submit_btn.closest('form').find('#BANK_ATTACHMENT');
	    	var cash_date 		= submit_btn.closest('form').find('#CASH_DATE').val();
	    	var challan_no 		= submit_btn.closest('form').find('#CHALAN_NO').val();
	    	var cash_attachment	= submit_btn.closest('form').find('#CASH_ATTACHMENT');
	    	var comment			= submit_btn.closest('form').find('#COMMENTS').val();

	    	var form_data = new FormData();
            form_data.append("_token", token);
            form_data.append("total_amount", total_amount);
            form_data.append("payment_method", payment_method);
            form_data.append("comment", comment);

            if (payment_method == 1) {
            	form_data.append("bank_druft", bank_druft);
            	form_data.append("bank_id", bank_id);
            	form_data.append("bank_branch", bank_branch);
            	form_data.append("bank_date", bank_date);
            	if ( bank_attachment[0].files.length > 0 ) {
            		form_data.append("bank_attachment", bank_attachment[0].files[0]);
            	}
            } else if (payment_method == 3) {
            	form_data.append("cash_date", cash_date);
            	form_data.append("challan_no", challan_no);
            	if ( cash_attachment[0].files.length > 0 ) {
            		form_data.append("cash_attachment", cash_attachment[0].files[0]);
            	}
            }

	        $.ajax({
	            url: action_url,
	            dataType: 'json',
                cache: false,
                contentType: false,
                processData: false,
                async: false,
                data: form_data,
                type: 'post'
	        }).done(function(res) {
	        	submit_btn.removeClass('btn_loader').addClass('btn-success green');
	            if (res.url) {
	            	setTimeout(function() {
					    window.location.href = res.url;
					}, 2000);
	            } else {
	            	$('#modal_add_content').modal('hide');
	            	$('.portlet').find('.contentArea').html(res.message);
	            }
	        });
	    }

	}

	return {
        //main function to initiate the module
        init: function () {
            basicOperations();
            paymentFormValidation();
        }
    };

}();