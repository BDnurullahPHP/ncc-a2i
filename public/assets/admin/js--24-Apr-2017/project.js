// Project management
var projectManaged = function () {

    var projectOperations = function() {

        $(document.body).on('click', '.pr_add_more', function(event) {
            var row_data = $(this).closest('.form-group').find('.pr_details_field .item_single_list').last().html();
            $(this).closest('.form-group').find('.pr_details_field').prepend('<ul class="item_single_list">'+row_data+'<li class="remove_item_data"><i class="fa fa-times"></i></li></ul>');
        });

        $(document.body).on('click', 'li.remove_item_data', function(event) {
            $(this).closest('ul').remove();
        });

        $(document.body).on('click', '.remove_project_sketch', function(event) {
            console.log($(this).find('input'));
        });

        $(document.body).on('change', '#area', function(event) {
            var zone = $(this).val();
            $('#ward').val('');
            $('#mouza').val('');
            if (zone != '') {
                $.ajax({
                    url: baseUrl+'get-area-land',
                    type: 'GET',
                    dataType: 'json',
                    data: {zone: zone},
                })
                .done(function(res) {
                    $('#ward').html(res.ward);
                    $('#project_lands').html(res.land).select2();
                });
            }
        });

        $(document.body).on('change', '#ward', function(event) {
            var zone = $('#area').val();
            var ward = $(this).val();
            $('#mouza').val('');
            if (ward != '' && zone != '') {
                $.ajax({
                    url: baseUrl+'get-ward-land',
                    type: 'GET',
                    dataType: 'json',
                    data: {zone: zone, ward: ward},
                })
                .done(function(res) {
                    $('#mouza').html(res.mouza);
                    $('#project_lands').html(res.land).select2();
                });
            }
        });

        $(document.body).on('change', '#mouza', function(event) {
            var zone = $('#area').val();
            var ward = $('#ward').val();
            var mouza = $(this).val();
            if (zone != '' && ward != '' && mouza != '') {
                $.ajax({
                    url: baseUrl+'get-mouza-land',
                    type: 'GET',
                    dataType: 'json',
                    data: {zone: zone, ward: ward, mouza: mouza},
                })
                .done(function(res) {
                    $('#project_lands').html(res.land).select2();
                });
            }
        });

        $(document.body).on('click', '.project_add_more', function(event) {
            var row_data = $(this).closest('.form-group').find('.attachment_single').first().html();
            $(this).parent('div').before('<div class="col-md-4 attachment_single">'+row_data+'<span class="remove_file" title="Remove"><i class="fa fa-times"></i></span><div/>');
        });
        $(document.body).on('click', 'span.remove_file', function(event) {
            $(this).closest('.attachment_single').remove();
        });
        // Delete post
        $(document.body).on('click', 'a.remove_project_file', function(event) {
            event.preventDefault();
            
            var parent_row  = $(this).closest('div');
            var get_url     = $(this).attr('href').split('=');
            
            swal({   
                title: "Are you sure?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No, cancel",
                closeOnConfirm: false, //If false then confirm message will enable
                closeOnCancel: true
            }, function(isConfirm){
                if (isConfirm) {

                    $.ajax({
                        url: get_url,
                        type: 'GET',
                        dataType: 'html',
                        data: false,
                    })
                    .done(function(res) {
                        parent_row.remove();
                        swal("Deleted", res, "success");
                    });

                }
            });
        });
        
        // Demesne Tender on change schedule id
        $(document.body).on('change', '#tender_no', function() {
            var tender_id = $(this).val();
            if (tender_id != '') {
                $.ajax({
                    url: tender_schedule_url,
                    type: 'GET',
                    dataType: 'html',
                    data: {tender_id: tender_id},
                })
                .done(function(response) {
                    $('#mahal_id').html(response);
                });
            }
        });
        // Demesne money collection
        $(document.body).on('change', '#schedule_id', function() {
            var schedule_id = $(this).val();
            if (schedule_id != '') {
                $.ajax({
                    url: tender_schedule_url,
                    type: 'GET',
                    dataType: 'html',
                    data: {schedule_id: schedule_id},
                })
                .done(function(response) {
                    $('#demesne_info').html(response);
                });
            }
        });

        $(document.body).on('click', '.remove_item_record', function(event) {
            var parent = $(this).closest('ul');
            var url = $(this).data('url');
            swal({   
                title: "Are you sure?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No, cancel",
                closeOnConfirm: false, //If false then confirm message will enable
                closeOnCancel: true
            }, function(isConfirm){
                if (isConfirm) {
                    $.ajax({
                        url: url,
                        type: 'GET',
                        dataType: 'html',
                        data: false,
                    })
                    .done(function(res) {
                        parent.remove();
                        swal("Deleted", res, "success");
                    });
                }
            });
        });

    }

    var projectFormvalidation = function() {

        $(document.body).on('click', '#project_submit', function(){
            var submit_btn      = $(this);
            var submit_parent   = submit_btn.parent('div');
            var submit_form     = submit_btn.closest('form');

            // To validate filesize
            $.validator.addMethod('filesize', function(value, element, param) {
                return this.optional(element) || (element.files[0].size <= param) 
            }); 

            $("#project_form").validate({
                errorElement: 'span', //default input error message container
                errorClass: 'help-block', // default input error message class
                //focusInvalid: false, // do not focus the last invalid input
                ignore: [],
                errorPlacement: function (error, element) {
                    // if (element.attr("type") == "radio") {
                    //     error.insertAfter($(element).parents('div'));
                    // } else {
                    //     error.insertAfter($(element));
                    // }
                    if(element.attr("name") == "LAND_ID") error.appendTo("#error_msg1")
                    else error.insertAfter($(element))
                },
                highlight: function (element) { // hightlight error inputs
                    $(element).closest('.form-group').addClass('form-error'); // set error class to the control group
                },
                unhighlight: function (element) { // revert the change done by hightlight
                    $(element).closest('.form-group').removeClass('form-error'); // set error class to the control group
                },
                success: function (label) {
                    label.closest('.form-group').removeClass('form-error'); // set success class to the control group
                },
                submitHandler: function (form, event) { 
                    event.preventDefault();
                    if (form) {

                        submit_btn.addClass('btn_loader').removeClass('btn-success');

                        var action_url          = submit_parent.find('#project_submit').data('action');
                        var token               = submit_form.find('input[name=_token]').val();
                        var project_land        = $('#project_lands').val();
                        var project_number      = $('#project_number').val();
                        var project_start_date  = $('#project_start_date').val();
                        var project_end_date    = $('#project_end_date').val();
                        var project_name        = $('#project_name').val();
                        var project_name_bn     = $('#project_name_bn').val();
                        var project_details     = $('#project_details').val();
                        var project_location    = $('#project_location').val();
                        var project_file        = $('#project_files').find('.attachment_single');
                        var project_is_active   = $('#project_is_active:checked').val();

                        var project_shop        = $('#project_shops').find('.pr_details_field .item_single_list');
                        var project_flat        = $('#project_flats').find('.pr_details_field .item_single_list');
                        var project_space       = $('#project_spaces').find('.pr_details_field .item_single_list');

                        var form_data           = new FormData();
                        form_data.append("_token", token);
                        form_data.append("land", project_land);
                        form_data.append("number", project_number);
                        form_data.append("start_date", project_start_date);
                        form_data.append("end_date", project_end_date);
                        form_data.append("name", project_name);
                        form_data.append("name_bn", project_name_bn);
                        form_data.append("details", project_details);
                        form_data.append("location", project_location);
                        form_data.append("is_active", typeof project_is_active != 'undefined' ? 1 : 0);
                        
                        project_file.each(function(index, el) {
                            if ( $(this).find('input.project_file')[0].files.length > 0 ) {
                                form_data.append('project_doc['+index+'][doc]', $(this).find('input.project_file')[0].files[0]);
                            }
                        });

                        project_shop.each(function(index, el) {
                            if ( $(this).find('.pr_type').val() != '' && $(this).find('.pr_position').val() != '' && $(this).find('.pr_measurement').val() != '' && $(this).find('.pr_uom').val() != '' ) {
                                if ($(this).find('input.pr_details_id').length > 0) {
                                    form_data.append('project_shop['+index+'][details_id]', $(this).find('input.pr_details_id').val());
                                }
                                form_data.append('project_shop['+index+'][category]', $(this).find('input.pr_category').val());
                                form_data.append('project_shop['+index+'][type]', $(this).find('.pr_type').val());
                                form_data.append('project_shop['+index+'][position]', $(this).find('.pr_position').val());
                                form_data.append('project_shop['+index+'][ssf_no]', $(this).find('.pr_ssf_no').val());
                                form_data.append('project_shop['+index+'][measurement]', $(this).find('.pr_measurement').val());
                                form_data.append('project_shop['+index+'][uom]', $(this).find('.pr_uom').val());

                                form_data.append('project_shop['+index+'][is_reserved]', typeof $(this).find('.pr_is_reserved:checked').val() != 'undefined' ? 1 : 0);
                                form_data.append('project_shop['+index+'][is_build]', typeof $(this).find('.pr_is_build:checked').val() != 'undefined' ? 1 : 0);
                                form_data.append('project_shop['+index+'][is_active]', typeof $(this).find('.pr_is_active:checked').val() != 'undefined' ? 1 : 0);
                            }
                        });
                        project_flat.each(function(index, el) {
                            if ( $(this).find('.pr_type').val() != '' && $(this).find('.pr_position').val() != '' && $(this).find('.pr_measurement').val() != '' && $(this).find('.pr_uom').val() != '' ) {
                                if ($(this).find('input.pr_details_id').length > 0) {
                                    form_data.append('project_flat['+index+'][details_id]', $(this).find('input.pr_details_id').val());
                                }
                                form_data.append('project_flat['+index+'][category]', $(this).find('input.pr_category').val());
                                form_data.append('project_flat['+index+'][type]', $(this).find('.pr_type').val());
                                form_data.append('project_flat['+index+'][position]', $(this).find('.pr_position').val());
                                form_data.append('project_flat['+index+'][ssf_no]', $(this).find('.pr_ssf_no').val());
                                form_data.append('project_flat['+index+'][measurement]', $(this).find('.pr_measurement').val());
                                form_data.append('project_flat['+index+'][uom]', $(this).find('.pr_uom').val());
                                form_data.append('project_flat['+index+'][is_reserved]', typeof $(this).find('.pr_is_reserved:checked').val() != 'undefined' ? 1 : 0);
                                form_data.append('project_flat['+index+'][is_build]', typeof $(this).find('.pr_is_build:checked').val() != 'undefined' ? 1 : 0);
                                form_data.append('project_flat['+index+'][is_active]', typeof $(this).find('.pr_is_active:checked').val() != 'undefined' ? 1 : 0);
                            }
                        });
                        project_space.each(function(index, el) {
                            if ( $(this).find('.pr_type').val() != '' && $(this).find('.pr_position').val() != '' && $(this).find('.pr_measurement').val() != '' && $(this).find('.pr_uom').val() != '' ) {
                                if ($(this).find('input.pr_details_id').length > 0) {
                                    form_data.append('project_space['+index+'][details_id]', $(this).find('input.pr_details_id').val());
                                }
                                form_data.append('project_space['+index+'][category]', $(this).find('input.pr_category').val());
                                form_data.append('project_space['+index+'][type]', $(this).find('.pr_type').val());
                                form_data.append('project_space['+index+'][position]', $(this).find('.pr_position').val());
                                form_data.append('project_space['+index+'][ssf_no]', $(this).find('.pr_ssf_no').val());
                                form_data.append('project_space['+index+'][measurement]', $(this).find('.pr_measurement').val());
                                form_data.append('project_space['+index+'][uom]', $(this).find('.pr_uom').val());
                                form_data.append('project_space['+index+'][is_reserved]', typeof $(this).find('.pr_is_reserved:checked').val() != 'undefined' ? 1 : 0);
                                form_data.append('project_space['+index+'][is_build]', typeof $(this).find('.pr_is_build:checked').val() != 'undefined' ? 1 : 0);
                                form_data.append('project_space['+index+'][is_active]', typeof $(this).find('.pr_is_active:checked').val() != 'undefined' ? 1 : 0);
                            }
                        });

                        $.ajax({
                            url: action_url,
                            dataType: 'json',
                            cache: false,
                            contentType: false,
                            processData: false,
                            async: false,
                            data: form_data,
                            type: 'post'
                        }).done(function(res) {
                            if (res) {
                                submit_btn.removeClass('btn_loader').addClass('btn-success');
                                if (res.flug==1) { // For data save response
                                    $('#lg_form_message').html('<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'+res.message+'</div>');
                                    $("#common_table").dataTable().fnDestroy();
                                    $('#modal_big_content').modal('hide');
                                    tableManaged();
                                } else { // For error response
                                    submit_btn.closest('form').find('.form_messsage').html('<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'+res.message+'</div>');
                                }
                            } else { // For problem with getting response
                                submit_btn.closest('form').find('.form_messsage').html('<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>অনুগ্রহ করে একটু পরে আবার চেষ্টা করুন</div>');
                            }
                        });
                    }
                }
            });
            
            $('.form-required').each(function () {
                $(this).rules('add', {
                    required: true,
                    // messages: {
                    //     required: "Enter something else"
                    // }
                });
            });
            $('.form-number').each(function () {
                $(this).rules('add', {
                    required: true,
                    number: true,
                });
            });
            $('.form-attachment').each(function () {
                $(this).rules('add', {
                    required: true,
                    extension: "jpg|jpeg|png|pdf",
                    filesize: 1048576,
                    messages: {
                        required: "সংযুক্তির ধরন JPG, PNG বা পিডিএফ এবং 1 MB চেয়ে কম হতে হবে", 
                        filesize: "সংযুক্তির ধরন 1 MB চেয়ে কম হতে হবে"
                    }
                });
            });
            $('.form-attachment-update').each(function () {
                $(this).rules('add', {
                    required: false,
                    extension: "jpg|jpeg|png|pdf",
                    filesize: 1048576,
                    messages: {
                        required: "সংযুক্তির ধরন JPG, PNG বা পিডিএফ এবং 1 MB চেয়ে কম হতে হবে", 
                        filesize: "সংযুক্তির ধরন 1 MB চেয়ে কম হতে হবে"
                    }
                });
            });
        });

    }

    return {
        //main function to initiate the module
        init: function () {
            projectOperations();
            projectFormvalidation();
        }
    };

}();