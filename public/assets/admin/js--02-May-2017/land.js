// Land
var landManaged = function () {

    var landOperations = function() {

        $(document.body).on('change', '#area', function(event) {
            var zone = $(this).val();
            $('#ward').val('');
            $('#mouza').val('');
            $('#jl_no').val('');
            if (zone != '') {
                $.ajax({
                    url: baseUrl+'get-ward',
                    type: 'GET',
                    dataType: 'html',
                    data: {zone: zone},
                })
                .done(function(res) {
                    $('#ward').html(res);
                });
            }
        });

        $(document.body).on('change', '#ward', function(event) {
            var ward = $(this).val();
            $('#mouza').val('');
            $('#jl_no').val('');
            if (ward != '') {
                $.ajax({
                    url: baseUrl+'get-mouza',
                    type: 'GET',
                    dataType: 'html',
                    data: {ward: ward},
                })
                .done(function(res) {
                    $('#mouza').html(res);
                });
            }
        });

        $(document.body).on('change', '#category', function(event) {
            var category = $(this).val();
            $('#subcategory').val('');
            if (category != '') {
                $.ajax({
                    url: baseUrl+'get-land-subcategory',
                    type: 'GET',
                    dataType: 'html',
                    data: {category: category},
                })
                .done(function(res) {
                    $('#subcategory').html(res);
                });
            }
        });

        $(document.body).on('change', 'input[name=is_case]', function(event) {
            if ($(this).val() == 1) {
                $(this).closest('.form-group').find('.case_details_field').show();
            } else {
                $(this).closest('.form-group').find('.case_details_field').hide();
            }
        });

        $(document.body).on('click', '.add_more', function(event) {
            var row_data = $(this).closest('.row_group').find('.row_data_content .row_single').last().html();
            $(this).closest('.row_group').find('.row_data_content').prepend('<div class="row row_single append_item">'+row_data+'<span class="remove_item" title="Remove"><i class="fa fa-times"></i></span><div/>');
            $(this).closest('.row_group').find('.row_data_content .row_single').find('input.date_picker').removeClass('hasDatepicker').removeAttr('id');
            
            var row_select = $(this).closest('.row_group').find('.row_data_content .row_single');
            
            row_select.each(function(index, el) {
                
                var input_group = $(this).find('input');
                
                input_group.each(function(key, elm) {
                    var input_name = $(this).attr('name');
                    $(this).attr('name', input_name + '_' + index);
                });
                    
            });
        });

        $(document.body).on('click', '.file_add_more', function(event) {
            var row_data = $(this).closest('.row_group').find('.row_data_content .row_single').first().html();
            $(this).closest('.row_group').find('.row_data_content').append('<div class="col-md-12 col-sm-12 col-xs-12 row_single append_item append_file">'+row_data+'<span class="remove_file" title="Remove"><i class="fa fa-times"></i></span><div/>');
        });

        $(document.body).on('click', 'span.remove_item, span.remove_file', function(event) {
            $(this).closest('.row_single').remove();
        });

        $(document.body).on('click', '.remove_item_record, .remove_doc_record', function(event) {
            if ( $(this).hasClass('remove_doc_record') ) {
                var parent = $(this).closest('.doc_single');
            } else {
                var parent = $(this).closest('.row_single');
            };
            
            var url = $(this).data('url');
            swal({   
                title: "Are you sure?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No, cancel",
                closeOnConfirm: false, //If false then confirm message will enable
                closeOnCancel: true
            }, function(isConfirm){
                if (isConfirm) {
                    $.ajax({
                        url: url,
                        type: 'GET',
                        dataType: 'html',
                        data: false,
                    })
                    .done(function(res) {
                        parent.remove();
                        swal("Deleted", res, "success");
                    });
                }
            });
        });

        $(document.body).on('click', '.land_type_add', function(event) {
            var parent      = $(this).closest('.form-group');
            var tcat        = parent.find('#category').val();
            var tcat_txt    = parent.find('#category option:selected').text();
            var tsubcat     = parent.find('#subcategory').val();
            var tsubcat_txt = parent.find('#subcategory option:selected').text();
            var tuse        = parent.find('#land_use').val();
            var total_land  = $('#total_land').val();

            if (isNaN(total_land) || total_land == 0) {
                sweetAlert("Oops...", "Total land required!", "error");
            } else {
                if ( tcat != '' && tsubcat != '' && tuse != '' ) {

                    var is_valid = landCal();

                    if (is_valid == 1) {
                        parent.find('#category').val('');
                        parent.find('#subcategory').val('');
                        parent.find('#land_use').val('');
                        $('#land_use_table').find('tr.total_row').show();

                        $('#land_use_table').prepend('<tr class="single_row"><td class="category_single" data-category="'+tcat+'">'+tcat_txt+'</td><td class="subcategory_single" data-subcategory="'+tsubcat+'">'+tsubcat_txt+'</td><td class="single_use" data-uses="'+tuse+'">'+tuse+'<span class="land_type_use"><i class="fa fa-times"></i></span></td></tr>');
                    } else {
                        sweetAlert("Oops...", "Total uses of land is not bigger than total land!", "error");
                    }
                } else {
                    sweetAlert("Oops...", "All field required!", "error");
                }
            }
                
        });
        $(document.body).on('click', 'span.land_type_use', function(event) {
            var total_lenght = $(this).closest('table').find('tr.single_row').length;
            if ( (total_lenght-1) == 0) {
                $(this).closest('table').find('tr.total_row').hide();
            } else {
                $(this).closest('table').find('tr.total_row').show();
            }
            $(this).closest('tr').remove();
            landCal();
        });
        $(document.body).on('click', 'span.remove_land_type_use', function(event) {
            var state = $(this);
            var url = $(this).data('url');
            swal({   
                title: "Are you sure?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No, cancel",
                closeOnConfirm: false, //If false then confirm message will enable
                closeOnCancel: true
            }, function(isConfirm){
                if (isConfirm) {
                    $.ajax({
                        url: url,
                        type: 'GET',
                        dataType: 'html',
                        data: false,
                    })
                    .done(function(res) {
                        var total_lenght = state.closest('table').find('tr.single_row').length;
                        if ( (total_lenght-1) == 0) {
                            state.closest('table').find('tr.total_row').hide();
                        } else {
                            state.closest('table').find('tr.total_row').show();
                        }
                        state.closest('tr').remove();
                        landCal();
                        swal("Deleted", res, "success");
                    });
                }
            });
        });

        $(document.body).on('click', 'a.edit_land_info', function(event) {
            event.preventDefault();
            $('#land_modal').modal('show');
        });

        $(document.body).on('blur', '#ousted_land', function(event) {
            var ousted = parseFloat($(this).val());
            var possession = parseFloat($('#possession_land').val());

            if (!isNaN(ousted) && !isNaN(possession))
                $('#total_land').val(ousted + possession);
        });

        $(document.body).on('blur', '#possession_land', function(event) {
            var ousted = parseFloat($(this).val());
            var possession = parseFloat($('#ousted_land').val());

            if (!isNaN(ousted) && !isNaN(possession))
                $('#total_land').val(ousted + possession);
        });

        function landCal() {
            var sum             = 0;
            var base_total      = Number($('#total_land').val());
            var single_total    = Number($('#land_use').val());
            var total_lands     = $('#land_use_table').find('.total_row').find('.total_uses');
            var rowCount        = $('#land_use_table >tbody >tr').length;

            if (rowCount > 1) {
                sum             = single_total;
                var land_rows   = $('#land_use_table').find('.single_row').find('.single_use');
                land_rows.each(function() {
                    sum += Number($(this).data('uses'));
                });
                if (sum > base_total) {
                    return 0;
                } else {
                    total_lands.find('strong').text(sum);
                    return 1;
                }
            } else {
                if (single_total > base_total) {
                    return 0;
                } else {
                    total_lands.find('strong').text(single_total);
                    return 1;
                }
            }
        }

    }

    // Get case selection value
    // function getCaseValue() {
    //     return $('input[name=is_case]:checked', '#land_form').val();
    // }

    var landFormvalidation = function() {

        $(document.body).on('click', '#land_submit', function() {
            var submit_btn  = $(this);
            var submit_form = submit_btn.closest('form');

            // To validate filesize
            $.validator.addMethod('filesize', function(value, element, param) {
                return this.optional(element) || (element.files[0].size <= param) 
            }); 

            $("#land_form").validate({
                errorElement: 'span', //default input error message container
                errorClass: 'help-block', // default input error message class
                //focusInvalid: false, // do not focus the last invalid input
                ignore: '',
                highlight: function (element) { // hightlight error inputs
                    $(element).closest('.form-group').addClass('form-error'); // set error class to the control group
                },
                unhighlight: function (element) { // revert the change done by hightlight
                    $(element).closest('.form-group').removeClass('form-error'); // set error class to the control group
                },
                success: function (label) {
                    label.closest('.form-group').removeClass('form-error'); // set success class to the control group
                },
                submitHandler: function (form, event) {
                    event.preventDefault();
                    if (form) {

                        var valid = false;

                        var action_url      = $('#land_form').find('#land_submit').data('action');
                        var token           = $('#land_form').find('input[name=_token]').val();
                        var land_owner      = $('#land_owner').val();
                        var area            = $('#area').val();
                        var ward            = $('#ward').val();
                        var mouza           = $('#mouza').val();
                        var area_name       = $('#area option:selected').text();
                        var ward_name       = $('#ward option:selected').text();
                        var mouza_name      = $('#mouza option:selected').text();
                        var land_no         = area_name+'/'+ward_name+'/'+mouza_name;
                        var land_use        = $('#land_use_table').find('.single_row');
                        var possession_land = $('#possession_land').val();
                        var ousted_land     = $('#ousted_land').val();
                        var total_land      = Number($('#total_land').val());
                        var total_lands     = $('#land_use_table').find('.total_row').find('.total_uses').find('strong').text();
                        var land_rs_cs      = $('#land_form').find('.land_rs_cs_row');
                        var land_details    = $('#land_details').val();
                        var land_dolil      = $('#dolil_multiple').find('.row_single');
                        var land_dcr        = $('#dcr_multiple').find('.row_single');
                        var land_ledger     = $('#ledger_multiple').find('.row_single');
                        var land_bahia      = $('#bahia_multiple').find('.row_single');
                        var land_tax        = $('#tax_multiple').find('.row_single');
                        var remarks         = $('#comments').val();
                        var is_case         = $('input[name=is_case]:checked', '#land_form').val();
                        var land_is_show    = $('input[name=is_active]:checked', '#land_form').val();

                        var case_number         = $('#land_form').find('.case_details_field').find('.case_number').val();
                        var case_complainant    = $('#land_form').find('.case_details_field').find('.case_complainant').val();
                        var case_defendant      = $('#land_form').find('.case_details_field').find('.case_defendant').val();
                        var settlement_date     = $('#land_form').find('.case_details_field').find('.settlement_date').val();
                        var court_document      = $('#land_form').find('.case_details_field').find('.court_document');
                        var case_remarks        = $('#land_form').find('.case_details_field').find('.case_remarks').val();

                        var khatian_exists      = $('#khatian_multiple').find('.row_single').first().find('input[type=text]');
                        var dag_no_exists       = $('#dag_multiple').find('.row_single').first().find('input[type=text]');
                        var jl_no_exists        = $('#jl_multiple').find('.row_single').first().find('input[type=text]');
                        var owner_exists        = $('#owner_multiple').find('.row_single').first().find('input[type=text]');

                        var form_data = new FormData();
                        form_data.append("_token", token);
                        form_data.append("land_no", land_no);
                        form_data.append("land_owner", land_owner);
                        form_data.append("area", area);
                        form_data.append("ward", ward);
                        form_data.append("mouza", mouza);
                        form_data.append("possession_land", possession_land);
                        form_data.append("ousted_land", ousted_land);
                        form_data.append("total_land", total_land);
                        form_data.append("land_details", land_details);
                        form_data.append("remarks", remarks);
                        form_data.append("is_case", is_case);
                        form_data.append("is_active", land_is_show);

                        if (is_case == 1) {

                            var case_id = $('#land_form').find('.case_details_field').find('input.land_case_id');
                            if (case_id.length > 0) {
                                form_data.append("case_id", case_id.val());
                            }
                            form_data.append("case_number", case_number);
                            form_data.append("case_complainant", case_complainant);
                            form_data.append("case_defendant", case_defendant);
                            form_data.append("settlement_date", settlement_date);
                            form_data.append("case_remarks", case_remarks);

                            if ( court_document[0].files.length > 0 ) {
                                form_data.append("court_document", court_document[0].files[0]);
                            }

                        }

                        land_rs_cs.each(function(index, el) {
                            var dag_each = $(this).find('.row_single');
                            dag_each.each(function(key, elm) {
                                var dag_val_rs = $(this).find('input.land_rs').val();
                                var dag_val_cs = $(this).find('input.land_cs').val();
                                var dag_val_sa = $(this).find('input.land_sa').val();
                                if ( $(this).find('input.dag_id').length > 0 ) {
                                    form_data.append('land_rs_cs['+index+']['+key+'][dag_id]', $(this).find('input.dag_id').val());
                                }
                                if (dag_val_rs != '' || dag_val_cs != '' || dag_val_sa != '') {
                                    form_data.append('land_rs_cs['+index+']['+key+'][type]', $(this).closest('.row_group').find('input.type').val());
                                }
                                if ( dag_val_rs != '' ) {
                                    form_data.append('land_rs_cs['+index+']['+key+'][rs]', dag_val_rs);
                                }
                                if ( dag_val_cs != '' ) {
                                    form_data.append('land_rs_cs['+index+']['+key+'][cs]', dag_val_cs);
                                }
                                if ( dag_val_sa != '' ) {
                                    form_data.append('land_rs_cs['+index+']['+key+'][sa]', dag_val_sa);
                                }
                            });
                        });

                        land_use.each(function(index, el) {
                            if ( typeof $(this).data('landuses') != 'undefined' ) {
                                form_data.append('land_use['+index+'][use_id]', $(this).data('landuses'));
                            }
                            form_data.append('land_use['+index+'][category]', $(this).find('.category_single').data('category'));
                            form_data.append('land_use['+index+'][subcategory]', $(this).find('.subcategory_single').data('subcategory'));
                            form_data.append('land_use['+index+'][total]', $(this).find('.single_use').data('uses'));
                        });

                        land_dolil.each(function(index, el) {
                            if ( $(this).find('input.dolil')[0].files.length > 0 ) {
                                form_data.append('document_row['+index+'][dolil]', $(this).find('input.dolil')[0].files[0]);
                            }
                        });
                        land_dcr.each(function(index, el) {
                            if ( $(this).find('input.dcr')[0].files.length > 0 ) {
                                form_data.append('document_row['+index+'][dcr]', $(this).find('input.dcr')[0].files[0]);
                            }
                        });
                        land_ledger.each(function(index, el) {
                            if ( $(this).find('input.ledger')[0].files.length > 0 ) {
                                form_data.append('document_row['+index+'][ledger]', $(this).find('input.ledger')[0].files[0]);
                            }
                        });
                        land_bahia.each(function(index, el) {
                            if ( $(this).find('input.bahia')[0].files.length > 0 ) {
                                form_data.append('document_row['+index+'][bahia]', $(this).find('input.bahia')[0].files[0]);
                            }
                        });

                        land_tax.each(function(index, el) {
                            if ( $(this).find('input.tax').length > 0 && $(this).find('input.tax')[0].files.length > 0 ) {
                                form_data.append('land_tax['+index+'][doc]', $(this).find('input.tax')[0].files[0]);
                            }
                            if ( $(this).find('input.tax_id').length > 0 ) {
                                form_data.append('land_tax['+index+'][tax_id]', $(this).find('input.tax_id').val());
                            }
                            if ( $(this).find('.date_multiple').val() != '' ) {
                                form_data.append('land_tax['+index+'][date]', $(this).find('.date_multiple').val());
                            }
                        });

                        if ( Number(total_lands) != total_land ) {
                            sweetAlert("Oops...", "Total land is equal to total use of land", "error");
                        } else {
                            submit_btn.addClass('btn_loader').removeClass('btn-success green');
                            
                            $.ajax({
                                url: action_url,
                                dataType: 'json',
                                cache: false,
                                contentType: false,
                                processData: false,
                                async: false,
                                data: form_data,
                                type: 'post'
                            }).done(function(res) {
                                submit_btn.removeClass('btn_loader').addClass('btn-success green');
                                if (res) {
                                    if (res.flug==1) { // For data save response
                                        $('#lg_form_message').html('<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'+res.message+'</div>');
                                        $("#common_table").dataTable().fnDestroy();
                                        $('#modal_big_content').modal('hide');
                                        tableManaged();
                                    } else { // For error response
                                        submit_btn.closest('form').find('.form_messsage').html('<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'+res.message+'</div>');
                                    }
                                } else { // For problem with getting response
                                    submit_btn.closest('form').find('.form_messsage').html('<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>অনুগ্রহ করে একটু পরে আবার চেষ্টা করুন</div>');
                                }
                            });
                        }
                    }
                }
            });
            
            $('.form-required').each(function () {
                $(this).rules('add', {
                    required: true,
                    // messages: {
                    //     required: "Enter something else"
                    // }
                });
            });
            $('.form-number').each(function () {
                $(this).rules('add', {
                    required: true,
                    number: true,
                });
            });
            $('.form-number-optional').each(function () {
                $(this).rules('add', {
                    required: false,
                    number: true,
                });
            });
            $('.form-required-one').each(function () {
                $(this).rules('add', {
                    require_from_group: [1],
                    number: true,
                });
            });
            $('.form-attachment').each(function () {
                $(this).rules('add', {
                    required: true,
                    extension: "jpg|jpeg|png|pdf",
                    filesize: 1048576,
                    messages: {
                        required: "সংযুক্তির ধরন JPG, PNG বা পিডিএফ এবং 1 MB চেয়ে কম হতে হবে", 
                        filesize: "সংযুক্তির ধরন 1 MB চেয়ে কম হতে হবে"
                    }
                });
            });
            $('.attachment-optional').each(function () {
                $(this).rules('add', {
                    required: false,
                    extension: "jpg|jpeg|png|pdf",
                    filesize: 1048576,
                    messages: {
                        required: "সংযুক্তির ধরন JPG, PNG বা পিডিএফ এবং 1 MB চেয়ে কম হতে হবে", 
                        filesize: "সংযুক্তির ধরন 1 MB চেয়ে কম হতে হবে"
                    }
                });
            });
            $('.case-required').each(function () {
                $(this).rules('add', {
                    required: function(element) {
                        return ($('input[name=is_case]:checked', '#land_form').val() == 1);
                    }
                });
            });
        });

    }

    return {
        //main function to initiate the module
        init: function () {
            landOperations();
            landFormvalidation();
        }
    };

}();