$(document).ready(function() {
	
	// Call for load overview data
	loadInfo();
    
	$(".home_thumb").on('click', '.fileclick', function() {
        $("#profile_image").trigger('click');
    })

    $("#profile_image").bind('change', function() {

        readURL(this);
        var input_val = $(this).val();
        
        if (input_val != "") {
            $(".thumb_img_box img").css('display', 'block');
        }
        else{
            $(".thumb_img_box img").css('display', 'none');
        }
       
    });

    $(".thumb_img_box").on('click', '.hide_comimg', function(event) {
        event.preventDefault();
        $('.thumb_img_box img').attr('src', '').css('display', 'none');
        $("#profile_image").val('');
    });

    // User password update form validation
    $('#password_form').validate({
        errorElement: 'span',
        errorClass: 'error-help-block',
        rules: {
            current_password: {
                required: true,
            },
            new_password: {
                required: true, minlength: 6,
            },
            retype_password: {
                required: true, equalTo: "#new_password", minlength: 6,
            },
        },
        submitHandler: function (form, event) {  
            event.preventDefault();
            if (form) {
                $.ajax({
		            url: update_pass_url,
		            dataType: 'json',
		            data: $('#password_form').serialize(),
		            type: 'post'
		        }).done(function(res) {
                    if (res) {
                        if (res.flug==1) {
                            $('#password_form')[0].reset();
                            $('#password_form').find('.form_messsage').html('<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'+res.message+'</div>');
                        } else {
                            $('#password_form').find('#current_password').focus();
                            $('#password_form').find('.form_messsage').html('<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'+res.message+'</div>');
                        }
                    } else {
                        $('#password_form').find('.form_messsage').html('<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>অনুগ্রহ করে একটু পরে আবার চেষ্টা করুন</div>');
                    }
		        });
            }
        }
    });

    // User information update form validation
    $('#information_form').validate({
        errorElement: 'span',
        errorClass: 'error-help-block',
        rules: {
            first_name: {
                required: true, minlength: 3,
            },
            last_name: {
                required: true,  minlength: 3,
            },
            user_name: {
                required: true,  minlength: 3,
            },
            designation: {
                required: false, minlength: 3,
            },
            phone_no: {
                required: true, minlength: 11, maxlength: 11,
            },
            address: {
                required: true, minlength: 5,
            },
        },
        submitHandler: function (form, event) {  
            event.preventDefault();
            if (form) {
                $.ajax({
                    url: update_profile_url,
                    dataType: 'json',
                    data: $('#information_form').serialize(),
                    type: 'post'
                }).done(function(res) {
                    if (res) {
                        if (res.flug==1) {
                            $('#information_form').find('.form_messsage').html('<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'+res.message+'</div>');
                            loadInfo();
                        } else {
                            $('#information_form').find('.form_messsage').html('<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'+res.message+'</div>');
                        }
                        
                    } else {
                        $('#information_form').find('.form_messsage').html('<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>অনুগ্রহ করে একটু পরে আবার চেষ্টা করুন</div>');
                    }
                    
                });
            }
        }
    });

    // Update user image
    $(document.body).on('click', '.change_image', function(event) {
        //event.preventDefault();

        var file_parent = $(this).closest('form').find('.home_thumb');
        var img_val = file_parent.find('#profile_image').val();
        var image_msg = $(this).closest('form').find('.form_messsage');

        if ( img_val != '' && file_parent.find('#profile_image')[0].files.length > 0 ) {

            var extension = file_parent.find('#profile_image').val().split('.').pop();

            if (['png', 'jpg', 'jpeg'].indexOf(extension) > -1) {

                var token = $(this).closest('form').find('input[name=_token]').val();
                var image = file_parent.find('#profile_image')[0].files[0];

                var form_data = new FormData();
                form_data.append("_token", token);
                form_data.append("image", image);

                $.ajax({
                    url: update_picture_url,
                    dataType: 'json',
                    cache: false,
                    contentType: false,
                    processData: false,
                    async: false,
                    data: form_data,
                    type: 'post'
                }).done(function(res) {
                    if (res.flug==1) {
                        image_msg.html('<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'+res.message+'</div>');
                        $('.profile_image').attr('src', baseUrl+'public/upload/user/'+res.image);
                        file_parent.find('#profile_image').val('');
                    } else {
                        image_msg.html('<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>অনুগ্রহ করে একটু পরে আবার চেষ্টা করুন</div>');
                    }
                });

            } else {
                image_msg.html('<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Invalid file format</div>');
            }
        } else {
            image_msg.html('<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>অনুগ্রহ করে ছবি নির্বাচন করুন</div>');
        }
    });

});

// Load user information
function loadInfo() {
	$.ajax({
		url: get_overview_src,
		type: 'GET',
		dataType: 'html',
		data: false,
	})
	.done(function(res) {
		$('#account_overview').html(res);
	});
}

// show image in image upload
function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('.thumb_img_box img').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
}