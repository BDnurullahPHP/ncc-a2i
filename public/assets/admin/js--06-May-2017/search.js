var SearchTableManaged = function () {

    return {
        //main function to initiate the module
        init: function () {

            if (!jQuery().dataTable) {
                return;
            }

            var source_data         = $('#search_form').attr('action');
            var param               = $('#search_form').serialize();
            var search_field        = $('.form-search');

            var term = {};

            search_field.each(function(index, el) {
                var name = $(this).attr('name');
                var val = $(this).val();
                var element = {};

                term[name] = val;
            });

            // begin second table
            oTable2 = $('table.search_table_data').dataTable({
                "processing": true,
                "serverSide": true,
                "searching": false,
                "pagingType": "full_numbers",
                'pageLength': 10,
                "aLengthMenu": [
                    [10, 30, 50],
                    [10, 30, 50] // change per page values here
                ],
                // Load data for the table's content from an Ajax source
                "ajax": {
                    //"type": "GET",
                    "url": source_data,
                    "data": function (d){
                                d.search = JSON.stringify(term);
                            }
                },
                //Set column definition initialisation properties.
                "columnDefs": [
                    {"targets":[0],"orderable":false,},
                    {"targets": [ -1 ], "orderable": false,},
                    {"targets": [ -2 ], "orderable": false,}
                ]
            });

        }

    };

}();

var AdvanceSearch = function() {

    // Handles report basic data
    var handleSearchBasic = function() {

        $("button.search_data_btn").click(function(event) {
            $(".portlet").show();
            $("table.search_table_data").dataTable().fnDestroy();
            SearchTableManaged.init();
        });

        $('a.search_report_btn').click(function(event) {
            event.preventDefault();
            var form_data = $(this).closest('form').serialize();
            var link = $(this).attr('href');
            var url = link+'?'+form_data;
            window.open(url, '_blank');
        });

        $(function() {
            $('input.auto_search_no').autocomplete({
                source: function(request, response) {
                    var mobile_no = $('input.auto_search_no').val();
                    //console.log(mobile_no);
                    $.ajax({
                        url: citizen_number,
                        type: 'GET',
                        dataType: 'json',
                        data: {mobile_no: mobile_no},
                    })
                    .done(function(data) {
                        response(data);
                    });
                },
                select: function( event, ui ) {
                    //var selected = ui.item.label;
                    $('#user_id_val').val(ui.item.value);
                    return false;
                },
                focus: function( event, ui ) {
                    $( "input.auto_search_no" ).val( ui.item.mobile );
                    return false;
                },
            }).autocomplete( "instance" )._renderItem = function( ul, item ) {
                return $( "<li>" )
                .append( "<div>" + item.mobile + "<br>" + item.name + "</div>" )
                .appendTo( ul );
            };

        });
    }

    return {
        //main function to initiate the module
        init: function () {
            handleSearchBasic();
        }

    };

}();

var MoneyCollection = function() {

    // Handles report basic data
    var handleSearchBasic = function() {

        $("#category").change(function(event) {
            var mobile = $('#mobile_no').val();
            var category = $(this).val();
            var sc_url = $(this).closest('form').find('#user_tender_schedule_url').val();
            if ( mobile != '' && category != '' ) {
                $.ajax({
                    url: sc_url,
                    type: 'GET',
                    dataType: 'html',
                    data: {mobile: mobile, category: category},
                })
                .done(function(res) {
                    $('#tender_schedule').html(res);
                });
            }
        });

        $("button.search_citizen_payment").click(function(event) {
            $(".portlet").find('.contentArea').html('');
            var mobile = $(this).closest('form').find('#mobile_no').val();
            var category = $(this).closest('form').find('#category').val();
            var schedule = $(this).closest('form').find('#tender_schedule').val();
            var particular = $(this).closest('form').find('#particular').val();
            var loader = $(this).closest('form').find('img.form_loader');
            var url = $(this).closest('form').attr('action');

            if ( mobile != '' && category != '' && schedule != '' ) {
                loader.show();
                $.ajax({
                    url: url,
                    type: 'GET',
                    dataType: 'html',
                    data: {mobile: mobile, category: category, schedule: schedule, particular: particular},
                })
                .done(function(res) {
                    loader.hide();
                    $(".portlet").find('.contentArea').html(res);
                    $(".portlet").show();
                });
            } else {
                sweetAlert("Oops...", "All field required!", "error");
            }
        });
    }

    return {
        //main function to initiate the module
        init: function () {
            handleSearchBasic();
        }

    };

}();

var OwnershipChange = function() {

    // Handles report basic data
    var handleSearchBasic = function() {

        $("button.search_citizen_property").click(function(event) {
            $(".portlet").find('.contentArea').html('');
            var mobile = $(this).closest('form').find('#mobile_no').val();
            var category = $(this).closest('form').find('#category').val();
            var schedule = $(this).closest('form').find('#tender_schedule').val();
            var type = $(this).closest('form').find('#type').val();
            var loader = $(this).closest('form').find('img.form_loader');
            var url = $(this).closest('form').attr('action');

            if ( mobile != '' && category != '' && schedule != '' && type != '' ) {
                loader.show();
                $.ajax({
                    url: url,
                    type: 'GET',
                    dataType: 'html',
                    data: {mobile: mobile, category: category, schedule: schedule, type: type},
                })
                .done(function(res) {
                    loader.hide();
                    $(".portlet").find('.contentArea').html(res);
                    $(".portlet").show();
                });
            } else {
                sweetAlert("Oops...", "All field required!", "error");
            }
        });
        
        $(document.body).on('blur', '#cutting_rate', function(event) {
            event.preventDefault();
            var rate = $(this).val();
            var total_rate = $('#property_price').val();
            var returnable = (parseInt(total_rate)*parseInt(rate))/100;
            $('#returnable_amount').val(returnable);
        });

        $(document.body).on('click', '.property_ownership_save', function(event) {
            $('#info_msg_pring').html('');
            var submit_btn      = $(this);
            var submit_form     = submit_btn.closest('form');
            var btn_text        = submit_btn.text();
            var action_url      = submit_btn.data('action');
            var print_url       = submit_btn.data('print');
            
            //********************************************
            // Check Bangladeshi mobile no
            $.validator.addMethod("mobileBD", function(value, element) {
                return this.optional(element) || value === "NA" || 
                    value.match(/^(?:\+?88)?01[15-9]\d{8}$/);
            }, "অনুগ্রহ করে সঠিক নম্বর টাইপ করুন'");
            submit_form.validate({
                errorElement: 'span', //default input error message container
                errorClass: 'help-block', // default input error message class
                //focusInvalid: false, // do not focus the last invalid input
                ignore: '',
                highlight: function (element) { // hightlight error inputs
                    $(element).closest('.form-group').addClass('form-error'); // set error class to the control group
                },
                unhighlight: function (element) { // revert the change done by hightlight
                    $(element).closest('.form-group').removeClass('form-error'); // set error class to the control group
                },
                success: function (label) {
                    label.closest('.form-group').removeClass('form-error'); // set success class to the control group
                },
                submitHandler: function (form, event) {
                    event.preventDefault();
                    if (form) {
                        submit_btn.html('<i class="fa-li fa fa-spinner fa-spin"></i> '+btn_text);
                        var form_data = submit_form.serialize();
                        
                        $.ajax({
                            url: action_url,
                            dataType: 'json',
                            async: false,
                            data: form_data,
                            type: 'post'
                        }).done(function(res) {
                            submit_btn.html(btn_text);
                            if (res.flag==1) { // For data save response
                                $('#info_msg_pring').html('<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'+res.message+'</div>');
                                $(".contentArea").html('');
                                $('#modal_big_content').modal('hide');
                                window.open(print_url, '_blank');
                            } else { // For error response
                                submit_btn.closest('form').find('.form_messsage').html('<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'+res.message+'</div>');
                            }
                        });
                    }
                }
            });
            
            $('.form-required').each(function () {
                $(this).rules('add', {
                    required: true,
                    // messages: {
                    //     required: "Enter something else"
                    // }
                });
            });
            $('.form-email').each(function () {
                $(this).rules('add', {
                    required: false, email: true,
                });
            });
            $('.form-mobile').each(function () {
                $(this).rules('add', {
                    required: true, minlength: 11, maxlength: 14, mobileBD: true,
                });
            });
            $('.form-password').each(function () {
                $(this).rules('add', {
                    required: true, minlength: 6,
                });
            });
            $('.form-repassword').each(function () {
                $(this).rules('add', {
                    required: true, equalTo: "#TE_APP_PASSWORD", minlength: 6,
                });
            });
            //********************************************
        });
    }

    return {
        //main function to initiate the module
        init: function () {
            handleSearchBasic();
        }

    };

}();