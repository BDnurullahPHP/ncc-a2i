$(".category_application").change(function(event) {
    var category = $(this).val();
    $('#application_content').html('');
    var data_url = $(this).parent('div').find('input.data_source').val();
    if ( category != '' ) {
        $('img.form_loader').show();
        $.ajax({
            url: data_url,
            type: 'GET',
            dataType: 'html',
            data: {category: category},
        })
        .done(function(res) {
            $('img.form_loader').hide();
            $('#application_content').html(res);

            // call datatable
            var source_data = $('table.datatable_ajax').data('source');
            $("table.datatable_ajax").dataTable().fnDestroy();
            // begin data table
            oTable2 = $('table.datatable_ajax').dataTable(dataTableSettings(source_data, category));
        });
    }
});

$(document.body).on('change', '#payment_method', function(event) {
    var method = $(this).val();
    console.log(method);
    if (method == 1) {
        $('.bank_pay_order').show();
        $('.cash_order').hide();
    } else if (method == 3) {
        $('.bank_pay_order').hide();
        $('.cash_order').show();
    } else {
        $('.bank_pay_order').hide();
        $('.cash_order').hide();
    }
});

$(document.body).on('change', '#division', function(event) {
    getDistrictList($(this).val());
});

$(document.body).on('change', '#district', function(event) {
    getThanaList($(this).val());
});

$(document.body).on('change', '#thana', function(event) {
    var thana = $(this).val();
    if (thana != '') {
        $.ajax({
            url: baseUrl+'get-thana',
            type: 'GET',
            dataType: 'html',
            data: {thana: thana},
        })
        .done(function(res) {
            $('#THANA_ENAME').val(res);
        });
    }
});

// Bank selection
$(document.body).on('change', '#bank_selection', function(event) {
    var bank = $(this).val();
    if (bank != '') {
        $.ajax({
            url: baseUrl+'get-bank-branch',
            type: 'GET',
            dataType: 'html',
            data: {bank: bank},
        })
        .done(function(res) {
            $('#branch_selection').val(res);
        });
    }
});

function getDistrictList(val){
    var division = val;
    if (division != '') {
        $.ajax({
            url: baseUrl+'get-district',
            type: 'GET',
            dataType: 'html',
            data: {division: division},
        })
        .done(function(res) {
            $('#district').html(res);
        });
    }
}

function getThanaList(val){
    var district = val;
    if (district != '') {
        $.ajax({
            url: baseUrl+'get-thana',
            type: 'GET',
            dataType: 'html',
            data: {district: district},
        })
        .done(function(res) {
            $('#thana').html(res);
        });
    }
}

// To calculate guarantee_percent of tender applicant amount
$('#applicant_bid_amount').blur(function(event) {
    var bid_amount = parseInt( $(this).val() );
    var guarantee = parseInt( $(this).parent('div').find('input.guarantee_percent').val() );
    $('#applicant_guarantee').val( Math.round( (bid_amount*guarantee)/100 ) );
});

// Applicant phone unique check
$(document.body).on('blur', '#applicant_phone_no', function(event) {
    var phone = $(this).val();
    var th = $(this);
    var text = $(this).parent('div').find('p.validation_error');
    var action_url = $(this).data('url');
    var disable = $(this).attr('readonly');

    if (phone != '' && !disable) {
        $.ajax({
            url: action_url,
            dataType: 'json',
            data: {phone: phone},
            type: 'get'
        }).done(function(res) {
            if (res.flug==0) {
                text.text(res.msg.message);
                th.val('');
            } else { 
                text.text('');
                th.val(phone);
            }
        });
    }
});

// Applicant email unique check
$(document.body).on('blur', '#applicant_email_id', function(event) {
    var email = $(this).val();
    var th = $(this);
    var text = $(this).parent('div').find('p.validation_error');
    var action_url = $(this).data('url');
    var disable = $(this).attr('readonly');

    if (email != '' && !disable) {
        $.ajax({
            url: action_url,
            dataType: 'json',
            data: {email: email},
            type: 'get'
        }).done(function(res) {
            if (res.flug==0) {
                text.text(res.msg.message);
                th.val('');
            } else { 
                text.text('');
                th.val(email);
            }
        });
    }
});

// Applicant nid unique check
$(document.body).on('blur', '#applicant_nid', function(event) {
    var nid = $(this).val();
    var th = $(this);
    var text = $(this).parent('div').find('p.validation_error');
    var action_url = $(this).data('url');
    var disable = $(this).attr('readonly');

    if (nid != '' && !disable) {
        $.ajax({
            url: action_url,
            dataType: 'json',
            data: {nid: nid},
            type: 'get'
        }).done(function(res) {
            if (res.flug==0) {
                text.text(res.msg.message);
                th.val('');
            } else { 
                text.text('');
                th.val(nid);
            }
        });
    }
});

$(document.body).on('click', '.tender_apply_submit', function(event) {

    var apply_form = $(this).closest('form');
    var submit_btn = $(this);

    // To validate filesize
    $.validator.addMethod('filesize', function(value, element, param) {
        return this.optional(element) || (element.files[0].size <= param) 
    }); 

    // Check Bangladeshi mobile no
    $.validator.addMethod("mobileBD", function(value, element) {
        return this.optional(element) || value === "NA" || 
            value.match(/^(?:\+?88)?01[15-9]\d{8}$/);
    }, "অনুগ্রহ করে সঠিক নম্বর টাইপ করুন'");

    $("#application_form").validate({
        errorElement: 'span', //default input error message container
        errorClass: 'help-block', // default input error message class
        //focusInvalid: false, // do not focus the last invalid input
        //ignore: "",
        rules: {
            FIRST_NAME: {
                required: true,  minlength: 3,
            },
            LAST_NAME: {
                required: true,  minlength: 3,
            },
            SPOUSE_NAME: {
                required: true,  minlength: 3,
            },
            division: {
                required: true,
            },
            district: {
                required: true,
            },
            thana: {
                required: true,
            },
            ROAD_NO: {
                required: true,
            },
            HOLDING_NO: {
                required: true,
            },
            thana: {
                required: true,
            },
            APPLICANT_PHONE: {
                required: true, minlength: 11, maxlength: 14, mobileBD: true,
            },
            APPLICANT_EMAIL: {
                required: false, email: true,
            },
            NID: {
                required: true,
            },
            TE_APP_PASSWORD: {
                required: true, minlength: 6,
            },
            RE_PASSWORD: {
                required: true, equalTo: "#TE_APP_PASSWORD", minlength: 6,
            },
            BID_AMOUNT: {
                required: true, number: true,
            },
            BG_AMOUNT: {
                required: true,
            },
            PAYMENT_METHOD: {
                required: false,
            },
            B_DRAFT_NO: {
                required: function(element) {
                    return ($('#payment_method').val() == 1);
                }
            },
            BANK_ID: {
                required: function(element) {
                    return ($('#payment_method').val() == 1);
                }
            },
            B_DRAFT_DATE: {
                required: function(element) {
                    return ($('#payment_method').val() == 1);
                }
            },
            CASH_DATE: {
                required: function(element) {
                    return ($('#payment_method').val() == 3);
                }
            },
            ATTACHMENT: {
                required: function(element) {
                    return ($('#payment_method').val() == 1);
                },
                extension: "jpg|jpeg|png",
                filesize: 1048576,
                // messages: {
                //     required: "সংযুক্তির ধরন jpg, jpeg, png এবং 1 MB চেয়ে কম হতে হবে", 
                //     filesize: "সংযুক্তির ধরন 1 MB চেয়ে কম হতে হবে"
                // }
            },
            CASH_ATTACHMENT: {
                required: function(element) {
                    return ($('#payment_method').val() == 3);
                },
                extension: "jpg|jpeg|png",
                filesize: 1048576,
                // messages: {
                //     required: "সংযুক্তির ধরন jpg, jpeg, png এবং 1 MB চেয়ে কম হতে হবে", 
                //     filesize: "সংযুক্তির ধরন 1 MB চেয়ে কম হতে হবে"
                // }
            },
        },
        // messages: {
        //     "name": "Please type your name",
        //     "password": "Please put your password",
        // },
        errorPlacement: function (error, element) {
            if(element.attr("name") == "B_DRAFT_DATE") error.appendTo("#error_msg1")
            else error.insertAfter($(element))
        },
        highlight: function (element) { // hightlight error inputs
            $(element).closest('.form-group').addClass('form-error'); // set error class to the control group
        },
        unhighlight: function (element) { // revert the change done by hightlight
            $(element).closest('.form-group').removeClass('form-error'); // set error class to the control group
        },
        success: function (label) {
            label.closest('.form-group').removeClass('form-error'); // set success class to the control group
        },
        submitHandler: function (form, event) {  
            event.preventDefault();
            if (form) {
                //form.submit();
                submit_btn.addClass('btn_loader').removeClass('btn-success green');

                var action_url = submit_btn.data('action');
                var token      = apply_form.find('input[name=_token]').val();
                var form_value = apply_form.find('.form-value');
                var form_file  = apply_form.find('.form-file');

                var form_data = new FormData();
                form_data.append("_token", token);

                form_value.each(function(index, el) {
                    var atr_name = $(this).attr('name');
                    var atr_value = $(this).val();
                    if (atr_value != '') {
                        form_data.append(atr_name, atr_value);
                    }
                });

                form_file.each(function(index, el) {
                    var atr_name = $(this).attr('name');
                    if (this.files[0]) {
                        form_data.append(atr_name, this.files[0]);
                    }
                });

                $.ajax({
                    url: action_url,
                    dataType: 'json',
                    cache: false,
                    contentType: false,
                    processData: false,
                    async: false,
                    data: form_data,
                    type: 'post'
                }).done(function(res) {
                    submit_btn.removeClass('btn_loader').addClass('btn-success green');
                    if (res.flag==1) { // For data save response
                        $('#lg_form_message').html('<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'+res.message+'</div>');
                        $("#common_table").dataTable().fnDestroy();
                        $('#modal_big_content').modal('hide');
                        tableManaged();
                    } else { // For error response
                        submit_btn.closest('form').find('.form_messsage').html('<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'+res.message+'</div>');
                    }
                });
            }
        }
    });

});
