var FormWizard = function() {
    return {
        init: function() {
            if (jQuery().bootstrapWizard) {
                var valid = false;
                var r = $("#submit_form");
                var mf = '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';
                var ml = '</div>';

                var o = function(e, r, t) {
                        var i = r.find("li").length,
                            o = t + 1;
                        $(".step-title", $("#form_wizard_1")).text("Step " + (t + 1) + " of " + i), jQuery("li", $("#form_wizard_1")).removeClass("done");
                        for (var n = r.find("li"), s = 0; t > s; s++) jQuery(n[s]).addClass("done");
                        1 == o ? $("#form_wizard_1").find(".button-previous").hide() : $("#form_wizard_1").find(".button-previous").show(), o >= i ? ($("#form_wizard_1").find(".button-next").hide(), $("#form_wizard_1").find(".button-submit").show()) : ($("#form_wizard_1").find(".button-next").show(), $("#form_wizard_1").find(".button-submit").hide()), App.scrollTo($(".page-title"))
                    }, 
                    vp = function() {
                        var tab = $('.tab-content').find('.tab-pane.active');
                        var p_sl = $('#tab1').find('.portlet.selected').length;
                        if ($('.project_id').is(":checked")) {
                            var validStat=1;
                        } else {
                            var validStat=0;
                        }
                        if (validStat == 0) {
                            valid = false;
                            tab.closest('.tab-content').find('.form_messsage').html(mf+'দরপত্রের জন্য মহাল নির্বাচন করুন'+ml);
                        }
                        else if(p_sl == 0) {
                            valid = false;
                            tab.closest('.tab-content').find('.form_messsage').html(mf+'দরপত্রের তথ্য যোগ করুন'+ml);
                        }
                        else {
                            var tender_id = $('#pre_tender_list').val();
                            valid = true;
                            tab.closest('.tab-content').find('.form_messsage').html('');
                        }
                    },
                    vt = function () {
                        valid = false;
                        $('#tender_form').validate({
                            errorElement: 'span',
                            errorClass: 'error-help-block',
                            //focusInvalid: false, // do not focus the last invalid input
                            ignore: [],
                            errorPlacement: function (error, element) {
                                // if (element.attr("type") == "radio") {
                                //     error.insertAfter($(element).parents('div'));
                                // } else {
                                //     error.insertAfter($(element));
                                // }
                                if(element.attr("name") == "TENDER_LOCATION") error.appendTo("#error_msg1")
                                else if (element.attr("name") == "TENDER_COPY_LOCATION") error.appendTo("#error_msg2")
                                else error.insertAfter($(element))
                            },
                            submitHandler: function (form, event) {  
                                event.preventDefault();
                                if (form) {
                                    valid = true;
                                    gs();
                                }
                            }
                        });

                        $('#tender_form').find('.tender-required').each(function () {
                            $(this).rules('add', {
                                required: true,
                            });
                        });
                    }, 
                    gs = function() {
                        var sc = $('#tab1').find('.portlet.selected');
                        var dt = '<form id="schedule_form" class="form-horizontal" action="#" method="post">';
                        dt += '<div class="form-group no-margin" id="schedule_data_collection">';

                        sc.each(function(index, el) {
                            var ht = $(this).find('.caption').text();
                            var sth = sc.find('table>thead').prop('outerHTML');
                            var str = $(this).find('table>tbody>tr.selected');

                            dt += '<div class="col-md-12 col-sm-12 col-xs-12 data_single_row"><h4>'+ht.trim()+'</h4><table class="table table-striped table-bordered table-hover">';
                            dt += sth;
                            dt += '<tbody>';
                            str.each(function(index, el) {
                                dt += $(this).prop('outerHTML');
                            });
                            dt += '</tbody>';
                            dt += '</table></div>';
                        });
                        dt += '</div></form>';

                        $('#tab3').html(dt);
                    }, 
                    vs = function() {
                        valid = false;
                        $('#schedule_form').validate({
                            errorElement: 'span',
                            errorClass: 'error-help-block',
                            //focusInvalid: false, // do not focus the last invalid input
                            ignore: [],
                            submitHandler: function (form, event) {  
                                event.preventDefault();
                                if (form) {
                                    valid = true;
                                    gr();
                                    gc();
                                }
                            }
                        });

                        $('#schedule_form').find('.schedule-required').each(function () {
                            $(this).rules('add', {
                                required: true, //number: true,
                            });
                        });
                    },
                    gr = function() {
                        var loader = $('#tab1').find('img.form_loader').prop('outerHTML');
                        $('#tab4').html(loader);
                        $('#tab4').find('img.form_loader').css('display', 'block');
                        var tender_id = $('#tab1').find('input.tender_id');
                        var re_tender_id = $('#tab1').find('#pre_tender_list');
                        var category = $('table#schedul_data>tbody').find('tr').first().find('td.project_input input.project_cat').val();
                        var con = [category];

                        if ( tender_id.length > 0 ) {
                            var param = {category: con, tender_id: tender_id.val()};
                        } else if( re_tender_id.length > 0 ) {
                            var param = {category: con, tender_id: re_tender_id.val()};
                        } else {
                            var param = {category: con};
                            console.log('ni');
                        }

                        $.ajax({
                            url: cat_condition_url,
                            type: 'GET',
                            async: false,
                            dataType: 'html',
                            data: param,
                        })
                        .done(function(res) {
                            $('#tab4').html(res);
                            $(function() {
                                $('.condition_text').redactor();
                            });
                        });
                    }, 
                    gc = function() {
                        var tn      = $('#tender_form').find('#tender_no').val();
                        var tm      = $('#tender_form').find('#tender_method option:selected').text();
                        var tsl     = $('#tender_form').find('#tender_location option:selected');
                        var tcl     = $('#tender_form').find('#tender_copy_location option:selected');
                        var tlt     = $('#tender_form').find('#tender_location_type option:selected').text();
                        var tt      = $('#tender_form').find('#tender_title').val();
                        var td      = $('#tender_form').find('#tender_dt').val();
                        var tpd     = $('#tender_form').find('#tender_publish_dt').val();
                        var tid     = $('#tab1').find('input.tender_id');

                        if ( tid.length > 0 ) {
                            var tdts = $('#tender_form').find('.tender_dt_single_row');
                        } else {
                            var tdts = $('#tender_form').find('.tender_dt_single_row').last();
                        }

                        var tartl   = $('.tender_article_set').find('.tender_dt_single_row').first().find('.tender_article_no label').text();
                        var tssdl   = $('.tender_article_set').find('.tender_dt_single_row').first().find('.te_s_dt label').text();
                        var tsrdl   = $('.tender_article_set').find('.tender_dt_single_row').first().find('.date_range_bx label').text();
                        var tbrdl   = $('.tender_article_set').find('.tender_dt_single_row').first().find('.te_lst_receive_dt label').text();
                        var tbodl   = $('.tender_article_set').find('.tender_dt_single_row').first().find('.te_opn_dt label').text();

                        $('#tc_number').find('p').text(tn);
                        $('#tc_method').find('p').text(tm);
                        $('#tc_name').find('p').text(tt);
                        $('#tc_date').find('p').text(td);
                        $('#tc_publish_date').find('p').text(tpd);
                        $('#tc_location_type').find('p').text(tlt);
                        $('#tc_location').find('ul').html('');
                        $('#tc_copy_location').find('ul').html('');

                        tsl.each(function(index, el) {
                            $('#tc_location').find('ul').append('<li>'+$(this).text()+'</li>');
                        });
                        tcl.each(function(index, el) {
                            $('#tc_copy_location').find('ul').append('<li>'+$(this).text()+'</li>');
                        });

                        var cs = $('#schedule_data_collection').find('.data_single_row');
                        var odt = '';

                        odt += '<div class="col-md-12 col-sm-12 col-xs-12 tender_date_single">';
                            odt += '<table class="table table-striped table-bordered table-hover">';
                                odt += '<thead>';
                                    odt += '<tr>';
                                        odt += '<th>'+tartl+'</th>';
                                        odt += '<th>'+tssdl+'</th>';
                                        odt += '<th>'+tsrdl+'</th>';
                                        odt += '<th>'+tbrdl+'</th>';
                                        odt += '<th>'+tbodl+'</th>';
                                    odt += '</tr>';
                                odt += '</thead>';
                                odt += '<tbody>';
                                    tdts.each(function(index, el) {
                                        var ltart = $(this).find('.tender_article_no p').text();
                                        var ltssd = $(this).find('.te_s_dt .tender_seling_range').val();
                                        var ltsstf = $(this).find('.date_range_bx').find('.tender_last_seling_time_from').val();
                                        var ltsstt = $(this).find('.date_range_bx').find('.tender_last_seling_time_to').val();
                                        var ltbrd = $(this).find('.te_lst_receive_dt .tender_last_receive_dt').val();
                                        var ltbod = $(this).find('.te_opn_dt .tender_opening_dt').val();
                                        
                                        if ( ltssd !='' && ltsstf != '' && ltsstt != '' && ltbrd != '' && ltbod != '' ) {
                                            if ($(this).find('input.date_id').length > 0) {
                                                odt += '<tr data-dateid="'+$(this).find('input.date_id').val()+'">';
                                            } else {
                                                odt += '<tr>';
                                            }
                                                odt += '<td class="clause">'+ltart+'</td>';
                                                odt += '<td class="tdssl_dt">'+ltssd+'</td>';
                                                odt += '<td class="tdssl_t">'+ltsstf+' - '+ltsstt+'</td>';
                                                odt += '<td class="tdr_dt">'+ltbrd+'</td>';
                                                odt += '<td class="tdo_dt">'+ltbod+'</td>';
                                            odt += '</tr>';
                                        }

                                    });
                                odt += '</tbody>';
                            odt += '</table>';
                        odt += '</div>';

                        cs.each(function(index, el) {
                            var tblt = $(this).find('h4').text();
                            var tblh = $(this).find('table>thead').prop('outerHTML');
                            var tblr = $(this).find('table>tbody>tr');
                            odt += '<div class="col-md-12 col-sm-12 col-xs-12 data_single_row">';
                                odt += '<h4>'+tblt+'</h4>';
                                odt += '<table class="table table-striped table-bordered table-hover">';
                                    odt += tblh;
                                    odt += '<tbody>';
                                    tblr.each(function(index, el) {
                                        odt += '<tr>';
                                        if ($(this).find('input.schedule_id').length > 0 && $(this).find('input.schedule_id').val() != '') {
                                            odt += '<input type="hidden" class="schedule_id"';
                                            odt += 'value="'+$(this).find('input.schedule_id').val()+'">';
                                        }
                                            odt += '<input type="hidden" class="project_cat"';
                                            odt += 'value="'+$(this).find('td.project_input .project_cat').val()+'">';
                                            odt += '<input type="hidden" class="project_id"';
                                            odt += 'value="'+$(this).find('td.project_input .project_id').val()+'">';
                                        var tbld = $(this).find('td.data_td');
                                        tbld.each(function(index, el) {
                                            if (index == 0) {
                                                odt += '<td class="pr_ssf_no">';
                                                    odt += $(this).html();
                                                odt += '</td>';
                                            }
                                            if (index == 1) {
                                                odt += '<td class="mohal_name">';
                                                    odt += $(this).html();
                                                odt += '</td>';
                                            }
                                            if (index == 2) {
                                                odt += '<td class="pr_location">';
                                                    odt += $(this).html();
                                                odt += '</td>';
                                            }
                                            if (index >= 3) {
                                                odt += '<td class="tender_particular text-center">';
                                                    odt += '<input type="hidden" class="uom"';
                                                    odt += 'value="'+$(this).find('input.uom').val()+'">';
                                                    if ($(this).find('input.particular_id').length > 0) {
                                                        odt += '<input type="hidden" class="tp_id"';
                                                        odt += 'value="'+$(this).find('input.particular_id').val()+'">';
                                                    }
                                                    odt += '<input type="hidden" class="particular"';
                                                    odt += 'value="'+$(this).find('input.particular').val()+'">';
                                                    odt += '<p>'+$(this).find('input.te_particular').val()+'</p>';
                                                odt += '</td>';
                                            }
                                        });
                                        odt += '</tr>';
                                    });
                                    odt += '</tbody>';
                                odt += '</table>';
                            odt += '</div>';
                        });
                        $('#pro_dtls_sc_overview').html(odt);

                        var trl = $('#tender_rules_data').find('.data_single_row');
                        var str = '';
                        trl.each(function(index, el) {
                            var rlh = $(this).find('h4').text();
                            var con = $(this).find('textarea').val();
                            str += '<div class="col-md-12 col-sm-12 col-xs-12 data_single_row">';
                                str += '<h4>'+rlh+'</h4>';
                                str += con;
                            str += '</div>'
                        });
                        $('#te_rules_overview').html(str);

                    };

                $("#form_wizard_1").bootstrapWizard({
                    nextSelector: ".button-next",
                    previousSelector: ".button-previous",
                    onTabClick: function(e, r, t, i) {
                        return false;
                    },
                    onNext: function(e, a, n) {
                        //console.log(e+', '+a+', '+n);
                        if (n == 1) {
                            vp();
                           // return void o(e, a, n);
                        } else if (n == 2) {
                            vt();
                            $("#tender_form").submit();
                        }
                        else if (n == 3) {
                            vs();
                            $("#schedule_form").submit();
                        }

                        if (valid) {
                            return void o(e, a, n);
                        } else {
                            return false;
                        }
                    },
                    onPrevious: function(e, r, a) {
                        return o(e, r, a);
                    },
                    onTabShow: function(e, r, t) {
                        var i = r.find("li").length,
                            a = t + 1,
                            o = a / i * 100;
                        $("#form_wizard_1").find(".progress-bar").css({
                            width: o + "%"
                        })
                    }
                }), $("#form_wizard_1").find(".button-previous").hide(), $("#form_wizard_1 .button-submit").click(function() {

                    swal({   
                        title: "Are you sure?",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Yes, save it!",
                        cancelButtonText: "No, cancel",
                        closeOnConfirm: true, //If false then confirm message will enable
                        closeOnCancel: true
                    }, function(isConfirm){
                        if (isConfirm) {

                            var token               = $('#project_confirm').find('input[name=_token]').val();
                            var tender_no           = $('#tender_form').find('#tender_no').val();
                            var tender_method       = $('#tender_form').find('#tender_method').val();
                            var tender_title        = $('#tender_form').find('#tender_title').val();
                            var tender_desc         = $('#tender_form').find('#tender_desc').val();
                            var tender_dt           = $('#tender_form').find('#tender_dt').val();
                            var tender_publish_dt   = $('#tender_form').find('#tender_publish_dt').val();
                            var te_location         = $('#tender_form').find('#tender_location option:selected');
                            var te_cp_location      = $('#tender_form').find('#tender_copy_location option:selected');
                            var te_rules            = $('#tender_rules_data').find('.data_single_row');
                            //var is_active           = $('input[name=IS_ACTIVE]:checked', '#tender_form').val();

                            var tender_date         = $('#pro_dtls_sc_overview .tender_date_single').find('table>tbody>tr');
                            var tender_schedule     = $('#pro_dtls_sc_overview .data_single_row').find('table>tbody>tr');
                            var tender_id           = $('#tab1').find('input.tender_id');
                            var re_tender_id        = $('#tab1').find('#pre_tender_list');

                            if ( $('#form_wizard_1').hasClass('form_wizard_edit') && tender_id.length > 0 ) {
                                var data_post_url = baseUrl+'admin/lease/'+'tender-update/'+tender_id.val();
                            } else {
                                var data_post_url = tender_create_url;
                            }

                            var form_data = new FormData();
                            form_data.append("_token", token);
                            form_data.append("tender_no", tender_no);
                            form_data.append("tender_method", tender_method);
                            form_data.append("tender_title", tender_title);
                            form_data.append("tender_desc", tender_desc);
                            form_data.append("tender_dt", tender_dt);
                            form_data.append("tender_publish_dt", tender_publish_dt);
                            form_data.append("is_active", 1);

                            if ( tender_id.length > 0 ) {
                                form_data.append("tender_id", tender_id.val());
                            }
                            if ( re_tender_id.length > 0 && re_tender_id.val() != '' ) {
                                form_data.append("re_tender_id", re_tender_id.val());
                            }

                            te_location.each(function(index, el) {
                                form_data.append('tender_location['+index+'][location]', $(this).val());
                                form_data.append('tender_location['+index+'][type]', 1);
                                if ( typeof $(this).data('location') !== 'undefined' ) {
                                    form_data.append('tender_location['+index+'][location_id]', $(this).data('location'));
                                }
                            });
                            te_cp_location.each(function(index, el) {
                                form_data.append('tender_copy_location['+index+'][location]', $(this).val());
                                form_data.append('tender_copy_location['+index+'][type]', 2);
                                if ( typeof $(this).data('location') !== 'undefined' ) {
                                    form_data.append('tender_copy_location['+index+'][location_id]', $(this).data('location'));
                                }
                            });

                            if (te_rules.find('input.te_con_id').length > 0) {
                                form_data.append('rules_con_id', te_rules.find('input.te_con_id').val());
                            }
                            form_data.append('rules_category', te_rules.find('input.condition_cat').val());
                            form_data.append('rules_data', te_rules.find('textarea').val());
                            

                            tender_date.each(function(index, el) {
                                if ( typeof $(this).data('dateid') !== 'undefined' ) {
                                    form_data.append('tender_date['+index+'][date_id]', $(this).data('dateid'));
                                }
                                form_data.append('tender_date['+index+'][clause]', $(this).find('td.clause').text());
                                form_data.append('tender_date['+index+'][last_sale_date]', $(this).find('td.tdssl_dt').text());
                                form_data.append('tender_date['+index+'][last_sale_time]', $(this).find('td.tdssl_t').text());
                                form_data.append('tender_date['+index+'][receive_date]', $(this).find('td.tdr_dt').text());
                                form_data.append('tender_date['+index+'][opening_date]', $(this).find('td.tdo_dt').text());
                            });

                            tender_schedule.each(function(key, elm) {

                                if ($(this).find('input.schedule_id').length > 0 && $(this).find('input.schedule_id').val() != '') {
                                    form_data.append('tender_schedule['+key+'][schedule_id]', $(this).find('input.schedule_id').val());
                                }
                                form_data.append('tender_schedule['+key+'][pr_category]', $(this).find('input.project_cat').val());
                                form_data.append('tender_schedule['+key+'][project_id]', $(this).find('input.project_id').val());

                                var tender_particular = $(this).find('td.tender_particular');

                                tender_particular.each(function(inx, element) {
                                    if ($(this).find('input.tp_id').length > 0) {
                                        form_data.append('tender_schedule['+key+'][tender_particular]['+inx+'][tp_id]', $(this).find('input.tp_id').val());
                                    }
                                    form_data.append('tender_schedule['+key+'][tender_particular]['+inx+'][particular_id]', $(this).find('input.particular').val());
                                    form_data.append('tender_schedule['+key+'][tender_particular]['+inx+'][particular_amt]', $(this).find('p').text());
                                    form_data.append('tender_schedule['+key+'][tender_particular]['+inx+'][uom]', $(this).find('input.uom').val());
                                });

                            });
                            
                            // Post tender form data
                            $.ajax({
                                url: data_post_url,
                                dataType: 'json',
                                cache: false,
                                contentType: false,
                                processData: false,
                                async: false,
                                data: form_data,
                                type: 'post'
                            })
                            .done(function(res) {
                                if (res) {
                                    if (res.flug==1) { // For data save response
                                        //swal("Saved", "Successfully saved", "success");
                                        $('.form-wizard').find('ul.nav-pills li:last-child').removeClass('active').addClass('done');
                                        $('.tab-content').find('.form_messsage').html('<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'+res.message+'</div>');
                                        $("#common_table").dataTable().fnDestroy();
                                        //$('#tender_add_content').modal('hide');
                                        tableManaged();
                                    } else { // For error response
                                        $('.tab-content').find('.form_messsage').html('<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'+res.message+'</div>');
                                    }
                                } else { // For problem with getting response
                                    $('.tab-content').find('.form_messsage').html('<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>অনুগ্রহ করে একটু পরে আবার চেষ্টা করুন</div>');
                                }
                            });
                        }
                    });

                    $('html, body').animate({
                        scrollTop: $("#form_wizard_1").offset().top
                    }, 500);

                }).hide();
            }
        }
    }
}();

$('#tender_add_content').on('show.bs.modal', function (e) {

    var modal_body = $(this).find('.modal-content');
    modal_body.find('#body-content').html('');
    modal_body.find('img.loader').show();

});

$('#tender_add_content').on('shown.bs.modal', function (e) {

    var modal_body  = $(this).find('.modal-content');
    var button      = $(e.relatedTarget);
    var action      = button.data('form');

    $.ajax({
        url: action,
        type: 'GET',
        dataType: 'html',
        data: false,
    })
    .done(function(res) {
        modal_body.find('img.loader').hide();
        modal_body.find('#body-content').html(res);
        var mt_title = modal_body.find('#body-content').find('.modal_top_title').text();

        if ( typeof mt_title != 'undefined' && mt_title != '' ) {
            modal_body.find('.modal-title').text(mt_title);
        }

        // Form wizard
        FormWizard.init();
        // Redactor js editor
        $(function() {
            $('.reactor_basic').redactor();
        });
        // Select2 multiselect
        $(".multi_select").select2();
    });
});

$(document.body).on('change', '.check_all', function(event) {
    $(this).closest('table').find('tbody').find('input:checkbox').prop('checked', this.checked);
    if($(this).closest('table').find('tbody').find('input:checkbox').prop('checked') == true){
        $(this).closest('.portlet').addClass('selected');
        $(this).closest('table').find('tbody tr').addClass('selected');
    } else {
        $(this).closest('.portlet').removeClass('selected');
        $(this).closest('table').find('tbody tr').removeClass('selected');
    }
});

$(document.body).on('change', '.table-striped>tbody>tr input:checkbox', function(event) {
    var chk = $(this);
    if ($(this).hasClass('remove_schedule') && $(this).prop('checked') == false) {
        var url = $(this).data('url');
        swal({   
            title: "Are you sure?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            cancelButtonText: "No, cancel",
            closeOnConfirm: false, //If false then confirm message will enable
            closeOnCancel: true
        }, function(isConfirm){
            if (isConfirm) {
                $.ajax({
                    url: url,
                    type: 'GET',
                    dataType: 'html',
                    data: false,
                })
                .done(function(res) {
                    makeSelection(chk);
                    swal("Deleted", res, "success");
                });
            } else {
                chk.prop('checked', true);
            }
        });
    } else {
        makeSelection(chk);
    }
});

$(document.body).on('click', '.date_add_more', function(event) {
    var te_article_set = $(this).closest('.tender_article_set');
    var date_pk_prob = te_article_set.find('.tender_dt_single_row').last().find('.te_s_dt .tender_seling_range');
    
    if (date_pk_prob.hasClass('hasDatepicker')) {
        date_pk_prob.removeClass('hasDatepicker').removeAttr('id');
    }

    var articles = $(this).closest('.tender_dt_single_row');
    var in_num = te_article_set.find('.tender_dt_single_row').last().find('.tender_article_no p').text();
    var article_number = Number(in_num)+1;
    var article_txt = articles.find('.tender_article_no label').text();
    var te_s_dt = articles.find('.te_s_dt').html();
    var te_dt_range = articles.find('.date_range_bx').html();
    var te_lst_rdt = articles.find('.te_lst_receive_dt').html();
    var te_opn_dt = articles.find('.te_opn_dt').html();

    te_article_set.append('<div class="row no-margin section_box_gray tender_dt_single_row"><div class="row no-margin"><div class="col-sm-1 text-center tender_article_no"><label for="">'+article_txt+'</label><p>'+article_number+'</p></div><div class="col-sm-4 te_s_dt">'+te_s_dt+'</div><div class="col-sm-5 text-center date_range_bx">'+te_dt_range+'</div><div class="col-sm-2"><span class="remove_clause" title="Remove"><i class="fa fa-times"></i></span></div></div><div class="row no-margin"><div class="col-sm-4 col-sm-offset-1 te_lst_receive_dt">'+te_lst_rdt+'</div><div class="col-sm-5 te_opn_dt">'+te_opn_dt+'</div></div></div>');
});

$(document.body).on('click', 'span.delete_clause', function(event) {
    var parent_row = $(this).closest('.tender_dt_single_row');
    var url = $(this).data('url');
    swal({   
        title: "Are you sure?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, delete it!",
        cancelButtonText: "No, cancel",
        closeOnConfirm: false, //If false then confirm message will enable
        closeOnCancel: true
    }, function(isConfirm){
        if (isConfirm) {
            $.ajax({
                url: url,
                type: 'GET',
                dataType: 'html',
                data: false,
            })
            .done(function(res) {
                parent_row.remove();
                swal("Deleted", res, "success");
            });
        }
    });
});

$(document.body).on('click', '.remove_clause', function(event) {
    $(this).closest('.tender_dt_single_row').remove();
});

//==============================
$(document.body).on('change', '#is_it_retender', function(event) {
    var retender = $(this).val();
    if ( $(this).is(':checked') ) {
        $("#tender_list").removeClass("hidden");
        $("#pre_tender_list").addClass("tender-required");
    } else {
        $("#tender_list").addClass("hidden");
        $("#pre_tender_list").removeClass("tender-required");
        $("#pre_tender_list").val('');
        $('#tab1').find('img.form_preloader').show();

        $.ajax({
            url: tender_schedule_data,
            type: 'GET',
            dataType: 'json',
            data: false,
        })
        .done(function(response) {
            $('#tab1').find('img.form_preloader').hide();
            $('table#schedul_data>tbody').html(response.project);
            $('#tab2').html(response.tender);

            // Reactor text editor
            $(function() {
                $('.reactor_basic').redactor();
            });
            // Select2 multiselect
            $(".multi_select").select2();
        });
    }
});

$(document.body).on('change', '#pre_tender_list', function() {
    var tender_id = $(this).val();
    if (tender_id != '') {
        $('#tab1').find('img.form_preloader').show();

        $.ajax({
            url: tender_schedule_data,
            type: 'GET',
            dataType: 'json',
            data: {tender_id: tender_id},
        })
        .done(function(response) {
            $('#tab1').find('img.form_preloader').hide();
            $('table#schedul_data>tbody').html(response.project);
            $('#tab2').html(response.tender);

            // Reactor text editor
            $(function() {
                $('.reactor_basic').redactor();
            });
            // Select2 multiselect
            $(".multi_select").select2();
        });
    }
});

// Make selected by checkbox
function makeSelection(chk) {
    if (chk.prop('checked') == true) {
        chk.closest('tr').addClass('selected');
    } else {
        chk.closest('tr').removeClass('selected');
    }

    if (chk.closest('tbody').find('tr.selected').length == 0) {
        chk.closest('.portlet').removeClass('selected');
    } else {
        chk.closest('.portlet').addClass('selected');
    }
}

// Lease primary selection
var leasePrimarySelect = function(){
    return {
        init: function(){
            
            $(document.body).on('blur', '#ui-datepicker-div', function(event) {
               var from_date = englishToBangla($('#from_date_id').val());
               var to_date = englishToBangla($('#to_date_id').val());
               $('#text_area_content').find('span.start_date').text(from_date);
               $('#text_area_content').find('span.end_date').text(to_date);
            });
            $(document.body).on('click', 'a.tab_next', function(event) {
                var is_valid = 0;
                $('.validDate').each(function () {
                    if ($(this).val() == "") {
                        //alert("Date Is Empty");
                        $(this).css("border", "1px solid red");
                        is_valid = 1;
                    } else {
                        $(this).css("border", "1px solid #ccc");
                    }
                });
                event.preventDefault();
                var letter_content = $('#text_area_content').html();
                $('#letter_body_id').redactor('code.set', letter_content);
                if(is_valid == 0){
                    $('.tab-content a[href="#tab_2"]').tab('show');
                }
            });
            $(document.body).on('click', 'a.tab_previous', function(event) {
                event.preventDefault();
                $('.tab-content a[href="#tab_1"]').tab('show');
            });
            // Lease Validity Save
            $(document.body).on('click', '.lease_validity_save', function(event) {
                var submit_btn  = $(this);
                var data_post_url = $(this).data('action');
                var data_print_url = $(this).data('print');
                $('#lease_validity').validate({
                    errorElement: 'span',
                    errorClass: 'error-help-block',
                    //focusInvalid: false, // do not focus the last invalid input
                    ignore: [],
                    submitHandler: function (form, event) {  
                        event.preventDefault();
                        if (form) {
                            submit_btn.addClass('btn_loader').removeClass('btn-success green');
                            //var data_each = $('table#installment>tbody>tr');
                            var token = $('#lease_validity').find('input[name=_token]').val();
                            var applicant = $('#lease_validity').find('input[name=applicant_id]').val();
                            var schedule = $('#lease_validity').find('input[name=schedule_id]').val();
                            var email = $('#lease_validity').find('input[name=applicant_email]').val();
                            var from_date = $('#lease_validity').find('input[name=DATE_FROM]').val();
                            var to_date = $('#lease_validity').find('input[name=DATE_TO]').val();
                            var remarks = $('#lease_validity').find('textarea[name=INITIAL_REMARKS]').val();
                            
                            var sharok_no = $('#lease_validity').find('input[name=sharok_no]').val();
                            var sharok_date = $('#lease_validity').find('input[name=sharok_date]').val();
                            var letter_subject = $('#lease_validity').find('input[name=letter_subject]').val();
                            var letter_sources = $('#lease_validity').find('input[name=letter_sources]').val();
                            var letter_body = $('#lease_validity').find('textarea[name=letter_body]').val();
                            var applicant_info = $('#lease_validity').find('textarea[name=applicant_info]').val();
                            var officer_info = $('#lease_validity').find('textarea[name=officer_info]').val();
                            var distribution = $('#lease_validity').find('textarea[name=distribution]').val();
                                
                            var form_data = new FormData();
                            form_data.append("_token", token);
                            form_data.append("applicant", applicant);
                            form_data.append("schedule", schedule);
                            form_data.append("email", email);
                            form_data.append("from_date", from_date);
                            form_data.append("to_date", to_date);
                            form_data.append("remarks", remarks);
                                
                            form_data.append("sharok_no", sharok_no);
                            form_data.append("sharok_date", sharok_date);
                            form_data.append("letter_subject", letter_subject);
                            form_data.append("letter_sources", letter_sources);
                            form_data.append("letter_body", letter_body);
                            form_data.append("applicant_info", applicant_info);
                            form_data.append("officer_info", officer_info);
                            form_data.append("distribution", distribution);

                            $.ajax({
                                url: data_post_url,
                                dataType: 'json',
                                cache: false,
                                contentType: false,
                                processData: false,
                                async: false,
                                data: form_data,
                                type: 'post'
                            })
                            .done(function(res) {
                                $(".form_messsage").html('<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'+res.message+'</div>');
                                //$(".lease_validity_save").closest('form')[0].reset();
                                $("#common_table").dataTable().fnDestroy();
                                listDataTable();
                                submit_btn.removeClass('btn_loader').addClass('btn-success green');
                                window.open(data_print_url, '_blank');
                            });
                        }
                    }
                });

                $('.form-required').each(function () {
                    $(this).rules('add', {
                        required: true,
                    });
                });
            });

        }
    };
}();

// Lease final applicant list
var leaseFinalList = function(){
    return {
        init: function(){
            
            $(document.body).on('click', '.finally_save', function(event) {
                var submit_btn  = $(this);
                //submit_btn.addClass('btn_loader').removeClass('btn-success');
                var data_post_url = $(this).data('action');
                var data_print_url = $(this).data('print');
                $('#final_letter').validate({
                    errorElement: 'span',
                    errorClass: 'error-help-block',
                    //focusInvalid: false, // do not focus the last invalid input
                    ignore: [],
                    submitHandler: function (form, event) {  
                        event.preventDefault();
                        if (form) {
                            submit_btn.addClass('btn_loader').removeClass('btn-success green');

                            var token = $('#final_letter').find('input[name=_token]').val();
                            var applicant = $('#final_letter').find('input[name=applicant_id]').val();
                            var schedule = $('#final_letter').find('input[name=schedule_id]').val();
                            var email = $('#final_letter').find('input[name=applicant_email]').val();
                            
                            var sharok_no = $('#final_letter').find('input[name=sharok_no]').val();
                            var sharok_date = $('#final_letter').find('input[name=sharok_date]').val();
                            var letter_subject = $('#final_letter').find('input[name=letter_subject]').val();
                            var letter_body = $('#final_letter').find('textarea[name=letter_body]').val();
                            var letter_conditions = $('#final_letter').find('textarea[name=letter_conditions]').val();
                            var applicant_info = $('#final_letter').find('textarea[name=applicant_info]').val();
                            var officer_info = $('#final_letter').find('textarea[name=officer_info]').val();
                            var distribution = $('#final_letter').find('textarea[name=distribution]').val();
                                
                            var form_data = new FormData();
                            form_data.append("_token", token);
                            form_data.append("applicant", applicant);
                            form_data.append("schedule", schedule);
                            form_data.append("email", email);
                            
                            form_data.append("sharok_no", sharok_no);
                            form_data.append("sharok_date", sharok_date);
                            form_data.append("letter_subject", letter_subject);
                            form_data.append("letter_body", letter_body);
                            form_data.append("letter_conditions", letter_conditions);
                            form_data.append("applicant_info", applicant_info);
                            form_data.append("officer_info", officer_info);
                            form_data.append("distribution", distribution);

                            $.ajax({
                                url: data_post_url,
                                dataType: 'json',
                                cache: false,
                                contentType: false,
                                processData: false,
                                async: false,
                                data: form_data,
                                type: 'post'
                            })
                            .done(function(res) {
                                $(".form_messsage").html('<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'+res.message+'</div>');
                                //$(".finally_save").closest('form')[0].reset();
                                $("#common_table").dataTable().fnDestroy();
                                listDataTable();
                                submit_btn.removeClass('btn_loader').addClass('btn-success green');
                                if (res.flug == 1) {
                                    window.open(data_print_url, '_blank');
                                }
                            });
                        }
                    }
                });

                $('.form-required').each(function () {
                    $(this).rules('add', {
                        required: true,
                    });
                });
            });
            
        }
    };
}();