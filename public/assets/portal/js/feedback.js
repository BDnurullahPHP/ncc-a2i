// Feedback form validation
var Feedback = function () {
			
   var onlineFeedback = function() {

        $("#feedback_form").validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block', // default input error message class
            //focusInvalid: false, // do not focus the last invalid input
            //ignore: "",
            rules: {
                'query': {
                    required: true,
                },
                'name': {
                    required: true, minlength: 3,
                },
                mobile: {
                    required: true, minlength: 11, maxlength: 14,
                },
                email: {
                    required: true, email: true,
                },
                address: {
                    required: true, minlength: 3,
                },
                message: {
                    required: true, minlength: 3,
                },
                'captcha': {
                    required: true,
                }
            },
            errorPlacement: function (error, element) {
                if(element.attr("name") == "date") error.appendTo("#error_msg1")
                else error.insertAfter($(element))
            },
            highlight: function (element) { // hightlight error inputs
                $(element).closest('.form-group').addClass('form-error'); // set error class to the control group
            },

            unhighlight: function (element) { // revert the change done by hightlight
                $(element).closest('.form-group').removeClass('form-error'); // set error class to the control group
            },

            success: function (label) {
                label.closest('.form-group').removeClass('form-error'); // set success class to the control group
            },
            submitHandler: function (form, event) {  
                event.preventDefault();
                if (form) {
                    form.submit();
                }
            }
        });

    }

    var basicOperations = function() {
        $("#date").datepicker({
            dateFormat: "dd/mm/yy",
            changeMonth: true,
            changeYear: true,
            yearRange: "c-40:c+10"
        });

        $("#date").inputmask("dd-mm-yyyy", {"placeholder": "dd-mm-yyyy"});

        $("#reset_form").click(function(event) {
            $("#reg_form")[0].reset();
            $("input:checkbox, input:radio").prop('checked', false);
            $("input:checkbox, input:radio").parent('div').removeClass('checked');
        });


    }

    return {
        //main function to initiate the module
        init: function () {
            onlineFeedback();
            basicOperations();
        }

    };

}();

var Contact = function () {

    return {
        init: function () {

            $("#contact_form").validate({
                errorElement: 'span', //default input error message container
                errorClass: 'help-block', // default input error message class
                //focusInvalid: false, // do not focus the last invalid input
                //ignore: "",
                rules: {
                    name: {
                        required: true, minlength: 3,
                    },
                    mobile: {
                        required: true, minlength: 11, maxlength: 14,
                    },
                    email: {
                        required: true, email: true,
                    },
                    message: {
                        required: true, minlength: 3,
                    },
                    'captcha': {
                        required: true,
                    }
                },
                errorPlacement: function (error, element) {
                    if(element.attr("name") == "captcha") error.appendTo("#error_msg1")
                    else error.insertAfter($(element))
                },
                highlight: function (element) { // hightlight error inputs
                    $(element).closest('.form-group').addClass('form-error'); // set error class to the control group
                },

                unhighlight: function (element) { // revert the change done by hightlight
                    $(element).closest('.form-group').removeClass('form-error'); // set error class to the control group
                },

                success: function (label) {
                    label.closest('.form-group').removeClass('form-error'); // set success class to the control group
                },
                submitHandler: function (form, event) {  
                    event.preventDefault();
                    if (form) {
                        form.submit();
                    }
                }
            });

        }
    };

}();