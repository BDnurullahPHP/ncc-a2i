
// Register form validation
var TenderApplicant = function () {

    var basicOperations = function() {

        $(document.body).on('change', '#division', function(event) {
            getDistrictList($(this).val());
        });

        // var division_val = $('#division').val();
        // if (division_val != '') {
        //     getDistrictList(division_val);
        // }

        $(document.body).on('change', '#district', function(event) {
            getThanaList($(this).val());
        });

        // var district_val = $('#division').val();
        // if (district_val != '') {
        //     getThanaList(district_val);
        // }

        $(document.body).on('change', '#thana', function(event) {
            var thana = $(this).val();
            if (thana != '') {
                $.ajax({
                    url: baseUrl+'get-thana',
                    type: 'GET',
                    dataType: 'html',
                    data: {thana: thana},
                })
                .done(function(res) {
                    $('#THANA_ENAME').val(res);
                });
            }
        });

        function getDistrictList(val){
            var division = val;
            if (division != '') {
                $.ajax({
                    url: baseUrl+'get-district',
                    type: 'GET',
                    dataType: 'html',
                    data: {division: division},
                })
                .done(function(res) {
                    $('#district').html(res);
                });
            }
        }

        function getThanaList(val){
            var district = val;
            if (district != '') {
                $.ajax({
                    url: baseUrl+'get-thana',
                    type: 'GET',
                    dataType: 'html',
                    data: {district: district},
                })
                .done(function(res) {
                    $('#thana').html(res);
                });
            }
        }

        // To calculate guarantee_percent of tender applicant amount
        $('#applicant_bid_amount').blur(function(event) {
            var bid_amount = parseInt( $(this).val() );
            var guarantee = parseInt( $(this).parent('div').find('input.guarantee_percent').val() );
            $('#applicant_guarantee').val( Math.round( (bid_amount*guarantee)/100 ) );
        });

        // Applicant phone unique check
        $(document.body).on('blur', '#applicant_phone_no', function(event) {
            var phone = $(this).val();
            var th = $(this);
            var text = $(this).parent('div').find('p.validation_error');
            var action_url = $(this).data('url');
            var disable = $(this).attr('readonly');

            if (phone != '' && !disable) {
                $.ajax({
                    url: action_url,
                    dataType: 'json',
                    data: {phone: phone},
                    type: 'get'
                }).done(function(res) {
                    if (res.flug==0) {
                        text.text(res.msg.message);
                        th.val('');
                        redirectToLogin(res);
                    } else { 
                        text.text('');
                        th.val(phone);
                    }
                });
            }
        });

        // Applicant email unique check
        $(document.body).on('blur', '#applicant_email_id', function(event) {
            var email = $(this).val();
            var th = $(this);
            var text = $(this).parent('div').find('p.validation_error');
            var action_url = $(this).data('url');
            var disable = $(this).attr('readonly');

            if (email != '' && !disable) {
                $.ajax({
                    url: action_url,
                    dataType: 'json',
                    data: {email: email},
                    type: 'get'
                }).done(function(res) {
                    if (res.flug==0) {
                        text.text(res.msg.message);
                        th.val('');
                        redirectToLogin(res);
                    } else { 
                        text.text('');
                        th.val(email);
                    }
                });
            }
        });

        // Applicant nid unique check
        $(document.body).on('blur', '#applicant_nid', function(event) {
            var nid = $(this).val();
            var th = $(this);
            var text = $(this).parent('div').find('p.validation_error');
            var action_url = $(this).data('url');
            var disable = $(this).attr('readonly');

            if (nid != '' && !disable) {
                $.ajax({
                    url: action_url,
                    dataType: 'json',
                    data: {nid: nid},
                    type: 'get'
                }).done(function(res) {
                    if (res.flug==0) {
                        text.text(res.msg.message);
                        th.val('');
                        redirectToLogin(res);
                    } else { 
                        text.text('');
                        th.val(nid);
                    }
                });
            }
        });

        // Redirect to login page if yes
        function redirectToLogin(res) {
            swal({   
                title: res.msg.message,
                text: res.msg.login_msg,
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: res.msg.yes,
                cancelButtonText: res.msg.no,
                closeOnConfirm: false, //If false then confirm message will enable
                closeOnCancel: true
            }, function(isConfirm){
                if (isConfirm) {
                    window.location.href = res.url;
                }
            });
        }

        $("#B_DRAFT_DATE").datepicker({
            dateFormat: "dd/mm/yy",
            changeMonth: true,
            changeYear: true,
            yearRange: "c-100:c+0"
        });

        $("#B_DRAFT_DATE").inputmask("dd-mm-yyyy", {"placeholder": "dd-mm-yyyy"});
    }

    var ajaxTableManaged = function() {
        $('table.display').each(function(index) {
            if ($('table.display').length > 0) {
                var source_data = $(this).data('source');
                // begin second table
                oTable2 = $(this).dataTable({
                    "processing": true,
                    "serverSide": true,
                    "searching": false,
                    "searchable": false,
                    "pagingType": "full_numbers",
                    'pageLength': 10,
                    "aLengthMenu": [
                        [10, 15, 20],
                        [10, 15, 20] // change per page values here
                    ],
                    // Load data for the table's content from an Ajax source
                    "ajax": {
                        //"type": "GET",
                        "url": source_data,
                    },
                    //Set column definition initialisation properties.
                    "columnDefs": [
                        {"targets": [0], "orderable": false,},
                        {"targets": [-1], "orderable": false,},
                        {"targets": [-2], "orderable": false,}
                    ]
                });
            }
        });
    }
            
    var onlineTenderApplication = function() {

        // To validate filesize
        $.validator.addMethod('filesize', function(value, element, param) {
            return this.optional(element) || (element.files[0].size <= param) 
        }); 

        // Check Bangladeshi mobile no
        $.validator.addMethod("mobileBD", function(value, element) {
            return this.optional(element) || value === "NA" || 
                value.match(/^(?:\+?88)?01[15-9]\d{8}$/);
        }, "অনুগ্রহ করে সঠিক নম্বর টাইপ করুন'");

        $("#tenderapplicant_form").validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block', // default input error message class
            //focusInvalid: false, // do not focus the last invalid input
            //ignore: "",
            rules: {
                FIRST_NAME: {
                    required: true,  minlength: 3,
                },
                LAST_NAME: {
                    required: true,  minlength: 3,
                },
                SPOUSE_NAME: {
                    required: true,  minlength: 3,
                },
                division: {
                    required: true,
                },
                district: {
                    required: true,
                },
                thana: {
                    required: true,
                },
                ROAD_NO: {
                    required: true,
                },
                HOLDING_NO: {
                    required: true,
                },
                thana: {
                    required: true,
                },
                APPLICANT_PHONE: {
                    required: true, minlength: 11, maxlength: 14, mobileBD: true,
                },
                APPLICANT_EMAIL: {
                    required: false, email: true,
                },
                NID: {
                    required: true,
                },
                TE_APP_PASSWORD: {
                    required: true, minlength: 6,
                },
                RE_PASSWORD: {
                    required: true, equalTo: "#TE_APP_PASSWORD", minlength: 6,
                },
                BID_AMOUNT: {
                    required: true, number: true,
                },
                BID_AMOUNT_TEXT: {
                    required: true,
                },
                BG_AMOUNT: {
                    required: true,
                },
                BG_AMOUNT_TEXT: {
                    required: true,
                },
                PAYMENT_METHOD: {
                    required: false,
                },
                B_DRAFT_NO: {
                    required: function(element) {
                        return ($('#payment_method').val() == 1);
                    }
                },
                BANK_ID: {
                    required: function(element) {
                        return ($('#payment_method').val() == 1);
                    }
                },
                B_DRAFT_DATE: {
                    required: function(element) {
                        return ($('#payment_method').val() == 1);
                    }
                },
                CASH_DATE: {
                    required: function(element) {
                        return ($('#payment_method').val() == 3);
                    }
                },
                ATTACHMENT: {
                    required: function(element) {
                        return ($('#payment_method').val() == 1);
                    },
                    extension: "jpg|jpeg|png",
                    filesize: 1048576,
                    messages: {
                        required: "সংযুক্তির ধরন jpg, jpeg, png এবং 1 MB চেয়ে কম হতে হবে", 
                        filesize: "সংযুক্তির ধরন 1 MB চেয়ে কম হতে হবে"
                    }
                },
                CASH_ATTACHMENT: {
                    required: function(element) {
                        return ($('#payment_method').val() == 3);
                    },
                    extension: "jpg|jpeg|png",
                    filesize: 1048576,
                    messages: {
                        required: "সংযুক্তির ধরন jpg, jpeg, png এবং 1 MB চেয়ে কম হতে হবে", 
                        filesize: "সংযুক্তির ধরন 1 MB চেয়ে কম হতে হবে"
                    }
                },
            },
            // messages: {
            //     "name": "Please type your name",
            //     "password": "Please put your password",
            // },
            errorPlacement: function (error, element) {
                if(element.attr("name") == "B_DRAFT_DATE") error.appendTo("#error_msg1")
                else error.insertAfter($(element))
            },
            highlight: function (element) { // hightlight error inputs
                $(element).closest('.form-group').addClass('form-error'); // set error class to the control group
            },
            unhighlight: function (element) { // revert the change done by hightlight
                $(element).closest('.form-group').removeClass('form-error'); // set error class to the control group
            },
            success: function (label) {
                label.closest('.form-group').removeClass('form-error'); // set success class to the control group
            },
            submitHandler: function (form, event) {  
                event.preventDefault();
                if (form) {
                    form.submit();
                }
            }
        });

    }

    var icheckCheckbox = function() {
        $('.iech').iCheck({
            checkboxClass: 'icheckbox_square-green',
            radioClass: 'iradio_square-green',
            increaseArea: '20%' // optional
        });
    }

    return {
        //main function to initiate the module
        init: function () {
            onlineTenderApplication();
            ajaxTableManaged();
            icheckCheckbox();
            basicOperations();
        }

    };

}();