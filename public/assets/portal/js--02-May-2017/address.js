$(document.body).on('change', '#division', function(event) {
    getDistrictList($(this).val());
});

// var division_val = $('#division').val();
// if (division_val != '') {
//     getDistrictList(division_val);
// }

$(document.body).on('change', '#district', function(event) {
    getThanaList($(this).val());
});

// var district_val = $('#division').val();
// if (district_val != '') {
//     getThanaList(district_val);
// }

$(document.body).on('change', '#thana', function(event) {
    var thana = $(this).val();
    if (thana != '') {
        $.ajax({
            url: baseUrl+'get-thana',
            type: 'GET',
            dataType: 'html',
            data: {thana: thana},
        })
        .done(function(res) {
            $('#THANA_ENAME').val(res);
        });
    }
});

function getDistrictList(val){
    var division = val;
    if (division != '') {
        $.ajax({
            url: baseUrl+'get-district',
            type: 'GET',
            dataType: 'html',
            data: {division: division},
        })
        .done(function(res) {
            $('#district').html(res);
        });
    }
}

function getThanaList(val){
    var district = val;
    if (district != '') {
        $.ajax({
            url: baseUrl+'get-thana',
            type: 'GET',
            dataType: 'html',
            data: {district: district},
        })
        .done(function(res) {
            $('#thana').html(res);
        });
    }
}