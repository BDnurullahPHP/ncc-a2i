var citizen = function(){

	var payment = function(){

		//$('.multi_select').select2();

		$('.datepicker').datepicker({
            changeMonth: true,
	        changeYear: true,
	        showButtonPanel: true,
	        dateFormat: 'MM yy',
	        onClose: function(dateText, inst) { 
	            $(this).datepicker('setDate', new Date(inst.selectedYear, inst.selectedMonth, 1));
	        }
        });

		$('#tender_schedule').change(function(event) {
			var schedule_id = $(this).val();
			if (schedule_id != '') {
				$.ajax({
					url: pendingMonthUrl,
					type: 'GET',
					dataType: 'html',
					data: {schedule_id: schedule_id},
				})
				.done(function(res) {
					$('#rent_pay_month').html(res);
				});
			} else {
				$('#rent_payment_content').html('');
			}
		});

		$('#rent_pay_month').change(function(event) {
			var vchild = $(this).val();
			if (vchild != '') {
				var schedule_id = $('#tender_schedule').val();
				$('.payment_cont').find('img.pre_loader').css('display', 'block');
				$('#rent_payment_content').html('');
				$.ajax({
					url: rentPaymentForm,
					type: 'GET',
					dataType: 'html',
					data: {schedule_id: schedule_id, vchild: vchild},
				})
				.done(function(res) {
					//console.log(res);
					$('.payment_cont').find('img.pre_loader').hide();
					$('#rent_payment_content').html(res);
				});
			} else {
				$('#rent_payment_content').html('');
			}
		});

		$(document.body).on('blur', 'input.payAmount', function(event) {
			var val = $(this).val();
			if (isNaN(parseInt(val))) {
				val = 500;
				$(this).val(500);
			} else if (val < 500) {
				val = 500;
				$(this).val(500);
			}
			var total = parseInt($('input.totalAmount').val());
			var pay = parseInt(val);
			var due = total-pay;
			$('input.dueAmount').val(due);
		});

		$(document.body).on('click', '.payment_confirmation', function(event) {
            event.preventDefault();
            var form = $(this).closest('form');
            // var remk = form.find('.user_remarks').val();
            // var parm = form.find('input.getw_param');

            swal({   
                title: "Are you sure?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#008000",
                confirmButtonText: "Yes, Pay it!",
                cancelButtonText: "No, cancel",
                closeOnConfirm: false, //If false then confirm message will enable
                closeOnCancel: true
            }, function(isConfirm){
                if (isConfirm) {
                    form.submit();
                }
            });
        });

	}

	var paymentData = function(){

		$('.datepicker').datepicker({
            changeMonth: true,
	        changeYear: true,
	        showButtonPanel: true,
	        dateFormat: 'MM yy',
	        onClose: function(dateText, inst) { 
	            $(this).datepicker('setDate', new Date(inst.selectedYear, inst.selectedMonth, 1));
	        }
        });

        $(document.body).on('click', 'button.search_data', function(event) {
        	event.preventDefault();
        	
        	$(".data_table_wrapper").css('display', 'block');
        	var form_data = $(this).closest('form');
        	var schedule = form_data.find('.schedule').val();
        	var year_range = form_data.find('.year_range').val();

        	$("table.search_data_table").dataTable().fnDestroy();
        
	        // begin data table
	        oTable2 = $('table.search_data_table').dataTable({
	            "processing": true,
	            "serverSide": true,
	            "searching": false,
	            "searchable": false,
	            "pagingType": "full_numbers",
	            'pageLength': 10,
	            "aLengthMenu": [
	                [10, 15, 20],
	                [10, 15, 20] // change per page values here
	            ],
	            // Load data for the table's content from an Ajax source
	            "ajax": {
	                //"type": "GET",
	                "url": source_data,
	                "data": function (d){
	                            d.schedule = schedule;
	                            d.year = year_range;
	                        }
	            },
	            //Set column definition initialisation properties.
	            "columnDefs": [
	                {"targets": [0],"orderable": false,},
	                {"targets": [ -1 ], "orderable": false,},
	                {"targets": [ -2 ], "orderable": false,}
	            ]
	        });

        });

	}

	return {
		//main function to initiate the module
        init: function () {
            payment();
            paymentData();
        }
	};

}();