-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jun 05, 2016 at 11:00 AM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `ncc`
--

-- --------------------------------------------------------

--
-- Table structure for table `ati_modules`
--

CREATE TABLE IF NOT EXISTS `ati_modules` (
  `MODULE_ID` int(11) NOT NULL,
  `MODULE_NAME` varchar(70) NOT NULL,
  `MODULE_NAME_BN` varchar(500) DEFAULT NULL,
  `CATEGORY` varchar(60) NOT NULL,
  `SL_NO` tinyint(2) NOT NULL,
  `MODULE_ICON` varchar(50) DEFAULT NULL,
  `ACTIVE_STATUS` tinyint(1) DEFAULT '1',
  `ENTERED_BY` int(10) DEFAULT NULL,
  `ENTRY_TIMESTAMP` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `UPDATE_BY` int(10) DEFAULT NULL,
  `UPDATED_TIMESTAMP` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ati_modules`
--

INSERT INTO `ati_modules` (`MODULE_ID`, `MODULE_NAME`, `MODULE_NAME_BN`, `CATEGORY`, `SL_NO`, `MODULE_ICON`, `ACTIVE_STATUS`, `ENTERED_BY`, `ENTRY_TIMESTAMP`, `UPDATE_BY`, `UPDATED_TIMESTAMP`) VALUES
(1, 'All Setup', 'সেটআপ সমূহ', '', 1, 'icon-wrench', 1, 1, '2016-04-20 11:49:04', 1, '2016-04-23 11:40:58');

-- --------------------------------------------------------

--
-- Table structure for table `ati_module_links`
--

CREATE TABLE IF NOT EXISTS `ati_module_links` (
  `LINK_ID` int(11) NOT NULL,
  `LINK_NAME` varchar(60) NOT NULL,
  `LINK_NAME_BN` varchar(500) DEFAULT NULL,
  `ATI_MLINK_PAGES` varchar(10) DEFAULT NULL,
  `MODULE_ID` int(2) DEFAULT NULL,
  `URL_URI` varchar(200) DEFAULT NULL,
  `LINK_DESC` varchar(100) DEFAULT NULL,
  `SL_NO` tinyint(2) NOT NULL,
  `CREATE` tinyint(1) DEFAULT '0',
  `READ` tinyint(1) DEFAULT '0',
  `UPDATE` tinyint(1) DEFAULT '0',
  `DELETE` tinyint(1) DEFAULT '0',
  `STATUS` tinyint(4) DEFAULT NULL,
  `ACTIVE_STATUS` tinyint(1) DEFAULT '1',
  `ENTERED_BY` int(10) DEFAULT NULL,
  `ENTRY_TIMESTAMP` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `UPDATE_BY` int(10) DEFAULT NULL,
  `UPDATED_TIMESTAMP` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ati_module_links`
--

INSERT INTO `ati_module_links` (`LINK_ID`, `LINK_NAME`, `LINK_NAME_BN`, `ATI_MLINK_PAGES`, `MODULE_ID`, `URL_URI`, `LINK_DESC`, `SL_NO`, `CREATE`, `READ`, `UPDATE`, `DELETE`, `STATUS`, `ACTIVE_STATUS`, `ENTERED_BY`, `ENTRY_TIMESTAMP`, `UPDATE_BY`, `UPDATED_TIMESTAMP`) VALUES
(1, 'Base Setup', 'মূল সেটআপ সমূহ', 'I,V,U,D,S', 1, 'baseSetup/index', NULL, 1, 1, 1, 1, 1, 1, 1, 1, '2016-04-23 05:40:02', NULL, NULL),
(2, 'Division', ' বিভাগ সমূহ', 'I,V,U,D,S', 1, 'setup/division', NULL, 2, 1, 1, 1, 1, 1, 1, 31, '2016-04-24 09:59:02', NULL, NULL),
(3, 'District', 'জেলা সমূহ', 'I,V,U,D,S', 1, 'setup/district', NULL, 3, 1, 1, 1, 1, 1, 1, 1, '2016-04-24 11:32:52', NULL, NULL),
(4, 'Thana', 'থানা', 'I,V,U,D,S', 1, 'setup/thana', NULL, 4, 1, 1, 1, 1, 1, 1, 1, '2016-04-25 08:46:50', NULL, NULL),
(5, 'Poat Office', 'পোষ্ট অফিস', 'I,V,U,D,S', 1, 'setup/postoffice', NULL, 5, 1, 1, 1, 1, 1, 1, 1, '2016-04-25 10:14:09', 1, '2016-04-25 16:48:18'),
(6, 'Mouza', 'মৌজা', 'I,V,U,D,S', 1, 'setup/mouza', NULL, 6, 1, 1, 1, 1, 1, 1, 1, '2016-04-30 04:09:31', NULL, NULL),
(7, 'Sub Land Type', 'ভূমির উপশ্রেণী', 'I,V,U,D,S', 1, 'setup/subLandType', NULL, 7, 1, 1, 1, 1, 1, 1, 1, '2016-05-02 11:46:39', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `land`
--

CREATE TABLE IF NOT EXISTS `land` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `land_number` varchar(150) NOT NULL COMMENT 'Auto generated number by system',
  `land_owner` varchar(100) NOT NULL,
  `area` int(10) unsigned NOT NULL,
  `ward` int(10) unsigned NOT NULL,
  `mouza` int(10) unsigned NOT NULL,
  `jl_no` int(10) unsigned NOT NULL,
  `category` int(10) unsigned NOT NULL,
  `subcategory` int(10) unsigned NOT NULL,
  `ousted_land` float NOT NULL,
  `possession_land` float NOT NULL,
  `total_land` float NOT NULL,
  `details` text CHARACTER SET utf8 NOT NULL,
  `comments` text CHARACTER SET utf8,
  `is_case` tinyint(1) NOT NULL COMMENT '1=yes, 2=no',
  `is_active` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0=no, 1=yes',
  `created_by` int(11) unsigned NOT NULL,
  `created_at` timestamp NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `land_ibfk_1` (`category`),
  KEY `land_ibfk_2` (`subcategory`),
  KEY `area` (`area`),
  KEY `ward` (`ward`),
  KEY `mouza` (`mouza`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `land_category`
--

CREATE TABLE IF NOT EXISTS `land_category` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) unsigned NOT NULL DEFAULT '0',
  `name` varchar(100) NOT NULL,
  `bn_name` varchar(100) CHARACTER SET utf8 NOT NULL,
  `is_active` tinyint(1) NOT NULL COMMENT '0=no, 1=yes',
  `created_by` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL,
  `updated_at` timestamp NOT NULL ON UPDATE CURRENT_TIMESTAMP,
  `update_by` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `land_category`
--

INSERT INTO `land_category` (`id`, `parent_id`, `name`, `bn_name`, `is_active`, `created_by`, `created_at`, `updated_at`, `update_by`) VALUES
(2, 0, 'sdsd', 'বন', 1, 1, '2016-06-04 06:26:58', '2016-06-04 06:26:58', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `land_dag_no`
--

CREATE TABLE IF NOT EXISTS `land_dag_no` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `land_id` int(10) unsigned NOT NULL,
  `rs` int(11) NOT NULL,
  `cs` int(11) DEFAULT NULL,
  `sa` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL,
  `created_by` int(10) unsigned NOT NULL,
  `updated_at` timestamp NOT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `land_id` (`land_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `land_docs`
--

CREATE TABLE IF NOT EXISTS `land_docs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `land_id` int(10) unsigned NOT NULL,
  `bahia_dolil` varchar(150) DEFAULT NULL,
  `dolil` varchar(150) DEFAULT NULL,
  `dcr` varchar(150) DEFAULT NULL,
  `ledger` varchar(150) DEFAULT NULL,
  `created_at` timestamp NOT NULL,
  `created_by` int(10) unsigned NOT NULL,
  `updated_at` timestamp NOT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `land_id` (`land_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `land_tax`
--

CREATE TABLE IF NOT EXISTS `land_tax` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `land_id` int(10) unsigned NOT NULL,
  `receipt` varchar(200) NOT NULL,
  `file_type` varchar(10) NOT NULL,
  `date` datetime NOT NULL,
  `created_at` timestamp NOT NULL,
  `created_by` int(10) unsigned NOT NULL,
  `updated_at` timestamp NOT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `land_id` (`land_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `land_unit`
--

CREATE TABLE IF NOT EXISTS `land_unit` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(150) NOT NULL,
  `bn_name` varchar(150) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `land_uses`
--

CREATE TABLE IF NOT EXISTS `land_uses` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `land_id` int(10) unsigned NOT NULL,
  `land_category` int(10) unsigned NOT NULL,
  `land_subcategory` int(10) unsigned NOT NULL,
  `uses` float NOT NULL,
  `created_at` timestamp NOT NULL,
  `created_by` int(10) unsigned NOT NULL,
  `updated_at` timestamp NOT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `land_id` (`land_id`),
  KEY `land_category` (`land_category`),
  KEY `land_subcategory` (`land_subcategory`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `lease`
--

CREATE TABLE IF NOT EXISTS `lease` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category_id` int(10) unsigned NOT NULL,
  `subcategory_id` int(10) unsigned NOT NULL,
  `lease_number` varchar(150) NOT NULL COMMENT 'auto generated number by system',
  `own_land` tinyint(1) NOT NULL DEFAULT '0',
  `owner_info` text,
  `name` varchar(150) NOT NULL,
  `bn_name` varchar(150) CHARACTER SET utf8 NOT NULL,
  `address` varchar(200) CHARACTER SET utf8 NOT NULL,
  `comments` text CHARACTER SET utf8,
  `status` tinyint(1) NOT NULL,
  `created_at` timestamp NOT NULL,
  `created_by` int(10) unsigned NOT NULL,
  `updated_at` timestamp NOT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `category_id` (`category_id`),
  KEY `subcategory_id` (`subcategory_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `lease_category`
--

CREATE TABLE IF NOT EXISTS `lease_category` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) unsigned NOT NULL DEFAULT '0',
  `name` varchar(150) NOT NULL,
  `bn_name` varchar(150) CHARACTER SET utf8 NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created_at` timestamp NOT NULL,
  `created_by` int(10) unsigned NOT NULL,
  `updated_at` timestamp NOT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `lease_lands`
--

CREATE TABLE IF NOT EXISTS `lease_lands` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `lease_id` int(10) unsigned NOT NULL,
  `land_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL,
  `created_by` int(10) unsigned NOT NULL,
  `updated_at` timestamp NOT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `lease_id` (`lease_id`),
  KEY `land_id` (`land_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2016_06_01_114152_entrust_setup_tables', 2);

-- --------------------------------------------------------

--
-- Table structure for table `ncc_mouza`
--

CREATE TABLE IF NOT EXISTS `ncc_mouza` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `word_id` int(10) unsigned NOT NULL,
  `name` varchar(150) NOT NULL,
  `bn_name` varchar(150) CHARACTER SET utf8 NOT NULL,
  `jl_no` float NOT NULL,
  `is_active` tinyint(1) NOT NULL COMMENT '0=no, 1=yes',
  `created_at` timestamp NOT NULL,
  `created_by` int(10) unsigned NOT NULL,
  `updated_at` timestamp NOT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `word_id` (`word_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ncc_ward`
--

CREATE TABLE IF NOT EXISTS `ncc_ward` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) unsigned NOT NULL DEFAULT '0',
  `name` varchar(150) NOT NULL,
  `bn_name` varchar(150) CHARACTER SET utf8 NOT NULL,
  `is_active` tinyint(1) NOT NULL COMMENT '0=no, 1=yes',
  `created_at` timestamp NOT NULL,
  `created_by` int(10) unsigned NOT NULL,
  `updated_at` timestamp NOT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE IF NOT EXISTS `permissions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `permissions_name_unique` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=11 ;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `display_name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'create-post', 'Create Posts', 'User can create post', '2016-01-06 01:59:50', '2016-01-06 01:59:50'),
(2, 'create-user', 'Create User', 'User can create new user', '2016-01-06 02:00:54', '2016-01-06 02:00:54'),
(3, 'edit-user', 'Edit Users', 'User can edit existing users', '2016-01-06 02:01:26', '2016-01-06 02:01:26'),
(4, 'edit-post', 'Edit post', 'User can edit post', '2016-02-15 18:33:46', '2016-02-15 18:33:46'),
(5, 'delete-post', 'Delete post', 'User can delete post', '2016-02-15 18:34:15', '2016-02-15 18:34:15'),
(6, 'view-post', 'View post', 'user can view post', '2016-02-15 18:37:00', '2016-02-15 18:37:00'),
(7, 'view-user', 'View User', 'User can view user', '2016-02-15 18:37:32', '2016-02-15 18:37:32'),
(8, 'delete-user', 'Delete user', 'user can delete user', '2016-02-15 18:39:07', '2016-02-15 18:39:07'),
(9, 'create-page', 'Create Page', 'User can create page', '2016-02-17 18:42:13', '2016-02-17 18:42:13'),
(10, 'edit-page', 'Edit Page', 'User can edit page', '2016-02-17 18:43:23', '2016-02-17 18:43:23');

-- --------------------------------------------------------

--
-- Table structure for table `permission_role`
--

CREATE TABLE IF NOT EXISTS `permission_role` (
  `permission_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`permission_id`,`role_id`),
  KEY `permission_role_role_id_foreign` (`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `permission_role`
--

INSERT INTO `permission_role` (`permission_id`, `role_id`) VALUES
(1, 1),
(2, 1),
(3, 1),
(4, 1),
(5, 1),
(6, 1),
(7, 1),
(8, 1),
(9, 1),
(10, 1),
(1, 2),
(3, 2),
(4, 2),
(5, 2),
(6, 2),
(7, 2),
(8, 2),
(6, 3),
(7, 3);

-- --------------------------------------------------------

--
-- Table structure for table `project`
--

CREATE TABLE IF NOT EXISTS `project` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `project_number` varchar(150) NOT NULL COMMENT 'auto generated number by the system',
  `name` varchar(150) NOT NULL,
  `bn_name` varchar(150) CHARACTER SET utf8 NOT NULL,
  `start_date` datetime DEFAULT NULL,
  `end_date` datetime DEFAULT NULL,
  `construction_duration` int(11) DEFAULT NULL COMMENT 'number of month',
  `budget` float DEFAULT NULL COMMENT 'amount of lakh',
  `address` varchar(200) CHARACTER SET utf8 NOT NULL,
  `total_floor` int(11) NOT NULL,
  `area` float NOT NULL,
  `facility` text CHARACTER SET utf8,
  `details` text CHARACTER SET utf8 NOT NULL,
  `attachment` varchar(200) DEFAULT NULL,
  `comments` text CHARACTER SET utf8,
  `is_active` tinyint(1) NOT NULL COMMENT '0=no, 1=yes',
  `created_at` timestamp NOT NULL,
  `created_by` int(10) unsigned NOT NULL,
  `updated_at` timestamp NOT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `project_flat`
--

CREATE TABLE IF NOT EXISTS `project_flat` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `project_id` int(10) unsigned NOT NULL,
  `type` tinyint(1) NOT NULL,
  `area` float DEFAULT NULL,
  `flat_no` int(11) NOT NULL,
  `unit_no` varchar(20) NOT NULL,
  `floor_no` int(11) NOT NULL,
  `facility` text CHARACTER SET utf8,
  `attachment` varchar(150) DEFAULT NULL,
  `status` tinyint(1) NOT NULL,
  `comments` text CHARACTER SET utf8,
  `created_at` timestamp NOT NULL,
  `created_by` int(10) unsigned NOT NULL,
  `updated_at` timestamp NOT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `project_id` (`project_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `project_lands`
--

CREATE TABLE IF NOT EXISTS `project_lands` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `project_id` int(10) unsigned NOT NULL,
  `land_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL,
  `created_by` int(10) unsigned NOT NULL,
  `updated_at` timestamp NOT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `project_id` (`project_id`),
  KEY `land_id` (`land_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `project_shop`
--

CREATE TABLE IF NOT EXISTS `project_shop` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `project_id` int(10) unsigned NOT NULL,
  `type` tinyint(1) NOT NULL,
  `area` float NOT NULL,
  `floor_no` int(11) NOT NULL,
  `shop_no` varchar(20) NOT NULL,
  `facility` text CHARACTER SET utf8,
  `attachment` varchar(150) NOT NULL,
  `comments` text CHARACTER SET utf8,
  `status` tinyint(4) NOT NULL,
  `created_at` timestamp NOT NULL,
  `created_by` int(10) unsigned NOT NULL,
  `updated_at` timestamp NOT NULL,
  `updated_by` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `project_id` (`project_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE IF NOT EXISTS `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `roles_name_unique` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `display_name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'super-admin', 'Super Administrator', 'This kind of admin have all access', '2016-01-05 21:55:22', '2016-02-16 19:56:26'),
(2, 'admin', 'Site Administrator', 'Admin can manage access control and add kind CRUD application', '2016-01-05 21:57:56', '2016-02-16 20:18:19'),
(3, 'member', 'Member', 'This type of user can only view post', '2016-02-15 18:39:19', '2016-02-15 18:39:19'),
(4, 'data-entry-operator', 'Data Entry Operator', 'The operator who can create post', '2016-02-16 20:20:13', '2016-02-16 20:20:13');

-- --------------------------------------------------------

--
-- Table structure for table `role_user`
--

CREATE TABLE IF NOT EXISTS `role_user` (
  `user_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`user_id`,`role_id`),
  KEY `role_user_role_id_foreign` (`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `role_user`
--

INSERT INTO `role_user` (`user_id`, `role_id`) VALUES
(1, 1),
(4, 1),
(6, 1),
(2, 2),
(3, 2),
(5, 2);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `first_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `user_name` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_user_name_unique` (`user_name`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=7 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `first_name`, `last_name`, `user_name`, `email`, `password`, `is_active`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'NCC', 'Admin', 'admin', 'admin@ncc.gov.bd', '$2y$10$yYj5FlZVFN7.mvfpxq223uRL.PjdNZj1URXIUIcDPJw2QR6fmfimG', 1, 'gYHUbwHiCxnd57OCINFwZEBygRiep9d3QOUAHLmalhpQEhWlgOUDhEyf4KIM', '2015-11-30 00:24:13', '2016-06-04 06:14:53'),
(2, 'Alamgir', 'Hossign', 'alamgir123', 'alamgir@gmail.com', '$2y$10$De3XX.jwSqCZX3u6wJ3NGeyDdLok1ONQeDworE8pBrAYRSaIx6NrG', 0, NULL, '2016-01-05 23:13:03', '2016-01-05 23:13:03'),
(3, 'Khan', 'hadiur', 'khan123', 'khan.hadiur@gmail.com', '$2y$10$F7Lsyha4kj.sBMgY.KT2dOs7oMRERWCf2qOAngovaY6FeFt2onR2S', 0, NULL, '2016-01-05 23:35:30', '2016-01-05 23:35:30'),
(4, 'Saif', 'Amin', 'saif123', 'saif@gmail.com', '$2y$10$XnVt5fougYO78qqD/xHjx.z.GwvOB5EO1aSmtjdVxHY8EGxEPRQWu', 0, NULL, '2016-01-05 23:41:38', '2016-01-05 23:41:38'),
(5, 'Test', 'user 1', 'test-user1', 'testmail1@email.com', '$2y$10$.d7FIPkWTV88UsD3N2b8wOGCn5i4BPvrerbG0uhTpj3zuhN8Hb3Nm', 1, NULL, '2016-02-13 23:42:52', '2016-02-15 18:30:23'),
(6, 'Abdul', 'Awal', 'ashu200', 'ashu200@yahoo.com', '$2y$10$7sifQ9bTVc8FmWjD22zBzO/SevszsXbMw1Kx.7GUM/GWhkKZNko6O', 1, NULL, '2016-02-15 18:16:04', '2016-02-15 18:31:39');

-- --------------------------------------------------------

--
-- Table structure for table `user_details`
--

CREATE TABLE IF NOT EXISTS `user_details` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `phone_no` varchar(50) DEFAULT NULL,
  `designation` varchar(200) DEFAULT NULL,
  `address` text,
  `image` varchar(150) DEFAULT NULL,
  `content_type` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `user_details`
--

INSERT INTO `user_details` (`id`, `user_id`, `phone_no`, `designation`, `address`, `image`, `content_type`) VALUES
(1, 1, '01735021845', 'Software Engineer', 'Dhaka, Bangladesh', 'user-3f569bebffe5bb8', 'jpg');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `land`
--
ALTER TABLE `land`
  ADD CONSTRAINT `land_ibfk_5` FOREIGN KEY (`mouza`) REFERENCES `ncc_mouza` (`id`),
  ADD CONSTRAINT `land_ibfk_1` FOREIGN KEY (`category`) REFERENCES `land_category` (`id`),
  ADD CONSTRAINT `land_ibfk_2` FOREIGN KEY (`subcategory`) REFERENCES `land_category` (`id`),
  ADD CONSTRAINT `land_ibfk_3` FOREIGN KEY (`area`) REFERENCES `ncc_ward` (`id`),
  ADD CONSTRAINT `land_ibfk_4` FOREIGN KEY (`ward`) REFERENCES `ncc_ward` (`id`);

--
-- Constraints for table `land_dag_no`
--
ALTER TABLE `land_dag_no`
  ADD CONSTRAINT `land_dag_no_ibfk_1` FOREIGN KEY (`land_id`) REFERENCES `land` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `land_docs`
--
ALTER TABLE `land_docs`
  ADD CONSTRAINT `land_docs_ibfk_1` FOREIGN KEY (`land_id`) REFERENCES `land` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `land_tax`
--
ALTER TABLE `land_tax`
  ADD CONSTRAINT `land_tax_ibfk_1` FOREIGN KEY (`land_id`) REFERENCES `land` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `land_uses`
--
ALTER TABLE `land_uses`
  ADD CONSTRAINT `land_uses_ibfk_1` FOREIGN KEY (`land_id`) REFERENCES `land` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `land_uses_ibfk_2` FOREIGN KEY (`land_category`) REFERENCES `land_category` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `land_uses_ibfk_3` FOREIGN KEY (`land_subcategory`) REFERENCES `land_category` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `lease`
--
ALTER TABLE `lease`
  ADD CONSTRAINT `lease_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `lease_category` (`id`),
  ADD CONSTRAINT `lease_ibfk_2` FOREIGN KEY (`subcategory_id`) REFERENCES `lease_category` (`id`);

--
-- Constraints for table `lease_lands`
--
ALTER TABLE `lease_lands`
  ADD CONSTRAINT `lease_lands_ibfk_1` FOREIGN KEY (`lease_id`) REFERENCES `lease` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `lease_lands_ibfk_2` FOREIGN KEY (`land_id`) REFERENCES `land` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `ncc_mouza`
--
ALTER TABLE `ncc_mouza`
  ADD CONSTRAINT `ncc_mouza_ibfk_1` FOREIGN KEY (`word_id`) REFERENCES `ncc_ward` (`id`);

--
-- Constraints for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `project_flat`
--
ALTER TABLE `project_flat`
  ADD CONSTRAINT `project_flat_ibfk_1` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `project_lands`
--
ALTER TABLE `project_lands`
  ADD CONSTRAINT `project_lands_ibfk_1` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `project_lands_ibfk_2` FOREIGN KEY (`land_id`) REFERENCES `land` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `project_shop`
--
ALTER TABLE `project_shop`
  ADD CONSTRAINT `project_shop_ibfk_1` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `role_user`
--
ALTER TABLE `role_user`
  ADD CONSTRAINT `role_user_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `role_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `user_details`
--
ALTER TABLE `user_details`
  ADD CONSTRAINT `user_details_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
