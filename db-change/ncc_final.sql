-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Oct 21, 2019 at 08:36 AM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `ncc_final`
--

-- --------------------------------------------------------

--
-- Table structure for table `ac_particular`
--

CREATE TABLE IF NOT EXISTS `ac_particular` (
  `PARTICULAR_ID` smallint(6) NOT NULL AUTO_INCREMENT,
  `PARTICULAR_NAME` varchar(100) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Title of the Particular e.g. Vat, Income Tax etc.',
  `PARTICULAR_DESC` text COLLATE utf8_unicode_ci,
  `TYPE` char(2) CHARACTER SET utf8 NOT NULL COMMENT '1=schedule, 2=rent',
  `IS_LEASE` tinyint(1) DEFAULT NULL,
  `IS_ACTIVE` tinyint(1) NOT NULL COMMENT '0=no, 1=yes',
  `CREATED_AT` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `CREATED_BY` int(10) NOT NULL,
  `UPDATED_AT` datetime DEFAULT NULL,
  `UPDATED_BY` int(10) DEFAULT NULL COMMENT 'Flag for tender schedule particular selection',
  PRIMARY KEY (`PARTICULAR_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=15 ;

--
-- Dumping data for table `ac_particular`
--

INSERT INTO `ac_particular` (`PARTICULAR_ID`, `PARTICULAR_NAME`, `PARTICULAR_DESC`, `TYPE`, `IS_LEASE`, `IS_ACTIVE`, `CREATED_AT`, `CREATED_BY`, `UPDATED_AT`, `UPDATED_BY`) VALUES
(3, 'জামানত', '', '', NULL, 1, '2016-07-24 23:54:59', 1, '2016-07-25 05:54:59', NULL),
(4, 'সিডিউলের মূল্য', '', '1', 0, 1, '2016-07-25 04:02:34', 1, '2016-07-25 10:02:34', NULL),
(5, 'মাসিক ভাড়া', '', '2', NULL, 1, '2016-08-01 01:00:25', 1, '2016-08-01 07:00:25', NULL),
(6, 'বায়না', '', '', NULL, 1, '2016-08-28 22:18:19', 1, '2016-08-29 09:17:06', 1),
(7, 'দরপত্রের মূল্য', '', '1', 1, 1, '2016-08-29 03:18:29', 1, '2016-08-29 09:18:29', NULL),
(8, 'দরপত্রের কিস্তি', NULL, '3', NULL, 1, '2016-09-24 18:00:00', 1, NULL, NULL),
(9, 'ভ্যাট', '', '', NULL, 1, '2016-10-25 06:43:16', 1, '2016-10-25 12:43:16', NULL),
(10, 'আয়কর বাবদ', '<p>প্রাথমিক নির্বাচন কারির অবশিষট টাকা</p>', '', NULL, 1, '2016-10-25 22:40:59', 1, '2016-10-26 04:40:59', NULL),
(11, 'খাসের টাকা', '<p>ইজারার খাসের টাকা</p>', '', NULL, 1, '2016-11-05 03:52:10', 1, '2016-11-05 09:52:53', 1),
(12, 'জরিমানা', NULL, '4', NULL, 1, '2016-11-19 03:52:10', 1, NULL, NULL),
(13, 'মালিকানা পরিবর্তন', '', '', NULL, 1, '2017-04-12 19:16:42', 1, '2017-04-13 01:16:42', NULL),
(14, 'মালিকানা বাতিল', '', '-1', NULL, 1, '2017-04-14 18:26:25', 1, '2017-04-15 00:26:25', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `ac_particular_charge`
--

CREATE TABLE IF NOT EXISTS `ac_particular_charge` (
  `P_CHARGE_ID` int(11) NOT NULL AUTO_INCREMENT,
  `PARTICULAR_ID` smallint(6) NOT NULL COMMENT 'Primary Key of ac_particular table',
  `AMOUNT` double NOT NULL COMMENT 'Amount to be charged against Particulars',
  `IS_CURRENT` tinyint(1) DEFAULT '0' COMMENT 'Current active charge',
  `IS_ACTIVE` tinyint(1) NOT NULL COMMENT '0=no, 1=yes',
  `CREATED_AT` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `CREATED_BY` int(10) NOT NULL,
  `UPDATED_AT` datetime DEFAULT NULL,
  `UPDATED_BY` int(10) DEFAULT NULL,
  PRIMARY KEY (`P_CHARGE_ID`),
  KEY `FK_PC_PARTICULAR_ID` (`PARTICULAR_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ac_paymentmst`
--

CREATE TABLE IF NOT EXISTS `ac_paymentmst` (
  `TRX_TRAN_NO` bigint(14) NOT NULL AUTO_INCREMENT,
  `TRX_TRAN_DT` date DEFAULT NULL COMMENT 'Payment Date',
  `TRAN_AMT` double DEFAULT NULL COMMENT 'Payment Amount',
  `VOUCHER_NO` bigint(14) DEFAULT NULL COMMENT 'Primary Key of ac_vouchermst table',
  `TRX_CODE_ID` tinyint(3) NOT NULL COMMENT 'Primary Key of ac_trxcode_info table',
  `COLLECTED_BY` bigint(20) DEFAULT NULL COMMENT 'Primary Key of sa_users table',
  `TCANCEL_FG` varchar(1) CHARACTER SET utf8 DEFAULT NULL COMMENT 'If Payment cancled',
  `REMARKS` varchar(200) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Remarks if have any',
  `IS_ACTIVE` tinyint(1) NOT NULL COMMENT '0=no, 1=yes',
  `CREATED_AT` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `CREATED_BY` int(10) DEFAULT NULL,
  `UPDATED_AT` datetime DEFAULT NULL,
  `UPDATED_BY` int(10) DEFAULT NULL,
  PRIMARY KEY (`TRX_TRAN_NO`),
  KEY `FK_PM_VOUCHER_NO` (`VOUCHER_NO`),
  KEY `FK_PM_USER_ID` (`COLLECTED_BY`),
  KEY `ac_paymentmst_ibfk_1` (`TRX_CODE_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `ac_paymentmst`
--

INSERT INTO `ac_paymentmst` (`TRX_TRAN_NO`, `TRX_TRAN_DT`, `TRAN_AMT`, `VOUCHER_NO`, `TRX_CODE_ID`, `COLLECTED_BY`, `TCANCEL_FG`, `REMARKS`, `IS_ACTIVE`, `CREATED_AT`, `CREATED_BY`, `UPDATED_AT`, `UPDATED_BY`) VALUES
(1, '2016-12-01', 500, 118, 4, NULL, NULL, NULL, 1, '2016-11-26 06:01:53', 6, '2016-11-26 12:01:53', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `ac_paymodeamt`
--

CREATE TABLE IF NOT EXISTS `ac_paymodeamt` (
  `MR_TRAN_NO` bigint(14) NOT NULL AUTO_INCREMENT,
  `MR_TRAN_DT` date NOT NULL COMMENT 'Payment Date',
  `TRX_TRAN_NO` bigint(14) NOT NULL COMMENT 'Primary Key of ac_paymentmst table',
  `MR_TRAN_AMT` double NOT NULL COMMENT 'Payment Amount particular wise',
  `VOUCHER_NO` bigint(14) NOT NULL COMMENT 'Primary Key of ac_vouchermst table',
  `TRX_CODE_ID` tinyint(3) NOT NULL COMMENT 'Primary Key of ac_trxcode_info table',
  `PAYMENT_MODE_ID` tinyint(2) DEFAULT NULL COMMENT 'Mode of Mayment e.g. Cash, Bank, Bikash, Online Payment, Cheque etc.',
  `REMARKS` varchar(200) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Remarks if have any',
  `IS_ACTIVE` tinyint(1) NOT NULL COMMENT '0=no, 1=yes',
  `CREATED_AT` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `CREATED_BY` int(10) DEFAULT NULL,
  `UPDATED_AT` datetime DEFAULT NULL,
  `UPDATED_BY` int(10) DEFAULT NULL,
  PRIMARY KEY (`MR_TRAN_NO`),
  KEY `FK_PA_TRX_TRAN_NO` (`TRX_TRAN_NO`),
  KEY `FK_PA_VOUCHER_NO` (`VOUCHER_NO`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `ac_paymodeamt`
--

INSERT INTO `ac_paymodeamt` (`MR_TRAN_NO`, `MR_TRAN_DT`, `TRX_TRAN_NO`, `MR_TRAN_AMT`, `VOUCHER_NO`, `TRX_CODE_ID`, `PAYMENT_MODE_ID`, `REMARKS`, `IS_ACTIVE`, `CREATED_AT`, `CREATED_BY`, `UPDATED_AT`, `UPDATED_BY`) VALUES
(1, '2016-12-01', 1, 500, 118, 4, 1, NULL, 1, '2016-11-26 06:01:53', 6, '2016-11-26 12:01:53', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `ac_pgw_return`
--

CREATE TABLE IF NOT EXISTS `ac_pgw_return` (
  `AC_PGW_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `VOUCHER_NO` bigint(14) NOT NULL COMMENT 'Primary key of ac_vouchermst table',
  `TRX_TRAN_NO` bigint(20) DEFAULT NULL COMMENT 'Primary key of ac_voucherchd table',
  `PAYMENT_TYPE` tinyint(1) NOT NULL COMMENT '1=bank pay order, 2=online, 3=cash',
  `STORE_ID` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Registered Store ID from SSL commerz',
  `TRAN_ID` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Transaction ID that was generated and sent from Merchant-end.',
  `VAL_ID` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'A Validation ID against the Transaction which is provided by SSLCOMMERZ',
  `AMOUNT` double DEFAULT NULL COMMENT 'The total Transaction Amount sent from Merchant-end including the Commissions added by Bank',
  `STORE_AMOUNT` double DEFAULT NULL COMMENT 'The total Transaction Amount sent from Merchant-end',
  `CARD_TYPE` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'The Bank Gateway Name that customer selected',
  `CARD_NO` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Customer’s Card number; only for BRAC and DBBL, not for Internet Banking',
  `CARD_ISSUER` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Issuer Bank Name',
  `CARD_BRAND` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'VISA, MASTER, AMEX, IB or MOBILE BANKING',
  `CARD_ISSUER_COUNTRY` varchar(20) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Card issuer country',
  `CARD_ISSUER_COUNTRY_CODE` char(2) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Card issuer country short code',
  `BANK_TRAN_ID` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Bank Transaction ID',
  `BANK_ID` tinyint(3) DEFAULT NULL COMMENT 'Primary Key of bank table',
  `B_DRAFT_NO` varchar(150) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Bank Draft Number',
  `ATTACHMENT` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `STATUS` varchar(6) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Transaction Status',
  `TRAN_DATE` datetime DEFAULT NULL COMMENT 'Transaction date',
  `CURRENCY` char(3) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Currency Type',
  `CURRENCY_TYPE` char(3) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Merchants requested amount in currency type',
  `CURRENCY_AMOUNT` double DEFAULT NULL COMMENT 'Currency Amount',
  `CURRENCY_RATE` double DEFAULT NULL COMMENT 'Currency Rate',
  `VERIFY_SIGN` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Data Validation Key',
  `VERIFY_KEY` text COLLATE utf8_unicode_ci COMMENT 'Data Validation Key',
  `BASE_FAIR` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Basic Fair',
  `RISK_LEVEL` tinyint(1) DEFAULT NULL COMMENT 'Transactions Risk Level',
  `RISK_TITLE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Transactions Risk Level',
  `ERROR` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Reason',
  `REMARKS` text CHARACTER SET utf8 COMMENT 'User remarks if any',
  `IS_ACTIVE` tinyint(1) NOT NULL COMMENT '0=no, 1=yes',
  `CREATED_AT` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `CREATED_BY` int(10) DEFAULT NULL,
  `UPDATED_AT` datetime DEFAULT NULL,
  `UPDATED_BY` int(10) DEFAULT NULL,
  PRIMARY KEY (`AC_PGW_ID`),
  KEY `ac_pgw_return_ibfk_1` (`VOUCHER_NO`),
  KEY `ac_pgw_return_ibfk_2` (`TRX_TRAN_NO`),
  KEY `BANK_ID` (`BANK_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=42 ;

--
-- Dumping data for table `ac_pgw_return`
--

INSERT INTO `ac_pgw_return` (`AC_PGW_ID`, `VOUCHER_NO`, `TRX_TRAN_NO`, `PAYMENT_TYPE`, `STORE_ID`, `TRAN_ID`, `VAL_ID`, `AMOUNT`, `STORE_AMOUNT`, `CARD_TYPE`, `CARD_NO`, `CARD_ISSUER`, `CARD_BRAND`, `CARD_ISSUER_COUNTRY`, `CARD_ISSUER_COUNTRY_CODE`, `BANK_TRAN_ID`, `BANK_ID`, `B_DRAFT_NO`, `ATTACHMENT`, `STATUS`, `TRAN_DATE`, `CURRENCY`, `CURRENCY_TYPE`, `CURRENCY_AMOUNT`, `CURRENCY_RATE`, `VERIFY_SIGN`, `VERIFY_KEY`, `BASE_FAIR`, `RISK_LEVEL`, `RISK_TITLE`, `ERROR`, `REMARKS`, `IS_ACTIVE`, `CREATED_AT`, `CREATED_BY`, `UPDATED_AT`, `UPDATED_BY`) VALUES
(5, 96, 47, 0, 'testbox', 'tranjLrPAMHDHf', '1611131802341OxcU9Cwjc5UiW9', 3000, 2895, 'AMEX-City Bank', NULL, NULL, NULL, NULL, NULL, '161113180234NOUUitMPoANQTab', NULL, NULL, NULL, 'VALID', '2016-11-13 18:02:25', 'BDT', 'BDT', 3000, 1, 'f2d718430237e717f10f90d4cf8b82c3', 'amount,bank_tran_id,base_fair,card_brand,card_issuer,card_issuer_country,card_issuer_country_code,card_no,card_type,currency,currency_amount,currency_rate,currency_type,risk_level,risk_title,status,store_amount,store_id,tran_date,tran_id,val_id,value_a,value_b,value_c,value_d', '0.00', 0, 'Safe', NULL, NULL, 1, '2016-11-13 06:03:12', 263, '2016-11-13 12:03:12', NULL),
(8, 103, 63, 0, 'testbox', 'tranvpQkntcuSG', '1611131831231KeXZwZc5V4OEfE', 500, 490, 'BKASH-BKash', NULL, 'BKash Mobile Banking', 'MOBILEBANKING', 'Bangladesh', 'BD', '16111318312309m8ET2oSeMr8rc', NULL, NULL, NULL, 'VALID', '2016-11-13 18:30:43', 'BDT', 'BDT', 500, 1, '0c43c9fa1991df802cc89f3654920f06', 'amount,bank_tran_id,base_fair,card_brand,card_issuer,card_issuer_country,card_issuer_country_code,card_no,card_type,currency,currency_amount,currency_rate,currency_type,risk_level,risk_title,status,store_amount,store_id,tran_date,tran_id,val_id,value_a,value_b,value_c,value_d', '0.00', 0, 'Safe', NULL, NULL, 1, '2016-11-13 06:32:14', 264, '2016-11-13 12:32:14', NULL),
(9, 105, 67, 0, 'testbox', 'tranY5ZBQeAR08', '161114145537L91zHWDn5zlhA5Y', 2000, 1940, 'VISA-Dutch Bangla', NULL, NULL, NULL, NULL, NULL, '1611141455370tjUbPt9CbmfkV5', NULL, NULL, NULL, 'VALID', '2016-11-14 14:55:31', 'BDT', 'BDT', 2000, 1, 'ea59624019dd2c102d63c1160c793fe9', 'amount,bank_tran_id,base_fair,card_brand,card_issuer,card_issuer_country,card_issuer_country_code,card_no,card_type,currency,currency_amount,currency_rate,currency_type,risk_level,risk_title,status,store_amount,store_id,tran_date,tran_id,val_id,value_a,value_b,value_c,value_d', '0.00', 0, 'Safe', NULL, NULL, 1, '2016-11-14 02:56:13', NULL, '2016-11-14 08:56:13', NULL),
(10, 109, 79, 0, 'testbox', 'tranfoniYdUgYC', '161114165658y5srBNvl3O0bPKQ', 3450, 3329.25, 'AMEX-City Bank', NULL, NULL, NULL, NULL, NULL, '1611141656581d4RgWXXspad6dR', NULL, NULL, NULL, 'VALID', '2016-11-14 16:56:53', 'BDT', 'BDT', 3450, 1, '3086e4144506960c547751203c8bcaef', 'amount,bank_tran_id,base_fair,card_brand,card_issuer,card_issuer_country,card_issuer_country_code,card_no,card_type,currency,currency_amount,currency_rate,currency_type,risk_level,risk_title,status,store_amount,store_id,tran_date,tran_id,val_id,value_a,value_b,value_c,value_d', '0.00', 0, 'Safe', NULL, NULL, 1, '2016-11-14 04:57:36', 264, '2016-11-14 10:57:36', NULL),
(11, 107, 73, 0, 'testbox', 'tranBFPABrzwgJ', '161114170725eQCHV3AqRuS71g2', 2475, 2400.75, 'MASTER-Dutch Bangla', '512815******4147', 'STANDARD CHARTERED BANK', 'MASTERCARD', 'Bangladesh', 'BD', '1611141707251PlgOAn9OiFcWWh', NULL, NULL, NULL, 'VALID', '2016-11-14 17:07:20', 'BDT', 'BDT', 2475, 1, '58d8249c3eed0293c62ee6eb45d3886f', 'amount,bank_tran_id,base_fair,card_brand,card_issuer,card_issuer_country,card_issuer_country_code,card_no,card_type,currency,currency_amount,currency_rate,currency_type,risk_level,risk_title,status,store_amount,store_id,tran_date,tran_id,val_id,value_a,value_b,value_c,value_d', '0.00', 0, 'Safe', NULL, NULL, 1, '2016-11-14 05:08:01', NULL, '2016-11-14 11:08:01', NULL),
(12, 107, 74, 0, 'testbox', 'tranGc1YbQVZUi', '161114170932zw7l4aZNbXp7wVP', 2250, 2193.75, 'IBBL-Islami Bank', NULL, 'Islami Bank Bangladesh Limited', 'IB', 'Bangladesh', 'BD', '1611141709320iNUDbYbUiaN7hr', NULL, NULL, NULL, 'VALID', '2016-11-14 17:09:25', 'BDT', 'BDT', 2250, 1, '1f00a55be071624e580c771d6fcc7486', 'amount,bank_tran_id,base_fair,card_brand,card_issuer,card_issuer_country,card_issuer_country_code,card_no,card_type,currency,currency_amount,currency_rate,currency_type,risk_level,risk_title,status,store_amount,store_id,tran_date,tran_id,val_id,value_a,value_b,value_c,value_d', '0.00', 0, 'Safe', NULL, NULL, 1, '2016-11-14 05:10:07', NULL, '2016-11-14 11:10:07', NULL),
(13, 112, 87, 0, 'testbox', 'trani4NkV9ilWx', '1611151450520h81BAiIW4nVLsF', 600, 582, 'MASTER-Dutch Bangla', '546630******5961', 'CAPITAL ONE BANK (USA), N.A.', 'MASTERCARD', 'United States', 'US', '1611151450521n6nZocQqYUUpBb', NULL, NULL, NULL, 'VALID', '2016-11-15 14:50:46', 'BDT', 'BDT', 600, 1, 'c88ea54dbf3f2da69400fc0a96af5462', 'amount,bank_tran_id,base_fair,card_brand,card_issuer,card_issuer_country,card_issuer_country_code,card_no,card_type,currency,currency_amount,currency_rate,currency_type,risk_level,risk_title,status,store_amount,store_id,tran_date,tran_id,val_id,value_a,value_b,value_c,value_d', '0.00', 1, 'Not Safe', NULL, NULL, 1, '2016-11-15 02:51:28', NULL, '2016-11-15 08:51:28', NULL),
(14, 107, 75, 0, 'testbox', 'tranoFZ8TwBrin', '161115150507YeCMvBk77YVSaLS', 2250, 2182.5, 'VISA-Dutch Bangla', NULL, NULL, NULL, NULL, NULL, '161115150507UROQGjZ5eCF7kUD', NULL, NULL, NULL, 'VALID', '2016-11-15 15:05:03', 'BDT', 'BDT', 2250, 1, '11d65728868e2e4da3646e6cecd6ef17', 'amount,bank_tran_id,base_fair,card_brand,card_issuer,card_issuer_country,card_issuer_country_code,card_no,card_type,currency,currency_amount,currency_rate,currency_type,risk_level,risk_title,status,store_amount,store_id,tran_date,tran_id,val_id,value_a,value_b,value_c,value_d', '0.00', 0, 'Safe', NULL, NULL, 1, '2016-11-15 03:05:43', NULL, '2016-11-15 09:05:43', NULL),
(15, 107, 76, 0, 'testbox', 'trandWp5n5tgr5', '1611151505221k1o2CvP5AIBGr0', 2250, 2209.5, 'NEXUS-Dutch Bangla', '100001*********5091', 'Dutch Bangla Bank Limited', 'NEXUS', 'Bangladesh', 'BD', '1611151505221S7q18Mg28h92Xl', NULL, NULL, NULL, 'VALID', '2016-11-15 15:05:17', 'BDT', 'BDT', 2250, 1, 'feaf0b42ec6cc5a88bc9cbca9a224967', 'amount,bank_tran_id,base_fair,card_brand,card_issuer,card_issuer_country,card_issuer_country_code,card_no,card_type,currency,currency_amount,currency_rate,currency_type,risk_level,risk_title,status,store_amount,store_id,tran_date,tran_id,val_id,value_a,value_b,value_c,value_d', '0.00', 0, 'Safe', NULL, NULL, 1, '2016-11-15 03:05:58', NULL, '2016-11-15 09:05:58', NULL),
(16, 115, 93, 0, 'testbox', 'tranacqmdAF3s3', '1611231412212yPrnZfBJPpT64k', 3360, 3242.4, 'AMEX-City Bank', NULL, NULL, NULL, NULL, NULL, '1611231412211v2OpebJH4hLNtu', NULL, NULL, NULL, 'VALID', '2016-11-23 14:11:44', 'BDT', 'BDT', 3360, 1, '476db5f07b06a363ff8176eafb4228f1', 'amount,bank_tran_id,base_fair,card_brand,card_issuer,card_issuer_country,card_issuer_country_code,card_no,card_type,currency,currency_amount,currency_rate,currency_type,risk_level,risk_title,status,store_amount,store_id,tran_date,tran_id,val_id,value_a,value_b,value_c,value_d', '0.00', 0, 'Safe', NULL, NULL, 1, '2016-11-23 02:12:59', 265, '2016-11-23 08:12:59', NULL),
(19, 113, 90, 0, 'testbox', 'trannxgINcbARW', '16112615272116F74p8TpyMr4Ts', 1200, 1164, 'VISA-Dutch Bangla', NULL, NULL, NULL, NULL, NULL, '1611261527211ptGz7BLuHc7Pjk', NULL, NULL, NULL, 'VALID', '2016-11-26 15:27:14', 'BDT', 'BDT', 1200, 1, '360b69a1938d05d10c418b8a01e4b1be', 'amount,bank_tran_id,base_fair,card_brand,card_issuer,card_issuer_country,card_issuer_country_code,card_no,card_type,currency,currency_amount,currency_rate,currency_type,risk_level,risk_title,status,store_amount,store_id,tran_date,tran_id,val_id,value_a,value_b,value_c,value_d', '0.00', 0, 'Safe', NULL, 'Comments', 1, '2016-11-26 03:27:57', 18, '2016-11-26 09:27:57', NULL),
(20, 114, 91, 0, 'testbox', 'tranUA0UqHLGXl', NULL, 1200, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'CANCEL', '2016-12-05 17:33:35', 'BDT', 'BDT', 1200, 1, '392aea1d80714efa9f81cc985e1e4e25', 'amount,bank_tran_id,base_fair,currency,currency_amount,currency_rate,currency_type,error,status,store_id,tran_date,tran_id,value_a,value_b,value_c,value_d', '0.00', NULL, NULL, 'Cancelled by User', 'gh', 1, '2016-12-05 05:34:33', 18, '2016-12-05 11:34:33', NULL),
(21, 119, 100, 0, 'testbox', 'tranlvVbhmw33l', NULL, 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'CANCEL', '2016-12-24 17:09:26', 'BDT', 'BDT', 3000, 1, '50f651a1c7bfdb05fcdb7dc34e3491a3', 'amount,bank_tran_id,base_fair,currency,currency_amount,currency_rate,currency_type,error,status,store_id,tran_date,tran_id,value_a,value_b,value_c,value_d', '0.00', NULL, NULL, 'Cancelled by User', NULL, 1, '2016-12-24 05:10:11', NULL, '2016-12-24 11:10:11', NULL),
(22, 120, 101, 0, 'testbox', 'tranGz1r0OkAKA', NULL, 3300, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'CANCEL', '2016-12-24 17:09:54', 'BDT', 'BDT', 3300, 1, '1e73d9132d3b1e1e3aa811852231acea', 'amount,bank_tran_id,base_fair,currency,currency_amount,currency_rate,currency_type,error,status,store_id,tran_date,tran_id,value_a,value_b,value_c,value_d', '0.00', NULL, NULL, 'Cancelled by User', NULL, 1, '2016-12-24 05:10:37', NULL, '2016-12-24 11:10:37', NULL),
(23, 114, 91, 0, 'testbox', 'tranETiwGq1BbI', NULL, 1200, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'CANCEL', '2016-12-24 17:26:38', 'BDT', 'BDT', 1200, 1, '8c88a7adcdbc01e274c2fcd0077127e1', 'amount,bank_tran_id,base_fair,currency,currency_amount,currency_rate,currency_type,error,status,store_id,tran_date,tran_id,value_a,value_b,value_c,value_d', '0.00', NULL, NULL, 'Cancelled by User', NULL, 1, '2016-12-24 05:27:21', 18, '2016-12-24 11:27:21', NULL),
(24, 114, 91, 0, 'testbox', 'tranUvkIEXELjo', NULL, 1200, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'CANCEL', '2016-12-24 17:40:35', 'BDT', 'BDT', 1200, 1, 'c5aa290f29662ec6bf73d5c69732e370', 'amount,bank_tran_id,base_fair,currency,currency_amount,currency_rate,currency_type,error,status,store_id,tran_date,tran_id,value_a,value_b,value_c,value_d', '0.00', NULL, NULL, 'Cancelled by User', NULL, 1, '2016-12-24 05:41:18', 18, '2016-12-24 11:41:18', NULL),
(25, 114, 91, 0, 'testbox', 'tranXswr0WLE4B', NULL, 1200, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'CANCEL', '2016-12-24 17:44:51', 'BDT', 'BDT', 1200, 1, 'd878bbf5732c28a8d56870547f6d673a', 'amount,bank_tran_id,base_fair,currency,currency_amount,currency_rate,currency_type,error,status,store_id,tran_date,tran_id,value_a,value_b,value_c,value_d', '0.00', NULL, NULL, 'Cancelled by User', NULL, 1, '2016-12-24 05:45:33', 18, '2016-12-24 11:45:33', NULL),
(26, 114, 91, 0, 'testbox', 'trannmCgqdRXy3', NULL, 1200, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'CANCEL', '2016-12-24 17:45:26', 'BDT', 'BDT', 1200, 1, 'bcbb2b69ed434fcedea504df82e6d74e', 'amount,bank_tran_id,base_fair,currency,currency_amount,currency_rate,currency_type,error,status,store_id,tran_date,tran_id,value_a,value_b,value_c,value_d', '0.00', NULL, NULL, 'Cancelled by User', NULL, 1, '2016-12-24 05:46:08', 18, '2016-12-24 11:46:08', NULL),
(27, 119, 100, 0, 'testbox', 'tranbh9RhgfSyL', '16122910584919pS1Rvo4aJIZnk', 3000, 2910, 'MASTER-Dutch Bangla', '519625XXXXXX7150', 'CITY BANK, LTD.', 'MASTERCARD', 'Bangladesh', 'BD', '16122910584901S83XHpZ9uYngl', NULL, NULL, NULL, 'VALID', '2016-12-29 10:58:43', 'BDT', 'BDT', 3000, 1, '69e612c39c03b1f3ff4d7fb558b1c71f', 'amount,bank_tran_id,base_fair,card_brand,card_issuer,card_issuer_country,card_issuer_country_code,card_no,card_type,currency,currency_amount,currency_rate,currency_type,risk_level,risk_title,status,store_amount,store_id,tran_date,tran_id,val_id,value_a,value_b,value_c,value_d', '0.00', 0, 'Safe', NULL, NULL, 1, '2016-12-28 22:59:28', NULL, '2016-12-29 04:59:28', NULL),
(28, 140, 138, 1, NULL, NULL, NULL, 3600, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 8, '34545', 'da6185a6bb87ed1a02cefb532f5e1edf.jpg', NULL, '2017-04-19 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Bank pay order', 1, '2017-03-31 14:18:18', 276, '2017-03-31 20:18:18', NULL),
(29, 119, 99, 1, NULL, NULL, NULL, 2000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 8, '3434', 'a0d5dafc15da7c94c0a79a12a59a2ffc.jpg', NULL, '2017-04-13 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'pay order 2', 1, '2017-03-31 14:24:26', NULL, '2017-03-31 20:24:26', NULL),
(30, 120, 101, 3, NULL, NULL, NULL, 3300, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '04118d8147d872c71fd03fbd954759f6.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Cash payment', 1, '2017-03-31 14:32:33', NULL, '2017-03-31 20:32:33', NULL),
(31, 141, 140, 2, 'testbox', 'tranBzciZqlyDe', '1704112346400HRR7bIir9eRief', 69697, 67257.605, 'AMEX-City Bank', NULL, NULL, NULL, NULL, NULL, '1704112346401WYMgq4far1DNwu', NULL, NULL, NULL, 'VALID', '2017-04-11 23:46:20', 'BDT', 'BDT', 69697, 1, '2b3dea5c4a90d883b0086142c1974389', 'amount,bank_tran_id,base_fair,card_brand,card_issuer,card_issuer_country,card_issuer_country_code,card_no,card_type,currency,currency_amount,currency_rate,currency_type,risk_level,risk_title,status,store_amount,store_id,tran_date,tran_id,val_id,value_a,value_b,value_c,value_d', '0.00', NULL, 'Safe', NULL, NULL, 1, '2017-04-11 11:47:26', NULL, '2017-04-11 17:47:26', NULL),
(32, 141, 139, 2, 'testbox', 'tran4ycS9UiXFJ', '17041123471416Xu5oWYCQgt8iN', 4500, 4342.5, 'AMEX-City Bank', NULL, NULL, NULL, NULL, NULL, '1704112347141TKUplfCMuhT0AW', NULL, NULL, NULL, 'VALID', '2017-04-11 23:47:07', 'BDT', 'BDT', 4500, 1, '061e33abc76f1090d0d96ce0d8c8b2a4', 'amount,bank_tran_id,base_fair,card_brand,card_issuer,card_issuer_country,card_issuer_country_code,card_no,card_type,currency,currency_amount,currency_rate,currency_type,risk_level,risk_title,status,store_amount,store_id,tran_date,tran_id,val_id,value_a,value_b,value_c,value_d', '0.00', NULL, 'Safe', NULL, NULL, 1, '2017-04-11 11:48:00', NULL, '2017-04-11 17:48:00', NULL),
(33, 142, 142, 3, NULL, NULL, NULL, 3600, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2017-04-11 11:53:16', 277, '2017-04-11 17:53:16', NULL),
(34, 142, 141, 1, NULL, NULL, NULL, 4500, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 8, '5522655362554', 'd3ed763db92be5a345f57a36b00931db.jpg', NULL, '2017-04-20 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2017-04-11 11:54:10', 277, '2017-04-11 17:54:10', NULL),
(35, 143, 143, 2, 'testbox', 'tranA6swJxLsRM', '1704112356280xViXw7EGXvVQzd', 8400, 8148, 'MASTER-Dutch Bangla', '512815XXXXXX5306', 'STANDARD CHARTERED BANK', 'MASTERCARD', 'Bangladesh', 'BD', '1704112356281MYeHPboXttcP74', NULL, NULL, NULL, 'VALID', '2017-04-11 23:56:22', 'BDT', 'BDT', 8400, 1, 'c72120d101cdccf70ce31f85165a4a87', 'amount,bank_tran_id,base_fair,card_brand,card_issuer,card_issuer_country,card_issuer_country_code,card_no,card_type,currency,currency_amount,currency_rate,currency_type,risk_level,risk_title,status,store_amount,store_id,tran_date,tran_id,val_id,value_a,value_b,value_c,value_d', '0.00', NULL, 'Safe', NULL, NULL, 1, '2017-04-11 11:57:14', 277, '2017-04-11 17:57:14', NULL),
(36, 148, 151, 1, NULL, NULL, NULL, 3600, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 8, '123456', 'd95969d800510802a3ed38968ede000e.jpg', NULL, '2017-04-14 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2017-04-15 05:23:58', 278, '2017-04-15 11:23:58', NULL),
(37, 123, 108, 2, 'testbox', 'tran60WsiCLS07', '1704161454213ML9znoIcvKgN75', 9660, 9370.2, 'VISA-Dutch Bangla', NULL, NULL, NULL, NULL, NULL, '1704161454211d9G3GYNj529uPF', NULL, NULL, NULL, 'VALID', '2017-04-16 14:53:59', 'BDT', 'BDT', 9660, 1, '0d36943d882b53ea19ff784e1cde2255', 'amount,bank_tran_id,base_fair,card_brand,card_issuer,card_issuer_country,card_issuer_country_code,card_no,card_type,currency,currency_amount,currency_rate,currency_type,risk_level,risk_title,status,store_amount,store_id,tran_date,tran_id,val_id,value_a,value_b,value_c,value_d', '0.00', NULL, 'Safe', NULL, NULL, 1, '2017-04-16 02:55:09', 267, '2017-04-16 08:55:09', NULL),
(38, 124, 109, 1, NULL, NULL, NULL, 9240, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 8, ' পে-অর্ডার নাম্বার: * ', '88d49a4fd8d26b7dcb90832d32da35e1.jpg', NULL, '2017-04-16 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'মন্তব্য', 1, '2017-04-16 02:57:38', NULL, '2017-04-16 08:57:38', NULL),
(39, 124, 109, 2, 'testbox', 'tranRwWeKsylDO', '170416145935nFH44nIs6SgpzZN', 9240, 9055.2, 'DBBLMOBILEB-Dbbl Mobile Banking', NULL, 'DBBL Mobile Banking', 'MOBILEBANKING', 'Bangladesh', 'BD', '1704161459350CG4PLKq0apyopL', NULL, NULL, NULL, 'VALID', '2017-04-16 14:59:30', 'BDT', 'BDT', 9240, 1, '1daebe0b43f702a1883bad1b29815a2a', 'amount,bank_tran_id,base_fair,card_brand,card_issuer,card_issuer_country,card_issuer_country_code,card_no,card_type,currency,currency_amount,currency_rate,currency_type,risk_level,risk_title,status,store_amount,store_id,tran_date,tran_id,val_id,value_a,value_b,value_c,value_d', '0.00', NULL, 'Safe', NULL, NULL, 1, '2017-04-16 03:00:23', 267, '2017-04-16 09:00:23', NULL),
(40, 122, 107, 3, NULL, NULL, NULL, 3600, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '5455', NULL, NULL, '2017-04-19 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'মন্তব্য', 1, '2017-04-16 03:32:35', NULL, '2017-04-16 09:32:35', NULL),
(41, 122, 106, 1, NULL, NULL, NULL, 2000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 8, '4555', '1ba08233c97f2e2222fc092a01ac660d.jpg', NULL, '2017-04-12 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2017-04-16 03:34:18', NULL, '2017-04-16 09:34:18', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `ac_trxcode_info`
--

CREATE TABLE IF NOT EXISTS `ac_trxcode_info` (
  `TRX_CODE_ID` tinyint(3) NOT NULL AUTO_INCREMENT COMMENT 'Primary key',
  `TRX_CODE_NO` char(2) NOT NULL COMMENT 'ex: CR, DR',
  `NARRATION` varchar(50) NOT NULL COMMENT 'CR=credit, DR=debit',
  PRIMARY KEY (`TRX_CODE_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `ac_trxcode_info`
--

INSERT INTO `ac_trxcode_info` (`TRX_CODE_ID`, `TRX_CODE_NO`, `NARRATION`) VALUES
(1, 'DC', 'Discount'),
(2, 'DR', 'Discount Return'),
(3, 'GR', 'Gross'),
(4, 'GS', 'Gross Sales'),
(5, 'PM', 'Payment'),
(6, 'RF', 'Refund'),
(7, 'VA', 'VAT'),
(8, 'VR', 'VAT Return');

-- --------------------------------------------------------

--
-- Table structure for table `ac_vn_ledgers`
--

CREATE TABLE IF NOT EXISTS `ac_vn_ledgers` (
  `VLEDGER_NO` bigint(14) NOT NULL AUTO_INCREMENT,
  `VLEDGER_DT` date DEFAULT NULL COMMENT 'Transaction create date',
  `TRX_CODE_ID` tinyint(3) NOT NULL COMMENT 'Primary Key of ac_trxcode_info table',
  `TRX_TRAN_NO` bigint(14) DEFAULT NULL COMMENT 'Primary key of ac_voucherchd table',
  `VOUCHER_NO` bigint(14) DEFAULT NULL COMMENT 'Primary Key of ac_vouchermst table',
  `CR_AMT` double DEFAULT NULL COMMENT 'Credit Amount',
  `DR_AMT` double DEFAULT NULL COMMENT 'Dedit Amount',
  `PITEM_TQTY` int(11) DEFAULT NULL COMMENT 'Quantity',
  `REMARKS` varchar(200) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Remarks if have any',
  `IS_ACTIVE` tinyint(1) NOT NULL COMMENT '0=no, 1=yes',
  `IS_VERIFIED` tinyint(1) DEFAULT '0' COMMENT '1 account verified , 0 waiting',
  `CREATED_AT` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `CREATED_BY` int(10) DEFAULT NULL,
  `UPDATED_AT` datetime DEFAULT NULL,
  `UPDATED_BY` int(10) DEFAULT NULL,
  PRIMARY KEY (`VLEDGER_NO`),
  KEY `ac_vn_ledgers_ibfk_1` (`VOUCHER_NO`),
  KEY `ac_vn_ledgers_ibfk_3` (`TRX_CODE_ID`),
  KEY `ac_vn_ledgers_ibfk_2` (`TRX_TRAN_NO`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=205 ;

--
-- Dumping data for table `ac_vn_ledgers`
--

INSERT INTO `ac_vn_ledgers` (`VLEDGER_NO`, `VLEDGER_DT`, `TRX_CODE_ID`, `TRX_TRAN_NO`, `VOUCHER_NO`, `CR_AMT`, `DR_AMT`, `PITEM_TQTY`, `REMARKS`, `IS_ACTIVE`, `IS_VERIFIED`, `CREATED_AT`, `CREATED_BY`, `UPDATED_AT`, `UPDATED_BY`) VALUES
(56, '2016-11-13', 4, 46, 96, 2000, NULL, NULL, NULL, 1, 0, '2016-11-12 22:03:56', NULL, '2016-11-13 04:03:56', NULL),
(57, '2016-11-13', 4, 47, 96, 3000, NULL, NULL, NULL, 1, 0, '2016-11-12 22:03:56', NULL, '2016-11-13 04:03:56', NULL),
(58, '2016-11-13', 4, 48, 97, 500, NULL, NULL, NULL, 1, 0, '2016-11-12 23:44:35', NULL, '2016-11-13 05:44:35', NULL),
(59, '2016-11-13', 4, 49, 97, 12000, NULL, NULL, NULL, 1, 0, '2016-11-12 23:44:35', NULL, '2016-11-13 05:44:35', NULL),
(60, '2016-11-13', 4, 50, 98, 500, NULL, NULL, NULL, 1, 0, '2016-11-13 00:15:14', NULL, '2016-11-13 06:15:14', NULL),
(61, '2016-11-13', 4, 51, 98, 12000, NULL, NULL, NULL, 1, 0, '2016-11-13 00:15:14', NULL, '2016-11-13 06:15:14', NULL),
(62, '2016-11-13', 5, 51, 98, 0, 12000, NULL, NULL, 1, 1, '2016-11-13 00:15:14', NULL, '2016-11-13 11:16:36', 1),
(63, '2016-11-13', 4, 52, 99, 1000, NULL, NULL, NULL, 1, 0, '2016-11-13 04:28:47', 1, '2016-11-13 10:28:47', NULL),
(64, '2016-11-13', 4, 53, 99, 2000, NULL, NULL, NULL, 1, 0, '2016-11-13 04:28:47', 1, '2016-11-13 10:28:47', NULL),
(65, '2016-11-13', 4, 54, 99, 3000, NULL, NULL, NULL, 1, 0, '2016-11-13 04:28:48', 1, '2016-11-13 10:28:48', NULL),
(69, '2016-11-13', 4, 58, 101, 600, NULL, NULL, NULL, 1, 0, '2016-11-13 05:38:10', 1, '2016-11-13 11:38:10', NULL),
(70, '2016-11-13', 4, 59, 101, 1800, NULL, NULL, NULL, 1, 0, '2016-11-13 05:38:10', 1, '2016-11-13 11:38:10', NULL),
(71, '2016-11-13', 4, 60, 101, 600, NULL, NULL, NULL, 1, 0, '2016-11-13 05:38:10', 1, '2016-11-13 11:38:10', NULL),
(72, '2016-11-13', 5, 47, 96, NULL, 3000, NULL, NULL, 1, 0, '2016-11-13 06:03:12', NULL, '2016-11-13 12:03:12', NULL),
(77, '2016-11-13', 4, 63, 103, 500, NULL, NULL, NULL, 1, 0, '2016-11-13 06:29:11', NULL, '2016-11-13 12:29:11', NULL),
(78, '2016-11-13', 4, 64, 103, 550500, NULL, NULL, NULL, 1, 0, '2016-11-13 06:29:12', NULL, '2016-11-13 12:29:12', NULL),
(79, '2016-11-13', 5, 63, 103, NULL, 500, NULL, NULL, 1, 0, '2016-11-13 06:32:14', NULL, '2016-11-13 12:32:14', NULL),
(83, '2016-11-14', 4, 67, 105, 2000, NULL, NULL, NULL, 1, 0, '2016-11-14 00:05:43', NULL, '2016-11-14 06:05:43', NULL),
(84, '2016-11-14', 4, 68, 105, 3000, NULL, NULL, NULL, 1, 0, '2016-11-14 00:05:43', NULL, '2016-11-14 06:05:43', NULL),
(85, '2016-11-14', 5, 68, 105, NULL, 3000, NULL, NULL, 1, 0, '2016-11-14 00:05:43', NULL, '2016-11-14 06:05:43', NULL),
(86, '2016-11-14', 5, 67, 105, NULL, 2000, NULL, NULL, 1, 0, '2016-11-14 02:56:13', NULL, '2016-11-14 08:56:13', NULL),
(91, '2016-11-14', 4, 73, 107, 2250, NULL, NULL, NULL, 1, 0, '2016-11-14 04:30:13', 1, '2016-11-14 10:30:13', NULL),
(92, '2016-11-14', 4, 74, 107, 2250, NULL, NULL, NULL, 1, 0, '2016-11-14 04:30:13', 1, '2016-11-14 10:30:13', NULL),
(93, '2016-11-14', 4, 75, 107, 2250, NULL, NULL, NULL, 1, 0, '2016-11-14 04:30:13', 1, '2016-11-14 10:30:13', NULL),
(94, '2016-11-14', 4, 76, 107, 2250, NULL, NULL, NULL, 1, 0, '2016-11-14 04:30:13', 1, '2016-11-14 10:30:13', NULL),
(95, '2016-11-14', 4, 77, 108, 2000, NULL, NULL, NULL, 1, 0, '2016-11-14 04:41:52', NULL, '2016-11-14 10:41:52', NULL),
(96, '2016-11-14', 4, 78, 108, 3000, NULL, NULL, NULL, 1, 0, '2016-11-14 04:41:52', NULL, '2016-11-14 10:41:52', NULL),
(97, '2016-11-14', 4, 79, 109, 3000, NULL, NULL, NULL, 1, 0, '2016-11-14 04:43:07', 1, '2016-11-14 10:43:07', NULL),
(98, '2016-11-14', 4, 80, 109, 3000, NULL, NULL, NULL, 1, 0, '2016-11-14 04:43:07', 1, '2016-11-14 10:43:07', NULL),
(99, '2016-11-14', 4, 81, 109, 3000, NULL, NULL, NULL, 1, 0, '2016-11-14 04:43:07', 1, '2016-11-14 10:43:07', NULL),
(100, '2016-11-14', 5, 79, 109, NULL, 3450, NULL, NULL, 1, 0, '2016-11-14 04:57:36', NULL, '2016-11-14 10:57:36', NULL),
(101, '2016-11-14', 5, 73, 107, NULL, 2475, NULL, NULL, 1, 0, '2016-11-14 05:08:01', NULL, '2016-11-14 11:08:01', NULL),
(102, '2016-11-14', 5, 74, 107, NULL, 2250, NULL, NULL, 1, 0, '2016-11-14 05:10:07', NULL, '2016-11-14 11:10:07', NULL),
(103, '2016-11-14', 4, 82, 110, 27525, NULL, NULL, NULL, 1, 0, '2016-11-14 07:39:29', 1, '2016-11-14 13:39:29', NULL),
(104, '2016-11-14', 4, 83, 110, 82575, NULL, NULL, NULL, 1, 0, '2016-11-14 07:39:29', 1, '2016-11-14 13:39:29', NULL),
(105, '2016-11-14', 4, 84, 110, 27525, NULL, NULL, NULL, 1, 0, '2016-11-14 07:39:29', 1, '2016-11-14 13:39:29', NULL),
(106, '2016-11-15', 4, 85, 111, 500, NULL, NULL, NULL, 1, 0, '2016-11-14 22:51:57', NULL, '2016-11-15 04:51:57', NULL),
(107, '2016-11-15', 4, 86, 111, 12000, NULL, NULL, NULL, 1, 0, '2016-11-14 22:51:57', NULL, '2016-11-15 04:51:57', NULL),
(108, '2016-11-15', 4, 87, 112, 600, NULL, NULL, NULL, 1, 0, '2016-11-14 22:53:13', 1, '2016-11-15 04:53:13', NULL),
(109, '2016-11-15', 4, 88, 112, 1800, NULL, NULL, NULL, 1, 0, '2016-11-14 22:53:13', 1, '2016-11-15 04:53:13', NULL),
(110, '2016-11-15', 4, 89, 112, 600, NULL, NULL, NULL, 1, 0, '2016-11-14 22:53:13', 1, '2016-11-15 04:53:13', NULL),
(111, '2016-11-15', 5, 87, 112, NULL, 600, NULL, NULL, 1, 0, '2016-11-15 02:51:28', NULL, '2016-11-15 08:51:28', NULL),
(112, '2016-11-15', 5, 75, 107, NULL, 2250, NULL, NULL, 1, 0, '2016-11-15 03:05:43', NULL, '2016-11-15 09:05:43', NULL),
(113, '2016-11-15', 5, 76, 107, NULL, 2250, NULL, NULL, 1, 0, '2016-11-15 03:05:58', NULL, '2016-11-15 09:05:58', NULL),
(114, '2016-11-21', 4, 90, 113, 1200, NULL, NULL, NULL, 1, 0, '2016-11-21 06:34:27', NULL, '2016-11-21 12:34:27', NULL),
(115, '2016-11-22', 4, 91, 114, 1200, NULL, NULL, NULL, 1, 0, '2016-11-22 03:05:37', NULL, '2016-11-22 09:05:37', NULL),
(116, '2016-11-23', 4, 92, 115, 2000, NULL, NULL, NULL, 1, 0, '2016-11-23 02:03:41', NULL, '2016-11-23 08:03:41', NULL),
(117, '2016-11-23', 4, 93, 115, 3360, NULL, NULL, NULL, 1, 0, '2016-11-23 02:03:41', NULL, '2016-11-23 08:03:41', NULL),
(118, '2016-11-23', 5, 93, 115, NULL, 3360, NULL, NULL, 1, 0, '2016-11-23 02:12:59', NULL, '2016-11-23 08:12:59', NULL),
(119, '2016-11-23', 4, 94, 116, 500, NULL, NULL, NULL, 1, 0, '2016-11-23 02:20:17', NULL, '2016-11-23 08:20:17', NULL),
(120, '2016-11-23', 4, 95, 116, 12000, NULL, NULL, NULL, 1, 0, '2016-11-23 02:20:17', NULL, '2016-11-23 08:20:17', NULL),
(121, '2016-11-23', 5, 95, 116, NULL, 12000, NULL, NULL, 1, 0, '2016-11-23 02:20:17', NULL, '2016-11-23 08:20:17', NULL),
(122, '2016-11-23', 4, 96, 117, 4320, NULL, NULL, NULL, 1, 0, '2016-11-23 06:02:26', 7, '2016-11-23 12:02:26', NULL),
(123, '2016-11-23', 4, 97, 117, 4320, NULL, NULL, NULL, 1, 0, '2016-11-23 06:02:26', 7, '2016-11-23 12:02:26', NULL),
(126, '2016-11-26', 5, 90, 113, NULL, 1200, NULL, NULL, 1, 0, '2016-11-26 03:27:57', NULL, '2016-11-26 09:27:57', NULL),
(127, '2016-12-01', 4, 98, 118, 500, NULL, NULL, NULL, 1, 0, '2016-11-26 06:01:53', 6, '2016-11-26 12:01:53', NULL),
(128, '2016-12-01', 5, 98, 118, NULL, 500, NULL, NULL, 1, 0, '2016-11-26 06:01:53', 6, '2016-11-26 12:01:53', NULL),
(129, '2016-11-28', 4, 99, 119, 2000, NULL, NULL, NULL, 1, 0, '2016-11-28 03:33:32', NULL, '2016-11-28 09:33:32', NULL),
(130, '2016-11-28', 4, 100, 119, 3000, NULL, NULL, NULL, 1, 0, '2016-11-28 03:33:32', NULL, '2016-11-28 09:33:32', NULL),
(131, '2016-11-30', 4, 101, 120, 3000, NULL, NULL, NULL, 1, 0, '2016-11-30 02:16:49', 1, '2016-11-30 08:16:49', NULL),
(132, '2016-11-30', 4, 102, 120, 3000, NULL, NULL, NULL, 1, 0, '2016-11-30 02:16:49', 1, '2016-11-30 08:16:49', NULL),
(133, '2016-11-30', 4, 103, 120, 3000, NULL, NULL, NULL, 1, 0, '2016-11-30 02:16:49', 1, '2016-11-30 08:16:49', NULL),
(134, '2016-12-07', 4, 104, 121, 500, NULL, NULL, NULL, 1, 0, '2016-12-07 00:22:03', NULL, '2016-12-07 06:22:03', NULL),
(135, '2016-12-07', 4, 105, 121, 120, NULL, NULL, NULL, 1, 0, '2016-12-07 00:22:03', NULL, '2016-12-07 06:22:03', NULL),
(136, '2016-12-07', 4, 106, 122, 2000, NULL, NULL, NULL, 1, 0, '2016-12-07 00:26:04', NULL, '2016-12-07 06:26:04', NULL),
(137, '2016-12-07', 4, 107, 122, 3600, NULL, NULL, NULL, 1, 0, '2016-12-07 00:26:04', NULL, '2016-12-07 06:26:04', NULL),
(138, '2016-12-07', 4, 108, 123, 8400, NULL, NULL, NULL, 1, 0, '2016-12-07 00:28:42', 1, '2016-12-07 06:28:42', NULL),
(139, '2016-12-07', 4, 109, 124, 8400, NULL, NULL, NULL, 1, 0, '2016-12-07 00:30:26', 1, '2016-12-07 06:30:26', NULL),
(140, '2016-12-07', 4, 110, 125, 8400, NULL, NULL, NULL, 1, 0, '2016-12-07 00:36:50', 1, '2016-12-07 06:36:50', NULL),
(141, '2016-12-07', 4, 111, 126, 8400, NULL, NULL, NULL, 1, 0, '2016-12-07 00:44:39', 1, '2016-12-07 06:44:39', NULL),
(142, '2016-12-12', 4, 112, 127, 2000, NULL, NULL, NULL, 1, 0, '2016-12-11 23:51:18', NULL, '2016-12-12 05:51:18', NULL),
(143, '2016-12-12', 4, 113, 127, 3000, NULL, NULL, NULL, 1, 0, '2016-12-11 23:51:18', NULL, '2016-12-12 05:51:18', NULL),
(144, '2016-12-12', 4, 114, 128, 3000, NULL, NULL, NULL, 1, 0, '2016-12-11 23:53:40', 1, '2016-12-12 05:53:40', NULL),
(145, '2016-12-12', 4, 115, 128, 3000, NULL, NULL, NULL, 1, 0, '2016-12-11 23:53:40', 1, '2016-12-12 05:53:40', NULL),
(146, '2016-12-12', 4, 116, 128, 3000, NULL, NULL, NULL, 1, 0, '2016-12-11 23:53:40', 1, '2016-12-12 05:53:40', NULL),
(147, '2016-12-17', 4, 117, 129, 2000, NULL, NULL, NULL, 1, 0, '2016-12-17 02:46:10', NULL, '2016-12-17 08:46:10', NULL),
(148, '2016-12-17', 4, 118, 129, 3000, NULL, NULL, NULL, 1, 0, '2016-12-17 02:46:10', NULL, '2016-12-17 08:46:10', NULL),
(149, '2016-12-21', 4, 119, 130, 1200, NULL, NULL, NULL, 1, 0, '2016-12-21 00:53:06', NULL, '2016-12-21 06:53:06', NULL),
(150, '2016-12-21', 4, 120, 131, 600, NULL, NULL, NULL, 1, 0, '2016-12-21 04:39:48', 6, '2016-12-21 10:39:48', NULL),
(151, '2016-12-21', 4, 121, 131, 1800, NULL, NULL, NULL, 1, 0, '2016-12-21 04:39:48', 6, '2016-12-21 10:39:48', NULL),
(152, '2016-12-21', 4, 122, 131, 600, NULL, NULL, NULL, 1, 0, '2016-12-21 04:39:48', 6, '2016-12-21 10:39:48', NULL),
(153, '2016-12-26', 4, 123, 132, 4500, NULL, NULL, NULL, 1, 0, '2016-12-26 06:36:17', NULL, '2016-12-26 12:36:17', NULL),
(154, '2016-12-26', 4, 124, 132, 3000000, NULL, NULL, NULL, 1, 0, '2016-12-26 06:36:17', NULL, '2016-12-26 12:36:17', NULL),
(155, '2016-12-26', 4, 125, 133, 3500000, NULL, NULL, NULL, 1, 0, '2016-12-26 06:43:14', 1, '2016-12-26 12:43:14', NULL),
(156, '2016-12-26', 4, 126, 133, 3500000, NULL, NULL, NULL, 1, 0, '2016-12-26 06:43:14', 1, '2016-12-26 12:43:14', NULL),
(157, '2016-12-29', 5, 100, 119, NULL, 3000, NULL, NULL, 1, 0, '2016-12-28 22:59:28', NULL, '2016-12-29 04:59:28', NULL),
(158, '2017-01-24', 4, 127, 134, 4500, NULL, NULL, NULL, 1, 0, '2017-01-23 23:23:09', NULL, '2017-01-24 05:23:09', NULL),
(159, '2017-01-24', 4, 128, 134, 3600, NULL, NULL, NULL, 1, 0, '2017-01-23 23:23:09', NULL, '2017-01-24 05:23:09', NULL),
(160, '2017-01-24', 5, 128, 134, NULL, 3600, NULL, NULL, 1, 0, '2017-01-23 23:23:09', NULL, '2017-01-24 05:23:09', NULL),
(161, '2017-01-29', 4, 129, 135, 1200, NULL, NULL, NULL, 1, 0, '2017-01-29 02:19:51', NULL, '2017-01-29 08:19:51', NULL),
(162, '2017-01-29', 4, 130, 136, 1200, NULL, NULL, NULL, 1, 0, '2017-01-29 02:19:51', NULL, '2017-01-29 08:19:51', NULL),
(163, '2017-01-31', 4, 131, 137, 2500, NULL, NULL, NULL, 1, 0, '2017-01-31 02:27:06', NULL, '2017-01-31 08:27:06', NULL),
(164, '2017-01-31', 4, 132, 137, 3360, NULL, NULL, NULL, 1, 0, '2017-01-31 02:27:06', NULL, '2017-01-31 08:27:06', NULL),
(165, '2017-03-08', 4, 133, 138, 4500, NULL, NULL, NULL, 1, 0, '2017-03-08 00:40:36', NULL, '2017-03-08 06:40:36', NULL),
(166, '2017-03-08', 4, 134, 138, 300000, NULL, NULL, NULL, 1, 0, '2017-03-08 00:40:36', NULL, '2017-03-08 06:40:36', NULL),
(167, '2017-03-12', 4, 135, 139, 4500, NULL, NULL, NULL, 1, 0, '2017-03-12 01:27:30', NULL, '2017-03-12 07:27:30', NULL),
(168, '2017-03-12', 4, 136, 139, 150, NULL, NULL, NULL, 1, 0, '2017-03-12 01:27:30', NULL, '2017-03-12 07:27:30', NULL),
(169, '2017-03-29', 4, 137, 140, 4500, NULL, NULL, NULL, 1, 0, '2017-03-29 11:18:40', NULL, '2017-03-29 17:18:40', NULL),
(170, '2017-03-29', 4, 138, 140, 3600, NULL, NULL, NULL, 1, 0, '2017-03-29 11:18:40', NULL, '2017-03-29 17:18:40', NULL),
(171, '2017-04-19', 5, 138, 140, NULL, 3600, NULL, NULL, 1, 0, '2017-03-31 14:18:18', NULL, '2017-03-31 20:18:18', NULL),
(172, '2017-04-13', 5, 99, 119, NULL, 2000, NULL, NULL, 1, 0, '2017-03-31 14:24:26', NULL, '2017-03-31 20:24:26', NULL),
(173, '2017-04-11', 5, 101, 120, NULL, 3300, NULL, NULL, 1, 0, '2017-03-31 14:32:33', NULL, '2017-03-31 20:32:33', NULL),
(174, '2017-04-11', 4, 139, 141, 4500, NULL, NULL, NULL, 1, 0, '2017-04-11 11:46:07', NULL, '2017-04-11 17:46:07', NULL),
(175, '2017-04-11', 4, 140, 141, 69697, NULL, NULL, NULL, 1, 0, '2017-04-11 11:46:07', NULL, '2017-04-11 17:46:07', NULL),
(176, '2017-04-11', 5, 140, 141, NULL, 69697, NULL, NULL, 1, 0, '2017-04-11 11:47:26', NULL, '2017-04-11 17:47:26', NULL),
(177, '2017-04-11', 5, 139, 141, NULL, 4500, NULL, NULL, 1, 0, '2017-04-11 11:48:00', NULL, '2017-04-11 17:48:00', NULL),
(178, '2017-04-11', 4, 141, 142, 4500, NULL, NULL, NULL, 1, 0, '2017-04-11 11:52:11', NULL, '2017-04-11 17:52:11', NULL),
(179, '2017-04-11', 4, 142, 142, 3600, NULL, NULL, NULL, 1, 0, '2017-04-11 11:52:11', NULL, '2017-04-11 17:52:11', NULL),
(180, '2017-04-12', 5, 142, 142, NULL, 3600, NULL, NULL, 1, 1, '2017-04-11 11:53:16', NULL, '2017-04-11 18:01:15', 1),
(181, '2017-04-20', 5, 141, 142, NULL, 4500, NULL, NULL, 1, 1, '2017-04-11 11:54:10', NULL, '2017-04-11 18:01:19', 1),
(182, '2017-04-11', 4, 143, 143, 8400, NULL, NULL, NULL, 1, 0, '2017-04-11 11:55:36', 1, '2017-04-11 17:55:36', NULL),
(183, '2017-04-11', 5, 143, 143, NULL, 8400, NULL, NULL, 1, 1, '2017-04-11 11:57:14', NULL, '2017-04-11 18:01:23', 1),
(184, '2017-04-11', 4, 144, 144, 1200, NULL, NULL, NULL, 1, 0, '2017-04-11 12:30:32', NULL, '2017-04-11 18:30:32', NULL),
(186, '2017-04-15', 6, 146, 146, 600, NULL, NULL, NULL, 1, 0, '2017-04-14 19:38:58', NULL, '2017-04-15 01:38:58', NULL),
(187, '2017-04-15', 4, 147, 147, 6, NULL, NULL, NULL, 1, 0, '2017-04-15 04:11:22', 1, '2017-04-15 10:11:22', NULL),
(188, '2017-04-15', 4, 148, 147, 18, NULL, NULL, NULL, 1, 0, '2017-04-15 04:11:22', 1, '2017-04-15 10:11:22', NULL),
(189, '2017-04-15', 4, 149, 147, 6, NULL, NULL, NULL, 1, 0, '2017-04-15 04:11:22', 1, '2017-04-15 10:11:22', NULL),
(190, '2017-04-15', 4, 150, 148, 4500, NULL, NULL, NULL, 1, 0, '2017-04-15 05:07:10', NULL, '2017-04-15 11:07:10', NULL),
(191, '2017-04-15', 4, 151, 148, 3600, NULL, NULL, NULL, 1, 0, '2017-04-15 05:07:10', NULL, '2017-04-15 11:07:10', NULL),
(192, '2017-04-14', 5, 151, 148, NULL, 3600, NULL, NULL, 1, 1, '2017-04-15 05:23:58', NULL, '2017-04-16 10:48:35', 1),
(193, '2017-04-15', 6, 152, 149, 600, NULL, NULL, NULL, 1, 0, '2017-04-15 05:37:12', NULL, '2017-04-15 11:37:12', NULL),
(194, '2017-04-16', 4, 153, 150, 4500, NULL, NULL, NULL, 1, 0, '2017-04-16 02:47:18', NULL, '2017-04-16 08:47:18', NULL),
(195, '2017-04-16', 4, 154, 150, 450, NULL, NULL, NULL, 1, 0, '2017-04-16 02:47:18', NULL, '2017-04-16 08:47:18', NULL),
(196, '2017-04-16', 4, 155, 151, 2500, NULL, NULL, NULL, 1, 0, '2017-04-16 02:51:44', NULL, '2017-04-16 08:51:44', NULL),
(197, '2017-04-16', 4, 156, 151, 4470, NULL, NULL, NULL, 1, 0, '2017-04-16 02:51:45', NULL, '2017-04-16 08:51:45', NULL),
(198, '2017-04-16', 5, 108, 123, NULL, 9660, NULL, NULL, 1, 0, '2017-04-16 02:55:09', NULL, '2017-04-16 08:55:09', NULL),
(199, '2017-04-16', 5, 109, 124, NULL, 9240, NULL, NULL, 1, 0, '2017-04-16 02:57:38', NULL, '2017-04-16 08:57:38', NULL),
(200, '2017-04-16', 5, 109, 124, NULL, 9240, NULL, NULL, 1, 0, '2017-04-16 03:00:23', NULL, '2017-04-16 09:00:23', NULL),
(201, '2017-04-16', 4, 157, 152, 4500, NULL, NULL, NULL, 1, 0, '2017-04-16 03:02:44', NULL, '2017-04-16 09:02:44', NULL),
(202, '2017-04-16', 4, 158, 152, 167, NULL, NULL, NULL, 1, 0, '2017-04-16 03:02:44', NULL, '2017-04-16 09:02:44', NULL),
(203, '2017-04-19', 5, 107, 122, NULL, 3600, NULL, NULL, 1, 0, '2017-04-16 03:32:35', NULL, '2017-04-16 09:32:35', NULL),
(204, '2017-04-12', 5, 106, 122, NULL, 2000, NULL, NULL, 1, 0, '2017-04-16 03:34:19', NULL, '2017-04-16 09:34:19', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `ac_voucherchd`
--

CREATE TABLE IF NOT EXISTS `ac_voucherchd` (
  `TRX_TRAN_NO` bigint(14) NOT NULL AUTO_INCREMENT,
  `TRX_TRAN_DT` date DEFAULT NULL COMMENT 'Transaction create Date',
  `VOUCHER_NO` bigint(14) DEFAULT NULL COMMENT 'Primary Key of ac_vouchermst table',
  `SCHEDULE_ID` bigint(20) DEFAULT NULL COMMENT 'Primary key of tender_schedule table',
  `PARTICULAR_ID` smallint(6) NOT NULL COMMENT 'Primary Key of ac_particular table',
  `PUNIT_PRICE` float DEFAULT NULL COMMENT 'Price of particular',
  `SEND_MAIL` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Determine if email send',
  `SEND_SMS` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Determine if SMS send',
  `IS_VIEW` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0=not view, 1=view applicant or citizen, 2=view accounts manager',
  `TCANCEL_FG` char(1) CHARACTER SET utf8 DEFAULT NULL COMMENT 'If Transaction is cancled',
  `REMARKS` text CHARACTER SET utf8 COMMENT 'Remarks if have any',
  `IS_ACTIVE` tinyint(1) NOT NULL COMMENT '0=no, 1=yes',
  `CREATED_AT` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `CREATED_BY` int(10) DEFAULT NULL,
  `UPDATED_AT` datetime DEFAULT NULL,
  `UPDATED_BY` int(10) DEFAULT NULL,
  PRIMARY KEY (`TRX_TRAN_NO`),
  KEY `FK_VC_VOUCHER_NO` (`VOUCHER_NO`),
  KEY `FK_VC_PARTICULAR_ID` (`PARTICULAR_ID`),
  KEY `SCHEDULE_ID` (`SCHEDULE_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=159 ;

--
-- Dumping data for table `ac_voucherchd`
--

INSERT INTO `ac_voucherchd` (`TRX_TRAN_NO`, `TRX_TRAN_DT`, `VOUCHER_NO`, `SCHEDULE_ID`, `PARTICULAR_ID`, `PUNIT_PRICE`, `SEND_MAIL`, `SEND_SMS`, `IS_VIEW`, `TCANCEL_FG`, `REMARKS`, `IS_ACTIVE`, `CREATED_AT`, `CREATED_BY`, `UPDATED_AT`, `UPDATED_BY`) VALUES
(46, '2016-11-13', 96, 128, 4, 2000, 0, 0, 0, NULL, NULL, 1, '2016-11-12 22:03:56', NULL, '2016-11-13 04:03:56', NULL),
(47, '2016-11-13', 96, 128, 3, 3000, 0, 0, 0, NULL, NULL, 1, '2016-11-12 22:03:56', NULL, '2016-11-13 04:03:56', NULL),
(48, '2016-11-13', 97, 124, 7, 500, 0, 0, 0, NULL, NULL, 1, '2016-11-12 23:44:34', NULL, '2016-11-13 05:44:34', NULL),
(49, '2016-11-13', 97, 124, 6, 12000, 0, 0, 0, NULL, NULL, 1, '2016-11-12 23:44:35', NULL, '2016-11-13 05:44:35', NULL),
(50, '2016-11-13', 98, 123, 7, 500, 0, 0, 0, NULL, NULL, 1, '2016-11-13 00:15:14', NULL, '2016-11-13 06:15:14', NULL),
(51, '2016-11-13', 98, 123, 6, 12000, 0, 0, 0, NULL, NULL, 1, '2016-11-13 00:15:14', NULL, '2016-11-13 06:15:14', NULL),
(52, '2016-11-13', 99, 128, 5, 1000, 0, 0, 0, NULL, NULL, 1, '2016-11-13 04:28:47', 1, '2016-11-13 10:28:47', NULL),
(53, '2016-11-13', 99, 128, 4, 2000, 0, 0, 0, NULL, NULL, 1, '2016-11-13 04:28:47', 1, '2016-11-13 10:28:47', NULL),
(54, '2016-11-13', 99, 128, 3, 3000, 0, 0, 0, NULL, NULL, 1, '2016-11-13 04:28:48', 1, '2016-11-13 10:28:48', NULL),
(58, '2016-11-13', 101, 124, 3, 600, 0, 0, 0, NULL, NULL, 1, '2016-11-13 05:38:10', 1, '2016-11-13 11:38:10', NULL),
(59, '2016-11-13', 101, 124, 9, 1800, 0, 0, 0, NULL, NULL, 1, '2016-11-13 05:38:10', 1, '2016-11-13 11:38:10', NULL),
(60, '2016-11-13', 101, 124, 10, 600, 0, 0, 0, NULL, NULL, 1, '2016-11-13 05:38:10', 1, '2016-11-13 11:38:10', NULL),
(63, '2016-11-13', 103, 124, 7, 500, 0, 0, 0, NULL, NULL, 1, '2016-11-13 06:29:11', NULL, '2016-11-13 12:29:11', NULL),
(64, '2016-11-13', 103, 124, 6, 550500, 0, 0, 0, NULL, NULL, 1, '2016-11-13 06:29:12', NULL, '2016-11-13 12:29:12', NULL),
(67, '2016-11-14', 105, 128, 4, 2000, 0, 0, 0, NULL, NULL, 1, '2016-11-14 00:05:43', NULL, '2016-11-14 06:05:43', NULL),
(68, '2016-11-14', 105, 128, 3, 3000, 0, 0, 0, NULL, NULL, 1, '2016-11-14 00:05:43', NULL, '2016-11-14 06:05:43', NULL),
(73, '2016-11-14', 107, 128, 8, 2250, 0, 0, 0, NULL, NULL, 1, '2016-11-14 04:30:12', 1, '2016-11-14 10:30:12', NULL),
(74, '2016-11-14', 107, 128, 8, 2250, 0, 0, 0, NULL, NULL, 1, '2016-11-14 04:30:13', 1, '2016-11-14 10:30:13', NULL),
(75, '2016-11-14', 107, 128, 8, 2250, 0, 0, 0, NULL, NULL, 1, '2016-11-14 04:30:13', 1, '2016-11-14 10:30:13', NULL),
(76, '2016-11-14', 107, 128, 8, 2250, 0, 0, 0, NULL, NULL, 1, '2016-11-14 04:30:13', 1, '2016-11-14 10:30:13', NULL),
(77, '2016-11-14', 108, 128, 4, 2000, 0, 0, 0, NULL, NULL, 1, '2016-11-14 04:41:52', NULL, '2016-11-14 10:41:52', NULL),
(78, '2016-11-14', 108, 128, 3, 3000, 0, 0, 0, NULL, NULL, 1, '2016-11-14 04:41:52', NULL, '2016-11-14 10:41:52', NULL),
(79, '2016-11-14', 109, 128, 8, 3000, 0, 0, 0, NULL, NULL, 1, '2016-11-14 04:43:07', 1, '2016-11-14 10:43:07', NULL),
(80, '2016-11-14', 109, 128, 8, 3000, 0, 0, 0, NULL, NULL, 1, '2016-11-14 04:43:07', 1, '2016-11-14 10:43:07', NULL),
(81, '2016-11-14', 109, 128, 8, 3000, 0, 0, 0, NULL, NULL, 1, '2016-11-14 04:43:07', 1, '2016-11-14 10:43:07', NULL),
(82, '2016-11-14', 110, 124, 3, 27525, 0, 0, 0, NULL, NULL, 1, '2016-11-14 07:39:29', 1, '2016-11-14 13:39:29', NULL),
(83, '2016-11-14', 110, 124, 9, 82575, 0, 0, 0, NULL, NULL, 1, '2016-11-14 07:39:29', 1, '2016-11-14 13:39:29', NULL),
(84, '2016-11-14', 110, 124, 10, 27525, 0, 0, 0, NULL, NULL, 1, '2016-11-14 07:39:29', 1, '2016-11-14 13:39:29', NULL),
(85, '2016-11-15', 111, 124, 7, 500, 0, 0, 0, NULL, NULL, 1, '2016-11-14 22:51:57', NULL, '2016-11-15 04:51:57', NULL),
(86, '2016-11-15', 111, 124, 6, 12000, 0, 0, 0, NULL, NULL, 1, '2016-11-14 22:51:57', NULL, '2016-11-15 04:51:57', NULL),
(87, '2016-11-15', 112, 124, 3, 600, 0, 0, 0, NULL, NULL, 1, '2016-11-14 22:53:13', 1, '2016-11-15 04:53:13', NULL),
(88, '2016-11-15', 112, 124, 9, 1800, 0, 0, 0, NULL, NULL, 1, '2016-11-14 22:53:13', 1, '2016-11-15 04:53:13', NULL),
(89, '2016-11-15', 112, 124, 10, 600, 0, 0, 0, NULL, NULL, 1, '2016-11-14 22:53:13', 1, '2016-11-15 04:53:13', NULL),
(90, '2016-11-21', 113, 128, 5, 1200, 0, 0, 0, NULL, NULL, 1, '2016-11-21 06:34:27', NULL, '2016-11-21 12:34:27', NULL),
(91, '2016-11-22', 114, 128, 5, 1200, 0, 0, 0, NULL, NULL, 1, '2016-11-22 03:05:37', NULL, '2016-11-22 09:05:37', NULL),
(92, '2016-11-23', 115, 137, 4, 2000, 0, 0, 0, NULL, NULL, 1, '2016-11-23 02:03:40', NULL, '2016-11-23 08:03:40', NULL),
(93, '2016-11-23', 115, 137, 3, 3360, 0, 0, 0, NULL, NULL, 1, '2016-11-23 02:03:41', NULL, '2016-11-23 08:03:41', NULL),
(94, '2016-11-23', 116, 136, 7, 500, 0, 0, 0, NULL, NULL, 1, '2016-11-23 02:20:17', NULL, '2016-11-23 08:20:17', NULL),
(95, '2016-11-23', 116, 136, 6, 12000, 0, 0, 0, NULL, NULL, 1, '2016-11-23 02:20:17', NULL, '2016-11-23 08:20:17', NULL),
(96, '2016-11-23', 117, 137, 8, 4320, 0, 0, 0, NULL, NULL, 1, '2016-11-23 06:02:26', 7, '2016-11-23 12:02:26', NULL),
(97, '2016-11-23', 117, 137, 8, 4320, 0, 0, 0, NULL, NULL, 1, '2016-11-23 06:02:26', 7, '2016-11-23 12:02:26', NULL),
(98, '2016-12-01', 118, 121, 11, 500, 0, 0, 0, NULL, NULL, 1, '2016-11-26 06:01:53', 6, '2016-11-26 12:01:53', NULL),
(99, '2016-11-28', 119, 141, 4, 2000, 0, 0, 0, NULL, NULL, 1, '2016-11-28 03:33:32', NULL, '2016-11-28 09:33:32', NULL),
(100, '2016-11-28', 119, 141, 3, 3000, 0, 0, 0, NULL, NULL, 1, '2016-11-28 03:33:32', NULL, '2016-11-28 09:33:32', NULL),
(101, '2016-11-30', 120, 141, 8, 3000, 0, 0, 0, NULL, NULL, 1, '2016-11-30 02:16:49', 1, '2016-11-30 08:16:49', NULL),
(102, '2016-11-30', 120, 141, 8, 3000, 0, 0, 0, NULL, NULL, 1, '2016-11-30 02:16:49', 1, '2016-11-30 08:16:49', NULL),
(103, '2016-11-30', 120, 141, 8, 3000, 0, 0, 0, NULL, NULL, 1, '2016-11-30 02:16:49', 1, '2016-11-30 08:16:49', NULL),
(104, '2016-12-07', 121, 134, 7, 500, 0, 0, 0, NULL, NULL, 1, '2016-12-07 00:22:03', NULL, '2016-12-07 06:22:03', NULL),
(105, '2016-12-07', 121, 134, 6, 120, 0, 0, 0, NULL, NULL, 1, '2016-12-07 00:22:03', NULL, '2016-12-07 06:22:03', NULL),
(106, '2016-12-07', 122, 139, 4, 2000, 0, 0, 0, NULL, NULL, 1, '2016-12-07 00:26:04', NULL, '2016-12-07 06:26:04', NULL),
(107, '2016-12-07', 122, 139, 3, 3600, 0, 0, 0, NULL, NULL, 1, '2016-12-07 00:26:04', NULL, '2016-12-07 06:26:04', NULL),
(108, '2016-12-07', 123, 139, 8, 8400, 0, 0, 0, NULL, NULL, 1, '2016-12-07 00:28:42', 1, '2016-12-07 06:28:42', NULL),
(109, '2016-12-07', 124, 139, 8, 8400, 0, 0, 0, NULL, NULL, 1, '2016-12-07 00:30:25', 1, '2016-12-07 06:30:25', NULL),
(110, '2016-12-07', 125, 139, 8, 8400, 0, 0, 0, NULL, NULL, 1, '2016-12-07 00:36:50', 1, '2016-12-07 06:36:50', NULL),
(111, '2016-12-07', 126, 139, 8, 8400, 0, 0, 0, NULL, NULL, 1, '2016-12-07 00:44:39', 1, '2016-12-07 06:44:39', NULL),
(112, '2016-12-12', 127, 141, 4, 2000, 0, 0, 0, NULL, NULL, 1, '2016-12-11 23:51:18', NULL, '2016-12-12 05:51:18', NULL),
(113, '2016-12-12', 127, 141, 3, 3000, 0, 0, 0, NULL, NULL, 1, '2016-12-11 23:51:18', NULL, '2016-12-12 05:51:18', NULL),
(114, '2016-12-12', 128, 141, 8, 3000, 0, 0, 0, NULL, NULL, 1, '2016-12-11 23:53:40', 1, '2016-12-12 05:53:40', NULL),
(115, '2016-12-12', 128, 141, 8, 3000, 0, 0, 0, NULL, NULL, 1, '2016-12-11 23:53:40', 1, '2016-12-12 05:53:40', NULL),
(116, '2016-12-12', 128, 141, 8, 3000, 0, 0, 0, NULL, NULL, 1, '2016-12-11 23:53:40', 1, '2016-12-12 05:53:40', NULL),
(117, '2016-12-17', 129, 141, 4, 2000, 0, 0, 0, NULL, NULL, 1, '2016-12-17 02:46:10', NULL, '2016-12-17 08:46:10', NULL),
(118, '2016-12-17', 129, 141, 3, 3000, 0, 0, 0, NULL, NULL, 1, '2016-12-17 02:46:10', NULL, '2016-12-17 08:46:10', NULL),
(119, '2016-12-21', 130, 128, 5, 1200, 0, 0, 0, NULL, NULL, 1, '2016-12-21 00:53:05', NULL, '2016-12-21 06:53:05', NULL),
(120, '2016-12-21', 131, 136, 3, 600, 0, 0, 0, NULL, NULL, 1, '2016-12-21 04:39:48', 6, '2016-12-21 10:39:48', NULL),
(121, '2016-12-21', 131, 136, 9, 1800, 0, 0, 0, NULL, NULL, 1, '2016-12-21 04:39:48', 6, '2016-12-21 10:39:48', NULL),
(122, '2016-12-21', 131, 136, 10, 600, 0, 0, 0, NULL, NULL, 1, '2016-12-21 04:39:48', 6, '2016-12-21 10:39:48', NULL),
(123, '2016-12-26', 132, 144, 4, 4500, 0, 0, 0, NULL, NULL, 1, '2016-12-26 06:36:17', NULL, '2016-12-26 12:36:17', NULL),
(124, '2016-12-26', 132, 144, 3, 3000000, 0, 0, 0, NULL, NULL, 1, '2016-12-26 06:36:17', NULL, '2016-12-26 12:36:17', NULL),
(125, '2016-12-26', 133, 144, 8, 3500000, 0, 0, 0, NULL, NULL, 1, '2016-12-26 06:43:14', 1, '2016-12-26 12:43:14', NULL),
(126, '2016-12-26', 133, 144, 8, 3500000, 0, 0, 0, NULL, NULL, 1, '2016-12-26 06:43:14', 1, '2016-12-26 12:43:14', NULL),
(127, '2017-01-24', 134, 138, 4, 4500, 0, 0, 0, NULL, NULL, 1, '2017-01-23 23:23:09', NULL, '2017-01-24 05:23:09', NULL),
(128, '2017-01-24', 134, 138, 3, 3600, 0, 0, 0, NULL, NULL, 1, '2017-01-23 23:23:09', NULL, '2017-01-24 05:23:09', NULL),
(129, '2017-01-29', 135, 128, 5, 1200, 0, 0, 0, NULL, NULL, 1, '2017-01-29 02:19:51', NULL, '2017-01-29 08:19:51', NULL),
(130, '2017-01-29', 136, 128, 5, 1200, 0, 0, 0, NULL, NULL, 1, '2017-01-29 02:19:51', NULL, '2017-01-29 08:19:51', NULL),
(131, '2017-01-31', 137, 145, 4, 2500, 0, 0, 0, NULL, NULL, 1, '2017-01-31 02:27:06', NULL, '2017-01-31 08:27:06', NULL),
(132, '2017-01-31', 137, 145, 3, 3360, 0, 0, 0, NULL, NULL, 1, '2017-01-31 02:27:06', NULL, '2017-01-31 08:27:06', NULL),
(133, '2017-03-08', 138, 138, 4, 4500, 0, 0, 0, NULL, NULL, 1, '2017-03-08 00:40:36', NULL, '2017-03-08 06:40:36', NULL),
(134, '2017-03-08', 138, 138, 3, 300000, 0, 0, 0, NULL, NULL, 1, '2017-03-08 00:40:36', NULL, '2017-03-08 06:40:36', NULL),
(135, '2017-03-12', 139, 139, 4, 4500, 0, 0, 0, NULL, NULL, 1, '2017-03-12 01:27:30', NULL, '2017-03-12 07:27:30', NULL),
(136, '2017-03-12', 139, 139, 3, 150, 0, 0, 0, NULL, NULL, 1, '2017-03-12 01:27:30', NULL, '2017-03-12 07:27:30', NULL),
(137, '2017-03-29', 140, 138, 4, 4500, 0, 0, 0, NULL, NULL, 1, '2017-03-29 11:18:40', NULL, '2017-03-29 17:18:40', NULL),
(138, '2017-03-29', 140, 138, 3, 3600, 0, 0, 0, NULL, NULL, 1, '2017-03-29 11:18:40', NULL, '2017-03-29 17:18:40', NULL),
(139, '2017-04-11', 141, 139, 4, 4500, 0, 0, 0, NULL, NULL, 1, '2017-04-11 11:46:07', NULL, '2017-04-11 17:46:07', NULL),
(140, '2017-04-11', 141, 139, 3, 69697, 0, 0, 0, NULL, NULL, 1, '2017-04-11 11:46:07', NULL, '2017-04-11 17:46:07', NULL),
(141, '2017-04-11', 142, 139, 4, 4500, 0, 0, 0, NULL, NULL, 1, '2017-04-11 11:52:11', NULL, '2017-04-11 17:52:11', NULL),
(142, '2017-04-11', 142, 139, 3, 3600, 0, 0, 0, NULL, NULL, 1, '2017-04-11 11:52:11', NULL, '2017-04-11 17:52:11', NULL),
(143, '2017-04-11', 143, 139, 8, 8400, 0, 0, 0, NULL, NULL, 1, '2017-04-11 11:55:36', 1, '2017-04-11 17:55:36', NULL),
(144, '2017-04-11', 144, 128, 5, 1200, 0, 0, 0, NULL, NULL, 1, '2017-04-11 12:30:32', NULL, '2017-04-11 18:30:32', NULL),
(146, '2017-04-15', 146, 141, 14, 600, 0, 0, 0, NULL, NULL, 1, '2017-04-14 19:38:58', NULL, '2017-04-15 01:38:58', NULL),
(147, '2017-04-15', 147, 134, 3, 6, 0, 0, 0, NULL, NULL, 1, '2017-04-15 04:11:22', 1, '2017-04-15 10:11:22', NULL),
(148, '2017-04-15', 147, 134, 9, 18, 0, 0, 0, NULL, NULL, 1, '2017-04-15 04:11:22', 1, '2017-04-15 10:11:22', NULL),
(149, '2017-04-15', 147, 134, 10, 6, 0, 0, 0, NULL, NULL, 1, '2017-04-15 04:11:22', 1, '2017-04-15 10:11:22', NULL),
(150, '2017-04-15', 148, 139, 4, 4500, 0, 0, 0, NULL, NULL, 1, '2017-04-15 05:07:10', NULL, '2017-04-15 11:07:10', NULL),
(151, '2017-04-15', 148, 139, 3, 3600, 0, 0, 0, NULL, NULL, 1, '2017-04-15 05:07:10', NULL, '2017-04-15 11:07:10', NULL),
(152, '2017-04-15', 149, 128, 14, 600, 0, 0, 0, NULL, NULL, 1, '2017-04-15 05:37:12', NULL, '2017-04-15 11:37:12', NULL),
(153, '2017-04-16', 150, 138, 4, 4500, 0, 0, 0, NULL, NULL, 1, '2017-04-16 02:47:18', NULL, '2017-04-16 08:47:18', NULL),
(154, '2017-04-16', 150, 138, 3, 450, 0, 0, 0, NULL, NULL, 1, '2017-04-16 02:47:18', NULL, '2017-04-16 08:47:18', NULL),
(155, '2017-04-16', 151, 145, 4, 2500, 0, 0, 0, NULL, NULL, 1, '2017-04-16 02:51:44', NULL, '2017-04-16 08:51:44', NULL),
(156, '2017-04-16', 151, 145, 3, 4470, 0, 0, 0, NULL, NULL, 1, '2017-04-16 02:51:45', NULL, '2017-04-16 08:51:45', NULL),
(157, '2017-04-16', 152, 139, 4, 4500, 0, 0, 0, NULL, NULL, 1, '2017-04-16 03:02:44', NULL, '2017-04-16 09:02:44', NULL),
(158, '2017-04-16', 152, 139, 3, 167, 0, 0, 0, NULL, NULL, 1, '2017-04-16 03:02:44', NULL, '2017-04-16 09:02:44', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `ac_vouchermst`
--

CREATE TABLE IF NOT EXISTS `ac_vouchermst` (
  `VOUCHER_NO` bigint(14) NOT NULL AUTO_INCREMENT,
  `VOUCHER_DT` date DEFAULT NULL COMMENT 'Voucher create Date',
  `CITIZEN_ID` bigint(20) DEFAULT NULL COMMENT 'Primary Key of sa_citizen table. This ID holder will be only Citizen',
  `TE_APP_ID` bigint(20) DEFAULT NULL COMMENT 'If applicant is new data will be populate',
  `LEASE_DE_ID` bigint(20) DEFAULT NULL COMMENT 'Primary key of lease_demesne table',
  `VCANCEL_FG` char(1) CHARACTER SET utf8 DEFAULT NULL COMMENT 'If Voucher is cancled',
  `REMARKS` text CHARACTER SET utf8 COMMENT 'Remarks if have any',
  `IS_ACTIVE` tinyint(1) NOT NULL COMMENT '0=no, 1=yes',
  `CREATED_AT` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `CREATED_BY` int(10) DEFAULT NULL,
  `UPDATED_AT` datetime DEFAULT NULL,
  `UPDATED_BY` int(10) DEFAULT NULL,
  PRIMARY KEY (`VOUCHER_NO`),
  KEY `FK_VM_TE_APP_ID` (`TE_APP_ID`),
  KEY `FK_VM_USER_ID_idx` (`CITIZEN_ID`),
  KEY `LEASE_DE_ID` (`LEASE_DE_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=153 ;

--
-- Dumping data for table `ac_vouchermst`
--

INSERT INTO `ac_vouchermst` (`VOUCHER_NO`, `VOUCHER_DT`, `CITIZEN_ID`, `TE_APP_ID`, `LEASE_DE_ID`, `VCANCEL_FG`, `REMARKS`, `IS_ACTIVE`, `CREATED_AT`, `CREATED_BY`, `UPDATED_AT`, `UPDATED_BY`) VALUES
(96, '2016-11-13', NULL, 263, NULL, NULL, NULL, 1, '2016-11-12 22:03:56', NULL, '2016-11-13 04:03:56', NULL),
(97, '2016-11-13', NULL, 263, NULL, NULL, NULL, 1, '2016-11-12 23:44:33', NULL, '2016-11-13 05:44:33', NULL),
(98, '2016-11-13', NULL, 263, NULL, NULL, NULL, 1, '2016-11-13 00:15:14', NULL, '2016-11-13 06:15:14', NULL),
(99, '2016-11-13', NULL, 263, NULL, NULL, NULL, 1, '2016-11-13 04:28:47', 1, '2016-11-13 10:28:47', NULL),
(101, '2016-11-13', NULL, 263, NULL, NULL, NULL, 1, '2016-11-13 05:38:10', 1, '2016-11-13 11:38:10', NULL),
(102, '2016-11-13', NULL, 263, NULL, NULL, NULL, 1, '2016-11-13 06:14:57', NULL, '2016-11-13 12:14:57', NULL),
(103, '2016-11-13', NULL, 264, NULL, NULL, NULL, 1, '2016-11-13 06:29:11', NULL, '2016-11-13 12:29:11', NULL),
(105, '2016-11-14', 7, NULL, NULL, NULL, NULL, 1, '2016-11-14 00:05:42', NULL, '2016-11-14 06:05:42', NULL),
(106, '2016-11-14', NULL, 263, NULL, NULL, NULL, 1, '2016-11-14 03:49:41', 1, '2016-11-14 09:49:41', NULL),
(107, '2016-11-14', 7, NULL, NULL, NULL, NULL, 1, '2016-11-14 04:30:12', 1, '2016-11-14 10:30:12', NULL),
(108, '2016-11-14', NULL, 264, NULL, NULL, NULL, 1, '2016-11-14 04:41:52', NULL, '2016-11-14 10:41:52', NULL),
(109, '2016-11-14', NULL, 264, NULL, NULL, NULL, 1, '2016-11-14 04:43:07', 1, '2016-11-14 10:43:07', NULL),
(110, '2016-11-14', NULL, 264, NULL, NULL, NULL, 1, '2016-11-14 07:39:29', 1, '2016-11-14 13:39:29', NULL),
(111, '2016-11-15', 7, NULL, NULL, NULL, NULL, 1, '2016-11-14 22:51:57', NULL, '2016-11-15 04:51:57', NULL),
(112, '2016-11-15', 7, NULL, NULL, NULL, NULL, 1, '2016-11-14 22:53:13', 1, '2016-11-15 04:53:13', NULL),
(113, '2016-11-21', 7, NULL, NULL, NULL, NULL, 1, '2016-11-21 06:34:27', NULL, '2016-11-21 12:34:27', NULL),
(114, '2016-11-22', 7, NULL, NULL, NULL, NULL, 1, '2016-11-22 03:05:37', NULL, '2016-11-22 09:05:37', NULL),
(115, '2016-11-23', NULL, 265, NULL, NULL, NULL, 1, '2016-11-23 02:03:40', NULL, '2016-11-23 08:03:40', NULL),
(116, '2016-11-23', NULL, 265, NULL, NULL, NULL, 1, '2016-11-23 02:20:17', NULL, '2016-11-23 08:20:17', NULL),
(117, '2016-11-23', NULL, 265, NULL, NULL, NULL, 1, '2016-11-23 06:02:26', 7, '2016-11-23 12:02:26', NULL),
(118, '2016-12-01', NULL, NULL, 15, NULL, NULL, 1, '2016-11-26 06:01:53', 6, '2016-11-26 12:01:53', NULL),
(119, '2016-11-28', 7, NULL, NULL, NULL, NULL, 1, '2016-11-28 03:33:32', NULL, '2016-11-28 09:33:32', NULL),
(120, '2016-11-30', 7, NULL, NULL, NULL, NULL, 1, '2016-11-30 02:16:49', 1, '2016-11-30 08:16:49', NULL),
(121, '2016-12-07', NULL, 266, NULL, NULL, NULL, 1, '2016-12-07 00:22:03', NULL, '2016-12-07 06:22:03', NULL),
(122, '2016-12-07', NULL, 267, NULL, NULL, NULL, 1, '2016-12-07 00:26:03', NULL, '2016-12-07 06:26:03', NULL),
(123, '2016-12-07', NULL, 267, NULL, NULL, NULL, 1, '2016-12-07 00:28:42', 1, '2016-12-07 06:28:42', NULL),
(124, '2016-12-07', NULL, 267, NULL, NULL, NULL, 1, '2016-12-07 00:30:25', 1, '2016-12-07 06:30:25', NULL),
(125, '2016-12-07', NULL, 267, NULL, NULL, NULL, 1, '2016-12-07 00:36:49', 1, '2016-12-07 06:36:49', NULL),
(126, '2016-12-07', NULL, 267, NULL, NULL, NULL, 1, '2016-12-07 00:44:39', 1, '2016-12-07 06:44:39', NULL),
(127, '2016-12-12', NULL, 268, NULL, NULL, NULL, 1, '2016-12-11 23:51:18', NULL, '2016-12-12 05:51:18', NULL),
(128, '2016-12-12', NULL, 268, NULL, NULL, NULL, 1, '2016-12-11 23:53:40', 1, '2016-12-12 05:53:40', NULL),
(129, '2016-12-17', 7, NULL, NULL, NULL, NULL, 1, '2016-12-17 02:46:10', NULL, '2016-12-17 08:46:10', NULL),
(130, '2016-12-21', 7, NULL, NULL, NULL, NULL, 1, '2016-12-21 00:53:05', NULL, '2016-12-21 06:53:05', NULL),
(131, '2016-12-21', NULL, 265, NULL, NULL, NULL, 1, '2016-12-21 04:39:48', 6, '2016-12-21 10:39:48', NULL),
(132, '2016-12-26', NULL, 269, NULL, NULL, NULL, 1, '2016-12-26 06:36:16', NULL, '2016-12-26 12:36:16', NULL),
(133, '2016-12-26', NULL, 269, NULL, NULL, NULL, 1, '2016-12-26 06:43:14', 1, '2016-12-26 12:43:14', NULL),
(134, '2017-01-24', NULL, 270, NULL, NULL, NULL, 1, '2017-01-23 23:23:09', NULL, '2017-01-24 05:23:09', NULL),
(135, '2017-01-29', 7, NULL, NULL, NULL, NULL, 1, '2017-01-29 02:19:51', NULL, '2017-01-29 08:19:51', NULL),
(136, '2017-01-29', 7, NULL, NULL, NULL, NULL, 1, '2017-01-29 02:19:51', NULL, '2017-01-29 08:19:51', NULL),
(137, '2017-01-31', 7, NULL, NULL, NULL, NULL, 1, '2017-01-31 02:27:06', NULL, '2017-01-31 08:27:06', NULL),
(138, '2017-03-08', NULL, 271, NULL, NULL, NULL, 1, '2017-03-08 00:40:35', NULL, '2017-03-08 06:40:35', NULL),
(139, '2017-03-12', NULL, 274, NULL, NULL, NULL, 1, '2017-03-12 01:27:29', NULL, '2017-03-12 07:27:29', NULL),
(140, '2017-03-29', NULL, 276, NULL, NULL, NULL, 1, '2017-03-29 11:18:40', NULL, '2017-03-29 17:18:40', NULL),
(141, '2017-04-11', 7, NULL, NULL, NULL, NULL, 1, '2017-04-11 11:46:07', NULL, '2017-04-11 17:46:07', NULL),
(142, '2017-04-11', NULL, 277, NULL, NULL, NULL, 1, '2017-04-11 11:52:11', NULL, '2017-04-11 17:52:11', NULL),
(143, '2017-04-11', NULL, 277, NULL, NULL, NULL, 1, '2017-04-11 11:55:36', 1, '2017-04-11 17:55:36', NULL),
(144, '2017-04-11', 7, NULL, NULL, NULL, NULL, 1, '2017-04-11 12:30:32', NULL, '2017-04-11 18:30:32', NULL),
(146, '2017-04-15', 7, NULL, NULL, NULL, NULL, 1, '2017-04-14 19:38:58', NULL, '2017-04-15 01:38:58', NULL),
(147, '2017-04-15', NULL, 266, NULL, NULL, NULL, 1, '2017-04-15 04:11:22', 1, '2017-04-15 10:11:22', NULL),
(148, '2017-04-15', NULL, 278, NULL, NULL, NULL, 1, '2017-04-15 05:07:10', NULL, '2017-04-15 11:07:10', NULL),
(149, '2017-04-15', 7, NULL, NULL, NULL, NULL, 1, '2017-04-15 05:37:12', NULL, '2017-04-15 11:37:12', NULL),
(150, '2017-04-16', NULL, 279, NULL, NULL, NULL, 1, '2017-04-16 02:47:17', NULL, '2017-04-16 08:47:17', NULL),
(151, '2017-04-16', NULL, 267, NULL, NULL, NULL, 1, '2017-04-16 02:51:44', NULL, '2017-04-16 08:51:44', NULL),
(152, '2017-04-16', NULL, 267, NULL, NULL, NULL, 1, '2017-04-16 03:02:44', NULL, '2017-04-16 09:02:44', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `applicant_property`
--

CREATE TABLE IF NOT EXISTS `applicant_property` (
  `APP_PRO_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `TE_APP_ID` bigint(20) NOT NULL COMMENT 'Primary key of tender_applicant table',
  `TENDER_ID` bigint(20) NOT NULL COMMENT 'Primary key of tender table',
  `SCHEDULE_ID` bigint(20) NOT NULL COMMENT 'Primary key of tender_schedule table',
  `PROJECT_ID` bigint(20) NOT NULL COMMENT 'Primary key of project table',
  `PR_DETAIL_ID` bigint(20) DEFAULT NULL COMMENT 'Primary key of project_details table',
  `PR_CATEGORY` tinyint(2) DEFAULT NULL COMMENT 'primary key of sa_category table',
  `PR_TYPE` tinyint(2) DEFAULT NULL COMMENT 'Primary key of sa_type table',
  `IS_TE_SC_BOUGHT` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0=no, 1=yes, 2=approved',
  `TE_SC_AMOUNT` double DEFAULT NULL COMMENT 'Tender Schedule Amount',
  `TE_SC_DATE` datetime DEFAULT NULL COMMENT 'Tender Schedule Buying Date',
  `BID_AMOUNT` double DEFAULT NULL COMMENT 'Propossed Bid Amount Submitted by Applicant',
  `BID_AMOUNT_TEXT` varchar(100) DEFAULT NULL COMMENT 'Propossed Bid Amount in text Submitted by Applicant',
  `BG_AMOUNT` double DEFAULT NULL COMMENT 'This may be the Tender Money or Security Money. This will be calculated in Percentage',
  `BG_AMOUNT_TEXT` varchar(100) DEFAULT NULL COMMENT 'Tender Money or Security Money in Text',
  `BG_PAYMENT_TYPE` tinyint(1) NOT NULL COMMENT '1=bank pay order, 2=online',
  `BG_PAYMENT_STATUS` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0=pending, 1=approved',
  `BANK_ID` tinyint(3) DEFAULT NULL COMMENT 'Primary Key of bank table',
  `B_DRAFT_NO` varchar(150) DEFAULT NULL COMMENT 'Draft Number',
  `B_DRAFT_DATE` datetime DEFAULT NULL COMMENT 'Draft Number Submission date',
  `B_DRAFT_ATTACHMENT` varchar(100) DEFAULT NULL COMMENT 'Draft as attachment',
  `REMARKS` text COMMENT 'Comments',
  `INITIAL_REMARKS` text COMMENT 'Initial selected remarks',
  `CANCEL_REMARKS` text COMMENT 'Remarks for cancel tender application',
  `IS_SELECTED` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0 = Determine that the applicant is not selected, 1 = Initial selected, 2 = Finally selected.',
  `IS_LEASE` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0=no, 1=yes',
  `IS_CANCEL` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0=active=1=cancel tender application',
  `IS_CITIZEN` tinyint(1) NOT NULL DEFAULT '0',
  `IS_VIEW` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Determine if view notification',
  `IS_ACTIVE` tinyint(1) NOT NULL DEFAULT '1' COMMENT '0=no, 1=yes',
  `CREATED_AT` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `CREATED_BY` bigint(20) NOT NULL,
  `UPDATED_AT` datetime DEFAULT NULL,
  `UPDATED_BY` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`APP_PRO_ID`),
  KEY `TE_APP_ID` (`TE_APP_ID`),
  KEY `TENDER_ID` (`TENDER_ID`),
  KEY `SCHEDULE_ID` (`SCHEDULE_ID`),
  KEY `PR_DETAIL_ID` (`PR_DETAIL_ID`),
  KEY `PR_CATEGORY` (`PR_CATEGORY`),
  KEY `PR_TYPE` (`PR_TYPE`),
  KEY `PROJECT_ID` (`PROJECT_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=39 ;

--
-- Dumping data for table `applicant_property`
--

INSERT INTO `applicant_property` (`APP_PRO_ID`, `TE_APP_ID`, `TENDER_ID`, `SCHEDULE_ID`, `PROJECT_ID`, `PR_DETAIL_ID`, `PR_CATEGORY`, `PR_TYPE`, `IS_TE_SC_BOUGHT`, `TE_SC_AMOUNT`, `TE_SC_DATE`, `BID_AMOUNT`, `BID_AMOUNT_TEXT`, `BG_AMOUNT`, `BG_AMOUNT_TEXT`, `BG_PAYMENT_TYPE`, `BG_PAYMENT_STATUS`, `BANK_ID`, `B_DRAFT_NO`, `B_DRAFT_DATE`, `B_DRAFT_ATTACHMENT`, `REMARKS`, `INITIAL_REMARKS`, `CANCEL_REMARKS`, `IS_SELECTED`, `IS_LEASE`, `IS_CANCEL`, `IS_CITIZEN`, `IS_VIEW`, `IS_ACTIVE`, `CREATED_AT`, `CREATED_BY`, `UPDATED_AT`, `UPDATED_BY`) VALUES
(18, 263, 49, 128, 64, 72, 22, 10, 1, 2000, '2016-11-13 18:02:25', 12000, 'বার হাজার টাকা মাত্র', 3000, 'তিন হাজার টাকা মাত্র', 1, 0, 8, '১৫৫৪২৩২৮', '2016-11-02 00:00:00', 'bank-5827e6227572f.jpg', NULL, '', NULL, 2, 0, 0, 0, 0, 1, '2016-11-12 22:03:56', 0, '2016-11-16 06:58:46', 1),
(19, 263, 48, 124, 66, NULL, 24, NULL, 0, 500, NULL, 12000, 'বার হাজার টাকা মাত্র', 12000, 'বার হাজার টাকা মাত্র', 2, 0, NULL, NULL, NULL, NULL, NULL, '', NULL, 2, 1, 0, 0, 0, 0, '2016-11-12 23:44:33', 0, '2016-11-13 12:48:56', 1),
(20, 263, 48, 123, 65, NULL, 24, NULL, 0, 500, NULL, 12000, 'বার হাজার টাকা মাত্র', 12000, 'বার হাজার টাকা মাত্র', 1, 0, 10, '12546', '2016-11-03 00:00:00', 'bank-582804eea8b84.jpg', NULL, NULL, NULL, 0, 1, 0, 0, 0, 1, '2016-11-13 00:15:14', 0, '2016-11-13 06:15:14', NULL),
(22, 264, 48, 124, 66, NULL, 24, NULL, 1, 500, '2016-11-13 18:30:43', 550500, 'পাঁচ লাখ পঞ্চাশ হাজার পাঁচশত টাকা', 550500, 'পাঁচ লাখ পঞ্চাশ হাজার পাঁচশত টাকা', 2, 0, NULL, NULL, NULL, NULL, NULL, '', NULL, 2, 1, 0, 0, 0, 0, '2016-11-13 06:29:11', 0, '2016-11-14 13:42:31', 1),
(23, 264, 49, 128, 64, 72, 22, 10, 0, 2000, NULL, 12000, 'বার হাজার টাকা মাত্র', 3000, 'তিন হাজার টাকা মাত্র', 2, 0, NULL, NULL, NULL, NULL, NULL, '', NULL, 1, 0, 0, 0, 0, 1, '2016-11-14 04:41:52', 0, '2016-11-14 10:43:07', 1),
(24, 265, 50, 137, 64, 73, 23, 10, 1, 2500, '2016-11-23 14:11:44', 12000, 'বার হাজার টাকা মাত্র', 3360, 'তিন হাজার টাকা মাত্র', 2, 0, NULL, NULL, NULL, NULL, NULL, '', NULL, 1, 0, 0, 0, 0, 1, '2016-11-23 02:03:40', 0, '2016-11-23 12:02:26', 7),
(25, 265, 53, 136, 66, NULL, 24, NULL, 0, 500, NULL, 12000, 'বার হাজার টাকা মাত্র', 12000, 'বার হাজার টাকা মাত্র', 1, 0, 8, '343432', '2016-11-21 00:00:00', 'bank-5835511a88e5d.jpeg', NULL, '', NULL, 2, 1, 0, 0, 0, 0, '2016-11-23 02:20:17', 0, '2016-12-21 11:05:26', 6),
(26, 266, 53, 134, 63, NULL, 24, NULL, 0, 500, NULL, 120, 'বার হাজার টাকা মাত্র', 120, 'তিন হাজার টাকা মাত্র', 2, 0, NULL, NULL, NULL, NULL, NULL, '', NULL, 1, 1, 0, 0, 0, 1, '2016-12-07 00:22:03', 0, '2017-04-15 10:11:22', 1),
(27, 267, 54, 139, 64, 70, 21, 10, 0, 4500, NULL, 12000, 'বার হাজার টাকা মাত্র', 3600, 'তিন হাজার টাকা মাত্র', 2, 0, NULL, NULL, NULL, NULL, NULL, '', NULL, 1, 0, 0, 0, 0, 1, '2016-12-07 00:26:03', 0, '2016-12-07 06:44:39', 1),
(28, 268, 54, 141, 64, 72, 22, 10, 0, 2000, NULL, 12000, 'বার হাজার টাকা মাত্র', 3000, 'বার হাজার টাকা মাত্র', 2, 0, NULL, NULL, NULL, NULL, NULL, '', NULL, 1, 0, 0, 0, 0, 1, '2016-12-11 23:51:18', 0, '2016-12-12 05:53:39', 1),
(29, 269, 56, 144, 64, 70, 21, 10, 0, 4500, NULL, 10000000, 'One Crore Only', 3000000, 'One Crore Only', 2, 0, NULL, NULL, NULL, NULL, NULL, '', NULL, 1, 0, 0, 0, 0, 1, '2016-12-26 06:36:16', 0, '2016-12-26 12:43:14', 1),
(30, 270, 54, 138, 64, 69, 21, 11, 0, 4500, NULL, 12000, 'বার হাজার টাকা মাত্র', 3600, 'বার হাজার টাকা মাত্র', 1, 0, 10, '655656565', '2017-01-23 00:00:00', 'bank-5886d841531f0.jpg', NULL, NULL, NULL, 0, 0, 0, 0, 0, 1, '2017-01-23 23:23:09', 0, '2017-01-24 05:23:09', NULL),
(31, 271, 54, 138, 64, 69, 21, 11, 0, 4500, NULL, 1000000, 'Ten lak TH', 300000, 'thirty thousand th only', 2, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 1, '2017-03-08 00:40:34', 0, '2017-03-08 06:40:34', NULL),
(32, 274, 54, 139, 64, 70, 21, 10, 0, 4500, NULL, 500.1, 'xzxczxc', 150, 'cvcv', 2, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 1, '2017-03-12 01:27:29', 0, '2017-03-12 07:27:29', NULL),
(33, 276, 54, 138, 64, 69, 21, 11, 0, 4500, NULL, 12000, '12000', 3600, '3600', 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 1, '2017-03-29 11:18:40', 0, '2017-03-29 17:18:40', NULL),
(34, 277, 54, 139, 64, 70, 21, 10, 0, 4500, NULL, 12000, 'Twelve thousand taka ', 3600, 'dfdf', 0, 0, NULL, NULL, NULL, NULL, NULL, '', NULL, 1, 0, 0, 0, 0, 1, '2017-04-11 11:52:11', 0, '2017-04-11 17:55:36', 1),
(35, 278, 54, 139, 64, 70, 21, 10, 0, 4500, NULL, 12000, '12000', 3600, '3600', 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 1, '2017-04-15 05:07:10', 0, '2017-04-15 11:07:10', NULL),
(36, 279, 54, 138, 64, 69, 21, 11, 0, 4500, NULL, 1500, '(কথায় লিখুন) ', 450, '(কথায় লিখুন) ', 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 1, '2017-04-16 02:47:17', 0, '2017-04-16 08:47:17', NULL),
(37, 267, 54, 145, 64, 73, 23, 10, 0, 2500, NULL, 15963, '(কথায় লিখুন)', 4470, '(কথায় লিখুন)', 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 1, '2017-04-16 02:51:43', 0, '2017-04-16 08:51:43', NULL),
(38, 267, 54, 139, 64, 70, 21, 10, 0, 4500, NULL, 555, '111', 167, '655665', 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 0, 1, '2017-04-16 03:02:44', 0, '2017-04-16 09:02:44', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `land`
--

CREATE TABLE IF NOT EXISTS `land` (
  `LAND_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `LAND_NUMBER` varchar(150) CHARACTER SET utf8 NOT NULL,
  `LAND_TITLE` varchar(300) COLLATE utf8_unicode_ci DEFAULT NULL,
  `LAND_OWNER` tinyint(2) NOT NULL,
  `AREA` int(10) NOT NULL,
  `WARD_ID` int(10) NOT NULL,
  `MOUZA_ID` int(10) NOT NULL,
  `OUSTED_LAND` float NOT NULL,
  `POSSESSION_LAND` float NOT NULL,
  `TOTAL_LAND` float NOT NULL,
  `DETAILS` text CHARACTER SET utf8 NOT NULL,
  `REMARKS` text CHARACTER SET utf8,
  `IS_ACTIVE` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0=NO, 1=YES',
  `CREATED_BY` int(11) NOT NULL,
  `CREATED_AT` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `UPDATED_AT` datetime DEFAULT NULL,
  `UPDATED_BY` tinyint(1) DEFAULT NULL COMMENT 'Title of the Land',
  PRIMARY KEY (`LAND_ID`),
  KEY `FK_LAND_WARD_ID` (`WARD_ID`),
  KEY `FK_LAND_MOUZA_ID` (`MOUZA_ID`),
  KEY `FK_AREA` (`AREA`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=28 ;

--
-- Dumping data for table `land`
--

INSERT INTO `land` (`LAND_ID`, `LAND_NUMBER`, `LAND_TITLE`, `LAND_OWNER`, `AREA`, `WARD_ID`, `MOUZA_ID`, `OUSTED_LAND`, `POSSESSION_LAND`, `TOTAL_LAND`, `DETAILS`, `REMARKS`, `IS_ACTIVE`, `CREATED_BY`, `CREATED_AT`, `UPDATED_AT`, `UPDATED_BY`) VALUES
(1, 'সিদ্ধিরগাঞ্জ/6578/232', '', 1, 13, 19, 13, 5, 23, 28, '<p>Good land</p>', '<p>okay</p>', 1, 1, '2017-02-01 05:40:13', '2016-07-21 11:47:10', NULL),
(3, 'আজিবপুর/5678/234', '', 1, 13, 18, 12, 5, 23, 28, '<p>Good land</p>', '<p>okay</p>', 1, 1, '2017-02-01 05:40:13', '2016-07-21 11:55:30', NULL),
(4, 'চাসাড়া/5548/2314', '', 1, 13, 19, 13, 5, 22, 28, '<p>Average</p>', 'sf', 1, 1, '2017-02-01 05:40:13', '2016-08-06 04:35:38', NULL),
(5, 'শিমরাইল/3456/2323', 'NCC land', 1, 13, 16, 10, 56, 45, 101, '<p>good land</p>', '', 1, 1, '2017-02-01 05:40:13', '2016-08-08 04:55:53', NULL),
(6, 'শিমরাইল/3456/123', 'জমির নাম ', 1, 13, 16, 10, 20, 50, 70, '<p>\r\n	                        <strong>নারায়ণগঞ্জ সিটি করপোরেশন</strong> \r\nনারায়ণগঞ্জ পৌরসভাকে (মিউনিসিপ্যালিটি) ২০১১ সালের ৫ মে নারায়ণগঞ্জ সিটি \r\nকরপোরেশনে রূপান্তর করা হয়। নারায়ণগঞ্জ শহর, সিদ্ধিরগঞ্জ পৌর এলাকা এবং \r\nবন্দর উপজেলার কমদরসূল পৌরসভাকে নিয়ে নারায়ণগঞ্জ সিটি করপোরেশন গঠিত হয়।</p><p>\r\n	                        সিদ্ধিরগঞ্জ পৌরসভাকে বিলুপ্ত করে এর ১ থেকে ৯ \r\nনম্বর ওয়ার্ডকে সিটি করপোরেশনের ১ থেকে ৯ নম্বর ওয়ার্ড হিসাবে ঘোষণা করা \r\nহয়েছে। সিদ্ধিরগঞ্জের পাইনাদী পূর্ব অংশ মিজমিজি বাতান পাড়াকে ১ নম্বর \r\nওয়ার্ড করা হয়েছে। ১ থেকে ৩ নম্বর ওয়ার্ডকে সংরক্ষিত মহিলা আসন হিসেবে \r\nতালিকা ভুক্ত করা হয়েছে। নারায়ণগঞ্জ পৌরসভা বিলুপ্ত করে এর ৯টি ওয়ার্ডকে\r\n সিটি করপোরেশনের ১০ থেকে ১৮ নম্বর ওয়ার্ড হিসেবে তালিকাভুক্ত করা হয়েছে।\r\n এই পৌরসভার সংরক্ষিত তিনটি মহিলা ওয়ার্ডকে ৪ থেকে ৬ নম্বর ওয়ার্ড হিসাবে\r\n ঘোষণা করা হয়। বন্দর উপজেলার কদম রসূল পৌরসভাকে বিলুপ্ত করে এর ৯টি \r\nওয়ার্ডকে সিটি করপোরেশনের ১৯ থেকে ২৭ নম্বর ওয়ার্ড হিসেবে তালিকাভুক্ত \r\nকরা হয়। এই পৌরসভার তিনাট মহিলা ওয়ার্ডকে সিটি করপোরেশনের ৭ থেকে ৯ নম্বর\r\n ওয়ার্ড হিসাবে ঘোষণা করা হয়।</p>', '<p><strong>নারায়ণগঞ্জ সিটি করপোরেশন</strong> \r\nনারায়ণগঞ্জ পৌরসভাকে (মিউনিসিপ্যালিটি) ২০১১ সালের ৫ মে নারায়ণগঞ্জ সিটি \r\nকরপোরেশনে রূপান্তর করা হয়। নারায়ণগঞ্জ শহর, সিদ্ধিরগঞ্জ পৌর এলাকা এবং \r\nবন্দর উপজেলার কমদরসূল পৌরসভাকে নিয়ে নারায়ণগঞ্জ সিটি করপোরেশন গঠিত হয়।</p>', 1, 1, '2017-02-01 05:40:13', '2016-08-23 06:59:15', 1),
(7, 'সিদ্ধিরগাঞ্জ/6578/123', 'জমির নাম', 1, 13, 19, 13, 20, 50, 70, '<p>\r\n	                        <strong>নারায়ণগঞ্জ সিটি করপোরেশন</strong> \r\nনারায়ণগঞ্জ পৌরসভাকে (মিউনিসিপ্যালিটি) ২০১১ সালের ৫ মে নারায়ণগঞ্জ সিটি \r\nকরপোরেশনে রূপান্তর করা হয়। নারায়ণগঞ্জ শহর, সিদ্ধিরগঞ্জ পৌর এলাকা এবং \r\nবন্দর উপজেলার কমদরসূল পৌরসভাকে নিয়ে নারায়ণগঞ্জ সিটি করপোরেশন গঠিত হয়।</p><p>\r\n	                        সিদ্ধিরগঞ্জ পৌরসভাকে বিলুপ্ত করে এর ১ থেকে ৯ \r\nনম্বর ওয়ার্ডকে সিটি করপোরেশনের ১ থেকে ৯ নম্বর ওয়ার্ড হিসাবে ঘোষণা করা \r\nহয়েছে। সিদ্ধিরগঞ্জের পাইনাদী পূর্ব অংশ মিজমিজি বাতান পাড়াকে ১ নম্বর \r\nওয়ার্ড করা হয়েছে। ১ থেকে ৩ নম্বর ওয়ার্ডকে সংরক্ষিত মহিলা আসন হিসেবে \r\nতালিকা ভুক্ত করা হয়েছে। নারায়ণগঞ্জ পৌরসভা বিলুপ্ত করে এর ৯টি ওয়ার্ডকে\r\n সিটি করপোরেশনের ১০ থেকে ১৮ নম্বর ওয়ার্ড হিসেবে তালিকাভুক্ত করা হয়েছে।\r\n এই পৌরসভার সংরক্ষিত তিনটি মহিলা ওয়ার্ডকে ৪ থেকে ৬ নম্বর ওয়ার্ড হিসাবে\r\n ঘোষণা করা হয়। বন্দর উপজেলার কদম রসূল পৌরসভাকে বিলুপ্ত করে এর ৯টি \r\nওয়ার্ডকে সিটি করপোরেশনের ১৯ থেকে ২৭ নম্বর ওয়ার্ড হিসেবে তালিকাভুক্ত \r\nকরা হয়। এই পৌরসভার তিনাট মহিলা ওয়ার্ডকে সিটি করপোরেশনের ৭ থেকে ৯ নম্বর\r\n ওয়ার্ড হিসাবে ঘোষণা করা হয়।</p>', '<p>\r\n	                        <strong>নারায়ণগঞ্জ সিটি করপোরেশন</strong> \r\nনারায়ণগঞ্জ পৌরসভাকে (মিউনিসিপ্যালিটি) ২০১১ সালের ৫ মে নারায়ণগঞ্জ সিটি \r\nকরপোরেশনে রূপান্তর করা হয়। নারায়ণগঞ্জ শহর, সিদ্ধিরগঞ্জ পৌর এলাকা এবং \r\nবন্দর উপজেলার কমদরসূল পৌরসভাকে নিয়ে নারায়ণগঞ্জ সিটি করপোরেশন গঠিত হয়।</p>', 1, 1, '2017-02-01 05:40:13', '2016-08-09 04:01:26', NULL),
(8, 'আজিবপুর/5678/123', 'জমির নাম', 1, 13, 18, 12, 20, 50, 70, '<p>\r\n	                        <strong>নারায়ণগঞ্জ সিটি করপোরেশন</strong> \r\nনারায়ণগঞ্জ পৌরসভাকে (মিউনিসিপ্যালিটি) ২০১১ সালের ৫ মে নারায়ণগঞ্জ সিটি \r\nকরপোরেশনে রূপান্তর করা হয়। নারায়ণগঞ্জ শহর, সিদ্ধিরগঞ্জ পৌর এলাকা এবং \r\nবন্দর উপজেলার কমদরসূল পৌরসভাকে নিয়ে নারায়ণগঞ্জ সিটি করপোরেশন গঠিত হয়।</p><p>\r\n	                        সিদ্ধিরগঞ্জ পৌরসভাকে বিলুপ্ত করে এর ১ থেকে ৯ \r\nনম্বর ওয়ার্ডকে সিটি করপোরেশনের ১ থেকে ৯ নম্বর ওয়ার্ড হিসাবে ঘোষণা করা \r\nহয়েছে। সিদ্ধিরগঞ্জের পাইনাদী পূর্ব অংশ মিজমিজি বাতান পাড়াকে ১ নম্বর \r\nওয়ার্ড করা হয়েছে। ১ থেকে ৩ নম্বর ওয়ার্ডকে সংরক্ষিত মহিলা আসন হিসেবে \r\nতালিকা ভুক্ত করা হয়েছে। নারায়ণগঞ্জ পৌরসভা বিলুপ্ত করে এর ৯টি ওয়ার্ডকে\r\n সিটি করপোরেশনের ১০ থেকে ১৮ নম্বর ওয়ার্ড হিসেবে তালিকাভুক্ত করা হয়েছে।\r\n এই পৌরসভার সংরক্ষিত তিনটি মহিলা ওয়ার্ডকে ৪ থেকে ৬ নম্বর ওয়ার্ড হিসাবে\r\n ঘোষণা করা হয়। বন্দর উপজেলার কদম রসূল পৌরসভাকে বিলুপ্ত করে এর ৯টি \r\nওয়ার্ডকে সিটি করপোরেশনের ১৯ থেকে ২৭ নম্বর ওয়ার্ড হিসেবে তালিকাভুক্ত \r\nকরা হয়। এই পৌরসভার তিনাট মহিলা ওয়ার্ডকে সিটি করপোরেশনের ৭ থেকে ৯ নম্বর\r\n ওয়ার্ড হিসাবে ঘোষণা করা হয়।</p>', '<p>\r\n	                        <strong>নারায়ণগঞ্জ সিটি করপোরেশন</strong> \r\nনারায়ণগঞ্জ পৌরসভাকে (মিউনিসিপ্যালিটি) ২০১১ সালের ৫ মে নারায়ণগঞ্জ সিটি \r\nকরপোরেশনে রূপান্তর করা হয়। নারায়ণগঞ্জ শহর, সিদ্ধিরগঞ্জ পৌর এলাকা এবং \r\nবন্দর উপজেলার কমদরসূল পৌরসভাকে নিয়ে নারায়ণগঞ্জ সিটি করপোরেশন গঠিত হয়।</p>', 1, 1, '2017-02-01 05:40:13', '2016-08-09 04:01:44', NULL),
(9, 'শিমরাইল/3456/3434', 'জমির নাম', 1, 13, 16, 10, 56, 35, 91, '<p><label>জমির বিস্তারিত</label></p>', '<p>মন্তব্য</p>', 0, 1, '2017-02-01 05:40:13', '2016-08-23 06:53:34', NULL),
(12, 'সিদ্ধিরগাঞ্জ/6578/223', 'জমির নাম', 1, 13, 19, 13, 34, 34, 68, '<p>\r\n	                        <strong>নারায়ণগঞ্জ সিটি করপোরেশন</strong> \r\nনারায়ণগঞ্জ পৌরসভাকে (মিউনিসিপ্যালিটি) ২০১১ সালের ৫ মে নারায়ণগঞ্জ সিটি \r\nকরপোরেশনে রূপান্তর করা হয়। নারায়ণগঞ্জ শহর, সিদ্ধিরগঞ্জ পৌর এলাকা এবং \r\nবন্দর উপজেলার কমদরসূল পৌরসভাকে নিয়ে নারায়ণগঞ্জ সিটি করপোরেশন গঠিত হয়।</p><p>\r\n	                        সিদ্ধিরগঞ্জ পৌরসভাকে বিলুপ্ত করে এর ১ থেকে ৯ \r\nনম্বর ওয়ার্ডকে সিটি করপোরেশনের ১ থেকে ৯ নম্বর ওয়ার্ড হিসাবে ঘোষণা করা \r\nহয়েছে। সিদ্ধিরগঞ্জের পাইনাদী পূর্ব অংশ মিজমিজি বাতান পাড়াকে ১ নম্বর \r\nওয়ার্ড করা হয়েছে। ১ থেকে ৩ নম্বর ওয়ার্ডকে সংরক্ষিত মহিলা আসন হিসেবে \r\nতালিকা ভুক্ত করা হয়েছে। নারায়ণগঞ্জ পৌরসভা বিলুপ্ত করে এর ৯টি ওয়ার্ডকে\r\n সিটি করপোরেশনের ১০ থেকে ১৮ নম্বর ওয়ার্ড হিসেবে তালিকাভুক্ত করা হয়েছে।\r\n এই পৌরসভার সংরক্ষিত তিনটি মহিলা ওয়ার্ডকে ৪ থেকে ৬ নম্বর ওয়ার্ড হিসাবে\r\n ঘোষণা করা হয়। বন্দর উপজেলার কদম রসূল পৌরসভাকে বিলুপ্ত করে এর ৯টি \r\nওয়ার্ডকে সিটি করপোরেশনের ১৯ থেকে ২৭ নম্বর ওয়ার্ড হিসেবে তালিকাভুক্ত \r\nকরা হয়। এই পৌরসভার তিনাট মহিলা ওয়ার্ডকে সিটি করপোরেশনের ৭ থেকে ৯ নম্বর\r\n ওয়ার্ড হিসাবে ঘোষণা করা হয়।</p>', '<p "="">\r\n	                        <strong>নারায়ণগঞ্জ সিটি করপোরেশন</strong> \r\nনারায়ণগঞ্জ পৌরসভাকে (মিউনিসিপ্যালিটি) ২০১১ সালের ৫ মে নারায়ণগঞ্জ সিটি \r\nকরপোরেশনে রূপান্তর করা হয়। নারায়ণগঞ্জ শহর, সিদ্ধিরগঞ্জ পৌর এলাকা এবং \r\nবন্দর উপজেলার কমদরসূল পৌরসভাকে নিয়ে নারায়ণগঞ্জ সিটি করপোরেশন গঠিত হয়।</p>', 1, 1, '2017-02-01 05:40:13', '2016-08-23 11:05:44', 1),
(14, 'সিদ্ধিরগাঞ্জ/6578/344', 'জমির নাম', 1, 13, 19, 13, 24, 32, 56, '<p>\r\n	                        <strong>নারায়ণগঞ্জ সিটি করপোরেশন</strong> \r\nনারায়ণগঞ্জ পৌরসভাকে (মিউনিসিপ্যালিটি) ২০১১ সালের ৫ মে নারায়ণগঞ্জ সিটি \r\nকরপোরেশনে রূপান্তর করা হয়। নারায়ণগঞ্জ শহর, সিদ্ধিরগঞ্জ পৌর এলাকা এবং \r\nবন্দর উপজেলার কমদরসূল পৌরসভাকে নিয়ে নারায়ণগঞ্জ সিটি করপোরেশন গঠিত হয়।</p><p>\r\n	                        সিদ্ধিরগঞ্জ পৌরসভাকে বিলুপ্ত করে এর ১ থেকে ৯ \r\nনম্বর ওয়ার্ডকে সিটি করপোরেশনের ১ থেকে ৯ নম্বর ওয়ার্ড হিসাবে ঘোষণা করা \r\nহয়েছে। সিদ্ধিরগঞ্জের পাইনাদী পূর্ব অংশ মিজমিজি বাতান পাড়াকে ১ নম্বর \r\nওয়ার্ড করা হয়েছে। ১ থেকে ৩ নম্বর ওয়ার্ডকে সংরক্ষিত মহিলা আসন হিসেবে \r\nতালিকা ভুক্ত করা হয়েছে। নারায়ণগঞ্জ পৌরসভা বিলুপ্ত করে এর ৯টি ওয়ার্ডকে\r\n সিটি করপোরেশনের ১০ থেকে ১৮ নম্বর ওয়ার্ড হিসেবে তালিকাভুক্ত করা হয়েছে।\r\n এই পৌরসভার সংরক্ষিত তিনটি মহিলা ওয়ার্ডকে ৪ থেকে ৬ নম্বর ওয়ার্ড হিসাবে\r\n ঘোষণা করা হয়। বন্দর উপজেলার কদম রসূল পৌরসভাকে বিলুপ্ত করে এর ৯টি \r\nওয়ার্ডকে সিটি করপোরেশনের ১৯ থেকে ২৭ নম্বর ওয়ার্ড হিসেবে তালিকাভুক্ত \r\nকরা হয়। এই পৌরসভার তিনাট মহিলা ওয়ার্ডকে সিটি করপোরেশনের ৭ থেকে ৯ নম্বর\r\n ওয়ার্ড হিসাবে ঘোষণা করা হয়।</p>', '<p "="">\r\n	                        <strong>নারায়ণগঞ্জ সিটি করপোরেশন</strong> \r\nনারায়ণগঞ্জ পৌরসভাকে (মিউনিসিপ্যালিটি) ২০১১ সালের ৫ মে নারায়ণগঞ্জ সিটি \r\nকরপোরেশনে রূপান্তর করা হয়। নারায়ণগঞ্জ শহর, সিদ্ধিরগঞ্জ পৌর এলাকা এবং \r\nবন্দর উপজেলার কমদরসূল পৌরসভাকে নিয়ে নারায়ণগঞ্জ সিটি করপোরেশন গঠিত হয়।</p>', 1, 1, '2017-02-01 05:40:13', '2016-08-09 05:28:43', NULL),
(16, 'সিদ্ধিরগাঞ্জ/6578/344', 'জমির নাম', 1, 13, 19, 13, 24, 32, 56, '<p>\r\n	                        <strong>নারায়ণগঞ্জ সিটি করপোরেশন</strong> \r\nনারায়ণগঞ্জ পৌরসভাকে (মিউনিসিপ্যালিটি) ২০১১ সালের ৫ মে নারায়ণগঞ্জ সিটি \r\nকরপোরেশনে রূপান্তর করা হয়। নারায়ণগঞ্জ শহর, সিদ্ধিরগঞ্জ পৌর এলাকা এবং \r\nবন্দর উপজেলার কমদরসূল পৌরসভাকে নিয়ে নারায়ণগঞ্জ সিটি করপোরেশন গঠিত হয়।</p><p>\r\n	                        সিদ্ধিরগঞ্জ পৌরসভাকে বিলুপ্ত করে এর ১ থেকে ৯ \r\nনম্বর ওয়ার্ডকে সিটি করপোরেশনের ১ থেকে ৯ নম্বর ওয়ার্ড হিসাবে ঘোষণা করা \r\nহয়েছে। সিদ্ধিরগঞ্জের পাইনাদী পূর্ব অংশ মিজমিজি বাতান পাড়াকে ১ নম্বর \r\nওয়ার্ড করা হয়েছে। ১ থেকে ৩ নম্বর ওয়ার্ডকে সংরক্ষিত মহিলা আসন হিসেবে \r\nতালিকা ভুক্ত করা হয়েছে। নারায়ণগঞ্জ পৌরসভা বিলুপ্ত করে এর ৯টি ওয়ার্ডকে\r\n সিটি করপোরেশনের ১০ থেকে ১৮ নম্বর ওয়ার্ড হিসেবে তালিকাভুক্ত করা হয়েছে।\r\n এই পৌরসভার সংরক্ষিত তিনটি মহিলা ওয়ার্ডকে ৪ থেকে ৬ নম্বর ওয়ার্ড হিসাবে\r\n ঘোষণা করা হয়। বন্দর উপজেলার কদম রসূল পৌরসভাকে বিলুপ্ত করে এর ৯টি \r\nওয়ার্ডকে সিটি করপোরেশনের ১৯ থেকে ২৭ নম্বর ওয়ার্ড হিসেবে তালিকাভুক্ত \r\nকরা হয়। এই পৌরসভার তিনাট মহিলা ওয়ার্ডকে সিটি করপোরেশনের ৭ থেকে ৯ নম্বর\r\n ওয়ার্ড হিসাবে ঘোষণা করা হয়।</p>', '<p "="">\r\n	                        <strong>নারায়ণগঞ্জ সিটি করপোরেশন</strong> \r\nনারায়ণগঞ্জ পৌরসভাকে (মিউনিসিপ্যালিটি) ২০১১ সালের ৫ মে নারায়ণগঞ্জ সিটি \r\nকরপোরেশনে রূপান্তর করা হয়। নারায়ণগঞ্জ শহর, সিদ্ধিরগঞ্জ পৌর এলাকা এবং \r\nবন্দর উপজেলার কমদরসূল পৌরসভাকে নিয়ে নারায়ণগঞ্জ সিটি করপোরেশন গঠিত হয়।</p>', 1, 1, '2017-02-01 05:40:13', '2016-08-09 05:30:38', NULL),
(17, 'শিমরাইল/3456/123', 'জমির নাম', 1, 13, 16, 10, 50, 50, 100, '<p>জমির বিস্তারি</p>', '<p>মন্তব্য</p>', 1, 1, '2017-02-01 05:40:13', '2016-11-26 08:21:04', 1),
(21, 'শিমরাইল/3456/232', 'Test land', 1, 13, 16, 10, 45, 45, 90, '<p>egrgfg dfgd</p>', '', 1, 1, '2017-02-01 05:40:13', '2016-10-05 05:54:40', NULL),
(22, 'শিমরাইল/3456/2455', 'land name', 1, 13, 16, 10, 34, 44, 78, '<p>tyty tyty</p>', '<p>সেবে তালিকা ভুক্ত করা হয়েছে। নারায়ণগঞ্জ পৌরসভা বিলুপ্ত করে এর ৯টি \r\nওয়ার্ডকে সিটি করপোরেশনের ১০ থেকে ১৮ নম্বর ওয়ার্ড হিসেবে তালিকাভুক্ত \r\nকরা হয়েছে। এই পৌরসভার সংরক্ষিত তিনটি ম</p>', 1, 1, '2017-02-01 05:40:13', '2016-10-09 12:20:41', NULL),
(23, 'আজিবপুর/5678/2324', 'NCC land', 1, 13, 18, 12, 5, 50, 55, '<p>LAND DETAILS</p>', '', 1, 1, '2017-02-01 05:40:13', '2016-11-26 08:24:18', NULL),
(27, 'জোন- ১/ওয়ার্ড ০৩/আজিবপুর', NULL, 1, 13, 18, 12, 0, 100, 100, '<p>ghghg</p>', '', 1, 1, '2017-02-02 02:19:21', '2017-02-02 08:19:21', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `land_case`
--

CREATE TABLE IF NOT EXISTS `land_case` (
  `LAND_CASE_ID` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'Primary key',
  `LAND_ID` bigint(20) NOT NULL COMMENT 'Primary key of land table',
  `CASE_NUMBER` varchar(100) NOT NULL COMMENT 'Land case number',
  `COMPLAINANT` varchar(150) NOT NULL COMMENT 'Complainant name',
  `DEFENDANT` varchar(150) NOT NULL COMMENT 'Defendant name',
  `COURT_DOCUMENT` varchar(100) DEFAULT NULL COMMENT 'Court document',
  `SETTLEMENT_DATE` datetime NOT NULL COMMENT 'Litigation settlement date',
  `REMARKS` text COMMENT 'Comments if any',
  `IS_ACTIVE` tinyint(4) NOT NULL,
  `CREATED_AT` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `CREATED_BY` bigint(20) NOT NULL,
  `UPDATED_AT` datetime DEFAULT NULL,
  `UPDATED_BY` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`LAND_CASE_ID`),
  KEY `LAND_ID` (`LAND_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `land_case`
--

INSERT INTO `land_case` (`LAND_CASE_ID`, `LAND_ID`, `CASE_NUMBER`, `COMPLAINANT`, `DEFENDANT`, `COURT_DOCUMENT`, `SETTLEMENT_DATE`, `REMARKS`, `IS_ACTIVE`, `CREATED_AT`, `CREATED_BY`, `UPDATED_AT`, `UPDATED_BY`) VALUES
(3, 21, '1596', 'Md. Rohim', 'Md. Korim', 'dolil-57f495a0e2e5a.png', '2016-10-06 00:00:00', '<p>\r\n	                        <strong>??????????? ???? ????????</strong> \r\n??????????? ???????? (???????????????) ???? ????? ? ?? ??????????? ???? \r\n????????? ???????? ??? ???? ??????????? ???, ??????????? ??? ????? ??? \r\n????? ??????? ??????? ???????? ????? ??????????? ???? ???????? ???? ????</p>', 1, '2016-10-06 05:47:54', 1, NULL, NULL),
(4, 27, '67', 'ikklhkl', 'fu', 'court-5892ebce50f8b.jpg', '2017-02-14 00:00:00', '<p>kjk</p>', 1, '2017-02-02 02:20:30', 1, '2017-02-02 08:49:15', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `land_category`
--

CREATE TABLE IF NOT EXISTS `land_category` (
  `CAT_ID` int(10) NOT NULL AUTO_INCREMENT,
  `PARENT_ID` double NOT NULL DEFAULT '0',
  `CAT_NAME` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `CAT_NAME_BN` varchar(100) CHARACTER SET utf8 NOT NULL COMMENT 'Category Bangla Name',
  `IS_ACTIVE` tinyint(1) NOT NULL COMMENT '0=NO, 1=YES',
  `CREATED_BY` bigint(20) NOT NULL,
  `CREATED_AT` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `UPDATED_AT` datetime DEFAULT NULL,
  `UPDATED_BY` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`CAT_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=24 ;

--
-- Dumping data for table `land_category`
--

INSERT INTO `land_category` (`CAT_ID`, `PARENT_ID`, `CAT_NAME`, `CAT_NAME_BN`, `IS_ACTIVE`, `CREATED_BY`, `CREATED_AT`, `UPDATED_AT`, `UPDATED_BY`) VALUES
(1, 0, 'Revenue', 'রাজস্ব', 1, 4, '2016-07-17 08:09:12', '2016-07-17 08:09:07', 4),
(2, 0, 'Non revenue', 'অ রাজস্ব', 1, 4, '2016-09-28 06:01:10', '2016-09-28 06:01:08', 1),
(4, 2, 'Road', 'রাস্তা', 1, 4, '2016-07-17 09:23:07', '2016-07-17 09:23:02', 4),
(5, 1, 'Land', 'জমি', 1, 1, '2017-01-09 08:28:45', '2016-09-28 10:42:57', 1),
(13, 1, 'Resident', 'আবাসিক', 1, 1, '2017-01-31 23:48:30', '2017-02-01 05:48:30', NULL),
(14, 1, 'Commercial', 'বাণিজ্যিক', 1, 1, '2017-01-31 23:49:07', '2017-02-01 05:49:07', NULL),
(15, 1, 'Pond', 'পুকুর', 1, 1, '2017-01-31 23:49:32', '2017-02-01 05:49:32', NULL),
(16, 1, 'Lake', 'লেক', 1, 1, '2017-01-31 23:49:52', '2017-02-01 05:49:52', NULL),
(17, 1, 'Cemetery', 'কবরস্থান', 1, 1, '2017-01-31 23:50:20', '2017-02-01 05:50:20', NULL),
(18, 1, 'Trough', 'নালা', 1, 1, '2017-01-31 23:50:54', '2017-02-01 05:50:54', NULL),
(19, 1, 'Unused', 'অব্যবহৃত', 1, 1, '2017-01-31 23:51:31', '2017-02-01 05:51:31', NULL),
(20, 1, 'Other', 'অন্যান্য', 1, 1, '2017-01-31 23:51:50', '2017-02-01 05:51:50', NULL),
(21, 2, 'Drain', 'ড্রেন', 1, 1, '2017-01-31 23:52:46', '2017-02-01 05:52:46', NULL),
(22, 2, 'Park', 'পার্ক', 1, 1, '2017-01-31 23:53:02', '2017-02-01 05:53:02', NULL),
(23, 2, 'Other', 'অন্যান্য', 1, 1, '2017-02-01 08:20:08', '2017-02-01 08:20:06', 1);

-- --------------------------------------------------------

--
-- Table structure for table `land_dag_no`
--

CREATE TABLE IF NOT EXISTS `land_dag_no` (
  `DAG_ID` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'Primary key of land_dag table',
  `LAND_ID` bigint(20) NOT NULL COMMENT 'Primary key of land table',
  `RS` int(11) DEFAULT NULL COMMENT 'Land RS(Revisional survey) no',
  `CS` int(11) DEFAULT NULL COMMENT 'Land CS(Cadastral survey) no',
  `SA` int(11) DEFAULT NULL COMMENT 'Land SA(State Acquisition survey) no',
  `BS` int(11) DEFAULT NULL COMMENT 'Land BS(Bangladesh survey) no',
  `TYPE` tinyint(1) NOT NULL COMMENT '1=khatian, 2=dag, 3=JL, 4=Land owner',
  `CREATED_AT` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `CREATED_BY` int(10) NOT NULL,
  `UPDATED_AT` datetime DEFAULT NULL,
  `UPDATED_BY` int(10) DEFAULT NULL,
  PRIMARY KEY (`DAG_ID`),
  KEY `FK_DAG_LAND_ID` (`LAND_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=120 ;

--
-- Dumping data for table `land_dag_no`
--

INSERT INTO `land_dag_no` (`DAG_ID`, `LAND_ID`, `RS`, `CS`, `SA`, `BS`, `TYPE`, `CREATED_AT`, `CREATED_BY`, `UPDATED_AT`, `UPDATED_BY`) VALUES
(1, 1, 23, 12, 42, NULL, 2, '2016-10-03 12:31:11', 1, '2016-07-21 11:47:10', NULL),
(2, 1, 34, 65, 32, NULL, 2, '2016-10-03 12:32:41', 1, '2016-07-21 11:47:10', NULL),
(3, 3, 23, 12, 42, NULL, 2, '2016-10-03 12:32:41', 1, '2016-07-21 11:55:30', NULL),
(4, 3, 34, 65, 32, NULL, 2, '2016-10-03 12:32:41', 1, '2016-07-21 11:55:30', NULL),
(5, 5, 24, 65, 55, NULL, 2, '2016-10-03 12:32:41', 1, '2016-08-08 04:55:53', NULL),
(7, 6, 123, 111, 111, NULL, 2, '2016-10-03 12:32:41', 1, '2016-08-23 06:59:16', 1),
(8, 6, 444, 444, 444, NULL, 2, '2016-10-03 12:32:41', 1, '2016-08-23 06:59:16', 1),
(9, 7, 444, 444, 444, NULL, 2, '2016-10-03 12:32:41', 1, '2016-08-09 04:01:26', NULL),
(10, 7, 123, 111, 111, NULL, 2, '2016-10-03 12:32:41', 1, '2016-08-09 04:01:27', NULL),
(11, 8, 444, 444, 444, NULL, 2, '2016-10-03 12:32:41', 1, '2016-08-09 04:01:44', NULL),
(12, 8, 123, 111, 111, NULL, 2, '2016-10-03 12:32:41', 1, '2016-08-09 04:01:44', NULL),
(13, 9, 34, 56, 65, NULL, 2, '2016-10-03 12:32:41', 1, '2016-08-23 06:53:34', 1),
(14, 9, 45, 55, 76, NULL, 2, '2016-10-03 12:32:41', 1, '2016-08-23 06:53:34', 1),
(17, 12, 43, 32, 44, NULL, 2, '2016-10-03 12:32:41', 1, '2016-08-23 11:05:44', 1),
(18, 12, 34, 44, 45, NULL, 2, '2016-10-03 12:32:41', 1, '2016-08-23 11:05:44', 1),
(19, 14, 65, 55, 75, NULL, 2, '2016-10-03 12:32:41', 1, '2016-08-09 05:28:43', NULL),
(20, 14, 34, 54, 65, NULL, 2, '2016-10-03 12:32:41', 1, '2016-08-09 05:28:43', NULL),
(24, 12, 12, 13, 14, NULL, 2, '2016-10-03 12:32:41', 0, '2016-08-23 11:05:44', 1),
(25, 17, 444, 444, 444, NULL, 2, '2016-10-03 12:32:41', 1, '2016-10-01 05:57:47', 1),
(26, 17, 123, 111, 111, NULL, 2, '2016-10-03 12:32:41', 1, '2016-10-01 05:57:47', 1),
(70, 21, 232, 231, 242, NULL, 1, '2016-10-05 22:42:34', 1, '2016-10-06 04:42:34', NULL),
(71, 21, 232, 231, 242, NULL, 1, '2016-10-05 22:42:34', 1, '2016-10-06 04:42:34', NULL),
(78, 21, 564, 785, 865, NULL, 2, '2016-10-05 22:42:34', 1, '2016-10-06 04:42:34', NULL),
(79, 21, 232, 464, 342, NULL, 2, '2016-10-05 22:42:35', 1, '2016-10-06 04:42:35', NULL),
(86, 21, 444, 664, 764, NULL, 3, '2016-10-05 22:42:35', 1, '2016-10-06 04:42:35', NULL),
(87, 21, 532, 655, 87, NULL, 3, '2016-10-05 22:42:35', 1, '2016-10-06 04:42:35', NULL),
(91, 21, 454, 4523, 4544, NULL, 4, '2016-10-05 22:42:35', 1, '2016-10-06 04:42:35', NULL),
(92, 22, 34, 45, 56, NULL, 1, '2016-10-06 00:25:34', 1, '2016-10-06 06:25:34', NULL),
(93, 22, 34, 34, 46, NULL, 1, '2016-10-06 00:25:34', 1, '2016-10-06 06:25:34', NULL),
(94, 22, 56, 67, 24, NULL, 2, '2016-10-06 00:25:34', 1, '2016-10-06 06:25:34', NULL),
(95, 22, 567, 657, 657, NULL, 2, '2016-10-06 00:25:34', 1, '2016-10-06 06:25:34', NULL),
(96, 22, 6, 78, 78, NULL, 3, '2016-10-06 00:25:34', 1, '2016-10-06 06:25:34', NULL),
(97, 22, 34, 5, 677, NULL, 3, '2016-10-06 00:25:34', 1, '2016-10-06 06:25:34', NULL),
(98, 22, 57, 56, 56, NULL, 4, '2016-10-06 00:25:35', 1, '2016-10-06 06:25:35', NULL),
(99, 23, 245, 12, 145, NULL, 1, '2016-11-26 02:24:18', 1, '2016-11-26 08:24:18', NULL),
(100, 23, 55, 154, 24, NULL, 2, '2016-11-26 02:24:18', 1, '2016-11-26 08:24:18', NULL),
(101, 23, 245, 452, 421, NULL, 3, '2016-11-26 02:24:18', 1, '2016-11-26 08:24:18', NULL),
(102, 23, 245, 421, 548, NULL, 4, '2016-11-26 02:24:18', 1, '2016-11-26 08:24:18', NULL),
(104, 27, NULL, 1, NULL, NULL, 1, '2017-02-02 02:19:21', 1, '2017-02-02 08:19:21', NULL),
(110, 27, 3, NULL, NULL, NULL, 2, '2017-02-02 02:20:30', 1, '2017-02-02 08:20:30', NULL),
(112, 27, NULL, 4, NULL, NULL, 3, '2017-02-02 02:20:30', 1, '2017-02-02 08:20:30', NULL),
(114, 27, 1, NULL, NULL, NULL, 4, '2017-02-02 02:20:30', 1, '2017-02-02 08:20:30', NULL),
(116, 27, NULL, 11, NULL, NULL, 1, '2017-02-02 02:49:15', 1, '2017-02-02 08:49:15', NULL),
(117, 27, NULL, NULL, 22, NULL, 2, '2017-02-02 02:49:15', 1, '2017-02-02 08:49:15', NULL),
(118, 27, NULL, NULL, 33, NULL, 3, '2017-02-02 02:49:15', 1, '2017-02-02 08:49:15', NULL),
(119, 27, NULL, 44, NULL, NULL, 4, '2017-02-02 02:49:15', 1, '2017-02-02 08:49:15', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `land_docs`
--

CREATE TABLE IF NOT EXISTS `land_docs` (
  `DOC_ID` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'Primary key of land_docs table',
  `LAND_ID` bigint(20) NOT NULL COMMENT 'Primary key of land table',
  `BAYA_DOLIL` varchar(150) CHARACTER SET utf8 DEFAULT NULL,
  `DOLIL` varchar(150) CHARACTER SET utf8 DEFAULT NULL,
  `DCR` varchar(150) CHARACTER SET utf8 DEFAULT NULL,
  `LEDGER` varchar(150) CHARACTER SET utf8 DEFAULT NULL,
  `CREATED_AT` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `CREATED_BY` int(10) unsigned NOT NULL,
  `UPDATED_AT` datetime DEFAULT NULL,
  `UPDATED_BY` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`DOC_ID`),
  KEY `FK_DOCS_LAND_ID` (`LAND_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=20 ;

--
-- Dumping data for table `land_docs`
--

INSERT INTO `land_docs` (`DOC_ID`, `LAND_ID`, `BAYA_DOLIL`, `DOLIL`, `DCR`, `LEDGER`, `CREATED_AT`, `CREATED_BY`, `UPDATED_AT`, `UPDATED_BY`) VALUES
(1, 1, 'bahia-5790b63eaefeb.jpg', 'dolil-5790b63eaf031.jpg', 'dcr-5790b63eaf06e.jpg', 'ledger-5790b63eaf0ab.jpg', '2016-07-21 05:47:10', 1, '2016-07-21 11:47:10', NULL),
(2, 3, 'bahia-5790b832d1380.jpg', 'dolil-5790b832d13cb.jpg', 'dcr-5790b832d140a.jpg', 'ledger-5790b832d1446.jpg', '2016-07-21 05:55:30', 1, '2016-07-21 11:55:30', NULL),
(5, 9, 'bahia-57a95aafec7e8.jpg', 'dolil-57a95aafec8be.jpg', 'dcr-57a95aafec92e.jpg', 'ledger-57a95aafec9a8.jpg', '2016-08-08 22:23:11', 1, '2016-08-09 04:23:11', NULL),
(6, 12, 'bahia-57a95ddf7d6ce.jpg', 'dolil-57a95ddf7d796.jpg', 'dcr-57a95ddf7d7fc.jpg', 'ledger-57a95ddf7d85d.jpg', '2016-08-08 22:36:47', 1, '2016-08-09 04:36:47', NULL),
(7, 12, 'bahia-57a95ddf8891c.jpg', NULL, NULL, NULL, '2016-08-08 22:36:47', 1, '2016-08-09 04:36:47', NULL),
(8, 16, 'bahia-57a96a7ef1cff.png', 'dolil-57a96a7ef1d86.png', 'dcr-57a96a7ef1dd8.png', 'ledger-57a96a7ef1e22.png', '2016-08-08 23:30:38', 1, '2016-08-09 05:30:38', NULL),
(9, 6, 'bahia-57bbf4442b1e9.jpg', 'dolil-57bbf4442b2b3.jpg', 'dcr-57bbf4442b31b.jpg', 'ledger-57bbf4442b37d.jpg', '2016-08-23 00:59:16', 1, '2016-08-23 06:59:16', NULL),
(10, 6, 'bahia-57bbf4445b9bb.jpg', NULL, NULL, NULL, '2016-08-23 00:59:16', 1, '2016-08-23 06:59:16', NULL),
(11, 17, 'bahia-57eb5bfbb897a.png', 'dolil-57eb5bfbb8a4b.png', 'dcr-57eb5bfbb8ad1.png', 'ledger-57eb5bfbb8b49.png', '2016-09-27 23:58:19', 1, '2016-09-28 05:58:19', NULL),
(16, 21, 'bahia-57f495a0e2d88.png', 'dolil-57f495a0e2e5a.png', 'dcr-57f495a0e2ec4.jpg', 'ledger-57f495a0e2f27.png', '2016-10-04 23:54:40', 1, '2016-10-05 05:54:40', NULL),
(17, 21, 'bahia-57f495a1236b8.jpg', NULL, NULL, NULL, '2016-10-04 23:54:41', 1, '2016-10-05 05:54:41', NULL),
(18, 22, 'bahia-57f5ee5f21a3e.jpg', 'dolil-57f5ee5f21b08.jpg', 'dcr-57f5ee5f21b6c.png', NULL, '2016-10-06 00:25:35', 1, '2016-10-06 06:25:35', NULL),
(19, 22, 'bahia-57f5ee5f2484b.png', NULL, NULL, NULL, '2016-10-06 00:25:35', 1, '2016-10-06 06:25:35', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `land_tax`
--

CREATE TABLE IF NOT EXISTS `land_tax` (
  `TAX_ID` int(10) NOT NULL AUTO_INCREMENT COMMENT 'Primary key of land_tax table',
  `LAND_ID` bigint(20) NOT NULL COMMENT 'Primary key of land table',
  `RECEIPT` varchar(200) CHARACTER SET utf8 NOT NULL COMMENT 'Receipt file name',
  `FILE_TYPE` varchar(10) CHARACTER SET utf8 NOT NULL COMMENT 'Receipt file extension',
  `DATE` datetime NOT NULL COMMENT 'Receipt date',
  `CREATED_AT` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `CREATED_BY` int(10) NOT NULL,
  `UPDATED_AT` datetime DEFAULT NULL,
  `UPDATED_BY` int(10) DEFAULT NULL,
  PRIMARY KEY (`TAX_ID`),
  KEY `FK_TAX_LAND_ID` (`LAND_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=17 ;

--
-- Dumping data for table `land_tax`
--

INSERT INTO `land_tax` (`TAX_ID`, `LAND_ID`, `RECEIPT`, `FILE_TYPE`, `DATE`, `CREATED_AT`, `CREATED_BY`, `UPDATED_AT`, `UPDATED_BY`) VALUES
(3, 9, 'tax-57a95ab00713e', 'jpg', '2016-08-25 00:00:00', '2016-08-23 06:53:36', 1, '2016-08-23 06:53:34', 1),
(4, 12, 'tax-57a95ddfa485c', 'jpg', '2016-08-18 00:00:00', '2016-08-23 11:05:47', 1, '2016-08-23 11:05:44', 1),
(5, 16, 'tax-57a96a7f00a41', 'png', '2016-08-25 00:00:00', '2016-08-08 23:30:39', 1, '2016-08-09 05:30:39', NULL),
(6, 6, 'tax-57bbf44466be5', 'jpg', '2016-08-09 00:00:00', '2016-08-23 00:59:16', 1, '2016-08-23 06:59:16', NULL),
(7, 6, 'tax-57bbf44481ff4', 'jpg', '2016-08-10 00:00:00', '2016-08-23 00:59:16', 1, '2016-08-23 06:59:16', NULL),
(8, 17, 'tax-57eb5bfbbf010', 'png', '2016-09-28 00:00:00', '2016-11-26 08:21:03', 1, '2016-11-26 08:21:04', 1),
(11, 21, 'tax-57f495a1265b8', 'jpg', '2016-10-22 00:00:00', '2016-10-06 04:42:37', 1, '2016-10-06 04:42:35', 1),
(12, 21, 'tax-57f495a1265b8', 'jpg', '2016-10-05 00:00:00', '2016-10-06 04:42:37', 1, '2016-10-06 04:42:35', 1),
(13, 22, 'tax-57f5ee5f27be0', 'jpg', '2016-10-20 00:00:00', '2016-11-07 12:10:14', 1, '2016-11-07 12:10:13', 1),
(14, 23, 'tax-583946b2a20f9', 'jpeg', '2016-11-30 00:00:00', '2016-11-26 02:24:18', 1, '2016-11-26 08:24:18', NULL),
(16, 27, 'tax-5892eb8a04b3c', 'jpg', '2017-02-16 00:00:00', '2017-02-02 02:19:22', 1, '2017-02-02 08:19:22', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `land_uses`
--

CREATE TABLE IF NOT EXISTS `land_uses` (
  `LU_ID` int(10) NOT NULL AUTO_INCREMENT,
  `LAND_ID` bigint(20) NOT NULL,
  `LAND_CATEGORY` int(10) NOT NULL COMMENT 'Primary key of land_category table',
  `LAND_SUBCATEGORY` int(10) NOT NULL COMMENT 'Primary key of land_category table',
  `USES` float NOT NULL,
  `CREATED_AT` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `CREATED_BY` int(10) NOT NULL,
  `UPDATED_AT` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `UPDATED_BY` int(10) DEFAULT NULL,
  PRIMARY KEY (`LU_ID`),
  KEY `LAND_ID` (`LAND_ID`),
  KEY `LAND_CATEGORY` (`LAND_CATEGORY`),
  KEY `LAND_SUBCATEGORY` (`LAND_SUBCATEGORY`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=35 ;

--
-- Dumping data for table `land_uses`
--

INSERT INTO `land_uses` (`LU_ID`, `LAND_ID`, `LAND_CATEGORY`, `LAND_SUBCATEGORY`, `USES`, `CREATED_AT`, `CREATED_BY`, `UPDATED_AT`, `UPDATED_BY`) VALUES
(1, 1, 2, 4, 8, '2016-07-21 05:47:10', 1, '2016-07-21 11:47:10', NULL),
(2, 1, 1, 5, 20, '2016-07-21 05:47:10', 1, '2016-07-21 11:47:10', NULL),
(3, 3, 2, 4, 8, '2016-07-21 05:55:30', 1, '2016-07-21 11:55:30', NULL),
(4, 3, 1, 5, 20, '2016-07-21 05:55:30', 1, '2016-07-21 11:55:30', NULL),
(6, 5, 1, 5, 90, '2016-08-07 22:55:54', 1, '2016-08-08 04:55:54', NULL),
(8, 6, 1, 5, 20, '2016-08-23 06:59:17', 1, '2016-08-23 06:59:16', 1),
(9, 7, 1, 5, 50, '2016-08-08 22:01:27', 1, '2016-08-09 04:01:27', NULL),
(10, 7, 1, 5, 20, '2016-08-08 22:01:27', 1, '2016-08-09 04:01:27', NULL),
(11, 8, 1, 5, 50, '2016-08-08 22:01:44', 1, '2016-08-09 04:01:44', NULL),
(12, 8, 1, 5, 20, '2016-08-08 22:01:44', 1, '2016-08-09 04:01:44', NULL),
(13, 9, 1, 5, 1, '2016-08-23 06:53:36', 1, '2016-08-23 06:53:34', 1),
(17, 12, 1, 5, 34, '2016-08-23 11:05:47', 1, '2016-08-23 11:05:44', 1),
(18, 12, 1, 5, 34, '2016-08-23 11:05:47', 1, '2016-08-23 11:05:44', 1),
(19, 14, 1, 5, 56, '2016-08-08 23:28:43', 1, '2016-08-09 05:28:43', NULL),
(20, 16, 1, 5, 56, '2016-08-08 23:30:38', 1, '2016-08-09 05:30:38', NULL),
(21, 9, 1, 5, 45, '2016-08-23 00:53:34', 0, '2016-08-23 06:53:34', 1),
(22, 6, 1, 5, 50, '2016-08-23 00:59:16', 0, '2016-08-23 06:59:16', 1),
(23, 17, 1, 5, 100, '2016-10-01 05:57:50', 1, '2016-10-01 05:57:47', 1),
(24, 19, 1, 5, 578, '2016-10-04 23:50:59', 1, '2016-10-05 05:50:59', NULL),
(25, 20, 1, 5, 578, '2016-10-04 23:51:56', 1, '2016-10-05 05:51:56', NULL),
(26, 21, 2, 4, 45, '2016-10-04 23:54:40', 1, '2016-10-05 05:54:40', NULL),
(27, 21, 1, 5, 45, '2016-10-04 23:54:40', 1, '2016-10-05 05:54:40', NULL),
(28, 22, 1, 5, 78, '2016-10-06 00:25:35', 1, '2016-10-06 06:25:35', NULL),
(29, 23, 2, 4, 55, '2016-11-26 02:24:18', 1, '2016-11-26 08:24:18', NULL),
(30, 25, 2, 22, 20, '2017-02-01 23:32:21', 1, '2017-02-02 05:32:21', NULL),
(31, 25, 1, 13, 80, '2017-02-01 23:32:22', 1, '2017-02-02 05:32:22', NULL),
(32, 26, 2, 22, 20, '2017-02-01 23:36:13', 1, '2017-02-02 05:36:13', NULL),
(33, 26, 1, 13, 80, '2017-02-01 23:36:13', 1, '2017-02-02 05:36:13', NULL),
(34, 27, 1, 5, 100, '2017-02-02 02:19:21', 1, '2017-02-02 08:19:21', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `lease_demesne`
--

CREATE TABLE IF NOT EXISTS `lease_demesne` (
  `LEASE_DE_ID` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'Primary key',
  `SCHEDULE_ID` bigint(20) NOT NULL COMMENT 'Primary key of tender_schedule',
  `NAME` varchar(100) NOT NULL COMMENT 'Khash owner name',
  `PHONE` varchar(50) NOT NULL COMMENT 'Khash owner phone munber',
  `ADDRESS` varchar(250) NOT NULL COMMENT 'Khash owner address',
  `REMARKS` text COMMENT 'Remarks if any',
  `IS_CANCEL` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0=no, 1=yes',
  `CANCEL_REMARKS` text COMMENT 'Cancel remarks if any',
  `IS_ACTIVE` tinyint(1) NOT NULL DEFAULT '1' COMMENT '0=no, 1=yes',
  `CREATED_AT` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `CREATED_BY` bigint(20) NOT NULL,
  `UPDATED_AT` datetime DEFAULT NULL,
  `UPDATED_BY` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`LEASE_DE_ID`),
  KEY `SCHEDULE_ID` (`SCHEDULE_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=18 ;

--
-- Dumping data for table `lease_demesne`
--

INSERT INTO `lease_demesne` (`LEASE_DE_ID`, `SCHEDULE_ID`, `NAME`, `PHONE`, `ADDRESS`, `REMARKS`, `IS_CANCEL`, `CANCEL_REMARKS`, `IS_ACTIVE`, `CREATED_AT`, `CREATED_BY`, `UPDATED_AT`, `UPDATED_BY`) VALUES
(15, 121, 'Abdul Awal', '01745124576', '<p>Narayongonj</p>', NULL, 1, '<p>Payment problem</p>', 1, '2016-11-13 03:53:25', 1, '2016-11-13 10:11:16', 1),
(16, 123, 'মোঃ করিম', '+8801', '<p><label for="ADDRESS">আদায় কারির ঠিকানা </label></p>', NULL, 0, NULL, 1, '2016-11-13 05:06:27', 1, '2016-11-13 11:06:27', NULL),
(17, 121, 'Abdul Awal', '01712454128', '<p>Narayongonj</p>', NULL, 0, NULL, 1, '2016-11-26 06:01:18', 6, '2017-04-16 09:28:40', 1);

-- --------------------------------------------------------

--
-- Table structure for table `lease_validity`
--

CREATE TABLE IF NOT EXISTS `lease_validity` (
  `LEASE_VL_ID` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'Primary key',
  `TE_APP_ID` bigint(20) DEFAULT NULL COMMENT 'Tender applicant id',
  `CITIZEN_ID` bigint(20) DEFAULT NULL COMMENT 'Primary Key of sa_citizen table. This ID holder will be only Citizen',
  `LEASE_DE_ID` bigint(20) DEFAULT NULL COMMENT 'Primary key of lease_demesne table',
  `SCHEDULE_ID` bigint(20) NOT NULL COMMENT 'Tender schedule id',
  `DATE_FROM` datetime NOT NULL COMMENT 'Lease start date',
  `DATE_TO` datetime NOT NULL COMMENT 'Lease end date',
  `IS_CALCEL` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0=not cancel, 1=cancel',
  `CANCEL_REMARKS` text COMMENT 'Cancel remarks if any',
  `IS_ACTIVE` tinyint(1) NOT NULL DEFAULT '1' COMMENT '0=no, 1=yes',
  `CREATED_AT` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `CREATED_BY` bigint(20) NOT NULL,
  `UPDATED_AT` datetime DEFAULT NULL,
  `UPDATED_BY` int(11) DEFAULT NULL,
  PRIMARY KEY (`LEASE_VL_ID`),
  KEY `TE_APP_ID` (`TE_APP_ID`),
  KEY `SCHEDULE_ID` (`SCHEDULE_ID`),
  KEY `LEASE_DE_ID` (`LEASE_DE_ID`),
  KEY `CITIZEN_ID` (`CITIZEN_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=40 ;

--
-- Dumping data for table `lease_validity`
--

INSERT INTO `lease_validity` (`LEASE_VL_ID`, `TE_APP_ID`, `CITIZEN_ID`, `LEASE_DE_ID`, `SCHEDULE_ID`, `DATE_FROM`, `DATE_TO`, `IS_CALCEL`, `CANCEL_REMARKS`, `IS_ACTIVE`, `CREATED_AT`, `CREATED_BY`, `UPDATED_AT`, `UPDATED_BY`) VALUES
(30, NULL, NULL, 15, 121, '2016-11-14 00:00:00', '2017-11-01 00:00:00', 0, NULL, 0, '2016-11-13 03:53:26', 1, '2016-11-13 10:11:16', 1),
(32, NULL, NULL, 16, 123, '2016-11-14 00:00:00', '2016-11-30 00:00:00', 0, NULL, 1, '2016-11-13 05:06:27', 1, '2016-11-13 11:06:27', NULL),
(34, 263, NULL, NULL, 124, '2016-11-14 00:00:00', '2016-11-30 00:00:00', 0, NULL, 1, '2016-11-13 05:38:09', 1, '2016-11-13 11:38:09', NULL),
(35, 264, NULL, NULL, 124, '2016-11-16 00:00:00', '2016-11-22 00:00:00', 0, NULL, 1, '2016-11-14 07:39:29', 1, '2016-11-14 13:39:29', NULL),
(36, NULL, 7, NULL, 124, '2016-11-16 00:00:00', '2016-11-17 00:00:00', 0, NULL, 1, '2016-11-14 22:53:12', 1, '2016-11-15 04:53:12', NULL),
(37, NULL, NULL, 17, 121, '2016-12-01 00:00:00', '2017-04-30 00:00:00', 0, NULL, 1, '2016-11-26 06:01:18', 6, '2017-04-16 09:28:40', 1),
(38, 265, NULL, NULL, 136, '2016-12-21 00:00:00', '2017-12-21 00:00:00', 0, NULL, 1, '2016-12-21 04:39:48', 6, '2016-12-21 10:39:48', NULL),
(39, 266, NULL, NULL, 134, '2017-04-15 00:00:00', '2017-04-30 00:00:00', 0, NULL, 1, '2017-04-15 04:11:21', 1, '2017-04-15 10:11:21', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `project`
--

CREATE TABLE IF NOT EXISTS `project` (
  `PROJECT_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `PR_UD_ID` varchar(200) CHARACTER SET utf8 DEFAULT NULL COMMENT 'If have any User Defined Number',
  `PR_NAME` varchar(500) CHARACTER SET utf8 NOT NULL COMMENT 'Name of the Project',
  `PR_NAME_BN` text CHARACTER SET utf8 NOT NULL COMMENT 'Name of the Project in Bangla',
  `PR_DESC` text CHARACTER SET utf8 NOT NULL COMMENT 'Description of the Project',
  `PR_START_DT` datetime DEFAULT NULL COMMENT 'Starting Date of the Project',
  `PR_END_DT` datetime DEFAULT NULL COMMENT 'End Date of the Project',
  `PROJECT_LOCATION` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Location of the address',
  `IS_LEASE` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0=no, 1=yes',
  `IS_ACTIVE` tinyint(1) NOT NULL COMMENT '0=no, 1=yes',
  `CREATED_AT` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `CREATED_BY` int(10) NOT NULL,
  `UPDATED_AT` datetime DEFAULT NULL,
  `UPDATED_BY` int(10) DEFAULT NULL,
  PRIMARY KEY (`PROJECT_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=87 ;

--
-- Dumping data for table `project`
--

INSERT INTO `project` (`PROJECT_ID`, `PR_UD_ID`, `PR_NAME`, `PR_NAME_BN`, `PR_DESC`, `PR_START_DT`, `PR_END_DT`, `PROJECT_LOCATION`, `IS_LEASE`, `IS_ACTIVE`, `CREATED_AT`, `CREATED_BY`, `UPDATED_AT`, `UPDATED_BY`) VALUES
(62, 'বাজার/০১-২০১৬', 'Bandor Bazer', 'বন্দর বাজার', '', NULL, NULL, '<p>বঙ্গবন্ধু সড়ক, নারায়ণগঞ্জ, বাংলাদেশ</p>', 1, 1, '2016-11-07 03:29:21', 1, '2016-11-07 09:36:16', 1),
(63, 'বাজার/০২-২০১৬', 'Bhabaniganj Hat Bazer', 'ভবানীগঞ্জ হাট বাজার', '', NULL, NULL, '<p>ভবানীগঞ্জ, নারায়ণগঞ্জ, বাংলাদেশ</p>', 1, 1, '2016-11-07 03:30:32', 1, '2016-11-07 09:36:00', 1),
(64, 'padma-1', 'Padma city plaza 1', 'পদ্ম সিটি প্লাজা-১', '<p><strong>নারায়ণগঞ্জ সিটি করপোরেশন</strong> নারায়ণগঞ্জ পৌরসভাকে \r\n(মিউনিসিপ্যালিটি) ২০১১ সালের ৫ মে নারায়ণগঞ্জ সিটি করপোরেশনে রূপান্তর \r\nকরা হয়। নারায়ণগঞ্জ শহর, সিদ্ধিরগঞ্জ পৌর এলাকা এবং বন্দর উপজেলার \r\nকমদরসূল পৌরসভাকে নিয়ে নারায়ণগঞ্জ সিটি করপোরেশন গঠিত হয়।</p>', '2014-11-13 00:00:00', '2016-03-25 00:00:00', '​নারায়ণগঞ্জ', 0, 1, '2016-11-07 03:33:46', 1, '2016-11-07 09:33:46', NULL),
(65, 'বাজার/০৩-২০১৬', 'Kalir Bazaar', 'কালীর বাজার', '', NULL, NULL, 'কালীর বাজার, নারায়ণগঞ্জ, বাংলাদেশ', 1, 1, '2016-11-07 03:36:57', 1, '2016-11-07 09:36:57', NULL),
(66, 'বাজার/০৪-২০১৬', 'Godnial Hat Bazer', 'গোদনাইল হাট বাজার', '', NULL, NULL, '<p>গোদনাইল,নারায়ণগঞ্জ, বাংলাদেশ</p>', 1, 1, '2016-11-07 03:39:32', 1, '2017-02-05 06:33:19', 1),
(67, 'padma-2', 'Padma city plaza 2', 'পদ্ম সিটি প্লাজা-২', '<p><strong>নারায়ণগঞ্জ সিটি করপোরেশন</strong> \r\nনারায়ণগঞ্জ পৌরসভাকে (মিউনিসিপ্যালিটি) ২০১১ সালের ৫ মে নারায়ণগঞ্জ সিটি \r\nকরপোরেশনে রূপান্তর করা হয়। নারায়ণগঞ্জ শহর, সিদ্ধিরগঞ্জ পৌর এলাকা এবং \r\nবন্দর উপজেলার কমদরসূল পৌরসভাকে নিয়ে নারায়ণগঞ্জ সিটি করপোরেশন গঠিত হয়। </p><p><br></p><p> সিদ্ধিরগঞ্জ পৌরসভাকে বিলুপ্ত করে এর ১ থেকে ৯ \r\nনম্বর ওয়ার্ডকে সিটি করপোরেশনের ১ থেকে ৯ নম্বর ওয়ার্ড হিসাবে ঘোষণা করা \r\nহয়েছে। সিদ্ধিরগঞ্জের পাইনাদী পূর্ব অংশ মিজমিজি বাতান পাড়াকে ১ নম্বর \r\nওয়ার্ড করা হয়েছে। ১ থেকে ৩ নম্বর ওয়ার্ডকে সংরক্ষিত মহিলা আসন হিসেবে \r\nতালিকা ভুক্ত করা হয়েছে।</p>', '2010-02-10 00:00:00', '2016-09-14 00:00:00', 'নারায়ণগঞ্জ', 0, 1, '2016-11-07 03:56:10', 1, '2016-11-07 09:56:10', NULL),
(71, '3434', 'Project test name', 'Projec bangla name', '<p>fgfg</p>', '2017-02-20 00:00:00', '2017-02-21 00:00:00', 'dghfhj', 0, 1, '2017-02-04 05:10:12', 1, '2017-02-04 11:55:18', 1),
(72, '1', 'প্রকল্পের নাম (English) *', 'প্রকল্পের বাংলা নাম *', '<p><label for="PR_DESC">প্রকল্পের বিবরণ</label> <span class="required_field">*</span></p>', '2017-04-19 00:00:00', '2017-04-19 00:00:00', 'প্রকল্পের অবস্থান *', 0, 1, '2017-04-19 04:28:32', 1, '2017-04-23 05:34:44', 1),
(74, '34345', 'প্রকল্পের নাম (English', 'modomotiu ', '<p>gf</p>', '2017-04-23 00:00:00', '2017-04-30 00:00:00', 'প্রকল্পের অবস্থান ', 0, 1, '2017-04-23 03:42:08', 1, '2017-04-23 09:42:08', NULL),
(75, '789', 'প্রকল্পের নাম (English)', 'রকল্পের বাংলা নাম', '<p>প্রকল্পের বিবরণ</p>', '2017-04-23 00:00:00', '2017-04-30 00:00:00', 'প্রকল্পের অবস্থান *', 0, 1, '2017-04-23 03:48:08', 1, '2017-04-23 09:48:08', NULL),
(76, 'প্রকল্পের নাম্বার 55', 'sdgdsfhd', ' fdhdfhjfgj', 'fgjhfgj', '2017-04-24 00:00:00', '2017-04-30 00:00:00', ' gfjfgj', 0, 1, '2017-04-23 03:48:57', 1, '2017-04-23 09:48:57', NULL),
(77, 'প্রকল্পের নাম্বার 555655', 'sdgdsfhd', ' fdhdfhjfgj', 'fgjhfgj', '2017-04-24 00:00:00', '2017-04-30 00:00:00', ' gfjfgj', 0, 1, '2017-04-23 03:50:41', 1, '2017-04-23 09:50:41', NULL),
(78, 'প্রকল্পের নাম্বার 55569', 'sdgdsfhd', ' fdhdfhjfgj', '<p>fgjhfgj</p>', '2017-04-24 00:00:00', '2017-04-30 00:00:00', ' gfjfgj', 0, 1, '2017-04-23 03:52:50', 1, '2017-04-23 11:33:31', 1),
(83, 'dghfgh', 'dsfhdfh', 'fghfgh', '<p>hfghfg</p>', '2017-04-24 00:00:00', '2017-04-17 00:00:00', 'hfghfgh', 0, 1, '2017-04-23 05:37:31', 1, '2017-04-23 11:37:31', NULL),
(84, '123', 'Nurullah', 'Khayrul', '<p>Nurullah </p><p style="margin-left: 40px;">Khayrul</p>', '2017-04-01 00:00:00', '2017-04-30 00:00:00', 'ATI Limited', 0, 1, '2017-04-23 05:38:17', 1, '2017-04-23 11:48:39', 1),
(85, 'undefined', 'undefined', 'undefined', 'undefined', '1970-01-01 00:00:00', '1970-01-01 00:00:00', 'undefined', 0, 0, '2017-04-24 01:06:10', 1, '2017-04-24 07:06:10', NULL),
(86, '24-04-2017', 'Padma City Plaza-4', 'পাদ্মা সিটি প্লাজা-৪', '<p>তেস্ত প্রজেচত</p>', '2017-04-01 00:00:00', '2017-04-30 00:00:00', 'টানবাজার', 0, 1, '2017-04-24 06:03:35', 1, '2017-04-24 12:03:35', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `project_details`
--

CREATE TABLE IF NOT EXISTS `project_details` (
  `PR_DETAIL_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `PROJECT_ID` bigint(20) NOT NULL COMMENT 'Primary Key of projects table.',
  `PR_CATEGORY` tinyint(2) NOT NULL COMMENT 'Category = Flat, Shop, Space or Lease',
  `PR_TYPE` tinyint(2) DEFAULT NULL COMMENT 'Flat = Type A, Space = Unit A, Shop = 1 etc',
  `POSITION` tinyint(2) DEFAULT NULL COMMENT '1st Floor, 2nd Floor etc',
  `PR_SSF_NO` varchar(45) CHARACTER SET utf8 DEFAULT NULL COMMENT 'SHOP/SPACE/FLAT NUMNER',
  `PR_MEASURMENT` double NOT NULL COMMENT 'E.G. 968.82 Square feet',
  `UOM` tinyint(2) NOT NULL COMMENT 'Unit Of Measure e.g. Square feet',
  `IS_RESERVED` tinyint(1) DEFAULT '0' COMMENT 'This will be mostly for shops. If any shop is kept reserved value 1 will be inserted',
  `IS_BUILT` tinyint(1) DEFAULT '0',
  `IS_ACTIVE` tinyint(1) NOT NULL COMMENT '0=no, 1=yes',
  `CREATED_AT` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `CREATED_BY` int(10) NOT NULL,
  `UPDATED_AT` datetime DEFAULT NULL,
  `UPDATED_BY` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'SHOP/SPACE/FLAT NO',
  PRIMARY KEY (`PR_DETAIL_ID`),
  KEY `FK_DE_PROJECT_ID` (`PROJECT_ID`),
  KEY `FK_DE_PR_CATEGORY` (`PR_CATEGORY`),
  KEY `FK_DE_PR_TYPE` (`PR_TYPE`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=105 ;

--
-- Dumping data for table `project_details`
--

INSERT INTO `project_details` (`PR_DETAIL_ID`, `PROJECT_ID`, `PR_CATEGORY`, `PR_TYPE`, `POSITION`, `PR_SSF_NO`, `PR_MEASURMENT`, `UOM`, `IS_RESERVED`, `IS_BUILT`, `IS_ACTIVE`, `CREATED_AT`, `CREATED_BY`, `UPDATED_AT`, `UPDATED_BY`) VALUES
(69, 64, 21, 11, 1, '1B', 650, 1, 0, 1, 1, '2016-11-07 03:33:46', 1, '2016-11-07 09:33:46', NULL),
(70, 64, 21, 10, 1, '1A', 600, 1, 0, 1, 1, '2016-11-07 03:33:46', 1, '2016-11-07 09:33:46', NULL),
(71, 64, 22, 11, 4, '2B', 1300, 1, 0, 1, 1, '2016-11-07 03:33:46', 1, '2016-11-07 09:33:46', NULL),
(72, 64, 22, 10, 4, '2A', 1200, 1, 0, 1, 1, '2016-11-07 03:33:46', 1, '2016-11-07 09:33:46', NULL),
(73, 64, 23, 10, 5, '3A', 2500, 1, 1, 1, 1, '2016-11-07 03:33:46', 1, '2016-11-07 09:33:46', NULL),
(74, 67, 21, 10, 1, '1B', 600, 1, 0, 1, 1, '2016-11-07 03:56:10', 1, '2016-11-07 09:56:10', NULL),
(75, 67, 21, 10, 1, '1A', 550, 1, 0, 1, 1, '2016-11-07 03:56:11', 1, '2016-11-07 09:56:11', NULL),
(76, 67, 22, 10, 4, '2F2', 1100, 1, 1, 1, 1, '2016-11-07 03:56:11', 1, '2016-11-07 09:56:11', NULL),
(77, 67, 22, 10, 4, '2F1', 1000, 1, 0, 1, 1, '2016-11-07 03:56:11', 1, '2016-11-07 09:56:11', NULL),
(79, 71, 21, 10, 1, '45', 453, 1, 0, 1, 1, '2017-02-04 05:10:13', 1, '2017-02-04 11:55:18', '1'),
(80, 72, 21, 11, 4, '159', 125, 1, 0, 1, 1, '2017-04-19 05:06:09', 1, '2017-04-19 11:09:03', '1'),
(81, 72, 21, 12, 5, '156', 1596, 1, 0, 1, 1, '2017-04-19 05:06:09', 1, '2017-04-19 11:09:03', '1'),
(82, 72, 23, 10, 1, '154969', 1555, 1, 0, 1, 1, '2017-04-19 05:06:09', 1, '2017-04-19 11:06:09', '1'),
(83, 72, 21, 11, 4, '855414', 45452, 1, 0, 1, 1, '2017-04-19 05:09:03', 1, '2017-04-23 05:34:44', '1'),
(84, 72, 21, 11, 5, '455', 5855, 1, 0, 1, 1, '2017-04-19 05:09:03', 1, '2017-04-23 05:34:44', '1'),
(85, 72, 23, 11, 2, '4554', 221, 1, 0, 1, 1, '2017-04-19 05:09:04', 1, '2017-04-19 11:09:04', '1'),
(88, 72, 21, 10, 4, '11', 1100, 1, 1, 1, 1, '2017-04-24 04:20:25', 1, '2017-04-24 10:20:25', NULL),
(95, 72, 21, 10, 1, '13', 1300, 1, 1, 1, 1, '2017-04-24 04:37:49', 1, '2017-04-24 10:37:49', NULL),
(96, 72, 21, 10, 1, '13', 1300, 1, 1, 1, 1, '2017-04-24 04:39:34', 1, '2017-04-24 10:39:34', NULL),
(97, 72, 21, 11, 4, '11', 1100, 1, 0, 1, 1, '2017-04-24 04:39:34', 1, '2017-04-24 10:39:34', NULL),
(98, 72, 21, 11, 1, '12', 1200, 1, 0, 1, 1, '2017-04-24 04:39:34', 1, '2017-04-24 10:39:34', NULL),
(99, 86, 21, 10, 5, '306', 125, 1, 1, 1, 1, '2017-04-24 06:05:51', 1, '2017-04-24 12:05:51', NULL),
(100, 86, 21, 10, 5, '303', 125, 1, 0, 1, 1, '2017-04-24 06:05:51', 1, '2017-04-24 12:05:51', NULL),
(101, 86, 21, 10, 5, '302', 125, 1, 0, 1, 1, '2017-04-24 06:05:51', 1, '2017-04-24 12:05:51', NULL),
(102, 86, 21, 10, 5, '301', 125, 1, 0, 1, 1, '2017-04-24 06:05:51', 1, '2017-04-24 12:05:51', NULL),
(103, 86, 21, 10, 4, '203', 125, 1, 0, 1, 1, '2017-04-24 06:05:51', 1, '2017-04-24 12:05:51', NULL),
(104, 86, 21, 10, 4, '201', 125, 1, 0, 1, 1, '2017-04-24 06:05:52', 1, '2017-04-24 12:05:52', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `project_docs`
--

CREATE TABLE IF NOT EXISTS `project_docs` (
  `PR_DOC_ID` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'Primary key',
  `PROJECT_ID` bigint(20) NOT NULL COMMENT 'Primary key of project table',
  `ATTACHMENT` varchar(150) DEFAULT NULL COMMENT 'Attachemnt',
  `TYPE` tinyint(1) DEFAULT NULL COMMENT 'Type of attachemnt',
  `IS_ACTIVE` tinyint(1) NOT NULL DEFAULT '1',
  `CREATED_AT` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `CREATED_BY` bigint(20) DEFAULT NULL,
  `UPDATED_AT` datetime DEFAULT NULL,
  `UPDATED_BY` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`PR_DOC_ID`),
  KEY `PROJECT_ID` (`PROJECT_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=14 ;

--
-- Dumping data for table `project_docs`
--

INSERT INTO `project_docs` (`PR_DOC_ID`, `PROJECT_ID`, `ATTACHMENT`, `TYPE`, `IS_ACTIVE`, `CREATED_AT`, `CREATED_BY`, `UPDATED_AT`, `UPDATED_BY`) VALUES
(2, 71, 'pro-5895b694ee922.jpg', NULL, 1, '2017-02-04 05:10:12', NULL, '2017-02-04 11:10:12', NULL),
(4, 71, 'pro-5895c12667a5c.jpg', NULL, 1, '2017-02-04 05:55:18', 1, '2017-02-04 11:55:18', NULL),
(5, 74, 'pro-58fc76f0d1921.jpg', NULL, 1, '2017-04-23 03:42:08', 1, '2017-04-23 09:42:08', NULL),
(6, 74, 'pro-58fc76f0e0769.jpg', NULL, 1, '2017-04-23 03:42:08', 1, '2017-04-23 09:42:08', NULL),
(7, 75, 'pro-58fc785895449.jpg', NULL, 1, '2017-04-23 03:48:08', 1, '2017-04-23 09:48:08', NULL),
(8, 75, 'pro-58fc78589ae21.jpg', NULL, 1, '2017-04-23 03:48:08', 1, '2017-04-23 09:48:08', NULL),
(9, 76, 'pro-58fc788919bb9.jpg', NULL, 1, '2017-04-23 03:48:57', 1, '2017-04-23 09:48:57', NULL),
(10, 77, 'pro-58fc78f1b8e99.jpg', NULL, 1, '2017-04-23 03:50:41', 1, '2017-04-23 09:50:41', NULL),
(11, 78, 'pro-58fc7972a1f69.jpg', NULL, 1, '2017-04-23 03:52:50', 1, '2017-04-23 09:52:50', NULL),
(12, 84, 'pro-58fc9497c07b1.jpg', NULL, 1, '2017-04-23 05:48:39', 1, '2017-04-23 11:48:39', NULL),
(13, 84, 'pro-58fc9497c55d1.jpg', NULL, 1, '2017-04-23 05:48:39', 1, '2017-04-23 11:48:39', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `project_land`
--

CREATE TABLE IF NOT EXISTS `project_land` (
  `PROJECT_LN_ID` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'Primary key',
  `PROJECT_ID` bigint(20) NOT NULL COMMENT 'Primary key of project table',
  `LAND_ID` bigint(20) NOT NULL,
  `IS_ACTIVE` tinyint(1) NOT NULL,
  `CREATED_AT` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `CREATED_BY` bigint(20) NOT NULL,
  `UPDATED_AT` datetime DEFAULT NULL,
  `UPDATED_BY` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`PROJECT_LN_ID`),
  KEY `project_land_ibfk_1` (`PROJECT_ID`),
  KEY `project_land_ibfk_2` (`LAND_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=110 ;

--
-- Dumping data for table `project_land`
--

INSERT INTO `project_land` (`PROJECT_LN_ID`, `PROJECT_ID`, `LAND_ID`, `IS_ACTIVE`, `CREATED_AT`, `CREATED_BY`, `UPDATED_AT`, `UPDATED_BY`) VALUES
(79, 64, 5, 1, '2016-11-07 03:33:46', 1, '2016-11-07 09:33:46', NULL),
(80, 64, 6, 1, '2016-11-07 03:33:46', 1, '2016-11-07 09:33:46', NULL),
(82, 63, 4, 1, '2016-11-07 03:36:00', 1, '2016-11-07 09:36:00', NULL),
(83, 62, 1, 1, '2016-11-07 03:36:16', 1, '2016-11-07 09:36:16', NULL),
(84, 65, 3, 1, '2016-11-07 03:36:57', 1, '2016-11-07 09:36:57', NULL),
(86, 67, 12, 1, '2016-11-07 03:56:10', 1, '2016-11-07 09:56:10', NULL),
(87, 67, 14, 1, '2016-11-07 03:56:10', 1, '2016-11-07 09:56:10', NULL),
(91, 71, 6, 1, '2017-02-04 05:55:18', 1, '2017-02-04 11:55:18', NULL),
(92, 66, 16, 1, '2017-02-05 00:33:19', 1, '2017-02-05 06:33:19', NULL),
(101, 72, 21, 1, '2017-04-22 23:34:44', 1, '2017-04-23 05:34:44', NULL),
(102, 72, 22, 1, '2017-04-22 23:34:44', 1, '2017-04-23 05:34:44', NULL),
(103, 74, 17, 1, '2017-04-23 03:42:08', 1, '2017-04-23 09:42:08', NULL),
(104, 75, 8, 1, '2017-04-23 03:48:08', 1, '2017-04-23 09:48:08', NULL),
(109, 84, 7, 1, '2017-04-23 05:48:39', 1, '2017-04-23 11:48:39', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sa_bank`
--

CREATE TABLE IF NOT EXISTS `sa_bank` (
  `BANK_ID` tinyint(3) NOT NULL AUTO_INCREMENT,
  `B_PARENT_ID` tinyint(3) DEFAULT NULL,
  `BANK_NAME` varchar(100) CHARACTER SET utf8 NOT NULL COMMENT 'Name of the Bank',
  `DISTRICT_ID` int(8) NOT NULL,
  `ADDRESS` text CHARACTER SET utf8 COMMENT 'Address Of Bank',
  `IS_ACTIVE` tinyint(1) NOT NULL COMMENT '0=no, 1=yes',
  `CREATED_AT` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `CREATED_BY` int(10) NOT NULL,
  `UPDATED_AT` datetime DEFAULT NULL,
  `UPDATED_BY` int(10) DEFAULT NULL,
  PRIMARY KEY (`BANK_ID`),
  KEY `DISTRICT_ID` (`DISTRICT_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=14 ;

--
-- Dumping data for table `sa_bank`
--

INSERT INTO `sa_bank` (`BANK_ID`, `B_PARENT_ID`, `BANK_NAME`, `DISTRICT_ID`, `ADDRESS`, `IS_ACTIVE`, `CREATED_AT`, `CREATED_BY`, `UPDATED_AT`, `UPDATED_BY`) VALUES
(8, 0, 'Bangladesh Bank', 13, '<p>Bangladesh Bank</p>', 1, '2016-07-20 00:26:40', 1, '2016-09-28 06:20:37', 1),
(10, 0, 'Dhaka Bank', 13, '<p>motijheel commercial area,Dhaka</p>', 1, '2016-07-20 01:55:11', 1, '2016-07-20 12:04:10', 1),
(13, 0, 'Jamuna Bank', 13, '<p>motijheel commercial area,Dhaka</p>', 1, '2016-07-20 02:09:57', 1, '2016-07-20 08:09:57', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sa_category`
--

CREATE TABLE IF NOT EXISTS `sa_category` (
  `PR_CATEGORY` tinyint(2) NOT NULL AUTO_INCREMENT,
  `CATE_NAME` varchar(100) CHARACTER SET utf8 NOT NULL COMMENT 'Category = Flat, Shop, Space or Lease',
  `CATE_DESC` text CHARACTER SET utf8 COMMENT 'Description of Category if have any',
  `IS_LEASE` tinyint(1) NOT NULL COMMENT '0=no, 1=yes',
  `IS_ACTIVE` tinyint(1) NOT NULL COMMENT '0=no, 1=yes',
  `CREATED_AT` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `CREATED_BY` int(10) NOT NULL,
  `UPDATED_AT` datetime DEFAULT NULL,
  `UPDATED_BY` int(10) DEFAULT NULL,
  PRIMARY KEY (`PR_CATEGORY`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=25 ;

--
-- Dumping data for table `sa_category`
--

INSERT INTO `sa_category` (`PR_CATEGORY`, `CATE_NAME`, `CATE_DESC`, `IS_LEASE`, `IS_ACTIVE`, `CREATED_AT`, `CREATED_BY`, `UPDATED_AT`, `UPDATED_BY`) VALUES
(21, 'দোকান', 'পদ্ম সিটি প্লাজা-৩ গাঙ্গুলী রোড,\r\nনীচতলা,১২ টি দোকান,২টি স্পেস।', 0, 1, '2016-07-14 00:57:56', 1, '2016-07-18 10:16:14', 1),
(22, 'ফ্ল্যাট', 'পদ্ম সিটি প্লাজা-১ এসএম মালেহ রোড,টাইপ-A,১৩৬২ বর্গফুট।', 0, 1, '2016-07-14 01:01:14', 1, '2016-07-14 07:01:14', NULL),
(23, 'স্পেস', 'পদ্ম সিটি প্লাজা-১ এসএম মালেহ রোড,টাইপ-A,৯৬৮.৮২ বর্গফুট,২৪,২২,০৫০/-।', 0, 1, '2016-07-18 04:20:17', 1, '2016-07-20 08:44:28', 1),
(24, 'ইজারা', '', 1, 1, '2016-08-29 00:31:27', 1, '2016-10-09 11:26:52', 1);

-- --------------------------------------------------------

--
-- Table structure for table `sa_category_conditions`
--

CREATE TABLE IF NOT EXISTS `sa_category_conditions` (
  `CATE_CON_ID` tinyint(2) NOT NULL AUTO_INCREMENT,
  `PR_CATEGORY` tinyint(2) NOT NULL COMMENT 'Primary Key of sa_category table',
  `CON_TITLE` varchar(100) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Title of condition if have any',
  `CON_DESC` text CHARACTER SET utf8 NOT NULL COMMENT 'Description of condition',
  `IS_ACTIVE` tinyint(1) NOT NULL COMMENT '0=no, 1=yes',
  `CREATED_AT` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `CREATED_BY` int(10) NOT NULL,
  `UPDATED_AT` datetime DEFAULT NULL,
  `UPDATED_BY` int(10) DEFAULT NULL,
  PRIMARY KEY (`CATE_CON_ID`),
  KEY `FK_SC_PR_CATEGORY` (`PR_CATEGORY`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=9 ;

--
-- Dumping data for table `sa_category_conditions`
--

INSERT INTO `sa_category_conditions` (`CATE_CON_ID`, `PR_CATEGORY`, `CON_TITLE`, `CON_DESC`, `IS_ACTIVE`, `CREATED_AT`, `CREATED_BY`, `UPDATED_AT`, `UPDATED_BY`) VALUES
(5, 22, 'দোকানের ভাড়া সিটি কর্পোরেশন প্রতি ৩(তিন) বছর পর পর বৃদ্ধি বা পুনঃ নির্ধারণ করতে পারবে।', '<p>সিটি কর্পোরেশন মার্কেট উপ-আইনমালা ২০১৩ মোতাবেগ প্রণীত। k</p>', 1, '2016-07-16 21:44:44', 1, '2017-01-04 09:53:21', 1),
(6, 24, 'বাংলাদেশের প্রকৃত নাগরিকগন আবেদন করতে পারবে।', '<p>আবেদনকারীকে নারায়ণগঞ্জ সিটি কর্পোরেশনের নির্ধারিত ফরমে আবেদন করতে হবে।</p>', 1, '2016-07-16 21:48:52', 1, '2016-11-16 08:38:36', 1),
(7, 23, 'বরাদ্দকৃত দোকানের ভাড়া সিটি কর্পোরেশন প্রতি ৩ বছর পর পর বৃদ্ধি করতে পারবে।', '<p>আবেদনকারীকে নারায়ণগঞ্জ সিটি কর্পোরেশনের নির্ধারিত ফরমে আবেদন করতে হবে। আবেদনকারীকে নারায়ণগঞ্জ সিটি কর্পোরেশনের নির্ধারিত ফরমে আবেদন করতে হবে। আবেদনকারীকে নারায়ণগঞ্জ সিটি কর্পোরেশনের নির্ধারিত ফরমে আবেদন করতে হবে। আবেদনকারীকে নারায়ণগঞ্জ সিটি কর্পোরেশনের নির্ধারিত ফরমে আবেদন করতে হবে।</p><p><br></p>', 1, '2016-07-18 04:26:28', 1, '2017-01-04 09:53:07', 1),
(8, 21, 'রকল্পের শ্রেণীর শর্তের শিরোনাম', '<p>1. সিদ্ধিরগঞ্জ পৌরসভাকে বিলুপ্ত করে এর ১ থেকে ৯ নম্বর ওয়ার্ডকে সিটি করপোরেশনের ১ থেকে ৯ নম্বর ওয়ার্ড হিসাবে ঘোষণা করা হয়েছে। </p><p>2. সিদ্ধিরগঞ্জের পাইনাদী পূর্ব অংশ মিজমিজি বাতান পাড়াকে ১ নম্বর ওয়ার্ড করা হয়েছে। ১ থেকে ৩ নম্বর ওয়ার্ডকে সংরক্ষিত মহিলা আসন </p><p>3.পৌরসভার তিনাট মহিলা ওয়ার্ডকে সিটি করপোরেশনের ৭ থেকে ৯ নম্বর ওয়ার্ড হিসাবে ঘোষণা করা হয়।</p><p>4.তালিকাভুক্ত করা হয়েছে। এই পৌরসভার সংরক্ষিত তিনটি মহিলা ওয়ার্ডকে<strong> ৪ থেকে ৬ নম্বর</strong> ওয়ার্ড হিসাবে ঘোষণা করা হয়। বন্দর উপজেলার<br></p>', 1, '2016-10-03 04:37:23', 1, '2016-10-03 10:40:49', 1);

-- --------------------------------------------------------

--
-- Table structure for table `sa_category_particulars`
--

CREATE TABLE IF NOT EXISTS `sa_category_particulars` (
  `CAT_PART_ID` bigint(14) NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  `PR_CATEGORY` tinyint(2) NOT NULL COMMENT 'Primary Key of sa_category table',
  `PARTICULAR_ID` smallint(6) NOT NULL COMMENT 'Primary Key of ac_particular table',
  `PARTICULAR_AMT` double NOT NULL COMMENT 'Amount to be charged against each Particulars',
  `UOM` tinyint(2) NOT NULL COMMENT 'Unit Of Measure e.g. Percentage, Taka etc.',
  `REMARKS` varchar(200) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Remarks if have any',
  `IS_ACTIVE` tinyint(1) NOT NULL COMMENT '0=no, 1=yes',
  `CREATED_AT` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `CREATED_BY` int(10) NOT NULL,
  `UPDATED_AT` datetime DEFAULT NULL,
  `UPDATED_BY` int(10) DEFAULT NULL,
  PRIMARY KEY (`CAT_PART_ID`),
  KEY `FK_SCP_PR_CATEGORY` (`PR_CATEGORY`),
  KEY `FK_SCP_PARTICULAR_ID` (`PARTICULAR_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=17 ;

--
-- Dumping data for table `sa_category_particulars`
--

INSERT INTO `sa_category_particulars` (`CAT_PART_ID`, `PR_CATEGORY`, `PARTICULAR_ID`, `PARTICULAR_AMT`, `UOM`, `REMARKS`, `IS_ACTIVE`, `CREATED_AT`, `CREATED_BY`, `UPDATED_AT`, `UPDATED_BY`) VALUES
(2, 21, 3, 30, 1, '', 1, '2016-07-25 04:27:07', 1, '2016-07-28 09:11:09', 1),
(3, 22, 4, 2000, 2, '\r\nমন্তব্য\r\n', 1, '2016-07-25 04:28:20', 1, '2016-10-03 12:28:15', 1),
(4, 22, 3, 25, 1, '', 1, '2016-07-25 04:28:42', 1, '2016-07-25 10:28:42', NULL),
(5, 23, 4, 2500, 2, '', 1, '2016-07-25 04:29:08', 1, '2016-07-25 10:29:08', NULL),
(6, 23, 3, 28, 1, '', 1, '2016-07-25 04:43:31', 1, '2016-07-28 09:10:57', 1),
(8, 21, 4, 4500, 2, '', 1, '2016-07-25 05:37:21', 1, '2016-07-28 09:10:42', 1),
(9, 21, 5, 1200, 2, '', 1, '2016-08-01 01:01:13', 1, '2016-08-01 07:01:13', NULL),
(10, 22, 5, 10000, 2, '', 1, '2016-08-01 01:01:31', 1, '2016-08-01 07:01:31', NULL),
(11, 23, 5, 6000, 2, '', 1, '2016-08-01 01:01:49', 1, '2016-08-01 07:01:49', NULL),
(12, 24, 6, 100, 1, '', 1, '2016-08-29 03:19:34', 1, '2016-08-29 09:19:34', NULL),
(13, 24, 7, 500, 2, '<p>মন্তব্য</p>', 1, '2016-08-29 03:19:58', 1, '2016-10-09 11:27:25', 1),
(14, 24, 9, 15, 1, '', 1, '2016-10-25 06:43:39', 1, '2016-10-26 04:37:40', 1),
(15, 24, 10, 5, 1, '', 1, '2016-10-25 22:41:38', 1, '2016-10-26 04:41:38', NULL),
(16, 24, 3, 5, 1, '<p>প্রাথমিকভাবে নির্বাচিত আবেদন  কারির জামানতের টাকা</p>', 1, '2016-10-25 22:54:36', 1, '2016-10-26 04:54:36', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sa_chat`
--

CREATE TABLE IF NOT EXISTS `sa_chat` (
  `CHAT_ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary key of sa_chat table',
  `USER_ID` bigint(20) NOT NULL COMMENT 'Primary key of sa_users',
  `MESSAGE` text COMMENT 'Message text',
  `IS_ACTIVE` tinyint(1) NOT NULL DEFAULT '1',
  `CREATED_AT` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `UPDATED_AT` datetime DEFAULT NULL,
  PRIMARY KEY (`CHAT_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `sa_citizen`
--

CREATE TABLE IF NOT EXISTS `sa_citizen` (
  `CITIZEN_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `USER_ID` bigint(20) NOT NULL COMMENT 'Primary Key of sa_user table',
  `TE_APP_ID` bigint(20) NOT NULL COMMENT 'Primary key of tender_applicant table',
  `SPOUSE_NAME` varchar(100) CHARACTER SET utf8 NOT NULL COMMENT 'Citizen Spouse Name',
  `NID` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Citizen national id number',
  `DIVISION_ID` tinyint(2) NOT NULL COMMENT 'Primary key of sa_divisions table',
  `DISTRICT_ID` int(8) NOT NULL COMMENT 'Primary Key of district Table',
  `THANA_ID` smallint(8) NOT NULL COMMENT 'Primary Key of thana Table',
  `PO_ID` smallint(6) DEFAULT NULL COMMENT 'Primary Key of post_office Table',
  `ROAD_NO` varchar(100) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Road Number of Applicant',
  `HOLDING_NO` varchar(100) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Holding Number of Applicant',
  `IS_DEFAULTER` tinyint(1) NOT NULL DEFAULT '0',
  `IS_ACTIVE` tinyint(1) NOT NULL DEFAULT '0',
  `CREATED_AT` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `CREATED_BY` int(10) NOT NULL,
  `UPDATED_AT` datetime DEFAULT NULL,
  `UPDATED_BY` int(10) DEFAULT NULL,
  PRIMARY KEY (`CITIZEN_ID`),
  KEY `FK_CIT_USER_ID` (`USER_ID`),
  KEY `DIVISION_ID` (`DIVISION_ID`),
  KEY `DISTRICT_ID` (`DISTRICT_ID`),
  KEY `THANA_ID` (`THANA_ID`),
  KEY `TE_APP_ID` (`TE_APP_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=9 ;

--
-- Dumping data for table `sa_citizen`
--

INSERT INTO `sa_citizen` (`CITIZEN_ID`, `USER_ID`, `TE_APP_ID`, `SPOUSE_NAME`, `NID`, `DIVISION_ID`, `DISTRICT_ID`, `THANA_ID`, `PO_ID`, `ROAD_NO`, `HOLDING_NO`, `IS_DEFAULTER`, `IS_ACTIVE`, `CREATED_AT`, `CREATED_BY`, `UPDATED_AT`, `UPDATED_BY`) VALUES
(7, 18, 263, 'Maynuddin', '1245764215487', 5, 6, 251, NULL, '15', '52', 0, 1, '2016-11-13 06:48:56', 0, '2016-11-13 12:48:56', NULL),
(8, 19, 264, 'মোঃ আঃ হাকিম', '১২৩৪৫৬৭৮৯৯৫১২৫৮৯৩২৪', 3, 28, 113, NULL, 'নাই', 'পাটুঃ-৫৮৯', 0, 1, '2016-11-14 07:42:31', 0, '2016-11-14 13:42:31', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sa_citizen_application`
--

CREATE TABLE IF NOT EXISTS `sa_citizen_application` (
  `CITIZEN_APP_ID` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'Primary key',
  `CITIZEN_ID` bigint(20) NOT NULL COMMENT 'Primary key of sa_citizen table',
  `TENDER_ID` bigint(20) NOT NULL COMMENT 'Primary key of tender table',
  `SCHEDULE_ID` bigint(20) NOT NULL COMMENT 'Primary key of tender_schedule table',
  `PROJECT_ID` bigint(20) NOT NULL COMMENT 'Primary key of project table',
  `PR_DETAIL_ID` bigint(20) DEFAULT NULL COMMENT 'Primary key of project details',
  `PR_CATEGORY` tinyint(2) DEFAULT NULL COMMENT 'primary key of sa_category table',
  `PR_TYPE` tinyint(2) DEFAULT NULL COMMENT 'Primary key of sa_type table',
  `IS_TE_SC_BOUGHT` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0=no, 1=yes, 2=approved',
  `TE_SC_AMOUNT` double DEFAULT NULL COMMENT 'Tender schedule amount',
  `TE_SC_DATE` datetime DEFAULT NULL COMMENT 'Tender Schedule Buying Date',
  `BID_AMOUNT` double DEFAULT NULL COMMENT 'Propossed Bid Amount Submitted by Applicant',
  `BID_AMOUNT_TEXT` varchar(100) DEFAULT NULL COMMENT 'Propossed Bid Amount in text Submitted by Applicant',
  `BG_AMOUNT` double DEFAULT NULL COMMENT 'This may be the Tender Money or Security Money. This will be calculated in Percentage',
  `BG_AMOUNT_TEXT` varchar(100) DEFAULT NULL COMMENT 'Tender Money or Security Money in Text',
  `BG_PAYMENT_TYPE` tinyint(1) NOT NULL COMMENT '1=bank pay order, 2=online',
  `BG_PAYMENT_STATUS` tinyint(1) DEFAULT '0' COMMENT '0=pending, 1=approved',
  `BANK_ID` tinyint(3) DEFAULT NULL COMMENT 'Primary Key of bank table',
  `B_DRAFT_NO` varchar(150) DEFAULT NULL COMMENT 'Draft Number',
  `B_DRAFT_DATE` datetime DEFAULT NULL COMMENT 'Draft Number Submission date',
  `B_DRAFT_ATTACHMENT` varchar(100) DEFAULT NULL COMMENT 'Draft as attachment',
  `REMARKS` text COMMENT 'Comments',
  `INITIAL_REMARKS` text COMMENT 'Initial selected remarks',
  `CANCEL_REMARKS` text COMMENT 'Remarks for cancel tender application',
  `IS_SELECTED` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0 = Determine that the applicant is not selected, 1 = Initial selected, 2 = Finally selected.',
  `IS_LEASE` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0=no, 1=yes',
  `IS_CANCEL` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=active=1=cancel tender application',
  `IS_CITIZEN` tinyint(1) NOT NULL DEFAULT '1',
  `IS_VIEW` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Determine if view notification',
  `IS_ACTIVE` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0=no, 1=yes',
  `CREATED_AT` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `CREATED_BY` bigint(20) NOT NULL,
  `UPDATED_AT` datetime DEFAULT NULL,
  `UPDATED_BY` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`CITIZEN_APP_ID`),
  KEY `CITIZEN_ID` (`CITIZEN_ID`),
  KEY `TENDER_ID` (`TENDER_ID`),
  KEY `SCHEDULE_ID` (`SCHEDULE_ID`),
  KEY `PROJECT_ID` (`PROJECT_ID`),
  KEY `PR_DETAIL_ID` (`PR_DETAIL_ID`),
  KEY `PR_CATEGORY` (`PR_CATEGORY`),
  KEY `PR_TYPE` (`PR_TYPE`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `sa_citizen_application`
--

INSERT INTO `sa_citizen_application` (`CITIZEN_APP_ID`, `CITIZEN_ID`, `TENDER_ID`, `SCHEDULE_ID`, `PROJECT_ID`, `PR_DETAIL_ID`, `PR_CATEGORY`, `PR_TYPE`, `IS_TE_SC_BOUGHT`, `TE_SC_AMOUNT`, `TE_SC_DATE`, `BID_AMOUNT`, `BID_AMOUNT_TEXT`, `BG_AMOUNT`, `BG_AMOUNT_TEXT`, `BG_PAYMENT_TYPE`, `BG_PAYMENT_STATUS`, `BANK_ID`, `B_DRAFT_NO`, `B_DRAFT_DATE`, `B_DRAFT_ATTACHMENT`, `REMARKS`, `INITIAL_REMARKS`, `CANCEL_REMARKS`, `IS_SELECTED`, `IS_LEASE`, `IS_CANCEL`, `IS_CITIZEN`, `IS_VIEW`, `IS_ACTIVE`, `CREATED_AT`, `CREATED_BY`, `UPDATED_AT`, `UPDATED_BY`) VALUES
(3, 7, 49, 128, 64, 72, 22, 10, 0, 2000, NULL, 12000, 'বার হাজার টাকা মাত্র', 3000, 'তিন হাজার ছয়শ টাকা মাত্র', 1, 0, 8, '৫৪৫৪৫৪৫৭৮৭৮', '2016-11-11 00:00:00', 'bank-582954320bb05.jpeg', NULL, '', NULL, 2, 0, 0, 1, 0, 0, '2016-11-14 00:05:42', 0, '2017-04-11 18:30:32', 1),
(4, 7, 48, 124, 66, NULL, 24, NULL, 0, 500, NULL, 12000, 'বার হাজার টাকা মাত্র', 12000, 'বার হাজার টাকা মাত্র', 2, 0, NULL, NULL, NULL, NULL, NULL, '', NULL, 2, 1, 0, 1, 0, 0, '2016-11-14 22:51:57', 0, '2016-12-21 11:10:57', 6),
(5, 7, 54, 141, 64, 72, 22, 10, 0, 2000, NULL, 12000, 'বার হাজার টাকা মাত্র', 3000, 'বার হাজার টাকা মাত্র', 2, 0, NULL, NULL, NULL, NULL, NULL, '', NULL, 1, 0, 0, 1, 0, 0, '2016-11-28 03:33:32', 0, '2017-04-15 01:23:56', 1),
(6, 7, 54, 141, 64, 72, 22, 10, 0, 2000, NULL, 12000, 'বার হাজার টাকা মাত্র', 3000, 'বার হাজার টাকা মাত্র', 2, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 1, 0, 0, '2016-12-17 02:46:10', 0, '2017-04-15 01:38:58', 1),
(7, 7, 54, 145, 64, 73, 23, 10, 0, 2500, NULL, 12000, 'বার হাজার টাকা মাত্র', 3360, 'বার হাজার টাকা মাত্র', 2, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 1, 0, 1, '2017-01-31 02:27:06', 0, '2017-01-31 08:27:06', NULL),
(8, 7, 54, 139, 64, 70, 21, 10, 0, 4500, NULL, 232323, 'asdfdfdf', 69697, 'fggdghg', 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 1, 0, 1, '2017-04-11 11:46:07', 0, '2017-04-11 17:46:07', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sa_citizen_property`
--

CREATE TABLE IF NOT EXISTS `sa_citizen_property` (
  `CIT_PRO_ID` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  `CITIZEN_ID` bigint(20) NOT NULL COMMENT 'Primary key of sa_citizen',
  `TENDER_ID` bigint(20) NOT NULL COMMENT 'Primary Key of tender table',
  `SCHEDULE_ID` bigint(20) NOT NULL COMMENT 'Primary key of tender_schedule',
  `PROJECT_ID` bigint(20) NOT NULL COMMENT 'Primary Key of project table',
  `PR_DETAIL_ID` bigint(20) DEFAULT NULL COMMENT 'Primary Key of project_details table',
  `PR_CATEGORY` tinyint(2) NOT NULL COMMENT 'Flat, Shop, Space or Lease',
  `PR_TYPE` tinyint(2) DEFAULT NULL COMMENT 'Flat = Type A, Space = Unit A, Shop = 1 etc',
  `IS_LEASE` tinyint(1) DEFAULT '0' COMMENT '0=no, 1=yes',
  `DEFAULTER` tinyint(1) DEFAULT '0',
  `IS_ACTIVE` tinyint(1) NOT NULL COMMENT '0=no, 1=yes',
  `CREATED_AT` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `CREATED_BY` int(10) NOT NULL,
  `UPDATED_AT` datetime DEFAULT NULL,
  `UPDATED_BY` int(10) DEFAULT NULL,
  PRIMARY KEY (`CIT_PRO_ID`),
  KEY `FK_CP_PROJECT_ID` (`PROJECT_ID`),
  KEY `FK_CP_TENDER_ID` (`TENDER_ID`),
  KEY `FK_CP_PR_CATEGORY` (`PR_CATEGORY`),
  KEY `FK_CP_PR_TYPE` (`PR_TYPE`),
  KEY `sa_citizen_property_ibfk_2` (`SCHEDULE_ID`),
  KEY `sa_citizen_property_ibfk_1_idx` (`CITIZEN_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=11 ;

--
-- Dumping data for table `sa_citizen_property`
--

INSERT INTO `sa_citizen_property` (`CIT_PRO_ID`, `CITIZEN_ID`, `TENDER_ID`, `SCHEDULE_ID`, `PROJECT_ID`, `PR_DETAIL_ID`, `PR_CATEGORY`, `PR_TYPE`, `IS_LEASE`, `DEFAULTER`, `IS_ACTIVE`, `CREATED_AT`, `CREATED_BY`, `UPDATED_AT`, `UPDATED_BY`) VALUES
(4, 7, 48, 124, 66, NULL, 24, NULL, 1, 0, 1, '2016-11-13 06:48:56', 0, '2016-11-13 12:48:56', NULL),
(5, 8, 48, 124, 66, NULL, 24, NULL, 1, 0, 1, '2016-11-14 07:42:31', 0, '2016-11-14 13:42:31', NULL),
(6, 7, 49, 128, 64, 72, 22, 10, 0, 0, 0, '2016-11-21 06:34:26', 1, '2017-04-15 11:37:12', 1),
(7, 7, 49, 128, 64, 72, 22, 10, 0, 0, 1, '2016-12-21 00:53:05', 7, '2016-12-21 06:53:05', NULL),
(9, 7, 48, 124, 66, NULL, 24, NULL, 1, 0, 1, '2016-12-21 05:10:57', 6, '2016-12-21 11:10:57', NULL),
(10, 7, 49, 128, 64, 72, 22, 10, 0, 0, 1, '2017-04-11 12:30:32', 1, '2017-04-11 18:30:32', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sa_districts`
--

CREATE TABLE IF NOT EXISTS `sa_districts` (
  `DISTRICT_ID` int(8) NOT NULL AUTO_INCREMENT COMMENT 'Primary Key of sa_districts Table.',
  `DIVISION_ID` tinyint(2) DEFAULT NULL COMMENT 'Devision Id , Foreign Key To DIVISION_ID Column Of sa_divisions Table.',
  `DISTRICT_ENAME` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'District Name In English.',
  `DISTRICT_LNAME` varchar(120) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'District Name In Local Language.',
  `UD_DISTRICT_CODE` varchar(6) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'User Define District Code.',
  `ORDER_SL` tinyint(2) DEFAULT '0' COMMENT 'Ascending Serial No.',
  `IS_ACTIVE` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0=NO, 1=YES',
  `CREATED_BY` int(11) NOT NULL,
  `CREATED_AT` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `UPDATED_AT` datetime DEFAULT NULL,
  `UPDATED_BY` int(10) DEFAULT NULL,
  `TAREA_STATUS` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Thanas Or Upazilla Area Status, T=Thana, U=Upazilla, B=Both',
  PRIMARY KEY (`DISTRICT_ID`),
  KEY `SA_DIST_DIVISION_ID` (`DIVISION_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=87 ;

--
-- Dumping data for table `sa_districts`
--

INSERT INTO `sa_districts` (`DISTRICT_ID`, `DIVISION_ID`, `DISTRICT_ENAME`, `DISTRICT_LNAME`, `UD_DISTRICT_CODE`, `ORDER_SL`, `IS_ACTIVE`, `CREATED_BY`, `CREATED_AT`, `UPDATED_AT`, `UPDATED_BY`, `TAREA_STATUS`) VALUES
(1, 4, 'BAGERHAT', NULL, '0401', 0, 1, 0, '2016-10-15 05:26:14', NULL, NULL, 'U'),
(2, 2, 'BANDARBAN', NULL, '0203', 0, 1, 0, '2016-10-15 05:26:14', NULL, NULL, 'U'),
(3, 1, 'BARGUNA', NULL, '0104', 0, 1, 0, '2016-10-15 05:26:14', NULL, NULL, 'U'),
(4, 1, 'BARISAL', NULL, '0106', 0, 1, 0, '2016-10-15 05:26:14', NULL, NULL, 'B'),
(5, 1, 'BHOLA', NULL, '0109', 0, 1, 0, '2016-10-15 05:26:14', NULL, NULL, 'U'),
(6, 5, 'BOGRA', NULL, '0510', 0, 1, 0, '2016-10-15 05:26:14', NULL, NULL, 'U'),
(7, 2, 'BRAHAMANBARIA', NULL, '0212', 0, 1, 0, '2016-10-15 05:26:14', NULL, NULL, 'U'),
(8, 2, 'CHANDPUR', NULL, '0213', 0, 1, 0, '2016-10-15 05:26:14', NULL, NULL, 'U'),
(9, 2, 'CHITTAGONG                ', NULL, '0215', 0, 1, 0, '2016-10-15 05:26:14', NULL, NULL, 'B'),
(10, 4, 'CHUADANGA', NULL, '0418', 0, 1, 0, '2016-10-15 05:26:14', NULL, NULL, 'U'),
(11, 2, 'COMILLA', NULL, '0219', 0, 1, 0, '2016-10-15 05:26:14', NULL, NULL, 'U'),
(12, 2, 'COX''S BAZAR', NULL, '0222', 0, 1, 0, '2016-10-15 05:26:14', NULL, NULL, 'U'),
(13, 3, 'DHAKA', NULL, '0326', 0, 1, 0, '2016-10-15 05:26:14', NULL, NULL, 'B'),
(14, 8, 'DINAJPUR', NULL, '0527', 0, 1, 0, '2016-10-15 05:26:14', NULL, NULL, 'U'),
(15, 3, 'FARIDPUR', NULL, '0329', 0, 1, 0, '2016-10-15 05:26:14', NULL, NULL, 'U'),
(16, 2, 'FENI', NULL, '0230', 0, 1, 0, '2016-10-15 05:26:14', NULL, NULL, 'U'),
(17, 8, 'GAIBANDHA', NULL, '0532', 0, 1, 0, '2016-10-15 05:26:14', NULL, NULL, 'U'),
(18, 3, 'GAZIPUR', NULL, '0333', 0, 1, 0, '2016-10-15 05:26:14', NULL, NULL, 'U'),
(19, 3, 'GOPALGANJ', NULL, '0335', 0, 1, 0, '2016-10-15 05:26:14', NULL, NULL, 'U'),
(20, 6, 'HABIGANJ', NULL, '0636', 0, 1, 0, '2016-10-15 05:26:14', NULL, NULL, 'U'),
(21, 5, 'JOYPURHAT', NULL, '0538', 0, 1, 0, '2016-10-15 05:26:14', NULL, NULL, 'U'),
(22, 3, 'JAMALPUR', NULL, '0339', 0, 1, 0, '2016-10-15 05:26:14', NULL, NULL, 'U'),
(23, 4, 'JESSORE', NULL, '0441', 0, 1, 0, '2016-10-15 05:26:14', NULL, NULL, 'B'),
(24, 1, 'JHALOKATI', NULL, '0142', 0, 1, 0, '2016-10-15 05:26:14', NULL, NULL, 'U'),
(25, 4, 'JHENAIDAH', NULL, '0444', 0, 1, 0, '2016-10-15 05:26:14', NULL, NULL, 'U'),
(26, 2, 'KHAGRACHHARI', NULL, '0246', 0, 1, 0, '2016-10-15 05:26:14', NULL, NULL, 'U'),
(27, 4, 'KHULNA', NULL, '0447', 0, 1, 0, '2016-10-15 05:26:14', NULL, NULL, 'B'),
(28, 3, 'KISHOREGANJ', NULL, '0348', 0, 1, 0, '2016-10-15 05:26:14', NULL, NULL, 'U'),
(29, 8, 'KURIGRAM', NULL, '0549', 0, 1, 0, '2016-10-15 05:26:14', NULL, NULL, 'U'),
(30, 4, 'KUSHTIA', NULL, '0450', 0, 1, 0, '2016-10-15 05:26:14', NULL, NULL, 'B'),
(31, 2, 'LAKSHMIPUR', NULL, '0251', 0, 1, 0, '2016-10-15 05:26:14', NULL, NULL, 'U'),
(32, 8, 'LALMONIRHAT', NULL, '0552', 0, 1, 0, '2016-10-15 05:26:14', NULL, NULL, 'U'),
(33, 3, 'MADARIPUR', NULL, '0354', 0, 1, 0, '2016-10-15 05:26:14', NULL, NULL, 'U'),
(34, 4, 'MAGURA', NULL, '0455', 0, 1, 0, '2016-10-15 05:26:14', NULL, NULL, 'B'),
(35, 3, 'MANIKGANJ', NULL, '0356', 0, 1, 0, '2016-10-15 05:26:14', NULL, NULL, 'U'),
(36, 4, 'MEHERPUR', NULL, '0457', 0, 1, 0, '2016-10-15 05:26:14', NULL, NULL, 'U'),
(37, 6, 'MAULVIBAZAR', NULL, '0658', 0, 1, 0, '2016-10-15 05:26:14', NULL, NULL, 'U'),
(38, 3, 'MUNSHIGANJ', NULL, '0359', 0, 1, 0, '2016-10-15 05:26:14', NULL, NULL, 'U'),
(39, 3, 'MYMENSINGH', NULL, '0361', 0, 1, 0, '2016-10-15 05:26:14', NULL, NULL, 'U'),
(40, 5, 'NAOGAON', NULL, '0564', 0, 1, 0, '2016-10-15 05:26:14', NULL, NULL, 'U'),
(41, 4, 'NORAIL', NULL, '0465', 0, 1, 0, '2016-10-15 05:26:14', NULL, NULL, 'U'),
(42, 3, 'NARAYANGANJ', NULL, '0367', 0, 1, 0, '2016-10-15 05:26:14', NULL, NULL, 'U'),
(43, 3, 'NARSINGDI', NULL, '0368', 0, 1, 0, '2016-10-15 05:26:14', NULL, NULL, 'U'),
(44, 5, 'NATORE', NULL, '0569', 0, 1, 0, '2016-10-15 05:26:14', NULL, NULL, 'U'),
(45, 5, 'NAWABGANJ', NULL, '0570', 0, 1, 0, '2016-10-15 05:26:14', NULL, NULL, 'U'),
(46, 3, 'NETRAKONA', NULL, '0372', 0, 1, 0, '2016-10-15 05:26:14', NULL, NULL, 'U'),
(47, 8, 'NILPHAMARI', NULL, '0573', 0, 1, 0, '2016-10-15 05:26:14', NULL, NULL, 'U'),
(48, 2, 'NOAKHALI', NULL, '0275', 0, 1, 0, '2016-10-15 05:26:14', NULL, NULL, 'U'),
(49, 5, 'PABNA', NULL, '0576', 0, 1, 0, '2016-10-15 05:26:14', NULL, NULL, 'U'),
(50, 8, 'PANCHAGARH', NULL, '0577', 0, 1, 0, '2016-10-15 05:26:14', NULL, NULL, 'U'),
(51, 1, 'PATUAKHALI', NULL, '0178', 0, 1, 0, '2016-10-15 05:26:14', NULL, NULL, 'U'),
(52, 1, 'PIROJPUR', NULL, '0179', 0, 1, 0, '2016-10-15 05:26:14', NULL, NULL, 'U'),
(53, 5, 'RAJSHAHI', NULL, '0581', 0, 1, 0, '2016-10-15 05:26:14', NULL, NULL, 'B'),
(54, 3, 'RAJBARI', NULL, '0382', 0, 1, 0, '2016-10-15 05:26:14', NULL, NULL, 'U'),
(55, 2, 'RANGAMATI', NULL, '0284', 0, 1, 0, '2016-10-15 05:26:14', NULL, NULL, 'U'),
(56, 8, 'RANGPUR', NULL, '0585', 0, 1, 0, '2016-10-15 05:26:14', NULL, NULL, 'U'),
(57, 3, 'SHARIATPUR', NULL, '0386', 0, 1, 0, '2016-10-15 05:26:14', NULL, NULL, 'U'),
(58, 4, 'SATKHIRA', NULL, '0487', 0, 1, 0, '2016-10-15 05:26:14', NULL, NULL, 'U'),
(59, 5, 'SIRAJGANJ', NULL, '0588', 0, 1, 0, '2016-10-15 05:26:14', NULL, NULL, 'U'),
(60, 3, 'SHERPUR', NULL, '0389', 0, 1, 0, '2016-10-15 05:26:14', NULL, NULL, 'U'),
(61, 6, 'SUNAMGANJ', NULL, '0690', 0, 1, 0, '2016-10-15 05:26:14', NULL, NULL, 'U'),
(62, 6, 'SYLHET', NULL, '0691', 0, 1, 0, '2016-10-15 05:26:14', NULL, NULL, 'B'),
(63, 3, 'TANGAIL', NULL, '0393', 0, 1, 0, '2016-10-15 05:26:14', NULL, NULL, 'U'),
(64, 8, 'THAKURGAON', NULL, '0594', 0, 1, 0, '2016-10-15 05:26:14', NULL, NULL, 'U'),
(85, 13, 'District Name ', NULL, NULL, 0, 1, 0, '2016-10-15 05:26:14', NULL, NULL, NULL),
(86, 5, 'Chapinawabganj', NULL, NULL, 0, 1, 0, '2016-10-15 05:26:14', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sa_divisions`
--

CREATE TABLE IF NOT EXISTS `sa_divisions` (
  `DIVISION_ID` tinyint(2) NOT NULL AUTO_INCREMENT COMMENT 'Primary Key Of sa_divisions Table.',
  `DIVISION_ENAME` varchar(50) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Devision Name In English.',
  `DIVISION_LNAME` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Devision Name In Local Language.',
  `ORDER_SL` tinyint(3) DEFAULT '0' COMMENT 'Ascending Serial No.',
  `UD_DIVISION_CODE` varchar(2) COLLATE utf8_unicode_ci NOT NULL COMMENT 'User Define Devision Code.',
  `IS_ACTIVE` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0=NO, 1=YES',
  `CREATED_BY` int(11) NOT NULL,
  `CREATED_AT` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `UPDATED_AT` datetime DEFAULT NULL,
  `UPDATED_BY` int(10) DEFAULT NULL,
  PRIMARY KEY (`DIVISION_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=14 ;

--
-- Dumping data for table `sa_divisions`
--

INSERT INTO `sa_divisions` (`DIVISION_ID`, `DIVISION_ENAME`, `DIVISION_LNAME`, `ORDER_SL`, `UD_DIVISION_CODE`, `IS_ACTIVE`, `CREATED_BY`, `CREATED_AT`, `UPDATED_AT`, `UPDATED_BY`) VALUES
(1, 'BARISAL', NULL, 0, '01', 1, 1, '2015-05-20 03:22:37', '2015-09-13 10:39:43', NULL),
(2, 'CHITTAGONG', NULL, 0, '02', 1, 1, '2015-05-20 03:22:37', '2015-09-13 10:39:43', NULL),
(3, 'DHAKA', NULL, 0, '03', 1, 1, '2015-05-20 03:22:37', '2015-09-13 10:39:43', NULL),
(4, 'KHULNA', NULL, 0, '04', 1, 1, '2015-05-20 03:22:37', '2015-09-13 10:39:43', NULL),
(5, 'RAJSHAHI', NULL, 0, '05', 1, 1, '2015-05-20 03:22:37', '2015-09-13 10:39:43', NULL),
(6, 'SYLHET', NULL, 0, '06', 1, 1, '2015-05-20 03:22:37', '2015-09-13 10:39:43', NULL),
(8, 'RANGPUR', NULL, 0, '07', 1, 1, '2015-05-20 03:22:37', '2015-09-13 10:39:43', NULL),
(13, 'MAYMANSING', NULL, 0, '', 1, 1, '2016-07-24 08:34:05', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sa_floor`
--

CREATE TABLE IF NOT EXISTS `sa_floor` (
  `FLOOR_ID` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'Primary key',
  `FLOOR_NUMBER` varchar(100) NOT NULL COMMENT 'Floor english name',
  `FLOOR_NUMBER_BN` varchar(100) NOT NULL COMMENT 'Floor bangla name',
  `DETAILS` text COMMENT 'Floor details',
  `IS_ACTIVE` int(11) NOT NULL DEFAULT '1' COMMENT '0=no, 1=yes',
  `CREATED_AT` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `CREATED_BY` bigint(20) NOT NULL,
  `UPDATED_AT` datetime DEFAULT NULL,
  `UPDATED_BY` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`FLOOR_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `sa_floor`
--

INSERT INTO `sa_floor` (`FLOOR_ID`, `FLOOR_NUMBER`, `FLOOR_NUMBER_BN`, `DETAILS`, `IS_ACTIVE`, `CREATED_AT`, `CREATED_BY`, `UPDATED_AT`, `UPDATED_BY`) VALUES
(1, '1st Floor', '১ম তলা', '', 1, '2016-10-15 03:17:11', 1, '2016-11-23 09:35:39', 5),
(4, '2nd Floor', '২য় তলা', '', 1, '2016-10-15 03:42:54', 1, '2016-11-23 09:35:33', 5),
(5, '3rd Floor', '৩য় তলা', '', 1, '2016-10-15 03:45:41', 1, '2016-11-23 09:35:26', 5);

-- --------------------------------------------------------

--
-- Table structure for table `sa_location`
--

CREATE TABLE IF NOT EXISTS `sa_location` (
  `LOCATION_ID` int(10) NOT NULL AUTO_INCREMENT,
  `LOCATION_NAME` varchar(200) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Name of the Location',
  `LOCATION_DESC` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'Description of the Location',
  `IS_ACTIVE` tinyint(1) NOT NULL COMMENT '0=no, 1=yes',
  `CREATED_AT` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `CREATED_BY` int(10) NOT NULL,
  `UPDATED_AT` datetime DEFAULT NULL,
  `UPDATED_BY` int(10) DEFAULT NULL,
  PRIMARY KEY (`LOCATION_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=11 ;

--
-- Dumping data for table `sa_location`
--

INSERT INTO `sa_location` (`LOCATION_ID`, `LOCATION_NAME`, `LOCATION_DESC`, `IS_ACTIVE`, `CREATED_AT`, `CREATED_BY`, `UPDATED_AT`, `UPDATED_BY`) VALUES
(7, 'হিসাব বিভাগ,নগর ভবন,নারায়ণগঞ্জ সিটি কর্পোরেশন নারায়ণগঞ্জ।', 'হিসাব বিভাগ,নগর ভবন,নারায়ণগঞ্জ সিটি কর্পোরেশন নারায়ণগঞ্জ।', 1, '2016-09-06 00:28:18', 1, '2016-09-06 06:28:18', NULL),
(8, 'উপ-পরিচালক,স্থানীয় সরকার শাখা,জেলা প্রশাসকের কার্যালয়,নারায়ণগঞ্জ।', 'উপ-পরিচালক,স্থানীয় সরকার শাখা,জেলা প্রশাসকের কার্যালয়,নারায়ণগঞ্জ।', 1, '2016-09-06 00:30:56', 1, '2016-09-06 06:30:56', NULL),
(9, 'সচিব স্থানীয় সরকার বিভাগ,স্থানীয় সরকার পল্লী উন্নয়ন ও সমবায় মন্ত্রণালয়,বাংলাদেশ সচিবালয়,ঢাকা  ', 'সচিব স্থানীয় সরকার বিভাগ,স্থানীয় সরকার পল্লী উন্নয়ন ও সমবায় মন্ত্রণালয়,বাংলাদেশ সচিবালয়,ঢাকা  ', 1, '2016-09-06 00:33:15', 1, '2016-09-06 06:33:15', NULL),
(10, 'জেলা প্রশাসক,নারায়ণগঞ্জ।', 'জেলা প্রশাসক,নারায়ণগঞ্জ।', 1, '2016-09-06 00:34:10', 1, '2016-09-06 06:34:10', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sa_mouza`
--

CREATE TABLE IF NOT EXISTS `sa_mouza` (
  `MOUZA_ID` int(10) NOT NULL AUTO_INCREMENT,
  `WARD_ID` int(10) NOT NULL,
  `MOUZA_NAME` varchar(150) CHARACTER SET utf8 DEFAULT NULL,
  `MOUZA_NAME_BN` varchar(500) CHARACTER SET utf8 NOT NULL,
  `JL_NO` varchar(30) CHARACTER SET utf8 DEFAULT NULL,
  `IS_ACTIVE` tinyint(1) NOT NULL COMMENT '0=NO, 1=YES',
  `CREATED_AT` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `CREATED_BY` int(10) NOT NULL,
  `UPDATED_AT` datetime DEFAULT NULL,
  `UPDATED_BY` int(10) DEFAULT NULL,
  PRIMARY KEY (`MOUZA_ID`),
  KEY `SA_MOUZA_WARD_ID` (`WARD_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=14 ;

--
-- Dumping data for table `sa_mouza`
--

INSERT INTO `sa_mouza` (`MOUZA_ID`, `WARD_ID`, `MOUZA_NAME`, `MOUZA_NAME_BN`, `JL_NO`, `IS_ACTIVE`, `CREATED_AT`, `CREATED_BY`, `UPDATED_AT`, `UPDATED_BY`) VALUES
(10, 16, 'Shimrail', 'শিমরাইল', '3456', 1, '2016-07-19 05:26:30', 1, '2016-07-19 11:26:30', NULL),
(12, 18, 'Agibpur', 'আজিবপুর', '5678', 1, '2016-07-19 05:30:56', 1, '2016-07-19 11:30:56', NULL),
(13, 19, 'Shidirganj', 'সিদ্ধিরগাঞ্জ', '6578', 1, '2016-07-19 05:32:11', 1, '2016-07-19 11:32:11', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sa_notifications`
--

CREATE TABLE IF NOT EXISTS `sa_notifications` (
  `NOTIFICATION_ID` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'Primary key',
  `TE_APP_ID` bigint(20) DEFAULT NULL COMMENT 'Primary key of tender table',
  `USER_ID` bigint(20) DEFAULT NULL COMMENT 'Primary key of sa_users',
  `CITIZEN_ID` bigint(20) DEFAULT NULL COMMENT 'Primary key of sa_citizen table',
  `LEASE_DE_ID` bigint(20) DEFAULT NULL COMMENT 'Primary key of lease_demesne table',
  `SCHEDULE_ID` bigint(20) DEFAULT NULL COMMENT 'Primary key of tender schedule table',
  `PARTICULAR_ID` smallint(6) DEFAULT NULL COMMENT 'Primary key of ac_particular table',
  `TRX_TRAN_NO` bigint(20) DEFAULT NULL COMMENT 'Primary key of ac_voucherchd table',
  `TITLE` varchar(255) NOT NULL COMMENT 'Notification title',
  `DETAILS` text COMMENT 'Details if any',
  `SEND_MAIL` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Determine if email send',
  `SEND_SMS` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Determine if SMS send',
  `IS_VIEW` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Determine if view notification',
  `IS_ACTIVE` tinyint(1) NOT NULL DEFAULT '1' COMMENT '0=no, 1=yes',
  `CREATED_BY` bigint(20) DEFAULT NULL,
  `CREATED_AT` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `UPDATED_AT` datetime DEFAULT NULL,
  `UPDATED_BY` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`NOTIFICATION_ID`),
  KEY `TE_APP_ID` (`TE_APP_ID`),
  KEY `CITIZEN_ID` (`CITIZEN_ID`),
  KEY `LEASE_DE_ID` (`LEASE_DE_ID`),
  KEY `SCHEDULE_ID` (`SCHEDULE_ID`),
  KEY `PARTICULAR_ID` (`PARTICULAR_ID`),
  KEY `TRX_TRAN_NO` (`TRX_TRAN_NO`),
  KEY `USER_ID` (`USER_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `sa_notifications`
--

INSERT INTO `sa_notifications` (`NOTIFICATION_ID`, `TE_APP_ID`, `USER_ID`, `CITIZEN_ID`, `LEASE_DE_ID`, `SCHEDULE_ID`, `PARTICULAR_ID`, `TRX_TRAN_NO`, `TITLE`, `DETAILS`, `SEND_MAIL`, `SEND_SMS`, `IS_VIEW`, `IS_ACTIVE`, `CREATED_BY`, `CREATED_AT`, `UPDATED_AT`, `UPDATED_BY`) VALUES
(1, NULL, NULL, 7, NULL, 145, NULL, NULL, 'সিডিউলের মূল্য পরিশোধ করুন।', 'আপনার আবেদনকৃত পদ্ম সিটি প্লাজা-১ এর ৫ তলার 3A নং স্পেস এর জন্য সিডিউলের মূল্য ২,৫০০.০০ টাকা এবং জামানত ৩,৩৬০.০০ টাকা পরিশোধ করুন।', 1, 1, 1, 1, NULL, '2017-01-31 11:52:47', '2017-01-31 11:52:46', NULL),
(2, 271, NULL, NULL, NULL, 138, NULL, NULL, 'সিডিউলের মূল্য পরিশোধ করুন।', 'আপনার আবেদনকৃত পদ্ম সিটি প্লাজা-১ এর ১ তলার 1B নং দোকান এর জন্য সিডিউলের মূল্য ৪,৫০০.০০ টাকা এবং জামানত ৩০০,০০০.০০ টাকা পরিশোধ করুন।', 1, 1, 0, 1, NULL, '2017-03-08 00:40:35', '2017-03-08 06:40:35', NULL),
(3, 274, NULL, NULL, NULL, 139, NULL, NULL, 'সিডিউলের মূল্য পরিশোধ করুন।', 'আপনার আবেদনকৃত পদ্ম সিটি প্লাজা-১ এর ১ তলার 1A নং দোকান এর জন্য সিডিউলের মূল্য ৪,৫০০.০০ টাকা এবং জামানত ১৫০.০০ টাকা পরিশোধ করুন।', 1, 1, 0, 1, NULL, '2017-03-12 01:27:29', '2017-03-12 07:27:29', NULL),
(4, 276, NULL, NULL, NULL, 138, NULL, NULL, 'সিডিউলের মূল্য পরিশোধ করুন।', 'আপনার আবেদনকৃত পদ্ম সিটি প্লাজা-১ এর ১ তলার 1B নং দোকান এর জন্য সিডিউলের মূল্য ৪,৫০০.০০ টাকা এবং জামানত ৩,৬০০.০০ টাকা পরিশোধ করুন।আপনার লগইন আইডিঃ 01966180999 এবং পাসওয়ার্ডঃ 123456 ব্যবহার করুন', 1, 1, 0, 1, NULL, '2017-03-29 11:18:40', '2017-03-29 17:18:40', NULL),
(5, NULL, NULL, 7, NULL, 139, NULL, NULL, 'সিডিউলের মূল্য পরিশোধ করুন।', 'আপনার আবেদনকৃত পদ্ম সিটি প্লাজা-১ এর ১ তলার 1A নং দোকান এর জন্য সিডিউলের মূল্য ৪,৫০০.০০ টাকা এবং জামানত ৬৯,৬৯৭.০০ টাকা পরিশোধ করুন।', 1, 1, 0, 1, NULL, '2017-04-11 11:46:07', '2017-04-11 17:46:07', NULL),
(6, 277, NULL, NULL, NULL, 139, NULL, NULL, 'সিডিউলের মূল্য পরিশোধ করুন।', 'আপনার আবেদনকৃত পদ্ম সিটি প্লাজা-১ এর ১ তলার 1A নং দোকান এর জন্য সিডিউলের মূল্য ৪,৫০০.০০ টাকা এবং জামানত ৩,৬০০.০০ টাকা পরিশোধ করুন।আপনার লগইন আইডিঃ 01749808288 এবং পাসওয়ার্ডঃ 123456 ব্যবহার করুন', 1, 1, 0, 1, NULL, '2017-04-11 11:52:11', '2017-04-11 17:52:11', NULL),
(7, 278, NULL, NULL, NULL, 139, NULL, NULL, 'সিডিউলের মূল্য পরিশোধ করুন।', 'আপনার আবেদনকৃত পদ্ম সিটি প্লাজা-১ এর ১ তলার 1A নং দোকান এর জন্য সিডিউলের মূল্য ৪,৫০০.০০ টাকা এবং জামানত ৩,৬০০.০০ টাকা পরিশোধ করুন।আপনার লগইন আইডিঃ 01722085398 এবং পাসওয়ার্ডঃ 123456 ব্যবহার করুন', 1, 1, 0, 1, NULL, '2017-04-15 05:07:10', '2017-04-15 11:07:10', NULL),
(8, 279, NULL, NULL, NULL, 138, NULL, NULL, 'সিডিউলের মূল্য পরিশোধ করুন।', 'আপনার আবেদনকৃত পদ্ম সিটি প্লাজা-১ এর ১ তলার 1B নং দোকান এর জন্য সিডিউলের মূল্য ৪,৫০০.০০ টাকা এবং জামানত ৪৫০.০০ টাকা পরিশোধ করুন।আপনার লগইন আইডিঃ 01624884262 এবং পাসওয়ার্ডঃ 123456 ব্যবহার করুন', 1, 1, 0, 1, NULL, '2017-04-16 02:47:17', '2017-04-16 08:47:17', NULL),
(9, 267, NULL, NULL, NULL, 145, NULL, NULL, 'সিডিউলের মূল্য পরিশোধ করুন।', 'আপনার আবেদনকৃত পদ্ম সিটি প্লাজা-১ এর ৫ তলার 3A নং স্পেস এর জন্য সিডিউলের মূল্য ২,৫০০.০০ টাকা এবং জামানত ৪,৪৭০.০০ টাকা পরিশোধ করুন।', 1, 1, 0, 1, NULL, '2017-04-16 02:51:44', '2017-04-16 08:51:44', NULL),
(10, 267, NULL, NULL, NULL, 139, NULL, NULL, 'সিডিউলের মূল্য পরিশোধ করুন।', 'আপনার আবেদনকৃত পদ্ম সিটি প্লাজা-১ এর ১ তলার 1A নং দোকান এর জন্য সিডিউলের মূল্য ৪,৫০০.০০ টাকা এবং জামানত ১৬৭.০০ টাকা পরিশোধ করুন।', 1, 1, 0, 1, NULL, '2017-04-16 03:02:44', '2017-04-16 09:02:44', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sa_officer`
--

CREATE TABLE IF NOT EXISTS `sa_officer` (
  `OFFICER_ID` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary key',
  `NAME` varchar(150) NOT NULL COMMENT 'Officer name',
  `DESIGNATION` varchar(150) NOT NULL COMMENT 'Officer designation',
  `OFFICE` varchar(150) NOT NULL COMMENT 'Officer office location name',
  `PHONE` varchar(20) NOT NULL COMMENT 'Phone number',
  `EMAIL` varchar(50) NOT NULL COMMENT 'Email address',
  `TYPE` tinyint(1) NOT NULL,
  `IS_ACTIVE` tinyint(4) NOT NULL DEFAULT '1',
  `CREATED_AT` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `CREATED_BY` int(10) unsigned DEFAULT NULL,
  `UPDATED_AT` datetime DEFAULT NULL,
  `UPDATED_BY` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`OFFICER_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `sa_officer`
--

INSERT INTO `sa_officer` (`OFFICER_ID`, `NAME`, `DESIGNATION`, `OFFICE`, `PHONE`, `EMAIL`, `TYPE`, `IS_ACTIVE`, `CREATED_AT`, `CREATED_BY`, `UPDATED_AT`, `UPDATED_BY`) VALUES
(1, 'মোঃ মোস্তফা কামাল মুুজমদার', 'প্রধান নির্বাহী কর্মকর্তা', 'নারায়নগঞ্জ সিটি কর্পোরেশন', '৭৬৩৩৪২৩', 'ceo@ncc.gov.bd', 1, 1, '2016-09-26 09:05:58', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sa_password_resets`
--

CREATE TABLE IF NOT EXISTS `sa_password_resets` (
  `EMAIL` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `TOKEN` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `CREATED_AT` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  KEY `password_resets_email_index` (`EMAIL`),
  KEY `password_resets_token_index` (`TOKEN`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `sa_permissions`
--

CREATE TABLE IF NOT EXISTS `sa_permissions` (
  `PERM_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `DISPLAY_NAME` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DESCRIPTION` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CREATED_AT` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `UPDATED_AT` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`PERM_ID`),
  UNIQUE KEY `sa_permissions_name_unique` (`NAME`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=21 ;

--
-- Dumping data for table `sa_permissions`
--

INSERT INTO `sa_permissions` (`PERM_ID`, `NAME`, `DISPLAY_NAME`, `DESCRIPTION`, `CREATED_AT`, `UPDATED_AT`) VALUES
(2, 'tender-edit', 'tender edit', 'Tender edit permission', '2016-07-13 05:58:36', '2016-08-08 00:25:57'),
(4, 'tender-create', 'Tender create', 'Tender create permission', '2016-07-14 00:25:03', '2016-08-08 00:25:35'),
(6, 'tender-delete', 'Tender delete', 'Tender delete permission', '2016-07-16 02:20:34', '2016-08-08 00:25:04'),
(7, 'lease-create', 'Lease create', 'Lease create permission', '2016-07-16 02:21:28', '2016-07-16 02:21:28'),
(8, 'lease-edit', 'Lease edit', 'Lease edit permission', '2016-07-16 02:22:18', '2016-07-16 02:22:18'),
(9, 'lease-delete', 'Lease delete', 'Lease delete permission', '2016-07-16 02:22:39', '2016-07-16 02:22:39'),
(10, 'project-create', 'Project create', 'Project create permission', '2016-07-16 02:23:09', '2016-07-16 02:23:09'),
(11, 'project-edit', 'Project edit', 'Project edit permission', '2016-07-16 02:23:24', '2016-07-16 02:23:24'),
(12, 'project-delete', 'Project delete', 'Project delete permission', '2016-07-16 02:23:41', '2016-07-16 02:23:41'),
(13, 'flat-and-shop-create', 'Flat and shop create', 'Flat and shop create permission', '2016-07-16 02:24:17', '2016-07-16 02:24:17'),
(14, 'flat-and-shop-edit', 'Flat and shop edit', 'Flat and shop edit permission', '2016-07-16 02:24:32', '2016-07-16 02:24:32'),
(15, 'flat-and-shop-delete', 'Flat and shop delete', 'Flat and shop delete permission', '2016-07-16 02:24:59', '2016-07-16 02:24:59'),
(16, 'user-management', 'User Management ', 'User Management  permission', '2016-07-16 02:25:52', '2016-07-16 02:25:52'),
(17, 'security-management', 'Security management', 'Security management permission', '2016-07-16 02:26:46', '2016-07-16 02:26:46'),
(18, 'view-log', 'View system logs', 'System log viewer permission', '2016-07-16 02:28:17', '2016-07-16 02:28:17'),
(19, 'citizen-management', 'citizen management', 'Citizen management', '2016-08-08 00:30:13', '2016-08-08 00:30:13'),
(20, 'account-management', 'Account management', 'Account management permission', '2016-08-08 02:50:45', '2016-08-08 02:50:45');

-- --------------------------------------------------------

--
-- Table structure for table `sa_permission_role`
--

CREATE TABLE IF NOT EXISTS `sa_permission_role` (
  `PERMISSION_ID` bigint(20) NOT NULL,
  `ROLE_ID` bigint(20) NOT NULL,
  PRIMARY KEY (`PERMISSION_ID`,`ROLE_ID`),
  KEY `sa_permission_role_role_id_foreign` (`ROLE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `sa_permission_role`
--

INSERT INTO `sa_permission_role` (`PERMISSION_ID`, `ROLE_ID`) VALUES
(7, 1),
(8, 1),
(9, 1),
(2, 2),
(4, 2),
(6, 2),
(10, 3),
(11, 3),
(12, 3),
(2, 4),
(4, 4),
(6, 4),
(7, 4),
(8, 4),
(9, 4),
(10, 4),
(11, 4),
(12, 4),
(13, 4),
(14, 4),
(15, 4),
(16, 4),
(17, 4),
(18, 4),
(20, 4),
(19, 5),
(20, 6);

-- --------------------------------------------------------

--
-- Table structure for table `sa_postoffices`
--

CREATE TABLE IF NOT EXISTS `sa_postoffices` (
  `POST_OFFICE_ID` smallint(6) NOT NULL AUTO_INCREMENT,
  `POST_OFFICE_ENAME` varchar(100) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Post Office Name In English.',
  `POST_CODE` smallint(5) NOT NULL COMMENT 'Post Code.',
  `THANA_ID` smallint(4) NOT NULL COMMENT 'Foreign Key To THANA_ID Column Of sa_thanas Table.',
  `THANA_NAME` varchar(300) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ORDER_SL` smallint(5) DEFAULT NULL COMMENT 'Ascending Serial No.',
  `UD_POFFICE_ID` int(8) DEFAULT NULL COMMENT 'User Define Post Office Id.',
  `IS_ACTIVE` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0=NO, 1=YES',
  `CREATED_BY` int(11) NOT NULL,
  `CREATED_AT` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `UPDATED_AT` datetime DEFAULT NULL,
  `UPDATED_BY` int(10) DEFAULT NULL,
  PRIMARY KEY (`POST_OFFICE_ID`),
  KEY `SA_POST_THANA_ID` (`THANA_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1245 ;

--
-- Dumping data for table `sa_postoffices`
--

INSERT INTO `sa_postoffices` (`POST_OFFICE_ID`, `POST_OFFICE_ENAME`, `POST_CODE`, `THANA_ID`, `THANA_NAME`, `ORDER_SL`, `UD_POFFICE_ID`, `IS_ACTIVE`, `CREATED_BY`, `CREATED_AT`, `UPDATED_AT`, `UPDATED_BY`) VALUES
(1, 'Bagerhat Sadar', 9300, 187, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:00', NULL, NULL),
(2, 'P.C College', 9301, 187, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:00', NULL, NULL),
(3, 'Rangdia', 9302, 187, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:00', NULL, NULL),
(4, 'Chalna Ankorage', 9350, 542, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:00', NULL, NULL),
(5, 'Mongla Port', 9351, 542, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:00', NULL, NULL),
(6, 'Barabaria', 9361, 188, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:00', NULL, NULL),
(7, 'Chitalmari', 9360, 188, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:01', NULL, NULL),
(8, 'Bhanganpar Bazar', 9372, 189, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:01', NULL, NULL),
(9, 'Fakirhat', 9370, 189, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:01', NULL, NULL),
(10, 'Mansa', 9371, 189, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:01', NULL, NULL),
(11, 'Kachua', 9310, 190, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:01', NULL, NULL),
(12, 'Sonarkola', 9311, 190, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:01', NULL, NULL),
(13, 'Charkulia', 9383, 191, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:01', NULL, NULL),
(14, 'Dariala', 9382, 191, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:01', NULL, NULL),
(15, 'Kahalpur', 9381, 191, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:01', NULL, NULL),
(16, 'Mollahat', 9380, 191, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:01', NULL, NULL),
(17, 'Nagarkandi', 9384, 191, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:01', NULL, NULL),
(18, 'Pak Gangni', 9385, 191, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:01', NULL, NULL),
(19, 'Morelganj', 9320, 193, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:01', NULL, NULL),
(20, 'Sannasi Bazar', 9321, 193, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:01', NULL, NULL),
(21, 'Telisatee', 9322, 193, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:01', NULL, NULL),
(22, 'Foylahat', 9341, 194, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:01', NULL, NULL),
(23, 'Gourambha', 9343, 194, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:01', NULL, NULL),
(24, 'Rampal', 9340, 194, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:01', NULL, NULL),
(25, 'Sonatunia', 9342, 194, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:01', NULL, NULL),
(26, 'Rayenda', 9330, 543, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:01', NULL, NULL),
(27, 'Alamdanga', 7210, 196, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:01', NULL, NULL),
(28, 'Hardi', 7211, 196, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:01', NULL, NULL),
(29, 'Chuadanga Sadar', 7200, 197, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:01', NULL, NULL),
(30, 'Munshiganj', 7201, 197, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:01', NULL, NULL),
(31, 'Andulbaria', 7222, 41, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:01', NULL, NULL),
(32, 'Damurhuda', 7220, 41, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:01', NULL, NULL),
(33, 'Darshana', 7221, 41, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:01', NULL, NULL),
(34, 'Doulatganj', 7230, 544, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:01', NULL, NULL),
(35, 'Bagharpara', 7470, 40, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:01', NULL, NULL),
(36, 'Gouranagar', 7471, 40, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:01', NULL, NULL),
(37, 'Chougachha', 7410, 200, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:01', NULL, NULL),
(38, 'Basundia', 7406, 545, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:01', NULL, NULL),
(39, 'Chanchra', 7402, 545, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:01', NULL, NULL),
(40, 'Churamankathi', 7407, 545, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:01', NULL, NULL),
(41, 'Jessore Airbach', 7404, 545, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:01', NULL, NULL),
(42, 'Jessore canttonment', 7403, 545, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:01', NULL, NULL),
(43, 'Jessore Sadar', 7400, 545, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:01', NULL, NULL),
(44, 'Jessore Upa-Shahar', 7401, 545, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:01', NULL, NULL),
(45, 'Rupdia', 7405, 545, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:01', NULL, NULL),
(46, 'Jhikargachha', 7420, 201, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:01', NULL, NULL),
(47, 'Keshobpur', 7450, 202, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:02', NULL, NULL),
(48, 'Monirampur', 7440, 204, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:02', NULL, NULL),
(49, 'Bhugilhat', 7462, 546, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:02', NULL, NULL),
(50, 'Noapara', 7460, 546, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:02', NULL, NULL),
(51, 'Rajghat', 7461, 546, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:02', NULL, NULL),
(52, 'Bag Achra', 7433, 205, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:02', NULL, NULL),
(53, 'Benapole', 7431, 205, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:02', NULL, NULL),
(54, 'Jadabpur', 7432, 205, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:02', NULL, NULL),
(55, 'Sarsa', 7430, 205, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:02', NULL, NULL),
(56, 'Harinakundu', 7310, 206, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:02', NULL, NULL),
(57, 'Jinaidaha Cadet College', 7301, 207, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:02', NULL, NULL),
(58, 'Jinaidaha Sadar', 7300, 207, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:02', NULL, NULL),
(59, 'Kotchandpur', 7330, 209, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:02', NULL, NULL),
(60, 'Maheshpur', 7340, 210, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:02', NULL, NULL),
(61, 'Hatbar Bazar', 7351, 547, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:02', NULL, NULL),
(62, 'Naldanga', 7350, 547, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:02', NULL, NULL),
(63, 'Kumiradaha', 7321, 211, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:02', NULL, NULL),
(64, 'Shailakupa', 7320, 211, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:02', NULL, NULL),
(65, 'Alaipur', 9240, 548, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:02', NULL, NULL),
(66, 'Belphulia', 9242, 548, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:02', NULL, NULL),
(67, 'Rupsha', 9241, 548, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:02', NULL, NULL),
(68, 'Batiaghat', 9260, 212, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:02', NULL, NULL),
(69, 'Surkalee', 9261, 212, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:02', NULL, NULL),
(70, 'Bajua', 9272, 549, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:02', NULL, NULL),
(71, 'Chalna Bazar', 9270, 549, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:02', NULL, NULL),
(72, 'Dakup', 9271, 549, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:02', NULL, NULL),
(73, 'Nalian', 9273, 549, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:02', NULL, NULL),
(74, 'Chandni Mahal', 9221, 216, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:02', NULL, NULL),
(75, 'Digalia', 9220, 216, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:02', NULL, NULL),
(76, 'Gazirhat', 9224, 216, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:02', NULL, NULL),
(77, 'Ghoshghati', 9223, 216, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:02', NULL, NULL),
(78, 'Senhati', 9222, 216, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:02', NULL, NULL),
(79, 'Atra Shilpa Area', 9207, 219, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:02', NULL, NULL),
(80, 'BIT Khulna', 9203, 219, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:02', NULL, NULL),
(81, 'Doulatpur', 9202, 219, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:02', NULL, NULL),
(82, 'Jahanabad Canttonmen', 9205, 219, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:02', NULL, NULL),
(83, 'Khula Sadar', 9100, 219, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:02', NULL, NULL),
(84, 'Khulna G.P.O', 9000, 219, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:02', NULL, NULL),
(85, 'Khulna Shipyard', 9201, 219, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:02', NULL, NULL),
(86, 'Khulna University', 9208, 219, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:02', NULL, NULL),
(87, 'Siramani', 9204, 219, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:02', NULL, NULL),
(88, 'Sonali Jute Mills', 9206, 219, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:03', NULL, NULL),
(89, 'Amadee', 9291, 550, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:03', NULL, NULL),
(90, 'Madinabad', 9290, 550, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:03', NULL, NULL),
(91, 'Chandkhali', 9284, 221, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:03', NULL, NULL),
(92, 'Garaikhali', 9285, 221, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:03', NULL, NULL),
(93, 'Godaipur', 9281, 221, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:03', NULL, NULL),
(94, 'Kapilmoni', 9282, 221, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:03', NULL, NULL),
(95, 'Katipara', 9283, 221, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:03', NULL, NULL),
(96, 'Paikgachha', 9280, 221, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:03', NULL, NULL),
(97, 'Phultala', 9210, 222, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:03', NULL, NULL),
(98, 'Chuknagar', 9252, 551, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:03', NULL, NULL),
(99, 'Ghonabanda', 9251, 551, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:03', NULL, NULL),
(100, 'Sajiara', 9250, 551, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:03', NULL, NULL),
(101, 'Shahapur', 9253, 551, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:03', NULL, NULL),
(102, 'Pak Barasat', 9231, 225, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:03', NULL, NULL),
(103, 'Terakhada', 9230, 225, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:03', NULL, NULL),
(104, 'Allardarga', 7042, 226, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:03', NULL, NULL),
(105, 'Bheramara', 7040, 226, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:03', NULL, NULL),
(106, 'Ganges Bheramara', 7041, 226, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:03', NULL, NULL),
(107, 'Janipur', 7020, 552, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:03', NULL, NULL),
(108, 'Khoksa', 7021, 552, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:03', NULL, NULL),
(109, 'Kumarkhali', 7010, 229, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:03', NULL, NULL),
(110, 'Panti', 7011, 229, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:03', NULL, NULL),
(111, 'Islami University', 7003, 230, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:03', NULL, NULL),
(112, 'Jagati', 7002, 230, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:03', NULL, NULL),
(113, 'Kushtia Mohini', 7001, 230, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:03', NULL, NULL),
(114, 'Kustia Sadar', 7000, 230, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:03', NULL, NULL),
(115, 'Amla Sadarpur', 7032, 231, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:03', NULL, NULL),
(116, 'Mirpur', 7030, 231, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:03', NULL, NULL),
(117, 'Poradaha', 7031, 231, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:03', NULL, NULL),
(118, 'Khasmathurapur', 7052, 553, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:03', NULL, NULL),
(119, 'Rafayetpur', 7050, 553, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:03', NULL, NULL),
(120, 'Taragunia', 7051, 553, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:03', NULL, NULL),
(121, 'Arpara', 7620, 554, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:03', NULL, NULL),
(122, 'Magura Sadar', 7600, 232, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:03', NULL, NULL),
(123, 'Binodpur', 7631, 233, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:03', NULL, NULL),
(124, 'Mohammadpur', 7630, 233, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:03', NULL, NULL),
(125, 'Nahata', 7632, 233, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:03', NULL, NULL),
(126, 'Langalbadh', 7611, 33, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:04', NULL, NULL),
(127, 'Nachol', 7612, 33, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:04', NULL, NULL),
(128, 'Shripur', 7610, 33, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:04', NULL, NULL),
(129, 'Gangni', 7110, 235, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:04', NULL, NULL),
(130, 'Amjhupi', 7101, 237, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:04', NULL, NULL),
(131, 'Meherpur Sadar', 7100, 237, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:04', NULL, NULL),
(132, 'Mujib Nagar Complex', 7102, 237, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:04', NULL, NULL),
(133, 'Kalia', 7520, 238, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:04', NULL, NULL),
(134, 'Baradia', 7514, 555, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:04', NULL, NULL),
(135, 'Itna', 7512, 555, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:04', NULL, NULL),
(136, 'Laxmipasha', 7510, 555, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:04', NULL, NULL),
(137, 'Lohagora', 7511, 555, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:04', NULL, NULL),
(138, 'Naldi', 7513, 555, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:04', NULL, NULL),
(139, 'Mohajan', 7521, 556, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:04', NULL, NULL),
(140, 'Narail Sadar', 7500, 240, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:04', NULL, NULL),
(141, 'Ratanganj', 7501, 240, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:04', NULL, NULL),
(142, 'Ashashuni', 9460, 241, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:04', NULL, NULL),
(143, 'Baradal', 9461, 241, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:04', NULL, NULL),
(144, 'Debbhata', 9430, 242, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:04', NULL, NULL),
(145, 'Gurugram', 9431, 242, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:04', NULL, NULL),
(146, 'Chandanpur', 9415, 243, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:04', NULL, NULL),
(147, 'Hamidpur', 9413, 243, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:04', NULL, NULL),
(148, 'Jhaudanga', 9412, 243, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:04', NULL, NULL),
(149, 'kalaroa', 9410, 243, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:04', NULL, NULL),
(150, 'Khordo', 9414, 243, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:04', NULL, NULL),
(151, 'Murarikati', 9411, 243, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:04', NULL, NULL),
(152, 'Kaliganj UPO', 9440, 244, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:04', NULL, NULL),
(153, 'Nalta Mubaroknagar', 9441, 244, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:04', NULL, NULL),
(154, 'Ratanpur', 9442, 244, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:04', NULL, NULL),
(155, 'Buri Goalini', 9453, 557, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:04', NULL, NULL),
(156, 'Gabura', 9454, 557, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:04', NULL, NULL),
(157, 'Habinagar', 9455, 557, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:04', NULL, NULL),
(158, 'Nakipur', 9450, 557, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:04', NULL, NULL),
(159, 'Naobeki', 9452, 557, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:04', NULL, NULL),
(160, 'Noornagar', 9451, 557, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:04', NULL, NULL),
(161, 'Budhhat', 9403, 245, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:04', NULL, NULL),
(162, 'Gunakar kati', 9402, 245, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:04', NULL, NULL),
(163, 'Satkhira Islamia Acc', 9401, 245, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:04', NULL, NULL),
(164, 'Satkhira Sadar', 9400, 245, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:04', NULL, NULL),
(165, 'Patkelghata', 9421, 247, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:05', NULL, NULL),
(166, 'Taala', 9420, 247, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:05', NULL, NULL),
(167, 'Demra', 1360, 419, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:05', NULL, NULL),
(168, 'Matuail', 1362, 419, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:05', NULL, NULL),
(169, 'Sarulia', 1361, 419, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:05', NULL, NULL),
(170, 'Dhaka Cantonment TSO', 1206, 417, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:05', NULL, NULL),
(171, 'Dhamrai', 1350, 420, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:05', NULL, NULL),
(172, 'Kamalpur', 1351, 420, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:05', NULL, NULL),
(173, 'Jigatala TSO', 1209, 421, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:05', NULL, NULL),
(174, 'Banani TSO', 1213, 423, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:05', NULL, NULL),
(175, 'Gulshan Model Town', 1212, 423, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:05', NULL, NULL),
(176, 'Dhania TSO', 1232, 425, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:05', NULL, NULL),
(177, 'Joypara', 1330, 558, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:05', NULL, NULL),
(178, 'Narisha', 1332, 558, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:05', NULL, NULL),
(179, 'Palamganj', 1331, 558, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:05', NULL, NULL),
(180, 'Ati', 1312, 430, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:05', NULL, NULL),
(181, 'Dhaka Jute Mills', 1311, 430, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:05', NULL, NULL),
(182, 'Kalatia', 1313, 430, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:05', NULL, NULL),
(183, 'Keraniganj', 1310, 430, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:05', NULL, NULL),
(184, 'Khilgaon TSO', 1219, 428, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:05', NULL, NULL),
(185, 'KhilkhetTSO', 1229, 429, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:05', NULL, NULL),
(186, 'Posta TSO', 1211, 432, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:05', NULL, NULL),
(187, 'Mirpur TSO', 1216, 433, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:05', NULL, NULL),
(188, 'Mohammadpur Housing', 1207, 434, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:05', NULL, NULL),
(189, 'Sangsad BhabanTSO', 1225, 434, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:05', NULL, NULL),
(190, 'BangabhabanTSO', 1222, 435, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:05', NULL, NULL),
(191, 'DilkushaTSO', 1223, 435, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:05', NULL, NULL),
(192, 'Agla', 1323, 436, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:05', NULL, NULL),
(193, 'Churain', 1325, 436, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:05', NULL, NULL),
(194, 'Daudpur', 1322, 436, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:05', NULL, NULL),
(195, 'Hasnabad', 1321, 436, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:05', NULL, NULL),
(196, 'Khalpar', 1324, 436, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:05', NULL, NULL),
(197, 'Nawabganj', 1320, 436, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:05', NULL, NULL),
(198, 'New Market TSO', 1205, 43, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:05', NULL, NULL),
(199, 'Dhaka GPO', 1000, 42, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:05', NULL, NULL),
(200, 'Shantinagr TSO', 1217, 68, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:05', NULL, NULL),
(201, 'Basabo TSO', 1214, 69, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:05', NULL, NULL),
(202, 'Amin Bazar', 1348, 70, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:06', NULL, NULL),
(203, 'Dairy Farm', 1341, 70, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:06', NULL, NULL),
(204, 'EPZ', 1349, 70, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:06', NULL, NULL),
(205, 'Jahangirnagar Univer', 1342, 70, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:06', NULL, NULL),
(206, 'Kashem Cotton Mills', 1346, 70, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:06', NULL, NULL),
(207, 'Rajphulbaria', 1347, 70, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:06', NULL, NULL),
(208, 'Savar', 1340, 70, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:06', NULL, NULL),
(209, 'Savar Canttonment', 1344, 70, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:06', NULL, NULL),
(210, 'Saver P.A.T.C', 1343, 70, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:06', NULL, NULL),
(211, 'Shimulia', 1345, 70, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:06', NULL, NULL),
(212, 'Dhaka Sadar HO', 1100, 74, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:06', NULL, NULL),
(213, 'Gendaria TSO', 1204, 74, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:06', NULL, NULL),
(214, 'Wari TSO', 1203, 74, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:06', NULL, NULL),
(215, 'Tejgaon TSO', 1215, 75, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:06', NULL, NULL),
(216, 'Dhaka Politechnic', 1208, 76, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:06', NULL, NULL),
(217, 'Uttara Model TwonTSO', 1230, 78, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:06', NULL, NULL),
(218, 'Alfadanga', 7870, 80, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:06', NULL, NULL),
(219, 'Bhanga', 7830, 81, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:06', NULL, NULL),
(220, 'Boalmari', 7860, 38, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:06', NULL, NULL),
(221, 'Rupatpat', 7861, 38, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:06', NULL, NULL),
(222, 'Charbadrashan', 7810, 82, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:06', NULL, NULL),
(223, 'Ambikapur', 7802, 83, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:06', NULL, NULL),
(224, 'Baitulaman Politecni', 7803, 83, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:06', NULL, NULL),
(225, 'Faridpur Sadar', 7800, 83, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:06', NULL, NULL),
(226, 'Kanaipur', 7801, 83, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:06', NULL, NULL),
(227, 'Kamarkali', 7851, 84, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:06', NULL, NULL),
(228, 'Madukhali', 7850, 84, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:06', NULL, NULL),
(229, 'Nagarkanda', 7840, 16, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:06', NULL, NULL),
(230, 'Talma', 7841, 16, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:06', NULL, NULL),
(231, 'Bishwa jaker Manjil', 7822, 85, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:06', NULL, NULL),
(232, 'Hat Krishapur', 7821, 85, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:06', NULL, NULL),
(233, 'Sadarpur', 7820, 85, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:06', NULL, NULL),
(234, 'Shriangan', 7804, 559, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:06', NULL, NULL),
(235, 'B.O.F', 1703, 86, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:06', NULL, NULL),
(236, 'B.R.R', 1701, 86, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:06', NULL, NULL),
(237, 'Chandna', 1702, 86, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:06', NULL, NULL),
(238, 'Gazipur Sadar', 1700, 86, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:06', NULL, NULL),
(239, 'National University', 1704, 86, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:06', NULL, NULL),
(240, 'Kaliakaar', 1705, 87, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:06', NULL, NULL),
(241, 'Safipur', 1751, 87, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:07', NULL, NULL),
(242, 'Kaliganj', 1720, 88, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:07', NULL, NULL),
(243, 'Pubail', 1721, 88, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:07', NULL, NULL),
(244, 'Santanpara', 1722, 88, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:07', NULL, NULL),
(245, 'Vaoal Jamalpur', 1723, 88, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:07', NULL, NULL),
(246, 'kapashia', 1730, 89, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:07', NULL, NULL),
(247, 'Ershad Nagar', 1712, 560, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:07', NULL, NULL),
(248, 'Monnunagar', 1710, 560, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:07', NULL, NULL),
(249, 'Nishat Nagar', 1711, 560, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:07', NULL, NULL),
(250, 'Barmi', 1743, 90, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:07', NULL, NULL),
(251, 'Bashamur', 1747, 90, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:07', NULL, NULL),
(252, 'Boubi', 1748, 90, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:07', NULL, NULL),
(253, 'Kawraid', 1745, 90, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:07', NULL, NULL),
(254, 'Satkhamair', 1744, 90, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:07', NULL, NULL),
(255, 'Sreepur', 1740, 90, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:07', NULL, NULL),
(256, 'Rajendrapur', 1741, 90, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:07', NULL, NULL),
(257, 'Rajendrapur Canttome', 1742, 90, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:07', NULL, NULL),
(258, 'Barfa', 8102, 91, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:07', NULL, NULL),
(259, 'Chandradighalia', 8013, 91, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:07', NULL, NULL),
(260, 'Gopalganj Sadar', 8100, 91, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:07', NULL, NULL),
(261, 'Ulpur', 8101, 91, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:07', NULL, NULL),
(262, 'Jonapur', 8133, 92, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:07', NULL, NULL),
(263, 'Kashiani', 8130, 92, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:07', NULL, NULL),
(264, 'Ramdia College', 8131, 92, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:07', NULL, NULL),
(265, 'Ratoil', 8132, 92, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:07', NULL, NULL),
(266, 'Kotalipara', 8110, 93, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:07', NULL, NULL),
(267, 'Batkiamari', 8141, 94, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:07', NULL, NULL),
(268, 'Khandarpara', 8142, 94, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:07', NULL, NULL),
(269, 'Maksudpur', 8140, 94, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:07', NULL, NULL),
(270, 'Patgati', 8121, 95, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:07', NULL, NULL),
(271, 'Tungipara', 8120, 95, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:07', NULL, NULL),
(272, 'Dewangonj', 2030, 97, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:07', NULL, NULL),
(273, 'Dewangonj S. Mills', 2031, 97, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:07', NULL, NULL),
(274, 'Durmoot', 2021, 98, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:07', NULL, NULL),
(275, 'Gilabari', 2022, 98, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:07', NULL, NULL),
(276, 'Islampur', 2020, 98, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:07', NULL, NULL),
(277, 'Jamalpur', 2000, 99, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:08', NULL, NULL),
(278, 'Nandina', 2001, 99, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:08', NULL, NULL),
(279, 'Narundi', 2002, 99, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:08', NULL, NULL),
(280, 'Mahmoodpur', 2013, 101, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:08', NULL, NULL),
(281, 'Malancha', 2012, 101, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:08', NULL, NULL),
(282, 'Malandah', 2010, 101, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:08', NULL, NULL),
(283, 'Balijhuri', 2041, 100, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:08', NULL, NULL),
(284, 'Mathargonj', 2040, 100, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:08', NULL, NULL),
(285, 'Bausee', 2052, 102, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:08', NULL, NULL),
(286, 'Gunerbari', 2051, 102, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:08', NULL, NULL),
(287, 'Jagannath Ghat', 2053, 102, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:08', NULL, NULL),
(288, 'Jamuna Sar Karkhana', 2055, 102, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:08', NULL, NULL),
(289, 'Pingna', 2054, 102, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:08', NULL, NULL),
(290, 'Shorishabari', 2050, 102, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:08', NULL, NULL),
(291, 'Bajitpur', 2336, 104, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:08', NULL, NULL),
(292, 'Laksmipur', 2338, 104, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:08', NULL, NULL),
(293, 'Sararchar', 2337, 104, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:08', NULL, NULL),
(294, 'Bhairab', 2350, 105, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:08', NULL, NULL),
(295, 'Hossenpur', 2320, 17, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:08', NULL, NULL),
(296, 'Karimganj', 2310, 107, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:08', NULL, NULL),
(297, 'Gochhihata', 2331, 108, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:08', NULL, NULL),
(298, 'Katiadi', 2330, 108, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:08', NULL, NULL),
(299, 'Kishoreganj S.Mills', 2301, 109, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:08', NULL, NULL),
(300, 'Kishoreganj Sadar', 2300, 109, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:08', NULL, NULL),
(301, 'Maizhati', 2302, 109, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:08', NULL, NULL),
(302, 'Nilganj', 2303, 109, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:08', NULL, NULL),
(303, 'Chhoysuti', 2341, 110, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:08', NULL, NULL),
(304, 'Kuliarchar', 2340, 110, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:08', NULL, NULL),
(305, 'Abdullahpur', 2371, 111, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:08', NULL, NULL),
(306, 'MIthamoin', 2370, 111, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:08', NULL, NULL),
(307, 'Nikli', 2360, 112, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:08', NULL, NULL),
(308, 'Ostagram', 2380, 103, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:08', NULL, NULL),
(309, 'Pakundia', 2326, 113, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:08', NULL, NULL),
(310, 'Tarial', 2316, 114, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:08', NULL, NULL),
(311, 'Bahadurpur', 7932, 561, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:08', NULL, NULL),
(312, 'Barhamganj', 7930, 561, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:08', NULL, NULL),
(313, 'Nilaksmibandar', 7931, 561, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:09', NULL, NULL),
(314, 'Umedpur', 7933, 561, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:09', NULL, NULL),
(315, 'Kalkini', 7920, 115, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:09', NULL, NULL),
(316, 'Sahabrampur', 7921, 115, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:09', NULL, NULL),
(317, 'Charmugria', 7901, 116, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:09', NULL, NULL),
(318, 'Habiganj', 7903, 116, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:09', NULL, NULL),
(319, 'Kulpaddi', 7902, 116, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:09', NULL, NULL),
(320, 'Madaripur Sadar', 7900, 116, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:09', NULL, NULL),
(321, 'Mustafapur', 7904, 116, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:09', NULL, NULL),
(322, 'Khalia', 7911, 117, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:09', NULL, NULL),
(323, 'Rajoir', 7910, 117, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:09', NULL, NULL),
(324, 'Gheor', 1840, 120, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:09', NULL, NULL),
(325, 'Jhitka', 1831, 563, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:09', NULL, NULL),
(326, 'Lechhraganj', 1830, 563, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:09', NULL, NULL),
(327, 'Barangail', 1804, 122, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:09', NULL, NULL),
(328, 'Gorpara', 1802, 122, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:09', NULL, NULL),
(329, 'Mahadebpur', 1803, 122, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:09', NULL, NULL),
(330, 'Manikganj Bazar', 1801, 122, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:09', NULL, NULL),
(331, 'Manikganj Sadar', 1800, 122, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:09', NULL, NULL),
(332, 'Baliati', 1811, 123, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:09', NULL, NULL),
(333, 'Saturia', 1810, 123, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:09', NULL, NULL),
(334, 'Aricha', 1851, 124, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:09', NULL, NULL),
(335, 'Shibaloy', 1850, 124, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:09', NULL, NULL),
(336, 'Tewta', 1852, 124, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:09', NULL, NULL),
(337, 'Uthli', 1853, 124, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:09', NULL, NULL),
(338, 'Baira', 1821, 125, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:09', NULL, NULL),
(339, 'joymantop', 1822, 125, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:09', NULL, NULL),
(340, 'Singair', 1820, 125, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:09', NULL, NULL),
(341, 'Gajaria', 1510, 126, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:09', NULL, NULL),
(342, 'Hossendi', 1511, 126, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:09', NULL, NULL),
(343, 'Rasulpur', 1512, 126, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:09', NULL, NULL),
(344, 'Gouragonj', 1334, 18, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:09', NULL, NULL),
(345, 'Haldia SO', 1532, 18, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:09', NULL, NULL),
(346, 'Haridia', 1333, 18, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:09', NULL, NULL),
(347, 'Haridia DESO', 1533, 18, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:09', NULL, NULL),
(348, 'Korhati', 1531, 18, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:09', NULL, NULL),
(349, 'Lohajang', 1530, 18, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:09', NULL, NULL),
(350, 'Madini Mandal', 1335, 18, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:09', NULL, NULL),
(351, 'Medini Mandal EDSO', 1535, 18, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:10', NULL, NULL),
(352, 'Kathakhali', 1503, 127, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:10', NULL, NULL),
(353, 'Mirkadim', 1502, 127, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:10', NULL, NULL),
(354, 'Munshiganj Sadar', 1500, 127, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:10', NULL, NULL),
(355, 'Rikabibazar', 1501, 127, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:10', NULL, NULL),
(356, 'Ichapur', 1542, 128, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:10', NULL, NULL),
(357, 'Kola', 1541, 128, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:10', NULL, NULL),
(358, 'Malkha Nagar', 1543, 128, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:10', NULL, NULL),
(359, 'Shekher Nagar', 1544, 128, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:10', NULL, NULL),
(360, 'Sirajdikhan', 1540, 128, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:10', NULL, NULL),
(361, 'Baghra', 1557, 129, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:10', NULL, NULL),
(362, 'Barikhal', 1551, 129, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:10', NULL, NULL),
(363, 'Bhaggyakul', 1558, 129, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:10', NULL, NULL),
(364, 'Hashara', 1553, 129, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:10', NULL, NULL),
(365, 'Kolapara', 1554, 129, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:10', NULL, NULL),
(366, 'Kumarbhog', 1555, 129, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:10', NULL, NULL),
(367, 'Mazpara', 1552, 129, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:10', NULL, NULL),
(368, 'Srinagar', 1550, 129, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:10', NULL, NULL),
(369, 'Vaggyakul SO', 1556, 129, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:10', NULL, NULL),
(370, 'Bajrajugini', 1523, 130, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:10', NULL, NULL),
(371, 'Baligao', 1522, 130, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:10', NULL, NULL),
(372, 'Betkahat', 1521, 130, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:10', NULL, NULL),
(373, 'Dighirpar', 1525, 130, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:10', NULL, NULL),
(374, 'Hasail', 1524, 130, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:10', NULL, NULL),
(375, 'Pura', 1527, 130, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:10', NULL, NULL),
(376, 'Pura EDSO', 1526, 130, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:10', NULL, NULL),
(377, 'Tangibari', 1520, 130, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:10', NULL, NULL),
(378, 'Bhaluka', 2240, 131, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:10', NULL, NULL),
(379, 'Fulbaria', 2216, 133, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:10', NULL, NULL),
(380, 'Duttarbazar', 2234, 134, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:10', NULL, NULL),
(381, 'Gaforgaon', 2230, 134, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:10', NULL, NULL),
(382, 'Kandipara', 2233, 134, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:10', NULL, NULL),
(383, 'Shibganj', 2231, 134, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:10', NULL, NULL),
(384, 'Usti', 2232, 134, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:10', NULL, NULL),
(385, 'Gouripur', 2270, 135, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:10', NULL, NULL),
(386, 'Ramgopalpur', 2271, 135, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:10', NULL, NULL),
(387, 'Dhara', 2261, 39, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:10', NULL, NULL),
(388, 'Haluaghat', 2260, 39, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:10', NULL, NULL),
(389, 'Munshirhat', 2262, 39, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:10', NULL, NULL),
(390, 'Atharabari', 2282, 136, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:11', NULL, NULL),
(391, 'Isshwargonj', 2280, 136, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:11', NULL, NULL),
(392, 'Sohagi', 2281, 136, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:11', NULL, NULL),
(393, 'Muktagachha', 2210, 138, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:11', NULL, NULL),
(394, 'Agriculture Universi', 2202, 137, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:11', NULL, NULL),
(395, 'Biddyaganj', 2204, 137, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:11', NULL, NULL),
(396, 'Kawatkhali', 2201, 137, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:11', NULL, NULL),
(397, 'Mymensingh Sadar', 2200, 137, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:11', NULL, NULL),
(398, 'Pearpur', 2205, 137, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:11', NULL, NULL),
(399, 'Shombhuganj', 2203, 137, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:11', NULL, NULL),
(400, 'Gangail', 2291, 139, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:11', NULL, NULL),
(401, 'Nandail', 2290, 139, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:11', NULL, NULL),
(402, 'Beltia', 2251, 139, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:11', NULL, NULL),
(403, 'Phulpur', 2250, 140, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:11', NULL, NULL),
(404, 'Tarakanda', 2252, 140, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:11', NULL, NULL),
(405, 'Ahmadbad', 2221, 141, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:11', NULL, NULL),
(406, 'Dhala', 2223, 141, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:11', NULL, NULL),
(407, 'Ram Amritaganj', 2222, 141, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:11', NULL, NULL),
(408, 'Trishal', 2220, 141, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:11', NULL, NULL),
(409, 'Araihazar', 1450, 142, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:11', NULL, NULL),
(410, 'Gopaldi', 1451, 142, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:11', NULL, NULL),
(411, 'Baidder Bazar', 1440, 564, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:11', NULL, NULL),
(412, 'Bara Nagar', 1441, 564, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:11', NULL, NULL),
(413, 'Barodi', 1442, 564, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:11', NULL, NULL),
(414, 'Bandar', 1410, 144, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:11', NULL, NULL),
(415, 'BIDS', 1413, 144, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:11', NULL, NULL),
(416, 'D.C Mills', 1411, 144, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:11', NULL, NULL),
(417, 'Madanganj', 1414, 144, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:11', NULL, NULL),
(418, 'Nabiganj', 1412, 144, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:11', NULL, NULL),
(419, 'Fatulla Bazar', 1421, 565, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:11', NULL, NULL),
(420, 'Fatullah', 1420, 565, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:11', NULL, NULL),
(421, 'Narayanganj Sadar', 1400, 19, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:11', NULL, NULL),
(422, 'Bhulta', 1462, 145, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:11', NULL, NULL),
(423, 'Kanchan', 1461, 145, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:11', NULL, NULL),
(424, 'Murapara', 1464, 145, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:11', NULL, NULL),
(425, 'Nagri', 1463, 145, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:11', NULL, NULL),
(426, 'Rupganj', 1460, 145, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:12', NULL, NULL),
(427, 'Adamjeenagar', 1431, 566, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:12', NULL, NULL),
(428, 'LN Mills', 1432, 566, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:12', NULL, NULL),
(429, 'Siddirganj', 1430, 566, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:12', NULL, NULL),
(430, 'Belabo', 1640, 146, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:12', NULL, NULL),
(431, 'Hatirdia', 1651, 147, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:12', NULL, NULL),
(432, 'Katabaria', 1652, 147, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:12', NULL, NULL),
(433, 'Monohordi', 1650, 147, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:12', NULL, NULL),
(434, 'Karimpur', 1605, 148, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:12', NULL, NULL),
(435, 'Madhabdi', 1604, 148, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:12', NULL, NULL),
(436, 'Narshingdi College', 1602, 148, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:12', NULL, NULL),
(437, 'Narshingdi Sadar', 1600, 148, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:12', NULL, NULL),
(438, 'Panchdona', 1603, 148, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:12', NULL, NULL),
(439, 'UMC Jute Mills', 1601, 148, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:12', NULL, NULL),
(440, 'Char Sindhur', 1612, 149, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:12', NULL, NULL),
(441, 'Ghorashal', 1613, 149, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:12', NULL, NULL),
(442, 'Ghorashal Urea Facto', 1611, 149, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:12', NULL, NULL),
(443, 'Palash', 1610, 149, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:12', NULL, NULL),
(444, 'Bazar Hasnabad', 1631, 150, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:12', NULL, NULL),
(445, 'Radhaganj bazar', 1632, 150, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:12', NULL, NULL),
(446, 'Raypura', 1630, 150, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:12', NULL, NULL),
(447, 'Shibpur', 1620, 151, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:12', NULL, NULL),
(448, 'Susnng Durgapur', 2420, 154, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:12', NULL, NULL),
(449, 'Atpara', 2470, 152, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:12', NULL, NULL),
(450, 'Barhatta', 2440, 153, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:12', NULL, NULL),
(451, 'Kalmakanda', 2430, 156, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:12', NULL, NULL),
(452, 'Kendua', 2480, 157, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:12', NULL, NULL),
(453, 'Khaliajhri', 2460, 157, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:12', NULL, NULL),
(454, 'Madan', 2490, 158, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:12', NULL, NULL),
(455, 'Moddoynagar', 2456, 159, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:12', NULL, NULL),
(456, 'Mohanganj', 2446, 159, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:12', NULL, NULL),
(457, 'Baikherhati', 2401, 160, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:12', NULL, NULL),
(458, 'Netrakona Sadar', 2400, 160, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:12', NULL, NULL),
(459, 'Jaria Jhanjhail', 2412, 161, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:12', NULL, NULL),
(460, 'Purbadhola', 2410, 161, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:12', NULL, NULL),
(461, 'Shamgonj', 2411, 161, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:12', NULL, NULL),
(462, 'Baliakandi', 7730, 162, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:12', NULL, NULL),
(463, 'Nalia', 7731, 162, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:12', NULL, NULL),
(464, 'Mrigibazar', 7723, 164, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:12', NULL, NULL),
(465, 'Pangsha', 7720, 164, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:12', NULL, NULL),
(466, 'Ramkol', 7721, 164, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:13', NULL, NULL),
(467, 'Ratandia', 7722, 164, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:13', NULL, NULL),
(468, 'Goalanda', 7710, 165, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:13', NULL, NULL),
(469, 'Khankhanapur', 7711, 165, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:13', NULL, NULL),
(470, 'Rajbari Sadar', 7700, 165, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:13', NULL, NULL),
(471, 'Bhedorganj', 8030, 32, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:13', NULL, NULL),
(472, 'Damudhya', 8040, 166, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:13', NULL, NULL),
(473, 'Gosairhat', 8050, 167, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:13', NULL, NULL),
(474, 'Jajira', 8010, 170, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:13', NULL, NULL),
(475, 'Bhozeshwar', 8021, 168, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:13', NULL, NULL),
(476, 'Gharisar', 8022, 168, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:13', NULL, NULL),
(477, 'Kartikpur', 8024, 168, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:13', NULL, NULL),
(478, 'Naria', 8020, 168, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:13', NULL, NULL),
(479, 'Upshi', 8023, 168, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:13', NULL, NULL),
(480, 'Angaria', 8001, 169, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:13', NULL, NULL),
(481, 'Chikandi', 8002, 169, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:13', NULL, NULL),
(482, 'Shariatpur Sadar', 8000, 169, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:13', NULL, NULL),
(483, 'Bakshigonj', 2140, 567, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:13', NULL, NULL),
(484, 'Jhinaigati', 2120, 171, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:13', NULL, NULL),
(485, 'Gonopaddi', 2151, 20, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:13', NULL, NULL),
(486, 'Nakla', 2150, 20, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:13', NULL, NULL),
(487, 'Hatibandha', 2111, 172, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:13', NULL, NULL),
(488, 'Nalitabari', 2110, 172, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:13', NULL, NULL),
(489, 'Sherpur Shadar', 2100, 173, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:13', NULL, NULL),
(490, 'Shribardi', 2130, 174, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:13', NULL, NULL),
(491, 'Basail', 1920, 175, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:13', NULL, NULL),
(492, 'Bhuapur', 1960, 176, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:13', NULL, NULL),
(493, 'Delduar', 1910, 177, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:13', NULL, NULL),
(494, 'Elasin', 1913, 177, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:13', NULL, NULL),
(495, 'Hinga Nagar', 1914, 177, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:13', NULL, NULL),
(496, 'Jangalia', 1911, 177, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:13', NULL, NULL),
(497, 'Lowhati', 1915, 177, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:13', NULL, NULL),
(498, 'Patharail', 1912, 177, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:13', NULL, NULL),
(499, 'D. Pakutia', 1982, 179, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:13', NULL, NULL),
(500, 'Dhalapara', 1983, 179, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:13', NULL, NULL),
(501, 'Ghatial', 1980, 179, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:13', NULL, NULL),
(502, 'Lohani', 1984, 179, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:13', NULL, NULL),
(503, 'Zahidganj', 1981, 179, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:13', NULL, NULL),
(504, 'Gopalpur', 1990, 180, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:13', NULL, NULL),
(505, 'Hemnagar', 1992, 180, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:13', NULL, NULL),
(506, 'Jhowail', 1991, 180, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:13', NULL, NULL),
(507, 'Ballabazar', 1973, 181, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:14', NULL, NULL),
(508, 'Elinga', 1974, 181, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:14', NULL, NULL),
(509, 'Kalihati', 1970, 181, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:14', NULL, NULL),
(510, 'Nagarbari', 1977, 181, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:14', NULL, NULL),
(511, 'Nagarbari SO', 1976, 181, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:14', NULL, NULL),
(512, 'Nagbari', 1972, 181, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:14', NULL, NULL),
(513, 'Palisha', 1975, 181, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:14', NULL, NULL),
(514, 'Rajafair', 1971, 181, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:14', NULL, NULL),
(515, 'Kashkawlia', 1930, 568, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:14', NULL, NULL),
(516, 'Dhobari', 1997, 182, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:14', NULL, NULL),
(517, 'Madhupur', 1996, 182, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:14', NULL, NULL),
(518, 'Gorai', 1941, 183, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:14', NULL, NULL),
(519, 'Jarmuki', 1944, 183, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:14', NULL, NULL),
(520, 'M.C. College', 1942, 183, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:14', NULL, NULL),
(521, 'Mirzapur', 1940, 183, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:14', NULL, NULL),
(522, 'Mohera', 1945, 183, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:14', NULL, NULL),
(523, 'Warri paikpara', 1943, 183, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:14', NULL, NULL),
(524, 'Dhuburia', 1937, 184, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:14', NULL, NULL),
(525, 'Nagarpur', 1936, 184, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:14', NULL, NULL),
(526, 'Salimabad', 1938, 184, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:14', NULL, NULL),
(527, 'Kochua', 1951, 185, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:14', NULL, NULL),
(528, 'Sakhipur', 1950, 185, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:14', NULL, NULL),
(529, 'Kagmari', 1901, 186, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:14', NULL, NULL),
(530, 'Korotia', 1903, 186, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:14', NULL, NULL),
(531, 'Purabari', 1904, 186, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:14', NULL, NULL),
(532, 'Santosh', 1902, 186, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:14', NULL, NULL),
(533, 'Tangail Sadar', 1900, 186, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:14', NULL, NULL),
(534, 'Alikadam', 4650, 475, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:14', NULL, NULL),
(535, 'Bandarban Sadar', 4600, 476, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:14', NULL, NULL),
(536, 'Naikhong', 4660, 478, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:14', NULL, NULL),
(537, 'Roanchhari', 4610, 479, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:14', NULL, NULL),
(538, 'Ruma', 4620, 480, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:14', NULL, NULL),
(539, 'Thanchi', 4641, 481, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:14', NULL, NULL),
(540, 'Kalipur', 3642, 569, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:14', NULL, NULL),
(541, 'Matlobganj', 3640, 569, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:14', NULL, NULL),
(542, 'Mohanpur', 3641, 569, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:14', NULL, NULL),
(543, 'Chotoshi', 3623, 497, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:14', NULL, NULL),
(544, 'Islamia Madrasha', 3624, 497, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:15', NULL, NULL),
(545, 'Khilabazar', 3621, 497, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:15', NULL, NULL),
(546, 'Pashchim Kherihar Al', 3622, 497, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:15', NULL, NULL),
(547, 'Shahrasti', 3620, 497, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:15', NULL, NULL),
(548, 'Anowara', 4376, 498, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:15', NULL, NULL),
(549, 'Battali', 4378, 498, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:15', NULL, NULL),
(550, 'Paroikora', 4377, 498, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:15', NULL, NULL),
(551, 'Boalkhali', 4366, 502, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:15', NULL, NULL),
(552, 'Charandwip', 4369, 502, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:15', NULL, NULL),
(553, 'Iqbal Park', 4365, 502, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:15', NULL, NULL),
(554, 'Kadurkhal', 4368, 502, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:15', NULL, NULL),
(555, 'Kanungopara', 4363, 502, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:15', NULL, NULL),
(556, 'Sakpura', 4367, 502, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:15', NULL, NULL),
(557, 'Saroatoli', 4364, 502, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:15', NULL, NULL),
(558, 'Al- Amin Baria Madra', 4221, 504, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:15', NULL, NULL),
(559, 'Amin Jute Mills', 4211, 504, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:15', NULL, NULL),
(560, 'Anandabazar', 4215, 504, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:15', NULL, NULL),
(561, 'Bayezid Bostami', 4210, 504, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:15', NULL, NULL),
(562, 'Chawkbazar', 4203, 504, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:15', NULL, NULL),
(563, 'Chitt. Cantonment', 4220, 504, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:15', NULL, NULL),
(564, 'Chitt. Customs Acca', 4219, 504, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:15', NULL, NULL);
INSERT INTO `sa_postoffices` (`POST_OFFICE_ID`, `POST_OFFICE_ENAME`, `POST_CODE`, `THANA_ID`, `THANA_NAME`, `ORDER_SL`, `UD_POFFICE_ID`, `IS_ACTIVE`, `CREATED_BY`, `CREATED_AT`, `UPDATED_AT`, `UPDATED_BY`) VALUES
(565, 'Chitt. Politechnic In', 4209, 504, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:15', NULL, NULL),
(566, 'Chitt. Sailers Colon', 4218, 504, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:15', NULL, NULL),
(567, 'Chittagong Airport', 4205, 504, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:15', NULL, NULL),
(568, 'Chittagong Bandar', 4100, 504, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:15', NULL, NULL),
(569, 'Chittagong GPO', 4000, 504, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:15', NULL, NULL),
(570, 'Export Processing', 4223, 504, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:15', NULL, NULL),
(571, 'Firozshah', 4207, 504, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:15', NULL, NULL),
(572, 'Halishahar', 4216, 504, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:15', NULL, NULL),
(573, 'Halishshar', 4225, 504, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:15', NULL, NULL),
(574, 'Jalalabad', 4214, 504, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:15', NULL, NULL),
(575, 'Jaldia Merine Accade', 4206, 504, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:15', NULL, NULL),
(576, 'Middle Patenga', 4222, 504, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:15', NULL, NULL),
(577, 'Mohard', 4208, 504, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:15', NULL, NULL),
(578, 'North Halishahar', 4226, 504, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:15', NULL, NULL),
(579, 'North Katuli', 4217, 504, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:15', NULL, NULL),
(580, 'Pahartoli', 4202, 504, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:15', NULL, NULL),
(581, 'Patenga', 4204, 504, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:16', NULL, NULL),
(582, 'Rampura TSO', 4224, 504, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:16', NULL, NULL),
(583, 'Wazedia', 4213, 504, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:16', NULL, NULL),
(584, 'Bangla Hili', 5270, 570, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:16', NULL, NULL),
(585, 'Biral', 5210, 262, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:16', NULL, NULL),
(586, 'Birampur', 5266, 260, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:16', NULL, NULL),
(587, 'Birganj', 5220, 261, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:16', NULL, NULL),
(588, 'Chrirbandar', 5240, 264, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:16', NULL, NULL),
(589, 'Ranirbandar', 5241, 264, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:16', NULL, NULL),
(590, 'Dinajpur Rajbari', 5201, 269, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:16', NULL, NULL),
(591, 'Dinajpur Sadar', 5200, 269, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:16', NULL, NULL),
(592, 'Khansama', 5230, 268, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:16', NULL, NULL),
(593, 'Pakarhat', 5231, 268, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:16', NULL, NULL),
(594, 'Maharajganj', 5226, 571, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:16', NULL, NULL),
(595, 'Nababganj', 5280, 270, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:16', NULL, NULL),
(596, 'Ghoraghat', 5291, 572, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:16', NULL, NULL),
(597, 'Osmanpur', 5290, 572, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:16', NULL, NULL),
(598, 'Parbatipur', 5250, 271, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:16', NULL, NULL),
(599, 'Fulbari', 5260, 21, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:16', NULL, NULL),
(600, 'Setabganj', 5216, 573, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:16', NULL, NULL),
(603, 'Gaibandha Sadar', 5700, 273, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:25', NULL, NULL),
(604, 'Gobindhaganj', 5740, 274, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:25', NULL, NULL),
(605, 'Mahimaganj', 5741, 274, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:26', NULL, NULL),
(606, 'Palashbari', 5730, 275, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:26', NULL, NULL),
(607, 'Bharatkhali', 5761, 272, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:26', NULL, NULL),
(608, 'Phulchhari', 5760, 272, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:26', NULL, NULL),
(609, 'Saadullapur', 5710, 276, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:26', NULL, NULL),
(610, 'Bamandanga', 5721, 278, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:26', NULL, NULL),
(611, 'Sundarganj', 5720, 278, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:26', NULL, NULL),
(612, 'Chilmari', 5630, 285, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:26', NULL, NULL),
(613, 'Jorgachh', 5631, 285, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:26', NULL, NULL),
(614, 'Kurigram Sadar', 5600, 287, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:26', NULL, NULL),
(615, 'Pandul', 5601, 287, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:26', NULL, NULL),
(616, 'Phulbari', 5680, 287, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:26', NULL, NULL),
(617, 'Nageshwar', 5660, 288, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:26', NULL, NULL),
(618, 'Nazimkhan', 5611, 289, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:26', NULL, NULL),
(619, 'Rajarhat', 5610, 289, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:26', NULL, NULL),
(620, 'Rajibpur', 5650, 576, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:26', NULL, NULL),
(621, 'Raumari', 5640, 290, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:26', NULL, NULL),
(622, 'Bazarhat', 5621, 576, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:26', NULL, NULL),
(623, 'Ulipur', 5620, 291, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:26', NULL, NULL),
(624, 'Aditmari', 5510, 292, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:26', NULL, NULL),
(625, 'Kulaghat SO', 5502, 295, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:26', NULL, NULL),
(626, 'Lalmonirhat Sadar', 5500, 295, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:26', NULL, NULL),
(627, 'Moghalhat', 5501, 295, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:26', NULL, NULL),
(628, 'Baura', 5541, 296, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:26', NULL, NULL),
(629, 'Burimari', 5542, 296, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:26', NULL, NULL),
(630, 'Patgram', 5540, 296, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:26', NULL, NULL),
(631, 'Tushbhandar', 5520, 577, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:26', NULL, NULL),
(632, 'Dimla', 5350, 317, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:26', NULL, NULL),
(633, 'Ghaga Kharibari', 5351, 317, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:26', NULL, NULL),
(634, 'Chilahati', 5341, 318, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:26', NULL, NULL),
(635, 'Domar', 5340, 318, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:26', NULL, NULL),
(636, 'Jaldhaka', 5330, 319, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:26', NULL, NULL),
(637, 'Kishoriganj', 5320, 320, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:26', NULL, NULL),
(638, 'Nilphamari Sadar', 5300, 321, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:26', NULL, NULL),
(639, 'Nilphamari Sugar Mil', 5301, 321, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:26', NULL, NULL),
(640, 'Syedpur', 5310, 322, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:26', NULL, NULL),
(641, 'Syedpur Upashahar', 5311, 322, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:27', NULL, NULL),
(642, 'Boda', 5010, 332, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:27', NULL, NULL),
(643, 'Chotto Dab', 5040, 578, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:27', NULL, NULL),
(644, 'Mirjapur', 5041, 578, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:27', NULL, NULL),
(645, 'Dabiganj', 5020, 333, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:27', NULL, NULL),
(646, 'Panchagar Sadar', 5000, 334, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:27', NULL, NULL),
(647, 'Tetulia', 5030, 335, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:27', NULL, NULL),
(648, 'Badarganj', 5430, 349, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:27', NULL, NULL),
(649, 'Shyampur', 5431, 349, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:27', NULL, NULL),
(650, 'Gangachara', 5410, 350, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:27', NULL, NULL),
(651, 'Haragachh', 5441, 25, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:27', NULL, NULL),
(652, 'Kaunia', 5440, 25, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:27', NULL, NULL),
(653, 'Mithapukur', 5460, 352, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:27', NULL, NULL),
(654, 'Pirgachha', 5450, 353, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:27', NULL, NULL),
(655, 'Alamnagar', 5402, 351, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:27', NULL, NULL),
(656, 'Mahiganj', 5403, 351, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:27', NULL, NULL),
(657, 'Rangpur Cadet Colleg', 5404, 351, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:27', NULL, NULL),
(658, 'Rangpur Carmiecal Col', 5405, 351, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:27', NULL, NULL),
(659, 'Rangpur Sadar', 5400, 351, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:27', NULL, NULL),
(660, 'Rangpur Upa-Shahar', 5401, 351, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:27', NULL, NULL),
(661, 'Taraganj', 5420, 355, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:27', NULL, NULL),
(662, 'Baliadangi', 5140, 365, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:27', NULL, NULL),
(663, 'Lahiri', 5141, 365, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:27', NULL, NULL),
(664, 'Pirganj', 5110, 367, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:27', NULL, NULL),
(665, 'Nekmarad', 5121, 368, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:27', NULL, NULL),
(666, 'Rani Sankail', 5120, 368, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:27', NULL, NULL),
(667, 'Ruhia', 5103, 26, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:27', NULL, NULL),
(668, 'Thakurgaon Road', 5101, 26, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:27', NULL, NULL),
(669, 'Thakurgaon Sadar', 5100, 26, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:27', NULL, NULL),
(670, 'Adamdighi', 5890, 248, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:27', NULL, NULL),
(671, 'Nasharatpur', 5892, 248, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:27', NULL, NULL),
(672, 'Santahar', 5891, 248, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:27', NULL, NULL),
(673, 'Bogra Canttonment', 5801, 249, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:27', NULL, NULL),
(674, 'Bogra Sadar', 5800, 249, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:27', NULL, NULL),
(675, 'Dhunat', 5850, 250, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:27', NULL, NULL),
(676, 'Gosaibari', 5851, 250, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:27', NULL, NULL),
(677, 'Dupchachia', 5880, 251, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:27', NULL, NULL),
(678, 'Talora', 5881, 251, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:27', NULL, NULL),
(679, 'Gabtoli', 5820, 252, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:27', NULL, NULL),
(680, 'Sukhanpukur', 5821, 252, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:27', NULL, NULL),
(681, 'Kahaloo', 5870, 253, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:28', NULL, NULL),
(682, 'Nandigram', 5860, 254, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:28', NULL, NULL),
(683, 'Chandan Baisha', 5831, 255, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:28', NULL, NULL),
(684, 'Sariakandi', 5830, 255, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:28', NULL, NULL),
(685, 'Chandaikona', 5841, 257, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:28', NULL, NULL),
(686, 'Palli Unnyan Accadem', 5842, 257, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:28', NULL, NULL),
(687, 'Sherpur', 5840, 257, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:28', NULL, NULL),
(688, 'Sonatola', 5826, 259, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:28', NULL, NULL),
(689, 'Amnura', 6303, 580, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:28', NULL, NULL),
(690, 'Chapinawbganj Sadar', 6300, 580, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:28', NULL, NULL),
(691, 'Rajarampur', 6301, 580, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:28', NULL, NULL),
(692, 'Ramchandrapur', 6302, 580, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:28', NULL, NULL),
(693, 'Mandumala', 6311, 581, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:28', NULL, NULL),
(694, 'Gomashtapur', 6321, 582, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:28', NULL, NULL),
(695, 'Rohanpur', 6320, 582, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:28', NULL, NULL),
(696, 'Kansart', 6341, 583, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:28', NULL, NULL),
(697, 'Manaksha', 6342, 583, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:28', NULL, NULL),
(698, 'Shibganj U.P.O', 6340, 583, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:28', NULL, NULL),
(699, 'Akklepur', 5940, 279, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:28', NULL, NULL),
(700, 'jamalganj', 5941, 279, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:28', NULL, NULL),
(701, 'Tilakpur', 5942, 279, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:28', NULL, NULL),
(702, 'Joypurhat Sadar', 5900, 280, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:28', NULL, NULL),
(703, 'kalai', 5930, 281, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:28', NULL, NULL),
(704, 'Khetlal', 5920, 282, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:28', NULL, NULL),
(705, 'Panchbibi', 5910, 22, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:28', NULL, NULL),
(706, 'Badalgachhi', 6570, 298, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:28', NULL, NULL),
(707, 'Dhamuirhat', 6580, 299, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:28', NULL, NULL),
(708, 'Naogaon Sadar', 6500, 302, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:28', NULL, NULL),
(709, 'Niamatpur', 6520, 23, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:28', NULL, NULL),
(710, 'Patnitala', 6540, 303, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:28', NULL, NULL),
(711, 'Kashimpur', 6591, 34, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:28', NULL, NULL),
(712, 'Raninagar', 6590, 34, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:28', NULL, NULL),
(713, 'Moduhil', 6561, 305, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:28', NULL, NULL),
(714, 'Sapahar', 6560, 305, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:28', NULL, NULL),
(715, 'Abdulpur', 6422, 584, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:28', NULL, NULL),
(716, 'Gopalpur U.P.O', 6420, 584, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:28', NULL, NULL),
(717, 'Lalpur S.O', 6421, 584, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:28', NULL, NULL),
(718, 'Baraigram', 6432, 585, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:28', NULL, NULL),
(719, 'Dayarampur', 6431, 585, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:29', NULL, NULL),
(720, 'Harua', 6430, 585, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:29', NULL, NULL),
(721, 'Gurudaspur', 6440, 308, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:29', NULL, NULL),
(722, 'Baiddyabal Gharia', 6402, 310, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:29', NULL, NULL),
(723, 'Digapatia', 6401, 310, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:29', NULL, NULL),
(724, 'Madhnagar', 6403, 310, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:29', NULL, NULL),
(725, 'Natore Sadar', 6400, 310, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:29', NULL, NULL),
(726, 'Singra', 6450, 311, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:29', NULL, NULL),
(727, 'Bera', 6680, 323, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:29', NULL, NULL),
(728, 'Kashinathpur', 6682, 323, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:29', NULL, NULL),
(729, 'Nakalia', 6681, 323, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:29', NULL, NULL),
(730, 'Puran Bharenga', 6683, 323, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:29', NULL, NULL),
(731, 'Bhangura', 6640, 324, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:29', NULL, NULL),
(732, 'Chatmohar', 6630, 325, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:29', NULL, NULL),
(733, 'Dhapari', 6621, 327, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:29', NULL, NULL),
(734, 'Ishwardi', 6620, 327, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:29', NULL, NULL),
(735, 'Pakshi', 6622, 327, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:29', NULL, NULL),
(736, 'Rajapur', 6623, 327, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:29', NULL, NULL),
(737, 'Hamayetpur', 6602, 328, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:29', NULL, NULL),
(738, 'Kaliko Cotton Mills', 6601, 328, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:29', NULL, NULL),
(739, 'Pabna Sadar', 6600, 328, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:29', NULL, NULL),
(740, 'Santhia', 6670, 329, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:29', NULL, NULL),
(741, 'Sagarkandi', 6661, 330, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:29', NULL, NULL),
(742, 'Sujanagar', 6660, 330, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:29', NULL, NULL),
(743, 'Arani', 6281, 336, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:29', NULL, NULL),
(744, 'Bagha', 6280, 336, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:29', NULL, NULL),
(745, 'Charghat', 6270, 340, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:29', NULL, NULL),
(746, 'Sarda', 6271, 340, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:29', NULL, NULL),
(747, 'Durgapur', 6240, 340, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:29', NULL, NULL),
(748, 'Godagari', 6290, 341, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:29', NULL, NULL),
(749, 'Premtoli', 6291, 341, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:29', NULL, NULL),
(750, 'Putia', 6260, 345, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:29', NULL, NULL),
(751, 'Binodpur Bazar', 6206, 586, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:29', NULL, NULL),
(752, 'Ghuramara', 6100, 586, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:29', NULL, NULL),
(753, 'Kazla', 6204, 586, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:29', NULL, NULL),
(754, 'Rajshahi Canttonment', 6202, 586, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:29', NULL, NULL),
(755, 'Rajshahi Court', 6201, 586, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:30', NULL, NULL),
(756, 'Rajshahi Sadar', 6000, 586, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:30', NULL, NULL),
(757, 'Rajshahi University', 6205, 586, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:30', NULL, NULL),
(758, 'Sapura', 6203, 586, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:30', NULL, NULL),
(759, 'Tanor', 6230, 348, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:30', NULL, NULL),
(760, 'Belkuchi', 6740, 356, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:30', NULL, NULL),
(761, 'Enayetpur', 6751, 356, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:30', NULL, NULL),
(762, 'Sohagpur', 6741, 356, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:30', NULL, NULL),
(763, 'Sthal', 6752, 356, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:30', NULL, NULL),
(764, 'Dhangora', 6720, 587, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:30', NULL, NULL),
(765, 'Malonga', 6721, 587, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:30', NULL, NULL),
(766, 'Gandail', 6712, 359, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:30', NULL, NULL),
(767, 'Kazipur', 6710, 359, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:30', NULL, NULL),
(768, 'Shuvgachha', 6711, 359, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:30', NULL, NULL),
(769, 'Ullapara R.S', 6761, 364, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:30', NULL, NULL),
(770, 'Ullapara', 6760, 364, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:30', NULL, NULL),
(771, 'Salap', 6763, 364, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:30', NULL, NULL),
(772, 'Lahiri Mohanpur', 6762, 364, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:30', NULL, NULL),
(773, 'Tarash', 6780, 363, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:30', NULL, NULL),
(774, 'Sirajganj Sadar', 6700, 362, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:30', NULL, NULL),
(775, 'Rashidabad', 6702, 362, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:30', NULL, NULL),
(776, 'Raipur', 6701, 362, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:30', NULL, NULL),
(777, 'Shahjadpur', 6770, 361, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:30', NULL, NULL),
(778, 'Porjana', 6771, 361, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:30', NULL, NULL),
(779, 'Kaijuri', 6773, 361, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:30', NULL, NULL),
(780, 'Jamirta', 6772, 361, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:30', NULL, NULL),
(781, 'Swarupkathi', 8520, 588, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:30', NULL, NULL),
(782, 'Kaurikhara', 8522, 588, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:30', NULL, NULL),
(783, 'Jalabari', 8523, 588, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:30', NULL, NULL),
(784, 'Darus Sunnat', 8521, 588, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:30', NULL, NULL),
(785, 'Pirojpur Sadar', 8500, 472, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:30', NULL, NULL),
(786, 'Parerhat', 8502, 472, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:30', NULL, NULL),
(787, 'Hularhat', 8501, 472, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:30', NULL, NULL),
(788, 'Sriramkathi', 8541, 471, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:30', NULL, NULL),
(789, 'Nazirpur', 8540, 471, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:30', NULL, NULL),
(790, 'Tushkhali', 8561, 470, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:30', NULL, NULL),
(791, 'Tiarkhali', 8564, 470, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:31', NULL, NULL),
(792, 'Shilarganj', 8566, 470, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:31', NULL, NULL),
(793, 'Mathbaria', 8560, 470, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:31', NULL, NULL),
(794, 'Halta', 8562, 470, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:31', NULL, NULL),
(795, 'Gulishakhali', 8563, 470, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:31', NULL, NULL),
(796, 'Betmor Natun Hat', 8565, 470, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:31', NULL, NULL),
(797, 'Keundia', 8511, 469, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:31', NULL, NULL),
(798, 'Kaukhali', 8510, 469, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:31', NULL, NULL),
(799, 'Joykul', 8512, 469, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:31', NULL, NULL),
(800, 'Jolagati', 8513, 469, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:31', NULL, NULL),
(801, 'Kanudashkathi', 8551, 468, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:31', NULL, NULL),
(802, 'Dhaoa', 8552, 468, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:31', NULL, NULL),
(803, 'Bhandaria', 8550, 468, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:31', NULL, NULL),
(804, 'Chakhar', 8531, 589, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:31', NULL, NULL),
(805, 'Banaripara', 8530, 589, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:31', NULL, NULL),
(806, 'Subidkhali', 8610, 590, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:31', NULL, NULL),
(807, 'Rahimabad', 8603, 467, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:31', NULL, NULL),
(808, 'Patuakhali Sadar', 8600, 467, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:31', NULL, NULL),
(809, 'Moukaran', 8601, 467, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:31', NULL, NULL),
(810, 'Dumkee', 8602, 467, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:31', NULL, NULL),
(811, 'Mahipur', 8651, 591, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:31', NULL, NULL),
(812, 'Khepupara', 8650, 591, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:31', NULL, NULL),
(813, 'Gazipur Bandar', 8641, 464, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:31', NULL, NULL),
(814, 'Galachipa', 8640, 464, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:31', NULL, NULL),
(815, 'Dashmina', 8630, 30, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:31', NULL, NULL),
(816, 'Kalishari', 8623, 462, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:31', NULL, NULL),
(817, 'Kalaia', 8624, 462, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:31', NULL, NULL),
(818, 'Birpasha', 8622, 462, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:31', NULL, NULL),
(819, 'Bauphal', 8620, 462, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:31', NULL, NULL),
(820, 'Bagabandar', 8621, 462, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:31', NULL, NULL),
(821, 'Nalchhiti', 8420, 460, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:31', NULL, NULL),
(822, 'Beerkathi', 8421, 460, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:31', NULL, NULL),
(823, 'Shoulajalia', 8433, 459, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:31', NULL, NULL),
(824, 'Niamatee', 8432, 459, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:31', NULL, NULL),
(825, 'Kathalia', 8430, 459, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:31', NULL, NULL),
(826, 'Amua', 8431, 459, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:31', NULL, NULL),
(827, 'Shekherhat', 8404, 458, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:31', NULL, NULL),
(828, 'Nabagram', 8401, 458, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:31', NULL, NULL),
(829, 'Jhalokathi Sadar', 8400, 458, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:31', NULL, NULL),
(830, 'Gabha', 8403, 458, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:32', NULL, NULL),
(831, 'Baukathi', 8402, 458, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:32', NULL, NULL),
(832, 'Lalmohan UPO', 8330, 455, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:32', NULL, NULL),
(833, 'Gazaria', 8332, 455, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:32', NULL, NULL),
(834, 'Daurihat', 8331, 455, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:32', NULL, NULL),
(835, 'Hatshoshiganj', 8350, 592, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:32', NULL, NULL),
(836, 'Hajirhat', 8360, 593, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:32', NULL, NULL),
(837, 'Hajipur', 8311, 454, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:32', NULL, NULL),
(838, 'Doulatkhan', 8310, 454, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:32', NULL, NULL),
(839, 'Keramatganj', 8342, 453, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:32', NULL, NULL),
(840, 'Dularhat', 8341, 453, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:32', NULL, NULL),
(841, 'Charfashion', 8340, 453, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:32', NULL, NULL),
(842, 'Mirzakalu', 8321, 452, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:32', NULL, NULL),
(843, 'Borhanuddin UPO', 8320, 452, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:32', NULL, NULL),
(844, 'Joynagar', 8301, 451, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:32', NULL, NULL),
(845, 'Bhola Sadar', 8300, 451, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:32', NULL, NULL),
(846, 'Wazirpur', 8220, 450, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:32', NULL, NULL),
(847, 'Shikarpur', 8224, 450, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:32', NULL, NULL),
(848, 'Jugirkanda', 8222, 450, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:32', NULL, NULL),
(849, 'Dhamura', 8221, 450, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:32', NULL, NULL),
(850, 'Dakuarhat', 8223, 450, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:32', NULL, NULL),
(851, 'Shialguni', 8283, 594, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:32', NULL, NULL),
(852, 'Sahebganj', 8280, 594, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:32', NULL, NULL),
(853, 'Padri Shibpur', 8282, 594, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:32', NULL, NULL),
(854, 'kalaskati', 8284, 594, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:32', NULL, NULL),
(855, 'Charamandi', 8281, 594, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:32', NULL, NULL),
(856, 'Muladi', 8250, 449, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:32', NULL, NULL),
(857, 'Kazirchar', 8251, 449, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:32', NULL, NULL),
(858, 'Charkalekhan', 8252, 449, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:32', NULL, NULL),
(859, 'Ulania', 8272, 448, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:32', NULL, NULL),
(860, 'Nalgora', 8273, 448, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:32', NULL, NULL),
(861, 'Mahendiganj', 8270, 448, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:32', NULL, NULL),
(862, 'Laskarpur', 8271, 448, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:32', NULL, NULL),
(863, 'Langutia', 8274, 448, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:32', NULL, NULL),
(864, 'Tarki Bandar', 8231, 445, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:32', NULL, NULL),
(865, 'Kashemabad', 8232, 445, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:32', NULL, NULL),
(866, 'Gouranadi', 8230, 445, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:32', NULL, NULL),
(867, 'Batajor', 8233, 445, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:32', NULL, NULL),
(868, 'Sugandia', 8203, 447, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:32', NULL, NULL),
(869, 'Saheberhat', 8202, 447, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:33', NULL, NULL),
(870, 'Patang', 8204, 447, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:33', NULL, NULL),
(871, 'Kashipur', 8205, 447, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:33', NULL, NULL),
(872, 'Jaguarhat', 8206, 447, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:33', NULL, NULL),
(873, 'Bukhainagar', 8201, 447, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:33', NULL, NULL),
(874, 'Barishal Sadar', 8200, 447, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:33', NULL, NULL),
(875, 'Osman Manjil', 8261, 595, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:33', NULL, NULL),
(876, 'Barajalia', 8260, 595, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:33', NULL, NULL),
(877, 'Thakur Mallik', 8214, 443, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:33', NULL, NULL),
(878, 'Rahamatpur', 8211, 443, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:33', NULL, NULL),
(879, 'Nizamuddin College', 8215, 443, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:33', NULL, NULL),
(880, 'Madhabpasha', 8213, 443, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:33', NULL, NULL),
(881, 'Chandpasha', 8212, 443, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:33', NULL, NULL),
(882, 'Barishal Cadet', 8216, 443, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:33', NULL, NULL),
(883, 'Babuganj', 8210, 443, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:33', NULL, NULL),
(884, 'Paisarhat', 8242, 442, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:33', NULL, NULL),
(885, 'Gaila', 8241, 442, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:33', NULL, NULL),
(886, 'Agailzhara', 8240, 442, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:33', NULL, NULL),
(887, 'Patharghata', 8720, 441, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:33', NULL, NULL),
(888, 'Kakchira', 8721, 441, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:33', NULL, NULL),
(889, 'Darul Ulam', 8741, 440, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:33', NULL, NULL),
(890, 'Betagi', 8740, 440, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:33', NULL, NULL),
(891, 'Nali Bandar', 8701, 29, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:33', NULL, NULL),
(892, 'Barguna Sadar', 8700, 29, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:33', NULL, NULL),
(893, 'Bamna', 8730, 439, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:33', NULL, NULL),
(894, 'Amtali', 8710, 438, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:33', NULL, NULL),
(895, 'Sylhet Cadet College', 3101, 402, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:33', NULL, NULL),
(896, 'Sylhet Biman Bondar', 3102, 402, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:33', NULL, NULL),
(897, 'Sylhe Sadar', 3100, 402, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:33', NULL, NULL),
(898, 'Silam', 3105, 402, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:33', NULL, NULL),
(899, 'Shahajalal Science & Eng. University', 3114, 402, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:33', NULL, NULL),
(900, 'Ranga Hajiganj', 3109, 402, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:33', NULL, NULL),
(901, 'Mogla', 3108, 402, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:33', NULL, NULL),
(902, 'Lalbazar', 3113, 402, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:33', NULL, NULL),
(903, 'Khadimnagar', 3103, 402, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:33', NULL, NULL),
(904, 'Kamalbazer', 3112, 402, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:33', NULL, NULL),
(905, 'Kadamtali', 3111, 402, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:34', NULL, NULL),
(906, 'Jalalabad Cantoment', 3104, 402, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:34', NULL, NULL),
(907, 'Birahimpur', 3106, 402, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:34', NULL, NULL),
(908, 'Companiganj', 3140, 395, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:34', NULL, NULL),
(909, 'Manikganj', 3182, 401, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:34', NULL, NULL),
(910, 'Kanaighat', 3180, 401, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:34', NULL, NULL),
(911, 'Gachbari', 3183, 401, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:34', NULL, NULL),
(912, 'Chatulbazar', 3181, 401, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:34', NULL, NULL),
(913, 'Jakiganj', 3190, 596, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:34', NULL, NULL),
(914, 'Ichhamati', 3191, 596, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:34', NULL, NULL),
(915, 'Jainthapur', 3156, 400, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:34', NULL, NULL),
(916, 'Ranaping', 3163, 398, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:34', NULL, NULL),
(917, 'Gopalgannj', 3160, 398, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:34', NULL, NULL),
(918, 'Dhaka Dakkhin', 3161, 398, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:34', NULL, NULL),
(919, 'Dakkhin Bhadashore', 3162, 398, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:34', NULL, NULL),
(920, 'banigram', 3164, 398, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:34', NULL, NULL),
(921, 'Jaflong', 3151, 399, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:34', NULL, NULL),
(922, 'Gowainghat', 3150, 399, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:34', NULL, NULL),
(923, 'Chiknagul', 3152, 399, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:34', NULL, NULL),
(924, 'Fenchuganj SareKarkh', 3117, 397, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:34', NULL, NULL),
(925, 'Fenchuganj', 3116, 397, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:34', NULL, NULL),
(926, 'Singer kanch', 3134, 394, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:34', NULL, NULL),
(927, 'Doulathpur', 3132, 394, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:34', NULL, NULL),
(928, 'Deokalas', 3133, 394, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:34', NULL, NULL),
(929, 'Dashghar', 3131, 394, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:34', NULL, NULL),
(930, 'Bishwanath', 3130, 394, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:34', NULL, NULL),
(931, 'Salia bazar', 3174, 27, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:34', NULL, NULL),
(932, 'Mathiura', 3172, 27, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:34', NULL, NULL),
(933, 'Kurar bazar', 3173, 27, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:34', NULL, NULL),
(934, 'jaldup', 3171, 27, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:34', NULL, NULL),
(935, 'Churkai', 3175, 27, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:34', NULL, NULL),
(936, 'Beanibazar', 3170, 27, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:34', NULL, NULL),
(937, 'Tajpur', 3123, 393, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:34', NULL, NULL),
(938, 'Omarpur', 3126, 393, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:34', NULL, NULL),
(939, 'Natun Bazar', 3129, 393, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:34', NULL, NULL),
(940, 'Kathal Khair', 3127, 393, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:34', NULL, NULL),
(941, 'Karua', 3121, 393, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:34', NULL, NULL),
(942, 'Goala Bazar', 3124, 393, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:34', NULL, NULL),
(943, 'Gaharpur', 3128, 393, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:34', NULL, NULL),
(944, 'Brahman Shashon', 3122, 393, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:34', NULL, NULL),
(945, 'Begumpur', 3125, 393, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:34', NULL, NULL),
(946, 'Balaganj', 3120, 393, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:35', NULL, NULL),
(947, 'Tahirpur', 3030, 392, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:35', NULL, NULL),
(948, 'Sunamganj Sadar', 3000, 391, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:35', NULL, NULL),
(949, 'Patharia', 3002, 391, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:35', NULL, NULL),
(950, 'Pagla', 3001, 391, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:35', NULL, NULL),
(951, '  Syedpur  ', 3061, 389, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:35', NULL, NULL),
(952, 'Shiramsi', 3065, 389, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:35', NULL, NULL),
(953, 'Rasulganj', 3064, 389, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:35', NULL, NULL),
(954, 'Jagnnathpur', 3060, 389, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:35', NULL, NULL),
(955, 'Hasan Fatemapur', 3063, 389, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:35', NULL, NULL),
(956, 'Atuajan', 3062, 389, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:35', NULL, NULL),
(957, 'Duara bazar', 3070, 388, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:35', NULL, NULL),
(958, 'Jagdal', 3041, 597, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:35', NULL, NULL),
(959, 'Dhirai Chandpur', 3040, 597, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:35', NULL, NULL),
(960, 'Moinpur', 3086, 384, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:35', NULL, NULL),
(961, 'Khurma', 3085, 384, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:35', NULL, NULL),
(962, 'jahidpur', 3087, 384, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:35', NULL, NULL),
(963, 'Islamabad', 3088, 384, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:35', NULL, NULL),
(964, 'Gabindaganj Natun Ba', 3084, 384, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:35', NULL, NULL),
(965, 'Gabindaganj', 3083, 384, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:35', NULL, NULL),
(966, 'Chourangi Bazar', 3893, 384, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:35', NULL, NULL),
(967, 'Chhatak Paper Mills', 3082, 384, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:35', NULL, NULL),
(968, 'Chhatak Cement Facto', 3081, 384, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:35', NULL, NULL),
(969, 'Chhatak', 3080, 384, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:35', NULL, NULL),
(970, 'Bishamsapur', 3010, 383, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:35', NULL, NULL),
(971, 'Sreemangal', 3210, 382, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:35', NULL, NULL),
(972, 'Satgaon', 3214, 382, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:35', NULL, NULL),
(973, 'Narain Chora', 3211, 382, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:35', NULL, NULL),
(974, 'Khejurichhara', 3213, 382, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:35', NULL, NULL),
(975, 'Kalighat', 3212, 382, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:35', NULL, NULL),
(976, 'Rajnagar', 3240, 381, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:35', NULL, NULL),
(977, 'Moulvibazar Sadar', 3200, 380, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:35', NULL, NULL),
(978, 'Monumukh', 3202, 380, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:35', NULL, NULL),
(979, 'Barakapan', 3201, 380, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:35', NULL, NULL),
(980, 'Afrozganj', 3203, 380, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:35', NULL, NULL),
(981, 'Tillagaon', 3231, 379, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:35', NULL, NULL),
(982, 'Prithimpasha', 3233, 379, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:36', NULL, NULL),
(983, 'Langla', 3232, 379, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:36', NULL, NULL),
(984, 'Kulaura', 3230, 379, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:36', NULL, NULL),
(985, 'Kajaldhara', 3234, 379, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:36', NULL, NULL),
(986, 'Baramchal', 3237, 379, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:36', NULL, NULL),
(987, 'Shamsher Nagar', 3223, 378, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:36', NULL, NULL),
(988, 'Patrakhola', 3222, 378, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:36', NULL, NULL),
(989, 'Munshibazar', 3224, 378, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:36', NULL, NULL),
(990, 'Keramatnaga', 3221, 378, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:36', NULL, NULL),
(991, 'Kamalganj', 3220, 378, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:36', NULL, NULL),
(992, 'Purbashahabajpur', 3253, 376, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:36', NULL, NULL),
(993, 'Juri', 3251, 376, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:36', NULL, NULL),
(994, 'Dhakkhinbag', 3252, 376, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:36', NULL, NULL),
(995, 'Baralekha', 3250, 376, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:36', NULL, NULL),
(996, 'Inathganj', 3374, 375, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:36', NULL, NULL),
(997, 'Goplarbazar', 3371, 375, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:36', NULL, NULL),
(998, 'Golduba', 3372, 375, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:36', NULL, NULL),
(999, 'Digalbak', 3373, 375, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:36', NULL, NULL),
(1000, 'Shahajibazar', 3332, 374, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:36', NULL, NULL),
(1001, 'Saihamnagar', 3333, 374, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:36', NULL, NULL),
(1002, 'Madhabpur', 3330, 374, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:36', NULL, NULL),
(1003, 'Itakhola', 3331, 374, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:36', NULL, NULL),
(1004, 'Shaestaganj', 3301, 373, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:36', NULL, NULL),
(1005, 'Hobiganj Sadar', 3300, 373, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:36', NULL, NULL),
(1006, 'Gopaya', 3302, 373, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:36', NULL, NULL),
(1007, 'Narapati', 3322, 372, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:36', NULL, NULL),
(1008, 'Chunarughat', 3320, 372, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:36', NULL, NULL),
(1009, 'Chandpurbagan', 3321, 372, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:36', NULL, NULL),
(1010, 'Azmireeganj', 3360, 369, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:36', NULL, NULL),
(1011, 'Bahubal', 3310, 370, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:36', NULL, NULL),
(1012, 'Baniachang', 3350, 371, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:36', NULL, NULL),
(1013, 'Jatrapasha', 3351, 371, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:36', NULL, NULL),
(1014, 'Kadirganj', 3352, 371, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:36', NULL, NULL),
(1015, 'Rangamati Sadar', 4500, 414, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:36', NULL, NULL),
(1016, 'Rajsthali', 4540, 413, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:36', NULL, NULL),
(1017, 'Nanichhar', 4520, 412, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:36', NULL, NULL),
(1018, 'Marishya', 4590, 599, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:36', NULL, NULL),
(1019, 'Longachh', 4580, 599, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:36', NULL, NULL),
(1020, 'Kaptai Project', 4532, 409, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:37', NULL, NULL),
(1021, 'Kaptai Nuton Bazar', 4533, 409, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:37', NULL, NULL),
(1022, 'Kaptai', 4530, 409, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:37', NULL, NULL),
(1023, 'Chandraghona', 4531, 409, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:37', NULL, NULL),
(1024, 'Jarachhari', 4560, 410, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:37', NULL, NULL),
(1025, 'Kalampati', 4510, 600, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:37', NULL, NULL),
(1026, 'Betbunia', 4511, 600, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:37', NULL, NULL),
(1027, 'Baghaaichhari', 4550, 405, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:37', NULL, NULL),
(1028, 'Barakal', 4570, 406, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:37', NULL, NULL),
(1029, 'T.P. Lamua', 3865, 64, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:37', NULL, NULL),
(1030, 'Senbag', 3860, 64, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:37', NULL, NULL),
(1031, 'Kankirhat', 3863, 64, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:37', NULL, NULL),
(1032, 'Kallyandi', 3861, 64, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:37', NULL, NULL),
(1033, 'Chatarpaia', 3864, 64, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:37', NULL, NULL),
(1034, 'Beezbag', 3862, 64, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:37', NULL, NULL),
(1035, 'Sonapur', 3802, 67, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:37', NULL, NULL),
(1036, 'Pak Kishoreganj', 3804, 67, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:37', NULL, NULL),
(1037, 'Noakhali Sadar', 3800, 67, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:37', NULL, NULL),
(1038, 'Noakhali College', 3801, 67, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:37', NULL, NULL),
(1039, 'Mriddarhat', 3806, 67, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:37', NULL, NULL),
(1040, 'Khalifar Hat', 3808, 67, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:37', NULL, NULL),
(1041, 'Kabirhat', 3807, 67, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:37', NULL, NULL),
(1042, 'Din Monir Hat', 3803, 67, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:37', NULL, NULL),
(1043, 'Charam Tua', 3809, 67, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:37', NULL, NULL),
(1044, 'Char Jabbar', 3812, 67, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:37', NULL, NULL),
(1045, 'Chaprashir Hat', 3811, 67, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:37', NULL, NULL),
(1046, 'Tamoraddi', 3892, 15, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:37', NULL, NULL),
(1047, 'Hatiya', 3890, 15, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:37', NULL, NULL),
(1048, 'Afazia', 3891, 15, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:37', NULL, NULL),
(1049, 'Solla', 3875, 62, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:37', NULL, NULL),
(1050, 'Shingbahura', 3883, 62, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:37', NULL, NULL),
(1051, 'Sampara', 3882, 62, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:37', NULL, NULL),
(1052, 'Sahapur', 3881, 62, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:37', NULL, NULL),
(1053, 'Rezzakpur', 3874, 62, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:37', NULL, NULL),
(1054, 'Palla', 3871, 62, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:38', NULL, NULL),
(1055, 'Khilpara', 3872, 62, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:38', NULL, NULL),
(1056, 'Karihati', 3877, 62, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:38', NULL, NULL),
(1057, 'Dosh Gharia', 3878, 62, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:38', NULL, NULL),
(1058, 'Chatkhil', 3870, 62, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:38', NULL, NULL),
(1059, 'Bodalcourt', 3873, 62, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:38', NULL, NULL),
(1060, 'Bansa Bazar', 3879, 62, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:38', NULL, NULL),
(1061, 'Thanar Hat', 3845, 61, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:38', NULL, NULL),
(1062, 'Tangirpar', 3832, 61, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:38', NULL, NULL),
(1063, 'Sonaimuri', 3827, 61, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:38', NULL, NULL),
(1064, 'Rajganj', 3834, 61, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:38', NULL, NULL),
(1065, 'Oachhekpur', 3835, 61, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:38', NULL, NULL),
(1066, 'Nandiapara', 3841, 61, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:38', NULL, NULL),
(1067, 'Nadona', 3839, 61, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:38', NULL, NULL),
(1068, 'Mir Owarishpur', 3823, 61, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:38', NULL, NULL),
(1069, 'Maheshganj', 3838, 61, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:38', NULL, NULL),
(1070, 'Khalishpur', 3842, 61, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:38', NULL, NULL),
(1071, 'Khalafat Bazar', 3833, 61, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:38', NULL, NULL),
(1072, 'Joynarayanpur', 3829, 61, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:38', NULL, NULL),
(1073, 'Joyag', 3844, 61, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:38', NULL, NULL),
(1074, 'Jamidar Hat', 3825, 61, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:38', NULL, NULL),
(1075, 'Gapalpur', 3828, 61, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:38', NULL, NULL),
(1076, 'Daurgapur', 3848, 61, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:38', NULL, NULL),
(1077, 'Dauti', 3843, 61, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:38', NULL, NULL),
(1078, 'Choumohani', 3821, 61, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:38', NULL, NULL),
(1079, 'Bhabani Jibanpur', 3837, 61, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:38', NULL, NULL),
(1080, 'Begumganj', 3820, 61, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:38', NULL, NULL),
(1081, 'Bazra', 3824, 61, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:38', NULL, NULL),
(1082, 'Banglabazar', 3822, 61, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:38', NULL, NULL),
(1083, 'Amisha Para', 3847, 61, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:38', NULL, NULL),
(1084, 'Alaiarpur', 3831, 61, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:38', NULL, NULL),
(1085, 'Charhajari', 3851, 601, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:38', NULL, NULL),
(1086, 'Basur Hat', 3850, 601, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:38', NULL, NULL),
(1087, 'Raypur', 3710, 58, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:38', NULL, NULL),
(1088, 'Rakhallia', 3711, 58, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:38', NULL, NULL),
(1089, 'Nagerdighirpar', 3712, 58, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:39', NULL, NULL),
(1090, 'Haydarganj', 3713, 58, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:39', NULL, NULL),
(1091, 'Bhuabari', 3714, 58, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:39', NULL, NULL),
(1092, 'Ramganj', 3720, 59, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:39', NULL, NULL),
(1093, 'Panpara', 3722, 59, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:39', NULL, NULL),
(1094, 'Naagmud', 3724, 59, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:39', NULL, NULL),
(1095, 'Kanchanpur', 3723, 59, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:39', NULL, NULL),
(1096, 'Dolta', 3725, 59, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:39', NULL, NULL),
(1097, 'Alipur', 3721, 59, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:39', NULL, NULL),
(1098, 'Rupchara', 3705, 57, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:39', NULL, NULL),
(1099, 'Mandari', 3703, 57, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:39', NULL, NULL),
(1100, 'Lakshimpur Sadar', 3700, 57, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:39', NULL, NULL),
(1101, 'Duttapara', 3706, 57, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:39', NULL, NULL),
(1102, 'Dalal Bazar', 3701, 57, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:39', NULL, NULL),
(1103, 'Choupalli', 3707, 57, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:39', NULL, NULL),
(1104, 'Chandraganj', 3708, 57, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:39', NULL, NULL),
(1105, 'Bhabaniganj', 3702, 57, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:39', NULL, NULL),
(1106, 'Amani Lakshimpur', 3709, 57, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:39', NULL, NULL),
(1107, 'Hajirghat', 3731, 602, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:39', NULL, NULL),
(1108, 'Char Alexgander', 3730, 602, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:39', NULL, NULL),
(1109, 'Ramghar Head Office', 4440, 603, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:39', NULL, NULL),
(1110, 'Panchhari', 4410, 54, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:39', NULL, NULL),
(1111, 'Matiranga', 4450, 53, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:39', NULL, NULL),
(1112, 'Manikchhari', 4460, 52, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:39', NULL, NULL),
(1113, 'Laxmichhari', 4470, 50, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:39', NULL, NULL),
(1114, 'Khagrachari Sadar', 4400, 49, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:39', NULL, NULL),
(1115, 'Diginala', 4420, 48, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:39', NULL, NULL),
(1116, 'Sonagazi', 3930, 47, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:39', NULL, NULL),
(1117, 'Motiganj', 3931, 47, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:39', NULL, NULL),
(1118, 'Kazirhat', 3933, 47, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:39', NULL, NULL),
(1119, 'Ahmadpur', 3932, 47, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:39', NULL, NULL),
(1120, 'Shuarbazar', 3941, 46, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:39', NULL, NULL),
(1121, 'Pashurampur', 3940, 46, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:39', NULL, NULL),
(1122, 'Fulgazi', 3942, 46, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:39', NULL, NULL),
(1123, 'Sharshadie', 3902, 44, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:39', NULL, NULL),
(1124, 'Laskarhat', 3903, 44, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:39', NULL, NULL),
(1125, 'Feni Sadar', 3900, 44, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:39', NULL, NULL);
INSERT INTO `sa_postoffices` (`POST_OFFICE_ID`, `POST_OFFICE_ENAME`, `POST_CODE`, `THANA_ID`, `THANA_NAME`, `ORDER_SL`, `UD_POFFICE_ID`, `IS_ACTIVE`, `CREATED_BY`, `CREATED_AT`, `UPDATED_AT`, `UPDATED_BY`) VALUES
(1126, 'Fazilpur', 3901, 44, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:39', NULL, NULL),
(1127, 'Dudmukha', 3921, 7, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:39', NULL, NULL),
(1128, 'Dagondhuia', 3920, 7, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:40', NULL, NULL),
(1129, 'Chhilonia', 3922, 7, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:40', NULL, NULL),
(1130, 'Puabashimulia', 3913, 6, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:40', NULL, NULL),
(1131, 'Daraga Hat', 3912, 6, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:40', NULL, NULL),
(1132, 'Chhagalnaia', 3910, 6, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:40', NULL, NULL),
(1133, 'Ukhia', 4750, 5, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:40', NULL, NULL),
(1134, 'Teknaf', 4760, 4, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:40', NULL, NULL),
(1135, 'St.Martin', 4762, 4, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:40', NULL, NULL),
(1136, 'Hnila', 4761, 4, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:40', NULL, NULL),
(1137, 'Ramu', 4730, 3, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:40', NULL, NULL),
(1138, 'Kutubdia', 4720, 541, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:40', NULL, NULL),
(1139, 'Gorakghat', 4710, 604, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:40', NULL, NULL),
(1140, 'Zhilanja', 4701, 540, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:40', NULL, NULL),
(1141, 'Eidga', 4702, 540, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:40', NULL, NULL),
(1142, 'Coxs Bazar Sadar', 4700, 540, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:40', NULL, NULL),
(1143, 'Malumghat', 4743, 605, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:40', NULL, NULL),
(1144, 'Chiringga S.O', 4741, 605, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:40', NULL, NULL),
(1145, 'Chiringga', 4740, 605, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:40', NULL, NULL),
(1146, 'Badarkali', 4742, 605, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:40', NULL, NULL),
(1147, 'Sonakanda', 3544, 536, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:40', NULL, NULL),
(1148, 'Ramchandarpur', 3541, 536, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:40', NULL, NULL),
(1149, 'Pantibazar', 3545, 536, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:40', NULL, NULL),
(1150, 'Muradnagar', 3540, 536, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:40', NULL, NULL),
(1151, 'Companyganj', 3542, 536, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:40', NULL, NULL),
(1152, 'Bangra', 3543, 536, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:40', NULL, NULL),
(1153, 'Nangalkot', 3580, 537, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:40', NULL, NULL),
(1154, 'Gunabati', 3583, 537, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:40', NULL, NULL),
(1155, 'Dhalua', 3581, 537, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:40', NULL, NULL),
(1156, 'Chhariabazar', 3582, 537, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:40', NULL, NULL),
(1157, 'Lakshamanpur', 3571, 533, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:40', NULL, NULL),
(1158, 'Laksam', 3570, 533, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:40', NULL, NULL),
(1159, 'Bipulasar', 3572, 533, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:40', NULL, NULL),
(1160, 'Homna', 3546, 531, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:40', NULL, NULL),
(1161, 'Gangamandal', 3531, 530, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:40', NULL, NULL),
(1162, 'Dhamtee', 3533, 530, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:40', NULL, NULL),
(1163, 'Davidhar', 3530, 530, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:40', NULL, NULL),
(1164, 'Barashalghar', 3532, 530, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:40', NULL, NULL),
(1165, 'Eliotganj', 3519, 529, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:40', NULL, NULL),
(1166, 'Daudkandi', 3516, 529, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:41', NULL, NULL),
(1167, 'Dashpara', 3518, 529, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:41', NULL, NULL),
(1168, 'Suaganj', 3504, 529, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:41', NULL, NULL),
(1169, 'Halimanagar', 3502, 528, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:41', NULL, NULL),
(1170, 'Courtbari', 3503, 528, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:41', NULL, NULL),
(1171, 'Comilla Sadar', 3500, 528, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:41', NULL, NULL),
(1172, 'Comilla Contoment', 3501, 528, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:41', NULL, NULL),
(1173, 'Chouddagram', 3550, 527, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:41', NULL, NULL),
(1174, 'Chiora', 3552, 527, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:41', NULL, NULL),
(1175, 'Batisa', 3551, 527, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:41', NULL, NULL),
(1176, 'Madhaiabazar', 3511, 526, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:41', NULL, NULL),
(1177, 'Chandia', 3510, 526, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:41', NULL, NULL),
(1178, 'Maynamoti bazar', 3521, 525, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:41', NULL, NULL),
(1179, 'Burichang', 3520, 525, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:41', NULL, NULL),
(1180, 'Brahmanpara', 3526, 524, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:41', NULL, NULL),
(1181, 'Poyalgachha', 3561, 523, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:41', NULL, NULL),
(1182, 'Murdafarganj', 3562, 523, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:41', NULL, NULL),
(1183, 'Barura', 3560, 523, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:41', NULL, NULL),
(1184, 'Sitakunda', 4310, 522, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:41', NULL, NULL),
(1185, 'Kumira', 4314, 522, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:41', NULL, NULL),
(1186, 'Jafrabad', 4317, 522, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:41', NULL, NULL),
(1187, 'Fouzdarhat', 4316, 522, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:41', NULL, NULL),
(1188, 'Bhatiari', 4315, 522, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:41', NULL, NULL),
(1189, 'Bawashbaria', 4313, 522, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:41', NULL, NULL),
(1190, 'Baroidhala', 4311, 522, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:41', NULL, NULL),
(1191, 'Barabkunda', 4312, 522, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:41', NULL, NULL),
(1192, 'Satkania', 4386, 522, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:41', NULL, NULL),
(1193, 'Bazalia', 4388, 522, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:41', NULL, NULL),
(1194, 'Baitul Ijjat', 4387, 522, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:41', NULL, NULL),
(1195, 'Urirchar', 4302, 520, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:41', NULL, NULL),
(1196, 'Shiberhat', 4301, 520, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:41', NULL, NULL),
(1197, 'Sandwip', 4300, 520, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:41', NULL, NULL),
(1198, 'Rouzan', 4340, 519, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:41', NULL, NULL),
(1199, 'Mohamuni', 4348, 519, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:41', NULL, NULL),
(1200, 'Kundeshwari', 4342, 519, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:41', NULL, NULL),
(1201, 'jagannath Hat', 4344, 519, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:41', NULL, NULL),
(1202, 'Guzra Noapara', 4346, 519, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:41', NULL, NULL),
(1203, 'Gahira', 4343, 519, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:41', NULL, NULL),
(1204, 'Fatepur', 4345, 519, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:42', NULL, NULL),
(1205, 'Dewanpur', 4347, 519, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:42', NULL, NULL),
(1206, 'Beenajuri', 4341, 519, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:42', NULL, NULL),
(1207, 'B.I.T Post Office', 4349, 519, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:42', NULL, NULL),
(1208, 'Rangunia', 4360, 519, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:42', NULL, NULL),
(1209, 'Dhamair', 4361, 519, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:42', NULL, NULL),
(1210, 'Patia Head Office', 4370, 516, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:42', NULL, NULL),
(1211, 'Budhpara', 4371, 516, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:42', NULL, NULL),
(1212, 'Mohazanhat', 4328, 513, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:42', NULL, NULL),
(1213, 'Mirsharai', 4320, 513, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:42', NULL, NULL),
(1214, 'Korerhat', 4327, 513, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:42', NULL, NULL),
(1215, 'Joarganj', 4324, 513, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:42', NULL, NULL),
(1216, 'Darrogahat', 4322, 513, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:42', NULL, NULL),
(1217, 'Bharawazhat', 4323, 513, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:42', NULL, NULL),
(1218, 'Azampur', 4325, 513, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:42', NULL, NULL),
(1219, 'Abutorab', 4321, 513, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:42', NULL, NULL),
(1220, 'Padua', 4397, 512, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:42', NULL, NULL),
(1221, 'Lohagara', 4396, 512, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:42', NULL, NULL),
(1222, 'Chunti', 4398, 512, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:42', NULL, NULL),
(1223, 'Khan Bahadur', 4391, 606, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:42', NULL, NULL),
(1224, 'Jaldi', 4390, 606, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:42', NULL, NULL),
(1225, 'Gunagari', 4392, 606, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:42', NULL, NULL),
(1226, 'Yunus Nagar', 4338, 509, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:42', NULL, NULL),
(1227, 'Nuralibari', 4337, 509, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:42', NULL, NULL),
(1228, 'Mirzapuur', 4334, 509, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:42', NULL, NULL),
(1229, 'Madrasa', 4339, 509, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:42', NULL, NULL),
(1230, 'Katirhat', 4333, 509, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:42', NULL, NULL),
(1231, 'Hathazari', 4330, 509, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:42', NULL, NULL),
(1232, 'Gorduara', 4332, 509, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:42', NULL, NULL),
(1233, 'Fatahabad', 4335, 509, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:42', NULL, NULL),
(1234, 'Chitt.University', 4331, 509, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:42', NULL, NULL),
(1235, 'Narayanhat', 4355, 507, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:42', NULL, NULL),
(1236, 'Nanupur', 4351, 507, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:42', NULL, NULL),
(1237, 'Najirhat', 4353, 507, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:42', NULL, NULL),
(1238, 'Harualchhari', 4354, 507, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:42', NULL, NULL),
(1239, 'Fatikchhari', 4350, 507, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:42', NULL, NULL),
(1240, 'Bhandar Sharif', 4352, 507, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:43', NULL, NULL),
(1241, 'Gachbaria', 4381, 607, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:43', NULL, NULL),
(1242, 'East Joara', 4380, 607, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:43', NULL, NULL),
(1243, 'Dohazari', 4382, 607, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:43', NULL, NULL),
(1244, 'Barma', 4383, 607, NULL, NULL, NULL, 0, 0, '2016-07-24 08:38:43', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sa_roles`
--

CREATE TABLE IF NOT EXISTS `sa_roles` (
  `ROLE_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `DISPLAY_NAME` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DESCRIPTION` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CREATED_AT` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `UPDATED_AT` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`ROLE_ID`),
  UNIQUE KEY `sa_roles_name_unique` (`NAME`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=8 ;

--
-- Dumping data for table `sa_roles`
--

INSERT INTO `sa_roles` (`ROLE_ID`, `NAME`, `DISPLAY_NAME`, `DESCRIPTION`, `CREATED_AT`, `UPDATED_AT`) VALUES
(1, 'lease-manager', 'Lease manager', 'Manage lease', '2016-07-13 22:24:58', '2016-07-16 02:18:52'),
(2, 'tender-manager', 'Tender manager', 'Manage tenders', '2016-07-13 22:25:33', '2016-08-08 00:24:15'),
(3, 'project-manager', 'Project manager', 'Manage projects and project flat and shop', '2016-07-13 22:26:58', '2016-07-16 02:16:12'),
(4, 'admin', 'Admin', 'Site administrator with all access', '2016-07-16 02:19:34', '2016-07-16 02:19:34'),
(5, 'citizen', 'Citizen', 'NCC citizen', '2016-08-08 00:29:05', '2016-08-08 00:29:05'),
(6, 'account-manager', 'Account manager', 'Manage account task', '2016-08-08 02:50:00', '2016-08-08 02:50:00'),
(7, 'role-name', 'Role View Name', 'Role Description', '2016-09-28 00:34:44', '2016-09-28 00:35:02');

-- --------------------------------------------------------

--
-- Table structure for table `sa_role_user`
--

CREATE TABLE IF NOT EXISTS `sa_role_user` (
  `USER_ID` bigint(20) NOT NULL,
  `ROLE_ID` bigint(20) NOT NULL,
  PRIMARY KEY (`USER_ID`,`ROLE_ID`),
  KEY `sa_role_user_role_id_foreign` (`ROLE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `sa_role_user`
--

INSERT INTO `sa_role_user` (`USER_ID`, `ROLE_ID`) VALUES
(6, 1),
(7, 2),
(5, 3),
(1, 4),
(9, 5),
(18, 5),
(19, 5),
(10, 6);

-- --------------------------------------------------------

--
-- Table structure for table `sa_thanas`
--

CREATE TABLE IF NOT EXISTS `sa_thanas` (
  `THANA_ID` smallint(8) NOT NULL AUTO_INCREMENT COMMENT 'Primary Key Of sa_thanas Table.',
  `DISTRICT_ID` int(8) DEFAULT NULL COMMENT 'Foreign Key To DISTRICT_ID Column Of sa_districts Table.',
  `THANA_ENAME` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Thanas Or Upazilla Name In English.',
  `THANA_LNAME` varchar(120) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Thanas Or Upazilla Name In Local Language.',
  `ORDER_SL` smallint(4) DEFAULT NULL COMMENT 'Ascending Serial No.',
  `UD_THANA_CODE` varchar(8) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'User Define Thanas Or Upazilla Code.',
  `IS_ACTIVE` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0=NO, 1=YES',
  `CREATED_BY` int(11) NOT NULL,
  `CREATED_AT` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `UPDATED_AT` datetime DEFAULT NULL,
  `UPDATED_BY` int(10) DEFAULT NULL,
  `TAREA_STATUS` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Thana Area Status, T=Thana, U=Upazilla, B=Both',
  `UAREA_STATUS` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Union/Ward Area Status, W=Ward, U=Union, B=Both',
  PRIMARY KEY (`THANA_ID`),
  KEY `SA_THANA_DISTRICT_ID` (`DISTRICT_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=609 ;

--
-- Dumping data for table `sa_thanas`
--

INSERT INTO `sa_thanas` (`THANA_ID`, `DISTRICT_ID`, `THANA_ENAME`, `THANA_LNAME`, `ORDER_SL`, `UD_THANA_CODE`, `IS_ACTIVE`, `CREATED_BY`, `CREATED_AT`, `UPDATED_AT`, `UPDATED_BY`, `TAREA_STATUS`, `UAREA_STATUS`) VALUES
(1, 12, 'MAHESHKHALI', NULL, NULL, '022249', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(2, 12, 'PEKUA', NULL, NULL, '022256', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'U'),
(3, 12, 'RAMU', NULL, NULL, '022266', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'U'),
(4, 12, 'TEKNAF', NULL, NULL, '022290', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(5, 12, 'UKHIA UPAZILA', NULL, NULL, '022294', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'U'),
(6, 16, 'CHHAGALNAIYA', NULL, NULL, '023014', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(7, 16, 'DAGANBHUIYAN', NULL, NULL, '023025', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(14, 15, 'SHALTHA', NULL, NULL, '032985', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'U'),
(15, 48, 'HATIYA', NULL, NULL, '027536', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(16, 15, 'NAGARKANDA', NULL, NULL, '032962', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(17, 28, 'HOSSAINPUR', NULL, NULL, '034827', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(18, 38, 'LOHAJANG', NULL, NULL, '035944', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'U'),
(19, 42, 'NARAYANGANJ SADAR', NULL, NULL, '036758', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(20, 60, 'NAKLA', NULL, NULL, '038967', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(21, 14, 'FULBARI', NULL, NULL, '052738', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(22, 21, 'PANCHBIBI', NULL, NULL, '053874', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(23, 40, 'NIAMATPUR', NULL, NULL, '056469', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'U'),
(24, 49, 'ATGHARIA', NULL, NULL, '057605', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(25, 56, 'KAUNIA', NULL, NULL, '058542', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(26, 64, 'THAKURGAON SADAR', NULL, NULL, '059494', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(27, 62, 'BEANI BAZAR', NULL, NULL, '069117', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(28, 13, 'BADDA', NULL, NULL, '032604', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'T', 'B'),
(29, 3, 'BARGUNA SADAR', NULL, NULL, '010428', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(30, 51, 'DASHMINA', NULL, NULL, '017852', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'U'),
(31, 48, 'COMPANIGANJ', NULL, NULL, '027521', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(32, 57, 'BHEDARGANJ', NULL, NULL, '038614', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(33, 34, 'SREEPUR', NULL, NULL, '045595', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'U'),
(34, 40, 'RANINAGAR', NULL, NULL, '056485', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'U'),
(35, 20, 'LAKHAI', NULL, NULL, '063668', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'U'),
(36, 4, 'BAKERGANJ', NULL, NULL, '010607', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(37, 61, 'JAMALGANJ', NULL, NULL, '069050', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'U'),
(38, 15, 'BOALMARI', NULL, NULL, '032918', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(39, 39, 'HALUAGHAT', NULL, NULL, '036124', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'U'),
(40, 23, 'BAGHER PARA', NULL, NULL, '044109', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(41, 10, 'DAMURHUDA', NULL, NULL, '041831', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(42, 13, 'PALTAN', NULL, NULL, '032665', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'T', 'W'),
(43, 13, 'NEW MARKET', NULL, NULL, '032663', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'T', 'W'),
(44, 16, 'FENI SADAR', NULL, NULL, '023029', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(45, 16, 'FULGAZI', NULL, NULL, '023041', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'U'),
(46, 16, 'PARSHURAM', NULL, NULL, '023051', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(47, 16, 'SONAGAZI', NULL, NULL, '023094', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(48, 26, 'DIGHINALA', NULL, NULL, '024643', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'U'),
(49, 26, 'KHAGRACHHARI SADAR', NULL, NULL, '024649', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(50, 26, 'LAKSHMICHHARI', NULL, NULL, '024661', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'U'),
(51, 26, 'MAHALCHHARI', NULL, NULL, '024665', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'U'),
(52, 26, 'MANIKCHHARI', NULL, NULL, '024667', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'U'),
(53, 26, 'MATIRANGA', NULL, NULL, '024670', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(54, 26, 'PANCHHARI', NULL, NULL, '024677', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'U'),
(55, 26, 'RAMGARH', NULL, NULL, '024680', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(56, 31, 'KAMALNAGAR', NULL, NULL, '025133', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'U'),
(57, 31, 'LAKSHMIPUR SADAR', NULL, NULL, '025143', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(58, 31, 'ROYPUR', NULL, NULL, '025158', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(59, 31, 'RAMGANJ', NULL, NULL, '025165', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(60, 31, 'RAMGATI', NULL, NULL, '025173', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(61, 48, 'BEGUMGANJ', NULL, NULL, '027507', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(62, 48, 'CHATKHIL', NULL, NULL, '027510', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(63, 48, 'KABIRHAT', NULL, NULL, '027547', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(64, 48, 'SENBAGH', NULL, NULL, '027580', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(65, 48, 'SONAIMURI', NULL, NULL, '027583', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(66, 48, 'SUBARNACHAR', NULL, NULL, '027585', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'U'),
(67, 48, 'NOAKHALI SADAR', NULL, NULL, '027587', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(68, 13, 'RAMNA', NULL, NULL, '032666', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'T', 'W'),
(69, 13, 'SABUJBAGH', NULL, NULL, '032668', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'T', 'B'),
(70, 13, 'SAVAR', NULL, NULL, '032672', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(71, 13, 'SHAH ALI', NULL, NULL, '032674', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'T', 'W'),
(72, 13, 'SHAHBAGH', NULL, NULL, '032675', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'T', 'W'),
(73, 13, 'SHYAMPUR', NULL, NULL, '032676', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'T', 'B'),
(74, 13, 'SUTRAPUR', NULL, NULL, '032688', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'T', 'W'),
(75, 13, 'TEJGAON', NULL, NULL, '032690', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'T', 'W'),
(76, 13, 'TEJGAON IND. AREA', NULL, NULL, '032692', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'T', 'W'),
(77, 13, 'TURAG', NULL, NULL, '032693', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'T', 'U'),
(78, 13, 'UTTARA', NULL, NULL, '032695', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'T', 'B'),
(79, 13, 'UTTAR KHAN', NULL, NULL, '032696', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'T', 'U'),
(80, 15, 'ALFADANGA', NULL, NULL, '032903', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'U'),
(81, 15, 'BHANGA', NULL, NULL, '032910', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(82, 15, 'CHAR BHADRASAN', NULL, NULL, '032921', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'U'),
(83, 15, 'FARIDPUR SADAR', NULL, NULL, '032947', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(84, 15, 'MADHUKHALI', NULL, NULL, '032956', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'U'),
(85, 15, 'SADARPUR', NULL, NULL, '032984', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'U'),
(86, 18, 'GAZIPUR SADAR', NULL, NULL, '033330', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(87, 18, 'KALIAKAIR', NULL, NULL, '033332', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(88, 18, 'KALIGANJ', NULL, NULL, '033334', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'U'),
(89, 18, 'KAPASIA', NULL, NULL, '033336', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'U'),
(90, 18, 'SREEPUR', NULL, NULL, '033386', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(91, 19, 'GOPALGANJ SADAR', NULL, NULL, '033532', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(92, 19, 'KASHIANI', NULL, NULL, '033543', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'U'),
(93, 19, 'KOTALIPARA', NULL, NULL, '033551', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(94, 19, 'MUKSUDPUR', NULL, NULL, '033558', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(95, 19, 'TUNGIPARA', NULL, NULL, '033591', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(96, 22, 'BAKSHIGANJ', NULL, NULL, '033907', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'U'),
(97, 22, 'DEWANGANJ', NULL, NULL, '033915', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(98, 22, 'ISLAMPUR', NULL, NULL, '033929', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(99, 22, 'JAMALPUR SADAR', NULL, NULL, '033936', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(100, 22, 'MADARGANJ', NULL, NULL, '033958', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(101, 22, 'MELANDAHA', NULL, NULL, '033961', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(102, 22, 'SARISHABARI UPAZILA', NULL, NULL, '033985', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(103, 28, 'AUSTAGRAM', NULL, NULL, '034802', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'U'),
(104, 28, 'BAJITPUR', NULL, NULL, '034806', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(105, 28, 'BHAIRAB', NULL, NULL, '034811', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(106, 28, 'ITNA', NULL, NULL, '034833', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'U'),
(107, 28, 'KARIMGANJ', NULL, NULL, '034842', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(108, 28, 'KATIADI', NULL, NULL, '034845', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(109, 28, 'KISHOREGANJ SADAR', NULL, NULL, '034849', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(110, 28, 'KULIAR CHAR', NULL, NULL, '034854', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(111, 28, 'MITHAMAIN', NULL, NULL, '034859', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'U'),
(112, 28, 'NIKLI', NULL, NULL, '034876', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'U'),
(113, 28, 'PAKUNDIA', NULL, NULL, '034879', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(114, 28, 'TARAIL', NULL, NULL, '034892', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'U'),
(115, 33, 'KALKINI', NULL, NULL, '035440', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(116, 33, 'MADARIPUR SADAR', NULL, NULL, '035454', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(117, 33, 'RAJOIR', NULL, NULL, '035480', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'U'),
(118, 33, 'SHIB CHAR', NULL, NULL, '035487', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(119, 35, 'DAULATPUR', NULL, NULL, '035610', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'U'),
(120, 35, 'GHIOR', NULL, NULL, '035622', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'U'),
(121, 35, 'HARIRAMPUR', NULL, NULL, '035628', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'U'),
(122, 35, 'MANIKGANJ SADAR', NULL, NULL, '035646', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(123, 35, 'SATURIA', NULL, NULL, '035670', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'U'),
(124, 35, 'SHIBALAYA', NULL, NULL, '035678', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'U'),
(125, 35, 'SINGAIR', NULL, NULL, '035682', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(126, 38, 'GAZARIA', NULL, NULL, '035924', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'U'),
(127, 38, 'MUNSHIGANJ SADAR', NULL, NULL, '035956', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(128, 38, 'SERAJDIKHAN', NULL, NULL, '035974', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'U'),
(129, 38, 'SREENAGAR', NULL, NULL, '035984', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'U'),
(130, 38, 'TONGIBARI', NULL, NULL, '035994', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'U'),
(131, 39, 'BHALUKA', NULL, NULL, '036113', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(132, 39, 'DHOBAURA', NULL, NULL, '036116', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'U'),
(133, 39, 'FULBARIA', NULL, NULL, '036120', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(134, 39, 'GAFFARGAON', NULL, NULL, '036122', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(135, 39, 'GAURIPUR', NULL, NULL, '036123', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(136, 39, 'ISHWARGANJ', NULL, NULL, '036131', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(137, 39, 'MYMENSINGH SADAR', NULL, NULL, '036152', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(138, 39, 'MUKTAGACHHA', NULL, NULL, '036165', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(139, 39, 'NANDAIL', NULL, NULL, '036172', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(140, 39, 'PHULPUR', NULL, NULL, '036181', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(141, 39, 'TRISHAL', NULL, NULL, '036194', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(142, 42, 'ARAIHAZAR', NULL, NULL, '036702', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'U'),
(143, 42, 'SONARGAON', NULL, NULL, '036704', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(144, 42, 'BANDAR', NULL, NULL, '036706', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(145, 42, 'RUPGANJ', NULL, NULL, '036768', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(146, 43, 'BELABO', NULL, NULL, '036807', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'U'),
(147, 43, 'MANOHARDI', NULL, NULL, '036852', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(148, 43, 'NARSINGDI SADAR', NULL, NULL, '036860', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(149, 43, 'PALASH', NULL, NULL, '036863', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(150, 43, 'ROYPURA', NULL, NULL, '036864', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(151, 43, 'SHIBPUR', NULL, NULL, '036876', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(152, 46, 'ATPARA', NULL, NULL, '037204', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'U'),
(153, 46, 'BARHATTA', NULL, NULL, '037209', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'U'),
(154, 46, 'DURGAPUR', NULL, NULL, '037218', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(155, 46, 'KHALIAJURI', NULL, NULL, '037238', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'U'),
(156, 46, 'KALMAKANDA', NULL, NULL, '037240', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'U'),
(157, 46, 'KENDUA', NULL, NULL, '037247', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(158, 46, 'MADAN', NULL, NULL, '037256', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(159, 46, 'MOHANGANJ', NULL, NULL, '037263', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(160, 46, 'NETROKONA SADAR', NULL, NULL, '037274', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(161, 46, 'PURBADHALA', NULL, NULL, '037283', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(162, 54, 'BALIAKANDI', NULL, NULL, '038207', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'U'),
(163, 54, 'GOALANDA', NULL, NULL, '038229', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(164, 54, 'PANGSHA', NULL, NULL, '038273', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(165, 54, 'RAJBARI SADAR', NULL, NULL, '038276', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(166, 57, 'DAMUDYA', NULL, NULL, '038625', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(167, 57, 'GOSAIRHAT', NULL, NULL, '038636', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'U'),
(168, 57, 'NARIA', NULL, NULL, '038665', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(169, 57, 'SHARIATPUR SADAR', NULL, NULL, '038669', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(170, 57, 'ZANJIRA', NULL, NULL, '038694', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(171, 60, 'JHENAIGATI', NULL, NULL, '038937', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'U'),
(172, 60, 'NALITABARI', NULL, NULL, '038970', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(173, 60, 'SHERPUR SADAR', NULL, NULL, '038988', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(174, 60, 'SREEBARDI', NULL, NULL, '038990', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(175, 63, 'BASAIL', NULL, NULL, '039309', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'U'),
(176, 63, 'BHUAPUR', NULL, NULL, '039319', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(177, 63, 'DELDUAR', NULL, NULL, '039323', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'U'),
(178, 63, 'DHANBARI', NULL, NULL, '039325', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(179, 63, 'GHATAIL', NULL, NULL, '039328', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(180, 63, 'GOPALPUR', NULL, NULL, '039338', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(181, 63, 'KALIHATI', NULL, NULL, '039347', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(182, 63, 'MADHUPUR', NULL, NULL, '039357', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(183, 63, 'MIRZAPUR', NULL, NULL, '039366', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(184, 63, 'NAGARPUR', NULL, NULL, '039376', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'U'),
(185, 63, 'SAKHIPUR', NULL, NULL, '039385', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(186, 63, 'TANGAIL SADAR', NULL, NULL, '039395', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(187, 1, 'BAGERHAT SADAR', NULL, NULL, '040108', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(188, 1, 'CHITALMARI', NULL, NULL, '040114', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'U'),
(189, 1, 'FAKIRHAT', NULL, NULL, '040134', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'U'),
(190, 1, 'KACHUA', NULL, NULL, '040138', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'U'),
(191, 1, 'MOLLAHAT', NULL, NULL, '040156', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'U'),
(192, 1, 'MONGLA', NULL, NULL, '040158', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(193, 1, 'MORRELGANJ', NULL, NULL, '040160', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(194, 1, 'RAMPAL', NULL, NULL, '040173', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'U'),
(195, 1, 'SARANKHOLA', NULL, NULL, '040177', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'U'),
(196, 10, 'ALAMDANGA', NULL, NULL, '041807', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(197, 10, 'CHUADANGA SADAR', NULL, NULL, '041823', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(198, 10, 'JIBAN NAGAR', NULL, NULL, '041855', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(199, 23, 'ABHAYNAGAR', NULL, NULL, '044104', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(200, 23, 'CHAUGACHHA', NULL, NULL, '044111', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(201, 23, 'JHIKARGACHHA', NULL, NULL, '044123', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(202, 23, 'KESHABPUR', NULL, NULL, '044138', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(203, 23, 'KOTWALI', NULL, NULL, '044147', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'T', 'B'),
(204, 23, 'MANIRAMPUR', NULL, NULL, '044161', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(205, 23, 'SHARSHA', NULL, NULL, '044190', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(206, 25, 'HARINAKUNDA', NULL, NULL, '044414', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(207, 25, 'JHENAIDAH SADAR', NULL, NULL, '044419', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(208, 25, 'KALIGANJ', NULL, NULL, '044433', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(209, 25, 'KOTCHANDPUR', NULL, NULL, '044442', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(210, 25, 'MAHESHPUR', NULL, NULL, '044471', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(211, 25, 'SHAILKUPA', NULL, NULL, '044480', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(212, 27, 'BATIAGHATA', NULL, NULL, '044712', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'T', 'U'),
(213, 27, 'DACOPE', NULL, NULL, '044717', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(214, 27, 'DAULATPUR', NULL, NULL, '044721', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'W'),
(215, 27, 'DUMURIA', NULL, NULL, '044730', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'U'),
(216, 27, 'DIGHALIA', NULL, NULL, '044740', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'U'),
(217, 27, 'KHALISHPUR', NULL, NULL, '044745', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'T', 'W'),
(218, 27, 'KHAN JAHAN ALI', NULL, NULL, '044748', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'T', 'W'),
(219, 27, 'KHULNA SADAR', NULL, NULL, '044751', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'T', 'W'),
(220, 27, 'KOYRA', NULL, NULL, '044753', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'U'),
(221, 27, 'PAIKGACHHA', NULL, NULL, '044764', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(222, 27, 'PHULTALA', NULL, NULL, '044769', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'T', 'U'),
(223, 27, 'RUPSA', NULL, NULL, '044775', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'U'),
(224, 27, 'SONADANGA', NULL, NULL, '044785', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'T', 'W'),
(225, 27, 'TEROKHADA', NULL, NULL, '044794', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'U'),
(226, 30, 'BHERAMARA', NULL, NULL, '045015', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(227, 30, 'DAULATPUR', NULL, NULL, '045039', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'U'),
(228, 30, 'KHOKSA', NULL, NULL, '045063', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(229, 30, 'KUMARKHALI', NULL, NULL, '045071', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(230, 30, 'KUSHTIA SADAR', NULL, NULL, '045079', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(231, 30, 'MIRPUR', NULL, NULL, '045094', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'T', 'B'),
(232, 34, 'MAGURA SADAR', NULL, NULL, '045557', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(233, 34, 'MOHAMMADPUR', NULL, NULL, '045566', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'T', 'U'),
(234, 34, 'SHALIKHA', NULL, NULL, '045585', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'U'),
(235, 36, 'GANGNI', NULL, NULL, '045747', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(236, 36, 'MUJIB NAGAR', NULL, NULL, '045760', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'U'),
(237, 36, 'MEHERPUR SADAR', NULL, NULL, '045787', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(238, 41, 'KALIA', NULL, NULL, '046528', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(239, 41, 'LOHAGARA', NULL, NULL, '046552', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(240, 41, 'NARAIL SADAR', NULL, NULL, '046576', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(241, 58, 'ASSASUNI', NULL, NULL, '048704', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'U'),
(242, 58, 'DEBHATA', NULL, NULL, '048725', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'U'),
(243, 58, 'KALAROA', NULL, NULL, '048743', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(244, 58, 'KALIGANJ', NULL, NULL, '048747', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'U'),
(245, 58, 'SATKHIRA SADAR', NULL, NULL, '048782', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(246, 58, 'SHYAMNAGAR', NULL, NULL, '048786', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'U'),
(247, 58, 'TALA', NULL, NULL, '048790', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'U'),
(248, 6, 'ADAMDIGHI', NULL, NULL, '051006', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(249, 6, 'BOGRA SADAR', NULL, NULL, '051020', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(250, 6, 'DHUNAT', NULL, NULL, '051027', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(251, 6, 'DHUPCHANCHIA', NULL, NULL, '051033', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(252, 6, 'GABTALI', NULL, NULL, '051040', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(253, 6, 'KAHALOO', NULL, NULL, '051054', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(254, 6, 'NANDIGRAM', NULL, NULL, '051067', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(255, 6, 'SARIAKANDI', NULL, NULL, '051081', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(256, 6, 'SHAJAHANPUR', NULL, NULL, '051085', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(257, 6, 'SHERPUR', NULL, NULL, '051088', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(258, 6, 'SHIBGANJ', NULL, NULL, '051094', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(259, 6, 'SONATOLA', NULL, NULL, '051095', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(260, 14, 'BIRAMPUR', NULL, NULL, '052710', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(261, 14, 'BIRGANJ', NULL, NULL, '052712', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(262, 14, 'BIRAL', NULL, NULL, '052717', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'U'),
(263, 14, 'BOCHAGANJ', NULL, NULL, '052721', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(264, 14, 'CHIRIRBANDAR', NULL, NULL, '052730', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'U'),
(265, 14, 'GHORAGHAT', NULL, NULL, '052743', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(266, 14, 'HAKIMPUR', NULL, NULL, '052747', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(267, 14, 'KAHAROLE', NULL, NULL, '052756', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'U'),
(268, 14, 'KHANSAMA', NULL, NULL, '052760', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'U'),
(269, 14, 'DINAJPUR SADAR', NULL, NULL, '052764', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(270, 14, 'NAWABGANJ', NULL, NULL, '052769', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'U'),
(271, 14, 'PARBATIPUR', NULL, NULL, '052777', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(272, 17, 'FULCHHARI', NULL, NULL, '053221', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'U'),
(273, 17, 'GAIBANDHA SADAR', NULL, NULL, '053224', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(274, 17, 'GOBINDAGANJ', NULL, NULL, '053230', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(275, 17, 'PALASHBARI', NULL, NULL, '053267', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'U'),
(276, 17, 'SADULLAPUR', NULL, NULL, '053282', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'U'),
(277, 17, 'SAGHATA', NULL, NULL, '053288', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'U'),
(278, 17, 'SUNDARGANJ', NULL, NULL, '053291', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(279, 21, 'AKKELPUR', NULL, NULL, '053813', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(280, 21, 'JOYPURHAT SADAR', NULL, NULL, '053847', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(281, 21, 'KALAI', NULL, NULL, '053858', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(282, 21, 'KHETLAL', NULL, NULL, '053861', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'U'),
(283, 29, 'BHURUNGAMARI', NULL, NULL, '054906', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'U'),
(284, 29, 'CHAR RAJIBPUR', NULL, NULL, '054908', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'U'),
(285, 29, 'CHILMARI', NULL, NULL, '054909', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'U'),
(286, 29, 'PHULBARI', NULL, NULL, '054918', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'U'),
(287, 29, 'KURIGRAM SADAR', NULL, NULL, '054952', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(288, 29, 'NAGESHWARI', NULL, NULL, '054961', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(289, 29, 'RAJARHAT', NULL, NULL, '054977', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'U'),
(290, 29, 'RAUMARI', NULL, NULL, '054979', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'U'),
(291, 29, 'ULIPUR', NULL, NULL, '054994', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(292, 32, 'ADITMARI', NULL, NULL, '055202', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'U'),
(293, 32, 'HATIBANDHA', NULL, NULL, '055233', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'U'),
(294, 32, 'KALIGANJ', NULL, NULL, '055239', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'U'),
(295, 32, 'LALMONIRHAT SADAR', NULL, NULL, '055255', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(296, 32, 'PATGRAM', NULL, NULL, '055270', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(297, 40, 'ATRAI', NULL, NULL, '056403', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'U'),
(298, 40, 'BADALGACHHI', NULL, NULL, '056406', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'U'),
(299, 40, 'DHAMOIRHAT', NULL, NULL, '056428', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(300, 40, 'MANDA', NULL, NULL, '056447', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'U'),
(301, 40, 'MAHADEBPUR', NULL, NULL, '056450', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'U'),
(302, 40, 'NAOGAON SADAR', NULL, NULL, '056460', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(303, 40, 'PATNITALA', NULL, NULL, '056475', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(304, 40, 'PORSHA', NULL, NULL, '056479', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'U'),
(305, 40, 'SAPAHAR', NULL, NULL, '056486', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'U'),
(306, 44, 'BAGATIPARA', NULL, NULL, '056909', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(307, 44, 'BARAIGRAM', NULL, NULL, '056915', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(308, 44, 'GURUDASPUR', NULL, NULL, '056941', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(309, 44, 'LALPUR', NULL, NULL, '056944', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(310, 44, 'NATORE SADAR', NULL, NULL, '056963', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(311, 44, 'SINGRA', NULL, NULL, '056991', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(312, 45, 'BHOLAHAT', NULL, NULL, '057018', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'U'),
(313, 45, 'GOMASTAPUR', NULL, NULL, '057037', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(314, 45, 'NACHOLE', NULL, NULL, '057056', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(315, 45, 'CHAPAI NABABGANJ SADAR', NULL, NULL, '057066', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(316, 45, 'SHIBGANJ', NULL, NULL, '057088', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(317, 47, 'DIMLA UPAZILA', NULL, NULL, '057312', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'U'),
(318, 47, 'DOMAR UPAZILA', NULL, NULL, '057315', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(319, 47, 'JALDHAKA UPAZILA', NULL, NULL, '057336', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(320, 47, 'KISHOREGANJ UPAZILA', NULL, NULL, '057345', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'U'),
(321, 47, 'NILPHAMARI SADAR UPAZ', NULL, NULL, '057364', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(322, 47, 'SAIDPUR UPAZILA', NULL, NULL, '057385', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(323, 49, 'BERA', NULL, NULL, '057616', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(324, 49, 'BHANGURA', NULL, NULL, '057619', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(325, 49, 'CHATMOHAR', NULL, NULL, '057622', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(326, 49, 'FARIDPUR', NULL, NULL, '057633', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(327, 49, 'ISHWARDI', NULL, NULL, '057639', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(328, 49, 'PABNA SADAR', NULL, NULL, '057655', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(329, 49, 'SANTHIA', NULL, NULL, '057672', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(330, 49, 'SUJANAGAR', NULL, NULL, '057683', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(331, 50, 'ATWARI', NULL, NULL, '057704', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'U'),
(332, 50, 'BODA', NULL, NULL, '057725', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(333, 50, 'DEBIGANJ', NULL, NULL, '057734', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'U'),
(334, 50, 'PANCHAGARH SADAR', NULL, NULL, '057773', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(335, 50, 'TENTULIA', NULL, NULL, '057790', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'U'),
(336, 53, 'BAGHA', NULL, NULL, '058110', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(337, 53, 'BAGHMARA', NULL, NULL, '058112', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(338, 53, 'BOALIA', NULL, NULL, '058122', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'T', 'W'),
(339, 53, 'CHARGHAT', NULL, NULL, '058125', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'T', 'B'),
(340, 53, 'DURGAPUR', NULL, NULL, '058131', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(341, 53, 'GODAGARI', NULL, NULL, '058134', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(342, 53, 'MATIHAR', NULL, NULL, '058140', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'T', 'W'),
(343, 53, 'MOHANPUR', NULL, NULL, '058153', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(344, 53, 'PABA', NULL, NULL, '058172', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'T', 'B'),
(345, 53, 'PUTHIA', NULL, NULL, '058182', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(346, 53, 'RAJPARA', NULL, NULL, '058185', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'T', 'W'),
(347, 53, 'SHAH MAKHDUM', NULL, NULL, '058190', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'W'),
(348, 53, 'TANORE', NULL, NULL, '058194', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'T', 'B'),
(349, 56, 'BADARGANJ', NULL, NULL, '058503', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(350, 56, 'GANGACHARA', NULL, NULL, '058527', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'U'),
(351, 56, 'RANGPUR SADAR', NULL, NULL, '058549', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(352, 56, 'MITHA PUKUR', NULL, NULL, '058558', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'U'),
(353, 56, 'PIRGACHHA', NULL, NULL, '058573', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'U'),
(354, 56, 'PIRGANJ', NULL, NULL, '058576', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'U'),
(355, 56, 'TARAGANJ', NULL, NULL, '058592', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'U'),
(356, 59, 'BELKUCHI', NULL, NULL, '058811', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(357, 59, 'CHAUHALI', NULL, NULL, '058827', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'U'),
(358, 59, 'KAMARKHANDA', NULL, NULL, '058844', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'U'),
(359, 59, 'KAZIPUR', NULL, NULL, '058850', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(360, 59, 'ROYGANJ', NULL, NULL, '058861', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(361, 59, 'SHAHJADPUR', NULL, NULL, '058867', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(362, 59, 'SIRAJGANJ SADAR', NULL, NULL, '058878', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(363, 59, 'TARASH', NULL, NULL, '058889', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'U'),
(364, 59, 'ULLAH PARA', NULL, NULL, '058894', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(365, 64, 'BALIADANGI', NULL, NULL, '059408', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'U'),
(366, 64, 'HARIPUR', NULL, NULL, '059451', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'U'),
(367, 64, 'PIRGANJ', NULL, NULL, '059482', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(368, 64, 'RANISANKAIL', NULL, NULL, '059486', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(369, 20, 'AJMIRIGANJ', NULL, NULL, '063602', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(370, 20, 'BAHUBAL', NULL, NULL, '063605', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'U'),
(371, 20, 'BANIACHONG', NULL, NULL, '063611', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'U'),
(372, 20, 'CHUNARUGHAT', NULL, NULL, '063626', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(373, 20, 'HABIGANJ SADAR', NULL, NULL, '063644', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(374, 20, 'MADHABPUR', NULL, NULL, '063671', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(375, 20, 'NABIGANJ', NULL, NULL, '063677', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(376, 37, 'BARLEKHA', NULL, NULL, '065814', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(377, 37, 'JURI', NULL, NULL, '065835', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'U'),
(378, 37, 'KAMALGANJ', NULL, NULL, '065856', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(379, 37, 'KULAURA', NULL, NULL, '065865', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(380, 37, 'MAULVIBAZAR SADAR', NULL, NULL, '065874', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(381, 37, 'RAJNAGAR', NULL, NULL, '065880', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'U'),
(382, 37, 'SREEMANGAL', NULL, NULL, '065883', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(383, 61, 'BISHWAMBARPUR', NULL, NULL, '069018', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'U'),
(384, 61, 'CHHATAK', NULL, NULL, '069023', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(385, 61, 'DAKSHIN SUNAMGANJ', NULL, NULL, '069027', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'U'),
(386, 61, 'DERAI', NULL, NULL, '069029', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(387, 61, 'DHARAMPASHA', NULL, NULL, '069032', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'U'),
(388, 61, 'DOWARABAZAR', NULL, NULL, '069033', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'U'),
(389, 61, 'JAGANNATHPUR', NULL, NULL, '069047', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(390, 61, 'SULLA', NULL, NULL, '069086', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'U'),
(391, 61, 'SUNAMGANJ SADAR', NULL, NULL, '069089', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(392, 61, 'TAHIRPUR', NULL, NULL, '069092', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'U'),
(393, 62, 'BALAGANJ', NULL, NULL, '069108', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'U'),
(394, 62, 'BISHWANATH', NULL, NULL, '069120', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'U'),
(395, 62, 'COMPANIGANJ', NULL, NULL, '069127', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'U'),
(396, 62, 'DAKSHIN SURMA', NULL, NULL, '069131', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'U'),
(397, 62, 'FENCHUGANJ', NULL, NULL, '069135', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'U'),
(398, 62, 'GOPALGANJ', NULL, NULL, '069138', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(399, 62, 'GOWAINGHAT', NULL, NULL, '069141', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'U'),
(400, 62, 'JAINTIAPUR', NULL, NULL, '069153', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'U'),
(401, 62, 'KANAIGHAT', NULL, NULL, '069159', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(402, 62, 'SYLHET SADAR', NULL, NULL, '069162', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'T', 'B'),
(403, 62, 'ZAKIGANJ', NULL, NULL, '069194', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(404, 18, 'TONGI', NULL, NULL, '033387', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'W'),
(405, 55, 'BAGHAICHHARI', NULL, NULL, '028407', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(406, 55, 'BARKAL UPAZILA', NULL, NULL, '028421', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'U'),
(407, 55, 'KAWKHALI (BETBUNIA)', NULL, NULL, '028425', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'U'),
(408, 55, 'BELAI CHHARI  UPAZI', NULL, NULL, '028429', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'U'),
(409, 55, 'KAPTAI  UPAZILA', NULL, NULL, '028436', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'U'),
(410, 55, 'JURAI CHHARI UPAZIL', NULL, NULL, '028447', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'U'),
(411, 55, 'LANGADU  UPAZILA', NULL, NULL, '028458', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'U'),
(412, 55, 'NANIARCHAR  UPAZILA', NULL, NULL, '028475', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'U'),
(413, 55, 'RAJASTHALI  UPAZILA', NULL, NULL, '028478', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'U'),
(414, 55, 'RANGAMATI SADAR  UP', NULL, NULL, '028487', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(415, 13, 'ADABOR', NULL, NULL, '032602', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'T', 'W'),
(416, 13, 'BIMAN BANDAR', NULL, NULL, '032606', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'T', 'B'),
(417, 13, 'CANTONMENT', NULL, NULL, '032608', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'T', 'W'),
(418, 13, 'DAKSHINKHAN', NULL, NULL, '032610', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'T', 'U'),
(419, 13, 'DEMRA', NULL, NULL, '032612', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'T', 'U'),
(420, 13, 'DHAMRAI', NULL, NULL, '032614', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(421, 13, 'DHANMONDI', NULL, NULL, '032616', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'T', 'W'),
(422, 13, 'DOHAR', NULL, NULL, '032618', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(423, 13, 'GULSHAN', NULL, NULL, '032626', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'T', 'W'),
(424, 13, 'HAZARIBAGH', NULL, NULL, '032628', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'T', 'B'),
(425, 13, 'JATRABARI', NULL, NULL, '032629', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'T', 'B'),
(426, 13, 'KAFRUL', NULL, NULL, '032630', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'T', 'W'),
(427, 13, 'KAMRANGIR CHAR', NULL, NULL, '032634', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'T', 'U'),
(428, 13, 'KHILGAON', NULL, NULL, '032636', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'T', 'B'),
(429, 13, 'KHILKHET', NULL, NULL, '032637', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'T', 'U'),
(430, 13, 'KERANIGANJ', NULL, NULL, '032638', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'U'),
(431, 13, 'KOTWALI', NULL, NULL, '032640', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'T', 'W'),
(432, 13, 'LALBAGH', NULL, NULL, '032642', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'T', 'W'),
(433, 13, 'MIRPUR', NULL, NULL, '032648', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'T', 'W'),
(434, 13, 'MOHAMMADPUR', NULL, NULL, '032650', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'T', 'W'),
(435, 13, 'MOTIJHEEL', NULL, NULL, '032654', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'T', 'W'),
(436, 13, 'NAWABGANJ', NULL, NULL, '032662', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'U'),
(437, 13, 'PALLABI', NULL, NULL, '032664', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'T', 'W'),
(438, 3, 'AMTALI', NULL, NULL, '010409', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(439, 3, 'BAMNA', NULL, NULL, '010419', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'U'),
(440, 3, 'BETAGI', NULL, NULL, '010447', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(441, 3, 'PATHARGHATA', NULL, NULL, '010485', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(442, 4, 'AGAILJHARA', NULL, NULL, '010602', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'U'),
(443, 4, 'BABUGANJ', NULL, NULL, '010603', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'U'),
(444, 4, 'BANARI PARA', NULL, NULL, '010610', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(445, 4, 'GAURNADI', NULL, NULL, '010632', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(446, 4, 'HIZLA', NULL, NULL, '010636', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'U'),
(447, 4, 'BARISAL SADAR (KOTWALI)', NULL, NULL, '010651', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'T', 'B'),
(448, 4, 'MHENDIGANJ', NULL, NULL, '010662', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(449, 4, 'MULADI', NULL, NULL, '010669', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(450, 4, 'WAZIRPUR', NULL, NULL, '010694', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'U'),
(451, 5, 'BHOLA SADAR', NULL, NULL, '010918', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(452, 5, 'BURHANUDDIN', NULL, NULL, '010921', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(453, 5, 'CHAR FASSON', NULL, NULL, '010925', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(454, 5, 'DAULAT KHAN', NULL, NULL, '010929', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(455, 5, 'LALMOHAN', NULL, NULL, '010954', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(456, 5, 'MANPURA', NULL, NULL, '010965', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'U'),
(457, 5, 'TAZUMUDDIN', NULL, NULL, '010991', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'U'),
(458, 24, 'JHALOKATI SADAR', NULL, NULL, '014240', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(459, 24, 'KANTHALIA', NULL, NULL, '014243', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'U'),
(460, 24, 'NALCHITY', NULL, NULL, '014273', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(461, 24, 'RAJAPUR', NULL, NULL, '014284', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'U'),
(462, 51, 'BAUPHAL', NULL, NULL, '017838', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(463, 51, 'DUMKI UPAZILA', NULL, NULL, '017855', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'U'),
(464, 51, 'GALACHIPA', NULL, NULL, '017857', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(465, 51, 'KALA PARA', NULL, NULL, '017866', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(466, 51, 'MIRZAGANJ UPAZILA', NULL, NULL, '017876', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'U'),
(467, 51, 'PATUAKHALI SADAR', NULL, NULL, '017895', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(468, 52, 'BHANDARIA', NULL, NULL, '017914', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'U'),
(469, 52, 'KAWKHALI', NULL, NULL, '017947', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'U'),
(470, 52, 'MATHBARIA', NULL, NULL, '017958', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(471, 52, 'NAZIRPUR UPAZILA', NULL, NULL, '017976', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'U'),
(472, 52, 'PIROJPUR SADAR', NULL, NULL, '017980', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(473, 52, 'NESARABAD (SWARUPKATI)', NULL, NULL, '017987', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(474, 52, 'ZIANAGAR', NULL, NULL, '017990', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'U'),
(475, 2, 'ALIKADAM', NULL, NULL, '020304', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'U'),
(476, 2, 'BANDARBAN SADAR', NULL, NULL, '020314', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(477, 2, 'LAMA', NULL, NULL, '020351', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(478, 2, 'NAIKHONGCHHARI', NULL, NULL, '020373', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'U'),
(479, 2, 'ROWANGCHHARI', NULL, NULL, '020389', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'U'),
(480, 2, 'RUMA', NULL, NULL, '020391', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'U'),
(481, 2, 'THANCHI', NULL, NULL, '020395', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'U'),
(482, 7, 'AKHAURA', NULL, NULL, '021202', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(483, 7, 'BANCHHARAMPUR', NULL, NULL, '021204', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'U'),
(484, 7, 'BRAHMANBARIA SADAR', NULL, NULL, '021213', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(485, 7, 'ASHUGANJ', NULL, NULL, '021233', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'U'),
(486, 7, 'KASBA', NULL, NULL, '021263', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(487, 7, 'NABINAGAR', NULL, NULL, '021285', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(488, 7, 'NASIRNAGAR', NULL, NULL, '021290', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'U'),
(489, 7, 'SARAIL', NULL, NULL, '021294', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'U'),
(490, 8, 'CHANDPUR SADAR', NULL, NULL, '021322', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(491, 8, 'FARIDGANJ', NULL, NULL, '021345', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(492, 8, 'HAIM CHAR', NULL, NULL, '021347', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'U'),
(493, 8, 'HAJIGANJ', NULL, NULL, '021349', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(494, 8, 'KACHUA', NULL, NULL, '021358', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(495, 8, 'MATLAB DAKSHIN', NULL, NULL, '021376', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(496, 8, 'MATLAB UTTAR', NULL, NULL, '021379', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(497, 8, 'SHAHRASTI', NULL, NULL, '021395', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(498, 9, 'ANOWARA', NULL, NULL, '021504', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'U'),
(499, 9, 'BAYEJID BOSTAMI', NULL, NULL, '021506', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'T', 'B'),
(500, 9, 'BANSHKHALI', NULL, NULL, '021508', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(501, 9, 'BAKALIA', NULL, NULL, '021510', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'T', 'W'),
(502, 9, 'BOALKHALI', NULL, NULL, '021512', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'U'),
(503, 9, 'CHANDANAISH', NULL, NULL, '021518', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'T', 'B'),
(504, 9, 'CHANDGAON', NULL, NULL, '021519', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'T', 'W'),
(505, 9, 'CHITTAGONG PORT', NULL, NULL, '021520', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'T', 'W'),
(506, 9, 'DOUBLE MOORING', NULL, NULL, '021528', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'T', 'W'),
(507, 9, 'FATIKCHHARI', NULL, NULL, '021533', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'U'),
(508, 9, 'HALISHAHAR', NULL, NULL, '021535', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'T', 'W'),
(509, 9, 'HATHAZARI', NULL, NULL, '021537', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(510, 9, 'KOTWALI', NULL, NULL, '021541', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'T', 'W'),
(511, 9, 'KHULSHI', NULL, NULL, '021543', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'T', 'W'),
(512, 9, 'LOHAGARA', NULL, NULL, '021547', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'U'),
(513, 9, 'MIRSHARAI', NULL, NULL, '021553', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(514, 9, 'PAHARTALI', NULL, NULL, '021555', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'T', 'W'),
(515, 9, 'PANCHLAISH', NULL, NULL, '021557', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'T', 'W'),
(516, 9, 'PATIYA', NULL, NULL, '021561', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(517, 9, 'PATENGA', NULL, NULL, '021565', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'W'),
(518, 9, 'RANGUNIA', NULL, NULL, '021570', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(519, 9, 'RAOZAN', NULL, NULL, '021574', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(520, 9, 'SANDWIP', NULL, NULL, '021578', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(521, 9, 'SATKANIA', NULL, NULL, '021582', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(522, 9, 'SITAKUNDA', NULL, NULL, '021586', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(523, 11, 'BARURA', NULL, NULL, '021909', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(524, 11, 'BRAHMAN PARA', NULL, NULL, '021915', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'U'),
(525, 11, 'BURICHANG', NULL, NULL, '021918', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'U'),
(526, 11, 'CHANDINA', NULL, NULL, '021927', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(527, 11, 'CHAUDDAGRAM', NULL, NULL, '021931', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(528, 11, 'COMILLA SADAR DAKSHIN', NULL, NULL, '021933', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(529, 11, 'DAUDKANDI', NULL, NULL, '021936', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B');
INSERT INTO `sa_thanas` (`THANA_ID`, `DISTRICT_ID`, `THANA_ENAME`, `THANA_LNAME`, `ORDER_SL`, `UD_THANA_CODE`, `IS_ACTIVE`, `CREATED_BY`, `CREATED_AT`, `UPDATED_AT`, `UPDATED_BY`, `TAREA_STATUS`, `UAREA_STATUS`) VALUES
(530, 11, 'DEBIDWAR', NULL, NULL, '021940', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(531, 11, 'HOMNA', NULL, NULL, '021954', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(532, 11, 'COMILLA ADARSHA SADAR', NULL, NULL, '021967', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(533, 11, 'LAKSAM', NULL, NULL, '021972', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(534, 11, 'MANOHARGANJ', NULL, NULL, '021974', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'U'),
(535, 11, 'MEGHNA', NULL, NULL, '021975', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'U'),
(536, 11, 'MURADNAGAR', NULL, NULL, '021981', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'U'),
(537, 11, 'NANGALKOT', NULL, NULL, '021987', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(538, 11, 'TITAS', NULL, NULL, '021994', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'U'),
(539, 12, 'CHAKARIA', NULL, NULL, '022216', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(540, 12, 'COX''S BAZAR SADAR', NULL, NULL, '022224', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'B'),
(541, 12, 'KUTUBDIA', NULL, NULL, '022245', 1, 0, '2016-10-15 05:27:05', NULL, NULL, 'U', 'U'),
(542, 1, 'Chalna Ankorage', NULL, NULL, NULL, 1, 0, '2016-10-15 05:27:05', NULL, NULL, NULL, NULL),
(543, 1, 'Rayenda', NULL, NULL, NULL, 1, 0, '2016-10-15 05:27:05', NULL, NULL, NULL, NULL),
(544, 10, 'Doulatganj', NULL, NULL, NULL, 1, 0, '2016-10-15 05:27:05', NULL, NULL, NULL, NULL),
(545, 23, 'JESSORE SADAR', NULL, NULL, NULL, 1, 0, '2016-10-15 05:27:05', NULL, NULL, NULL, NULL),
(546, 23, 'NOAPARA', NULL, NULL, NULL, 1, 0, '2016-10-15 05:27:05', NULL, NULL, NULL, NULL),
(547, 25, 'NALDANGA', NULL, NULL, NULL, 1, 0, '2016-10-15 05:27:05', NULL, NULL, NULL, NULL),
(548, 27, 'ALAIPUR', NULL, NULL, NULL, 1, 0, '2016-10-15 05:27:05', NULL, NULL, NULL, NULL),
(549, 27, 'CHALNA BAZAR', NULL, NULL, NULL, 1, 0, '2016-10-15 05:27:05', NULL, NULL, NULL, NULL),
(550, 27, 'MADINABAD', NULL, NULL, NULL, 1, 0, '2016-10-15 05:27:05', NULL, NULL, NULL, NULL),
(551, 27, 'SAJIARA', NULL, NULL, NULL, 1, 0, '2016-10-15 05:27:05', NULL, NULL, NULL, NULL),
(552, 30, 'JANIPUR', NULL, NULL, NULL, 1, 0, '2016-10-15 05:27:05', NULL, NULL, NULL, NULL),
(553, 30, 'Rafayetpur', NULL, NULL, NULL, 1, 0, '2016-10-15 05:27:05', NULL, NULL, NULL, NULL),
(554, 34, 'ARPARA', NULL, NULL, NULL, 1, 0, '2016-10-15 05:27:05', NULL, NULL, NULL, NULL),
(555, 41, 'Laxmipasha', NULL, NULL, NULL, 1, 0, '2016-10-15 05:27:05', NULL, NULL, NULL, NULL),
(556, 41, 'Mohajan', NULL, NULL, NULL, 1, 0, '2016-10-15 05:27:05', NULL, NULL, NULL, NULL),
(557, 58, 'Nakipur', NULL, NULL, NULL, 1, 0, '2016-10-15 05:27:05', NULL, NULL, NULL, NULL),
(558, 13, 'Joypara', NULL, NULL, NULL, 1, 0, '2016-10-15 05:27:05', NULL, NULL, NULL, NULL),
(559, 15, 'Shriangan', NULL, NULL, NULL, 1, 0, '2016-10-15 05:27:05', NULL, NULL, NULL, NULL),
(560, 18, 'Monnunagar', NULL, NULL, NULL, 1, 0, '2016-10-15 05:27:05', NULL, NULL, NULL, NULL),
(561, 33, 'Barhamganj', NULL, NULL, NULL, 1, 0, '2016-10-15 05:27:05', NULL, NULL, NULL, NULL),
(563, 35, 'Lechhraganj', NULL, NULL, NULL, 1, 0, '2016-10-15 05:27:05', NULL, NULL, NULL, NULL),
(564, 42, 'Baidder Bazar', NULL, NULL, NULL, 1, 0, '2016-10-15 05:27:05', NULL, NULL, NULL, NULL),
(565, 42, 'Fatullah', NULL, NULL, NULL, 1, 0, '2016-10-15 05:27:05', NULL, NULL, NULL, NULL),
(566, 42, 'Siddirganj', NULL, NULL, NULL, 1, 0, '2016-10-15 05:27:05', NULL, NULL, NULL, NULL),
(567, 60, 'Bakshigonj', NULL, NULL, NULL, 1, 0, '2016-10-15 05:27:05', NULL, NULL, NULL, NULL),
(568, 63, 'Kashkawlia', NULL, NULL, NULL, 1, 0, '2016-10-15 05:27:05', NULL, NULL, NULL, NULL),
(569, 8, 'Matlobganj', NULL, NULL, NULL, 1, 0, '2016-10-15 05:27:05', NULL, NULL, NULL, NULL),
(570, 14, 'Bangla Hili', NULL, NULL, NULL, 1, 0, '2016-10-15 05:27:05', NULL, NULL, NULL, NULL),
(571, 14, 'Maharajganj', NULL, NULL, NULL, 1, 0, '2016-10-15 05:27:05', NULL, NULL, NULL, NULL),
(572, 14, 'Osmanpur', NULL, NULL, NULL, 1, 0, '2016-10-15 05:27:05', NULL, NULL, NULL, NULL),
(573, 14, 'Setabganj', NULL, NULL, NULL, 1, 0, '2016-10-15 05:27:05', NULL, NULL, NULL, NULL),
(575, 17, 'Bonarpara', NULL, NULL, NULL, 1, 0, '2016-10-15 05:27:05', NULL, NULL, NULL, NULL),
(576, 29, 'Rajibpur', NULL, NULL, NULL, 1, 0, '2016-10-15 05:27:05', NULL, NULL, NULL, NULL),
(577, 32, 'Tushbhandar', NULL, NULL, NULL, 1, 0, '2016-10-15 05:27:05', NULL, NULL, NULL, NULL),
(578, 50, 'Chotto Dab', NULL, NULL, NULL, 1, 0, '2016-10-15 05:27:05', NULL, NULL, NULL, NULL),
(579, 53, 'Chapinawabganj', NULL, NULL, NULL, 1, 0, '2016-10-15 05:27:05', NULL, NULL, NULL, NULL),
(580, 86, 'Chapinawabganj Sadar', NULL, NULL, NULL, 1, 0, '2016-10-15 05:27:05', NULL, NULL, NULL, NULL),
(581, 86, 'Nachol', NULL, NULL, NULL, 1, 0, '2016-10-15 05:27:05', NULL, NULL, NULL, NULL),
(582, 86, 'Rohanpur', NULL, NULL, NULL, 1, 0, '2016-10-15 05:27:05', NULL, NULL, NULL, NULL),
(583, 86, 'Shibganj U.P.O', NULL, NULL, NULL, 1, 0, '2016-10-15 05:27:05', NULL, NULL, NULL, NULL),
(584, 44, 'Gopalpur UPO', NULL, NULL, NULL, 1, 0, '2016-10-15 05:27:05', NULL, NULL, NULL, NULL),
(585, 44, 'Harua', NULL, NULL, NULL, 1, 0, '2016-10-15 05:27:05', NULL, NULL, NULL, NULL),
(586, 53, 'Rajshahi Sadar', NULL, NULL, NULL, 1, 0, '2016-10-15 05:27:05', NULL, NULL, NULL, NULL),
(587, 59, 'Dhangora', NULL, NULL, NULL, 1, 0, '2016-10-15 05:27:05', NULL, NULL, NULL, NULL),
(588, 52, 'Swarupkathi', NULL, NULL, NULL, 1, 0, '2016-10-15 05:27:05', NULL, NULL, NULL, NULL),
(589, 52, 'Banaripara', NULL, NULL, NULL, 1, 0, '2016-10-15 05:27:05', NULL, NULL, NULL, NULL),
(590, 51, 'Subidkhali', NULL, NULL, NULL, 1, 0, '2016-10-15 05:27:05', NULL, NULL, NULL, NULL),
(591, 51, 'Khepupara', NULL, NULL, NULL, 1, 0, '2016-10-15 05:27:05', NULL, NULL, NULL, NULL),
(592, 5, 'Hatshoshiganj', NULL, NULL, NULL, 1, 0, '2016-10-15 05:27:05', NULL, NULL, NULL, NULL),
(593, 5, 'Hajirhat', NULL, NULL, NULL, 1, 0, '2016-10-15 05:27:05', NULL, NULL, NULL, NULL),
(594, 4, 'SAHEBGANJ', NULL, NULL, NULL, 1, 0, '2016-10-15 05:27:05', NULL, NULL, NULL, NULL),
(595, 4, 'Barajalia', NULL, NULL, NULL, 1, 0, '2016-10-15 05:27:05', NULL, NULL, NULL, NULL),
(596, 62, 'Jakiganj', NULL, NULL, NULL, 1, 0, '2016-10-15 05:27:05', NULL, NULL, NULL, NULL),
(597, 61, 'Dhirai Chandpur', NULL, NULL, NULL, 1, 0, '2016-10-15 05:27:05', NULL, NULL, NULL, NULL),
(598, 55, 'Marishya', NULL, NULL, NULL, 1, 0, '2016-10-15 05:27:05', NULL, NULL, NULL, NULL),
(599, 55, 'Longachh', NULL, NULL, NULL, 1, 0, '2016-10-15 05:27:05', NULL, NULL, NULL, NULL),
(600, 55, 'Kalampati', NULL, NULL, NULL, 1, 0, '2016-10-15 05:27:05', NULL, NULL, NULL, NULL),
(601, 48, 'Basurhat', NULL, NULL, NULL, 1, 0, '2016-10-15 05:27:05', NULL, NULL, NULL, NULL),
(602, 31, 'Char Alexgander', NULL, NULL, NULL, 1, 0, '2016-10-15 05:27:05', NULL, NULL, NULL, NULL),
(603, 31, 'Ramghar', NULL, NULL, NULL, 1, 0, '2016-10-15 05:27:05', NULL, NULL, NULL, NULL),
(604, 12, 'Gorakghat', NULL, NULL, NULL, 1, 0, '2016-10-15 05:27:05', NULL, NULL, NULL, NULL),
(605, 12, 'Chiringga', NULL, NULL, NULL, 1, 0, '2016-10-15 05:27:05', NULL, NULL, NULL, NULL),
(606, 9, 'Jaldi', NULL, NULL, NULL, 1, 0, '2016-10-15 05:27:05', NULL, NULL, NULL, NULL),
(607, 9, 'East Joara', NULL, NULL, NULL, 1, 0, '2016-10-15 05:27:05', NULL, NULL, NULL, NULL),
(608, 9, 'Chittagong Sadar', NULL, NULL, NULL, 1, 0, '2016-10-15 05:27:05', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sa_type`
--

CREATE TABLE IF NOT EXISTS `sa_type` (
  `PR_TYPE` tinyint(2) NOT NULL AUTO_INCREMENT,
  `TYPE_NAME` varchar(100) CHARACTER SET utf8 NOT NULL COMMENT 'Flat = Type A, Space = Unit A, Shop = 1 etc',
  `TYPE_DESC` varchar(100) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Type Description if have any',
  `IS_ACTIVE` tinyint(1) NOT NULL COMMENT '0=no, 1=yes',
  `CREATED_AT` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `CREATED_BY` int(10) NOT NULL,
  `UPDATED_AT` datetime DEFAULT NULL,
  `UPDATED_BY` int(10) DEFAULT NULL,
  PRIMARY KEY (`PR_TYPE`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=13 ;

--
-- Dumping data for table `sa_type`
--

INSERT INTO `sa_type` (`PR_TYPE`, `TYPE_NAME`, `TYPE_DESC`, `IS_ACTIVE`, `CREATED_AT`, `CREATED_BY`, `UPDATED_AT`, `UPDATED_BY`) VALUES
(10, 'টাইপ-A', 'টাইপ-A,১৩৬২ বর্গফুট,৬৮,১০,০০০/-', 1, '2016-07-17 02:34:30', 5, '2016-07-17 08:34:30', NULL),
(11, 'টাইপ-B', 'টাইপ-B,১২৯৪ বর্গফুট,৫১,৭৬,০০০/-', 1, '2016-07-17 02:38:15', 5, '2016-07-20 06:40:10', 1),
(12, 'টাইপ-C', '', 1, '2016-11-23 02:44:42', 5, '2016-11-28 06:51:58', 5);

-- --------------------------------------------------------

--
-- Table structure for table `sa_users`
--

CREATE TABLE IF NOT EXISTS `sa_users` (
  `USER_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `USER_TYPE` char(1) NOT NULL COMMENT 'E.G. N = Internal User, C = Citizen',
  `FIRST_NAME` varchar(100) CHARACTER SET utf8 NOT NULL COMMENT 'First Name',
  `LAST_NAME` varchar(100) CHARACTER SET utf8 NOT NULL COMMENT 'Last Name',
  `FULL_NAME` varchar(100) CHARACTER SET utf8 NOT NULL COMMENT 'Full Name',
  `FULL_NAME_BN` varchar(500) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Full Name in Bangla',
  `GENDER` char(1) NOT NULL COMMENT 'Gender of User',
  `USERNAME` varchar(60) CHARACTER SET utf8 NOT NULL COMMENT 'Username for Login',
  `EMAIL` varchar(100) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Email ID for Login',
  `PASSWORD` varchar(60) CHARACTER SET utf8 NOT NULL COMMENT 'Password for Login',
  `MOBILE_NO` varchar(15) CHARACTER SET utf8 NOT NULL COMMENT 'User mobile no for Login or notification purpose',
  `IS_ACTIVE` tinyint(1) NOT NULL DEFAULT '0',
  `REMEMBER_TOKEN` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `RESET_TOKEN` varchar(60) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Password reset token',
  `CREATED_AT` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `CREATED_BY` int(10) NOT NULL,
  `UPDATED_AT` datetime DEFAULT NULL,
  `UPDATED_BY` int(10) DEFAULT NULL,
  PRIMARY KEY (`USER_ID`),
  UNIQUE KEY `SA_USERS_UESRNAME_UNIQUE` (`USERNAME`),
  UNIQUE KEY `SA_USERS_MOBILE_NO_UNIQUE` (`MOBILE_NO`),
  UNIQUE KEY `SA_USERS_EMAIL_UNIQUE` (`EMAIL`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=20 ;

--
-- Dumping data for table `sa_users`
--

INSERT INTO `sa_users` (`USER_ID`, `USER_TYPE`, `FIRST_NAME`, `LAST_NAME`, `FULL_NAME`, `FULL_NAME_BN`, `GENDER`, `USERNAME`, `EMAIL`, `PASSWORD`, `MOBILE_NO`, `IS_ACTIVE`, `REMEMBER_TOKEN`, `RESET_TOKEN`, `CREATED_AT`, `CREATED_BY`, `UPDATED_AT`, `UPDATED_BY`) VALUES
(1, 'N', 'NCC', 'Admin', 'NCC Admin', NULL, 'm', 'admin', 'admin@ncc.gov.bd', '$2y$10$Pg5s/R26TcXw6O6M4yH1euQQPFNu7.SuvQYAt9TYGV3QJCkprYS5K', '01754874542', 1, 'EJYwznd8vtZqqSO7Yt0YrRGV5nk0csWsyFemWokcLrnwQ9pHWUjQKLkkCTRf', NULL, '2016-07-12 06:43:00', 1, '2018-08-12 06:05:00', NULL),
(5, 'N', 'Project', 'Manager', 'Project Manager', NULL, 'm', 'project.manager', 'project.manager@ncc.gov.bd', '$2y$10$6ne.6fhRnHwomX6uY5GvqOwE.Ixmz9zk1nq6bguFeSuGThD1lS7g2', '01754871548', 1, 'YtNN2P36uinfbwCj3hDrvFz6ubnrwV7LxsYgL7VMt7sgLQOiel19ZYZ6y6gA', NULL, '2016-07-16 03:21:22', 1, '2016-12-28 05:26:48', NULL),
(6, 'N', 'Lease', 'Manager', 'Lease Manager', NULL, 'm', 'lease.manager', 'lease.manager@ncc.gov.bd', '$2y$10$cOwLUwpULNMK2tOwrrKUROi7ASauB0DG.G/SbkNkctIfFZ3gQcahq', '01754879456', 1, 'rc5PEdQjDJ7KNpouIIeOblNgig5aGMo7ihofUSlv77ta9H2MqxgtswEmxPfR', NULL, '2016-07-16 03:22:56', 1, '2017-01-25 05:39:50', NULL),
(7, 'N', 'Tender', 'Manager', 'Tender Manager', NULL, 'm', 'tender.manager', 'tender.manager@gmail.com', '$2y$10$nyvSfkC8xpeYZNH38bbIce6METlyFKozmFkUERx0qjNNWUs.fx/C2', '01754124576', 1, 'BXv3ak5fOHe4HINZRSri9l1bnQhL6wzCUHqyCcGeXaXZSAozQBV3NP6SZxqu', NULL, '2016-08-08 00:37:13', 1, '2017-01-25 10:45:04', NULL),
(9, 'C', 'NCC', 'Citizen', 'NCC Citizen', NULL, 'm', 'ncc.citizen', 'ncc.citizen@gmail.com', '$2y$10$F0lhwsU6VLcv4N98tuNN4ucMatOW.TJuA.OvFavCdqkYiiOgaQoAS', '01725487415', 1, 'TMnYCXxTzIaPj0zSSllX7pTb7xyLXOn6BMPf0O4OpaBzmsOpEXNLRw8vGcpN', NULL, '2016-08-08 00:43:32', 1, '2016-11-02 10:10:49', NULL),
(10, 'N', 'Account', 'Manager', 'Account Manager', NULL, 'm', 'account.manager', 'account.manager@ncc.gov.bd', '$2y$10$MO6QmTlaInRgxwMw2KaFQ.sMzWHi3QPopY.OQ9XC49lFHBYoUgL9.', '01478451548', 1, 'e5BuhTQLvUsevus5AeI7IVj4iRrVjFMktmxLBFuzD8SHULUmiyuur6O7Ok4Z', NULL, '2016-08-08 02:52:40', 1, '2017-02-27 08:45:21', NULL),
(18, 'C', 'Abdul', 'Awal', 'Abdul Awal', NULL, 'm', 'awal.ashu', 'awal.ashu@gmail.com', '$2y$10$Nns8lXy9EqJTT/mWKhjah.cCuCr9B9.loyZOX6a03FBFBp2jnzOU6', '01738031018', 1, '8BYYSK8NUfU7PD4ovY5eldFovRDcjTd80bKLhU2mMiCpmtY3E1cSLW1wGp06', NULL, '2016-11-13 06:48:56', 0, '2017-04-15 01:54:11', NULL),
(19, 'C', 'মোঃ', 'নুরুল্লাহ', 'মোঃ নুরুল্লাহ', NULL, 'm', 'mail', 'mail@mail.com', '$2y$10$90V1vew9L/AhE8QZGtuBMO0hmLATQn5m6fzR11.OD3burd1dT6MBe', '01927680643', 0, '0TpBRKGbkrF25mylDm3kv5op3yHXYYCXDB4U5Xjahp48kMxuzOzDC01AFY6S', NULL, '2016-11-14 07:42:31', 0, '2016-12-08 08:26:12', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sa_user_details`
--

CREATE TABLE IF NOT EXISTS `sa_user_details` (
  `UD_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `USER_ID` bigint(20) NOT NULL,
  `DESIGNATION` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `ADDRESS` text CHARACTER SET utf8,
  `IMAGE` varchar(150) DEFAULT NULL,
  `CONTENT_TYPE` varchar(10) DEFAULT NULL,
  `CREATED_AT` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `UPDATED_AT` datetime DEFAULT NULL,
  PRIMARY KEY (`UD_ID`),
  KEY `USER_ID` (`USER_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `sa_user_details`
--

INSERT INTO `sa_user_details` (`UD_ID`, `USER_ID`, `DESIGNATION`, `ADDRESS`, `IMAGE`, `CONTENT_TYPE`, `CREATED_AT`, `UPDATED_AT`) VALUES
(1, 1, 'Admin', 'Dhaka, Bangladesh', 'user-588dc55e8566b', 'jpg', '2017-01-29 10:35:16', '2017-01-29 10:35:10'),
(3, 9, '', '', NULL, NULL, '2016-08-09 02:05:26', '2016-08-09 08:05:26'),
(7, 18, '', '', 'user-588c3740982c5', 'jpg', '2017-03-12 06:36:47', '2017-03-12 06:36:43');

-- --------------------------------------------------------

--
-- Table structure for table `sa_ward`
--

CREATE TABLE IF NOT EXISTS `sa_ward` (
  `WARD_ID` int(10) NOT NULL AUTO_INCREMENT,
  `PARENT_ID` int(10) NOT NULL DEFAULT '0',
  `WARD_NAME` varchar(150) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Name of the Ward',
  `WARD_NAME_BN` varchar(150) CHARACTER SET utf8 NOT NULL COMMENT 'Name of the Ward in Bangla',
  `IS_ACTIVE` tinyint(1) NOT NULL COMMENT '0=NO, 1=YES',
  `CREATED_AT` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `CREATED_BY` int(10) NOT NULL,
  `UPDATED_AT` datetime DEFAULT NULL,
  `UPDATED_BY` int(10) DEFAULT NULL,
  PRIMARY KEY (`WARD_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=25 ;

--
-- Dumping data for table `sa_ward`
--

INSERT INTO `sa_ward` (`WARD_ID`, `PARENT_ID`, `WARD_NAME`, `WARD_NAME_BN`, `IS_ACTIVE`, `CREATED_AT`, `CREATED_BY`, `UPDATED_AT`, `UPDATED_BY`) VALUES
(13, 0, 'Zone-1', 'জোন- ১', 1, '2016-10-03 04:58:10', 1, '2016-10-03 04:58:10', 1),
(14, 0, 'Zone-2', 'জোন- ২', 1, '2016-07-19 03:21:44', 1, '2016-07-19 09:21:44', NULL),
(15, 0, 'Zone-3', 'জোন- ৩', 1, '2016-10-03 06:20:02', 1, '2016-07-19 09:25:00', NULL),
(16, 13, 'Ward 01', 'ওয়ার্ড ০১', 1, '2016-07-19 03:28:48', 1, '2016-07-19 09:28:48', NULL),
(17, 13, 'Ward 02', 'ওয়ার্ড ০২', 1, '2016-07-19 03:29:58', 1, '2016-07-19 09:29:58', NULL),
(18, 13, 'Ward 03', 'ওয়ার্ড ০৩', 1, '2016-10-13 04:47:46', 1, '2016-10-13 04:47:47', 1),
(19, 13, 'Ward 04', 'ওয়ার্ড ০৪', 1, '2016-07-19 03:32:07', 1, '2016-07-19 09:32:07', NULL),
(21, 0, 'Zone 4', 'জোন- ৪', 1, '2016-10-03 04:58:40', 1, '2016-10-03 04:58:40', 1),
(22, 0, 'Zone 5', 'অঞ্চলের নাম (বাংলা)', 0, '2016-10-03 04:58:16', 1, '2016-10-03 04:58:16', 1),
(23, 22, 'Ward 5', 'অঞ্চলের নাম বাংলা', 0, '2016-10-13 04:46:41', 1, '2016-10-13 04:46:41', 1),
(24, 0, 'Zone Name', 'অঞ্চলের নাম', 0, '2016-10-13 00:03:47', 1, '2016-10-13 06:03:47', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `security_refund`
--

CREATE TABLE IF NOT EXISTS `security_refund` (
  `SF_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `TE_APP_ID` bigint(20) DEFAULT NULL,
  `CITIZEN_ID` bigint(20) DEFAULT NULL,
  `TRX_TRAN_NO` bigint(20) DEFAULT NULL,
  `TENDER_ID` bigint(20) DEFAULT NULL,
  `SCHEDULE_ID` bigint(20) DEFAULT NULL,
  `PROJECT_ID` bigint(20) DEFAULT NULL,
  `PR_DETAIL_ID` bigint(20) DEFAULT NULL,
  `PR_CATEGORY` tinyint(2) DEFAULT NULL,
  `PR_TYPE` tinyint(2) DEFAULT NULL,
  `IS_LEASE` tinyint(1) NOT NULL,
  `AMOUNT` double DEFAULT NULL,
  `REMARKS` text CHARACTER SET utf8,
  `IS_PAID` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0=no, 1=yes',
  `IS_ACTIVE` tinyint(1) NOT NULL DEFAULT '1',
  `CREATED_AT` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `CREATED_BY` bigint(20) DEFAULT NULL,
  `UPDATED_AT` datetime DEFAULT NULL,
  `UPDATED_BY` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`SF_ID`),
  KEY `TE_APP_ID` (`TE_APP_ID`),
  KEY `CITIZEN_ID` (`CITIZEN_ID`),
  KEY `TRX_TRAN_NO` (`TRX_TRAN_NO`),
  KEY `TENDER_ID` (`TENDER_ID`),
  KEY `SCHEDULE_ID` (`SCHEDULE_ID`),
  KEY `PROJECT_ID` (`PROJECT_ID`),
  KEY `PR_DETAIL_ID` (`PR_DETAIL_ID`),
  KEY `PR_CATEGORY` (`PR_CATEGORY`),
  KEY `PR_TYPE` (`PR_TYPE`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `security_refund`
--

INSERT INTO `security_refund` (`SF_ID`, `TE_APP_ID`, `CITIZEN_ID`, `TRX_TRAN_NO`, `TENDER_ID`, `SCHEDULE_ID`, `PROJECT_ID`, `PR_DETAIL_ID`, `PR_CATEGORY`, `PR_TYPE`, `IS_LEASE`, `AMOUNT`, `REMARKS`, `IS_PAID`, `IS_ACTIVE`, `CREATED_AT`, `CREATED_BY`, `UPDATED_AT`, `UPDATED_BY`) VALUES
(1, 264, NULL, 47, 48, 124, 66, NULL, 24, NULL, 0, 3000, NULL, 0, 1, '2017-04-11 14:14:20', 1, '2017-04-11 20:14:20', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tender`
--

CREATE TABLE IF NOT EXISTS `tender` (
  `TENDER_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `TENDER_NO` text CHARACTER SET utf8 NOT NULL COMMENT 'User Defined Tender No',
  `PRE_TENDER_ID` bigint(20) DEFAULT NULL COMMENT 'Reference Tender Id in case of Re tender',
  `TENDER_METHOD` tinyint(1) NOT NULL COMMENT 'e.g. Open Tender Method(OTM)',
  `TENDER_TITLE` text CHARACTER SET utf8 COMMENT 'Location of the schedule to sell',
  `TENDER_DESC` text CHARACTER SET utf8 NOT NULL COMMENT 'Description of the Project',
  `TENDER_DT` datetime DEFAULT NULL COMMENT 'Tender Date',
  `TENDER_PUBLISH_DT` datetime DEFAULT NULL COMMENT 'Tender Publish Date',
  `IS_LEASE` tinyint(1) NOT NULL COMMENT '0=no, 1=yes',
  `IS_ACTIVE` tinyint(1) NOT NULL COMMENT '0=no, 1=yes',
  `CREATED_AT` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `CREATED_BY` int(10) NOT NULL,
  `UPDATED_AT` datetime DEFAULT NULL,
  `UPDATED_BY` int(10) DEFAULT NULL,
  PRIMARY KEY (`TENDER_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=59 ;

--
-- Dumping data for table `tender`
--

INSERT INTO `tender` (`TENDER_ID`, `TENDER_NO`, `PRE_TENDER_ID`, `TENDER_METHOD`, `TENDER_TITLE`, `TENDER_DESC`, `TENDER_DT`, `TENDER_PUBLISH_DT`, `IS_LEASE`, `IS_ACTIVE`, `CREATED_AT`, `CREATED_BY`, `UPDATED_AT`, `UPDATED_BY`) VALUES
(48, '৪৬.৪৪৬৭০০.১৪.১৮(৮).১৬', NULL, 1, 'হাট-বাজার/সায়রাত মহালের ইজারা বিজ্ঞপ্তি', '', '2016-11-13 00:00:00', '2016-11-13 00:00:00', 1, 1, '2016-11-12 21:56:40', 1, '2016-11-13 03:56:40', NULL),
(49, '৪৬.৪৪৬.১৪.১৮(৮).১৬', NULL, 1, 'দোকান/স্পেস/বরাদ্দের দরপত্র বিজ্ঞপ্তি', '', '2016-11-13 00:00:00', '2016-11-13 00:00:00', 0, 1, '2016-11-12 22:00:16', 1, '2016-11-22 04:56:12', 1),
(50, '৮৭৮৪৮৫৭৪৬৫', NULL, 1, 'দোকান/ফ্ল্যাট বরাদ্দের দরপত্র', '', '2016-11-20 00:00:00', '2016-11-21 00:00:00', 0, 1, '2016-11-20 06:27:31', 1, '2016-11-22 10:45:28', 1),
(53, '৪৫৩৬৭৩৪৩২', NULL, 1, 'অস্থায়ী পুশুর হাট', '', '2016-11-21 00:00:00', '2016-11-21 00:00:00', 1, 1, '2016-11-21 03:41:30', 1, '2016-12-07 03:51:33', 1),
(54, '৪৬.৪৪৬৭০০.১৪.১৮৫৪৫৩', NULL, 1, 'দোকান/স্পেস বরাদ্দের দরপত্র', '', '2017-01-10 00:00:00', '2017-01-10 00:00:00', 0, 1, '2016-11-23 04:11:35', 7, '2017-01-10 03:39:49', 1),
(55, 'df', NULL, 1, 'fds', '', '2016-12-07 00:00:00', '2016-12-07 00:00:00', 1, 1, '2016-12-07 00:19:52', 1, '2016-12-08 09:06:44', NULL),
(56, '3545345', NULL, 1, 'iueyr78eufrgw', '', '2016-12-26 00:00:00', '2016-12-26 00:00:00', 0, 1, '2016-12-26 06:26:25', 1, '2016-12-26 12:26:25', NULL),
(57, '555', NULL, 1, '4523', '', '2017-04-23 00:00:00', '2017-04-23 00:00:00', 0, 1, '2017-04-23 02:11:49', 1, '2017-04-23 08:11:49', NULL),
(58, '৪৬.১৬.৬৭০০.০১৪.২১.০০১.১৬.২২০', NULL, 1, 'দুকান', '', '2017-04-25 00:00:00', '2017-04-24 00:00:00', 0, 1, '2017-04-24 06:10:25', 1, '2017-04-24 12:10:25', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tender_applicant`
--

CREATE TABLE IF NOT EXISTS `tender_applicant` (
  `TE_APP_ID` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  `FIRST_NAME` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Tender applicant first name',
  `LAST_NAME` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Tender applicant last name',
  `APPLICANT_NAME` varchar(100) CHARACTER SET utf8 NOT NULL COMMENT 'Name of the Applicant',
  `APPLICANT_PHONE` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Applicant Phone Number',
  `NID` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Applicant national ID',
  `PASSWORD` varchar(60) CHARACTER SET utf8 NOT NULL,
  `SPOUSE_NAME` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `EMAIL` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `REMEMBER_TOKEN` varchar(100) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Use as remember user credentials',
  `RESET_TOKEN` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Password reset token',
  `DIVISION_ID` tinyint(2) NOT NULL COMMENT 'Primary key of sa_divisions table',
  `DISTRICT_ID` int(8) NOT NULL COMMENT 'Primary Key of district Table',
  `THANA_ID` smallint(8) NOT NULL COMMENT 'Primary Key of thana Table',
  `PO_ID` smallint(6) NOT NULL COMMENT 'Primary Key of post_office Table',
  `ROAD_NO` varchar(100) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Road Number of Applicant',
  `HOLDING_NO` varchar(100) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Holding Number of Applicant',
  `IS_ACTIVE` tinyint(1) NOT NULL COMMENT '0=no, 1=yes',
  `CREATED_AT` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `CREATED_BY` bigint(20) DEFAULT NULL,
  `UPDATED_AT` datetime DEFAULT NULL,
  `UPDATED_BY` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`TE_APP_ID`),
  UNIQUE KEY `APPLICANT_PHONE_UNIQUE` (`APPLICANT_PHONE`),
  KEY `DISTRICT_ID` (`DISTRICT_ID`),
  KEY `THANA_ID` (`THANA_ID`),
  KEY `DIVISION_ID` (`DIVISION_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=280 ;

--
-- Dumping data for table `tender_applicant`
--

INSERT INTO `tender_applicant` (`TE_APP_ID`, `FIRST_NAME`, `LAST_NAME`, `APPLICANT_NAME`, `APPLICANT_PHONE`, `NID`, `PASSWORD`, `SPOUSE_NAME`, `EMAIL`, `REMEMBER_TOKEN`, `RESET_TOKEN`, `DIVISION_ID`, `DISTRICT_ID`, `THANA_ID`, `PO_ID`, `ROAD_NO`, `HOLDING_NO`, `IS_ACTIVE`, `CREATED_AT`, `CREATED_BY`, `UPDATED_AT`, `UPDATED_BY`) VALUES
(263, 'Abdul', 'Awal', 'Abdul Awal', '01738034518', '1245764215487', '$2y$10$cRFpgi3BhV9ankaSSw5cVuYW4X05ipaA6zCk4xdm.i1sy9N5D16uG', 'Maynuddin', 'awal.ashu@gmail.com', NULL, NULL, 5, 6, 251, 0, '15', '52', 0, '2016-11-12 22:03:56', NULL, '2016-11-13 12:48:56', NULL),
(264, 'মোঃ', 'নুরুল্লাহ', 'মোঃ নুরুল্লাহ', '01927680643', '১২৩৪৫৬৭৮৯৯৫১২৫৮৯৩২৪', '$2y$10$90V1vew9L/AhE8QZGtuBMO0hmLATQn5m6fzR11.OD3burd1dT6MBe', 'মোঃ আঃ হাকিম', 'mail@mail.com', NULL, NULL, 3, 28, 113, 0, 'নাই', 'পাটুঃ-৫৮৯', 0, '2016-11-13 06:29:11', NULL, '2016-11-14 13:42:31', NULL),
(265, 'Abdul', 'Awal', 'Abdul Awal', '01966181000', '0124575142546', '$2y$10$.MhIEr1ZfOS/Ttuq./JbMuoDVLffbIRHtMCZk6yC.wgerfvpCFeMK', 'Maynuddin', 'abdulawal@atilimited.net', NULL, NULL, 3, 46, 152, 0, '12', '221', 1, '2016-11-23 02:03:40', NULL, '2017-01-24 11:30:09', NULL),
(266, 'মোঃ আব্দুল', 'নুরুল্লাহ', 'মোঃ আব্দুল নুরুল্লাহ', '01624426363', '1989159685423', '$2y$10$cJLsjk.TuWNEtNL9geQfj.4fOEV9WX5EvelkLz3X5LTmR72/8OlCC', 'মোঃ আঃ রহিম', 'maild@mail.com', NULL, NULL, 2, 2, 476, 0, 'f', 'df', 1, '2016-12-07 00:22:03', NULL, '2016-12-07 06:22:03', NULL),
(267, 'মোঃ', 'নুরুল্লাহ', 'মোঃ নুরুল্লাহ', '', '12593587463254', '$2y$10$489OEhRq0Am2NUWZSyk1.OUtgDmc1.74aKUnvUjSRHk1Qgvd64A7S', 'মোঃ আঃ রহিম', 'mail5@mail.com', NULL, NULL, 1, 5, 454, 0, 'f', 'পাটুঃ-৫৮৯', 1, '2016-12-07 00:26:03', NULL, '2016-12-07 06:26:03', NULL),
(268, 'Sharov', 'Khan', 'Sharov Khan', '01731214425', '0124575124562', '$2y$10$EZZk2SGpGAoZ//4HrjItY.XS7cjigBWOExFCivUF3tY2fMBrzCdEm', 'Mr. x', 'sourovh207@gmail.com', NULL, NULL, 2, 7, 484, 0, '12', '34', 1, '2016-12-11 23:51:17', NULL, '2016-12-12 05:51:17', NULL),
(269, 'Mehediv', 'Hasan', 'Mehediv Hasan', '01822184508', '2693624618815', '$2y$10$jgwEkDYM6WhHfnkMaLyUruOhw7OmC9lz.Kv9L5oDAPcmnWH0nqggO', 'Late Abdul Khaleque', 'hasan01914889472@gmail.com', NULL, NULL, 3, 13, 428, 0, '04', '317', 1, '2016-12-26 06:36:16', NULL, '2016-12-26 12:36:16', NULL),
(270, 'Parvez', 'Rana', 'Parvez Rana', '01749808200', '1245794512456', '$2y$10$6wIQLhLlwfZK8JkjfKp/B.ZBOoZQRt6nZml27POg1vnNlSfCNhVh2', 'Salim Uddin', 'abdulawal200@gmail.com', NULL, NULL, 1, 4, 448, 0, '34', '54', 1, '2017-01-23 23:23:09', NULL, '2017-01-24 05:23:09', NULL),
(271, 'Md. ', 'Abdul', 'Md.  Abdul', '01674636168', '5623126547951', '$2y$10$WmQiLL3JUnjqMktPG.u2/Owg6evh7yMfYphDl82qBRqJ7SV11OW8u', 'Karim', 'abdulkarim@atilimited.net', NULL, NULL, 3, 13, 433, 0, 'road:10', 'House: 520', 1, '2017-03-08 00:40:34', NULL, '2017-03-08 06:40:34', NULL),
(274, 'Abdul', 'Karim', 'Abdul Karim', '01920536213', '121212121', '$2y$10$YU157DdspKsHqXGbuDT.oOABf0dUFTWtScYfUaXa6eH35MeTH6kh2', 'asas', 'nurul@atilimited.net', NULL, NULL, 2, 7, 484, 0, '1', '02', 1, '2017-03-12 01:27:29', NULL, '2017-03-12 07:27:29', NULL),
(275, 'sdsd', 'dfdfdf', 'sdsd dfdfdf', '01754215489', '2323', '$2y$10$Nlfh9rQRIr6vRRpCXTfunufr3AdTrQemUTF46M8Rh9Fqj3evcCj.G', 'fghghj', '', NULL, NULL, 8, 47, 318, 0, '34', '465', 1, '2017-03-28 12:14:01', NULL, '2017-03-28 18:14:01', NULL),
(276, 'Test', 'User', 'Test User', '01966180999', '21154', '$2y$10$lPumlQbQ4LaB19Q.QvRGH.4xhI1om8FrwkB1uSHOY1GvykwftNRje', 'Test parent', '', NULL, NULL, 1, 3, 438, 0, '12', '11', 1, '2017-03-29 11:18:40', NULL, '2017-03-29 17:18:40', NULL),
(277, 'Biplob', 'Hossign', 'Biplob Hossign', '01749808288', '6555', '$2y$10$uOD1JgaT4AaJ/KCLu2d/BeM1tZLMz0UHYymdnTcd2hNeiiUJWTO76', 'Abul Kalam', '', NULL, NULL, 3, 39, 136, 0, '3', '34', 1, '2017-04-11 11:52:11', NULL, '2017-04-11 17:52:11', NULL),
(278, 'Rakib', 'Roni', 'Rakib Roni', '01722085398', '2323696', '$2y$10$naSdqAX64ciJj3QS2QSjYeAi5XzN.awZboE0nK0fm7zu1X84OnMBK', 'Mr. x', '', NULL, NULL, 3, 15, 38, 0, '12', '12', 1, '2017-04-15 05:07:10', NULL, '2017-04-15 11:07:10', NULL),
(279, 'MD.', 'Nurullah', 'MD. Nurullah', '01624884262', '1596325874156', '$2y$10$DGAViLM5HLfdPdlSN1vu4eE8Oh/UKY.3OZc8thibqMxTy6J7CfX7O', 'Md. Abdul Hakim', '', NULL, NULL, 3, 28, 113, 0, 'রোড নং', 'হোল্ডিং নং', 1, '2017-04-16 02:47:17', NULL, '2017-04-16 08:47:17', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tender_conditions`
--

CREATE TABLE IF NOT EXISTS `tender_conditions` (
  `TE_CON_ID` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'Primary key',
  `TENDER_ID` bigint(20) NOT NULL COMMENT 'Primary of tender table',
  `PR_CATEGORY` tinyint(2) NOT NULL COMMENT 'Primary key of sa_category table',
  `CON_DESC` text NOT NULL COMMENT 'Tender conditions',
  `IS_ACTIVE` tinyint(1) NOT NULL COMMENT '0=no, 1=yes',
  `CREATED_AT` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `CREATED_BY` bigint(20) NOT NULL,
  `UPDATED_AT` datetime DEFAULT NULL,
  `UPDATED_BY` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`TE_CON_ID`),
  KEY `TENDER_ID` (`TENDER_ID`),
  KEY `PR_CATEGORY` (`PR_CATEGORY`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `tender_conditions`
--

INSERT INTO `tender_conditions` (`TE_CON_ID`, `TENDER_ID`, `PR_CATEGORY`, `CON_DESC`, `IS_ACTIVE`, `CREATED_AT`, `CREATED_BY`, `UPDATED_AT`, `UPDATED_BY`) VALUES
(1, 50, 22, '<p style="box-sizing: border-box; margin: 0px 0px 10px; font-family: bg-font !important; color: rgb(34, 34, 34); font-size: 13px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; letter-spacing: normal; orphans: 2; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; text-align: justify;"><strong>নারায়ণগঞ্জ সিটি করপোরেশন</strong><span class="Apple-converted-space"> </span>নারায়ণগঞ্জ পৌরসভাকে (মিউনিসিপ্যালিটি) ২০১১ সালের ৫ মে নারায়ণগঞ্জ সিটি করপোরেশনে রূপান্তর করা হয়। নারায়ণগঞ্জ শহর, সিদ্ধিরগঞ্জ পৌর এলাকা এবং বন্দর উপজেলার কমদরসূল পৌরসভাকে নিয়ে নারায়ণগঞ্জ সিটি করপোরেশন গঠিত হয়।</p><p style="box-sizing: border-box; margin: 0px 0px 10px; font-family: bg-font !important; color: rgb(34, 34, 34); font-size: 13px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; letter-spacing: normal; orphans: 2; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; text-align: justify;" rel="box-sizing: border-box; margin: 0px 0px 10px; font-family: bg-font !important; color: rgb(34, 34, 34); font-size: 13px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; letter-spacing: normal; orphans: 2; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; text-align: justify;">সিদ্ধিরগঞ্জ পৌরসভাকে বিলুপ্ত করে এর ১ থেকে ৯ নম্বর ওয়ার্ডকে সিটি করপোরেশনের ১ থেকে ৯ নম্বর ওয়ার্ড হিসাবে ঘোষণা করা হয়েছে। সিদ্ধিরগঞ্জের পাইনাদী পূর্ব অংশ মিজমিজি বাতান পাড়াকে ১ নম্বর ওয়ার্ড করা হয়েছে। ১ থেকে ৩ নম্বর ওয়ার্ডকে সংরক্ষিত মহিলা আসন হিসেবে তালিকা ভুক্ত করা হয়েছে। নারায়ণগঞ্জ পৌরসভা বিলুপ্ত করে এর ৯টি ওয়ার্ডকে সিটি করপোরেশনের ১০ থেকে ১৮ নম্বর ওয়ার্ড হিসেবে তালিকাভুক্ত করা হয়েছে। এই পৌরসভার সংরক্ষিত তিনটি মহিলা ওয়ার্ডকে ৪ থেকে ৬ নম্বর ওয়ার্ড হিসাবে ঘোষণা করা হয়। বন্দর উপজেলার কদম রসূল পৌরসভাকে বিলুপ্ত করে এর ৯টি ওয়ার্ডকে সিটি করপোরেশনের ১৯ থেকে ২৭ নম্বর ওয়ার্ড হিসেবে তালিকাভুক্ত করা হয়। এই পৌরসভার তিনাট মহিলা ওয়ার্ডকে সিটি করপোরেশনের ৭ থেকে ৯ নম্বর ওয়ার্ড হিসাবে ঘোষণা করা হয়।</p>', 1, '2016-11-21 06:17:12', 1, '2016-11-21 06:17:10', 1),
(2, 50, 21, '<p>1. সিদ্ধিরগঞ্জ পৌরসভাকে বিলুপ্ত করে এর ১ থেকে ৯ নম্বর ওয়ার্ডকে সিটি করপোরেশনের ১ থেকে ৯ নম্বর ওয়ার্ড হিসাবে ঘোষণা করা হয়েছে। </p><p>2. সিদ্ধিরগঞ্জের পাইনাদী পূর্ব অংশ মিজমিজি বাতান পাড়াকে ১ নম্বর ওয়ার্ড করা হয়েছে। ১ থেকে ৩ নম্বর ওয়ার্ডকে সংরক্ষিত মহিলা আসন </p><p>3.পৌরসভার তিনাট মহিলা ওয়ার্ডকে সিটি করপোরেশনের ৭ থেকে ৯ নম্বর ওয়ার্ড হিসাবে ঘোষণা করা হয়।</p><p>4.তালিকাভুক্ত করা হয়েছে। এই পৌরসভার সংরক্ষিত তিনটি মহিলা ওয়ার্ডকে<strong> ৪ থেকে ৬ নম্বর</strong> ওয়ার্ড হিসাবে ঘোষণা করা হয়। বন্দর উপজেলার<br></p>', 1, '2016-11-21 06:17:12', 1, '2016-11-21 06:17:10', 1),
(3, 53, 24, '<p style="box-sizing: border-box; margin: 0px 0px 10px; font-family: bg-font !important; color: rgb(34, 34, 34); font-size: 13px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; letter-spacing: normal; orphans: 2; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; text-align: justify;"><strong>নারায়ণগঞ্জ সিটি করপোরেশন</strong><span class="Apple-converted-space"> </span>নারায়ণগঞ্জ পৌরসভাকে (মিউনিসিপ্যালিটি) ২০১১ সালের ৫ মে নারায়ণগঞ্জ সিটি করপোরেশনে রূপান্তর করা হয়। নারায়ণগঞ্জ শহর, সিদ্ধিরগঞ্জ পৌর এলাকা এবং বন্দর উপজেলার কমদরসূল পৌরসভাকে নিয়ে নারায়ণগঞ্জ সিটি করপোরেশন গঠিত হয়।</p><p style="box-sizing: border-box; margin: 0px 0px 10px; font-family: bg-font !important; color: rgb(34, 34, 34); font-size: 13px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; letter-spacing: normal; orphans: 2; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; text-align: justify;" rel="box-sizing: border-box; margin: 0px 0px 10px; font-family: bg-font !important; color: rgb(34, 34, 34); font-size: 13px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; letter-spacing: normal; orphans: 2; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; text-align: justify;">সিদ্ধিরগঞ্জ পৌরসভাকে বিলুপ্ত করে এর ১ থেকে ৯ নম্বর ওয়ার্ডকে সিটি করপোরেশনের ১ থেকে ৯ নম্বর ওয়ার্ড হিসাবে ঘোষণা করা হয়েছে। সিদ্ধিরগঞ্জের পাইনাদী পূর্ব অংশ মিজমিজি বাতান পাড়াকে ১ নম্বর ওয়ার্ড করা হয়েছে। ১ থেকে ৩ নম্বর ওয়ার্ডকে সংরক্ষিত মহিলা আসন হিসেবে তালিকা ভুক্ত করা হয়েছে। নারায়ণগঞ্জ পৌরসভা বিলুপ্ত করে এর ৯টি ওয়ার্ডকে সিটি করপোরেশনের ১০ থেকে ১৮ নম্বর ওয়ার্ড হিসেবে তালিকাভুক্ত করা হয়েছে। এই পৌরসভার সংরক্ষিত তিনটি মহিলা ওয়ার্ডকে ৪ থেকে ৬ নম্বর ওয়ার্ড হিসাবে ঘোষণা করা হয়। বন্দর উপজেলার কদম রসূল পৌরসভাকে বিলুপ্ত করে এর ৯টি ওয়ার্ডকে সিটি করপোরেশনের ১৯ থেকে ২৭ নম্বর ওয়ার্ড হিসেবে তালিকাভুক্ত করা হয়। এই পৌরসভার তিনাট মহিলা ওয়ার্ডকে সিটি করপোরেশনের ৭ থেকে ৯ নম্বর ওয়ার্ড হিসাবে ঘোষণা করা হয়।</p><p><br></p><p style="box-sizing: border-box; margin: 0px 0px 10px; font-family: bg-font !important; color: rgb(34, 34, 34); font-size: 13px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; letter-spacing: normal; orphans: 2; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; text-align: justify;"><strong>নারায়ণগঞ্জ সিটি করপোরেশন</strong><span class="Apple-converted-space"> </span>নারায়ণগঞ্জ পৌরসভাকে (মিউনিসিপ্যালিটি) ২০১১ সালের ৫ মে নারায়ণগঞ্জ সিটি করপোরেশনে রূপান্তর করা হয়। নারায়ণগঞ্জ শহর, সিদ্ধিরগঞ্জ পৌর এলাকা এবং বন্দর উপজেলার কমদরসূল পৌরসভাকে নিয়ে নারায়ণগঞ্জ সিটি করপোরেশন গঠিত হয়।</p><p style="box-sizing: border-box; margin: 0px 0px 10px; font-family: bg-font !important; color: rgb(34, 34, 34); font-size: 13px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; letter-spacing: normal; orphans: 2; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; text-align: justify;" rel="box-sizing: border-box; margin: 0px 0px 10px; font-family: bg-font !important; color: rgb(34, 34, 34); font-size: 13px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; letter-spacing: normal; orphans: 2; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; text-align: justify;">সিদ্ধিরগঞ্জ পৌরসভাকে বিলুপ্ত করে এর ১ থেকে ৯ নম্বর ওয়ার্ডকে সিটি করপোরেশনের ১ থেকে ৯ নম্বর ওয়ার্ড হিসাবে ঘোষণা করা হয়েছে। সিদ্ধিরগঞ্জের পাইনাদী পূর্ব অংশ মিজমিজি বাতান পাড়াকে ১ নম্বর ওয়ার্ড করা হয়েছে। ১ থেকে ৩ নম্বর ওয়ার্ডকে সংরক্ষিত মহিলা আসন হিসেবে তালিকা ভুক্ত করা হয়েছে। নারায়ণগঞ্জ পৌরসভা বিলুপ্ত করে এর ৯টি ওয়ার্ডকে সিটি করপোরেশনের ১০ থেকে ১৮ নম্বর ওয়ার্ড হিসেবে তালিকাভুক্ত করা হয়েছে। এই পৌরসভার সংরক্ষিত তিনটি মহিলা ওয়ার্ডকে ৪ থেকে ৬ নম্বর ওয়ার্ড হিসাবে ঘোষণা করা হয়। বন্দর উপজেলার কদম রসূল পৌরসভাকে বিলুপ্ত করে এর ৯টি ওয়ার্ডকে সিটি করপোরেশনের ১৯ থেকে ২৭ নম্বর ওয়ার্ড হিসেবে তালিকাভুক্ত করা হয়। এই পৌরসভার তিনাট মহিলা ওয়ার্ডকে সিটি করপোরেশনের ৭ থেকে ৯ নম্বর ওয়ার্ড হিসাবে ঘোষণা করা হয়।</p><p style="box-sizing: border-box; margin: 0px 0px 10px; font-family: bg-font !important; color: rgb(34, 34, 34); font-size: 13px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; letter-spacing: normal; orphans: 2; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; text-align: justify;"><strong>নারায়ণগঞ্জ সিটি করপোরেশন</strong><span class="Apple-converted-space"> </span>নারায়ণগঞ্জ পৌরসভাকে (মিউনিসিপ্যালিটি) ২০১১ সালের ৫ মে নারায়ণগঞ্জ সিটি করপোরেশনে রূপান্তর করা হয়। নারায়ণগঞ্জ শহর, সিদ্ধিরগঞ্জ পৌর এলাকা এবং বন্দর উপজেলার কমদরসূল পৌরসভাকে নিয়ে নারায়ণগঞ্জ সিটি করপোরেশন গঠিত হয়।</p><p style="box-sizing: border-box; margin: 0px 0px 10px; font-family: bg-font !important; color: rgb(34, 34, 34); font-size: 13px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; letter-spacing: normal; orphans: 2; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; text-align: justify;" rel="box-sizing: border-box; margin: 0px 0px 10px; font-family: bg-font !important; color: rgb(34, 34, 34); font-size: 13px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; letter-spacing: normal; orphans: 2; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; text-align: justify;">সিদ্ধিরগঞ্জ পৌরসভাকে বিলুপ্ত করে এর ১ থেকে ৯ নম্বর ওয়ার্ডকে সিটি করপোরেশনের ১ থেকে ৯ নম্বর ওয়ার্ড হিসাবে ঘোষণা করা হয়েছে। সিদ্ধিরগঞ্জের পাইনাদী পূর্ব অংশ মিজমিজি বাতান পাড়াকে ১ নম্বর ওয়ার্ড করা হয়েছে। ১ থেকে ৩ নম্বর ওয়ার্ডকে সংরক্ষিত মহিলা আসন হিসেবে তালিকা ভুক্ত করা হয়েছে। নারায়ণগঞ্জ পৌরসভা বিলুপ্ত করে এর ৯টি ওয়ার্ডকে সিটি করপোরেশনের ১০ থেকে ১৮ নম্বর ওয়ার্ড হিসেবে তালিকাভুক্ত করা হয়েছে। এই পৌরসভার সংরক্ষিত তিনটি মহিলা ওয়ার্ডকে ৪ থেকে ৬ নম্বর ওয়ার্ড হিসাবে ঘোষণা করা হয়। বন্দর উপজেলার কদম রসূল পৌরসভাকে বিলুপ্ত করে এর ৯টি ওয়ার্ডকে সিটি করপোরেশনের ১৯ থেকে ২৭ নম্বর ওয়ার্ড হিসেবে তালিকাভুক্ত করা হয়। এই পৌরসভার তিনাট মহিলা ওয়ার্ডকে সিটি করপোরেশনের ৭ থেকে ৯ নম্বর ওয়ার্ড হিসাবে ঘোষণা করা হয়।</p><p style="box-sizing: border-box; margin: 0px 0px 10px; font-family: bg-font !important; color: rgb(34, 34, 34); font-size: 13px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; letter-spacing: normal; orphans: 2; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; text-align: justify;"><br></p><p style="box-sizing: border-box; margin: 0px 0px 10px; font-family: bg-font !important; color: rgb(34, 34, 34); font-size: 13px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; letter-spacing: normal; orphans: 2; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; text-align: justify;"><strong>নারায়ণগঞ্জ সিটি করপোরেশন</strong><span class="Apple-converted-space"> </span>নারায়ণগঞ্জ পৌরসভাকে (মিউনিসিপ্যালিটি) ২০১১ সালের ৫ মে নারায়ণগঞ্জ সিটি করপোরেশনে রূপান্তর করা হয়। নারায়ণগঞ্জ শহর, সিদ্ধিরগঞ্জ পৌর এলাকা এবং বন্দর উপজেলার কমদরসূল পৌরসভাকে নিয়ে নারায়ণগঞ্জ সিটি করপোরেশন গঠিত হয়।</p><p style="box-sizing: border-box; margin: 0px 0px 10px; font-family: bg-font !important; color: rgb(34, 34, 34); font-size: 13px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; letter-spacing: normal; orphans: 2; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; text-align: justify;" rel="box-sizing: border-box; margin: 0px 0px 10px; font-family: bg-font !important; color: rgb(34, 34, 34); font-size: 13px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; letter-spacing: normal; orphans: 2; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; text-align: justify;">সিদ্ধিরগঞ্জ পৌরসভাকে বিলুপ্ত করে এর ১ থেকে ৯ নম্বর ওয়ার্ডকে সিটি করপোরেশনের ১ থেকে ৯ নম্বর ওয়ার্ড হিসাবে ঘোষণা করা হয়েছে। সিদ্ধিরগঞ্জের পাইনাদী পূর্ব অংশ মিজমিজি বাতান পাড়াকে ১ নম্বর ওয়ার্ড করা হয়েছে। ১ থেকে ৩ নম্বর ওয়ার্ডকে সংরক্ষিত মহিলা আসন হিসেবে তালিকা ভুক্ত করা হয়েছে। নারায়ণগঞ্জ পৌরসভা বিলুপ্ত করে এর ৯টি ওয়ার্ডকে সিটি করপোরেশনের ১০ থেকে ১৮ নম্বর ওয়ার্ড হিসেবে তালিকাভুক্ত করা হয়েছে। এই পৌরসভার সংরক্ষিত তিনটি মহিলা ওয়ার্ডকে ৪ থেকে ৬ নম্বর ওয়ার্ড হিসাবে ঘোষণা করা হয়। বন্দর উপজেলার কদম রসূল পৌরসভাকে বিলুপ্ত করে এর ৯টি ওয়ার্ডকে সিটি করপোরেশনের ১৯ থেকে ২৭ নম্বর ওয়ার্ড হিসেবে তালিকাভুক্ত করা হয়। এই পৌরসভার তিনাট মহিলা ওয়ার্ডকে সিটি করপোরেশনের ৭ থেকে ৯ নম্বর ওয়ার্ড হিসাবে ঘোষণা করা হয়।</p><p><br></p>', 1, '2016-11-21 10:10:21', 1, '2016-11-21 10:10:19', 1),
(4, 54, 22, '<p>সিটি কর্পোরেশন মার্কেট উপ-আইনমালা ২০১৩ মোতাবেগ প্রণীত।</p>', 1, '2016-12-07 06:24:04', 7, '2016-12-07 06:23:36', 1),
(5, 54, 21, '<p>1. সিদ্ধিরগঞ্জ পৌরসভাকে বিলুপ্ত করে এর ১ থেকে ৯ নম্বর ওয়ার্ডকে সিটি করপোরেশনের ১ থেকে ৯ নম্বর ওয়ার্ড হিসাবে ঘোষণা করা হয়েছে। </p><p>2. সিদ্ধিরগঞ্জের পাইনাদী পূর্ব অংশ মিজমিজি বাতান পাড়াকে ১ নম্বর ওয়ার্ড করা হয়েছে। ১ থেকে ৩ নম্বর ওয়ার্ডকে সংরক্ষিত মহিলা আসন </p><p>3.পৌরসভার তিনাট মহিলা ওয়ার্ডকে সিটি করপোরেশনের ৭ থেকে ৯ নম্বর ওয়ার্ড হিসাবে ঘোষণা করা হয়।</p><p>4.তালিকাভুক্ত করা হয়েছে। এই পৌরসভার সংরক্ষিত তিনটি মহিলা ওয়ার্ডকে<strong> ৪ থেকে ৬ নম্বর</strong> ওয়ার্ড হিসাবে ঘোষণা করা হয়। বন্দর উপজেলার<br></p>', 1, '2016-12-07 06:24:04', 7, '2016-12-07 06:23:36', 1),
(6, 55, 24, '<p>আবেদনকারীকে নারায়ণগঞ্জ সিটি কর্পোরেশনের নির্ধারিত ফরমে আবেদন করতে হবে।</p>', 1, '2016-12-07 00:19:53', 1, '2016-12-07 06:19:53', NULL),
(7, 56, 21, '<p>1. সিদ্ধিরগঞ্জ পৌরসভাকে বিলুপ্ত করে এর ১ থেকে ৯ নম্বর ওয়ার্ডকে সিটি করপোরেশনের ১ থেকে ৯ নম্বর ওয়ার্ড হিসাবে ঘোষণা করা হয়েছে। </p><p>2. সিদ্ধিরগঞ্জের পাইনাদী পূর্ব অংশ মিজমিজি বাতান পাড়াকে ১ নম্বর ওয়ার্ড করা হয়েছে। ১ থেকে ৩ নম্বর ওয়ার্ডকে সংরক্ষিত মহিলা আসন </p><p>3.পৌরসভার তিনাট মহিলা ওয়ার্ডকে সিটি করপোরেশনের ৭ থেকে ৯ নম্বর ওয়ার্ড হিসাবে ঘোষণা করা হয়।</p><p>4.তালিকাভুক্ত করা হয়েছে। এই পৌরসভার সংরক্ষিত তিনটি মহিলা ওয়ার্ডকে<strong> ৪ থেকে ৬ নম্বর</strong> ওয়ার্ড হিসাবে ঘোষণা করা হয়। বন্দর উপজেলার<br></p>', 1, '2016-12-26 06:26:26', 1, '2016-12-26 12:26:26', NULL),
(8, 57, 23, '<p>আবেদনকারীকে নারায়ণগঞ্জ সিটি কর্পোরেশনের নির্ধারিত ফরমে আবেদন করতে হবে। আবেদনকারীকে নারায়ণগঞ্জ সিটি কর্পোরেশনের নির্ধারিত ফরমে আবেদন করতে হবে। আবেদনকারীকে নারায়ণগঞ্জ সিটি কর্পোরেশনের নির্ধারিত ফরমে আবেদন করতে হবে। আবেদনকারীকে নারায়ণগঞ্জ সিটি কর্পোরেশনের নির্ধারিত ফরমে আবেদন করতে হবে।</p><p><br></p>', 1, '2017-04-23 02:11:49', 1, '2017-04-23 08:11:49', NULL),
(9, 57, 21, '<p>1. সিদ্ধিরগঞ্জ পৌরসভাকে বিলুপ্ত করে এর ১ থেকে ৯ নম্বর ওয়ার্ডকে সিটি করপোরেশনের ১ থেকে ৯ নম্বর ওয়ার্ড হিসাবে ঘোষণা করা হয়েছে। </p><p>2. সিদ্ধিরগঞ্জের পাইনাদী পূর্ব অংশ মিজমিজি বাতান পাড়াকে ১ নম্বর ওয়ার্ড করা হয়েছে। ১ থেকে ৩ নম্বর ওয়ার্ডকে সংরক্ষিত মহিলা আসন </p><p>3.পৌরসভার তিনাট মহিলা ওয়ার্ডকে সিটি করপোরেশনের ৭ থেকে ৯ নম্বর ওয়ার্ড হিসাবে ঘোষণা করা হয়।</p><p>4.তালিকাভুক্ত করা হয়েছে। এই পৌরসভার সংরক্ষিত তিনটি মহিলা ওয়ার্ডকে<strong> ৪ থেকে ৬ নম্বর</strong> ওয়ার্ড হিসাবে ঘোষণা করা হয়। বন্দর উপজেলার<br></p>', 1, '2017-04-23 02:11:49', 1, '2017-04-23 08:11:49', NULL),
(10, 58, 21, '<p>1. সিদ্ধিরগঞ্জ পৌরসভাকে বিলুপ্ত করে এর ১ থেকে ৯ নম্বর ওয়ার্ডকে সিটি করপোরেশনের ১ থেকে ৯ নম্বর ওয়ার্ড হিসাবে ঘোষণা করা হয়েছে। </p><p>2. সিদ্ধিরগঞ্জের পাইনাদী পূর্ব অংশ মিজমিজি বাতান পাড়াকে ১ নম্বর ওয়ার্ড করা হয়েছে। ১ থেকে ৩ নম্বর ওয়ার্ডকে সংরক্ষিত মহিলা আসন </p><p>3.পৌরসভার তিনাট মহিলা ওয়ার্ডকে সিটি করপোরেশনের ৭ থেকে ৯ নম্বর ওয়ার্ড হিসাবে ঘোষণা করা হয়।</p><p>4.তালিকাভুক্ত করা হয়েছে। এই পৌরসভার সংরক্ষিত তিনটি মহিলা ওয়ার্ডকে<strong> ৪ থেকে ৬ নম্বর</strong> ওয়ার্ড হিসাবে ঘোষণা করা হয়। বন্দর উপজেলার<br></p>', 1, '2017-04-24 06:10:25', 1, '2017-04-24 12:10:25', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tender_date_time`
--

CREATE TABLE IF NOT EXISTS `tender_date_time` (
  `TENDER_DT_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `TENDER_ID` bigint(20) NOT NULL COMMENT 'Primary key of tender table',
  `LAST_SELLING_DT_FROM` datetime DEFAULT NULL COMMENT 'Tender last selling date from',
  `LAST_SELLING_DT_TO` datetime DEFAULT NULL COMMENT 'Tender last selling date to',
  `TE_LAST_RECIEVE_DT` datetime DEFAULT NULL COMMENT 'Tender last receive date',
  `TE_OPENING_DT` datetime DEFAULT NULL COMMENT 'Tender opening date',
  `CLAUSE` int(10) unsigned DEFAULT NULL COMMENT 'clause number',
  `IS_ACTIVE` tinyint(1) NOT NULL DEFAULT '1' COMMENT '0=no, 1=yes',
  `CREATED_AT` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `UPDATED_AT` datetime DEFAULT NULL,
  `CREATED_BY` int(10) NOT NULL,
  `UPDATED_BY` int(10) DEFAULT NULL,
  PRIMARY KEY (`TENDER_DT_ID`),
  KEY `tender_date_time_ibfk_1` (`TENDER_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=46 ;

--
-- Dumping data for table `tender_date_time`
--

INSERT INTO `tender_date_time` (`TENDER_DT_ID`, `TENDER_ID`, `LAST_SELLING_DT_FROM`, `LAST_SELLING_DT_TO`, `TE_LAST_RECIEVE_DT`, `TE_OPENING_DT`, `CLAUSE`, `IS_ACTIVE`, `CREATED_AT`, `UPDATED_AT`, `CREATED_BY`, `UPDATED_BY`) VALUES
(37, 48, '2016-11-20 10:00:00', '1201-10-18 07:00:00', '2016-11-13 12:00:00', '2016-11-13 12:00:00', 1, 1, '2017-10-18 08:46:52', '2016-11-13 03:56:40', 1, NULL),
(38, 49, '2016-11-20 10:00:00', '2016-11-20 17:00:00', '2016-11-13 12:00:00', '2016-11-13 12:00:00', NULL, 1, '2016-11-12 22:00:16', '2016-11-13 04:00:16', 1, NULL),
(39, 50, '2016-11-24 00:00:00', '2016-11-24 00:00:00', '2016-11-20 00:00:00', '2016-11-20 00:00:00', NULL, 1, '2016-11-22 10:45:14', '2016-11-22 10:45:29', 1, 1),
(40, 53, '2016-12-07 00:00:00', '2016-12-07 00:00:00', '2016-12-08 00:00:00', '2016-12-08 00:00:00', 1, 1, '2016-12-07 03:52:01', '2016-12-07 03:51:33', 1, 1),
(41, 54, '2017-04-23 00:00:00', '2017-04-23 00:00:00', '2017-04-28 00:00:00', '2017-04-28 00:00:00', NULL, 1, '2017-01-10 03:39:49', '2017-01-10 03:39:49', 7, 1),
(42, 55, '2016-12-08 00:00:00', '2016-12-08 00:00:00', '2016-12-07 00:00:00', '2016-12-07 00:00:00', 1, 1, '2016-12-07 00:19:53', '2016-12-07 06:19:53', 1, NULL),
(43, 56, '2016-12-30 00:00:00', '2016-12-30 00:00:00', '2016-12-29 00:00:00', '2016-12-29 00:00:00', NULL, 1, '2016-12-26 06:26:26', '2016-12-26 12:26:26', 1, NULL),
(44, 57, '2017-04-23 00:00:00', '2017-04-23 00:00:00', '2017-04-23 00:00:00', '2017-04-23 00:00:00', NULL, 1, '2017-04-23 02:11:49', '2017-04-23 08:11:49', 1, NULL),
(45, 58, '2017-04-30 00:00:00', '2017-04-30 00:00:00', '2017-04-24 00:00:00', '2017-04-24 00:00:00', NULL, 1, '2017-04-24 06:10:25', '2017-04-24 12:10:25', 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tender_installment`
--

CREATE TABLE IF NOT EXISTS `tender_installment` (
  `TENDER_INS_ID` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'Primary key',
  `TE_APP_ID` bigint(20) DEFAULT NULL COMMENT 'Tender applicant id',
  `CITIZEN_ID` bigint(20) DEFAULT NULL COMMENT 'Primary key of sa_citizen',
  `TRX_TRAN_NO` bigint(20) DEFAULT NULL COMMENT 'Primary key of ac_voucherchd table',
  `SCHEDULE_ID` bigint(20) NOT NULL COMMENT 'Tender schedule id',
  `INSTALLMENT` tinyint(2) NOT NULL COMMENT 'Number of installment',
  `AMOUNT` double NOT NULL COMMENT 'Installment amount',
  `PUNISHMENT` tinyint(2) DEFAULT NULL COMMENT 'percentage of punishment if applicant failed to pay within date',
  `DATE` datetime NOT NULL COMMENT 'Installment date',
  `IS_PAID` tinyint(1) DEFAULT '0' COMMENT 'Determine if payment is paid by applicant',
  `IS_APPROVED` tinyint(1) DEFAULT '0' COMMENT '0=not approved payment, 1=approved payment',
  `REMARKS` text COMMENT 'Remarks if any',
  `IS_ACTIVE` tinyint(1) NOT NULL DEFAULT '1',
  `CREATED_AT` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `CREATED_BY` bigint(20) DEFAULT NULL,
  `UPDATED_AT` datetime DEFAULT NULL,
  `UPDATED_BY` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`TENDER_INS_ID`),
  KEY `tender_installment_ibfk_1` (`TE_APP_ID`),
  KEY `tender_installment_ibfk_2` (`SCHEDULE_ID`),
  KEY `TRX_TRAN_NO` (`TRX_TRAN_NO`),
  KEY `CITIZEN_ID` (`CITIZEN_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=27 ;

--
-- Dumping data for table `tender_installment`
--

INSERT INTO `tender_installment` (`TENDER_INS_ID`, `TE_APP_ID`, `CITIZEN_ID`, `TRX_TRAN_NO`, `SCHEDULE_ID`, `INSTALLMENT`, `AMOUNT`, `PUNISHMENT`, `DATE`, `IS_PAID`, `IS_APPROVED`, `REMARKS`, `IS_ACTIVE`, `CREATED_AT`, `CREATED_BY`, `UPDATED_AT`, `UPDATED_BY`) VALUES
(5, NULL, 7, 73, 128, 1, 2250, 10, '2016-11-10 00:00:00', 1, 0, NULL, 1, '2016-11-14 04:30:13', 1, '2016-11-14 11:08:01', NULL),
(6, NULL, 7, 74, 128, 2, 2250, 10, '2016-11-23 00:00:00', 1, 0, NULL, 1, '2016-11-14 04:30:13', 1, '2016-11-14 11:10:07', NULL),
(7, NULL, 7, 75, 128, 3, 2250, 10, '2016-11-30 00:00:00', 1, 0, NULL, 1, '2016-11-14 04:30:13', 1, '2016-11-15 09:05:43', NULL),
(8, NULL, 7, 76, 128, 4, 2250, 10, '2016-12-07 00:00:00', 1, 0, NULL, 1, '2016-11-14 04:30:13', 1, '2016-11-15 09:05:58', NULL),
(9, 264, NULL, 79, 128, 1, 3000, 15, '2016-11-10 00:00:00', 1, 0, NULL, 1, '2016-11-14 04:43:07', 1, '2016-11-14 10:57:36', NULL),
(10, 264, NULL, 80, 128, 2, 3000, 15, '2016-11-12 00:00:00', 0, 0, NULL, 1, '2016-11-14 04:43:07', 1, '2016-11-14 10:43:07', NULL),
(11, 264, NULL, 81, 128, 3, 3000, 15, '2016-11-22 00:00:00', 0, 0, NULL, 1, '2016-11-14 04:43:07', 1, '2016-11-14 10:43:07', NULL),
(12, 265, NULL, 96, 137, 1, 4320, 10, '2016-11-24 00:00:00', 0, 0, NULL, 1, '2016-11-23 06:02:26', 7, '2016-11-23 12:02:26', NULL),
(13, 265, NULL, 97, 137, 2, 4320, 10, '2016-12-07 00:00:00', 0, 0, NULL, 1, '2016-11-23 06:02:26', 7, '2016-11-23 12:02:26', NULL),
(14, NULL, 7, 101, 141, 1, 3000, 10, '2016-11-30 00:00:00', 0, 0, NULL, 1, '2016-11-30 02:16:49', 1, '2016-11-30 08:16:49', NULL),
(15, NULL, 7, 102, 141, 2, 3000, 10, '2016-12-07 00:00:00', 0, 0, NULL, 1, '2016-11-30 02:16:49', 1, '2016-11-30 08:16:49', NULL),
(16, NULL, 7, 103, 141, 3, 3000, 10, '2016-12-14 00:00:00', 0, 0, NULL, 1, '2016-11-30 02:16:49', 1, '2016-11-30 08:16:49', NULL),
(17, 267, NULL, 108, 139, 1, 8400, 15, '2016-12-07 00:00:00', 1, 0, NULL, 1, '2016-12-07 00:28:42', 1, '2017-04-16 08:55:09', NULL),
(18, 267, NULL, 109, 139, 1, 8400, 10, '2016-12-14 00:00:00', 1, 0, NULL, 1, '2016-12-07 00:30:26', 1, '2017-04-16 09:00:23', NULL),
(19, 267, NULL, 110, 139, 1, 8400, 10, '2016-12-07 00:00:00', 0, 0, NULL, 1, '2016-12-07 00:36:50', 1, '2016-12-07 06:36:50', NULL),
(20, 267, NULL, 111, 139, 1, 8400, 15, '2016-12-14 00:00:00', 0, 0, NULL, 1, '2016-12-07 00:44:39', 1, '2016-12-07 06:44:39', NULL),
(21, 268, NULL, 114, 141, 1, 3000, 10, '2016-12-13 00:00:00', 0, 0, NULL, 1, '2016-12-11 23:53:40', 1, '2016-12-12 05:53:40', NULL),
(22, 268, NULL, 115, 141, 2, 3000, 10, '2016-12-20 00:00:00', 0, 0, NULL, 1, '2016-12-11 23:53:40', 1, '2016-12-12 05:53:40', NULL),
(23, 268, NULL, 116, 141, 3, 3000, 10, '2016-12-27 00:00:00', 0, 0, NULL, 1, '2016-12-11 23:53:40', 1, '2016-12-12 05:53:40', NULL),
(24, 269, NULL, 125, 144, 1, 3500000, 5, '2016-12-27 00:00:00', 0, 0, NULL, 1, '2016-12-26 06:43:14', 1, '2016-12-26 12:43:14', NULL),
(25, 269, NULL, 126, 144, 2, 3500000, 5, '2016-12-31 00:00:00', 0, 0, NULL, 1, '2016-12-26 06:43:14', 1, '2016-12-26 12:43:14', NULL),
(26, 277, NULL, 143, 139, 1, 8400, 5, '2017-04-20 00:00:00', 1, 0, NULL, 1, '2017-04-11 11:55:36', 1, '2017-04-11 17:57:14', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tender_latter`
--

CREATE TABLE IF NOT EXISTS `tender_latter` (
  `TENDER_LETTER_ID` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'Primary key',
  `TE_APP_ID` bigint(20) DEFAULT NULL COMMENT 'Tender applicant id',
  `CITIZEN_ID` bigint(20) DEFAULT NULL COMMENT 'Primary key of sa_citizen',
  `SCHEDULE_ID` bigint(20) NOT NULL COMMENT 'Tender schedule id',
  `PARTICULAR_ID` smallint(6) DEFAULT NULL,
  `LATTER_NUMBER` varchar(200) NOT NULL COMMENT 'Letter number',
  `LETTER_SOURCE` varchar(150) DEFAULT NULL,
  `DATE` datetime NOT NULL COMMENT 'Letter date',
  `SUBJECT` varchar(250) NOT NULL COMMENT 'letter subject',
  `BODY` text NOT NULL COMMENT 'Letter body',
  `CONDITIONS` text COMMENT 'Letter conditions',
  `APPLICANT_INFO` text COMMENT 'Letter applicant information',
  `OFFICER_INFO` text COMMENT 'Letter officer information',
  `DISTRIBUTION` text COMMENT 'Letter distribution information',
  `TYPE` tinyint(1) DEFAULT '0' COMMENT '0 = primary, 1 = final',
  `IS_LEASE` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0=tender, 1=lease',
  `CREATED_AT` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `CREATED_BY` int(10) unsigned NOT NULL,
  `UPDATED_AT` datetime DEFAULT NULL,
  `UPDATED_BY` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`TENDER_LETTER_ID`),
  KEY `tender_latter_ibfk_1` (`TE_APP_ID`),
  KEY `tender_latter_ibfk_2` (`SCHEDULE_ID`),
  KEY `PARTICULAR_ID` (`PARTICULAR_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=111 ;

--
-- Dumping data for table `tender_latter`
--

INSERT INTO `tender_latter` (`TENDER_LETTER_ID`, `TE_APP_ID`, `CITIZEN_ID`, `SCHEDULE_ID`, `PARTICULAR_ID`, `LATTER_NUMBER`, `LETTER_SOURCE`, `DATE`, `SUBJECT`, `BODY`, `CONDITIONS`, `APPLICANT_INFO`, `OFFICER_INFO`, `DISTRIBUTION`, `TYPE`, `IS_LEASE`, `CREATED_AT`, `CREATED_BY`, `UPDATED_AT`, `UPDATED_BY`) VALUES
(73, 263, NULL, 128, NULL, '8997565', 'গত 13-11-2016 তারিখে আপনার দাখিলকৃত দরপত্র।', '2016-11-13 00:00:00', 'ইজারার অবশিষ্ট টাকা পরিশোধ প্রসঙ্গে', '<p>উপর্যুক্ত বিষয়ে জানানো যাচ্ছে যে, নারায়ণগঞ্জ সিটি কর্পোরেশনাধীন পদ্ম সিটি প্লাজা-১ <span class="start_date">১৩-১১-২০১৬</span> তারিখ হতে <span class="end_date">১২-১১-২০১৭</span> তারিখ পর্যন্ত ইজারা গ্রহণের জন্য আপনার প্রস্তাবিত দর ১২,০০০/- টাকা সর্বোচ্চ বিবেচিত হওয়ায় তা গৃহিত হয়েছে। আপনি দরপত্রের সাথে পে-অর্ডার দ্বারা ৩,০০০/- টাকা জমা প্রদান করেছেন।</p><p>এমতাবস্থায়, পত্র প্রাপ্তির ০৭ (সাত) কার্যদিবসের মধ্যে প্রস্তাবিত মূল্যের উপর\r\n                            \r\n                                                                    ২৫% জামানত বাবদ ৩,০০০/- টাকা \r\n                                                            \r\n                            সর্বমোট ৩,০০০/- টাকা সিটি কর্পোরেশন তহবিলে জমা প্রদান করতঃ সিটি কর্পোরেশনের সহিত ৩০০/-টাকা নন জুডিশিয়াল স্ট্যাম্পে চুক্তিপত্র সম্পাদানের জন্য অনুরোধ করা হলো।\r\n                        </p>', NULL, '<p>জনাব Abdul Awal</p><p>                            পিতা/স্বামী- Maynuddin</p><p>                            বাসা নং- 52, রাস্তা- 15</p><p>                            থানা- DHUPCHANCHIA, জেলা- BOGRA</p>', '<p>(মোঃ মোস্তফা কামাল মুুজমদার)</p><p>                            (প্রধান নির্বাহী কর্মকর্তা)</p><p>                            (নারায়নগঞ্জ সিটি কর্পোরেশন)</p><p>                            (৭৬৩৩৪২৩)</p><p>                            (ceo@ncc.gov.bd)</p>', '<p>1। হিসাব বিভাগ,নগর ভবন,নারায়ণগঞ্জ সিটি কর্পোরেশন নারায়ণগঞ্জ।<br></p><p>                                                2। উপ-পরিচালক,স্থানীয় সরকার শাখা,জেলা প্রশাসকের কার্যালয়,নারায়ণগঞ্জ।<br></p><p>                                                3। সচিব স্থানীয় সরকার বিভাগ,স্থানীয় সরকার পল্লী উন্নয়ন ও সমবায় মন্ত্রণালয়,বাংলাদেশ সচিবালয়,ঢাকা  <br></p><p>                                                4। জেলা প্রশাসক,নারায়ণগঞ্জ।<br></p>', 0, 1, '2016-11-13 04:28:47', 1, '2016-11-13 10:28:47', NULL),
(75, 263, NULL, 124, NULL, '৪৬.৪৪৬৭০০.১৪.১৮(৮).১৬', 'গত 13-11-2016 তারিখে আপনার দাখিলকৃত দরপত্র।', '2016-11-13 00:00:00', 'ইজারার অবশিষ্ট টাকা পরিশোধ প্রসঙ্গে', '<p>উপর্যুক্ত বিষয়ে জানানো যাচ্ছে যে, নারায়ণগঞ্জ সিটি কর্পোরেশনাধীন গোদনাইল হাট বাজার <span class="start_date">১৪-১১-২০১৬</span> তারিখ হতে <span class="end_date">৩০-১১-২০১৬</span> তারিখ পর্যন্ত ইজারা গ্রহণের জন্য আপনার প্রস্তাবিত দর ১২,০০০/- টাকা সর্বোচ্চ বিবেচিত হওয়ায় তা গৃহিত হয়েছে। আপনি দরপত্রের সাথে পে-অর্ডার দ্বারা ১২,০০০/- টাকা জমা প্রদান করেছেন।</p><p>এমতাবস্থায়, পত্র প্রাপ্তির ০৭ (সাত) কার্যদিবসের মধ্যে প্রস্তাবিত মূল্যের উপর\r\n                            \r\n                                                                    ৫% জামানত বাবদ ৬০০/- টাকা \r\n                                                                    ১৫% ভ্যাট বাবদ ১,৮০০/- টাকা \r\n                                                                    ৫% আয়কর বাবদ বাবদ ৬০০/- টাকা \r\n                                                            \r\n                            সর্বমোট ৩,০০০/- টাকা সিটি কর্পোরেশন তহবিলে জমা প্রদান করতঃ সিটি কর্পোরেশনের সহিত ৩০০/-টাকা নন জুডিশিয়াল স্ট্যাম্পে চুক্তিপত্র সম্পাদানের জন্য অনুরোধ করা হলো।\r\n                        </p>', NULL, '<p>জনাব Abdul Awal</p><p>                            পিতা/স্বামী- Maynuddin</p><p>                            বাসা নং- 52, রাস্তা- 15</p><p>                            থানা- DHUPCHANCHIA, জেলা- BOGRA</p>', '<p>(মোঃ মোস্তফা কামাল মুুজমদার)</p><p>                            (প্রধান নির্বাহী কর্মকর্তা)</p><p>                            (নারায়নগঞ্জ সিটি কর্পোরেশন)</p><p>                            (৭৬৩৩৪২৩)</p><p>                            (ceo@ncc.gov.bd)</p>', '<p>1। হিসাব বিভাগ,নগর ভবন,নারায়ণগঞ্জ সিটি কর্পোরেশন নারায়ণগঞ্জ।<br></p><p>                                                2। উপ-পরিচালক,স্থানীয় সরকার শাখা,জেলা প্রশাসকের কার্যালয়,নারায়ণগঞ্জ।<br></p><p>                                                3। সচিব স্থানীয় সরকার বিভাগ,স্থানীয় সরকার পল্লী উন্নয়ন ও সমবায় মন্ত্রণালয়,বাংলাদেশ সচিবালয়,ঢাকা  <br></p><p>                                                4। জেলা প্রশাসক,নারায়ণগঞ্জ।<br></p>', 0, 1, '2016-11-13 05:38:09', 1, '2016-11-13 11:38:09', NULL),
(76, 263, NULL, 124, NULL, 'এনসিসি/বাজার/ইজারা/৯৯/(অনশ)/', NULL, '2016-11-13 00:00:00', 'গোদনাইল হাট বাজার ইজারা প্রদানের কার্যাদেশ পত্র', '<p>উপর্যুক্ত বিষয়ে জানানো যাচ্ছে যে, নারায়ণগঞ্জ সিটি কর্পোরেশনের নিয়ন্ত্রণাধীন গোদনাইল হাট বাজার ১৪/১১/২০১৬ তারিখ হতে ৩০/১১/২০১৬ তারিখ পর্যন্ত ইজারা গ্রহণের জন্য আপনার প্রস্তাবিত দর ১২,০০০/- (বার হাজার টাকা মাত্র) টাকা সর্বোচ্চ বিবেচিত হওয়ায় তা সিটি কর্পোরেশন কর্তৃক গৃহীত হয়। </p><p>                        আপনি ইজারা মুলা মূল্য বাবদ ১২,০০০/- টাকা,                                                                             ৫% জামানত বাবদ ৬০০/- টাকা </p><p>                                                                                                                                                        ১৫% ভ্যাট বাবদ ১,৮০০/- টাকা </p><p>                                                                                                                                                        ৫% আয়কর বাবদ বাবদ ৬০০/- টাকা </p><p>                    সর্বমোট ১৫,০০০/- টাকা পরিশোধ করায় ১৪/১১/২০১৬ তারিখ হতে ৩০/১১/২০১৬ তারিখ পর্যন্ত বর্ণিত গোদনাইল হাট বাজার হতে টোল আদায় করার কার্যাদেশ প্রদান করা হলো।</p>', '<p>শর্তাবলি ঃ</p><p>                ১। গোদনাইল হাট বাজার পরিচালনার ক্ষেত্রে ইজারার বিজ্ঞপ্তির বর্ণিত শর্তাবলি প্রযোজ্য হবে।</p><p>                ২। শুধুমাত্র গোদনাইল হাট বাজার হতে টোল আদায় করতে হবে।</p><p>                ৩। এই কার্যাদেশের মেয়াদ ১৪/১১/২০১৬ তারিখ হতে ৩০/১১/২০১৬ তারিখ পর্যন্ত।</p>', '<p>জনাব Abdul Awal</p><p>                            পিতা/স্বামী- Maynuddin</p><p>                            বাসা নং- 52, রাস্তা- 15</p><p>                            থানা- DHUPCHANCHIA, জেলা- BOGRA</p>', '<p>(মোঃ মোস্তফা কামাল মুুজমদার)</p><p>                            (প্রধান নির্বাহী কর্মকর্তা)</p><p>                            (নারায়নগঞ্জ সিটি কর্পোরেশন)</p><p>                            (৭৬৩৩৪২৩)</p><p>                            (ceo@ncc.gov.bd)</p>', '<p>১। হিসাব বিভাগ,নগর ভবন,নারায়ণগঞ্জ সিটি কর্পোরেশন নারায়ণগঞ্জ।<br></p><p>                                                ২। উপ-পরিচালক,স্থানীয় সরকার শাখা,জেলা প্রশাসকের কার্যালয়,নারায়ণগঞ্জ।<br></p><p>                                                ৩। সচিব স্থানীয় সরকার বিভাগ,স্থানীয় সরকার পল্লী উন্নয়ন ও সমবায় মন্ত্রণালয়,বাংলাদেশ সচিবালয়,ঢাকা  <br></p><p>                                                ৪। জেলা প্রশাসক,নারায়ণগঞ্জ।<br></p>', 1, 1, '2016-11-13 06:48:56', 1, '2016-11-13 12:48:56', NULL),
(78, NULL, 7, 128, NULL, 'gbghgh', 'গত 14-11-2016 তারিখে আপনার দাখিলকৃত দরপত্র।', '2016-11-14 00:00:00', 'পদ্ম সিটি প্লাজা-১', '<p>উপর্যুক্ত বিষয়ে জানানো যাচ্ছে যে, নারায়ণগঞ্জ সিটি কর্পোরেশনাধীন পদ্ম সিটি প্লাজা-১ ১৪২৩ বাংলা সনের ১ বৈশাখ হতে ৩০ চৈত্র পর্যন্ত এক বত্সর মেয়াদে ইজারা গ্রহণের জন্য আপনার প্রস্তাবিত দর ১২০০০/-টাকা সর্বোচ্চ বিবেচিত হওয়ায় তা গ্রেহিত হয়েছে। আপনি দরপত্রের সাথে পে-অর্ডার দ্বারা ৩০০০ টাকা জমা প্রদান করেছেন।</p><p>                               এমতাবস্তায়, পত্র প্রাপ্তির ০৭ (সাত) কার্যদিবসের মধ্যে প্রস্তাবিত মূল্য এর উপর ১৫% ভ্যাট বাবদ ৫০০০/-টাকা, ৫% আয়কর বাবদ ৮৯৬৬/-টাকা এবং ৫% জামানত বাবদ ৫০০/-টাকা সহ সর্বমোট ৫৯৬৩ টাকা সিটি কর্পোরেশন তহবিলে জমা প্রদান করতঃ সিটি কর্পোরেশনের সহিত ৩০০/-টাকা নন জুডিশিয়াল স্ট্যাম্পে চুক্তিপত্র সম্পাদানের জন্য অনুরোধ করা হলো।</p>', NULL, '<p>জনাব Abdul Awal</p><p>                                পিতা/স্বামী- Maynuddin</p><p>                                বাসা নং- 52, রাস্তা- 15</p><p>                                থানা- DHUPCHANCHIA, জেলা- BOGRA</p>', '<p>(মোঃ মোস্তফা কামাল মুুজমদার)</p><p>                                (প্রধান নির্বাহী কর্মকর্তা)</p><p>                                (নারায়নগঞ্জ সিটি কর্পোরেশন)</p><p>                                (৭৬৩৩৪২৩)</p><p>                                (ceo@ncc.gov.bd)</p>', '<p>১। হিসাব বিভাগ,নগর ভবন,নারায়ণগঞ্জ সিটি কর্পোরেশন নারায়ণগঞ্জ।<br></p><p>                                                        ২। উপ-পরিচালক,স্থানীয় সরকার শাখা,জেলা প্রশাসকের কার্যালয়,নারায়ণগঞ্জ।<br></p><p>                                                        ৩। সচিব স্থানীয় সরকার বিভাগ,স্থানীয় সরকার পল্লী উন্নয়ন ও সমবায় মন্ত্রণালয়,বাংলাদেশ সচিবালয়,ঢাকা  <br></p><p>                                                        ৪। জেলা প্রশাসক,নারায়ণগঞ্জ।<br></p>', 0, 0, '2016-11-14 04:30:14', 0, '2016-11-14 10:30:14', NULL),
(79, 264, NULL, 128, NULL, '535338754', 'গত 14-11-2016 তারিখে আপনার দাখিলকৃত দরপত্র।', '2016-11-14 00:00:00', 'পদ্ম সিটি প্লাজা-১', '<p>উপর্যুক্ত বিষয়ে জানানো যাচ্ছে যে, নারায়ণগঞ্জ সিটি কর্পোরেশনাধীন পদ্ম সিটি প্লাজা-১ ১৪২৩ বাংলা সনের ১ বৈশাখ হতে ৩০ চৈত্র পর্যন্ত এক বত্সর মেয়াদে ইজারা গ্রহণের জন্য আপনার প্রস্তাবিত দর ১২০০০/-টাকা সর্বোচ্চ বিবেচিত হওয়ায় তা গ্রেহিত হয়েছে। আপনি দরপত্রের সাথে পে-অর্ডার দ্বারা ৩০০০ টাকা জমা প্রদান করেছেন।</p><p>                               এমতাবস্তায়, পত্র প্রাপ্তির ০৭ (সাত) কার্যদিবসের মধ্যে প্রস্তাবিত মূল্য এর উপর ১৫% ভ্যাট বাবদ ৫০০০/-টাকা, ৫% আয়কর বাবদ ৮৯৬৬/-টাকা এবং ৫% জামানত বাবদ ৫০০/-টাকা সহ সর্বমোট ৫৯৬৩ টাকা সিটি কর্পোরেশন তহবিলে জমা প্রদান করতঃ সিটি কর্পোরেশনের সহিত ৩০০/-টাকা নন জুডিশিয়াল স্ট্যাম্পে চুক্তিপত্র সম্পাদানের জন্য অনুরোধ করা হলো।</p>', NULL, '<p>জনাব মোঃ নুরুল্লাহ</p><p>                                পিতা/স্বামী- মোঃ আঃ হাকিম</p><p>                                বাসা নং- পাটুঃ-৫৮৯, রাস্তা- নাই</p><p>                                থানা- PAKUNDIA, জেলা- KISHOREGANJ</p>', '<p>(মোঃ মোস্তফা কামাল মুুজমদার)</p><p>                                (প্রধান নির্বাহী কর্মকর্তা)</p><p>                                (নারায়নগঞ্জ সিটি কর্পোরেশন)</p><p>                                (৭৬৩৩৪২৩)</p><p>                                (ceo@ncc.gov.bd)</p>', '<p>১। হিসাব বিভাগ,নগর ভবন,নারায়ণগঞ্জ সিটি কর্পোরেশন নারায়ণগঞ্জ।<br></p><p>                                                        ২। উপ-পরিচালক,স্থানীয় সরকার শাখা,জেলা প্রশাসকের কার্যালয়,নারায়ণগঞ্জ।<br></p><p>                                                        ৩। সচিব স্থানীয় সরকার বিভাগ,স্থানীয় সরকার পল্লী উন্নয়ন ও সমবায় মন্ত্রণালয়,বাংলাদেশ সচিবালয়,ঢাকা  <br></p><p>                                                        ৪। জেলা প্রশাসক,নারায়ণগঞ্জ।<br></p>', 0, 0, '2016-11-14 04:43:07', 0, '2016-11-14 10:43:07', NULL),
(80, 264, NULL, 124, NULL, '64447723434', 'গত 14-11-2016 তারিখে আপনার দাখিলকৃত দরপত্র।', '2016-11-14 00:00:00', 'ইজারার অবশিষ্ট টাকা পরিশোধ প্রসঙ্গে', '<p>উপর্যুক্ত বিষয়ে জানানো যাচ্ছে যে, নারায়ণগঞ্জ সিটি কর্পোরেশনাধীন গোদনাইল হাট বাজার <span class="start_date">১৬-১১-২০১৬</span> তারিখ হতে <span class="end_date">২২-১১-২০১৬</span> তারিখ পর্যন্ত ইজারা গ্রহণের জন্য আপনার প্রস্তাবিত দর ৫৫০,৫০০/- টাকা সর্বোচ্চ বিবেচিত হওয়ায় তা গৃহিত হয়েছে। আপনি দরপত্রের সাথে পে-অর্ডার দ্বারা ৫৫০,৫০০/- টাকা জমা প্রদান করেছেন।</p><p>এমতাবস্থায়, পত্র প্রাপ্তির ০৭ (সাত) কার্যদিবসের মধ্যে প্রস্তাবিত মূল্যের উপর\r\n                            \r\n                                                                    ৫% জামানত বাবদ ২৭,৫২৫/- টাকা \r\n                                                                    ১৫% ভ্যাট বাবদ ৮২,৫৭৫/- টাকা \r\n                                                                    ৫% আয়কর বাবদ বাবদ ২৭,৫২৫/- টাকা \r\n                                                            \r\n                            সর্বমোট ১৩৭,৬২৫/- টাকা সিটি কর্পোরেশন তহবিলে জমা প্রদান করতঃ সিটি কর্পোরেশনের সহিত ৩০০/-টাকা নন জুডিশিয়াল স্ট্যাম্পে চুক্তিপত্র সম্পাদানের জন্য অনুরোধ করা হলো।\r\n                        </p>', NULL, '<p>জনাব মোঃ নুরুল্লাহ</p><p>                            পিতা/স্বামী- মোঃ আঃ হাকিম</p><p>                            বাসা নং- পাটুঃ-৫৮৯, রাস্তা- নাই</p><p>                            থানা- PAKUNDIA, জেলা- KISHOREGANJ</p>', '<p>(মোঃ মোস্তফা কামাল মুুজমদার)</p><p>                            (প্রধান নির্বাহী কর্মকর্তা)</p><p>                            (নারায়নগঞ্জ সিটি কর্পোরেশন)</p><p>                            (৭৬৩৩৪২৩)</p><p>                            (ceo@ncc.gov.bd)</p>', '<p>1। হিসাব বিভাগ,নগর ভবন,নারায়ণগঞ্জ সিটি কর্পোরেশন নারায়ণগঞ্জ।<br></p><p>                                                2। উপ-পরিচালক,স্থানীয় সরকার শাখা,জেলা প্রশাসকের কার্যালয়,নারায়ণগঞ্জ।<br></p><p>                                                3। সচিব স্থানীয় সরকার বিভাগ,স্থানীয় সরকার পল্লী উন্নয়ন ও সমবায় মন্ত্রণালয়,বাংলাদেশ সচিবালয়,ঢাকা  <br></p><p>                                                4। জেলা প্রশাসক,নারায়ণগঞ্জ।<br></p>', 0, 1, '2016-11-14 07:39:29', 1, '2016-11-14 13:39:29', NULL),
(81, 264, NULL, 124, NULL, '346475757', NULL, '2016-11-14 00:00:00', 'গোদনাইল হাট বাজার ইজারা প্রদানের কার্যাদেশ পত্র', '<p>উপর্যুক্ত বিষয়ে জানানো যাচ্ছে যে, নারায়ণগঞ্জ সিটি কর্পোরেশনের নিয়ন্ত্রণাধীন গোদনাইল হাট বাজার ১৬/১১/২০১৬ তারিখ হতে ২২/১১/২০১৬ তারিখ পর্যন্ত ইজারা গ্রহণের জন্য আপনার প্রস্তাবিত দর ৫৫০,৫০০/- (পাঁচ লাখ পঞ্চাশ হাজার পাঁচশত টাকা) টাকা সর্বোচ্চ বিবেচিত হওয়ায় তা সিটি কর্পোরেশন কর্তৃক গৃহীত হয়। </p><p>                        আপনি ইজারা মুলা মূল্য বাবদ ৫৫০,৫০০/- টাকা,                                                                             ৫% জামানত বাবদ ২৭,৫২৫/- টাকা </p><p>                                                                                                                                                        ১৫% ভ্যাট বাবদ ৮২,৫৭৫/- টাকা </p><p>                                                                                                                                                        ৫% আয়কর বাবদ বাবদ ২৭,৫২৫/- টাকা </p><p>                    সর্বমোট ৬৮৮,১২৫/- টাকা পরিশোধ করায় ১৬/১১/২০১৬ তারিখ হতে ২২/১১/২০১৬ তারিখ পর্যন্ত বর্ণিত গোদনাইল হাট বাজার হতে টোল আদায় করার কার্যাদেশ প্রদান করা হলো।</p>', '<p>শর্তাবলি ঃ</p><p>                ১। গোদনাইল হাট বাজার পরিচালনার ক্ষেত্রে ইজারার বিজ্ঞপ্তির বর্ণিত শর্তাবলি প্রযোজ্য হবে।</p><p>                ২। শুধুমাত্র গোদনাইল হাট বাজার হতে টোল আদায় করতে হবে।</p><p>                ৩। এই কার্যাদেশের মেয়াদ ১৬/১১/২০১৬ তারিখ হতে ২২/১১/২০১৬ তারিখ পর্যন্ত।</p>', '<p>জনাব মোঃ নুরুল্লাহ</p><p>                            পিতা/স্বামী- মোঃ আঃ হাকিম</p><p>                            বাসা নং- পাটুঃ-৫৮৯, রাস্তা- নাই</p><p>                            থানা- PAKUNDIA, জেলা- KISHOREGANJ</p>', '<p>(মোঃ মোস্তফা কামাল মুুজমদার)</p><p>                            (প্রধান নির্বাহী কর্মকর্তা)</p><p>                            (নারায়নগঞ্জ সিটি কর্পোরেশন)</p><p>                            (৭৬৩৩৪২৩)</p><p>                            (ceo@ncc.gov.bd)</p>', '<p>১। হিসাব বিভাগ,নগর ভবন,নারায়ণগঞ্জ সিটি কর্পোরেশন নারায়ণগঞ্জ।<br></p><p>                                                ২। উপ-পরিচালক,স্থানীয় সরকার শাখা,জেলা প্রশাসকের কার্যালয়,নারায়ণগঞ্জ।<br></p><p>                                                ৩। সচিব স্থানীয় সরকার বিভাগ,স্থানীয় সরকার পল্লী উন্নয়ন ও সমবায় মন্ত্রণালয়,বাংলাদেশ সচিবালয়,ঢাকা  <br></p><p>                                                ৪। জেলা প্রশাসক,নারায়ণগঞ্জ।<br></p>', 1, 1, '2016-11-14 07:42:31', 1, '2016-11-14 13:42:31', NULL),
(82, NULL, 7, 124, NULL, '565656', 'গত 15-11-2016 তারিখে আপনার দাখিলকৃত দরপত্র।', '2016-11-15 00:00:00', 'ইজারার অবশিষ্ট টাকা পরিশোধ প্রসঙ্গে', '<p>উপর্যুক্ত বিষয়ে জানানো যাচ্ছে যে, নারায়ণগঞ্জ সিটি কর্পোরেশনাধীন গোদনাইল হাট বাজার <span class="start_date">১৬-১১-২০১৬</span> তারিখ হতে <span class="end_date">১৭-১১-২০১৬</span> তারিখ পর্যন্ত ইজারা গ্রহণের জন্য আপনার প্রস্তাবিত দর ১২,০০০/- টাকা সর্বোচ্চ বিবেচিত হওয়ায় তা গৃহিত হয়েছে। আপনি দরপত্রের সাথে পে-অর্ডার দ্বারা ১২,০০০/- টাকা জমা প্রদান করেছেন।</p><p>এমতাবস্থায়, পত্র প্রাপ্তির ০৭ (সাত) কার্যদিবসের মধ্যে প্রস্তাবিত মূল্যের উপর\r\n                            \r\n                                                                    ৫% জামানত বাবদ ৬০০/- টাকা \r\n                                                                    ১৫% ভ্যাট বাবদ ১,৮০০/- টাকা \r\n                                                                    ৫% আয়কর বাবদ বাবদ ৬০০/- টাকা \r\n                                                            \r\n                            সর্বমোট ৩,০০০/- টাকা সিটি কর্পোরেশন তহবিলে জমা প্রদান করতঃ সিটি কর্পোরেশনের সহিত ৩০০/-টাকা নন জুডিশিয়াল স্ট্যাম্পে চুক্তিপত্র সম্পাদানের জন্য অনুরোধ করা হলো।\r\n                        </p>', NULL, '<p>জনাব Abdul Awal</p><p>                            পিতা/স্বামী- Maynuddin</p><p>                            বাসা নং- 52, রাস্তা- 15</p><p>                            থানা- DHUPCHANCHIA, জেলা- BOGRA</p>', '<p>(মোঃ মোস্তফা কামাল মুুজমদার)</p><p>                            (প্রধান নির্বাহী কর্মকর্তা)</p><p>                            (নারায়নগঞ্জ সিটি কর্পোরেশন)</p><p>                            (৭৬৩৩৪২৩)</p><p>                            (ceo@ncc.gov.bd)</p>', '<p>1। হিসাব বিভাগ,নগর ভবন,নারায়ণগঞ্জ সিটি কর্পোরেশন নারায়ণগঞ্জ।<br></p><p>                                                2। উপ-পরিচালক,স্থানীয় সরকার শাখা,জেলা প্রশাসকের কার্যালয়,নারায়ণগঞ্জ।<br></p><p>                                                3। সচিব স্থানীয় সরকার বিভাগ,স্থানীয় সরকার পল্লী উন্নয়ন ও সমবায় মন্ত্রণালয়,বাংলাদেশ সচিবালয়,ঢাকা  <br></p><p>                                                4। জেলা প্রশাসক,নারায়ণগঞ্জ।<br></p>', 0, 1, '2016-11-14 22:53:12', 1, '2016-11-15 04:53:12', NULL),
(83, NULL, 7, 128, NULL, '5673452423', NULL, '2016-11-16 00:00:00', 'পদ্ম সিটি প্লাজা-১', '<p>উপর্যুক্ত বিষয়ে জানানো যাচ্ছে যে, নারায়ণগঞ্জ সিটি কর্পোরেশনাধীন পদ্ম সিটি প্লাজা-১ ১৪২৩ বাংলা সনের ১ বৈশাখ হতে ৩০ চৈত্র পর্যন্ত এক বত্সর মেয়াদে ইজারা গ্রহণের জন্য আপনার প্রস্তাবিত দর ১২০০০/-টাকা সর্বোচ্চ বিবেচিত হওয়ায় তা গ্রেহিত হয়েছে। আপনি দরপত্রের সাথে পে-অর্ডার দ্বারা ৩০০০ টাকা জমা প্রদান করেছেন।</p><p>                               এমতাবস্তায়, পত্র প্রাপ্তির ০৭ (সাত) কার্যদিবসের মধ্যে প্রস্তাবিত মূল্য এর উপর ১৫% ভ্যাট বাবদ ৫০০০/-টাকা, ৫% আয়কর বাবদ ৮৯৬৬/-টাকা এবং ৫% জামানত বাবদ ৫০০/-টাকা সহ সর্বমোট ৫৯৬৩ টাকা সিটি কর্পোরেশন তহবিলে জমা প্রদান করতঃ সিটি কর্পোরেশনের সহিত ৩০০/-টাকা নন জুডিশিয়াল স্ট্যাম্পে চুক্তিপত্র সম্পাদানের জন্য অনুরোধ করা হলো।</p>', '<p>শর্তাবলি ঃ</p><p>                ১। পদ্ম সিটি প্লাজা-১ পরিচালনার ক্ষেত্রে ইজারার বিজ্ঞপ্তির বর্ণিত শর্তাবলি প্রযোজ্য হবে।</p><p>                ২। শুধুমাত্র পদ্ম সিটি প্লাজা-১ হতে টোল আদায় করতে হবে।</p><p>                ৩। এই কার্যাদেশের মেয়াদ ১৬/১১/২০১৬ তারিখ হতে ১৬/১১/২০১৬ তারিখ পর্যন্ত।</p>', '<p>জনাব Abdul Awal</p><p>                        পিতা/স্বামী- Maynuddin</p><p>                        বাসা নং- 52, রাস্তা- 15</p><p>                        থানা- DHUPCHANCHIA, জেলা- BOGRA</p>', '<p>(মোঃ মোস্তফা কামাল মুুজমদার)</p><p>                        (প্রধান নির্বাহী কর্মকর্তা)</p><p>                        (নারায়নগঞ্জ সিটি কর্পোরেশন)</p><p>                        (৭৬৩৩৪২৩)</p><p>                        (ceo@ncc.gov.bd)</p>', '<p>১। হিসাব বিভাগ,নগর ভবন,নারায়ণগঞ্জ সিটি কর্পোরেশন নারায়ণগঞ্জ।<br></p><p>                                        ২। উপ-পরিচালক,স্থানীয় সরকার শাখা,জেলা প্রশাসকের কার্যালয়,নারায়ণগঞ্জ।<br></p><p>                                        ৩। সচিব স্থানীয় সরকার বিভাগ,স্থানীয় সরকার পল্লী উন্নয়ন ও সমবায় মন্ত্রণালয়,বাংলাদেশ সচিবালয়,ঢাকা  <br></p><p>                                        ৪। জেলা প্রশাসক,নারায়ণগঞ্জ।<br></p>', 1, 0, '2016-11-16 00:36:11', 1, '2016-11-16 06:36:11', NULL),
(84, 263, NULL, 128, NULL, '522349848', NULL, '2016-11-16 00:00:00', 'পদ্ম সিটি প্লাজা-১', '<p>উপর্যুক্ত বিষয়ে জানানো যাচ্ছে যে, নারায়ণগঞ্জ সিটি কর্পোরেশনাধীন পদ্ম সিটি প্লাজা-১ ১৪২৩ বাংলা সনের ১ বৈশাখ হতে ৩০ চৈত্র পর্যন্ত এক বত্সর মেয়াদে ইজারা গ্রহণের জন্য আপনার প্রস্তাবিত দর ১২০০০/-টাকা সর্বোচ্চ বিবেচিত হওয়ায় তা গ্রেহিত হয়েছে। আপনি দরপত্রের সাথে পে-অর্ডার দ্বারা ৩০০০ টাকা জমা প্রদান করেছেন।</p><p>                               এমতাবস্তায়, পত্র প্রাপ্তির ০৭ (সাত) কার্যদিবসের মধ্যে প্রস্তাবিত মূল্য এর উপর ১৫% ভ্যাট বাবদ ৫০০০/-টাকা, ৫% আয়কর বাবদ ৮৯৬৬/-টাকা এবং ৫% জামানত বাবদ ৫০০/-টাকা সহ সর্বমোট ৫৯৬৩ টাকা সিটি কর্পোরেশন তহবিলে জমা প্রদান করতঃ সিটি কর্পোরেশনের সহিত ৩০০/-টাকা নন জুডিশিয়াল স্ট্যাম্পে চুক্তিপত্র সম্পাদানের জন্য অনুরোধ করা হলো।</p>', '<p>শর্তাবলি ঃ</p><p>                ১। পদ্ম সিটি প্লাজা-১ পরিচালনার ক্ষেত্রে ইজারার বিজ্ঞপ্তির বর্ণিত শর্তাবলি প্রযোজ্য হবে।</p><p>                ২। শুধুমাত্র পদ্ম সিটি প্লাজা-১ হতে টোল আদায় করতে হবে।</p><p>                ৩। এই কার্যাদেশের মেয়াদ ১৬/১১/২০১৬ তারিখ হতে ১৬/১১/২০১৬ তারিখ পর্যন্ত।</p>', '<p>জনাব Abdul Awal</p><p>                        পিতা/স্বামী- Maynuddin</p><p>                        বাসা নং- 52, রাস্তা- 15</p><p>                        থানা- DHUPCHANCHIA, জেলা- BOGRA</p>', '<p>(মোঃ মোস্তফা কামাল মুুজমদার)</p><p>                        (প্রধান নির্বাহী কর্মকর্তা)</p><p>                        (নারায়নগঞ্জ সিটি কর্পোরেশন)</p><p>                        (৭৬৩৩৪২৩)</p><p>                        (ceo@ncc.gov.bd)</p>', '<p>১। হিসাব বিভাগ,নগর ভবন,নারায়ণগঞ্জ সিটি কর্পোরেশন নারায়ণগঞ্জ।<br></p><p>                                        ২। উপ-পরিচালক,স্থানীয় সরকার শাখা,জেলা প্রশাসকের কার্যালয়,নারায়ণগঞ্জ।<br></p><p>                                        ৩। সচিব স্থানীয় সরকার বিভাগ,স্থানীয় সরকার পল্লী উন্নয়ন ও সমবায় মন্ত্রণালয়,বাংলাদেশ সচিবালয়,ঢাকা  <br></p><p>                                        ৪। জেলা প্রশাসক,নারায়ণগঞ্জ।<br></p>', 1, 0, '2016-11-16 00:58:46', 1, '2016-11-16 06:58:46', NULL),
(85, NULL, 7, 128, NULL, '546474', NULL, '2016-11-21 00:00:00', 'পদ্ম সিটি প্লাজা-১', '<p>উপর্যুক্ত বিষয়ে জানানো যাচ্ছে যে, নারায়ণগঞ্জ সিটি কর্পোরেশনাধীন পদ্ম সিটি প্লাজা-১ ১৪২৩ বাংলা সনের ১ বৈশাখ হতে ৩০ চৈত্র পর্যন্ত এক বত্সর মেয়াদে ইজারা গ্রহণের জন্য আপনার প্রস্তাবিত দর ১২০০০/-টাকা সর্বোচ্চ বিবেচিত হওয়ায় তা গ্রেহিত হয়েছে। আপনি দরপত্রের সাথে পে-অর্ডার দ্বারা ৩০০০ টাকা জমা প্রদান করেছেন।</p><p>                               এমতাবস্তায়, পত্র প্রাপ্তির ০৭ (সাত) কার্যদিবসের মধ্যে প্রস্তাবিত মূল্য এর উপর ১৫% ভ্যাট বাবদ ৫০০০/-টাকা, ৫% আয়কর বাবদ ৮৯৬৬/-টাকা এবং ৫% জামানত বাবদ ৫০০/-টাকা সহ সর্বমোট ৫৯৬৩ টাকা সিটি কর্পোরেশন তহবিলে জমা প্রদান করতঃ সিটি কর্পোরেশনের সহিত ৩০০/-টাকা নন জুডিশিয়াল স্ট্যাম্পে চুক্তিপত্র সম্পাদানের জন্য অনুরোধ করা হলো।</p>', '<p>শর্তাবলি ঃ</p><p>                ১। পদ্ম সিটি প্লাজা-১ পরিচালনার ক্ষেত্রে ইজারার বিজ্ঞপ্তির বর্ণিত শর্তাবলি প্রযোজ্য হবে।</p><p>                ২। শুধুমাত্র পদ্ম সিটি প্লাজা-১ হতে টোল আদায় করতে হবে।</p><p>                ৩। এই কার্যাদেশের মেয়াদ ২১/১১/২০১৬ তারিখ হতে ২১/১১/২০১৬ তারিখ পর্যন্ত।</p>', '<p>জনাব Abdul Awal</p><p>                        পিতা/স্বামী- Maynuddin</p><p>                        বাসা নং- 52, রাস্তা- 15</p><p>                        থানা- DHUPCHANCHIA, জেলা- BOGRA</p>', '<p>(মোঃ মোস্তফা কামাল মুুজমদার)</p><p>                        (প্রধান নির্বাহী কর্মকর্তা)</p><p>                        (নারায়নগঞ্জ সিটি কর্পোরেশন)</p><p>                        (৭৬৩৩৪২৩)</p><p>                        (ceo@ncc.gov.bd)</p>', '<p>১। হিসাব বিভাগ,নগর ভবন,নারায়ণগঞ্জ সিটি কর্পোরেশন নারায়ণগঞ্জ।<br></p><p>                                        ২। উপ-পরিচালক,স্থানীয় সরকার শাখা,জেলা প্রশাসকের কার্যালয়,নারায়ণগঞ্জ।<br></p><p>                                        ৩। সচিব স্থানীয় সরকার বিভাগ,স্থানীয় সরকার পল্লী উন্নয়ন ও সমবায় মন্ত্রণালয়,বাংলাদেশ সচিবালয়,ঢাকা  <br></p><p>                                        ৪। জেলা প্রশাসক,নারায়ণগঞ্জ।<br></p>', 1, 0, '2016-11-21 06:34:26', 1, '2016-11-21 12:34:26', NULL),
(86, 265, NULL, 137, NULL, '৪৬.৪৪৬৭০০.১৪.১৮(৮).১৬/', 'গত 23-11-2016 তারিখে আপনার দাখিলকৃত দরপত্র।', '2016-11-23 00:00:00', 'পদ্ম সিটি প্লাজা-১', '<p>উপর্যুক্ত বিষয়ে জানানো যাচ্ছে যে, নারায়ণগঞ্জ সিটি কর্পোরেশনাধীন পদ্ম সিটি প্লাজা-১ ১৪২৩ বাংলা সনের ১ বৈশাখ হতে ৩০ চৈত্র পর্যন্ত এক বত্সর মেয়াদে ইজারা গ্রহণের জন্য আপনার প্রস্তাবিত দর ১২০০০/-টাকা সর্বোচ্চ বিবেচিত হওয়ায় তা গ্রেহিত হয়েছে। আপনি দরপত্রের সাথে পে-অর্ডার দ্বারা ৩৩৬০ টাকা জমা প্রদান করেছেন।</p><p>                               এমতাবস্তায়, পত্র প্রাপ্তির ০৭ (সাত) কার্যদিবসের মধ্যে প্রস্তাবিত মূল্য এর উপর ১৫% ভ্যাট বাবদ ৫০০০/-টাকা, ৫% আয়কর বাবদ ৮৯৬৬/-টাকা এবং ৫% জামানত বাবদ ৫০০/-টাকা সহ সর্বমোট ৫৯৬৩ টাকা সিটি কর্পোরেশন তহবিলে জমা প্রদান করতঃ সিটি কর্পোরেশনের সহিত ৩০০/-টাকা নন জুডিশিয়াল স্ট্যাম্পে চুক্তিপত্র সম্পাদানের জন্য অনুরোধ করা হলো।</p>', NULL, '<p>জনাব Abdul Awal</p><p>                                পিতা/স্বামী- Maynuddin</p><p>                                বাসা নং- 221, রাস্তা- 12</p><p>                                থানা- ATPARA, জেলা- NETRAKONA</p>', '<p>(মোঃ মোস্তফা কামাল মুুজমদার)</p><p>                                (প্রধান নির্বাহী কর্মকর্তা)</p><p>                                (নারায়নগঞ্জ সিটি কর্পোরেশন)</p><p>                                (৭৬৩৩৪২৩)</p><p>                                (ceo@ncc.gov.bd)</p>', '<p>১। হিসাব বিভাগ,নগর ভবন,নারায়ণগঞ্জ সিটি কর্পোরেশন নারায়ণগঞ্জ।<br></p><p>                                                        ২। উপ-পরিচালক,স্থানীয় সরকার শাখা,জেলা প্রশাসকের কার্যালয়,নারায়ণগঞ্জ।<br></p><p>                                                        ৩। সচিব স্থানীয় সরকার বিভাগ,স্থানীয় সরকার পল্লী উন্নয়ন ও সমবায় মন্ত্রণালয়,বাংলাদেশ সচিবালয়,ঢাকা  <br></p><p>                                                        ৪। জেলা প্রশাসক,নারায়ণগঞ্জ।<br></p>', 0, 0, '2016-11-23 06:02:26', 0, '2016-11-23 12:02:26', NULL);
INSERT INTO `tender_latter` (`TENDER_LETTER_ID`, `TE_APP_ID`, `CITIZEN_ID`, `SCHEDULE_ID`, `PARTICULAR_ID`, `LATTER_NUMBER`, `LETTER_SOURCE`, `DATE`, `SUBJECT`, `BODY`, `CONDITIONS`, `APPLICANT_INFO`, `OFFICER_INFO`, `DISTRIBUTION`, `TYPE`, `IS_LEASE`, `CREATED_AT`, `CREATED_BY`, `UPDATED_AT`, `UPDATED_BY`) VALUES
(87, NULL, 264, 124, NULL, '৪৬.৪৪৬৭০০.১৮(৮).১৬/', NULL, '2016-11-23 00:00:00', 'গোদনাইল হাট বাজার', '<p>উপর্যুক্ত বিষয়ে জানানো যাচ্ছে যে, নারায়ণগঞ্জ সিটি কর্পোরেশনাধীন গোদনাইল হাট বাজার ১৪২৩ বাংলা সনের ১ বৈশাখ হতে ৩০ চৈত্র পর্যন্ত এক বত্সর মেয়াদে ইজারা গ্রহণের জন্য আপনার প্রস্তাবিত দর ৫৫০৫০০/-টাকা সর্বোচ্চ বিবেচিত হওয়ায় তা গ্রেহিত হয়েছে। আপনি দরপত্রের সাথে পে-অর্ডার দ্বারা ৫৫০৫০০ টাকা জমা প্রদান করেছেন।</p><p>                               এমতাবস্তায়, পত্র প্রাপ্তির ০৭ (সাত) কার্যদিবসের মধ্যে প্রস্তাবিত মূল্য এর উপর ১৫% ভ্যাট বাবদ ৫০০০/-টাকা, ৫% আয়কর বাবদ ৮৯৬৬/-টাকা এবং ৫% জামানত বাবদ ৫০০/-টাকা সহ সর্বমোট ৫৯৬৩ টাকা সিটি কর্পোরেশন তহবিলে জমা প্রদান করতঃ সিটি কর্পোরেশনের সহিত ৩০০/-টাকা নন জুডিশিয়াল স্ট্যাম্পে চুক্তিপত্র সম্পাদানের জন্য অনুরোধ করা হলো।</p>', '<p>শর্তাবলি ঃ</p><p>                ১। গোদনাইল হাট বাজার পরিচালনার ক্ষেত্রে ইজারার বিজ্ঞপ্তির বর্ণিত শর্তাবলি প্রযোজ্য হবে।</p><p>                ২। শুধুমাত্র গোদনাইল হাট বাজার হতে টোল আদায় করতে হবে।</p><p><span id="selection-marker-1" class="redactor-selection-marker"></span>                ৩। এই কার্যাদেশের মেয়াদ ২৩/১১/২০১৬ তারিখ হতে ২৩/১১/২০১৬ তারিখ পর্যন্ত।</p>', '<p>জনাব মোঃ নুরুল্লাহ</p><p>                        পিতা/স্বামী- মোঃ আঃ হাকিম</p><p>                        বাসা নং- পাটুঃ-৫৮৯, রাস্তা- নাই</p><p>                        থানা- PAKUNDIA, জেলা- KISHOREGANJ</p>', '<p>(মোঃ মোস্তফা কামাল মুুজমদার)</p><p>                        (প্রধান নির্বাহী কর্মকর্তা)</p><p>                        (নারায়নগঞ্জ সিটি কর্পোরেশন)</p><p>                        (৭৬৩৩৪২৩)</p><p>                        (ceo@ncc.gov.bd)</p>', '<p>১। হিসাব বিভাগ,নগর ভবন,নারায়ণগঞ্জ সিটি কর্পোরেশন নারায়ণগঞ্জ।<br></p><p>                                        ২। উপ-পরিচালক,স্থানীয় সরকার শাখা,জেলা প্রশাসকের কার্যালয়,নারায়ণগঞ্জ।<br></p><p>                                        ৩। সচিব স্থানীয় সরকার বিভাগ,স্থানীয় সরকার পল্লী উন্নয়ন ও সমবায় মন্ত্রণালয়,বাংলাদেশ সচিবালয়,ঢাকা  <br></p><p>                                        ৪। জেলা প্রশাসক,নারায়ণগঞ্জ।<br></p>', 1, 0, '2016-11-23 06:12:11', 7, '2016-11-23 12:12:11', NULL),
(88, NULL, 264, 124, NULL, '৪৬.৪৪৬৭০.১৮(৮).১৬/', NULL, '2016-11-23 00:00:00', 'গোদনাইল হাট বাজার', '<p>উপর্যুক্ত বিষয়ে জানানো যাচ্ছে যে, নারায়ণগঞ্জ সিটি কর্পোরেশনাধীন গোদনাইল হাট বাজার ১৪২৩ বাংলা সনের ১ বৈশাখ হতে ৩০ চৈত্র পর্যন্ত এক বত্সর মেয়াদে ইজারা গ্রহণের জন্য আপনার প্রস্তাবিত দর ৫৫০৫০০/-টাকা সর্বোচ্চ বিবেচিত হওয়ায় তা গ্রেহিত হয়েছে। আপনি দরপত্রের সাথে পে-অর্ডার দ্বারা ৫৫০৫০০ টাকা জমা প্রদান করেছেন।</p><p>                               এমতাবস্তায়, পত্র প্রাপ্তির ০৭ (সাত) কার্যদিবসের মধ্যে প্রস্তাবিত মূল্য এর উপর ১৫% ভ্যাট বাবদ ৫০০০/-টাকা, ৫% আয়কর বাবদ ৮৯৬৬/-টাকা এবং ৫% জামানত বাবদ ৫০০/-টাকা সহ সর্বমোট ৫৯৬৩ টাকা সিটি কর্পোরেশন তহবিলে জমা প্রদান করতঃ সিটি কর্পোরেশনের সহিত ৩০০/-টাকা নন জুডিশিয়াল স্ট্যাম্পে চুক্তিপত্র সম্পাদানের জন্য অনুরোধ করা হলো।</p>', '<p>শর্তাবলি ঃ</p><p>                ১। গোদনাইল হাট বাজার পরিচালনার ক্ষেত্রে ইজারার বিজ্ঞপ্তির বর্ণিত শর্তাবলি প্রযোজ্য হবে।</p><p>                ২। শুধুমাত্র গোদনাইল হাট বাজার হতে টোল আদায় করতে হবে।</p><p><span id="selection-marker-1" class="redactor-selection-marker"></span>                ৩। এই কার্যাদেশের মেয়াদ ২৩/১১/২০১৬ তারিখ হতে ২৩/১১/২০১৬ তারিখ পর্যন্ত।</p>', '<p>জনাব মোঃ নুরুল্লাহ</p><p>                        পিতা/স্বামী- মোঃ আঃ হাকিম</p><p>                        বাসা নং- পাটুঃ-৫৮৯, রাস্তা- নাই</p><p>                        থানা- PAKUNDIA, জেলা- KISHOREGANJ</p>', '<p>(মোঃ মোস্তফা কামাল মুুজমদার)</p><p>                        (প্রধান নির্বাহী কর্মকর্তা)</p><p>                        (নারায়নগঞ্জ সিটি কর্পোরেশন)</p><p>                        (৭৬৩৩৪২৩)</p><p>                        (ceo@ncc.gov.bd)</p>', '<p>১। হিসাব বিভাগ,নগর ভবন,নারায়ণগঞ্জ সিটি কর্পোরেশন নারায়ণগঞ্জ।<br></p><p>                                        ২। উপ-পরিচালক,স্থানীয় সরকার শাখা,জেলা প্রশাসকের কার্যালয়,নারায়ণগঞ্জ।<br></p><p>                                        ৩। সচিব স্থানীয় সরকার বিভাগ,স্থানীয় সরকার পল্লী উন্নয়ন ও সমবায় মন্ত্রণালয়,বাংলাদেশ সচিবালয়,ঢাকা  <br></p><p>                                        ৪। জেলা প্রশাসক,নারায়ণগঞ্জ।<br></p>', 1, 0, '2016-11-23 06:14:51', 7, '2016-11-23 12:14:51', NULL),
(89, NULL, 7, 141, NULL, '125222', 'গত 30-11-2016 তারিখে আপনার দাখিলকৃত দরপত্র।', '2016-11-30 00:00:00', 'পদ্ম সিটি প্লাজা-১', '<p>উপর্যুক্ত বিষয়ে জানানো যাচ্ছে যে, নারায়ণগঞ্জ সিটি কর্পোরেশনাধীন পদ্ম সিটি প্লাজা-১ ১৪২৩ বাংলা সনের ১ বৈশাখ হতে ৩০ চৈত্র পর্যন্ত এক বত্সর মেয়াদে ইজারা গ্রহণের জন্য আপনার প্রস্তাবিত দর ১২০০০/-টাকা সর্বোচ্চ বিবেচিত হওয়ায় তা গ্রেহিত হয়েছে। আপনি দরপত্রের সাথে পে-অর্ডার দ্বারা ৩০০০ টাকা জমা প্রদান করেছেন।</p><p>                               এমতাবস্তায়, পত্র প্রাপ্তির ০৭ (সাত) কার্যদিবসের মধ্যে প্রস্তাবিত মূল্য এর উপর ১৫% ভ্যাট বাবদ ৫০০০/-টাকা, ৫% আয়কর বাবদ ৮৯৬৬/-টাকা এবং ৫% জামানত বাবদ ৫০০/-টাকা সহ সর্বমোট ৫৯৬৩ টাকা সিটি কর্পোরেশন তহবিলে জমা প্রদান করতঃ সিটি কর্পোরেশনের সহিত ৩০০/-টাকা নন জুডিশিয়াল স্ট্যাম্পে চুক্তিপত্র সম্পাদানের জন্য অনুরোধ করা হলো।</p>', NULL, '<p>জনাব Abdul Awal</p><p>                                পিতা/স্বামী- Maynuddin</p><p>                                বাসা নং- 52, রাস্তা- 15</p><p>                                থানা- DHUPCHANCHIA, জেলা- BOGRA</p>', '<p>(মোঃ মোস্তফা কামাল মুুজমদার)</p><p>                                (প্রধান নির্বাহী কর্মকর্তা)</p><p>                                (নারায়নগঞ্জ সিটি কর্পোরেশন)</p><p>                                (৭৬৩৩৪২৩)</p><p>                                (ceo@ncc.gov.bd)</p>', '<p>১। হিসাব বিভাগ,নগর ভবন,নারায়ণগঞ্জ সিটি কর্পোরেশন নারায়ণগঞ্জ।<br></p><p>                                                        ২। উপ-পরিচালক,স্থানীয় সরকার শাখা,জেলা প্রশাসকের কার্যালয়,নারায়ণগঞ্জ।<br></p><p>                                                        ৩। সচিব স্থানীয় সরকার বিভাগ,স্থানীয় সরকার পল্লী উন্নয়ন ও সমবায় মন্ত্রণালয়,বাংলাদেশ সচিবালয়,ঢাকা  <br></p><p>                                                        ৪। জেলা প্রশাসক,নারায়ণগঞ্জ।<br></p>', 0, 0, '2016-11-30 02:16:49', 0, '2016-11-30 08:16:49', NULL),
(90, 267, NULL, 139, NULL, 'এনসিসি/বাজার/ইজারা/৯৯/(অনশ)/১৩5', 'গত 07-12-2016 তারিখে আপনার দাখিলকৃত দরপত্র।', '2016-12-07 00:00:00', 'পদ্ম সিটি প্লাজা-১jhl', '<p>উপর্যুক্ত বিষয়ে জানানো যাচ্ছে যে, নারায়ণগঞ্জ সিটি কর্পোরেশনাধীন পদ্ম সিটি প্লাজা-১ ১৪২৩ বাংলা সনের ১ বৈশাখ হতে ৩০ চৈত্র পর্যন্ত এক বত্সর মেয়াদে ইজারা গ্রহণের জন্য আপনার প্রস্তাবিত দর ১২০০০/-টাকা সর্বোচ্চ বিবেচিত হওয়ায় তা গ্রেহিত হয়েছে। আপনি দরপত্রের সাথে পে-অর্ডার দ্বারা ৩৬০০ টাকা জমা প্রদান করেছেন।</p><p>                               এমতাবস্তায়, পত্র প্রাপ্তির ০৭ (সাত) কার্যদিবসের মধ্যে প্রস্তাবিত মূল্য এর উপর ১৫% ভ্যাট বাবদ ৫০০০/-টাকা, ৫% আয়কর বাবদ ৮৯৬৬/-টাকা এবং ৫% জামানত বাবদ ৫০০/-টাকা সহ সর্বমোট ৫৯৬৩ টাকা সিটি কর্পোরেশন তহবিলে জমা প্রদান করতঃ সিটি কর্পোরেশনের সহিত ৩০০/-টাকা নন জুডিশিয়াল স্ট্যাম্পে চুক্তিপত্র সম্পাদানের জন্য অনুরোধ করা হলো।</p>', NULL, '<p>জনাব মোঃ নুরুল্লাহ</p><p>                                পিতা/স্বামী- মোঃ আঃ রহিম</p><p>                                বাসা নং- পাটুঃ-৫৮৯, রাস্তা- f</p><p>                                থানা- DAULAT KHAN, জেলা- BHOLA</p>', '<p>(মোঃ মোস্তফা কামাল মুুজমদার)</p><p>                                (প্রধান নির্বাহী কর্মকর্তা)</p><p>                                (নারায়নগঞ্জ সিটি কর্পোরেশন)</p><p>                                (৭৬৩৩৪২৩)</p><p>                                (ceo@ncc.gov.bd)</p>', '<p>১। হিসাব বিভাগ,নগর ভবন,নারায়ণগঞ্জ সিটি কর্পোরেশন নারায়ণগঞ্জ।<br></p><p>                                                        ২। উপ-পরিচালক,স্থানীয় সরকার শাখা,জেলা প্রশাসকের কার্যালয়,নারায়ণগঞ্জ।<br></p><p>                                                        ৩। সচিব স্থানীয় সরকার বিভাগ,স্থানীয় সরকার পল্লী উন্নয়ন ও সমবায় মন্ত্রণালয়,বাংলাদেশ সচিবালয়,ঢাকা  <br></p><p>                                                        ৪। জেলা প্রশাসক,নারায়ণগঞ্জ।<br></p>', 0, 0, '2016-12-07 00:28:42', 0, '2016-12-07 06:28:42', NULL),
(91, 267, NULL, 139, NULL, 'dfh', 'গত 07-12-2016 তারিখে আপনার দাখিলকৃত দরপত্র।', '2016-12-07 00:00:00', 'পদ্ম সিটি প্লাজা-১g', '<p>উপর্যুক্ত বিষয়ে জানানো যাচ্ছে যে, নারায়ণগঞ্জ সিটি কর্পোরেশনাধীন পদ্ম সিটি প্লাজা-১ ১৪২৩ বাংলা সনের ১ বৈশাখ হতে ৩০ চৈত্র পর্যন্ত এক বত্সর মেয়াদে ইজারা গ্রহণের জন্য আপনার প্রস্তাবিত দর ১২০০০/-টাকা সর্বোচ্চ বিবেচিত হওয়ায় তা গ্রেহিত হয়েছে। আপনি দরপত্রের সাথে পে-অর্ডার দ্বারা ৩৬০০ টাকা জমা প্রদান করেছেন।</p><p>                               এমতাবস্তায়, পত্র প্রাপ্তির ০৭ (সাত) কার্যদিবসের মধ্যে প্রস্তাবিত মূল্য এর উপর ১৫% ভ্যাট বাবদ ৫০০০/-টাকা, ৫% আয়কর বাবদ ৮৯৬৬/-টাকা এবং ৫% জামানত বাবদ ৫০০/-টাকা সহ সর্বমোট ৫৯৬৩ টাকা সিটি কর্পোরেশন তহবিলে জমা প্রদান করতঃ সিটি কর্পোরেশনের সহিত ৩০০/-টাকা নন জুডিশিয়াল স্ট্যাম্পে চুক্তিপত্র সম্পাদানের জন্য অনুরোধ করা হলো।</p>', NULL, '<p>জনাব মোঃ নুরুল্লাহ</p><p>                                পিতা/স্বামী- মোঃ আঃ রহিম</p><p>                                বাসা নং- পাটুঃ-৫৮৯, রাস্তা- f</p><p>                                থানা- DAULAT KHAN, জেলা- BHOLA</p>', '<p>(মোঃ মোস্তফা কামাল মুুজমদার)</p><p>                                (প্রধান নির্বাহী কর্মকর্তা)</p><p>                                (নারায়নগঞ্জ সিটি কর্পোরেশন)</p><p>                                (৭৬৩৩৪২৩)</p><p>                                (ceo@ncc.gov.bd)</p>', '<p>১। হিসাব বিভাগ,নগর ভবন,নারায়ণগঞ্জ সিটি কর্পোরেশন নারায়ণগঞ্জ।<br></p><p>                                                        ২। উপ-পরিচালক,স্থানীয় সরকার শাখা,জেলা প্রশাসকের কার্যালয়,নারায়ণগঞ্জ।<br></p><p>                                                        ৩। সচিব স্থানীয় সরকার বিভাগ,স্থানীয় সরকার পল্লী উন্নয়ন ও সমবায় মন্ত্রণালয়,বাংলাদেশ সচিবালয়,ঢাকা  <br></p><p>                                                        ৪। জেলা প্রশাসক,নারায়ণগঞ্জ।<br></p>', 0, 0, '2016-12-07 00:30:26', 0, '2016-12-07 06:30:26', NULL),
(92, 267, NULL, 139, NULL, 'e', 'গত 07-12-2016 তারিখে আপনার দাখিলকৃত দরপত্র।', '2016-12-07 00:00:00', 'পদ্ম সিটি প্লাজা-১d', '<p>উপর্যুক্ত বিষয়ে জানানো যাচ্ছে যে, নারায়ণগঞ্জ সিটি কর্পোরেশনাধীন পদ্ম সিটি প্লাজা-১ ১৪২৩ বাংলা সনের ১ বৈশাখ হতে ৩০ চৈত্র পর্যন্ত এক বত্সর মেয়াদে ইজারা গ্রহণের জন্য আপনার প্রস্তাবিত দর ১২০০০/-টাকা সর্বোচ্চ বিবেচিত হওয়ায় তা গ্রেহিত হয়েছে। আপনি দরপত্রের সাথে পে-অর্ডার দ্বারা ৩৬০০ টাকা জমা প্রদান করেছেন।</p><p>                               এমতাবস্তায়, পত্র প্রাপ্তির ০৭ (সাত) কার্যদিবসের মধ্যে প্রস্তাবিত মূল্য এর উপর ১৫% ভ্যাট বাবদ ৫০০০/-টাকা, ৫% আয়কর বাবদ ৮৯৬৬/-টাকা এবং ৫% জামানত বাবদ ৫০০/-টাকা সহ সর্বমোট ৫৯৬৩ টাকা সিটি কর্পোরেশন তহবিলে জমা প্রদান করতঃ সিটি কর্পোরেশনের সহিত ৩০০/-টাকা নন জুডিশিয়াল স্ট্যাম্পে চুক্তিপত্র সম্পাদানের জন্য অনুরোধ করা হলো।</p>', NULL, '<p>জনাব মোঃ নুরুল্লাহ</p><p>                                পিতা/স্বামী- মোঃ আঃ রহিম</p><p>                                বাসা নং- পাটুঃ-৫৮৯, রাস্তা- f</p><p>                                থানা- DAULAT KHAN, জেলা- BHOLA</p>', '<p>(মোঃ মোস্তফা কামাল মুুজমদার)</p><p>                                (প্রধান নির্বাহী কর্মকর্তা)</p><p>                                (নারায়নগঞ্জ সিটি কর্পোরেশন)</p><p>                                (৭৬৩৩৪২৩)</p><p>                                (ceo@ncc.gov.bd)</p>', '<p>১। হিসাব বিভাগ,নগর ভবন,নারায়ণগঞ্জ সিটি কর্পোরেশন নারায়ণগঞ্জ।<br></p><p>                                                        ২। উপ-পরিচালক,স্থানীয় সরকার শাখা,জেলা প্রশাসকের কার্যালয়,নারায়ণগঞ্জ।<br></p><p>                                                        ৩। সচিব স্থানীয় সরকার বিভাগ,স্থানীয় সরকার পল্লী উন্নয়ন ও সমবায় মন্ত্রণালয়,বাংলাদেশ সচিবালয়,ঢাকা  <br></p><p>                                                        ৪। জেলা প্রশাসক,নারায়ণগঞ্জ।<br></p>', 0, 0, '2016-12-07 00:36:50', 0, '2016-12-07 06:36:50', NULL),
(93, 267, NULL, 139, NULL, '4445', 'গত 07-12-2016 তারিখে আপনার দাখিলকৃত দরপত্র।', '2016-12-07 00:00:00', 'পদ্ম সিটি প্লাজা-১', '<p>উপর্যুক্ত বিষয়ে জানানো যাচ্ছে যে, নারায়ণগঞ্জ সিটি কর্পোরেশনাধীন পদ্ম সিটি প্লাজা-১ ১৪২৩ বাংলা সনের ১ বৈশাখ হতে ৩০ চৈত্র পর্যন্ত এক বত্সর মেয়াদে ইজারা গ্রহণের জন্য আপনার প্রস্তাবিত দর ১২০০০/-টাকা সর্বোচ্চ বিবেচিত হওয়ায় তা গ্রেহিত হয়েছে। আপনি দরপত্রের সাথে পে-অর্ডার দ্বারা ৩৬০০ টাকা জমা প্রদান করেছেন।</p><p>                               এমতাবস্তায়, পত্র প্রাপ্তির ০৭ (সাত) কার্যদিবসের মধ্যে প্রস্তাবিত মূল্য এর উপর ১৫% ভ্যাট বাবদ ৫০০০/-টাকা, ৫% আয়কর বাবদ ৮৯৬৬/-টাকা এবং ৫% জামানত বাবদ ৫০০/-টাকা সহ সর্বমোট ৫৯৬৩ টাকা সিটি কর্পোরেশন তহবিলে জমা প্রদান করতঃ সিটি কর্পোরেশনের সহিত ৩০০/-টাকা নন জুডিশিয়াল স্ট্যাম্পে চুক্তিপত্র সম্পাদানের জন্য অনুরোধ করা হলো।</p>', NULL, '<p>জনাব মোঃ নুরুল্লাহ</p><p>                                পিতা/স্বামী- মোঃ আঃ রহিম</p><p>                                বাসা নং- পাটুঃ-৫৮৯, রাস্তা- f</p><p>                                থানা- DAULAT KHAN, জেলা- BHOLA</p>', '<p>(মোঃ মোস্তফা কামাল মুুজমদার)</p><p>                                (প্রধান নির্বাহী কর্মকর্তা)</p><p>                                (নারায়নগঞ্জ সিটি কর্পোরেশন)</p><p>                                (৭৬৩৩৪২৩)</p><p>                                (ceo@ncc.gov.bd)</p>', '<p>১। হিসাব বিভাগ,নগর ভবন,নারায়ণগঞ্জ সিটি কর্পোরেশন নারায়ণগঞ্জ।<br></p><p>                                                        ২। উপ-পরিচালক,স্থানীয় সরকার শাখা,জেলা প্রশাসকের কার্যালয়,নারায়ণগঞ্জ।<br></p><p>                                                        ৩। সচিব স্থানীয় সরকার বিভাগ,স্থানীয় সরকার পল্লী উন্নয়ন ও সমবায় মন্ত্রণালয়,বাংলাদেশ সচিবালয়,ঢাকা  <br></p><p>                                                        ৪। জেলা প্রশাসক,নারায়ণগঞ্জ।<br></p>', 0, 0, '2016-12-07 00:44:39', 0, '2016-12-07 06:44:39', NULL),
(94, 268, NULL, 141, NULL, '15759874558', 'গত 12-12-2016 তারিখে আপনার দাখিলকৃত দরপত্র।', '2016-12-12 00:00:00', 'পদ্ম সিটি প্লাজা-১', '<p>উপর্যুক্ত বিষয়ে জানানো যাচ্ছে যে, নারায়ণগঞ্জ সিটি কর্পোরেশনাধীন পদ্ম সিটি প্লাজা-১ ১৪২৩ বাংলা সনের ১ বৈশাখ হতে ৩০ চৈত্র পর্যন্ত এক বত্সর মেয়াদে ইজারা গ্রহণের জন্য আপনার প্রস্তাবিত দর ১২০০০/-টাকা সর্বোচ্চ বিবেচিত হওয়ায় তা গ্রেহিত হয়েছে। আপনি দরপত্রের সাথে পে-অর্ডার দ্বারা ৩০০০ টাকা জমা প্রদান করেছেন।</p><p>                               এমতাবস্তায়, পত্র প্রাপ্তির ০৭ (সাত) কার্যদিবসের মধ্যে প্রস্তাবিত মূল্য এর উপর ১৫% ভ্যাট বাবদ ৫০০০/-টাকা, ৫% আয়কর বাবদ ৮৯৬৬/-টাকা এবং ৫% জামানত বাবদ ৫০০/-টাকা সহ সর্বমোট ৫৯৬৩ টাকা সিটি কর্পোরেশন তহবিলে জমা প্রদান করতঃ সিটি কর্পোরেশনের সহিত ৩০০/-টাকা নন জুডিশিয়াল স্ট্যাম্পে চুক্তিপত্র সম্পাদানের জন্য অনুরোধ করা হলো।</p>', NULL, '<p>জনাব Sharov Khan</p><p>                                পিতা/স্বামী- Mr. x</p><p>                                বাসা নং- 34, রাস্তা- 12</p><p>                                থানা- BRAHMANBARIA SADAR, জেলা- BRAHAMANBARIA</p>', '<p>(মোঃ মোস্তফা কামাল মুুজমদার)</p><p>                                (প্রধান নির্বাহী কর্মকর্তা)</p><p>                                (নারায়নগঞ্জ সিটি কর্পোরেশন)</p><p>                                (৭৬৩৩৪২৩)</p><p>                                (ceo@ncc.gov.bd)</p>', '<p>১। হিসাব বিভাগ,নগর ভবন,নারায়ণগঞ্জ সিটি কর্পোরেশন নারায়ণগঞ্জ।<br></p><p>                                                        ২। উপ-পরিচালক,স্থানীয় সরকার শাখা,জেলা প্রশাসকের কার্যালয়,নারায়ণগঞ্জ।<br></p><p>                                                        ৩। সচিব স্থানীয় সরকার বিভাগ,স্থানীয় সরকার পল্লী উন্নয়ন ও সমবায় মন্ত্রণালয়,বাংলাদেশ সচিবালয়,ঢাকা  <br></p><p>                                                        ৪। জেলা প্রশাসক,নারায়ণগঞ্জ।<br></p>', 0, 0, '2016-12-11 23:53:40', 0, '2016-12-12 05:53:40', NULL),
(95, NULL, 7, 128, NULL, '675675733', NULL, '2016-12-21 00:00:00', 'পদ্ম সিটি প্লাজা-১', '<p>উপর্যুক্ত বিষয়ে জানানো যাচ্ছে যে, নারায়ণগঞ্জ সিটি কর্পোরেশনাধীন পদ্ম সিটি প্লাজা-১ ১৪২৩ বাংলা সনের ১ বৈশাখ হতে ৩০ চৈত্র পর্যন্ত এক বত্সর মেয়াদে ইজারা গ্রহণের জন্য আপনার প্রস্তাবিত দর ১২০০০/-টাকা সর্বোচ্চ বিবেচিত হওয়ায় তা গ্রেহিত হয়েছে। আপনি দরপত্রের সাথে পে-অর্ডার দ্বারা ৩০০০ টাকা জমা প্রদান করেছেন।</p><p>                               এমতাবস্তায়, পত্র প্রাপ্তির ০৭ (সাত) কার্যদিবসের মধ্যে প্রস্তাবিত মূল্য এর উপর ১৫% ভ্যাট বাবদ ৫০০০/-টাকা, ৫% আয়কর বাবদ ৮৯৬৬/-টাকা এবং ৫% জামানত বাবদ ৫০০/-টাকা সহ সর্বমোট ৫৯৬৩ টাকা সিটি কর্পোরেশন তহবিলে জমা প্রদান করতঃ সিটি কর্পোরেশনের সহিত ৩০০/-টাকা নন জুডিশিয়াল স্ট্যাম্পে চুক্তিপত্র সম্পাদানের জন্য অনুরোধ করা হলো।</p>', '<p>শর্তাবলি ঃ</p><p>                ১। পদ্ম সিটি প্লাজা-১ পরিচালনার ক্ষেত্রে ইজারার বিজ্ঞপ্তির বর্ণিত শর্তাবলি প্রযোজ্য হবে।</p><p>                ২। শুধুমাত্র পদ্ম সিটি প্লাজা-১ হতে টোল আদায় করতে হবে।</p><p>                ৩। এই কার্যাদেশের মেয়াদ ২১/১২/২০১৬ তারিখ হতে ২১/১২/২০১৬ তারিখ পর্যন্ত।</p>', '<p>জনাব Abdul Awal</p><p>                        পিতা/স্বামী- Maynuddin</p><p>                        বাসা নং- 52, রাস্তা- 15</p><p>                        থানা- DHUPCHANCHIA, জেলা- BOGRA</p>', '<p>(মোঃ মোস্তফা কামাল মুুজমদার)</p><p>                        (প্রধান নির্বাহী কর্মকর্তা)</p><p>                        (নারায়নগঞ্জ সিটি কর্পোরেশন)</p><p>                        (৭৬৩৩৪২৩)</p><p>                        (ceo@ncc.gov.bd)</p>', '<p>১। হিসাব বিভাগ,নগর ভবন,নারায়ণগঞ্জ সিটি কর্পোরেশন নারায়ণগঞ্জ।<br></p><p>                                        ২। উপ-পরিচালক,স্থানীয় সরকার শাখা,জেলা প্রশাসকের কার্যালয়,নারায়ণগঞ্জ।<br></p><p>                                        ৩। সচিব স্থানীয় সরকার বিভাগ,স্থানীয় সরকার পল্লী উন্নয়ন ও সমবায় মন্ত্রণালয়,বাংলাদেশ সচিবালয়,ঢাকা  <br></p><p>                                        ৪। জেলা প্রশাসক,নারায়ণগঞ্জ।<br></p>', 1, 0, '2016-12-21 00:53:05', 7, '2016-12-21 06:53:05', NULL),
(96, 265, NULL, 136, NULL, '87fu8787df98', 'গত 21-12-2016 তারিখে আপনার দাখিলকৃত দরপত্র।', '2016-12-21 00:00:00', 'ইজারার অবশিষ্ট টাকা পরিশোধ প্রসঙ্গে', '<p>উপর্যুক্ত বিষয়ে জানানো যাচ্ছে যে, নারায়ণগঞ্জ সিটি কর্পোরেশনাধীন গোদনাইল হাট বাজার <span class="start_date">২১-১২-২০১৬</span> তারিখ হতে <span class="end_date">২১-১২-২০১৭</span> তারিখ পর্যন্ত ইজারা গ্রহণের জন্য আপনার প্রস্তাবিত দর ১২,০০০/- টাকা সর্বোচ্চ বিবেচিত হওয়ায় তা গৃহিত হয়েছে। আপনি দরপত্রের সাথে পে-অর্ডার দ্বারা ১২,০০০/- টাকা জমা প্রদান করেছেন।</p><p>এমতাবস্থায়, পত্র প্রাপ্তির ০৭ (সাত) কার্যদিবসের মধ্যে প্রস্তাবিত মূল্যের উপর\r\n                            \r\n                                                                    ৫% জামানত বাবদ ৬০০/- টাকা \r\n                                                                    ১৫% ভ্যাট বাবদ ১,৮০০/- টাকা \r\n                                                                    ৫% আয়কর বাবদ বাবদ ৬০০/- টাকা \r\n                                                            \r\n                            সর্বমোট ৩,০০০/- টাকা সিটি কর্পোরেশন তহবিলে জমা প্রদান করতঃ সিটি কর্পোরেশনের সহিত ৩০০/-টাকা নন জুডিশিয়াল স্ট্যাম্পে চুক্তিপত্র সম্পাদানের জন্য অনুরোধ করা হলো।\r\n                        </p>', NULL, '<p>জনাব Abdul Awal</p><p>                            পিতা/স্বামী- Maynuddin</p><p>                            বাসা নং- 221, রাস্তা- 12</p><p>                            থানা- ATPARA, জেলা- NETRAKONA</p>', '<p>(মোঃ মোস্তফা কামাল মুুজমদার)</p><p>                            (প্রধান নির্বাহী কর্মকর্তা)</p><p>                            (নারায়নগঞ্জ সিটি কর্পোরেশন)</p><p>                            (৭৬৩৩৪২৩)</p><p>                            (ceo@ncc.gov.bd)</p>', '<p>1। হিসাব বিভাগ,নগর ভবন,নারায়ণগঞ্জ সিটি কর্পোরেশন নারায়ণগঞ্জ।<br></p><p>                                                2। উপ-পরিচালক,স্থানীয় সরকার শাখা,জেলা প্রশাসকের কার্যালয়,নারায়ণগঞ্জ।<br></p><p>                                                3। সচিব স্থানীয় সরকার বিভাগ,স্থানীয় সরকার পল্লী উন্নয়ন ও সমবায় মন্ত্রণালয়,বাংলাদেশ সচিবালয়,ঢাকা  <br></p><p>                                                4। জেলা প্রশাসক,নারায়ণগঞ্জ।<br></p>', 0, 1, '2016-12-21 04:39:48', 6, '2016-12-21 10:39:48', NULL),
(97, NULL, 265, 136, NULL, '৫৬৬৪৪৪৩৪৩৪', NULL, '2016-12-21 00:00:00', 'গোদনাইল হাট বাজার ইজারা প্রদানের কার্যাদেশ পত্র', '<p>উপর্যুক্ত বিষয়ে জানানো যাচ্ছে যে, নারায়ণগঞ্জ সিটি কর্পোরেশনের নিয়ন্ত্রণাধীন গোদনাইল হাট বাজার ২১/১২/২০১৬ তারিখ হতে ২১/১২/২০১৭ তারিখ পর্যন্ত ইজারা গ্রহণের জন্য আপনার প্রস্তাবিত দর ১২,০০০/- (বার হাজার টাকা মাত্র) টাকা সর্বোচ্চ বিবেচিত হওয়ায় তা সিটি কর্পোরেশন কর্তৃক গৃহীত হয়। </p><p>                        আপনি ইজারা মুলা মূল্য বাবদ ১২,০০০/- টাকা,                                                                             ৫% জামানত বাবদ ৬০০/- টাকা </p><p>                                                                                                                                                        ১৫% ভ্যাট বাবদ ১,৮০০/- টাকা </p><p>                                                                                                                                                        ৫% আয়কর বাবদ বাবদ ৬০০/- টাকা </p><p>                    সর্বমোট ১৫,০০০/- টাকা পরিশোধ করায় ২১/১২/২০১৬ তারিখ হতে ২১/১২/২০১৭ তারিখ পর্যন্ত বর্ণিত গোদনাইল হাট বাজার হতে টোল আদায় করার কার্যাদেশ প্রদান করা হলো।</p>', '<p>শর্তাবলি ঃ</p><p>                ১। গোদনাইল হাট বাজার পরিচালনার ক্ষেত্রে ইজারার বিজ্ঞপ্তির বর্ণিত শর্তাবলি প্রযোজ্য হবে।</p><p>                ২। শুধুমাত্র গোদনাইল হাট বাজার হতে টোল আদায় করতে হবে।</p><p>                ৩। এই কার্যাদেশের মেয়াদ ২১/১২/২০১৬ তারিখ হতে ২১/১২/২০১৭ তারিখ পর্যন্ত।</p>', '<p>জনাব Abdul Awal</p><p>                            পিতা/স্বামী- Maynuddin</p><p>                            বাসা নং- 221, রাস্তা- 12</p><p>                            থানা- ATPARA, জেলা- NETRAKONA</p>', '<p>(মোঃ মোস্তফা কামাল মুুজমদার)</p><p>                            (প্রধান নির্বাহী কর্মকর্তা)</p><p>                            (নারায়নগঞ্জ সিটি কর্পোরেশন)</p><p>                            (৭৬৩৩৪২৩)</p><p>                            (ceo@ncc.gov.bd)</p>', '<p>১। হিসাব বিভাগ,নগর ভবন,নারায়ণগঞ্জ সিটি কর্পোরেশন নারায়ণগঞ্জ।<br></p><p>                                                ২। উপ-পরিচালক,স্থানীয় সরকার শাখা,জেলা প্রশাসকের কার্যালয়,নারায়ণগঞ্জ।<br></p><p>                                                ৩। সচিব স্থানীয় সরকার বিভাগ,স্থানীয় সরকার পল্লী উন্নয়ন ও সমবায় মন্ত্রণালয়,বাংলাদেশ সচিবালয়,ঢাকা  <br></p><p>                                                ৪। জেলা প্রশাসক,নারায়ণগঞ্জ।<br></p>', 1, 1, '2016-12-21 05:05:26', 6, '2016-12-21 11:05:26', NULL),
(98, NULL, 7, 124, NULL, 'rtt6465656', NULL, '2016-12-21 00:00:00', 'গোদনাইল হাট বাজার ইজারা প্রদানের কার্যাদেশ পত্র', '<p>উপর্যুক্ত বিষয়ে জানানো যাচ্ছে যে, নারায়ণগঞ্জ সিটি কর্পোরেশনের নিয়ন্ত্রণাধীন গোদনাইল হাট বাজার ১৬/১১/২০১৬ তারিখ হতে ১৭/১১/২০১৬ তারিখ পর্যন্ত ইজারা গ্রহণের জন্য আপনার প্রস্তাবিত দর ১২,০০০/- (বার হাজার টাকা মাত্র) টাকা সর্বোচ্চ বিবেচিত হওয়ায় তা সিটি কর্পোরেশন কর্তৃক গৃহীত হয়। </p><p>                        আপনি ইজারা মুলা মূল্য বাবদ ১২,০০০/- টাকা,                                                                             ৫% জামানত বাবদ ৬০০/- টাকা </p><p>                                                                                                                                                        ১৫% ভ্যাট বাবদ ১,৮০০/- টাকা </p><p>                                                                                                                                                        ৫% আয়কর বাবদ বাবদ ৬০০/- টাকা </p><p>                    সর্বমোট ১৫,০০০/- টাকা পরিশোধ করায় ১৬/১১/২০১৬ তারিখ হতে ১৭/১১/২০১৬ তারিখ পর্যন্ত বর্ণিত গোদনাইল হাট বাজার হতে টোল আদায় করার কার্যাদেশ প্রদান করা হলো।</p>', '<p>শর্তাবলি ঃ</p><p>                ১। গোদনাইল হাট বাজার পরিচালনার ক্ষেত্রে ইজারার বিজ্ঞপ্তির বর্ণিত শর্তাবলি প্রযোজ্য হবে।</p><p>                ২। শুধুমাত্র গোদনাইল হাট বাজার হতে টোল আদায় করতে হবে।</p><p>                ৩। এই কার্যাদেশের মেয়াদ ১৬/১১/২০১৬ তারিখ হতে ১৭/১১/২০১৬ তারিখ পর্যন্ত।</p>', '<p>জনাব Abdul Awal</p><p>                            পিতা/স্বামী- Maynuddin</p><p>                            বাসা নং- 52, রাস্তা- 15</p><p>                            থানা- DHUPCHANCHIA, জেলা- BOGRA</p>', '<p>(মোঃ মোস্তফা কামাল মুুজমদার)</p><p>                            (প্রধান নির্বাহী কর্মকর্তা)</p><p>                            (নারায়নগঞ্জ সিটি কর্পোরেশন)</p><p>                            (৭৬৩৩৪২৩)</p><p>                            (ceo@ncc.gov.bd)</p>', '<p>১। হিসাব বিভাগ,নগর ভবন,নারায়ণগঞ্জ সিটি কর্পোরেশন নারায়ণগঞ্জ।<br></p><p>                                                ২। উপ-পরিচালক,স্থানীয় সরকার শাখা,জেলা প্রশাসকের কার্যালয়,নারায়ণগঞ্জ।<br></p><p>                                                ৩। সচিব স্থানীয় সরকার বিভাগ,স্থানীয় সরকার পল্লী উন্নয়ন ও সমবায় মন্ত্রণালয়,বাংলাদেশ সচিবালয়,ঢাকা  <br></p><p>                                                ৪। জেলা প্রশাসক,নারায়ণগঞ্জ।<br></p>', 1, 1, '2016-12-21 05:10:56', 6, '2016-12-21 11:10:56', NULL);
INSERT INTO `tender_latter` (`TENDER_LETTER_ID`, `TE_APP_ID`, `CITIZEN_ID`, `SCHEDULE_ID`, `PARTICULAR_ID`, `LATTER_NUMBER`, `LETTER_SOURCE`, `DATE`, `SUBJECT`, `BODY`, `CONDITIONS`, `APPLICANT_INFO`, `OFFICER_INFO`, `DISTRIBUTION`, `TYPE`, `IS_LEASE`, `CREATED_AT`, `CREATED_BY`, `UPDATED_AT`, `UPDATED_BY`) VALUES
(99, 269, NULL, 144, NULL, '635463546', 'গত 26-12-2016 তারিখে আপনার দাখিলকৃত দরপত্র।', '2016-12-26 00:00:00', 'পদ্ম সিটি প্লাজা-১', '<p>উপর্যুক্ত বিষয়ে জানানো যাচ্ছে যে, নারায়ণগঞ্জ সিটি কর্পোরেশনাধীন পদ্ম সিটি প্লাজা-১ ১৪২৩ বাংলা সনের ১ বৈশাখ হতে ৩০ চৈত্র পর্যন্ত এক বত্সর মেয়াদে ইজারা গ্রহণের জন্য আপনার প্রস্তাবিত দর ১০০০০০০০/-টাকা সর্বোচ্চ বিবেচিত হওয়ায় তা গ্রেহিত হয়েছে। আপনি দরপত্রের সাথে পে-অর্ডার দ্বারা ৩০০০০০০ টাকা জমা প্রদান করেছেন।</p><p>                               এমতাবস্তায়, পত্র প্রাপ্তির ০৭ (সাত) কার্যদিবসের মধ্যে প্রস্তাবিত মূল্য এর উপর ১৫% ভ্যাট বাবদ ৫০০০/-টাকা, ৫% আয়কর বাবদ ৮৯৬৬/-টাকা এবং ৫% জামানত বাবদ ৫০০/-টাকা সহ সর্বমোট ৫৯৬৩ টাকা সিটি কর্পোরেশন তহবিলে জমা প্রদান করতঃ সিটি কর্পোরেশনের সহিত ৩০০/-টাকা নন জুডিশিয়াল স্ট্যাম্পে চুক্তিপত্র সম্পাদানের জন্য অনুরোধ করা হলো।</p>', NULL, '<p>জনাব Mehediv Hasan</p><p>                                পিতা/স্বামী- Late Abdul Khaleque</p><p>                                বাসা নং- 317, রাস্তা- 04</p><p>                                থানা- KHILGAON, জেলা- DHAKA</p>', '<p>(মোঃ মোস্তফা কামাল মুুজমদার)</p><p>                                (প্রধান নির্বাহী কর্মকর্তা)</p><p>                                (নারায়নগঞ্জ সিটি কর্পোরেশন)</p><p>                                (৭৬৩৩৪২৩)</p><p>                                (ceo@ncc.gov.bd)</p>', '<p>১। হিসাব বিভাগ,নগর ভবন,নারায়ণগঞ্জ সিটি কর্পোরেশন নারায়ণগঞ্জ।<br></p><p>                                                        ২। উপ-পরিচালক,স্থানীয় সরকার শাখা,জেলা প্রশাসকের কার্যালয়,নারায়ণগঞ্জ।<br></p><p>                                                        ৩। সচিব স্থানীয় সরকার বিভাগ,স্থানীয় সরকার পল্লী উন্নয়ন ও সমবায় মন্ত্রণালয়,বাংলাদেশ সচিবালয়,ঢাকা  <br></p><p>                                                        ৪। জেলা প্রশাসক,নারায়ণগঞ্জ।<br></p>', 0, 0, '2016-12-26 06:43:15', 0, '2016-12-26 12:43:15', NULL),
(100, 277, NULL, 139, NULL, '2334', 'গত 11-04-2017 তারিখে আপনার দাখিলকৃত দরপত্র।', '2017-04-11 00:00:00', 'পদ্ম সিটি প্লাজা-১', '<p>উপর্যুক্ত বিষয়ে জানানো যাচ্ছে যে, নারায়ণগঞ্জ সিটি কর্পোরেশনাধীন পদ্ম সিটি প্লাজা-১ ১৪২৩ বাংলা সনের ১ বৈশাখ হতে ৩০ চৈত্র পর্যন্ত এক বত্সর মেয়াদে ইজারা গ্রহণের জন্য আপনার প্রস্তাবিত দর ১২০০০/-টাকা সর্বোচ্চ বিবেচিত হওয়ায় তা গ্রেহিত হয়েছে। আপনি দরপত্রের সাথে পে-অর্ডার দ্বারা ৩৬০০ টাকা জমা প্রদান করেছেন।</p><p>                               এমতাবস্তায়, পত্র প্রাপ্তির ০৭ (সাত) কার্যদিবসের মধ্যে প্রস্তাবিত মূল্য এর উপর ১৫% ভ্যাট বাবদ ৫০০০/-টাকা, ৫% আয়কর বাবদ ৮৯৬৬/-টাকা এবং ৫% জামানত বাবদ ৫০০/-টাকা সহ সর্বমোট ৫৯৬৩ টাকা সিটি কর্পোরেশন তহবিলে জমা প্রদান করতঃ সিটি কর্পোরেশনের সহিত ৩০০/-টাকা নন জুডিশিয়াল স্ট্যাম্পে চুক্তিপত্র সম্পাদানের জন্য অনুরোধ করা হলো।</p>', NULL, '<p>জনাব Biplob Hossign</p><p>                                পিতা/স্বামী- Abul Kalam</p><p>                                বাসা নং- 34, রাস্তা- 3</p><p>                                থানা- ISHWARGANJ, জেলা- MYMENSINGH</p>', '<p>(মোঃ মোস্তফা কামাল মুুজমদার)</p><p>                                (প্রধান নির্বাহী কর্মকর্তা)</p><p>                                (নারায়নগঞ্জ সিটি কর্পোরেশন)</p><p>                                (৭৬৩৩৪২৩)</p><p>                                (ceo@ncc.gov.bd)</p>', '<p>১। হিসাব বিভাগ,নগর ভবন,নারায়ণগঞ্জ সিটি কর্পোরেশন নারায়ণগঞ্জ।<br></p><p>                                                        ২। উপ-পরিচালক,স্থানীয় সরকার শাখা,জেলা প্রশাসকের কার্যালয়,নারায়ণগঞ্জ।<br></p><p>                                                        ৩। সচিব স্থানীয় সরকার বিভাগ,স্থানীয় সরকার পল্লী উন্নয়ন ও সমবায় মন্ত্রণালয়,বাংলাদেশ সচিবালয়,ঢাকা  <br></p><p>                                                        ৪। জেলা প্রশাসক,নারায়ণগঞ্জ।<br></p>', 0, 0, '2017-04-11 11:55:36', 0, '2017-04-11 17:55:36', NULL),
(101, NULL, 7, 128, NULL, '4545', NULL, '2017-04-11 00:00:00', 'পদ্ম সিটি প্লাজা-১', '<p>উপর্যুক্ত বিষয়ে জানানো যাচ্ছে যে, নারায়ণগঞ্জ সিটি কর্পোরেশনাধীন পদ্ম সিটি প্লাজা-১ ১৪২৩ বাংলা সনের ১ বৈশাখ হতে ৩০ চৈত্র পর্যন্ত এক বত্সর মেয়াদে ইজারা গ্রহণের জন্য আপনার প্রস্তাবিত দর ১২০০০/-টাকা সর্বোচ্চ বিবেচিত হওয়ায় তা গ্রেহিত হয়েছে। আপনি দরপত্রের সাথে পে-অর্ডার দ্বারা ৩০০০ টাকা জমা প্রদান করেছেন।</p><p>                               এমতাবস্তায়, পত্র প্রাপ্তির ০৭ (সাত) কার্যদিবসের মধ্যে প্রস্তাবিত মূল্য এর উপর ১৫% ভ্যাট বাবদ ৫০০০/-টাকা, ৫% আয়কর বাবদ ৮৯৬৬/-টাকা এবং ৫% জামানত বাবদ ৫০০/-টাকা সহ সর্বমোট ৫৯৬৩ টাকা সিটি কর্পোরেশন তহবিলে জমা প্রদান করতঃ সিটি কর্পোরেশনের সহিত ৩০০/-টাকা নন জুডিশিয়াল স্ট্যাম্পে চুক্তিপত্র সম্পাদানের জন্য অনুরোধ করা হলো।</p>', '<p>শর্তাবলি ঃ</p><p>                ১। পদ্ম সিটি প্লাজা-১ পরিচালনার ক্ষেত্রে ইজারার বিজ্ঞপ্তির বর্ণিত শর্তাবলি প্রযোজ্য হবে।</p><p>                ২। শুধুমাত্র পদ্ম সিটি প্লাজা-১ হতে টোল আদায় করতে হবে।</p><p>                ৩। এই কার্যাদেশের মেয়াদ ১১/০৪/২০১৭ তারিখ হতে ১১/০৪/২০১৭ তারিখ পর্যন্ত।</p>', '<p>জনাব Abdul Awal</p><p>                        পিতা/স্বামী- Maynuddin</p><p>                        বাসা নং- 52, রাস্তা- 15</p><p>                        থানা- DHUPCHANCHIA, জেলা- BOGRA</p>', '<p>(মোঃ মোস্তফা কামাল মুুজমদার)</p><p>                        (প্রধান নির্বাহী কর্মকর্তা)</p><p>                        (নারায়নগঞ্জ সিটি কর্পোরেশন)</p><p>                        (৭৬৩৩৪২৩)</p><p>                        (ceo@ncc.gov.bd)</p>', '<p>১। হিসাব বিভাগ,নগর ভবন,নারায়ণগঞ্জ সিটি কর্পোরেশন নারায়ণগঞ্জ।<br></p><p>                                        ২। উপ-পরিচালক,স্থানীয় সরকার শাখা,জেলা প্রশাসকের কার্যালয়,নারায়ণগঞ্জ।<br></p><p>                                        ৩। সচিব স্থানীয় সরকার বিভাগ,স্থানীয় সরকার পল্লী উন্নয়ন ও সমবায় মন্ত্রণালয়,বাংলাদেশ সচিবালয়,ঢাকা  <br></p><p>                                        ৪। জেলা প্রশাসক,নারায়ণগঞ্জ।<br></p>', 1, 0, '2017-04-11 12:02:57', 1, '2017-04-11 18:02:57', NULL),
(102, NULL, 7, 128, NULL, '45455', NULL, '2017-04-11 00:00:00', 'পদ্ম সিটি প্লাজা-১', '<p>উপর্যুক্ত বিষয়ে জানানো যাচ্ছে যে, নারায়ণগঞ্জ সিটি কর্পোরেশনাধীন পদ্ম সিটি প্লাজা-১ ১৪২৩ বাংলা সনের ১ বৈশাখ হতে ৩০ চৈত্র পর্যন্ত এক বত্সর মেয়াদে ইজারা গ্রহণের জন্য আপনার প্রস্তাবিত দর ১২০০০/-টাকা সর্বোচ্চ বিবেচিত হওয়ায় তা গ্রেহিত হয়েছে। আপনি দরপত্রের সাথে পে-অর্ডার দ্বারা ৩০০০ টাকা জমা প্রদান করেছেন।</p><p>                               এমতাবস্তায়, পত্র প্রাপ্তির ০৭ (সাত) কার্যদিবসের মধ্যে প্রস্তাবিত মূল্য এর উপর ১৫% ভ্যাট বাবদ ৫০০০/-টাকা, ৫% আয়কর বাবদ ৮৯৬৬/-টাকা এবং ৫% জামানত বাবদ ৫০০/-টাকা সহ সর্বমোট ৫৯৬৩ টাকা সিটি কর্পোরেশন তহবিলে জমা প্রদান করতঃ সিটি কর্পোরেশনের সহিত ৩০০/-টাকা নন জুডিশিয়াল স্ট্যাম্পে চুক্তিপত্র সম্পাদানের জন্য অনুরোধ করা হলো।</p>', '<p>শর্তাবলি ঃ</p><p>                ১। পদ্ম সিটি প্লাজা-১ পরিচালনার ক্ষেত্রে ইজারার বিজ্ঞপ্তির বর্ণিত শর্তাবলি প্রযোজ্য হবে।</p><p>                ২। শুধুমাত্র পদ্ম সিটি প্লাজা-১ হতে টোল আদায় করতে হবে।</p><p>                ৩। এই কার্যাদেশের মেয়াদ ১১/০৪/২০১৭ তারিখ হতে ১১/০৪/২০১৭ তারিখ পর্যন্ত।</p>', '<p>জনাব Abdul Awal</p><p>                        পিতা/স্বামী- Maynuddin</p><p>                        বাসা নং- 52, রাস্তা- 15</p><p>                        থানা- DHUPCHANCHIA, জেলা- BOGRA</p>', '<p>(মোঃ মোস্তফা কামাল মুুজমদার)</p><p>                        (প্রধান নির্বাহী কর্মকর্তা)</p><p>                        (নারায়নগঞ্জ সিটি কর্পোরেশন)</p><p>                        (৭৬৩৩৪২৩)</p><p>                        (ceo@ncc.gov.bd)</p>', '<p>১। হিসাব বিভাগ,নগর ভবন,নারায়ণগঞ্জ সিটি কর্পোরেশন নারায়ণগঞ্জ।<br></p><p>                                        ২। উপ-পরিচালক,স্থানীয় সরকার শাখা,জেলা প্রশাসকের কার্যালয়,নারায়ণগঞ্জ।<br></p><p>                                        ৩। সচিব স্থানীয় সরকার বিভাগ,স্থানীয় সরকার পল্লী উন্নয়ন ও সমবায় মন্ত্রণালয়,বাংলাদেশ সচিবালয়,ঢাকা  <br></p><p>                                        ৪। জেলা প্রশাসক,নারায়ণগঞ্জ।<br></p>', 1, 0, '2017-04-11 12:04:05', 1, '2017-04-11 18:04:05', NULL),
(103, NULL, 7, 128, NULL, '454552', NULL, '2017-04-11 00:00:00', 'পদ্ম সিটি প্লাজা-১', '<p>উপর্যুক্ত বিষয়ে জানানো যাচ্ছে যে, নারায়ণগঞ্জ সিটি কর্পোরেশনাধীন পদ্ম সিটি প্লাজা-১ ১৪২৩ বাংলা সনের ১ বৈশাখ হতে ৩০ চৈত্র পর্যন্ত এক বত্সর মেয়াদে ইজারা গ্রহণের জন্য আপনার প্রস্তাবিত দর ১২০০০/-টাকা সর্বোচ্চ বিবেচিত হওয়ায় তা গ্রেহিত হয়েছে। আপনি দরপত্রের সাথে পে-অর্ডার দ্বারা ৩০০০ টাকা জমা প্রদান করেছেন।</p><p>                               এমতাবস্তায়, পত্র প্রাপ্তির ০৭ (সাত) কার্যদিবসের মধ্যে প্রস্তাবিত মূল্য এর উপর ১৫% ভ্যাট বাবদ ৫০০০/-টাকা, ৫% আয়কর বাবদ ৮৯৬৬/-টাকা এবং ৫% জামানত বাবদ ৫০০/-টাকা সহ সর্বমোট ৫৯৬৩ টাকা সিটি কর্পোরেশন তহবিলে জমা প্রদান করতঃ সিটি কর্পোরেশনের সহিত ৩০০/-টাকা নন জুডিশিয়াল স্ট্যাম্পে চুক্তিপত্র সম্পাদানের জন্য অনুরোধ করা হলো।</p>', '<p>শর্তাবলি ঃ</p><p>                ১। পদ্ম সিটি প্লাজা-১ পরিচালনার ক্ষেত্রে ইজারার বিজ্ঞপ্তির বর্ণিত শর্তাবলি প্রযোজ্য হবে।</p><p>                ২। শুধুমাত্র পদ্ম সিটি প্লাজা-১ হতে টোল আদায় করতে হবে।</p><p>                ৩। এই কার্যাদেশের মেয়াদ ১১/০৪/২০১৭ তারিখ হতে ১১/০৪/২০১৭ তারিখ পর্যন্ত।</p>', '<p>জনাব Abdul Awal</p><p>                        পিতা/স্বামী- Maynuddin</p><p>                        বাসা নং- 52, রাস্তা- 15</p><p>                        থানা- DHUPCHANCHIA, জেলা- BOGRA</p>', '<p>(মোঃ মোস্তফা কামাল মুুজমদার)</p><p>                        (প্রধান নির্বাহী কর্মকর্তা)</p><p>                        (নারায়নগঞ্জ সিটি কর্পোরেশন)</p><p>                        (৭৬৩৩৪২৩)</p><p>                        (ceo@ncc.gov.bd)</p>', '<p>১। হিসাব বিভাগ,নগর ভবন,নারায়ণগঞ্জ সিটি কর্পোরেশন নারায়ণগঞ্জ।<br></p><p>                                        ২। উপ-পরিচালক,স্থানীয় সরকার শাখা,জেলা প্রশাসকের কার্যালয়,নারায়ণগঞ্জ।<br></p><p>                                        ৩। সচিব স্থানীয় সরকার বিভাগ,স্থানীয় সরকার পল্লী উন্নয়ন ও সমবায় মন্ত্রণালয়,বাংলাদেশ সচিবালয়,ঢাকা  <br></p><p>                                        ৪। জেলা প্রশাসক,নারায়ণগঞ্জ।<br></p>', 1, 0, '2017-04-11 12:30:32', 1, '2017-04-11 18:30:32', NULL),
(104, NULL, 7, 128, NULL, 'dfdf', NULL, '2017-04-11 00:00:00', 'পদ্ম সিটি প্লাজা-১', '<p>উপর্যুক্ত বিষয়ে জানানো যাচ্ছে যে, নারায়ণগঞ্জ সিটি কর্পোরেশনাধীন পদ্ম সিটি প্লাজা-১ ১৪২৩ বাংলা সনের ১ বৈশাখ হতে ৩০ চৈত্র পর্যন্ত এক বত্সর মেয়াদে ইজারা গ্রহণের জন্য আপনার প্রস্তাবিত দর ১২০০০/-টাকা সর্বোচ্চ বিবেচিত হওয়ায় তা গ্রেহিত হয়েছে। আপনি দরপত্রের সাথে পে-অর্ডার দ্বারা ৩০০০ টাকা জমা প্রদান করেছেন।</p><p>                               এমতাবস্তায়, পত্র প্রাপ্তির ০৭ (সাত) কার্যদিবসের মধ্যে প্রস্তাবিত মূল্য এর উপর ১৫% ভ্যাট বাবদ ৫০০০/-টাকা, ৫% আয়কর বাবদ ৮৯৬৬/-টাকা এবং ৫% জামানত বাবদ ৫০০/-টাকা সহ সর্বমোট ৫৯৬৩ টাকা সিটি কর্পোরেশন তহবিলে জমা প্রদান করতঃ সিটি কর্পোরেশনের সহিত ৩০০/-টাকা নন জুডিশিয়াল স্ট্যাম্পে চুক্তিপত্র সম্পাদানের জন্য অনুরোধ করা হলো।</p>', '<p>শর্তাবলি ঃ</p><p>                ১। পদ্ম সিটি প্লাজা-১ পরিচালনার ক্ষেত্রে ইজারার বিজ্ঞপ্তির বর্ণিত শর্তাবলি প্রযোজ্য হবে।</p><p>                ২। শুধুমাত্র পদ্ম সিটি প্লাজা-১ হতে টোল আদায় করতে হবে।</p><p>                ৩। এই কার্যাদেশের মেয়াদ ১১/০৪/২০১৭ তারিখ হতে ১১/০৪/২০১৭ তারিখ পর্যন্ত।</p>', '<p>জনাব Abdul Awal</p><p>                        পিতা/স্বামী- Maynuddin</p><p>                        বাসা নং- 52, রাস্তা- 15</p><p>                        থানা- DHUPCHANCHIA, জেলা- BOGRA</p>', '<p>(মোঃ মোস্তফা কামাল মুুজমদার)</p><p>                        (প্রধান নির্বাহী কর্মকর্তা)</p><p>                        (নারায়নগঞ্জ সিটি কর্পোরেশন)</p><p>                        (৭৬৩৩৪২৩)</p><p>                        (ceo@ncc.gov.bd)</p>', '<p>১। হিসাব বিভাগ,নগর ভবন,নারায়ণগঞ্জ সিটি কর্পোরেশন নারায়ণগঞ্জ।<br></p><p>                                        ২। উপ-পরিচালক,স্থানীয় সরকার শাখা,জেলা প্রশাসকের কার্যালয়,নারায়ণগঞ্জ।<br></p><p>                                        ৩। সচিব স্থানীয় সরকার বিভাগ,স্থানীয় সরকার পল্লী উন্নয়ন ও সমবায় মন্ত্রণালয়,বাংলাদেশ সচিবালয়,ঢাকা  <br></p><p>                                        ৪। জেলা প্রশাসক,নারায়ণগঞ্জ।<br></p>', 1, 0, '2017-04-11 14:13:23', 1, '2017-04-11 20:13:23', NULL),
(105, NULL, 264, 124, NULL, 'sdsd', NULL, '2017-04-11 00:00:00', 'গোদনাইল হাট বাজার', '<p>উপর্যুক্ত বিষয়ে জানানো যাচ্ছে যে, নারায়ণগঞ্জ সিটি কর্পোরেশনাধীন গোদনাইল হাট বাজার ১৪২৩ বাংলা সনের ১ বৈশাখ হতে ৩০ চৈত্র পর্যন্ত এক বত্সর মেয়াদে ইজারা গ্রহণের জন্য আপনার প্রস্তাবিত দর ৫৫০৫০০/-টাকা সর্বোচ্চ বিবেচিত হওয়ায় তা গ্রেহিত হয়েছে। আপনি দরপত্রের সাথে পে-অর্ডার দ্বারা ৫৫০৫০০ টাকা জমা প্রদান করেছেন।</p><p>                               এমতাবস্তায়, পত্র প্রাপ্তির ০৭ (সাত) কার্যদিবসের মধ্যে প্রস্তাবিত মূল্য এর উপর ১৫% ভ্যাট বাবদ ৫০০০/-টাকা, ৫% আয়কর বাবদ ৮৯৬৬/-টাকা এবং ৫% জামানত বাবদ ৫০০/-টাকা সহ সর্বমোট ৫৯৬৩ টাকা সিটি কর্পোরেশন তহবিলে জমা প্রদান করতঃ সিটি কর্পোরেশনের সহিত ৩০০/-টাকা নন জুডিশিয়াল স্ট্যাম্পে চুক্তিপত্র সম্পাদানের জন্য অনুরোধ করা হলো।</p>', '<p>শর্তাবলি ঃ</p><p>                ১। গোদনাইল হাট বাজার পরিচালনার ক্ষেত্রে ইজারার বিজ্ঞপ্তির বর্ণিত শর্তাবলি প্রযোজ্য হবে।</p><p>                ২। শুধুমাত্র গোদনাইল হাট বাজার হতে টোল আদায় করতে হবে।</p><p>                ৩। এই কার্যাদেশের মেয়াদ ১১/০৪/২০১৭ তারিখ হতে ১১/০৪/২০১৭ তারিখ পর্যন্ত।</p>', '<p>জনাব মোঃ নুরুল্লাহ</p><p>                        পিতা/স্বামী- মোঃ আঃ হাকিম</p><p>                        বাসা নং- পাটুঃ-৫৮৯, রাস্তা- নাই</p><p>                        থানা- PAKUNDIA, জেলা- KISHOREGANJ</p>', '<p>(মোঃ মোস্তফা কামাল মুুজমদার)</p><p>                        (প্রধান নির্বাহী কর্মকর্তা)</p><p>                        (নারায়নগঞ্জ সিটি কর্পোরেশন)</p><p>                        (৭৬৩৩৪২৩)</p><p>                        (ceo@ncc.gov.bd)</p>', '<p>১। হিসাব বিভাগ,নগর ভবন,নারায়ণগঞ্জ সিটি কর্পোরেশন নারায়ণগঞ্জ।<br></p><p>                                        ২। উপ-পরিচালক,স্থানীয় সরকার শাখা,জেলা প্রশাসকের কার্যালয়,নারায়ণগঞ্জ।<br></p><p>                                        ৩। সচিব স্থানীয় সরকার বিভাগ,স্থানীয় সরকার পল্লী উন্নয়ন ও সমবায় মন্ত্রণালয়,বাংলাদেশ সচিবালয়,ঢাকা  <br></p><p>                                        ৪। জেলা প্রশাসক,নারায়ণগঞ্জ।<br></p>', 1, 0, '2017-04-11 14:13:49', 1, '2017-04-11 20:13:49', NULL),
(106, NULL, 264, 124, NULL, '343434', NULL, '2017-04-11 00:00:00', 'গোদনাইল হাট বাজার', '<p>উপর্যুক্ত বিষয়ে জানানো যাচ্ছে যে, নারায়ণগঞ্জ সিটি কর্পোরেশনাধীন গোদনাইল হাট বাজার ১৪২৩ বাংলা সনের ১ বৈশাখ হতে ৩০ চৈত্র পর্যন্ত এক বত্সর মেয়াদে ইজারা গ্রহণের জন্য আপনার প্রস্তাবিত দর ৫৫০৫০০/-টাকা সর্বোচ্চ বিবেচিত হওয়ায় তা গ্রেহিত হয়েছে। আপনি দরপত্রের সাথে পে-অর্ডার দ্বারা ৫৫০৫০০ টাকা জমা প্রদান করেছেন।</p><p>                               এমতাবস্তায়, পত্র প্রাপ্তির ০৭ (সাত) কার্যদিবসের মধ্যে প্রস্তাবিত মূল্য এর উপর ১৫% ভ্যাট বাবদ ৫০০০/-টাকা, ৫% আয়কর বাবদ ৮৯৬৬/-টাকা এবং ৫% জামানত বাবদ ৫০০/-টাকা সহ সর্বমোট ৫৯৬৩ টাকা সিটি কর্পোরেশন তহবিলে জমা প্রদান করতঃ সিটি কর্পোরেশনের সহিত ৩০০/-টাকা নন জুডিশিয়াল স্ট্যাম্পে চুক্তিপত্র সম্পাদানের জন্য অনুরোধ করা হলো।</p>', '<p>শর্তাবলি ঃ</p><p>                ১। গোদনাইল হাট বাজার পরিচালনার ক্ষেত্রে ইজারার বিজ্ঞপ্তির বর্ণিত শর্তাবলি প্রযোজ্য হবে।</p><p>                ২। শুধুমাত্র গোদনাইল হাট বাজার হতে টোল আদায় করতে হবে।</p><p>                ৩। এই কার্যাদেশের মেয়াদ ১১/০৪/২০১৭ তারিখ হতে ১১/০৪/২০১৭ তারিখ পর্যন্ত।</p>', '<p>জনাব মোঃ নুরুল্লাহ</p><p>                        পিতা/স্বামী- মোঃ আঃ হাকিম</p><p>                        বাসা নং- পাটুঃ-৫৮৯, রাস্তা- নাই</p><p>                        থানা- PAKUNDIA, জেলা- KISHOREGANJ</p>', '<p>(মোঃ মোস্তফা কামাল মুুজমদার)</p><p>                        (প্রধান নির্বাহী কর্মকর্তা)</p><p>                        (নারায়নগঞ্জ সিটি কর্পোরেশন)</p><p>                        (৭৬৩৩৪২৩)</p><p>                        (ceo@ncc.gov.bd)</p>', '<p>১। হিসাব বিভাগ,নগর ভবন,নারায়ণগঞ্জ সিটি কর্পোরেশন নারায়ণগঞ্জ।<br></p><p>                                        ২। উপ-পরিচালক,স্থানীয় সরকার শাখা,জেলা প্রশাসকের কার্যালয়,নারায়ণগঞ্জ।<br></p><p>                                        ৩। সচিব স্থানীয় সরকার বিভাগ,স্থানীয় সরকার পল্লী উন্নয়ন ও সমবায় মন্ত্রণালয়,বাংলাদেশ সচিবালয়,ঢাকা  <br></p><p>                                        ৪। জেলা প্রশাসক,নারায়ণগঞ্জ।<br></p>', 1, 0, '2017-04-11 14:14:20', 1, '2017-04-11 20:14:20', NULL),
(107, NULL, 7, 141, 14, '334', NULL, '2017-04-15 00:00:00', 'মালিকানা বাতিল', '<p>উপর্যুক্ত বিষয়ে জানানো যাচ্ছে যে, আপনার অধীনে থাকা নারায়ণগঞ্জ সিটি কর্পোরেশনাধীন পদ্ম সিটি প্লাজা-১ এর ১/এ/৩০০ নং দোকানটির মালিকানা বাতিল করা হল।</p>', NULL, NULL, '<p>(মোঃ মোস্তফা কামাল মুুজমদার)</p><p>                            (প্রধান নির্বাহী কর্মকর্তা)</p><p>                            (নারায়নগঞ্জ সিটি কর্পোরেশন)</p><p>                            (৭৬৩৩৪২৩)</p><p>                            (ceo@ncc.gov.bd)</p>', NULL, 0, 0, '2017-04-14 19:23:56', 1, '2017-04-15 01:23:56', NULL),
(108, NULL, 7, 141, 14, '334', NULL, '2017-04-15 00:00:00', 'মালিকানা বাতিল', '<p>উপর্যুক্ত বিষয়ে জানানো যাচ্ছে যে, আপনার অধীনে থাকা নারায়ণগঞ্জ সিটি কর্পোরেশনাধীন পদ্ম সিটি প্লাজা-১ এর ১/এ/৩০০ নং দোকানটির মালিকানা বাতিল করা হল।</p>', NULL, NULL, '<p>(মোঃ মোস্তফা কামাল মুুজমদার)</p><p>                            (প্রধান নির্বাহী কর্মকর্তা)</p><p>                            (নারায়নগঞ্জ সিটি কর্পোরেশন)</p><p>                            (৭৬৩৩৪২৩)</p><p>                            (ceo@ncc.gov.bd)</p>', NULL, 0, 0, '2017-04-14 19:38:58', 1, '2017-04-15 01:38:58', NULL),
(109, 266, NULL, 134, NULL, '46636', 'গত 15-04-2017 তারিখে আপনার দাখিলকৃত দরপত্র।', '2017-04-15 00:00:00', 'ইজারার অবশিষ্ট টাকা পরিশোধ প্রসঙ্গে', '<p>উপর্যুক্ত বিষয়ে জানানো যাচ্ছে যে, নারায়ণগঞ্জ সিটি কর্পোরেশনাধীন ভবানীগঞ্জ হাট বাজার <span class="start_date">১৫-০৪-২০১৭</span> তারিখ হতে <span class="end_date">৩০-০৪-২০১৭</span> তারিখ পর্যন্ত ইজারা গ্রহণের জন্য আপনার প্রস্তাবিত দর ১২০/- টাকা সর্বোচ্চ বিবেচিত হওয়ায় তা গৃহিত হয়েছে। আপনি দরপত্রের সাথে পে-অর্ডার দ্বারা ১২০/- টাকা জমা প্রদান করেছেন।</p><p>এমতাবস্থায়, পত্র প্রাপ্তির ০৭ (সাত) কার্যদিবসের মধ্যে প্রস্তাবিত মূল্যের উপর\r\n                            \r\n                                                                    ৫% জামানত বাবদ ৬/- টাকা \r\n                                                                    ১৫% ভ্যাট বাবদ ১৮/- টাকা \r\n                                                                    ৫% আয়কর বাবদ বাবদ ৬/- টাকা \r\n                                                            \r\n                            সর্বমোট ৩০/- টাকা সিটি কর্পোরেশন তহবিলে জমা প্রদান করতঃ সিটি কর্পোরেশনের সহিত ৩০০/-টাকা নন জুডিশিয়াল স্ট্যাম্পে চুক্তিপত্র সম্পাদানের জন্য অনুরোধ করা হলো।\r\n                        </p>', NULL, '<p>জনাব মোঃ আব্দুল নুরুল্লাহ</p><p>                            পিতা/স্বামী- মোঃ আঃ রহিম</p><p>                            বাসা নং- df, রাস্তা- f</p><p>                            থানা- BANDARBAN SADAR, জেলা- BANDARBAN</p>', '<p>(মোঃ মোস্তফা কামাল মুুজমদার)</p><p>                            (প্রধান নির্বাহী কর্মকর্তা)</p><p>                            (নারায়নগঞ্জ সিটি কর্পোরেশন)</p><p>                            (৭৬৩৩৪২৩)</p><p>                            (ceo@ncc.gov.bd)</p>', '<p>1। হিসাব বিভাগ,নগর ভবন,নারায়ণগঞ্জ সিটি কর্পোরেশন নারায়ণগঞ্জ।<br></p><p>                                                2। উপ-পরিচালক,স্থানীয় সরকার শাখা,জেলা প্রশাসকের কার্যালয়,নারায়ণগঞ্জ।<br></p><p>                                                3। সচিব স্থানীয় সরকার বিভাগ,স্থানীয় সরকার পল্লী উন্নয়ন ও সমবায় মন্ত্রণালয়,বাংলাদেশ সচিবালয়,ঢাকা  <br></p><p>                                                4। জেলা প্রশাসক,নারায়ণগঞ্জ।<br></p>', 0, 1, '2017-04-15 04:11:22', 1, '2017-04-15 10:11:22', NULL),
(110, NULL, 7, 128, 14, '1236', NULL, '2017-04-15 00:00:00', 'মালিকানা বাতিল', '<p>উপর্যুক্ত বিষয়ে জানানো যাচ্ছে যে, আপনার অধীনে থাকা নারায়ণগঞ্জ সিটি কর্পোরেশনাধীন পদ্ম সিটি প্লাজা-১ এর ১/এ/৩০০ নং দোকানটির মালিকানা বাতিল করা হল।</p>', NULL, NULL, '<p>(মোঃ মোস্তফা কামাল মুুজমদার)</p><p>                            (প্রধান নির্বাহী কর্মকর্তা)</p><p>                            (নারায়নগঞ্জ সিটি কর্পোরেশন)</p><p>                            (৭৬৩৩৪২৩)</p><p>                            (ceo@ncc.gov.bd)</p>', NULL, 0, 0, '2017-04-15 05:37:12', 1, '2017-04-15 11:37:12', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tender_location`
--

CREATE TABLE IF NOT EXISTS `tender_location` (
  `TE_LOC_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `TENDER_ID` bigint(20) NOT NULL COMMENT 'Primary Key of tender table',
  `LOCATION_ID` int(10) NOT NULL COMMENT 'Primary Key of sa_location table',
  `TYPE` int(11) DEFAULT NULL COMMENT '1=Tender sells location, 2=Tender copy send location',
  `IS_ACTIVE` tinyint(1) NOT NULL COMMENT '0=no, 1=yes',
  `CREATED_AT` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `CREATED_BY` int(10) NOT NULL,
  `UPDATED_AT` datetime DEFAULT NULL,
  `UPDATED_BY` int(10) DEFAULT NULL,
  PRIMARY KEY (`TE_LOC_ID`),
  KEY `FK_LOC_TENDER_ID` (`TENDER_ID`),
  KEY `FK_LOC_LOCATION_ID` (`LOCATION_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=89 ;

--
-- Dumping data for table `tender_location`
--

INSERT INTO `tender_location` (`TE_LOC_ID`, `TENDER_ID`, `LOCATION_ID`, `TYPE`, `IS_ACTIVE`, `CREATED_AT`, `CREATED_BY`, `UPDATED_AT`, `UPDATED_BY`) VALUES
(58, 54, 7, 1, 1, '2016-11-12 21:56:40', 1, '2016-12-07 06:23:36', 1),
(59, 54, 8, 1, 1, '2016-11-12 21:56:40', 1, '2016-12-07 06:23:36', 1),
(60, 49, 7, 1, 1, '2016-11-12 22:00:16', 1, '2016-11-13 04:00:16', NULL),
(61, 49, 8, 1, 1, '2016-11-12 22:00:16', 1, '2016-11-13 04:00:16', NULL),
(62, 54, 9, 2, 1, '2016-11-20 06:27:32', 1, '2016-12-07 06:23:36', 1),
(63, 54, 10, 2, 1, '2016-11-20 06:27:32', 1, '2016-12-07 06:23:36', 1),
(64, 50, 7, 2, 1, '2016-11-20 06:27:32', 1, '2016-11-20 12:27:32', NULL),
(65, 50, 8, 2, 1, '2016-11-20 06:27:32', 1, '2016-11-20 12:27:32', NULL),
(69, 53, 7, 1, 1, '2016-11-21 03:41:30', 1, '2016-11-21 09:41:30', NULL),
(70, 53, 8, 1, 1, '2016-11-21 03:41:30', 1, '2016-11-21 09:41:30', NULL),
(71, 53, 10, 2, 1, '2016-11-21 03:41:30', 1, '2016-11-21 09:41:30', NULL),
(72, 49, 10, 2, 1, '2016-11-21 22:56:12', 1, '2016-11-22 04:56:12', NULL),
(73, 54, 7, 1, 1, '2016-11-23 04:11:35', 7, '2016-11-23 10:11:35', NULL),
(74, 54, 8, 1, 1, '2016-11-23 04:11:35', 7, '2016-11-23 10:11:35', NULL),
(75, 54, 9, 2, 1, '2016-11-23 04:11:35', 7, '2016-11-23 10:11:35', NULL),
(76, 54, 10, 2, 1, '2016-11-23 04:11:35', 7, '2016-11-23 10:11:35', NULL),
(77, 55, 8, 1, 1, '2016-12-07 00:19:52', 1, '2016-12-07 06:19:52', NULL),
(78, 55, 8, 2, 1, '2016-12-07 00:19:52', 1, '2016-12-07 06:19:52', NULL),
(79, 56, 7, 1, 1, '2016-12-26 06:26:26', 1, '2016-12-26 12:26:26', NULL),
(80, 56, 8, 1, 1, '2016-12-26 06:26:26', 1, '2016-12-26 12:26:26', NULL),
(81, 56, 9, 2, 1, '2016-12-26 06:26:26', 1, '2016-12-26 12:26:26', NULL),
(82, 56, 10, 2, 1, '2016-12-26 06:26:26', 1, '2016-12-26 12:26:26', NULL),
(83, 57, 8, 1, 1, '2017-04-23 02:11:49', 1, '2017-04-23 08:11:49', NULL),
(84, 57, 8, 2, 1, '2017-04-23 02:11:49', 1, '2017-04-23 08:11:49', NULL),
(85, 58, 7, 1, 1, '2017-04-24 06:10:25', 1, '2017-04-24 12:10:25', NULL),
(86, 58, 8, 1, 1, '2017-04-24 06:10:25', 1, '2017-04-24 12:10:25', NULL),
(87, 58, 9, 1, 1, '2017-04-24 06:10:25', 1, '2017-04-24 12:10:25', NULL),
(88, 58, 9, 2, 1, '2017-04-24 06:10:25', 1, '2017-04-24 12:10:25', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tender_particulars`
--

CREATE TABLE IF NOT EXISTS `tender_particulars` (
  `TE_PART_ID` bigint(14) NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  `SCHEDULE_ID` bigint(20) NOT NULL COMMENT 'Primary Key of tender_schedule table',
  `PARTICULAR_ID` smallint(6) NOT NULL COMMENT 'Primary Key of ac_particular table',
  `PARTICULAR_AMT` double NOT NULL COMMENT 'Amount to be charged against each Particulars',
  `UOM` tinyint(2) NOT NULL COMMENT 'Unit Of Measure e.g.1= Percentage, 2= Taka etc.',
  `PARTICULAR_SL` tinyint(2) NOT NULL COMMENT 'Applicant will be charged according to this serial',
  `CURRENT_SL` tinyint(1) DEFAULT NULL COMMENT 'Current serial of payment for applicant',
  `REMARKS` varchar(200) CHARACTER SET utf8 DEFAULT NULL COMMENT 'Remarks if have any',
  `IS_ACTIVE` tinyint(1) NOT NULL COMMENT '0=no, 1=yes',
  `CREATED_AT` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `CREATED_BY` int(10) NOT NULL,
  `UPDATED_AT` datetime DEFAULT NULL,
  `UPDATED_BY` int(10) DEFAULT NULL,
  PRIMARY KEY (`TE_PART_ID`),
  KEY `FK_TP_SCHEDULE_ID` (`SCHEDULE_ID`),
  KEY `FK_TP_PARTICULAR_ID` (`PARTICULAR_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=456 ;

--
-- Dumping data for table `tender_particulars`
--

INSERT INTO `tender_particulars` (`TE_PART_ID`, `SCHEDULE_ID`, `PARTICULAR_ID`, `PARTICULAR_AMT`, `UOM`, `PARTICULAR_SL`, `CURRENT_SL`, `REMARKS`, `IS_ACTIVE`, `CREATED_AT`, `CREATED_BY`, `UPDATED_AT`, `UPDATED_BY`) VALUES
(327, 121, 3, 5, 1, 1, NULL, NULL, 1, '2016-11-12 21:56:40', 1, '2016-11-13 03:56:40', NULL),
(328, 121, 6, 100, 1, 2, NULL, NULL, 1, '2016-11-12 21:56:40', 1, '2016-11-13 03:56:40', NULL),
(329, 121, 7, 500, 2, 3, NULL, NULL, 1, '2016-11-12 21:56:40', 1, '2016-11-13 03:56:40', NULL),
(330, 121, 9, 15, 1, 4, NULL, NULL, 1, '2016-11-12 21:56:40', 1, '2016-11-13 03:56:40', NULL),
(331, 121, 10, 5, 1, 5, NULL, NULL, 1, '2016-11-12 21:56:40', 1, '2016-11-13 03:56:40', NULL),
(332, 122, 3, 5, 1, 6, NULL, NULL, 1, '2016-11-12 21:56:40', 1, '2016-11-13 03:56:40', NULL),
(333, 122, 6, 100, 1, 7, NULL, NULL, 1, '2016-11-12 21:56:40', 1, '2016-11-13 03:56:40', NULL),
(334, 122, 7, 500, 2, 8, NULL, NULL, 1, '2016-11-12 21:56:40', 1, '2016-11-13 03:56:40', NULL),
(335, 122, 9, 15, 1, 9, NULL, NULL, 1, '2016-11-12 21:56:40', 1, '2016-11-13 03:56:40', NULL),
(336, 122, 10, 5, 1, 10, NULL, NULL, 1, '2016-11-12 21:56:40', 1, '2016-11-13 03:56:40', NULL),
(337, 123, 3, 5, 1, 11, NULL, NULL, 1, '2016-11-12 21:56:41', 1, '2016-11-13 03:56:41', NULL),
(338, 123, 6, 100, 1, 12, NULL, NULL, 1, '2016-11-12 21:56:41', 1, '2016-11-13 03:56:41', NULL),
(339, 123, 7, 500, 2, 13, NULL, NULL, 1, '2016-11-12 21:56:41', 1, '2016-11-13 03:56:41', NULL),
(340, 123, 9, 15, 1, 14, NULL, NULL, 1, '2016-11-12 21:56:41', 1, '2016-11-13 03:56:41', NULL),
(341, 123, 10, 5, 1, 15, NULL, NULL, 1, '2016-11-12 21:56:41', 1, '2016-11-13 03:56:41', NULL),
(342, 124, 3, 5, 1, 16, NULL, NULL, 1, '2016-11-12 21:56:41', 1, '2016-11-13 03:56:41', NULL),
(343, 124, 6, 100, 1, 17, NULL, NULL, 1, '2016-11-12 21:56:41', 1, '2016-11-13 03:56:41', NULL),
(344, 124, 7, 500, 2, 18, NULL, NULL, 1, '2016-11-12 21:56:41', 1, '2016-11-13 03:56:41', NULL),
(345, 124, 9, 15, 1, 19, NULL, NULL, 1, '2016-11-12 21:56:41', 1, '2016-11-13 03:56:41', NULL),
(346, 124, 10, 5, 1, 20, NULL, NULL, 1, '2016-11-12 21:56:41', 1, '2016-11-13 03:56:41', NULL),
(347, 125, 5, 1200, 2, 21, NULL, NULL, 1, '2016-11-12 22:00:16', 1, '2016-11-13 04:00:16', NULL),
(348, 125, 4, 4500, 2, 22, NULL, NULL, 1, '2016-11-12 22:00:16', 1, '2016-11-13 04:00:16', NULL),
(349, 125, 3, 30, 1, 23, NULL, NULL, 1, '2016-11-12 22:00:16', 1, '2016-11-13 04:00:16', NULL),
(350, 126, 5, 1200, 2, 24, NULL, NULL, 1, '2016-11-12 22:00:16', 1, '2016-11-13 04:00:16', NULL),
(351, 126, 4, 4500, 2, 25, NULL, NULL, 1, '2016-11-12 22:00:16', 1, '2016-11-13 04:00:16', NULL),
(352, 126, 3, 30, 1, 26, NULL, NULL, 1, '2016-11-12 22:00:17', 1, '2016-11-13 04:00:17', NULL),
(353, 127, 5, 1000, 2, 27, NULL, NULL, 1, '2016-11-12 22:00:17', 1, '2016-11-13 04:00:17', NULL),
(354, 127, 4, 2000, 2, 28, NULL, NULL, 1, '2016-11-12 22:00:17', 1, '2016-11-13 04:00:17', NULL),
(355, 127, 3, 25, 1, 29, NULL, NULL, 1, '2016-11-12 22:00:17', 1, '2016-11-13 04:00:17', NULL),
(356, 128, 5, 1000, 2, 30, NULL, NULL, 1, '2016-11-12 22:00:17', 1, '2016-11-13 04:00:17', NULL),
(357, 128, 4, 2000, 2, 31, NULL, NULL, 1, '2016-11-12 22:00:17', 1, '2016-11-13 04:00:17', NULL),
(358, 128, 3, 25, 1, 32, NULL, NULL, 1, '2016-11-12 22:00:17', 1, '2016-11-13 04:00:17', NULL),
(359, 129, 5, 1200, 2, 33, NULL, NULL, 1, '2016-11-20 06:27:32', 1, '2016-11-21 06:17:11', 1),
(360, 129, 4, 4500, 2, 34, NULL, NULL, 1, '2016-11-20 06:27:32', 1, '2016-11-21 06:17:11', 1),
(361, 129, 3, 30, 1, 35, NULL, NULL, 1, '2016-11-20 06:27:32', 1, '2016-11-21 06:17:11', 1),
(362, 130, 5, 1200, 2, 36, NULL, NULL, 1, '2016-11-20 06:27:32', 1, '2016-11-21 06:17:11', 1),
(363, 130, 4, 4500, 2, 37, NULL, NULL, 1, '2016-11-20 06:27:32', 1, '2016-11-21 06:17:11', 1),
(364, 130, 3, 30, 1, 38, NULL, NULL, 1, '2016-11-20 06:27:32', 1, '2016-11-21 06:17:11', 1),
(365, 131, 5, 10000, 2, 39, NULL, NULL, 1, '2016-11-20 06:27:32', 1, '2016-11-21 06:17:11', 1),
(366, 131, 4, 2000, 2, 40, NULL, NULL, 1, '2016-11-20 06:27:32', 1, '2016-11-21 06:17:11', 1),
(367, 131, 3, 25, 1, 41, NULL, NULL, 1, '2016-11-20 06:27:32', 1, '2016-11-21 06:17:11', 1),
(368, 132, 5, 10000, 2, 42, NULL, NULL, 1, '2016-11-20 06:27:32', 1, '2016-11-21 06:17:11', 1),
(369, 132, 4, 2000, 2, 43, NULL, NULL, 1, '2016-11-20 06:27:32', 1, '2016-11-21 06:17:11', 1),
(370, 132, 3, 25, 1, 44, NULL, NULL, 1, '2016-11-20 06:27:32', 1, '2016-11-21 06:17:11', 1),
(371, 133, 3, 5, 1, 45, NULL, NULL, 1, '2016-11-21 03:41:31', 1, '2016-11-21 10:10:19', 1),
(372, 133, 6, 100, 1, 46, NULL, NULL, 1, '2016-11-21 03:41:31', 1, '2016-11-21 10:10:19', 1),
(373, 133, 7, 500, 2, 47, NULL, NULL, 1, '2016-11-21 03:41:31', 1, '2016-11-21 10:10:19', 1),
(374, 133, 9, 15, 1, 48, NULL, NULL, 1, '2016-11-21 03:41:31', 1, '2016-11-21 10:10:19', 1),
(375, 133, 10, 5, 1, 49, NULL, NULL, 1, '2016-11-21 03:41:31', 1, '2016-11-21 10:10:19', 1),
(376, 134, 3, 5, 1, 50, NULL, NULL, 1, '2016-11-21 03:41:31', 1, '2016-11-21 10:10:19', 1),
(377, 134, 6, 100, 1, 51, NULL, NULL, 1, '2016-11-21 03:41:31', 1, '2016-11-21 10:10:19', 1),
(378, 134, 7, 500, 2, 52, NULL, NULL, 1, '2016-11-21 03:41:31', 1, '2016-11-21 10:10:19', 1),
(379, 134, 9, 15, 1, 53, NULL, NULL, 1, '2016-11-21 03:41:31', 1, '2016-11-21 10:10:19', 1),
(380, 134, 10, 5, 1, 54, NULL, NULL, 1, '2016-11-21 03:41:31', 1, '2016-11-21 10:10:19', 1),
(381, 135, 3, 5, 1, 55, NULL, NULL, 1, '2016-11-21 03:41:31', 1, '2016-11-21 10:10:19', 1),
(382, 135, 6, 100, 1, 56, NULL, NULL, 1, '2016-11-21 03:41:31', 1, '2016-11-21 10:10:19', 1),
(383, 135, 7, 500, 2, 57, NULL, NULL, 1, '2016-11-21 03:41:32', 1, '2016-11-21 10:10:20', 1),
(384, 135, 9, 15, 1, 58, NULL, NULL, 1, '2016-11-21 03:41:32', 1, '2016-11-21 10:10:20', 1),
(385, 135, 10, 5, 1, 59, NULL, NULL, 1, '2016-11-21 03:41:32', 1, '2016-11-21 10:10:20', 1),
(386, 136, 3, 5, 1, 60, NULL, NULL, 1, '2016-11-21 03:41:32', 1, '2016-11-21 10:10:20', 1),
(387, 136, 6, 100, 1, 61, NULL, NULL, 1, '2016-11-21 03:41:32', 1, '2016-11-21 10:10:20', 1),
(388, 136, 7, 500, 2, 62, NULL, NULL, 1, '2016-11-21 03:41:32', 1, '2016-11-21 10:10:20', 1),
(389, 136, 9, 15, 1, 63, NULL, NULL, 1, '2016-11-21 03:41:32', 1, '2016-11-21 10:10:20', 1),
(390, 136, 10, 5, 1, 64, NULL, NULL, 1, '2016-11-21 03:41:32', 1, '2016-11-21 10:10:20', 1),
(391, 137, 5, 6000, 2, 65, NULL, NULL, 1, '2016-11-22 04:45:29', 1, '2016-11-22 10:45:29', NULL),
(392, 137, 4, 2500, 2, 66, NULL, NULL, 1, '2016-11-22 04:45:29', 1, '2016-11-22 10:45:29', NULL),
(393, 137, 3, 28, 1, 67, NULL, NULL, 1, '2016-11-22 04:45:29', 1, '2016-11-22 10:45:29', NULL),
(394, 138, 5, 1200, 2, 68, NULL, NULL, 1, '2016-11-23 04:11:36', 7, '2016-12-07 06:23:36', 1),
(395, 138, 4, 4500, 2, 69, NULL, NULL, 1, '2016-11-23 04:11:36', 7, '2016-12-07 06:23:36', 1),
(396, 138, 3, 30, 1, 70, NULL, NULL, 1, '2016-11-23 04:11:36', 7, '2016-12-07 06:23:36', 1),
(397, 139, 5, 1200, 2, 71, NULL, NULL, 1, '2016-11-23 04:11:36', 7, '2016-12-07 06:23:36', 1),
(398, 139, 4, 4500, 2, 72, NULL, NULL, 1, '2016-11-23 04:11:36', 7, '2016-12-07 06:23:36', 1),
(399, 139, 3, 30, 1, 73, NULL, NULL, 1, '2016-11-23 04:11:36', 7, '2016-12-07 06:23:36', 1),
(400, 140, 5, 10000, 2, 74, NULL, NULL, 1, '2016-11-23 04:11:36', 7, '2016-12-07 06:23:36', 1),
(401, 140, 4, 2000, 2, 75, NULL, NULL, 1, '2016-11-23 04:11:36', 7, '2016-12-07 06:23:36', 1),
(402, 140, 3, 25, 1, 76, NULL, NULL, 1, '2016-11-23 04:11:36', 7, '2016-12-07 06:23:36', 1),
(403, 141, 5, 10000, 2, 77, NULL, NULL, 1, '2016-11-23 04:11:36', 7, '2016-12-07 06:23:36', 1),
(404, 141, 4, 2000, 2, 78, NULL, NULL, 1, '2016-11-23 04:11:36', 7, '2016-12-07 06:23:37', 1),
(405, 141, 3, 25, 1, 79, NULL, NULL, 1, '2016-11-23 04:11:36', 7, '2016-12-07 06:23:37', 1),
(406, 142, 3, 5, 1, 80, NULL, NULL, 1, '2016-12-07 00:19:53', 1, '2016-12-07 06:19:53', NULL),
(407, 142, 6, 100, 1, 81, NULL, NULL, 1, '2016-12-07 00:19:53', 1, '2016-12-07 06:19:53', NULL),
(408, 142, 7, 500, 2, 82, NULL, NULL, 1, '2016-12-07 00:19:53', 1, '2016-12-07 06:19:53', NULL),
(409, 142, 9, 15, 1, 83, NULL, NULL, 1, '2016-12-07 00:19:53', 1, '2016-12-07 06:19:53', NULL),
(410, 142, 10, 5, 1, 84, NULL, NULL, 1, '2016-12-07 00:19:53', 1, '2016-12-07 06:19:53', NULL),
(411, 143, 5, 1200, 2, 85, NULL, NULL, 1, '2016-12-26 06:26:26', 1, '2016-12-26 12:26:26', NULL),
(412, 143, 4, 4500, 2, 86, NULL, NULL, 1, '2016-12-26 06:26:26', 1, '2016-12-26 12:26:26', NULL),
(413, 143, 3, 30, 1, 87, NULL, NULL, 1, '2016-12-26 06:26:26', 1, '2016-12-26 12:26:26', NULL),
(414, 144, 5, 1200, 2, 88, NULL, NULL, 1, '2016-12-26 06:26:26', 1, '2016-12-26 12:26:26', NULL),
(415, 144, 4, 4500, 2, 89, NULL, NULL, 1, '2016-12-26 06:26:26', 1, '2016-12-26 12:26:26', NULL),
(416, 144, 3, 30, 1, 90, NULL, NULL, 1, '2016-12-26 06:26:26', 1, '2016-12-26 12:26:26', NULL),
(417, 145, 5, 6000, 2, 91, NULL, NULL, 1, '2017-01-09 21:39:49', 1, '2017-01-10 03:39:49', NULL),
(418, 145, 4, 2500, 2, 92, NULL, NULL, 1, '2017-01-09 21:39:49', 1, '2017-01-10 03:39:49', NULL),
(419, 145, 3, 28, 1, 93, NULL, NULL, 1, '2017-01-09 21:39:49', 1, '2017-01-10 03:39:49', NULL),
(420, 146, 5, 1200, 2, 94, NULL, NULL, 1, '2017-04-23 02:11:50', 1, '2017-04-23 08:11:50', NULL),
(421, 146, 4, 4500, 2, 95, NULL, NULL, 1, '2017-04-23 02:11:50', 1, '2017-04-23 08:11:50', NULL),
(422, 146, 3, 30, 1, 96, NULL, NULL, 1, '2017-04-23 02:11:50', 1, '2017-04-23 08:11:50', NULL),
(423, 147, 5, 1200, 2, 97, NULL, NULL, 1, '2017-04-23 02:11:50', 1, '2017-04-23 08:11:50', NULL),
(424, 147, 4, 4500, 2, 98, NULL, NULL, 1, '2017-04-23 02:11:50', 1, '2017-04-23 08:11:50', NULL),
(425, 147, 3, 30, 1, 99, NULL, NULL, 1, '2017-04-23 02:11:50', 1, '2017-04-23 08:11:50', NULL),
(426, 148, 5, 1200, 2, 100, NULL, NULL, 1, '2017-04-23 02:11:50', 1, '2017-04-23 08:11:50', NULL),
(427, 148, 4, 4500, 2, 101, NULL, NULL, 1, '2017-04-23 02:11:50', 1, '2017-04-23 08:11:50', NULL),
(428, 148, 3, 30, 1, 102, NULL, NULL, 1, '2017-04-23 02:11:50', 1, '2017-04-23 08:11:50', NULL),
(429, 149, 5, 1200, 2, 103, NULL, NULL, 1, '2017-04-23 02:11:50', 1, '2017-04-23 08:11:50', NULL),
(430, 149, 4, 4500, 2, 104, NULL, NULL, 1, '2017-04-23 02:11:50', 1, '2017-04-23 08:11:50', NULL),
(431, 149, 3, 30, 1, 105, NULL, NULL, 1, '2017-04-23 02:11:50', 1, '2017-04-23 08:11:50', NULL),
(432, 150, 5, 6000, 2, 106, NULL, NULL, 1, '2017-04-23 02:11:51', 1, '2017-04-23 08:11:51', NULL),
(433, 150, 4, 2500, 2, 107, NULL, NULL, 1, '2017-04-23 02:11:51', 1, '2017-04-23 08:11:51', NULL),
(434, 150, 3, 28, 1, 108, NULL, NULL, 1, '2017-04-23 02:11:51', 1, '2017-04-23 08:11:51', NULL),
(435, 151, 5, 6000, 2, 109, NULL, NULL, 1, '2017-04-23 02:11:51', 1, '2017-04-23 08:11:51', NULL),
(436, 151, 4, 2500, 2, 110, NULL, NULL, 1, '2017-04-23 02:11:51', 1, '2017-04-23 08:11:51', NULL),
(437, 151, 3, 28, 1, 111, NULL, NULL, 1, '2017-04-23 02:11:51', 1, '2017-04-23 08:11:51', NULL),
(438, 152, 5, 6, 2, 112, NULL, NULL, 1, '2017-04-24 06:10:26', 1, '2017-04-24 12:10:26', NULL),
(439, 152, 4, 1500, 2, 113, NULL, NULL, 1, '2017-04-24 06:10:26', 1, '2017-04-24 12:10:26', NULL),
(440, 152, 3, 30, 1, 114, NULL, NULL, 1, '2017-04-24 06:10:26', 1, '2017-04-24 12:10:26', NULL),
(441, 153, 5, 6, 2, 115, NULL, NULL, 1, '2017-04-24 06:10:26', 1, '2017-04-24 12:10:26', NULL),
(442, 153, 4, 1500, 2, 116, NULL, NULL, 1, '2017-04-24 06:10:26', 1, '2017-04-24 12:10:26', NULL),
(443, 153, 3, 30, 1, 117, NULL, NULL, 1, '2017-04-24 06:10:26', 1, '2017-04-24 12:10:26', NULL),
(444, 154, 5, 6, 2, 118, NULL, NULL, 1, '2017-04-24 06:10:26', 1, '2017-04-24 12:10:26', NULL),
(445, 154, 4, 1500, 2, 119, NULL, NULL, 1, '2017-04-24 06:10:26', 1, '2017-04-24 12:10:26', NULL),
(446, 154, 3, 30, 1, 120, NULL, NULL, 1, '2017-04-24 06:10:26', 1, '2017-04-24 12:10:26', NULL),
(447, 155, 5, 6, 2, 121, NULL, NULL, 1, '2017-04-24 06:10:26', 1, '2017-04-24 12:10:26', NULL),
(448, 155, 4, 1500, 2, 122, NULL, NULL, 1, '2017-04-24 06:10:26', 1, '2017-04-24 12:10:26', NULL),
(449, 155, 3, 30, 1, 123, NULL, NULL, 1, '2017-04-24 06:10:26', 1, '2017-04-24 12:10:26', NULL),
(450, 156, 5, 6, 2, 124, NULL, NULL, 1, '2017-04-24 06:10:26', 1, '2017-04-24 12:10:26', NULL),
(451, 156, 4, 1500, 2, 125, NULL, NULL, 1, '2017-04-24 06:10:27', 1, '2017-04-24 12:10:27', NULL),
(452, 156, 3, 30, 1, 126, NULL, NULL, 1, '2017-04-24 06:10:27', 1, '2017-04-24 12:10:27', NULL),
(453, 157, 5, 6, 2, 127, NULL, NULL, 1, '2017-04-24 06:10:27', 1, '2017-04-24 12:10:27', NULL),
(454, 157, 4, 1500, 2, 127, NULL, NULL, 1, '2017-04-24 06:10:27', 1, '2017-04-24 12:10:27', NULL),
(455, 157, 3, 30, 1, 127, NULL, NULL, 1, '2017-04-24 06:10:27', 1, '2017-04-24 12:10:27', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tender_schedule`
--

CREATE TABLE IF NOT EXISTS `tender_schedule` (
  `SCHEDULE_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `TENDER_ID` bigint(20) NOT NULL COMMENT 'Primary Key of tender table',
  `PROJECT_ID` bigint(20) NOT NULL COMMENT 'Primary Key of project table.',
  `PR_DETAIL_ID` bigint(20) DEFAULT NULL COMMENT 'Primary Key of project_details table',
  `PR_CATEGORY` tinyint(2) DEFAULT NULL COMMENT 'Flat, Shop, Space or Lease',
  `PR_TYPE` tinyint(2) DEFAULT NULL COMMENT 'Flat = Type A, Space = Unit A, Shop = 1 etc',
  `LOWEST_TENDER_MONEY` double DEFAULT NULL COMMENT 'City Corporation Defined Lowest Tender Money',
  `IS_CURRENT` tinyint(1) NOT NULL COMMENT '0=no, 1=yes',
  `IS_ACTIVE` tinyint(1) NOT NULL COMMENT '0=no, 1=yes',
  `SC_SERIAL` tinyint(2) NOT NULL COMMENT 'Project Selling Period. This will be miltiple only for Lease',
  `CREATED_AT` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `CREATED_BY` int(10) NOT NULL,
  `UPDATED_AT` datetime DEFAULT NULL,
  `UPDATED_BY` int(10) DEFAULT NULL,
  PRIMARY KEY (`SCHEDULE_ID`),
  KEY `FK_SH_PROJECT_ID` (`PROJECT_ID`),
  KEY `FK_SH_TENDER_ID` (`TENDER_ID`),
  KEY `FK_SH_PR_CATEGORY` (`PR_CATEGORY`),
  KEY `FK_SH_PR_TYPE` (`PR_TYPE`),
  KEY `tender_schedule_ibfk_1` (`PR_DETAIL_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=158 ;

--
-- Dumping data for table `tender_schedule`
--

INSERT INTO `tender_schedule` (`SCHEDULE_ID`, `TENDER_ID`, `PROJECT_ID`, `PR_DETAIL_ID`, `PR_CATEGORY`, `PR_TYPE`, `LOWEST_TENDER_MONEY`, `IS_CURRENT`, `IS_ACTIVE`, `SC_SERIAL`, `CREATED_AT`, `CREATED_BY`, `UPDATED_AT`, `UPDATED_BY`) VALUES
(121, 48, 62, NULL, 24, NULL, NULL, 1, 1, 1, '2016-11-12 21:56:40', 1, '2016-11-13 03:56:40', NULL),
(122, 48, 63, NULL, 24, NULL, NULL, 1, 1, 2, '2016-11-12 21:56:40', 1, '2016-11-13 03:56:40', NULL),
(123, 48, 65, NULL, 24, NULL, NULL, 1, 1, 3, '2016-11-12 21:56:40', 1, '2016-11-13 03:56:40', NULL),
(124, 48, 66, NULL, 24, NULL, NULL, 1, 1, 4, '2016-11-12 21:56:41', 1, '2016-11-13 03:56:41', NULL),
(125, 49, 64, 69, 21, 11, 10000, 1, 1, 5, '2016-11-12 22:00:16', 1, '2016-11-13 04:00:16', NULL),
(126, 49, 64, 70, 21, 10, 10000, 1, 1, 6, '2016-11-12 22:00:16', 1, '2016-11-13 04:00:16', NULL),
(127, 49, 64, 71, 22, 11, 12000, 1, 1, 7, '2016-11-12 22:00:17', 1, '2016-11-13 04:00:17', NULL),
(128, 49, 64, 72, 22, 10, 12000, 1, 1, 8, '2016-11-12 22:00:17', 1, '2016-11-13 04:00:17', NULL),
(129, 50, 64, 69, 21, 11, 15000, 1, 1, 9, '2016-11-20 06:27:32', 1, '2016-11-21 06:17:10', 1),
(130, 50, 64, 70, 21, 10, 15000, 1, 1, 10, '2016-11-20 06:27:32', 1, '2016-11-21 06:17:11', 1),
(131, 50, 64, 71, 22, 11, 13000, 1, 1, 11, '2016-11-20 06:27:32', 1, '2016-11-21 06:17:11', 1),
(132, 50, 64, 72, 22, 10, 13000, 1, 1, 12, '2016-11-20 06:27:32', 1, '2016-11-21 06:17:11', 1),
(133, 53, 62, NULL, 24, NULL, NULL, 1, 1, 13, '2016-11-21 03:41:31', 1, '2016-11-21 10:10:19', 1),
(134, 53, 63, NULL, 24, NULL, NULL, 1, 1, 14, '2016-11-21 03:41:31', 1, '2016-11-21 10:10:19', 1),
(135, 53, 65, NULL, 24, NULL, NULL, 1, 1, 15, '2016-11-21 03:41:31', 1, '2016-11-21 10:10:19', 1),
(136, 53, 66, NULL, 24, NULL, NULL, 1, 1, 16, '2016-11-21 03:41:32', 1, '2016-11-21 10:10:20', 1),
(137, 50, 64, 73, 23, 10, 150505, 1, 1, 17, '2016-11-22 04:45:29', 1, '2016-11-22 10:45:29', NULL),
(138, 54, 64, 69, 21, 11, 15000, 1, 1, 18, '2016-11-23 04:11:36', 7, '2016-12-07 06:23:36', 1),
(139, 54, 64, 70, 21, 10, 15000, 1, 1, 19, '2016-11-23 04:11:36', 7, '2016-12-07 06:23:36', 1),
(140, 54, 64, 71, 22, 11, 18000, 1, 1, 20, '2016-11-23 04:11:36', 7, '2016-12-07 06:23:36', 1),
(141, 54, 64, 72, 22, 10, 18000, 1, 1, 21, '2016-11-23 04:11:36', 7, '2016-12-07 06:23:36', 1),
(142, 55, 63, NULL, 24, NULL, NULL, 1, 1, 22, '2016-12-07 00:19:53', 1, '2016-12-07 06:19:53', NULL),
(143, 56, 64, 69, 21, 11, 12000, 1, 1, 23, '2016-12-26 06:26:26', 1, '2016-12-26 12:26:26', NULL),
(144, 56, 64, 70, 21, 10, 12000, 1, 1, 24, '2016-12-26 06:26:26', 1, '2016-12-26 12:26:26', NULL),
(145, 54, 64, 73, 23, 10, 20000, 1, 1, 25, '2017-01-09 21:39:49', 1, '2017-01-10 03:39:49', NULL),
(146, 57, 72, 80, 21, 11, 555, 1, 1, 26, '2017-04-23 02:11:50', 1, '2017-04-23 08:11:50', NULL),
(147, 57, 72, 81, 21, 12, 66, 1, 1, 27, '2017-04-23 02:11:50', 1, '2017-04-23 08:11:50', NULL),
(148, 57, 72, 83, 21, 11, 44, 1, 1, 28, '2017-04-23 02:11:50', 1, '2017-04-23 08:11:50', NULL),
(149, 57, 72, 84, 21, 11, 777, 1, 1, 29, '2017-04-23 02:11:50', 1, '2017-04-23 08:11:50', NULL),
(150, 57, 72, 82, 23, 10, 88, 1, 1, 30, '2017-04-23 02:11:50', 1, '2017-04-23 08:11:50', NULL),
(151, 57, 72, 85, 23, 11, 99, 1, 1, 31, '2017-04-23 02:11:51', 1, '2017-04-23 08:11:51', NULL),
(152, 58, 86, 99, 21, 10, 1500000, 1, 1, 32, '2017-04-24 06:10:26', 1, '2017-04-24 12:10:26', NULL),
(153, 58, 86, 100, 21, 10, 1500000, 1, 1, 33, '2017-04-24 06:10:26', 1, '2017-04-24 12:10:26', NULL),
(154, 58, 86, 101, 21, 10, 1500000, 1, 1, 34, '2017-04-24 06:10:26', 1, '2017-04-24 12:10:26', NULL),
(155, 58, 86, 102, 21, 10, 1500000, 1, 1, 35, '2017-04-24 06:10:26', 1, '2017-04-24 12:10:26', NULL),
(156, 58, 86, 103, 21, 10, 1500000, 1, 1, 36, '2017-04-24 06:10:26', 1, '2017-04-24 12:10:26', NULL),
(157, 58, 86, 104, 21, 10, 1500000, 1, 1, 37, '2017-04-24 06:10:27', 1, '2017-04-24 12:10:27', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_account_log`
--

CREATE TABLE IF NOT EXISTS `user_account_log` (
  `USER_LG_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `CITIZEN_ID` bigint(20) DEFAULT NULL,
  `TE_APP_ID` bigint(20) DEFAULT NULL,
  `FULL_NAME` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `SPOUSE_NAME` varchar(50) CHARACTER SET utf8 NOT NULL,
  `FULL_NAME_BN` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `GENDER` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `MOBILE` varchar(50) CHARACTER SET utf8 NOT NULL,
  `NID` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `EMAIL` varchar(50) DEFAULT NULL,
  `DIVISION_ID` tinyint(2) NOT NULL,
  `DISTRICT_ID` int(8) NOT NULL,
  `THANA_ID` smallint(8) NOT NULL,
  `PO_ID` smallint(6) DEFAULT NULL,
  `ROAD_NO` varchar(50) CHARACTER SET utf8 NOT NULL,
  `HOLDING_NO` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `IS_ACTIVE` tinyint(1) NOT NULL DEFAULT '1',
  `CREATED_AT` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `CREATED_BY` bigint(20) DEFAULT NULL,
  `UPDATED_AT` datetime DEFAULT NULL,
  `UPDATED_BY` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`USER_LG_ID`),
  KEY `CITIZEN_ID` (`CITIZEN_ID`),
  KEY `TE_APP_ID` (`TE_APP_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `user_account_log`
--

INSERT INTO `user_account_log` (`USER_LG_ID`, `CITIZEN_ID`, `TE_APP_ID`, `FULL_NAME`, `SPOUSE_NAME`, `FULL_NAME_BN`, `GENDER`, `MOBILE`, `NID`, `EMAIL`, `DIVISION_ID`, `DISTRICT_ID`, `THANA_ID`, `PO_ID`, `ROAD_NO`, `HOLDING_NO`, `IS_ACTIVE`, `CREATED_AT`, `CREATED_BY`, `UPDATED_AT`, `UPDATED_BY`) VALUES
(1, 7, NULL, 'Abdul Awal', 'Maynuddin', NULL, 'm', '01738031018', '1245764215487', 'awal.ashu@gmail.com', 5, 6, 251, NULL, '15', '52', 1, '2017-04-14 18:38:15', 0, '2017-04-15 00:38:15', NULL);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `ac_particular_charge`
--
ALTER TABLE `ac_particular_charge`
  ADD CONSTRAINT `FK_PC_PARTICULAR_ID` FOREIGN KEY (`PARTICULAR_ID`) REFERENCES `ac_particular` (`PARTICULAR_ID`);

--
-- Constraints for table `ac_paymentmst`
--
ALTER TABLE `ac_paymentmst`
  ADD CONSTRAINT `ac_paymentmst_ibfk_1` FOREIGN KEY (`TRX_CODE_ID`) REFERENCES `ac_trxcode_info` (`TRX_CODE_ID`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_PM_USER_ID` FOREIGN KEY (`COLLECTED_BY`) REFERENCES `sa_users` (`USER_ID`),
  ADD CONSTRAINT `FK_PM_VOUCHER_NO` FOREIGN KEY (`VOUCHER_NO`) REFERENCES `ac_vouchermst` (`VOUCHER_NO`) ON DELETE CASCADE;

--
-- Constraints for table `ac_paymodeamt`
--
ALTER TABLE `ac_paymodeamt`
  ADD CONSTRAINT `FK_PA_TRX_TRAN_NO` FOREIGN KEY (`TRX_TRAN_NO`) REFERENCES `ac_paymentmst` (`TRX_TRAN_NO`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_PA_VOUCHER_NO` FOREIGN KEY (`VOUCHER_NO`) REFERENCES `ac_vouchermst` (`VOUCHER_NO`) ON DELETE CASCADE;

--
-- Constraints for table `ac_pgw_return`
--
ALTER TABLE `ac_pgw_return`
  ADD CONSTRAINT `ac_pgw_return_ibfk_1` FOREIGN KEY (`VOUCHER_NO`) REFERENCES `ac_vouchermst` (`VOUCHER_NO`) ON DELETE CASCADE,
  ADD CONSTRAINT `ac_pgw_return_ibfk_2` FOREIGN KEY (`TRX_TRAN_NO`) REFERENCES `ac_voucherchd` (`TRX_TRAN_NO`) ON DELETE CASCADE,
  ADD CONSTRAINT `ac_pgw_return_ibfk_3` FOREIGN KEY (`BANK_ID`) REFERENCES `sa_bank` (`BANK_ID`) ON DELETE CASCADE;

--
-- Constraints for table `ac_vn_ledgers`
--
ALTER TABLE `ac_vn_ledgers`
  ADD CONSTRAINT `ac_vn_ledgers_ibfk_1` FOREIGN KEY (`VOUCHER_NO`) REFERENCES `ac_vouchermst` (`VOUCHER_NO`) ON DELETE CASCADE,
  ADD CONSTRAINT `ac_vn_ledgers_ibfk_2` FOREIGN KEY (`TRX_TRAN_NO`) REFERENCES `ac_voucherchd` (`TRX_TRAN_NO`) ON DELETE CASCADE,
  ADD CONSTRAINT `ac_vn_ledgers_ibfk_3` FOREIGN KEY (`TRX_CODE_ID`) REFERENCES `ac_trxcode_info` (`TRX_CODE_ID`) ON DELETE CASCADE;

--
-- Constraints for table `ac_voucherchd`
--
ALTER TABLE `ac_voucherchd`
  ADD CONSTRAINT `ac_voucherchd_ibfk_1` FOREIGN KEY (`SCHEDULE_ID`) REFERENCES `tender_schedule` (`SCHEDULE_ID`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_VC_PARTICULAR_ID` FOREIGN KEY (`PARTICULAR_ID`) REFERENCES `ac_particular` (`PARTICULAR_ID`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_VC_VOUCHER_NO` FOREIGN KEY (`VOUCHER_NO`) REFERENCES `ac_vouchermst` (`VOUCHER_NO`) ON DELETE CASCADE;

--
-- Constraints for table `ac_vouchermst`
--
ALTER TABLE `ac_vouchermst`
  ADD CONSTRAINT `ac_vouchermst_ibfk_1` FOREIGN KEY (`LEASE_DE_ID`) REFERENCES `lease_demesne` (`LEASE_DE_ID`) ON DELETE CASCADE,
  ADD CONSTRAINT `ac_vouchermst_ibfk_2` FOREIGN KEY (`CITIZEN_ID`) REFERENCES `sa_citizen` (`CITIZEN_ID`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_VM_TE_APP_ID` FOREIGN KEY (`TE_APP_ID`) REFERENCES `tender_applicant` (`TE_APP_ID`) ON DELETE CASCADE;

--
-- Constraints for table `applicant_property`
--
ALTER TABLE `applicant_property`
  ADD CONSTRAINT `applicant_property_ibfk_1` FOREIGN KEY (`TE_APP_ID`) REFERENCES `tender_applicant` (`TE_APP_ID`) ON DELETE CASCADE,
  ADD CONSTRAINT `applicant_property_ibfk_10` FOREIGN KEY (`PR_DETAIL_ID`) REFERENCES `project_details` (`PR_DETAIL_ID`) ON DELETE CASCADE,
  ADD CONSTRAINT `applicant_property_ibfk_11` FOREIGN KEY (`PR_CATEGORY`) REFERENCES `sa_category` (`PR_CATEGORY`) ON DELETE CASCADE,
  ADD CONSTRAINT `applicant_property_ibfk_12` FOREIGN KEY (`PR_TYPE`) REFERENCES `sa_type` (`PR_TYPE`) ON DELETE CASCADE,
  ADD CONSTRAINT `applicant_property_ibfk_13` FOREIGN KEY (`PROJECT_ID`) REFERENCES `project` (`PROJECT_ID`) ON DELETE CASCADE,
  ADD CONSTRAINT `applicant_property_ibfk_2` FOREIGN KEY (`TENDER_ID`) REFERENCES `tender` (`TENDER_ID`) ON DELETE CASCADE,
  ADD CONSTRAINT `applicant_property_ibfk_3` FOREIGN KEY (`SCHEDULE_ID`) REFERENCES `tender_schedule` (`SCHEDULE_ID`) ON DELETE CASCADE,
  ADD CONSTRAINT `applicant_property_ibfk_4` FOREIGN KEY (`PR_DETAIL_ID`) REFERENCES `project_details` (`PR_DETAIL_ID`) ON DELETE CASCADE,
  ADD CONSTRAINT `applicant_property_ibfk_5` FOREIGN KEY (`PR_CATEGORY`) REFERENCES `sa_category` (`PR_CATEGORY`) ON DELETE CASCADE,
  ADD CONSTRAINT `applicant_property_ibfk_6` FOREIGN KEY (`PR_TYPE`) REFERENCES `sa_type` (`PR_TYPE`) ON DELETE CASCADE,
  ADD CONSTRAINT `applicant_property_ibfk_7` FOREIGN KEY (`TE_APP_ID`) REFERENCES `tender_applicant` (`TE_APP_ID`) ON DELETE CASCADE,
  ADD CONSTRAINT `applicant_property_ibfk_8` FOREIGN KEY (`TENDER_ID`) REFERENCES `tender` (`TENDER_ID`) ON DELETE CASCADE,
  ADD CONSTRAINT `applicant_property_ibfk_9` FOREIGN KEY (`SCHEDULE_ID`) REFERENCES `tender_schedule` (`SCHEDULE_ID`) ON DELETE CASCADE;

--
-- Constraints for table `land`
--
ALTER TABLE `land`
  ADD CONSTRAINT `FK_AREA` FOREIGN KEY (`AREA`) REFERENCES `sa_ward` (`WARD_ID`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_LAND_MOUZA_ID` FOREIGN KEY (`MOUZA_ID`) REFERENCES `sa_mouza` (`MOUZA_ID`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_LAND_WARD_ID` FOREIGN KEY (`WARD_ID`) REFERENCES `sa_ward` (`WARD_ID`) ON DELETE CASCADE;

--
-- Constraints for table `land_case`
--
ALTER TABLE `land_case`
  ADD CONSTRAINT `land_case_ibfk_1` FOREIGN KEY (`LAND_ID`) REFERENCES `land` (`LAND_ID`) ON DELETE CASCADE;

--
-- Constraints for table `land_dag_no`
--
ALTER TABLE `land_dag_no`
  ADD CONSTRAINT `land_dag_no_ibfk_1` FOREIGN KEY (`LAND_ID`) REFERENCES `land` (`LAND_ID`) ON DELETE CASCADE;

--
-- Constraints for table `land_docs`
--
ALTER TABLE `land_docs`
  ADD CONSTRAINT `land_docs_ibfk_1` FOREIGN KEY (`LAND_ID`) REFERENCES `land` (`LAND_ID`) ON DELETE CASCADE;

--
-- Constraints for table `land_tax`
--
ALTER TABLE `land_tax`
  ADD CONSTRAINT `land_tax_ibfk_1` FOREIGN KEY (`LAND_ID`) REFERENCES `land` (`LAND_ID`) ON DELETE CASCADE;

--
-- Constraints for table `land_uses`
--
ALTER TABLE `land_uses`
  ADD CONSTRAINT `LAND_USES_IBFK_2` FOREIGN KEY (`LAND_CATEGORY`) REFERENCES `land_category` (`CAT_ID`) ON DELETE CASCADE,
  ADD CONSTRAINT `LAND_USES_IBFK_3` FOREIGN KEY (`LAND_SUBCATEGORY`) REFERENCES `land_category` (`CAT_ID`) ON DELETE CASCADE;

--
-- Constraints for table `lease_demesne`
--
ALTER TABLE `lease_demesne`
  ADD CONSTRAINT `lease_demesne_ibfk_1` FOREIGN KEY (`SCHEDULE_ID`) REFERENCES `tender_schedule` (`SCHEDULE_ID`) ON DELETE CASCADE;

--
-- Constraints for table `lease_validity`
--
ALTER TABLE `lease_validity`
  ADD CONSTRAINT `lease_validity_ibfk_1` FOREIGN KEY (`TE_APP_ID`) REFERENCES `tender_applicant` (`TE_APP_ID`) ON DELETE CASCADE,
  ADD CONSTRAINT `lease_validity_ibfk_2` FOREIGN KEY (`SCHEDULE_ID`) REFERENCES `tender_schedule` (`SCHEDULE_ID`) ON DELETE CASCADE,
  ADD CONSTRAINT `lease_validity_ibfk_4` FOREIGN KEY (`LEASE_DE_ID`) REFERENCES `lease_demesne` (`LEASE_DE_ID`) ON DELETE CASCADE,
  ADD CONSTRAINT `lease_validity_ibfk_5` FOREIGN KEY (`CITIZEN_ID`) REFERENCES `sa_citizen` (`CITIZEN_ID`) ON DELETE CASCADE;

--
-- Constraints for table `project_details`
--
ALTER TABLE `project_details`
  ADD CONSTRAINT `FK_DE_PROJECT_ID` FOREIGN KEY (`PROJECT_ID`) REFERENCES `project` (`PROJECT_ID`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_DE_PR_CATEGORY` FOREIGN KEY (`PR_CATEGORY`) REFERENCES `sa_category` (`PR_CATEGORY`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_DE_PR_TYPE` FOREIGN KEY (`PR_TYPE`) REFERENCES `sa_type` (`PR_TYPE`) ON DELETE CASCADE;

--
-- Constraints for table `project_docs`
--
ALTER TABLE `project_docs`
  ADD CONSTRAINT `project_docs_ibfk_1` FOREIGN KEY (`PROJECT_ID`) REFERENCES `project` (`PROJECT_ID`) ON DELETE CASCADE;

--
-- Constraints for table `project_land`
--
ALTER TABLE `project_land`
  ADD CONSTRAINT `project_land_ibfk_1` FOREIGN KEY (`PROJECT_ID`) REFERENCES `project` (`PROJECT_ID`) ON DELETE CASCADE,
  ADD CONSTRAINT `project_land_ibfk_2` FOREIGN KEY (`LAND_ID`) REFERENCES `land` (`LAND_ID`) ON DELETE CASCADE;

--
-- Constraints for table `sa_bank`
--
ALTER TABLE `sa_bank`
  ADD CONSTRAINT `sa_bank_ibfk_1` FOREIGN KEY (`DISTRICT_ID`) REFERENCES `sa_districts` (`DISTRICT_ID`) ON DELETE CASCADE;

--
-- Constraints for table `sa_category_conditions`
--
ALTER TABLE `sa_category_conditions`
  ADD CONSTRAINT `FK_SC_PR_CATEGORY` FOREIGN KEY (`PR_CATEGORY`) REFERENCES `sa_category` (`PR_CATEGORY`) ON DELETE CASCADE;

--
-- Constraints for table `sa_category_particulars`
--
ALTER TABLE `sa_category_particulars`
  ADD CONSTRAINT `FK_SCP_PARTICULAR_ID` FOREIGN KEY (`PARTICULAR_ID`) REFERENCES `ac_particular` (`PARTICULAR_ID`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_SCP_PR_CATEGORY` FOREIGN KEY (`PR_CATEGORY`) REFERENCES `sa_category` (`PR_CATEGORY`) ON DELETE CASCADE;

--
-- Constraints for table `sa_citizen`
--
ALTER TABLE `sa_citizen`
  ADD CONSTRAINT `FK_CIT_USER_ID` FOREIGN KEY (`USER_ID`) REFERENCES `sa_users` (`USER_ID`) ON DELETE CASCADE,
  ADD CONSTRAINT `sa_citizen_ibfk_1` FOREIGN KEY (`DIVISION_ID`) REFERENCES `sa_divisions` (`DIVISION_ID`) ON DELETE CASCADE,
  ADD CONSTRAINT `sa_citizen_ibfk_2` FOREIGN KEY (`DISTRICT_ID`) REFERENCES `sa_districts` (`DISTRICT_ID`) ON DELETE CASCADE,
  ADD CONSTRAINT `sa_citizen_ibfk_3` FOREIGN KEY (`THANA_ID`) REFERENCES `sa_thanas` (`THANA_ID`) ON DELETE CASCADE,
  ADD CONSTRAINT `sa_citizen_ibfk_4` FOREIGN KEY (`TE_APP_ID`) REFERENCES `tender_applicant` (`TE_APP_ID`) ON DELETE CASCADE;

--
-- Constraints for table `sa_citizen_application`
--
ALTER TABLE `sa_citizen_application`
  ADD CONSTRAINT `sa_citizen_application_ibfk_1` FOREIGN KEY (`CITIZEN_ID`) REFERENCES `sa_citizen` (`CITIZEN_ID`) ON DELETE CASCADE,
  ADD CONSTRAINT `sa_citizen_application_ibfk_2` FOREIGN KEY (`TENDER_ID`) REFERENCES `tender` (`TENDER_ID`) ON DELETE CASCADE,
  ADD CONSTRAINT `sa_citizen_application_ibfk_3` FOREIGN KEY (`SCHEDULE_ID`) REFERENCES `tender_schedule` (`SCHEDULE_ID`) ON DELETE CASCADE,
  ADD CONSTRAINT `sa_citizen_application_ibfk_4` FOREIGN KEY (`PROJECT_ID`) REFERENCES `project` (`PROJECT_ID`) ON DELETE CASCADE,
  ADD CONSTRAINT `sa_citizen_application_ibfk_5` FOREIGN KEY (`PR_DETAIL_ID`) REFERENCES `project_details` (`PR_DETAIL_ID`) ON DELETE CASCADE,
  ADD CONSTRAINT `sa_citizen_application_ibfk_6` FOREIGN KEY (`PR_CATEGORY`) REFERENCES `sa_category` (`PR_CATEGORY`) ON DELETE CASCADE,
  ADD CONSTRAINT `sa_citizen_application_ibfk_7` FOREIGN KEY (`PR_TYPE`) REFERENCES `sa_type` (`PR_TYPE`) ON DELETE CASCADE;

--
-- Constraints for table `sa_citizen_property`
--
ALTER TABLE `sa_citizen_property`
  ADD CONSTRAINT `FK_CP_PROJECT_ID` FOREIGN KEY (`PROJECT_ID`) REFERENCES `project` (`PROJECT_ID`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_CP_PR_CATEGORY` FOREIGN KEY (`PR_CATEGORY`) REFERENCES `sa_category` (`PR_CATEGORY`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_CP_PR_TYPE` FOREIGN KEY (`PR_TYPE`) REFERENCES `sa_type` (`PR_TYPE`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_CP_TENDER_ID` FOREIGN KEY (`TENDER_ID`) REFERENCES `tender` (`TENDER_ID`) ON DELETE CASCADE,
  ADD CONSTRAINT `sa_citizen_property_ibfk_2` FOREIGN KEY (`SCHEDULE_ID`) REFERENCES `tender_schedule` (`SCHEDULE_ID`) ON DELETE CASCADE;

--
-- Constraints for table `sa_districts`
--
ALTER TABLE `sa_districts`
  ADD CONSTRAINT `SA_DIST_DIVISION_ID` FOREIGN KEY (`DIVISION_ID`) REFERENCES `sa_divisions` (`DIVISION_ID`);

--
-- Constraints for table `sa_mouza`
--
ALTER TABLE `sa_mouza`
  ADD CONSTRAINT `SA_MOUZA_WARD_ID` FOREIGN KEY (`WARD_ID`) REFERENCES `sa_ward` (`WARD_ID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `sa_notifications`
--
ALTER TABLE `sa_notifications`
  ADD CONSTRAINT `sa_notifications_ibfk_1` FOREIGN KEY (`TE_APP_ID`) REFERENCES `tender_applicant` (`TE_APP_ID`) ON DELETE CASCADE,
  ADD CONSTRAINT `sa_notifications_ibfk_2` FOREIGN KEY (`CITIZEN_ID`) REFERENCES `sa_citizen` (`CITIZEN_ID`) ON DELETE CASCADE,
  ADD CONSTRAINT `sa_notifications_ibfk_3` FOREIGN KEY (`LEASE_DE_ID`) REFERENCES `lease_demesne` (`LEASE_DE_ID`) ON DELETE CASCADE,
  ADD CONSTRAINT `sa_notifications_ibfk_4` FOREIGN KEY (`SCHEDULE_ID`) REFERENCES `tender_schedule` (`SCHEDULE_ID`) ON DELETE CASCADE,
  ADD CONSTRAINT `sa_notifications_ibfk_5` FOREIGN KEY (`PARTICULAR_ID`) REFERENCES `ac_particular` (`PARTICULAR_ID`) ON DELETE CASCADE,
  ADD CONSTRAINT `sa_notifications_ibfk_6` FOREIGN KEY (`TRX_TRAN_NO`) REFERENCES `ac_voucherchd` (`TRX_TRAN_NO`) ON DELETE CASCADE,
  ADD CONSTRAINT `sa_notifications_ibfk_7` FOREIGN KEY (`USER_ID`) REFERENCES `sa_users` (`USER_ID`) ON DELETE CASCADE;

--
-- Constraints for table `sa_permission_role`
--
ALTER TABLE `sa_permission_role`
  ADD CONSTRAINT `sa_permission_role_permission_id_foreign` FOREIGN KEY (`PERMISSION_ID`) REFERENCES `sa_permissions` (`PERM_ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `sa_permission_role_role_id_foreign` FOREIGN KEY (`ROLE_ID`) REFERENCES `sa_roles` (`ROLE_ID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `sa_postoffices`
--
ALTER TABLE `sa_postoffices`
  ADD CONSTRAINT `SA_POST_THANA_ID` FOREIGN KEY (`THANA_ID`) REFERENCES `sa_thanas` (`THANA_ID`);

--
-- Constraints for table `sa_role_user`
--
ALTER TABLE `sa_role_user`
  ADD CONSTRAINT `sa_role_user_role_id_foreign` FOREIGN KEY (`ROLE_ID`) REFERENCES `sa_roles` (`ROLE_ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `sa_role_user_user_id_foreign` FOREIGN KEY (`USER_ID`) REFERENCES `sa_users` (`USER_ID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `sa_thanas`
--
ALTER TABLE `sa_thanas`
  ADD CONSTRAINT `SA_THANA_DISTRICT_ID` FOREIGN KEY (`DISTRICT_ID`) REFERENCES `sa_districts` (`DISTRICT_ID`);

--
-- Constraints for table `sa_user_details`
--
ALTER TABLE `sa_user_details`
  ADD CONSTRAINT `sa_user_details_ibfk_1` FOREIGN KEY (`USER_ID`) REFERENCES `sa_users` (`USER_ID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `security_refund`
--
ALTER TABLE `security_refund`
  ADD CONSTRAINT `security_refund_ibfk_1` FOREIGN KEY (`TE_APP_ID`) REFERENCES `tender_applicant` (`TE_APP_ID`) ON DELETE CASCADE,
  ADD CONSTRAINT `security_refund_ibfk_2` FOREIGN KEY (`CITIZEN_ID`) REFERENCES `sa_citizen` (`CITIZEN_ID`) ON DELETE CASCADE,
  ADD CONSTRAINT `security_refund_ibfk_3` FOREIGN KEY (`TRX_TRAN_NO`) REFERENCES `ac_voucherchd` (`TRX_TRAN_NO`) ON DELETE CASCADE,
  ADD CONSTRAINT `security_refund_ibfk_4` FOREIGN KEY (`TENDER_ID`) REFERENCES `tender` (`TENDER_ID`) ON DELETE CASCADE,
  ADD CONSTRAINT `security_refund_ibfk_5` FOREIGN KEY (`SCHEDULE_ID`) REFERENCES `tender_schedule` (`SCHEDULE_ID`) ON DELETE CASCADE,
  ADD CONSTRAINT `security_refund_ibfk_6` FOREIGN KEY (`PROJECT_ID`) REFERENCES `project` (`PROJECT_ID`) ON DELETE CASCADE,
  ADD CONSTRAINT `security_refund_ibfk_7` FOREIGN KEY (`PR_DETAIL_ID`) REFERENCES `project_details` (`PR_DETAIL_ID`) ON DELETE CASCADE,
  ADD CONSTRAINT `security_refund_ibfk_8` FOREIGN KEY (`PR_CATEGORY`) REFERENCES `sa_category` (`PR_CATEGORY`) ON DELETE CASCADE,
  ADD CONSTRAINT `security_refund_ibfk_9` FOREIGN KEY (`PR_TYPE`) REFERENCES `sa_type` (`PR_TYPE`) ON DELETE CASCADE;

--
-- Constraints for table `tender_applicant`
--
ALTER TABLE `tender_applicant`
  ADD CONSTRAINT `tender_applicant_ibfk_1` FOREIGN KEY (`DISTRICT_ID`) REFERENCES `sa_districts` (`DISTRICT_ID`) ON DELETE CASCADE,
  ADD CONSTRAINT `tender_applicant_ibfk_2` FOREIGN KEY (`THANA_ID`) REFERENCES `sa_thanas` (`THANA_ID`) ON DELETE CASCADE,
  ADD CONSTRAINT `tender_applicant_ibfk_3` FOREIGN KEY (`DIVISION_ID`) REFERENCES `sa_divisions` (`DIVISION_ID`) ON DELETE CASCADE;

--
-- Constraints for table `tender_conditions`
--
ALTER TABLE `tender_conditions`
  ADD CONSTRAINT `tender_conditions_ibfk_1` FOREIGN KEY (`TENDER_ID`) REFERENCES `tender` (`TENDER_ID`) ON DELETE CASCADE,
  ADD CONSTRAINT `tender_conditions_ibfk_2` FOREIGN KEY (`PR_CATEGORY`) REFERENCES `sa_category` (`PR_CATEGORY`) ON DELETE CASCADE;

--
-- Constraints for table `tender_date_time`
--
ALTER TABLE `tender_date_time`
  ADD CONSTRAINT `tender_date_time_ibfk_1` FOREIGN KEY (`TENDER_ID`) REFERENCES `tender` (`TENDER_ID`) ON DELETE CASCADE;

--
-- Constraints for table `tender_installment`
--
ALTER TABLE `tender_installment`
  ADD CONSTRAINT `tender_installment_ibfk_1` FOREIGN KEY (`TE_APP_ID`) REFERENCES `tender_applicant` (`TE_APP_ID`) ON DELETE CASCADE,
  ADD CONSTRAINT `tender_installment_ibfk_2` FOREIGN KEY (`SCHEDULE_ID`) REFERENCES `tender_schedule` (`SCHEDULE_ID`) ON DELETE CASCADE,
  ADD CONSTRAINT `tender_installment_ibfk_3` FOREIGN KEY (`TRX_TRAN_NO`) REFERENCES `ac_voucherchd` (`TRX_TRAN_NO`) ON DELETE CASCADE,
  ADD CONSTRAINT `tender_installment_ibfk_4` FOREIGN KEY (`CITIZEN_ID`) REFERENCES `sa_citizen` (`CITIZEN_ID`) ON DELETE CASCADE;

--
-- Constraints for table `tender_latter`
--
ALTER TABLE `tender_latter`
  ADD CONSTRAINT `tender_latter_ibfk_1` FOREIGN KEY (`TE_APP_ID`) REFERENCES `tender_applicant` (`TE_APP_ID`) ON DELETE CASCADE,
  ADD CONSTRAINT `tender_latter_ibfk_2` FOREIGN KEY (`SCHEDULE_ID`) REFERENCES `tender_schedule` (`SCHEDULE_ID`) ON DELETE CASCADE,
  ADD CONSTRAINT `tender_latter_ibfk_3` FOREIGN KEY (`PARTICULAR_ID`) REFERENCES `ac_particular` (`PARTICULAR_ID`) ON DELETE CASCADE;

--
-- Constraints for table `tender_location`
--
ALTER TABLE `tender_location`
  ADD CONSTRAINT `FK_LOC_LOCATION_ID` FOREIGN KEY (`LOCATION_ID`) REFERENCES `sa_location` (`LOCATION_ID`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_LOC_TENDER_ID` FOREIGN KEY (`TENDER_ID`) REFERENCES `tender` (`TENDER_ID`) ON DELETE CASCADE;

--
-- Constraints for table `tender_particulars`
--
ALTER TABLE `tender_particulars`
  ADD CONSTRAINT `FK_TP_PARTICULAR_ID` FOREIGN KEY (`PARTICULAR_ID`) REFERENCES `ac_particular` (`PARTICULAR_ID`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_TP_SCHEDULE_ID` FOREIGN KEY (`SCHEDULE_ID`) REFERENCES `tender_schedule` (`SCHEDULE_ID`) ON DELETE CASCADE;

--
-- Constraints for table `tender_schedule`
--
ALTER TABLE `tender_schedule`
  ADD CONSTRAINT `FK_SH_PROJECT_ID` FOREIGN KEY (`PROJECT_ID`) REFERENCES `project` (`PROJECT_ID`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_SH_PR_CATEGORY` FOREIGN KEY (`PR_CATEGORY`) REFERENCES `sa_category` (`PR_CATEGORY`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_SH_PR_TYPE` FOREIGN KEY (`PR_TYPE`) REFERENCES `sa_type` (`PR_TYPE`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_SH_TENDER_ID` FOREIGN KEY (`TENDER_ID`) REFERENCES `tender` (`TENDER_ID`) ON DELETE CASCADE,
  ADD CONSTRAINT `tender_schedule_ibfk_1` FOREIGN KEY (`PR_DETAIL_ID`) REFERENCES `project_details` (`PR_DETAIL_ID`) ON DELETE CASCADE;

--
-- Constraints for table `user_account_log`
--
ALTER TABLE `user_account_log`
  ADD CONSTRAINT `user_account_log_ibfk_1` FOREIGN KEY (`CITIZEN_ID`) REFERENCES `sa_citizen` (`CITIZEN_ID`) ON DELETE CASCADE,
  ADD CONSTRAINT `user_account_log_ibfk_2` FOREIGN KEY (`TE_APP_ID`) REFERENCES `tender_applicant` (`TE_APP_ID`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
