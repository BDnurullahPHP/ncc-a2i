<?php

return [
    // সায়রাত মহালের নাম
    'sayarata_mahal'                => 'সায়রাত মহাল',
    'sayarata_mahal_list'           => 'সায়রাত মহালের তালিকা',

    'add_mahal_title'               => 'নতুন মহাল যোগ করুন',
    'mahal_lend'                    => 'মহালের জমি',
    'mahal_number'                  => 'মহালের নাম্বার',
    'mahal_bn_name'                 => 'মহালের নাম',
    'mahal_bn_ex'                   => 'কালীর বাজার',
    'mahal_en_name'                 => 'মহালের নাম (English)',
    'mahal_address'                 => 'মহালের ঠিকানা',
    'mahal_exists'                  => 'মহালের এই তথ্যটি বিদ্যমান',
    'edit_mahal_title'              => 'মহালের তথ্য হালনাগাদ করুন',
    'mahal_details_info'            => 'মহালের বিস্তারিত তথ্য',
    'mohal_list'                    => 'মহাল সমূহ',

    // lease
    'lease'                         => 'ইজারার',
    'lease_management'              => 'ইজারার ব্যবস্থাপনা',
    'lease_dropdown_list'           => 'ইজারার তালিকা',
    'lease_dtls_dropdown_list'      => 'ইজারার বিবরণী তালিকা',

    // lease category
    'lease_dropdown_category'       => 'ইজারার শ্রেণী',
    'lease_heading_categorys'       => 'ইজারার শ্রেণী সমূহ',
    'lease_category_name'           => 'ইজারার শ্রেণীর নাম',
    'lease_category_des'            => 'ইজারার শ্রেণীর বর্ণনা',
    'lease_category_exists'         => 'ইজারার শ্রেণী বিদ্যমান',
    'lease_category_name_ex'        => 'Ex. স্পেস/ফ্ল্যাট/দোকান',
    'lease_category_name_des_ex'    => 'Ex. পদ্ম সিটি প্লাজা-১ এসএম মালেহ রোড',

    // lease type
    'lease_dropdown_type'           => 'ইজারার ধরন',
    'lease_heading_types'           => 'ইজারার ধরন সমূহ',
    'lease_type_name'               => 'ইজারার ধরনের নাম',
    'lease_type_des'                => 'ইজারার ধরনের বর্ণনা',
    'lease_type_exists'             => 'ইজারার ধরন বিদ্যমান',
    'lease_type_name_ex'            => 'Ex.টাইপ-A/টাইপ-B/টাইপ-C',
    'lease_type_des_ex'             => 'Ex.পদ্ম সিটি প্লাজা-১ এসএম মালেহ রোড,টাইপ-A,৯৬৮.৮২',

    // lease category condition
    'lease_dropdown_cat_con'        => 'ইজারার শ্রেণীর শর্তাবলী ',
    'lease_heading_cat_cons'        => 'ইজারার শ্রেণীর শর্ত সমূহ',
    'lease_cat_con_name'            => 'ইজারার শ্রেণীর শর্তের শিরোনাম',
    'lease_cat_con_des'             => 'ইজারার শ্রেণীর শর্তের বর্ণনা',
    'lease_cat_con_exists'          => 'ইজারার শ্রেণীর শর্ত বিদ্যমান',
    'lease_cat_con_name_ex'         => 'Ex.বাংলাদেশের প্রকৃত নাগরিকগন আবেদন করতে পারবে',
    'lease_cat_con_des_ex'          => 'Ex.আবেদনকারীকে নারায়ণগঞ্জ সিটি কর্পোরেশনের নির্ধারিত ফরমে আবেদন করতে হবে।',

    // lease location
    'lease_heading_locations'       => 'ইজারার স্থান সমূহ',
    'lease_dropdown_location'       => 'ইজারার স্থান',
    'lease_location_name'           => 'ইজারার স্থানের নাম',
    'lease_location_des'            => 'ইজারার স্থানের বর্ণনা',
    'lease_location_name_ex'        => 'Ex.এসএম মালেহ রোড ',
    'lease_location_des_ex'         => 'Ex.পদ্ম সিটি প্লাজা-১ এসএম মালেহ রোড,নারায়ণগঞ্জ',
    'lease_duration'                => 'ইজারা সময়কাল',

    'lease_information'             => 'ইজারার তথ্য',
    'flat_and_shop'                 => 'ফ্ল্যাট এবং দোকান ব্যবস্থাপনা',
    'flat_space_and_shop_list'      => 'ফ্ল্যাট, স্পেস এবং দোকানের তালিকা',
    'flat_space_and_shop_info'      => 'ফ্ল্যাট, স্পেস এবং দোকানের তথ্য',
    'lease_details_type'            => 'ধরন',
    'lease_details_type_pl'         => 'Ex. Type A/Unit A/Shop 1',
    'lease_details_position'        => 'অবস্থান',
    'lease_details_position_pl'     => 'Ex. 1',
    'lease_position_help'           => 'প্রথম তলার জন্য ১',
    'lease_pr_ssf_no'               => 'দোকান/স্পেস বা ফ্ল্যাট নাম্বার',
    'lease_pr_ssf_no_pl'            => 'Ex. 2F1',
    'lease_details_size'            => 'আয়তন',
    'lease_details_size_pl'         => 'Ex. 1200',
    'is_reserved'                   => 'সংরক্ষিত',
    'is_build'                      => 'নির্মিত',
    'lease_exists'                  => 'ইজারার বিদ্যমান',
    'lease_notice'                  => 'ইজারার বিজ্ঞপ্তি',
    'lease_info'                    => 'ইজারার তথ্য',
    'lease_mohal'                   => 'সয়রাত মহালের নাম',
    'lease_value'                   => 'দরপত্র মূল্য',
    'booking_amount'                => 'বায়নার পরিমান',
    'lease_comparison_list'         => 'ইজারার তুলনামূলক তালিকা',
    'lease_applicant_list'          => 'ইজারার আবেদনকারীর তালিকা',
    'is_primary_exist'              => 'এই তথ্যটি বিদ্যমান',
    're_tender'                     => 'ইহা কি পুনঃদরপত্র ?',
    'is_sharok_exist'               => 'এই স্মারক নং টি বিদ্যমান',
    'is_schedule_exist'             => 'এই মহাল টি বিদ্যমান',

    //খাস এর তথ্য
    'demesne_list'                  => 'খাস এর তালিকা',
    'demesne_list_title'            => 'খাসকৃত সায়রাত মহালের তালিকা',
    'demesne_title'                 => 'খাসের তথ্য যোগ করুন',
    'tender_number'                 => 'দরপত্রের নাম্বার',
    'collector_name'                => 'আদায় কারির নাম',
    'collector_father_name'         => 'আদায় কারির পিতার নাম',
    'collector_mobile'              => 'আদায় কারির মোবাইল',
    'collector_address'             => 'আদায় কারির ঠিকানা',

    //খাস এর টাকা সংগ্রহ
    'demesne_money'                 => 'খাসের টাকা সংগ্রহ',
    'demesne_list_money_title'      => 'খাসের টাকা সংগ্রহের তালিকা',
    'demesne_money_collection'      => 'টাকার পরিমাণ',

    //ইজারার রিপোর্ট
    'lease_repor'                   => 'ইজারার রিপোর্ট',
    //'demesne_list_money_title'      => 'খাসের টাকা সংগ্রহের তালিকা',
    //'demesne_money_collection'      => 'টাকার পরিমাণ',

    //lease management

    'start_date'                     => 'শুরুর তারিখ',
    'end_date'                       => 'শেষ তারিখ',
    'date'                           => 'তারিখ',
    'lease_applicant_information'    => 'ইজারা আবেদনকারীর তথ্য',
   'open_date'                       => 'প্রকাশের তারিখ',
    'sale_last_date'                 => 'বিক্রয়ের শেষ তারিখ',
    'open_date_time'                 => 'খোলার তারিখ ও সময়',
        //final selection application list
    'applicant_name'                 => 'আবেদনকারীর নাম',
    'expected_money'                 => 'প্রত্যাশিত টাকা',
    'money_received'                 => 'প্রাপ্ত টাকা',
    'due_money'                      => 'বকেয়া টাকা',
    'khaas_term'                     => 'খাসের  মেয়াদ',
    'comparison_of_lease_information'=> 'ইজারার তুলানামুলক তথ্য',

    //report lease revenue colleciton
    'revenue_collection_details' => 'রাজস্ব আদায় এর বিবরণ',
    'mahal_name'                 => 'মহালের নাম',
    'lease_customer_name'        => 'ইজারা প্রাপ্ত গ্রাহকের নাম',
    'lease_value'                => 'ইজারার মূল্য',
];
