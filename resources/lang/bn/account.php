<?php

return [
    // Account Particuler
    'particular_list'                   => 'পার্টিকুলার সমূহ',
    'particular_title'                  => 'নতুন পার্টিকুলার যোগ করুন',
    'particular_title_edit'             => 'পার্টিকুলারের তথ্য হালনাগাদ করুন',
    'particular_name'                   => 'পার্টিকুলারের নাম',
    'particular_description'            => 'পার্টিকুলারের বর্ণনা',
    'eg_particular_name'                => 'উদাহরণঃ ভ্যাট',
    'particular_exists'                 => 'পার্টিকুলারের নাম বিদ্যমান',
    'particular_uom'                    => 'পরিমাপের একক (টাকা)',
    'particular_uom_per'                => 'পরিমাপের একক (শতাংশ)',
    'particular_type_exists'            => 'পার্টিকুলার এর তথ্যটি বিদ্যমান',

    //Account Management
    'account_management'                => 'হিসাব ব্যবস্থাপনা',
    'applicant_name'                    => 'আবেদনকারীর নাম',
    'project_name'                      => 'প্রকল্পের নাম',

    // Category particular
    'particular_cra_title'              => 'পার্টিকুলারের শ্রেণীর তথ্য যোগ করুন',
    'particular_cra_title_edit'         => 'পার্টিকুলারের শ্রেণীর তথ্য হালনাগাদ করুন',
    'category_particular' 				          => 'শ্রেণীর পার্টিকুলার',
    'category_name' 					               => 'শ্রেণীর নাম',
    'particular_amount'                 => 'টাকার পরিমাণ',
    'particular_amount_pl'              => 'টাকার পরিমাণ/শতকরা হার',
    'particular_amount_ex'              => 'শ্রেণীর পার্টিকুলারের টাকার পরিমাণ/শতকরা হার',
    'installment_pay_list'              => 'আবেদনকারীর কিস্তি পরিশোধের তালিকা',
    'applicant'                         => 'আবেদনকারী',
    'installment_approved' 				         => 'টাকা সফলভাবে গ্রহণ করা হয়েছে',

    // Receive Register
    'receive_register'                  => 'রিসিভ রেজিস্টার',
    'revenue_list'                      => 'রাজস্ব খাতের তালিকা',
    'tender_ins_payment'                => 'দরপত্রের কিস্তি পরিশোধের তালিকা',
    'payment_info'                      => 'অর্থ প্রদানের তথ্য',
    'money_collection'                  => 'টাকা সংগ্রহ',
    'citizen_collection_ledger'         => 'নাগরিক খতিয়ান',
    'tender_apply'                      => 'দরপত্রের আবেদন',

    'getting_from_whom' => 'যাহার নিকট হতে প্রাপ্ত',
    'receipt_no'        => 'রশিদ নং',
    'receipt_date'      => 'রশিদের তারিখ',
    'receipt_type'      => 'কি বাবদ পাওয়া গেল',
    'amount_money'      => 'টাকার পরিমাণ',

        //owner ship change
    'client_mobile' => 'গ্রাহকের মোবাইল',
    'paper_name'    => 'খাতের নাম',
    'tender'        => 'দরপত্র',
    'type'          => 'ধরন',

        //account manager
    'Daily_income_statement'                                 => 'দৈনিক আয়ের বিবরণী',
    'Store_space_flat_lease_statistics'                      => 'দোকান / স্পেস / ফ্ল্যাট / ইজারা এর পরিসংখ্যান',
    'tt'                                                     => 'টি',
    'Total_expected'                                         => 'মোট প্রত্যাশিত',
    'Gross_income'                                           => 'মোট আয়',
    'Total_owes'                                             => 'মোট বকেয়া',
    'View_more'                                              => 'আরো দেখুন',
    'Playlist_Citizens_List'                                 => 'খেলাপকারী নাগরিকের তালিকা',
        'In_store_space_flat_lease_income_statistics_in_year'=> 'সালে দোকান / স্পেস / ফ্ল্যাট / ইজারা এর আয়ের পরিসংখ্যান',
        'sl_no'                                              => 'ক্রমিক',
    'name'                                                   => 'নাম',
    'project'                                                => 'প্রকল্প',
    'Amount_of_money'                                        => 'টাকার পরিমাণ',

];
