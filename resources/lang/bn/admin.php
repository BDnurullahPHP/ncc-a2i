<?php

return [
     'Narayanganj_City_Corporation'  => 'নারায়নগঞ্জ সিটি কর্পোরেশন',
     'Expected_Revenue'              => 'প্রত্যাশিত রাজস্ব',
    'dashboard' 					                => 'ড্যাশবোর্ড',
    'dashboard_heading' 			          => 'বিবরণ ও পরিসংখ্যান',
    'total_expeted_revenue' 		       => 'মোট প্রত্যাশিত রাজস্ব',
    'total_collect_revenue' 		       => 'মোট রাজস্ব আদায়',
    'total_due_revenue' 			          => 'মোট রাজস্ব বাকি',

    // Project Manager Dashboard
    'total_project'      => 'মোট প্রকল্প',
    'ready_project'      => 'রেডি প্রকল্প',
    'under_con_project'  => 'নির্মানাধীন প্রকল্প',

    'Comparative_description_of_the_expected_and_realized_revenue' => 'প্রত্যাশিত ও আদায়কৃত রাজস্বের তুলনামূূলক বিবরণ',
    'Expected_Revenue'				                                         => 'Expected Revenue',
    'Revenue_collection'			                                        => 'Revenue Collection',
    'details_about_the_flats'		                                    => 'ফ্ল্যাট সমূহ এর বিবরণ',
    'ready_flat'					                                              => 'রেডি ফ্ল্যাট',
    'tt'							                                                    => 'টি',
    'under_construction'			                                        => 'নির্মানাধীন',
    'allotment'						                                              => 'বরাদ্দ',
    'Store_details'					                                           => 'দোকান সমূহ এর বিবরণ',
    'Ready_Store'					                                             => 'রেডি দোকান',
    'Store_allocation'				                                         => 'দোকান বরাদ্দ',
    'details_of_the_spaces'			                                     => 'স্পেস সমূহ এর বিবরণ',
    'ready_space'					                                             => 'রেডি স্পেস',
    'Space_allocation'				                                         => 'স্পেস বরাদ্দ',
    'description_of_the_syarat_mahal'                              => 'সায়রাত মহালের বিবরণ',
    'Syarat_Mahal'					                                            => 'সায়রাত মহাল',
    'Lease'							                                                 => 'ইজারা',
    'Land_information'				                                         => 'জমির তথ্য',
    'Total_land_(percent)'			                                      => 'মোট জমি (শতাংশ)',
    'Occupancy_land_(percent)'		                                   => 'দখল জমি (শতাংশ)',
    'Occupied_land'		                                              => 'বেদখল জমি (শতাংশ)',

        //footer
    'overall_collaboration'			             => 'সার্বিক সহযোগিতায়',
    'Copyright'						                      => 'কপিরাইট',
    'Narayanganj_City_Corporation_and_A2i' => 'নারায়নগঞ্জ সিটি কর্পোরেশন ও এটুআই',
    'Design_and_development'		             => 'ডিজাইন ও ডেভেলপমেন্ট',

        //project manager
    'total_project'					=> 'মোট প্রকল্প',
    'Total_store'					  => 'মোট দোকান',
    'Total_Flat'					   => 'মোট ফ্ল্যাট',
    'Total_Space'					  => 'মোট স্পেস',

        //lease manager
    'Total_Tender'					               => 'মোট দরপত্র',
    'Running_tender'				              => 'চলমান দরপত্র',
    'The_initially_selected_applicant'=> 'প্রাথমিকভাবে নির্বাচিত আবেদনকারী',
    'Finally_selected_applicant'	     => 'চূড়ান্তভাবে নির্বাচিত আবেদনকারী',
    'Month_applicant_details'		       => 'Month_applicant_details',
    'Description_of_leases'			        => 'ইজারা সমূহ এর বিবরণ',
    'gradual'						                   => 'ক্রমিক',
    'Mahal_name'					                 => 'মহালের নাম',
    'Mahal_address'					              => 'মহালের ঠিকানা',
    'Security'						                  => 'জামানত',
    'Earnest_money'					              => 'বায়না',
    'Tender_price'					               => 'দরপত্রের মূল্য',
    'VAT'							                      => 'ভ্যাট',
    'Income_tax'					                 => 'আয়কর বাবদ',
     'Of_the_year'					               => 'সালের',
    'Current'						                   => 'চলতি',
    'Month_applicant_details'		       => 'মাসের আবেদনকারীর বিবরণ',
    'View_more'						                 => 'আরো দেখুন',

        //teder
    'Details_of_ongoing_tender'		   => 'চলমান দরপত্র সমূহ এর বিবরণ',
    'Market_name'					              => 'মার্কেটের নাম',
    'Flat_shop_Space_no'			         => 'ফ্ল্যাট/দোকান স্পেসের নং',
    'Measure_square_feet'			        => 'পরিমাপ (বর্গফুট)',
    'Minimum_salami_Amount'			      => 'সর্বনিম্ন সালামীর<br>পরিমান',
    'Every_square_foot_Monthly_rent'=> 'প্রতি বর্গফুটের মাসিক ভাড়া',
    'Schedule_Price'				            => 'শিডিউলের মূল্য',
    'The_amount_of_security'		      => 'জামানতের পরিমান',
    'Application_deadline'			       => 'আবেদনের  শেষ তারিখ',

     //home
    'all'							             => 'সকল',
    'Download'						         => 'ডাউনলোড',
    'Apply'							           => 'আবেদন',
    'Social_communication'			=> 'সামাজিক যোগাযোগ ',
        //apply
    'Its_conditions'				=> 'এর শর্ত সমূহ',
    'I_agree'						     => 'আমি একমত',

];
