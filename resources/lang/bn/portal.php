<?php

return [
        // base
   'online_flat'                    => 'অনলাইনে ফ্ল্যাট, দোকান, স্পেস বরাদ্দ ও ইজারা প্রদান সম্পর্কিত সফটওয়্যার',
    'home'                          => 'হোম',
    'contact'                       => 'যোগাযোগ',
    'tender_notice'                 => 'টেন্ডার বিজ্ঞপ্তি',
    'lease_notice'                  => 'ইজারা বিজ্ঞপ্তি',
    'usage_guidelines'              => 'ব্যবহার নির্দেশিকা',
    'views'                         => 'মতামত',
        //footer
    'copyright'                      => 'কপিরাইট',
    'NARAYANGANJ_CITY_CORPORATION'   => 'নারায়নগঞ্জ সিটি কর্পোরেশন',
    'a2i'                            => 'এটুআই',
    'design_and_development'         => 'ডিজাইন ও ডেভেলপমেন্ট',
    'o'                              => 'ও',
     'photo_galary'                  => 'ফটো গ্যালারি',
     'contact_information'           => 'যোগাযোগের তথ্য',

         //tender notice space
    'sl_no'                                    => 'ক্রঃনংঃ',
    'market_name'                              => 'মার্কেটের নাম',
    'type'                                     => 'ধরণ',
    'measuring'                                => 'পরিমাপ &#40;বর্গ ফুট&#41;',
    'minimum_salami_mount_(money)'             => 'সর্বনিম্ন সালামীর পরিমান (টাকা)',
    'every_square_footMonthly_rent_(money)'    => 'প্রতি বর্গফুটের মাসিক ভাড়া (টাকা)',
    'Schedule_Price_(Taka)_Each_Non-refundable'=> 'শিডিউলের মূল্য (টাকা) &#40;প্রতিটি অফেরতযোগ্য&#41;',
    'The_amount_of_security_Submission_bid'    => 'জামানতের পরিমান &#40;দাখিলকৃত দরের&#41;',
    'Activity'                                 => 'কার্যকলাপ',

        //lease notice
    'Narayanganj_City_Corporation_Regulation_Hat_bazar/_Syarat_Mahal'=> 'নারায়ণগঞ্জ সিটি কর্পোরেশন নিয়ন্ত্রাধিন হাট-বাজার/সায়রাত মহাল',
    'year_lease_notification'                                        => 'সনের ইজারা বিজ্ঞপ্তি',
    'Point_no'                                                       => 'দফা নং',
    'Name_of_the_syarat_mahal'                                       => 'সায়রাত মহালের নাম',
    'Mahal_location'                                                 => 'মহালের অবস্থান',
    'The_amount_of_winding_(Suggested_value)_(Percent)'              => 'বায়নার পরিমান &#40;প্রস্তাবিত মূল্যর&#41; &#40;শতাংশ&#41;',
    'Tender_price_(Non-refundable)_(Money)'                          => 'দরপত্র মূল্য &#40;অফেরতযোগ্য&#41;&#40;টাকা&#41;',
    'Details'                                                        => 'বিস্তারিত',
    'Activity'                                                       => 'কার্যকলাপ',

    'Notice_of'                            => 'এর বিজ্ঞপ্তি',
    'T'                                    => 'টি',
    'There_are_currently_no_notifications' => 'বর্তমানে কোন বিজ্ঞপ্তি নাই',

    //feedbacek

    'Message'                        => 'বার্তা',
    'Message_label'                  => 'আপনার বার্তা',
     'send'                          => 'প্রেরণ করুন',
         //contace

    'name'                          => 'নাম',
    'enter_your_name_here'          => 'আপনার নাম এখানে লিখুন',
    'your_name'                     => 'আপনার নাম',
    'mobile_number'                 => 'মোবাইল নাম্বার:',
    'enter_your_mobile_number_here' => 'আপনার মোবাইল নাম্বার এখানে লিখুন',
    'your_mobile_number'            => 'আপনার মোবাইল নাম্বার',
    'email'                         => 'ইমেইল :',
    'Enter_your_email_here_write'   => 'আপনার ইমেইল এখানে লিখুন',
    'Enter_your_email_here'         => 'আপনার ইমেইল এখানে লিখুন',
    'Enter_your_email'              => 'আপনার ইমেইল লিখুন',
    'message'                       => 'বার্তা :',
    'Enter_your_message_here'       => 'আপনার বার্তা এখানে লিখুন',
    'your_message'                  => 'Your Message',
];
