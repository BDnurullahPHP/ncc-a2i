<?php

return [
    'mm_security_access'            => 'সিকউরিটি এ্যান্ড অ্যাক্সেস',
    'user_role'                     => 'ইউজার রোল',
    'role_list'                     => 'রোল সমূহ',
    'all_role'                      => 'সকল রোল',
    'role_exists'                   => 'রোলটি বিদ্যমান',
    'role_name'                     => 'রোলের নাম',
    'role_details'                  => 'রোলের বিবরণ',
    'role_select'                   => 'রোল নির্বাচন করুন',
    'all_permission'                => 'সকল অনুমতি',
    'permission_list'               => 'অনুমতি সমুহ',
    'permission_role'               => 'রোলের অনুমতি',
    'permission_name'               => 'অনুমতির নাম',
    'assign_permission'             => 'অনুমতি অর্পণ করা',
    'display_name'                  => 'প্রদর্শন নাম',
    'permission_details'            => 'অনুমতির বিবরণ',
    'permission_exists'             => 'অনুমতিটি বিদ্যমান',
    'permission_create_message'     => 'অনুমতিটি সফলভাবে সংরক্ষিত হয়েছে',

    'system_logs'                   => 'সিস্টেম লগ',
];
