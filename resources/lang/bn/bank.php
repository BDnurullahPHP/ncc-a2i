<?php

return [
    'bank_title'                => 'ব্যাংকের তথ্য যোগ করুন',
    'bank_title_edit'           => 'ব্যাংকের তথ্য হালনাগাদ করুন',
    'bank_list'                 => 'ব্যাংক সমূহ',
    'bank_name'                 => 'ব্যাংকের নাম',
    'parent_bank'               => 'ব্যাংকের প্রধান শাখা',
    'bank_address'              => 'ব্যাংকের ঠিকানা ',
    'enter_bank_name'           => 'ব্যাংকের ঠিকানা এন্ট্রি দিন',
    'select_bank_name'          => 'ব্যাংকের নাম সিলেক্ট করুন',
    'enter_bank_address'        => 'ব্যাংকের ঠিকানাএন্ট্রি দিন',
    'bank_create_message'       => 'সফলভাবে সংরক্ষিত হয়েছে',
    'bank_exits'                => 'ব্যাংকের নাম বিদ্যমান',
    'bank_name_ex'              => 'Ex.বাংলাদেশ ব্যাংক',
    'bank_address_ex'           => 'Ex.Plot: 73, Blok B, Kemal Ataturk Ave, Dhaka 1213, Bangladesh ',
];
