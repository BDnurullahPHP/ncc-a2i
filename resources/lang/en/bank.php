<?php

return [
    'bank_title'                => 'Add New Bank',
    'bank_title_edit'           => 'Edit Bank Info',
    'bank_list'                 => 'Bank List',
    'bank_name'                 => 'Name',
    'parent_bank'               => 'Parent Bank',
    'bank_address'              => 'Bank Address',
    'enter_bank_name'           => 'Enter Bank Name',
    'select_bank_name'          => 'Enter Bank Name',
    'enter_bank_address'        => 'Enter Address',
    'bank_create_message'       => 'Successfully saved',
    'bank_exits'                => 'Bank Name already exist',
    'bank_name_ex'              => 'Ex.বাংলাদেশ ব্যাংক',
    'bank_address_ex'           => 'Ex.Plot: 73, Blok B, Kemal Ataturk Ave, Dhaka 1213, Bangladesh ',
];
