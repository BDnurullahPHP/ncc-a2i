<?php

return [
    'mm_security_access'            => 'Security &amp; Access',
    'user_role'                     => 'User role',
    'role_list'                     => 'Role list',
    'all_role'                      => 'All Role',
    'role_exists'                   => 'Role exists',
    'role_name'                     => 'Role name',
    'role_details'                  => 'Role details',
    'role_select'                   => 'Select role',
    'all_permission'                => 'All Permission',
    'permission_list'               => 'Permission list',
    'permission_role'               => 'Permission role',
    'permission_name'               => 'Permission name',
    'assign_permission'             => 'Assign permission',
    'display_name'                  => 'Display name',
    'permission_details'            => 'Permission details',
    'permission_exists'             => 'Permission exists',
    'permission_create_message'     => 'Permission successfully created',

    'system_logs'                   => 'System logs',
];
