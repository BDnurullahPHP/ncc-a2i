<?php

return [
    // base
    'online_flat'                      => 'Online Flat Store Space Allotment and Lease Related Software',
    'home'                             => 'Home',
    'contact'                          => 'Contact',
    'tender_notice'                    => 'Tender Notice',
    'lease_notice'                     => 'Lease Notice',
    'usage_guidelines'                 => 'Usage Guidelines',
    'views'                            => 'Feedback',

    //footer
    'copyright'                     => 'Copyright',
    'NARAYANGANJ_CITY_CORPORATION'  => 'NARAYANGANJ CITY CORPORATION',
    'a2i'                           => 'a2i',
    'design_and_development'        => 'Design and Development',
    'o'                             => 'And',
    'photo_galary'                  => 'Photo Galary',
    'contact_information'           => 'Contact Information',
    //tender notice space
    'sl_no'                                    => 'Sl No',
    'market_name'                              => 'Market Name',
    'type'                                     => 'Type',
    'measuring'                                => 'Measuring',
    'minimum_salami_mount_(money)'             => 'Minimum Salami Amount (Money)',
    'every_square_footMonthly_rent_(money)'    => 'Every Square Foot Monthly Rent (Money)',
    'Schedule_Price_(Taka)_Each_Non-refundable'=> 'Schedule Price (Taka) Each Non-refundable',
    'The_amount_of_security_Submission_bid'    => 'The Amount of Security Submission Bid',
    'Activity'                                 => 'Activity',

    //lease notice
    'Narayanganj_City_Corporation_Regulation_Hat_bazar/_Syarat_Mahal'=> 'Narayanganj City Corporation Regulation Hat Bazar / Syarat Mahal',
    'year_lease_notification'                                        => 'Year Lease Notification',
    'Point_no'                                                       => 'Point No',
    'Name_of_the_syarat_mahal'                                       => 'Name of The Syarat Mahal',
    'Mahal_location'                                                 => 'Mahal Location',
    'The_amount_of_winding_(Suggested_value)_(Percent)'              => 'The Amount of Winding (Suggested Value) (Percent)',
    'Tender_price_(Non-refundable)_(Money)'                          => 'Tender Price (Non-refundable) (Money)',
    'Details'                                                        => 'Details',
    'Activity'                                                       => 'Activity',

    'Notice_of'                            => 'Notice Of',
    'T'                                    => '',
    'There_are_currently_no_notifications' => 'There are currently no notifications',
        //feedbacek

    'Message'                       => 'Message',
    'Message_label'                 => 'Your Message',
    'send'                          => 'Send',
    //contace

    'name'                          => 'Name :',
    'enter_your_name_here'          => 'Enter Your Name Here',
    'your_name'                     => 'Your Name',
    'mobile_number'                 => 'Mobile Number',
    'enter_your_mobile_number_here' => 'Enter Your Mobile Number Here',
    'your_mobile_number'            => 'Your Mobile Number',
    'email'                         => 'Email :',
    'Enter_your_email_here_write'   => 'Enter your email here',
    'Enter_your_email_here'         => 'Enter your email here',
    'Enter_your_email'              => 'Enter your email',
    'message'                       => 'Message',
    'Enter_your_message_here'       => 'Enter your message here',
    'your_message'                  => 'Your Message',

];
