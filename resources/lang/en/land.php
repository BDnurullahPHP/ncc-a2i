<?php

return [
    // Land Category
    'land_category_title' 		        => 'Add New Land category',
    'land_category_title_edit'      => 'Edit Land Category',
    'land_dropdown_category' 		     => 'Land category',
    'land_category_name'            => 'Category name',
    'land_category_bn_name'         => 'Category name (বাংলা)',
    'land_category_en_name_ex'      => 'Ex. Revenue',
    'land_category_bn_name_ex'      => 'Ex. রাজস্ব',

    // Land Sub-Category
    'land_subcategory_title' 		     => 'Add New Land Subcategory',
    'land_subcategory_title_edit'   => 'Edit Land Subcategory',
    'land_subcategory_name'         => 'Subcategory name',
    'land_subcategory_bn_name'      => 'Subcategory name (বাংলা)',
    'land_subcategory_en_name_ex'   => 'Ex. Road',
    'land_subcategory_bn_name_ex'   => 'Ex. রাস্তা',

    'land_dropdown_label' 			       => 'Land management',
    'land_dropdown_list'            => 'List',
    'land_title'                    => 'Land title',
    'land_number' 			               => 'Land number',
    'land_dropdown_subcategory'     => 'Land subcategory',
    'land_information'              => 'Land information',
    'land_mouza_name'               => 'Mouza name',
    'land_khotian_no'               => 'Ledger no',
    'land_khotian_no_pl'            => 'Land ledger no',
    'land_dag_no'                   => 'Dag no',
    'land_dag_no_pl'                => 'Land dag no',
    'land_land_type'                => 'Land type',
    'land_total_land'               => 'Total land(percent)',
    'land_total_land_pl'            => 'Total land',
    'land_total_possession'         => 'Possession(percent)',
    'land_total_possession_pl'      => 'Total possession land',
    'land_ousted_land'              => 'Ousted(percent)',
    'land_ousted_land_pl'           => 'Total ousted land',
    'land_details'                  => 'Details',
    'land_details_pl'               => 'Land details',
    'land_form_title'               => 'Create new land',
    'land_form_owner'               => 'land owner',
    'land_form_division'            => 'Division',
    'land_form_district'            => 'District',
    'land_form_upazila'             => 'Upazila',
    'land_form_mouza'               => 'Mouza',
    'land_form_jl_no'               => 'JL no',
    'land_form_jl_no_pl'            => 'Land JL no',
    'land_form_khatian_no'          => 'Khatian no',
    'land_form_khatian_no_pl'       => 'Land khatian no',
    'khatian_no_rs'                 => 'Khatian number',
    'dag_no_rs'                     => 'Dag number',
    'jl_no_rs'                      => 'JL number',
    'owner_rs'                      => 'Owner',
    'land_form_category'            => 'Category',
    'land_form_subcategory'         => 'Subcategory',
    'land_form_use_category'        => 'Land use category',
    'land_form_use_subcategory'     => 'Land use subcategory',
    'land_form_rs'                  => 'RS',
    'land_form_rs_pl'               => 'Land RS',
    'land_form_cs'                  => 'CS',
    'land_form_cs_pl'               => 'Land CS',
    'land_form_sa'                  => 'SA',
    'land_form_sa_pl'               => 'Land SA',
    'land_form_land_use'            => 'Land use',
    'land_form_details'             => 'Land details',
    'land_form_is_case'             => 'Is land has case',
    'land_form_document'            => 'Land document',
    'land_form_dcr'                 => 'Land DCR',
    'land_form_ledger'              => 'Land ledger',
    'land_form_bahia'               => 'Bahia document',
    'land_form_tax_receipt'         => 'Land tax receipt ',
    'land_form_tax_date'            => 'Receipt date',
    'land_exists'                   => 'Land exists',
    'land_details_info'             => 'Land Details Information',
    'cat_name_exists'               => 'Category name exists',
    'cat_bn_name_exists'            => 'Category bangla name exists',
    'subcat_name_exists'            => 'Subcategory name exists',
    'subcat_bn_name_exists'         => 'Subcategory bangla name exists',
    'land_zone'                     => 'Zone',
    'land_ward'                     => 'Ward',
    'add_data'                      => 'Add Land information',
    'is_lease'                      => 'Lease?',
    'case_details'                  => 'Case details',
    'case_details_pl'               => 'Case details information',
    'land_dag'                      => 'Land Dag',
    'case_number'                   => 'Case number',
    'case_complainant'              => 'Case complainant',
    'case_defendant'                => 'Case defendant',
    'case_settlement_date'          => 'Case settlement date',
    'court_document'                => 'Court document',
    'land_report'                   => 'Land report',
    'add_land_use'                  => 'Add',
    'yes'                           => 'Yes',
    'no'                            => 'No',

    //zone
    'zone_title'                    => 'Add New Zone',
    'zone_title_edit'               => 'Edit Zone Info',
    'zone'                          => 'Zone',
    'zone_name'                     => 'Zone Name',
    'zone_bn_name'                  => 'Zone Name (বাংলা)',
    'zone_name_ex'                  => 'Ex.Zone-1/Zone-2',
    'zone_bn_name_ex'               => 'Ex.জোন-১/জোন-২',
    'zone_name_exists'              => 'Zone Name Exists',
    'zone_ban_name_exists'          => 'Zone Name Exists',

    //ward
    'ward_title'                    => 'Add New Ward',
    'ward_title_edit'               => 'Edit Ward Info',
    'ward'                          => 'Ward',
    'ward_name'                     => 'Ward Name',
    'ward_bn_name'                  => 'Ward Name (বাংলা)',
    'ward_name_ex'                  => 'Ex.Ward 01/Ward 02',
    'ward_bn_name_ex'               => 'Ex.ওয়ার্ড ০১/ওয়ার্ড ০২',
    'ward_name_exists'              => 'Ward Name exists',
    'ward_bn_name_exists'           => 'Ward Name (বাংলা ) exists',

    //Mouza
    'mouza_title'                   => 'Add New Mouza',
    'mouza_title_edit'              => 'Edit Mouza Info',
    'mouza_list'                    => 'Mouza',
    'mouza_name'                    => 'Mouza Name',
    'mouza_bn_name'                 => 'Mouza Name (বাংলা)',
    'jl_number'                     => 'JL Number',
    'select_word'                   => 'Word Name',
    'select_zone'                   => 'Zone Name',
    'mouza_name_ex'                 => 'Ex.Shimrail',
    'mouza_bn_name_ex'              => 'Ex.শিমরাইল',
    'jl_number_ex'                  => 'Ex.2345',
    'mouza_name_exists'             => 'This Mouza info Exists',

    //Land
    'land'                          => 'Land Description',
    'land_use'                      => 'Land Use Description',
    'land_doc'                      => 'Land Documents',
    'land_dags'                     => 'Land RS/CS/SA Descripton',
];
