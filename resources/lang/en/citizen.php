<?php

return [
    'rent_management'                   => 'Rent management',
    'rent_information'                  => 'Rent information',
    'rent_payment'                      => 'Rent payment',
    'rent_history'                      => 'Rent history',
    'application_overview' 				         => 'Application overview',
    'payment_history' 					             => 'Payment history',
    'pay' 								                      => 'Pay',
    'app_form_info'						               => 'Application form information',
    'installment_history'               => 'Installment History',
    'installment_number'                => 'Installment Number',
    'installment_amount'                => 'Installment Amount',
    'installment_date'                  => 'Installment pay Date',
    'installment_pay_status'            => 'Installment Status',
    'installment_status'                => 'Payment Status',
    'application_status'                => 'Application status',
    'payment_information'               => 'Payment information',
    'citizen_service'                   => 'Citizen service',
    'ownership_change_or_cancel'        => 'Ownership change/cancel',

    //money colloection
    'collect_customer_money'            => 'Collect Customer Money',
    'customer_mobile'                   => 'Customer Mobile',
    'sector_name'                       => 'Sector name',
    'tender'                            => 'Tender',
    'income_source'                     => 'Income Source',
    'search'                            => 'Search',

    //report revinue details
    'revenue_sector_list'               => 'Revenue Sector List',
    'year'                              => 'Year',
    'month'                             => 'Month',
    'select'                            => 'Select',
    'january'                           => 'January',
    'february'                          => 'February',
    'march'                             => 'March',
    'april'                             => 'April',
    'may'                               => 'May',
    'jun'                               => 'Jun',
    'july'                              => 'July',
    'august'                            => 'August',
    'september'                         => 'September',
    'october'                           => 'October',
    'november'                          => 'November',
    'december'                          => 'December',
    'customer_name'                     => 'Customer name',
    'sl_no'                             => 'SL No',
    'memo_no_date'                      => 'Memo No and Date',
    'payable_money'                     => 'Payable Money',
    'money_received'                    => 'Money Received',
    'due_money'                         => 'Due Money',
    'description_of_revenue_collection' => 'Description of Revenue Collection',

    //citizen ledger
    'start_date'                        => 'Start Date',
    'end_date'                          => 'End Date',
    'customer_mobile'                   => 'Customer Mobile',

    //ledger revenue collection
    'details_of_the_revenue'            => 'Details of The Revenue',
    'mahal_name'                        => 'Mahal Name',
    'lease'                             => 'Lease',
    'c_year'                            => 'Year',
    'name_of_the_lease_holder'          => 'Name of The Lease Holder',
    'lease_value'                       => 'Lease Value',

    //apply online
    'Apply'                                    => 'Apply',
    'Application_form'                         => 'Application Form',
    'Applicant_Information'                    => 'Applicant Information',
    'National_identity_card_birth_registration'=> 'National Id Card / Birth Registration',
    'division'                                 => 'Division',
    'District'                                 => 'District',
    'Upazila_Thana'                            => 'Upazila Thana',
    'road_no'                                  => 'Road No',
    'holding_no'                               => 'Holding No',
    'mobile_numbermobile_number'               => 'Mobile Number',
    'email'                                    => 'Email',
    'password'                                 => 'Password',
    'LeaseT'                                   => 'Lease',
    'Bidding_rate_by_the_borrower'             => 'Bidding Rate By The Borrower',
    'amount_of_winding'                        => 'Amount Of Winding',
    'The_amount_of_security'                   => 'The Amount Of Security',
    'TenderT'                                  => 'Tender',

    'a_name'                                                                         => 'Name',
    'Father_Husband_name'                                                            => 'Father / Husband Name',
    'address'                                                                        => 'Address',
    'Payment_Deadline'                                                               => 'Payment Deadline',
    'But_due_to_the_condition_of_your_installment_due_to_the_condition_of_the_tender'=> 'But Due To The Condition Of Your Installment Due To The Condition Of The Tender',
    'total_taka'                                                                     => 'Total Taka',
    'Payment_Types'                                                                  => 'Payment Types',
    'Pay_order_number'                                                               => 'Pay Order Number',
    'Pay_order_bank_name'                                                            => 'Pay Order Bank Name',
    'Bank_branch_name'                                                               => 'Bank Branch Name',
    'Pay_order_date'                                                                 => 'Pay Order Date',
    'Pay_order_attachment'                                                           => 'Pay Order Attachment',
    'Invoice_No'                                                                     => 'Invoice No',
    'date'                                                                           => 'date',
    'Attachment'                                                                     => 'Attachment',
    'Attachment_type'                                                                => 'Attachment Type jpg, jpeg, png',
    'comment'                                                                        => 'Comment',
    'pay_order_Attachment_type'                                                      => 'Attachment Type : jpg, jpeg, png',
    'Enter_the_pay_order_number'                                                     => 'Enter The Pay Order Number',
    'Enter_the_invoice_number'                                                       => 'Enter The Invoice Number',

];
