<?php

return [
    // সায়রাত মহালের নাম
    'sayarata_mahal'                => 'Sayarata Mahals',
    'sayarata_mahal_list'           => 'List of Sayarata Mahal',

    'add_mahal_title'               => 'New Mahal Info Add',
    'mahal_lend'                    => 'Mahal Land',
    'mahal_number'                  => 'Mahal Number',
    'mahal_bn_name'                 => 'Mahal Name(বাংলা)',
    'mahal_en_name'                 => 'Mahal Name',
    'mahal_address'                 => 'Mahal Address',
    'mahal_ex'                      => 'Kalir Bazaar',
    'mahal_exists'                  => 'This Info Exists',
    'edit_mahal_title'              => 'Edit Mahal Info',
    'mahal_details_info'            => 'Mahal Details Info',
    'mohal_list'                    => 'Mahal list',

    // Lease category
    'Lease'                         => 'Lease',
    'lease_management'              => 'Lease Management',
    'lease_dropdown_list'           => 'Lease List',
    'lease_dtls_dropdown_list'      => 'Lease details list',

    // Lease category
    'lease_dropdown_category'       => 'Lease Category',
    'lease_heading_categorys'       => 'Lease Categorys',
    'lease_dropdown_location'       => 'Lease Location',
    'lease_category_name'           => 'Lease Category Name',
    'lease_category_des'            => 'Lease Category Description',
    'lease_category_exists'         => 'Lease Category exists',
    'lease_category_name_ex'        => 'Ex. স্পেস/ফ্ল্যাট/দোকান',
    'lease_category_name_des_ex'    => 'Ex. পদ্ম সিটি প্লাজা-১ এসএম মালেহ রোড',

    // Lease type
    'lease_dropdown_type'           => 'Lease Type',
    'lease_heading_types'           => 'Lease Types',
    'lease_dropdown_location'       => 'Lease Location',
    'lease_type_name'               => 'Lease Type Name',
    'lease_type_des'                => 'Lease Type Description',
    'lease_type_exists'             => 'Lease Type exists',
    'lease_type_name_ex'            => 'Ex.টাইপ A/টাইপ B/টাইপ C',
    'lease_type_des_ex'             => 'Ex.পদ্ম সিটি প্লাজা-১ এসএম মালেহ রোড,টাইপ-A,৯৬৮.৮২',

    // Lease category condition
    'lease_dropdown_cat_con'        => 'Lease Category Condition ',
    'lease_heading_cat_cons'        => 'Lease Category Conditions',
    'lease_cat_con_name'            => 'Lease Category Condition Title',
    'lease_cat_con_des'             => 'Lease Category Condition Des.',
    'lease_cat_con_exists'          => 'Lease Category Condition Exists',
    'lease_cat_con_name_ex'         => 'Ex.বাংলাদেশের প্রকৃত নাগরিকগন আবেদন করতে পারবে',
    'lease_cat_con_des_ex'          => 'Ex.আবেদনকারীকে নারায়ণগঞ্জ সিটি কর্পোরেশনের নির্ধারিত ফরমে আবেদন করতে হবে।',

    // Lease location
    'lease_heading_locations'       => 'Lease Location',
    'lease_dropdown_location'       => 'Lease Location',
    'lease_location_name'           => 'Location Name',
    'lease_location_des'            => 'Location Description',
    'lease_location_name_ex'        => 'Ex.এসএম মালেহ রোড ',
    'lease_location_des_ex'         => 'Ex.পদ্ম সিটি প্লাজা-১ এসএম মালেহ রোড,নারায়ণগঞ্জ',
    'lease_duration'                => 'Lease duration',

    'lease_information'             => 'Lease Information',
    'lease_number'                  => 'Lease number',
    'lease_location'                => 'Lease location',
    'flat_and_shop'                 => 'Flat &amp; Shop Management',
    'flat_space_and_shop_list'      => 'Flat, Space &amp; Shop list',
    'flat_space_and_shop_info'      => 'Flat, Space &amp; Shop information',
    'lease_details_type'            => 'Type',
    'lease_details_type_pl'         => 'Ex. Type A/Unit A/Shop 1',
    'lease_details_position'        => 'Position',
    'lease_details_position_pl'     => 'Ex. 1',
    'lease_position_help'           => 'For first floor use: 1',
    'lease_pr_ssf_no'               => 'Shop/space or flat number',
    'lease_pr_ssf_no_pl'            => 'Ex. 2F1',
    'lease_details_size'            => 'Size',
    'lease_details_size_pl'         => 'Ex. 1200',
    'is_reserved'                   => 'Reserved',
    'is_build'                      => 'Build',
    'lease_exists'                  => 'Lease exists',
    'lease_info'                    => 'Lease Information',
    'lease_mohal'                   => 'Soyrat Mohaler Nam',
    'lease_value'                   => 'Tender Value',
    'booking_amount'                => 'Booking Amount',
    'is_primary_exist'              => 'This info Exists',
    're_tender'                     => 'Is it re-tender?',

    //lease management
    'collector_name'                    => 'Collector Name',
    'collector_mobile'                  => 'Collector Mobile',
    'collector_address'                 => 'Collector Address',
    'tender_number'                     => 'Tender Number',
    'start_date'                        => 'Start Date',
    'end_date'                          => 'End Date',
    'demesne_list_money_title'          => 'Demesne List Money Title',
    'demesne_money_collection'          => 'Demesne Money Collection and Date',
    'demesne_title'                     => 'Demesne Title',
    'date'                              => 'Date',
    'lease_applicant_information'       => 'Lease Applicant Information',
    'open_date'                         => 'Open Date',
    'sale_last_date'                    => 'Sale Last Date',
    'open_date_time'                    => 'Open Date Time',

    //final selection application list
    'applicant_name'                 => 'Applicant Name',
    'expected_money'                 => 'Expected Money',
    'money_received'                 => 'money Received',
    'due_money'                      => 'Due Money',
    'khaas_term'                     => 'Khaas Term',
    'comparison_of_lease_information'=> 'Comparison of Lease Information',

    //report lease revenue colleciton
    'details_of_the_revenue '    => 'Details of The Revenue',
    'revenue_collection_details' => 'Revenue Collection Details',
    'mahal_name'                 => 'Mahal Name',
    'lease_customer_name'        => 'Lease Customer Name',
    'lease_value'                => 'Lease Value',
];
