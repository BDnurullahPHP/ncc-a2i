<?php

return [
    // Account Particuler
    'particular_list'                  	=> 'Particular List',
    'particular_title'                 	=> 'Add New Particular',
    'particular_title_edit'             => 'Edit Particular Info',
    'particular_name'                  	=> 'Particular Name',
    'eg_particular_name'               	=> 'e.g. VAT',
    'particular_exists'                	=> 'This Particular Name exists',
    'particular_uom'                    => 'Unit (Taka)',
    'particular_uom_per'                => 'Unit (%)',
    'particular_type_exists'            => 'This Particular info exists',

     //Account Management
    'account_management'               	=> 'Account Management',
    'applicant_name'                    => 'Applicant Name',
    'project_name'                      => 'Project Name',

    // Category particular
    'particular_cra_title'              => 'Add New Particular Category',
    'particular_cra_title_edit'         => 'Edit Particular Category',
    'category_particular' 				          => 'Category particular',
    'category_name'			 		               => 'Category name',
    'particular_amount'                 => 'Charged amount',
    'particular_amount_pl'              => 'Charged amount/percentage',
    'installment_pay_list'              => 'Applicant installment payment list',
    'applicant'                         => 'Applicant',
    'installment_approved'			           => 'Successfully approved installment',

    // Receive Register
    'receive_register'                  => 'Receive Register',
    'revenue_list'                      => 'Revenue list',
    'tender_ins_payment'                => 'Tender installment payment',
    'payment_info'                      => 'payment information',
    'money_collection'                  => 'Money collection',
    'citizen_collection_ledger'         => 'Citizen ledger',
    'tender_apply'                      => 'Tender apply',

    'getting_from_whom' => 'Getting From whom',
    'receipt_no'        => 'Receipt No',
    'receipt_date'      => 'Receipt Date',
    'receipt_type'      => 'What is the key',
    'amount_money'      => 'Amount of money',

    //owner ship change
    'client_mobile' => 'Client Mobile',
    'paper_name'    => 'Paper Name',
    'tender'        => 'Tender',
    'type'          => 'Type',

    //account manager
    'Daily_income_statement'                             => 'Daily income statement',
    'Store_space_flat_lease_statistics'                  => 'Store Space Flat Lease Statistics',
    'tt'                                                 => '',
    'Total_expected'                                     => 'Total Expected',
    'Gross_income'                                       => 'Gross Income',
    'Total_owes'                                         => 'Total Owes',
    'View_more'                                          => 'View More',
    'Playlist_Citizens_List'                             => 'Playlist Citizens List',
    'In_store_space_flat_lease_income_statistics_in_year'=> 'In Store Space Flat Lease Income Statistics In Year',
    'sl_no'                                              => 'SL No',
    'name'                                               => 'Name',
    'project'                                            => 'Project',
    'Amount_of_money'                                    => 'Amount of Money',
];
