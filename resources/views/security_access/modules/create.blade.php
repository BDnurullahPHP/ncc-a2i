{!! Form::open(array('url' => '#', 'id'=>'land_category_form', 'class'=>'form-horizontal', 'method' => 'post')) !!}
<!--=============Title for modal===============-->
<div class="modal_top_title">মডিউলের তথ্য যোগ করুন</div>
<!--===========End title for modal=============-->

<!--========Show form post message=============-->
<div class="form-group">
    <div class="col-sm-5 col-sm-offset-3 form_messsage">

    </div>
</div>
<!--========End form post message=============-->

<div class="form-group">
    <label for="MODULE_NAME" class="col-sm-3 control-label">মডিউলের নাম <span style="color: red">*</span></label>
    <div class="col-sm-5">
        {!! Form::text('MODULE_NAME', Input::old('MODULE_NAME'), array('class'=>'form-control form-required')) !!}
    </div>
</div>
<div class="form-group">
    <label for="MODULE_NAME_BN" class="col-sm-3 control-label">মডিউলের নাম(বাংলা) <span style="color: red">*</span></label>
    <div class="col-sm-5">
        {!! Form::text('MODULE_NAME_BN', Input::old('MODULE_NAME_BN'), array('class'=>'form-control form-required')) !!}
    </div>
</div>
<div class="form-group">
    <label for="MODULE_ICON" class="col-sm-3 control-label">মডিউলের আইকন <span style="color: red">*</span></label>
    <div class="col-sm-5">
        {!! Form::text('MODULE_ICON', Input::old('MODULE_ICON'), array('class'=>'form-control form-required')) !!}
    </div>
</div>
<div class="hr-line-dashed"></div>
<div class="form-group">
    <div class="col-lg-offset-3 col-lg-10">
        <span class="modal_msg pull-left"></span>
        <button type="button" class="btn btn-default btn-sm reset_form">{{ trans('common.form_reset') }}</button>
        <button type="submit" data-action="{{ route('module_save') }}" class="btn btn-success btn-sm form_submit">{{ trans('common.form_submit') }}</button>
        <span class="loadingImg"></span>
    </div>
</div>
{!! Form::close() !!}