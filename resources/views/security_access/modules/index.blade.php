@extends('admin_access.layout.master')

@section('style')

@endsection

@section('content')

<h3>Module List
    @if($access->CREATE == 1)
    <a href="#" data-toggle="modal" data-target="#modal_add_content" class="btn btn-primary btn-sm pull-right" data-form="{{ route('module_create') }}">{{ trans('common.add_new') }} <i class="fa fa-plus"></i></a>
    @endif
</h3>
<div class="portlet light bordered">
    <div class="portlet-body contentArea">
        <table class="table table-striped table-bordered table-hover" data-source="{{ route('module_data') }}" id="common_table">
            <thead>
                <tr>
                    <th>{{ trans('common.table_sl') }}</th>
                    <th>মডিউলের নাম</th>
                    <th>মডিউলের নাম(বাংলা)</th>
                    <th>মডিউলের আইকন</th>
                    <th>{{ trans('common.table_action') }}</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
</div>

@endsection