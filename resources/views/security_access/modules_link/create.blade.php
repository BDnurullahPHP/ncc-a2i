{!! Form::open(array('url' => '#', 'id'=>'land_category_form', 'class'=>'form-horizontal', 'method' => 'post')) !!}
<!--=============Title for modal===============-->
<div class="modal_top_title">মডিউলের লিংকের তথ্য যোগ করুন</div>
<!--===========End title for modal=============-->

<!--========Show form post message=============-->
<div class="form-group">
    <div class="col-sm-5 col-sm-offset-3 form_messsage">

    </div>
</div>
<!--========End form post message=============-->

<div class="form-group">
    <label for="MODULE_ID" class="col-sm-3 control-label">মডিউল <span style="color: red">*</span></label>
    <div class="col-sm-5">
        {!! Form::select('MODULE_ID', $module, array(Input::old('MODULE_ID')), array('class'=>'form-control form-required', 'id'=>'MODULE_ID', 'placeholder' => trans('common.form_select'))) !!}
    </div>
</div>
<div class="form-group">
    <label for="LINK_NAME" class="col-sm-3 control-label">লিংকের নাম <span style="color: red">*</span></label>
    <div class="col-sm-5">
        {!! Form::text('LINK_NAME', Input::old('LINK_NAME'), array('class'=>'form-control form-required')) !!}
    </div>
</div>
<div class="form-group">
    <label for="LINK_NAME_BN" class="col-sm-3 control-label">লিংকের নাম(বাংলা) <span style="color: red">*</span></label>
    <div class="col-sm-5">
        {!! Form::text('LINK_NAME_BN', Input::old('LINK_NAME_BN'), array('class'=>'form-control form-required')) !!}
    </div>
</div>
<div class="form-group">
    <label for="LINK_URI" class="col-sm-3 control-label">লিংকের ইউ আর এল <span style="color: red">*</span></label>
    <div class="col-sm-5">
        {!! Form::text('LINK_URI', Input::old('LINK_URI'), array('class'=>'form-control form-required')) !!}
    </div>
</div>
<div class="hr-line-dashed"></div>
<div class="form-group">
    <div class="col-lg-offset-3 col-lg-10">
        <span class="modal_msg pull-left"></span>
        <button type="button" class="btn btn-default btn-sm reset_form">{{ trans('common.form_reset') }}</button>
        <button type="submit" data-action="{{ route('module_link_save') }}" class="btn btn-success btn-sm form_submit">{{ trans('common.form_submit') }}</button>
        <span class="loadingImg"></span>
    </div>
</div>
{!! Form::close() !!}