@extends('admin_access.layout.master')

@section('style')

@endsection

@section('content')

<h3>মডিউল লিংকের তালিকা
    @if($access->CREATE == 1)
    <a href="#" data-toggle="modal" data-target="#modal_add_content" class="btn btn-primary btn-sm pull-right" data-form="{{ route('module_create_link') }}">{{ trans('common.add_new') }} <i class="fa fa-plus"></i></a>
    @endif
</h3>
<div class="portlet light bordered">
    <div class="portlet-body contentArea">
        <table class="table table-striped table-bordered table-hover" data-source="{{ route('module_data_link') }}" id="common_table">
            <thead>
                <tr>
                    <th>{{ trans('common.table_sl') }}</th>
                    <th>মডিউলের নাম</th>
                    <th>লিংকের নাম</th>
                    <th>লিংকের নাম (বাংলা)</th>
                    <th>লিংকের ইউ আর এল</th>
                    <th>{{ trans('common.table_action') }}</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
</div>

@endsection