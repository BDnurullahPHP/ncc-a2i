@extends('admin_access.layout.master')

@section('style')

@endsection

@section('script')

<script src="{{ Helpers::asset('abc/jquery.bootstrap-duallistbox.min.js') }}" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="{{ Helpers::asset('abc/bootstrap-duallistbox.css') }}">

<script type="text/javascript">
$(document).ready(function() {
    $(document).on('click', '#assignModule', function() {
        var myVar = setInterval(function() {
            myTimer()
        }, 1000);
        function myTimer() {
            var demo2 = $('.demo2').bootstrapDualListbox({
                nonSelectedListLabel: 'Non-selected',
                selectedListLabel: 'Selected',
                preserveSelectionOnMove: 'moved',
                moveOnSelect: false,
                //nonSelectedFilter: 'ion ([7-9]|[1][0-2])'
            });
        }
    });
    $(document).on('click', '.moveall, .move', function() {
        var module = $('.demo2').val();
        var action = $('#addModule').val();
        $.ajax({
            url: action,
            type: "GET",
            dataType: 'json',
            data: {module: module},
            success: function(response) {
                if (response == 1) {
                    $('#message').text('Module Assign Successfully');
                }
                // you will get response from your php page (what you echo or print)                 
            },
        });
    });
    $(document).on('click', '.removeall, .remove', function() {
        var module = $('.demo2').val();
        var action = $('#removeModule').val();
        $.ajax({
            url: action,
            type: "GET",
            data: {module: module},
            success: function(response) {
                if (response == 1) {
                    $('#message').text('Module Remove Successfully');
                }
            },
        });
    });
    $(document).on("click", ".chkAssignPage", function() {
        var value = $(this).val();
        var checked = ($($(this)).is(':checked')) ? 1 : 2;
        var action = $('#addRemovePage').val();
        $.ajax({
            type: "GET",
            url: action,
            data: {values: value, is_checked: checked},
            success: function(result) {
                //alert(result);
            }
        });
    });
});
</script>


@endsection
@section('content')

<h3>Organization Module
<!--    <a href="#" data-toggle="modal" data-target="#modal_add_content" class="btn btn-primary btn-sm pull-right" data-form="{{ route('module_create') }}">{{ trans('common.add_new') }} <i class="fa fa-plus"></i></a>-->
</h3>
<div class="portlet light bordered">
    <div class="portlet-body contentArea">
        <table id="data-table-basic" class="table table-striped">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Logo</th>
                    <th>Organization</th>
                    <th>Status</th>
                    <th>Group</th>
                    <th>User</th>
                    <th>Modules</th>
                    <th>Pages</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $i = 1;
                foreach ($org as $careProvider) {
                    ?>
                    <tr>
                        <td><?php echo $i . '.'; ?></td>
                        <td><img style="width: 50px;" alt="<?php echo $careProvider->ORG_NAME; ?>" title="<?php echo $careProvider->ORG_NAME; ?>"/></td>
                        <td><?php echo $careProvider->ORG_NAME; ?></td>
                        <td class="center">
                            @if($access->STATUS == 1)
                            <?php if ($careProvider->STATUS == '1') { ?>
                                <span style="color:green; font-size: 25px;" data-hId="<?php echo $careProvider->ORG_ID; ?>" status="<?php echo $careProvider->STATUS; ?>" class="statusType"><i class="md md-check-box"></i></span>
                            <?php } else { ?>
                                <span style="color:red; font-size: 25px;" data-hId="<?php echo $careProvider->ORG_ID; ?>" status="<?php echo $careProvider->STATUS; ?>" class="statusType"><i class="md-not-interested"></i></span>
                            <?php } ?>
                            @endif
                        </td>
                        <td class="center">
                            @if($access->CREATE == 1)
                            <a href="#" data-toggle="modal" data-target="#modal_add_content" class="btn btn-danger" data-form="{{ route('create_group') }}">Create Group</a>
                            @endif
                        </td>
                        <td class="center">
                            @if($access->CREATE == 1)
                            <button class="btn bgm-red addModule" data-hid="<?php echo $careProvider->ORG_ID; ?>" title="Assign Module">Create User</button>
                            @endif
                        </td>
                        <td class="center" style="width: 140px;">
                            @if($access->CREATE == 1)
                            <a href="#" data-toggle="modal" id="assignModule" data-target="#modal_add_content" class="btn btn-danger" data-form="{{ route('assign_module') }}">Assign Module</a>
                            @endif
                        </td>
                        <td class="center">
                            @if($access->CREATE == 1)
                            <a href="#" data-toggle="modal" data-target="#modal_add_content" class="btn btn-danger" data-form="{{ route('add_pages') }}">Add Pages</a>
                            @endif
                        </td>
                    </tr>
                    <?php
                    $i++;
                }
                ?>
            </tbody>
        </table>
    </div>
</div>

@endsection