<div>
    <!--=============Title for modal===============-->
    <div class="modal_top_title">Assign Modules</div>
    <!--===========End title for modal=============-->

    <!--========Show form post message=============-->
    <div class="col-sm-5 col-sm-offset-3 form_messsage">
        <span id="message" style="font-size: 16px; color: green;"></span>
    </div>
    <!--========End form post message=============-->
    <input type="hidden" id="addRemovePage" value="{{ route('add_remove_pages') }}">
    <table class="table table-bordered">
        <thead>
            <tr>
                <th style="width: 300px;">Module Name</th>
                <th>Create</th>
                <th>Read</th>
                <th>Update</th>
                <th>Delete</th>
                <th>Status</th>
            </tr>
        </thead>
        <tbody>
            @foreach($active_modules as $key =>$org_module)
            <tr>
                <td colspan="6" style="background: #f7f7f7; color: #5D8C53; padding: 3px;"><span class=" md-chevron-right"></span>  <?php echo $org_module->MODULE_NAME; ?></td>
            </tr>
            <?php
            $org_module_links = DB::table('sa_module_links')->where('MODULE_ID', '=', $org_module->MODULE_ID)->get();
            ?>
            @foreach ($org_module_links as $org_module_link)
            <?php
            $org_active_module_links = DB::table('sa_org_mlink')->where('LINK_ID', '=', $org_module_link->LINK_ID)->where('ORG_ID', 1)->first();

            if (!empty($org_active_module_links)) {
                $is_checked_create = ($org_active_module_links->CREATE == 1) ? "checked='checked'" : "";
                $is_checked_read = ($org_active_module_links->READ == 1) ? "checked='checked'" : "";
                $is_checked_update = ($org_active_module_links->UPDATE == 1) ? "checked='checked'" : "";
                $is_checked_delete = ($org_active_module_links->DELETE == 1) ? "checked='checked'" : "";
                $is_checked_status = ($org_active_module_links->STATUS == 1) ? "checked='checked'" : "";
            }
            ?>
            <tr>
                <td style="padding-left: 20px;"><?php echo $org_module_link->LINK_NAME; ?></td>
                <td class="text-center">
                    <input type="checkbox" class="chkAssignPage" <?php echo (!empty($org_active_module_links)) ? $is_checked_create : ""; ?> title="Create" value="<?php echo $org_module->MODULE_ID . ',' . $org_module_link->LINK_ID . ',' . 'C' . "," . 1; ?>"/>
                </td>
                <td class="text-center">
                    <input type="checkbox" class="chkAssignPage" <?php echo (!empty($org_active_module_links)) ? $is_checked_read : ""; ?>  title="Create" value="<?php echo $org_module->MODULE_ID . ',' . $org_module_link->LINK_ID . ',' . 'R' . "," . 1; ?>"/>
                </td>
                <td class="text-center">
                    <input type="checkbox" class="chkAssignPage" <?php echo (!empty($org_active_module_links)) ? $is_checked_update : ""; ?> title="Create"  value="<?php echo $org_module->MODULE_ID . ',' . $org_module_link->LINK_ID . ',' . 'U' . "," . 1; ?>"/>
                </td>
                <td class="text-center">
                    <input type="checkbox" class="chkAssignPage" <?php echo (!empty($org_active_module_links)) ? $is_checked_delete : ""; ?> title="Create" value="<?php echo $org_module->MODULE_ID . ',' . $org_module_link->LINK_ID . ',' . 'D' . "," . 1; ?>"/>
                </td>
                <td class="text-center">
                    <input type="checkbox" class="chkAssignPage" <?php echo (!empty($org_active_module_links)) ? $is_checked_status : ""; ?>  title="Create" value="<?php echo $org_module->MODULE_ID . ',' . $org_module_link->LINK_ID . ',' . 'S' . "," . 1; ?>"/>
                </td>
            </tr>
            @endforeach
            @endforeach
        </tbody>
    </table>
</div>