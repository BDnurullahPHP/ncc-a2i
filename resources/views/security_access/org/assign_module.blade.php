<div>
    <!--=============Title for modal===============-->
    <div class="modal_top_title">Assign Modules</div>
    <!--===========End title for modal=============-->

    <!--========Show form post message=============-->
    <div class="form-group">
        <div class="col-sm-5 col-sm-offset-3 form_messsage">
            <span id="message" style="font-size: 16px; color: green;"></span>
        </div>
    </div>
    <!--========End form post message=============-->
    <input type="hidden" id="addModule" value="{{ route('add_module') }}">
    <input type="hidden" id="removeModule" value="{{ route('remove_module') }}">
    <div class="row">
        <div class="col-md-11">
            <select multiple="multiple" size="10" name="duallistbox_demo2" class="demo2">
                @foreach($modules as $key => $module)
                <option value="{{ $module->MODULE_ID }}" >{{ $module->MODULE_NAME }}</option>
                @endforeach
                @foreach($active_modules as $key => $module)
                <option value="{{ $module->MODULE_ID }}" selected="selected">{{ $module->MODULE_NAME }}</option>
                @endforeach
            </select>
        </div>
    </div>
</div>