@extends('admin_access.layout.master')

@section('style')

@endsection

@section('script')
<!--<script src="{{ Helpers::asset('assets/global/plugins/jstree/dist/jstree.min.js') }}" type="text/javascript"></script>
<script src="{{ Helpers::asset('assets/pages/scripts/ui-tree.min.js') }}" type="text/javascript"></script>
<link href="{{ Helpers::asset('assets/global/plugins/jstree/dist/themes/default/style.min.css') }}" rel="stylesheet" type="text/css" />-->
<script type="text/javascript">
    // Group
    $(document).on("change", "#userGroup", function() {
        var groupID = $(this).val();
        var action = $('#groupTolevel').val();
        var action1 = $('#getModuleAcceesByGroup').val();
        $.ajax({
            type: "GET",
            url: action,
            data: {groupID: groupID},
            success: function(result) {
                $('#userGroupLevel').html(result);
            }
        });
        $.ajax({
            type: "GET",
            url: action1,//securityAccess/getModuleAcceesByGroup
            data: {group: groupID},
            beforeSend: function() {
                $("#modules").addClass("loadingIMid");
            },
            success: function(result2) {
                $('#modules').html(result2);
                $("#modules > li ul").first().addClass("in");
            }
        });
    });

// Level
    $(document).on("change", "#userGroupLevel", function() {
        var group_id = $("#userGroup").val();
        var level_id = $(this).val();
        var action = $('#levelToName').val();
        var action1 = $('#moduleByGroupLevel').val();
        $.ajax({
            type: "GET",
            url: action,
            data: {group_id: group_id,level_id: level_id},
            success: function(result) {
                $('#userName').html(result);
            }
        });
        $.ajax({
            type: "GET",
            url: action1,
            data: {group: group_id, level: level_id},
            beforeSend: function() {
                $("#modules").addClass("loadingIMid");
            },
            success: function(result2) {
                $('#modules').html(result2).removeClass("loadingIMid");
                $("#modules > li ul").first().addClass("in");
            }
        });
    });
    // User
    $(document).on("change", "#userName", function() {
        var group_id = $("#userGroup").val();
        var level_id = $("#userGroupLevel").val();
        var user_id = $(this).val();
        var action = $('#assignModulebyGroup').val();
        $.ajax({
            type: "GET",
            url: action,
            data: {group_id: group_id, level_id: level_id, user_id:user_id},
            beforeSend: function() {
                $("#modules").addClass("loadingIMid");
            },
            success: function(result2) {
                $('#modules').html(result2).removeClass("loadingIMid");
                $("#modules > li ul").first().addClass("in");
            }
        });
    });
    
    
    $(document).on("click", ".chkPage", function () {
            //alert($(this).val());
            var value = $(this).val();
            var group = $("#userGroup").val();
            var level = $("#userGroupLevel").val();
            var user = $("#userName").val();
            var action = $("#assignModuleToGroupAction").val();            
            var checked = ($($(this)).is(':checked')) ? 1 : 0;
            if (group == "") {
                alert("Please Select Group");
                return false;
            } else if (level == "") {
                alert("Please Select Level");
                return false;
            } else if (user == "") {
                alert("Please Select User");
                return false;
            } else {
                $.ajax({
                    type: "GET",
                    url: action,
                    data: {group_id: group, level_id: level, user: user, values: value, is_checked: checked},
                    success: function (result) {

                    }
                });
            }
        });
</script>
@endsection

@section('content')
<input type="hidden" id="groupTolevel" value="{{ route('get_group_to_level') }}">
<input type="hidden" id="getModuleAcceesByGroup" value="{{ route('get_module_by_group') }}">
<input type="hidden" id="moduleByGroupLevel" value="{{ route('get_module_by_g_level') }}">
<input type="hidden" id="assignModuleToGroupAction" value="{{ route('assign_module_to_group') }}">
<input type="hidden" id="levelToName" value="{{ route('get_user_by_level') }}">

<input type="hidden" id="assignModulebyGroup" value="{{ route('assign_module_by_group') }}">
<h4> Assign Module </h4>
<div class="portlet light bordered">
    <div class="portlet-body contentArea">
        <div class="row" style="background: #F7F7F7;">
            @if($access->CREATE == 1)
            <div class="col-sm-2" >
                <select class="form-control" id="userGroup" name="userGroup">
                    <option value="">Select Group</option>
                    @foreach($groups as $key=>$value)
                    <option value="{{ $value->USERGRP_ID }}">{{ $value->USERGRP_NAME }}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-sm-2">
                <select class="form-control" id="userGroupLevel" name="userGroupLevel">
                    <option value="">Select Level</option>
                </select>
            </div>
            <div class="col-sm-2">
                <select class="form-control" id="userName" name="userName">
                    <option value="">Select User</option>
                </select>
            </div>
            @endif
        </div>
        <br clear="all"/>
        <div class="row">
            <div class="col-md-8">
                <div class="portlet light bordered">
                    <ul id="tree_1" class="tree" style="padding: 5px;">
                        <li>
                            <a class="tree-toggle" href="#" data-role="branch" data-toggle="branch" data-value="Bootstrap_Tree" style="float: left;"> Modules </a>
                            <span class="loadingCon"></span>
                            <span style="float: right;">
                                <span style="padding: 0 5px;">Create</span>
                                <span style="padding: 0 5px;">Read</span>
                                <span style="padding: 0 5px;">Update</span>
                                <span style="padding: 0 5px;">Delete</span>
                                <span style="padding: 0 5px;">Status</span>
                            </span>
                            <br clear="all"/>
                            <ul class="branch in" id="modules" style="width: 98%; padding: 5px; margin-left: 15px;">
                                <?php
                                foreach ($org_modules as $org_module) {
                                    $org_module_links = DB::table('sa_org_mlink')
                                                    ->leftJoin('sa_module_links', 'sa_org_mlink.LINK_ID', '=', 'sa_module_links.LINK_ID')
                                                    ->where('sa_org_mlink.MODULE_ID', '=', $org_module->MODULE_ID)->get();
                                    ?>
                                    <li>
                                        <a id="nut3" data-value="Bootstrap_Tree" data-toggle="branch" class="tree-toggle closed" href="#" style="color: #4A8BC2;"><?php echo $org_module->MODULE_NAME; ?></a>
                                        <ul class="branch in" style="padding-left: 5px; width: 96%;">
                                            <?php
                                            foreach ($org_module_links as $org_module_link):
                                                ?>
                                                <li style="border-bottom: 1px dashed #ccc; padding: 0 0 5px 0; background: #f2f2f2; padding-left: 5px;">
                                                    <span style="color: #333;"><i class="icon-file"></i> <?php echo $org_module_link->LINK_NAME; ?></span>
                                                    <span style="float: right;">
                                                        <span style="padding: 0 17px;">
                                                            <?php if ($org_module_link->CREATE == 1) { ?>
                                                                <input type="checkbox" class="chkPage" title="Create"  value="<?php echo $org_module->MODULE_ID . ',' . $org_module_link->ORG_MLINKS_ID . ',' . 'C'; ?>"/>
                                                            <?php } else { ?>
                                                                <input type="checkbox" title="Create" disabled="disabled"/>
                                                            <?php } ?>
                                                        </span>
                                                        <span style="padding: 0 17px;">
                                                            <?php if ($org_module_link->READ == 1) { ?>
                                                                <input type="checkbox" class="chkPage" title="Read" value="<?php echo $org_module->MODULE_ID . ',' . $org_module_link->ORG_MLINKS_ID . ',' . 'R'; ?>"/>
                                                            <?php } else { ?>
                                                                <input type="checkbox" title="Read" disabled="disabled"/>
                                                            <?php } ?>
                                                        </span>
                                                        <span style="padding: 0 17px;">
                                                            <?php if ($org_module_link->UPDATE == 1) { ?>
                                                                <input type="checkbox" class="chkPage" title="Update" value="<?php echo $org_module->MODULE_ID . ',' . $org_module_link->ORG_MLINKS_ID . ',' . 'U'; ?>"/>
                                                            <?php } else { ?>
                                                                <input type="checkbox" title="Update" disabled="disabled"/>
                                                            <?php } ?>
                                                        </span>
                                                        <span style="padding: 0 17px;">
                                                            <?php if ($org_module_link->DELETE == 1) { ?>
                                                                <input type="checkbox" class="chkPage" title="Delete" value="<?php echo $org_module->MODULE_ID . ',' . $org_module_link->ORG_MLINKS_ID . ',' . 'D'; ?>"/>
                                                            <?php } else { ?>
                                                                <input type="checkbox" title="Delete" disabled="disabled"/>
                                                            <?php } ?>
                                                        </span>
                                                        <span style="padding: 0 17px;">
                                                            <?php if ($org_module_link->STATUS == 1) { ?>
                                                                <input type="checkbox" class="chkPage" title="Delete" value="<?php echo $org_module->MODULE_ID . ',' . $org_module_link->ORG_MLINKS_ID . ',' . 'S'; ?>"/>
                                                            <?php } else { ?>
                                                                <input type="checkbox" title="Delete" disabled="disabled"/>
                                                            <?php } ?>
                                                        </span>
                                                    </span>
                                                    <br clear="all"/>
                                                </li>
                                            <?php endforeach; ?>
                                        </ul>
                                    </li>
                                    <?php
                                }
                                ?>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
            <!--end .col -->
            <!--            <div class="col-md-4">
                            <div class="card">
                                <div class="card-header ch-alt" style="padding: 11px;">
                                    <h2>Users</h2>
                                </div>
            
            
                            </div>
                        </div>-->
            <!--end .col -->
        </div>
    </div>
</div>

<!--<div class="col-md-6">
    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption">
                <i class="icon-social-dribbble font-blue-sharp"></i>
                <span class="caption-subject font-blue-sharp bold uppercase">Default Tree</span>
            </div>
            <div class="actions">
                <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                    <i class="icon-cloud-upload"></i>
                </a>
                <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                    <i class="icon-wrench"></i>
                </a>
                <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                    <i class="icon-trash"></i>
                </a>
            </div>
        </div>
        <div class="portlet-body">
            <div id="tree_1" class="tree-demo">
                <ul>
                    <li> Root node 1
                        <ul>
                            <li data-jstree='{ "selected" : true }'>
                                <a href="javascript:;"> Initially selected </a>
                            </li>
                            <li data-jstree='{ "icon" : "fa fa-briefcase icon-state-success " }'> custom icon URL </li>
                            <li data-jstree='{ "opened" : true }'> initially open
                                <ul>
                                    <li data-jstree='{ "disabled" : true }'> Disabled Node </li>
                                    <li data-jstree='{ "type" : "file" }'> Another node </li>
                                </ul>
                            </li>
                            <li data-jstree='{ "icon" : "fa fa-warning icon-state-danger" }'> Custom icon class (bootstrap) </li>
                        </ul>
                    </li>
                    <li data-jstree='{ "type" : "file" }'>
                        <a href="http://www.jstree.com/"> Clickanle link node </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>-->
@endsection
