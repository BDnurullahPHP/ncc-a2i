<div class="row applicant_form">
    <div class="col-md-6 col-sm-6">
        <div class="row row_each">
            <div class="col-md-6 col-sm-6">
                <label for="">{{ trans('tender.tender_name') }}</label>
            </div>
            <div class="col-md-6 col-sm-6">
                <p>{{ $tender_name }}</p>
            </div>
        </div>
        <div class="row row_each">
            <div class="col-md-6 col-sm-6">
                <label for="">আবেদনকারীর নাম</label>
            </div>
            <div class="col-md-6 col-sm-6">
                <p>{{ Auth::check()?$citizen_user->FULL_NAME:$user->APPLICANT_NAME }}</p>
            </div>
        </div>
        <div class="row row_each">
            <div class="col-md-6 col-sm-6">
                <label for="">মোবাইল নাম্বার</label>
            </div>
            <div class="col-md-6 col-sm-6">
                <p>{{ Auth::check()?$citizen_user->MOBILE_NO:$user->APPLICANT_PHONE }}</p>
            </div>
        </div>
        <div class="row row_each">
            <div class="col-md-6 col-sm-6">
                <label for="">জাতীয় পরিচয়পত্র</label>
            </div>
            <div class="col-md-6 col-sm-6">
                <p>{{ Auth::check()?$citizen_user->getCitizen->NID:$user->NID }}</p>
            </div>
        </div>
        <div class="row row_each">
            <div class="col-md-6 col-sm-6">
                <label for="">উপজেলা/থানা</label>
            </div>
            <div class="col-md-6 col-sm-6">
                <p>{{ $user_thana }}</p>
            </div>
        </div>
        <div class="row row_each">
            <div class="col-md-6 col-sm-6">
                <label for="">হোল্ডিং নং</label>
            </div>
            <div class="col-md-6 col-sm-6">
                <p>{{ Auth::check()?$citizen_user->getCitizen->HOLDING_NO:$user->HOLDING_NO }}</p>
            </div>
        </div>
    </div>
    <div class="col-md-6 col-sm-6">
        <div class="row row_each">
            <div class="col-md-6 col-sm-6">
                <label for="">আবেদনকারীর পিতা/স্বামী</label>
            </div>
            <div class="col-md-6 col-sm-6">
                <p>{{ Auth::check()?$citizen_user->getCitizen->SPOUSE_NAME:$user->SPOUSE_NAME }}</p>
            </div>
        </div>
        <div class="row row_each">
            <div class="col-md-6 col-sm-6">
                <label for="">ইমেইল</label>
            </div>
            <div class="col-md-6 col-sm-6">
                <p>{{ Auth::check()?$citizen_user->EMAIL:$user->EMAIL }}</p>
            </div>
        </div>
        <div class="row row_each">
            <div class="col-md-6 col-sm-6">
                <label for="">জেলা</label>
            </div>
            <div class="col-md-6 col-sm-6">
                <p>{{ $user_district }}</p>
            </div>
        </div>
        <div class="row row_each">
            <div class="col-md-6 col-sm-6">
                <label for="">রোড নং</label>
            </div>
            <div class="col-md-6 col-sm-6">
                <p>{{ Auth::check()?$citizen_user->getCitizen->ROAD_NO:$user->ROAD_NO }}</p>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12 col-sm-12">
        <h4 style="margin-bottom:20px;">দরপত্রের বিবরণ</h4>
    </div>
</div>
{{-- <div class="row">
    <div class="col-md-6 col-sm-6">
        <div class="row row_each">
            <div class="col-md-6 col-sm-6">
                <label for="">দরদাতা কতৃক প্রদত্ত দর</label>
            </div>
            <div class="col-md-6 col-sm-6">
                <p>{{ $user->BID_AMOUNT }} &#40;টাকা&#41;</p>
            </div>
        </div>
    </div>
    <div class="col-md-6 col-sm-6">
        <div class="row row_each">
            <div class="col-md-6 col-sm-6">
                <label for="">জামানতের পরিমান</label>
            </div>
            <div class="col-md-6 col-sm-6">
                <p>{{ $user->BG_AMOUNT }} &#40;টাকা&#41;</p>
            </div>
        </div>
    </div>
</div> --}}