@extends('admin_access.layout.master')

@section('style')

@endsection

@section('content')

	<h3>অর্থ প্রদানের তথ্য</h3>

	<div class="portlet light bordered">

	    <div class="row">
	    	<div class="col-md-5 col-sm-6 col-xs-12">
	    		<div id="lg_form_message"></div>
	    	</div>
	    </div>
	    <div class="portlet-body contentArea">
	        
			<table class="table table-striped table-bordered table-hover" data-source="{{ Auth::check()?route('citizen_pay_data'):route('applicant_pay_data') }}" id="common_table">
			    <thead>
			        <tr>
			            <th>{{ trans('common.table_sl') }}</th>
			            <th>{{ trans('common.payment_type') }}</th>
			            <th>ব্যাংক ড্রাফট / চালান / ব্যাংক ট্রানজিশন</th>
			            <th>অর্থ প্রদানের খাত</th>
			            <th>পরিমাণ</th>
			            <th>সময়</th>
			            <!-- <th>অবস্থা</th> -->
			            {{-- <th>{{ trans('common.table_action') }}</th> --}}
			        </tr>
			    </thead>
			    <tbody>
			    	
			    </tbody>
			</table>

	    </div>
	</div>

@endsection