@extends('admin_access.layout.master')

@section('css_file')

@endsection

@section('style')
    
    <style type="text/css">
        .applicant_form_info label{
            font-weight: 700;
        }
        .page-content p{
            margin-top: 0px;
            margin-bottom: 5px;
        }
        .row_each{
            padding-bottom: 10px;
        }
        @media print{
            button.print_btn, a.arrow_back_db, .alert{
                display: none;
            }
        }

    </style>

@endsection

@section('js_file')

@endsection

@section('script')

	<script type="text/javascript">
        $(document).ready(function() {

	    });
	</script>

@endsection

@section('content')

	<div class="row">
        <div class="col-sm-6">
            <a href="{{ Auth::check()?route('admin_dashboard'):route('applicant_dashboard') }}" class="btn btn-sm btn-primary arrow_back_db"><i class="fa fa-arrow-left" aria-hidden="true"></i> ড্যাশবোর্ড</a>
            <!--========Session message=======-->
            @if (Session::has('success'))
                <div class="alert alert-success">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  {{ Session::get('success') }}
                </div>
            @endif
            @if (Session::has('error'))
                <div class="alert alert-danger">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  {{ Session::get('error') }}
                </div>
            @endif
            @if (count($errors) > 0)
                @foreach ($errors->all() as $error)
                  <div class="alert alert-danger">
                      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                      {{ $error }}
                  </div>
                @endforeach
            @endif
            <!--======end Session message=====-->
            <h3 class="page_heading">লেনদেনের তথ্য</h3>
        </div>
    </div>
    <!-- END PAGE TITLE-->
    <!-- END PAGE HEADER-->
    <!-- BEGIN DASHBOARD STATS 1-->
    @if(!empty($payment))
        <!--================================================================-->
        <table border="0" cellpadding="0" cellspacing="0" width="100%">
            <tbody>
                <tr>
                    <td style="width:60%;">
                        <h4>টাকা প্রদানকারীর তথ্য</h4>
                        <p>নাম: {{ $user_type=='applicant'?$user->APPLICANT_NAME:$user->getTenderCitizen->FULL_NAME }}</p>
                        <p>ঠিকানা: {{ $user_thana }}, 
                            @if($user_type=='applicant')
                                {{ $user->ROAD_NO }} / {{ $user->HOLDING_NO }}
                            @else
                                {{ $user->ROAD_NO }} / {{ $user->HOLDING_NO }}
                            @endif
                        </p>
                    </td>
                    <td style="width:40%;">
                        
                        @if(!empty($payment))
                            <h4>পেমেন্টের বিবরণ</h4>
                            @if($payment->PAYMENT_TYPE == 1)
                                <p>ব্যাংক ড্রাফট নম্বর: {{ $payment->B_DRAFT_NO }}</p>
                                <p>তারিখ: {{ $payment->TRAN_DATE }}</p>
                            @elseif($payment->PAYMENT_TYPE == 3)
                                <p>চালান নম্বর: {{ $payment->B_DRAFT_NO }}</p>
                                <p>তারিখ: {{ $payment->TRAN_DATE }}</p>
                            @else
                                <p>ট্রানজিশন নাম্বার: {{ $payment->TRAN_ID }}</p>
                                <p>ট্রানজিশন সময়: {{ $payment->TRAN_DATE }}</p>
                                <p>অ্যাকাউন্টের ধরন: {{ $payment->CARD_TYPE }}</p>
                            @endif
                            <p>মোট লেনদেন: {{ number_format($payment->AMOUNT) }} টাকা</p>
                        @endif
                    </td>
                </tr>
            </tbody>
                
        </table>
        <!--================================================================-->
        <div class="row">
            <div class="col-md-12" style="padding-top:25px;">
                <table class="table table-striped table-hover">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>টাকা প্রদানের খাত</th>
                            <th>দরপত্র</th>
                            <th>দরপত্রের প্রকল্প</th>
                            @if(isset($project_details) && !empty($project_category))
                                <th>প্রকল্পের শ্রেনী</th>
                                <th>ধরন</th>
                            @endif
                            <th>মোট টাকা</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>1</td>
                            <td>{{ $particular }}</td>
                            <td>{{ $tender_name }}</td>
                            <td>{{ $project->PR_NAME_BN }}</td>
                            @if(isset($project_details) && !empty($project_category))
                                <td>{{ $project_category }}</td>
                                <td>{{ $project_details->getType->TYPE_NAME }}</td>
                            @endif
                            <td>{{ number_format($payment->AMOUNT) }}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <button class="btn btn-success print_btn pull-right" onclick="javascript:window.print();"><i class="fa fa-print"></i></button>
            </div>
        </div>
    @endif

@endsection