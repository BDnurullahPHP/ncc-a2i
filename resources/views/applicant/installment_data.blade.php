<table class="table table-striped table-bordered table-hover">
    <thead>
        <tr>
            <th>{{ trans('citizen.installment_number') }}</th>
            <th>{{ trans('citizen.installment_amount') }}</th>
            <th>{{ trans('citizen.installment_date') }}</th>
            <th>{{ trans('citizen.installment_pay_status') }}</th>
            <th>{{ trans('citizen.installment_status') }}</th>
        </tr>
    </thead>
    <tbody>
        @foreach($app_installment as $key => $value)
            <tr>
                <td>{{ $value->INSTALLMENT }}</td>
                <td>
                    @if( $value->IS_PAID == 0 )
                        @if(date('Y-m-d') > date('Y-m-d', strtotime($value->DATE)))
                            <?php $p_amount = ($value->AMOUNT * $value->PUNISHMENT)/100; ?>
                            {{ number_format($value->AMOUNT,2) }}+<span style="color:#a94442;">{{ number_format($p_amount,2) }}</span>
                        @else
                            {{ number_format($value->AMOUNT,2) }}
                        @endif
                    @else
                        <?php 
                            $paid_ins = DB::table('ac_vn_ledgers')
                                            ->where('TRX_TRAN_NO', $value->TRX_TRAN_NO)
                                            ->where('TRX_CODE_ID', 5)
                                            ->sum('DR_AMT');
                        ?>
                        {{ number_format($paid_ins,2) }}
                    @endif
                </td>
                <td>
                    @if( $value->IS_PAID == 0 && date('Y-m-d') > date('Y-m-d', strtotime($value->DATE)) )
                        <span style="color:#a94442;">{{ date('d/m/Y', strtotime($value->DATE)) }}</span>
                    @else
                        {{ date('d/m/Y', strtotime($value->DATE)) }}
                    @endif
                </td>
                <td>
                    @if($value->IS_PAID == 1)
                        <span class="label label-info">{{ trans('common.approve_wait') }}</span>
                    @else
                        @if( date('Y-m-d') > date('Y-m-d', strtotime($value->DATE)) )
                            <?php 
                                $punishment_amount = ($value->AMOUNT * $value->PUNISHMENT)/100;
                                $total_amount = $value->AMOUNT + $punishment_amount;
                            ?>
                        @else
                            <?php $total_amount = $value->AMOUNT; ?>
                        @endif

                        <?php $payment_api = Config::get('payment'); ?>

                        {!! Form::open(array('url' => $payment_api['dev']?$payment_api['test_url']:$payment_api['url'], 'name' => 'payment_gw', 'method' => 'POST', 'id'=>'payment_gw_'.$value->INSTALLMENT, 'class' =>'form-horizontal')) !!}

                            {!! Form::hidden('total_amount', round($total_amount, 2)) !!}
                            {!! Form::hidden('store_id', $payment_api['dev']?$payment_api['test_store_id']:$payment_api['store_id']) !!}
                            {!! Form::hidden('tran_id', 'tran'.str_random(10)) !!}
                            @if(Auth::check())
                                {!! Form::hidden('success_url', route('installment_pay', ['citizen', $value->TENDER_INS_ID, $value->TRX_TRAN_NO, $value->SCHEDULE_ID])) !!}
                                {!! Form::hidden('fail_url', route('installment_pay', ['citizen', $value->TENDER_INS_ID, $value->TRX_TRAN_NO, $value->SCHEDULE_ID])) !!}
                                {!! Form::hidden('cancel_url', route('installment_pay', ['citizen', $value->TENDER_INS_ID, $value->TRX_TRAN_NO, $value->SCHEDULE_ID])) !!}
                                {!! Form::hidden('cus_name', $citizen->FULL_NAME) !!}
                                {!! Form::hidden('cus_email', $citizen->EMAIL) !!}
                                {!! Form::hidden('cus_phone', $citizen->EMAIL) !!}
                            @else
                                {!! Form::hidden('success_url', route('installment_pay', ['applicant', $value->TENDER_INS_ID, $value->TRX_TRAN_NO, $value->SCHEDULE_ID])) !!}
                                {!! Form::hidden('fail_url', route('installment_pay', ['applicant', $value->TENDER_INS_ID, $value->TRX_TRAN_NO, $value->SCHEDULE_ID])) !!}
                                {!! Form::hidden('cancel_url', route('installment_pay', ['applicant', $value->TENDER_INS_ID, $value->TRX_TRAN_NO, $value->SCHEDULE_ID])) !!}
                                {!! Form::hidden('cus_name', $applicant->APPLICANT_NAME) !!}
                                {!! Form::hidden('cus_email', $applicant->EMAIL) !!}
                                {!! Form::hidden('cus_phone', $applicant->APPLICANT_PHONE) !!}
                            @endif
                            {!! Form::hidden('version', '2.00') !!}
                            {!! Form::hidden('currency', 'BDT') !!}
                            
                            @if( date('Y-m-d') > date('Y-m-d', strtotime($value->DATE)) )
                                <button type="submit" class="btn btn-danger btn-xs payment_confirmation">{{ trans('citizen.pay') }}</button>
                            @else
                                <button type="submit" class="btn btn-primary btn-xs payment_confirmation">{{ trans('citizen.pay') }}</button>
                            @endif

                        {!! Form::close() !!}
                    @endif
                </td>
                <td>
                    @if($value->IS_PAID == 0)
                        <span class="label label-warning">{{ trans('common.unpaid') }}</span>
                    @else
                        <span class="label label-success">{{ trans('common.paid') }}</span>
                    @endif
                </td>
            </tr>
        @endforeach
    </tbody>
</table>