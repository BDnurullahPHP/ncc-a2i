<div class="row">
	<div class="col-md-4">
		<p><strong>{{trans('citizen.a_name')}}</strong></p>
		{{ $applicant_name }}
	</div>
	<div class="col-md-4">
		<p><strong>{{trans('citizen.Father_Husband_name')}}</strong></p>
		{{ $applicant_fname }}
	</div>
	<div class="col-md-4">
		<p><strong>{{trans('citizen.address')}}</strong></p>
		{{ $applicant_address }}
	</div>
</div>
<hr>
<div class="row">

	<?php $payment_api = Config::get('payment'); ?>

	@foreach($app_voucher_chd as $key => $vchild)
		<div class="col-md-12">
			<?php 
				$total_paid = DB::table('ac_vn_ledgers')
									->where('TRX_TRAN_NO', $vchild->TRX_TRAN_NO)
									->where('TRX_CODE_ID', 5)
									->sum('DR_AMT');
				$approved = DB::table('ac_vn_ledgers')
									->where('TRX_TRAN_NO', $vchild->TRX_TRAN_NO)
									->where('TRX_CODE_ID', 5)
									->where('IS_VERIFIED', 1)
									->first();

				if (empty($total_paid)) {
					$due_amt = $vchild->CR_AMT;
					$pay_status = '&#40;<strong style="color:#a94442;">'.trans('common.unpaid').'</strong>&#41;';
					$pay_status .= ' <strong>'.trans('common.paid').': </strong> '.Helpers::numberConvert(0).' '.trans('common.taka');
				} else {
					$due_amt = floatval($vchild->CR_AMT)-floatval($total_paid);
					$pay_status = '&#40;<strong style="color:#a94442;">'.trans('common.paid').'</strong>&#41;';
					if (!empty($approved)) {
						$pay_status .= ' <strong>'.trans('common.approved').': </strong> '.Helpers::numberConvert(number_format($due_amt, 2)).' '.trans('common.taka');
					} else {
						$pay_status .= ' &#40;<strong style="color:#a94442;">'.trans('common.approve_wait').'</strong>&#41';
					}
					$pay_status .= '<br><strong>'.trans('common.payment_type').'</strong>: ';

					$payment_info = DB::table('ac_pgw_return')->where('TRX_TRAN_NO', $vchild->TRX_TRAN_NO)->first();

					if (!empty($payment_info)) {
						if ($payment_info->PAYMENT_TYPE == 1) {
							$pay_status .= trans('common.bank_draft');
							$pay_status .= '<br><strong>'.trans('common.bank_draft_number').'</strong>: '.$payment_info->B_DRAFT_NO;
							$pay_status .= '<br><strong>'.trans('common.attachment').'</strong>: ';
							if (!empty($payment_info->ATTACHMENT) && File::exists(public_path()."/upload/bank/".$payment_info->ATTACHMENT)) {
								$pay_status .= '<a target="_blank" href="'.Helpers::asset('upload/bank/'.$payment_info->ATTACHMENT).'">'.$payment_info->ATTACHMENT.'</a>';
							}
						} elseif ($payment_info->PAYMENT_TYPE == 3) {
							$pay_status .= trans('common.cash');
							$pay_status .= '<br><strong>'.trans('common.challan_no').'</strong>: '.$payment_info->B_DRAFT_NO;
							if (!empty($payment_info->ATTACHMENT) && File::exists(public_path()."/upload/bank/".$payment_info->ATTACHMENT)) {
								$pay_status .= '<br><strong>'.trans('common.attachment').'</strong>: ';
								$pay_status .= '<a target="_blank" href="'.Helpers::asset('upload/bank/'.$payment_info->ATTACHMENT).'">'.$payment_info->ATTACHMENT.'</a>';
							}
						} else {
							$pay_status .= trans('common.online');
							$pay_status .= '<br><strong>'.trans('common.bank_tran_number').'</strong>: '.$payment_info->BANK_TRAN_ID;
						}
					}
				}
			?>
			<div class="alert {{ $due_amt != 0 ? 'alert-warning' : 'alert-success' }}">
                <p style="margin-bottom: 10px;">
                	<strong>{{ $vchild->getParticular->PARTICULAR_NAME }}: </strong>
                	{{ Helpers::numberConvert( number_format($vchild->CR_AMT) ) }} {{ trans('common.taka') }}{!! $pay_status !!}
                </p>
                @if($due_amt != 0)
                	<?php 
                		$payable_amount = round($due_amt, 2);
                		if (isset($ctz_data) && !empty($ctz_data)) {
                			$return_url = route('make_payment', ['citizen', $vchild->PARTICULAR_ID, $vchild->TRX_TRAN_NO, $schedule_id]);
                			$cus_name = $ctz_data->getTenderCitizen->FULL_NAME;
	                		$cus_email = empty($ctz_data->getTenderCitizen->EMAIL)?Config::get('mail')['from']['address']:$ctz_data->getTenderCitizen->EMAIL;
	                		$cus_phone = $ctz_data->getTenderCitizen->MOBILE_NO;
                			$value = ['citizen', $vchild->PARTICULAR_ID, $vchild->TRX_TRAN_NO, $schedule_id, $user_id];
                		} else {
                			$return_url = route('make_payment', ['applicant', $vchild->PARTICULAR_ID, $vchild->TRX_TRAN_NO, $schedule_id]);
                			$cus_name = $applicant_data->APPLICANT_NAME;
                			$cus_email = empty($applicant_data->EMAIL)?Config::get('mail')['from']['address']:$applicant_data->EMAIL;
                			$cus_phone = $applicant_data->APPLICANT_PHONE;
                			$value = ['applicant', $vchild->PARTICULAR_ID, $vchild->TRX_TRAN_NO, $schedule_id, $user_id];
                		}
                		$user_arr = array('amount'=>$payable_amount, 'particular' => $vchild->PARTICULAR_ID,'url'=>$return_url,'name'=>$cus_name,'email'=>$cus_email,'phone'=>$cus_phone,'val'=>$value);
                		$user_jsn = json_encode($user_arr);

                	?>
                	<a href="#" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#modal_add_content" data-form="{{ route('payment_selection') }}?up_data={{ $user_jsn }}">{{ trans('citizen.pay') }}</a>
            	@endif
            </div>
		</div>
	@endforeach
	<?php unset($vchild); ?>
</div>

@if( $app_property->IS_LEASE == 0 && isset($installment) && count($installment) > 0 )

	<div class="row">
		@foreach($installment as $index => $value)
			<div class="col-md-12">
				<?php 
					if ($value->INSTALLMENT == 2)
                        $no = 'য়';
                    elseif ($value->INSTALLMENT == 3)
                        $no = 'য়';
                    elseif ($value->INSTALLMENT == 4)
                        $no = 'র্থ';
                    elseif ($value->INSTALLMENT == 6)
                        $no = 'ষট্';
                    else
                        $no = 'ম';

					if ( date('Y-m-d') > date('Y-m-d', strtotime($value->DATE)) ) {
						$punishment_amount = ($value->AMOUNT * $value->PUNISHMENT)/100;
                        $total_amount = $value->AMOUNT + $punishment_amount;
					} else {
						$total_amount = $value->AMOUNT;
					}

					if ($value->IS_PAID == 0) {
						$pay_status = '&#40;<strong style="color:#a94442;">'.trans('common.unpaid').'</strong>&#41;';
						$pay_status .= ' <strong>'.trans('common.paid').': </strong> '.Helpers::numberConvert(0).' '.trans('common.taka');
					} else {
						$pay_status = '&#40;<strong style="color:#a94442;">'.trans('common.paid').'</strong>&#41;';
						if ($value->IS_APPROVED == 1) {
							$pay_status .= ' <strong>'.trans('common.paid').': </strong> '.Helpers::numberConvert(number_format($total_amount, 2)).' '.trans('common.taka');
						} else {
							$pay_status .= ' &#40;<strong style="color:#a94442;">'.trans('common.approve_wait').'</strong>&#41';
						}
						$pay_status .= '<br><strong>'.trans('common.payment_type').'</strong>: ';

						$payment_info = DB::table('ac_pgw_return')->where('TRX_TRAN_NO', $value->TRX_TRAN_NO)->first();

						if (!empty($payment_info)) {
							if ($payment_info->PAYMENT_TYPE == 1) {
								$pay_status .= trans('common.bank_draft');
								$pay_status .= '<br><strong>'.trans('common.bank_draft_number').'</strong>: '.$payment_info->B_DRAFT_NO;
								$pay_status .= '<br><strong>'.trans('common.attachment').'</strong>: ';
								if (!empty($payment_info->ATTACHMENT) && File::exists(public_path()."/upload/bank/".$payment_info->ATTACHMENT)) {
									$pay_status .= '<a target="_blank" href="'.Helpers::asset('upload/bank/'.$payment_info->ATTACHMENT).'">'.$payment_info->ATTACHMENT.'</a>';
								}
							} elseif ($payment_info->PAYMENT_TYPE == 3) {
								$pay_status .= trans('common.cash');
								$pay_status .= '<br><strong>'.trans('common.challan_no').'</strong>: '.$payment_info->B_DRAFT_NO;
								if (!empty($payment_info->ATTACHMENT) && File::exists(public_path()."/upload/bank/".$payment_info->ATTACHMENT)) {
									$pay_status .= '<br><strong>'.trans('common.attachment').'</strong>: ';
									$pay_status .= '<a target="_blank" href="'.Helpers::asset('upload/bank/'.$payment_info->ATTACHMENT).'">'.$payment_info->ATTACHMENT.'</a>';
								}
							} else {
								$pay_status .= trans('common.online');
								$pay_status .= '<br><strong>'.trans('common.bank_tran_number').'</strong>: '.$payment_info->BANK_TRAN_ID;
							}
						}
					}
				?>
				@if( date('Y-m-d') > date('Y-m-d', strtotime($value->DATE)) && $value->IS_PAID == 0 )
					<div class="alert alert-danger">
				@elseif($value->IS_PAID == 0)
					<div class="alert alert-warning">
				@elseif($value->IS_PAID == 1)
					<div class="alert alert-success">
				@else
					<div class="alert alert-info">
				@endif
	                <p style="margin-bottom: 5px;">
	                	<strong>{{ Helpers::numberConvert($value->INSTALLMENT).$no }} কিস্তি: </strong>
	                	@if($value->IS_PAID == 1)
	                		<?php 
	                			$paid_ins = DB::table('ac_vn_ledgers')
												->where('TRX_TRAN_NO', $value->TRX_TRAN_NO)
												->where('TRX_CODE_ID', 5)
												->sum('DR_AMT');
	                		?>
	                		{{ Helpers::numberConvert( number_format($paid_ins) ) }} {{ trans('common.taka') }}{!! $pay_status !!}
	                	@else
	                		{{ Helpers::numberConvert( number_format($value->AMOUNT) ) }} {{ trans('common.taka') }}{!! $pay_status !!}
	                	@endif
	                </p>
	                @if($value->IS_PAID == 0)
	                	<p style="margin-bottom: 5px;">
	                		<strong>{{trans('citizen.Payment_Deadline')}}পরিশোধের শেষ তারিখ:</strong> 
	                		{{ date('d-m-Y', strtotime($value->DATE)) }}
	                	</p>
	                	@if ( date('Y-m-d') > date('Y-m-d', strtotime($value->DATE)) )
	                		<p style="margin-bottom: 10px;">{{trans('citizen.But_due_to_the_condition_of_your_installment_due_to_the_condition_of_the_tender')}} {{ $value->PUNISHMENT }} শতাংশ জরিমানা ধরে {{ Helpers::numberConvert($value->INSTALLMENT).$no }} কিস্তির মোট <strong>{{ Helpers::numberConvert(number_format($total_amount, 2)) }}</strong> {{ trans('common.taka') }}</p>
	                	@endif
	                	<?php 
	                		$payable_amount = round($total_amount, 2);
	                		if (isset($ctz_data) && !empty($ctz_data)) {
	                			$return_url = route('installment_pay', ['citizen', $value->TENDER_INS_ID, $value->TRX_TRAN_NO, $value->SCHEDULE_ID]);
	                			$cus_name = $ctz_data->getTenderCitizen->FULL_NAME;
	                			$cus_email = empty($ctz_data->getTenderCitizen->EMAIL)?Config::get('mail')['from']['address']:$ctz_data->getTenderCitizen->EMAIL;
	                			$cus_phone = $ctz_data->getTenderCitizen->MOBILE_NO;
	                			$value = ['citizen', $value->TENDER_INS_ID, $value->TRX_TRAN_NO, $value->SCHEDULE_ID, $user_id];
	                		} else {
	                			$return_url = route('installment_pay', ['applicant', $value->TENDER_INS_ID, $value->TRX_TRAN_NO, $value->SCHEDULE_ID]);
	                			$cus_name = $applicant_data->APPLICANT_NAME;
	                			$cus_email = empty($applicant_data->EMAIL)?Config::get('mail')['from']['address']:$applicant_data->EMAIL;
	                			$cus_phone = $applicant_data->APPLICANT_PHONE;
	                			$value = ['applicant', $value->TENDER_INS_ID, $value->TRX_TRAN_NO, $value->SCHEDULE_ID, $user_id];
	                		}
	                		$user_arr = array('amount'=>$payable_amount, 'particular' =>8, 'url'=>$return_url,'name'=>$cus_name,'email'=>$cus_email,'phone'=>$cus_phone,'val'=>$value);
	                		$user_jsn = json_encode($user_arr);

	                	?>
	                	<a href="#" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#modal_add_content" data-form="{{ route('payment_selection') }}?up_data={{ $user_jsn }}">{{ trans('citizen.pay') }}</a>
	            	@endif
	            </div>
			</div>
		@endforeach
	</div>
	
@endif
