@extends('admin_access.layout.master')

@section('style')

	<style type="text/css">
		img.form_preloader{
			display: none;
			margin-top: 20px;
			width: 32px;
		}
	</style>

@endsection

@section('script')

	<script src="{{ Helpers::asset('assets/admin/js/payment.js') }}" type="text/javascript"></script>	
	<script type="text/javascript">
		var notification_url = "{{ Auth::check()?route('citizen_notification_data'):route('app_notification_data') }}";

		$(document).ready(function() {

			paymentForm.init();

			$(document.body).on('change', '#tender_project', function() {
			    var state = $(this);
			    var schedule_id = state.val();
			    $('.portlet .contentArea').html('')
			    if (schedule_id != '') {
			        state.closest('form').find('img.form_preloader').show();

			        $.ajax({
			            url: notification_url,
			            type: 'GET',
			            dataType: 'html',
			            data: {schedule_id: schedule_id},
			        })
			        .done(function(response) {
			            state.closest('form').find('img.form_preloader').hide();
			            $('.portlet .contentArea').html(response);
			        });
			    }
			});

			$(document.body).on('click', '.payment_confirmation', function(event) {
                event.preventDefault();
                var form = $(this).closest('form');
                swal({   
                    title: "Are you sure?",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#008000",
                    confirmButtonText: "Yes, Pay it!",
                    cancelButtonText: "No, cancel",
                    closeOnConfirm: false, //If false then confirm message will enable
                    closeOnCancel: true
                }, function(isConfirm){
                    if (isConfirm) {
                        form.submit();
                    }
                });
            });

		});
	</script>

@endsection

@section('content')

	<h3>{{ trans('citizen.application_status') }}</h3>
	<div class="row">
		<div class="col-md-6">
			<!--========Session message=======-->
            @if (Session::has('success'))
                <div class="alert alert-success">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  {{ Session::get('success') }}
                </div>
            @endif
            @if (Session::has('error'))
                <div class="alert alert-danger">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  {{ Session::get('error') }}
                </div>
            @endif
            @if (count($errors) > 0)
                @foreach ($errors->all() as $error)
                  <div class="alert alert-danger">
                      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                      {{ $error }}
                  </div>
                @endforeach
            @endif
            <!--======end Session message=====-->
		</div>
	</div>
	<div class="row" style="margin-bottom:15px;">
		{!! Form::open(array('url' => '#', 'class'=>'form', 'method' => 'post')) !!}

			<div class="col-md-4 col-sm-4">
				{!! Form::label('project_id', trans('project.project_or_mohal_name')) !!}
				<select name="project_id" class="form-control" id="tender_project">
					<option value="">{{ trans('common.form_select') }}</option>
					@foreach($project as $key => $pr)
						<option value="{{ $pr->SCHEDULE_ID }}">
							{{ $pr->getSchedule->getProject->PR_NAME_BN }} / 
                            {{ $pr->getSchedule->getCategory->CATE_NAME }}
							@if( !empty($pr->getSchedule->getProjectDetail) )
                             	/ {{ $pr->getSchedule->getProjectType->TYPE_NAME }} / 
                            	{{ $pr->getSchedule->getProjectDetail->PR_SSF_NO }}
                            @endif
						</option>
					@endforeach
				</select>
			</div>
			<div class="col-md-2 col-sm-2">
				<img src="{{ Helpers::asset('assets/img/loader.gif') }}" alt="" class="form_preloader">
			</div>

		{!! Form::close() !!}
	</div>

	<div class="portlet light bordered">
	    <div class="portlet-body contentArea"></div>
	</div>

@endsection