@extends('admin_access.layout.master')

@section('css_file')

@endsection

@section('style')
    
    <style type="text/css">
        .applicant_form_info label{
            font-weight: 700;
        }
        .applicant_form_info p{
            margin-top: 0px;
            margin-bottom: 5px;
        }
        .row_each{
            padding-bottom: 10px;
        }

    </style>

@endsection

@section('js_file')

@endsection

@section('script')

	<script type="text/javascript">
            $(document).ready(function() {
	        $('.payment_confirmation').click(function(event) {
                event.preventDefault();
                var form = $(this).closest('form');
                swal({   
                    title: "Are you sure?",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#008000",
                    confirmButtonText: "Yes, Pay it!",
                    cancelButtonText: "No, cancel",
                    closeOnConfirm: false, //If false then confirm message will enable
                    closeOnCancel: true
                }, function(isConfirm){
                    if (isConfirm) {
                            form.submit();
                        }
                    });
                });
	    });
	</script>

@endsection

@section('content')

	<div class="row">
        <div class="col-sm-6">
            <h3 class="page_heading">{{ trans('admin.dashboard') }}
<!--                <small>{{ trans('admin.dashboard_heading') }}</small>-->
            </h3>
            <!--========Session message=======-->
            @if (Session::has('success'))
                <div class="alert alert-success">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  {{ Session::get('success') }}
                </div>
            @endif
            @if (Session::has('error'))
                <div class="alert alert-danger">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  {{ Session::get('error') }}
                </div>
            @endif
            @if (count($errors) > 0)
                @foreach ($errors->all() as $error)
                  <div class="alert alert-danger">
                      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                      {{ $error }}
                  </div>
                @endforeach
            @endif
            <!--======end Session message=====-->
        </div>
    </div>
    <!-- END PAGE TITLE-->
    <!-- END PAGE HEADER-->
    <!-- BEGIN DASHBOARD STATS 1-->
    <div class="row">
        
        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
            <a class="dashboard-stat dashboard-stat-v2 green openModal" href="{{ route('application_form_history') }}" data-action="revenue/revenue_khat">
                <div class="visual">
                    <i class="fa fa-bar-chart-o"></i>
                </div>
                <div class="details">
                    <div class="number">
                        <span data-counter="counterup" data-value="{{ Helpers::numberConvert($total_apply) }}"></span>{{ Helpers::numberConvert($total_apply) }} টি</div>
                    <div class="desc">আবেদন</div>
                </div>
            </a>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
            <a class="dashboard-stat dashboard-stat-v2 red openModal" href="{{ route('application_form_history') }}" data-action="revenue/revenue_khat">
                <div class="visual">
                    <i class="fa fa-shopping-cart"></i>
                </div>
                <div class="details">
                    <div class="number">
                        <span data-counter="counterup" data-value="{{ Helpers::numberConvert($total_approve) }}">{{ Helpers::numberConvert($total_approve) }} টি</span>
                    </div>
                    <div class="desc">অনুমোদিত</div>
                </div>
            </a>
        </div>
       <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
            <div class="dashboard-stat dashboard-stat-v2 purple openModal">
                <div class="visual">
                    <i class="fa fa-globe"></i>
                </div>
                <div class="details">    
                    <div class="desc">
                        <h4> <b>সাম্প্রতিক প্রকাশিত</b></h4>
                    </div>
                    <a target="_blank" href="{{ route('get_tender') }}" class="desc"> 
                        <i class="fa fa-share"></i> {{ Helpers::numberConvert($current_tender) }} টি নতুন টেন্ডার  
                    </a><br>
                    <a target="_blank" href="{{ route('get_lease') }}" class="desc"> 
                        <i class="fa fa-share"></i> {{ Helpers::numberConvert($current_lease) }} টি নতুন ইজারা  
                    </a>
                </div>
            </div>
        </div>
    </div>

@endsection