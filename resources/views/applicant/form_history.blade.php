@extends('admin_access.layout.master')

@section('style')

	<style type="text/css">
		img.form_preloader{
			display: none;
			margin-top: 20px;
			width: 32px;
		}
		.applicant_form label{
			font-weight: 700;
		}
	</style>

@endsection

@section('script')
	
	<script type="text/javascript">
		var application_form_url = "{{ Auth::check()?route('citizen_form_data'):route('get_form_data') }}";

		$(document).ready(function() {
			$(document.body).on('change', '#tender_project', function() {
			    var state = $(this);
			    var schedule_id = state.val();
			    $('.portlet .contentArea').html('')
			    if (schedule_id != '') {
			        state.closest('form').find('img.form_preloader').show();

			        $.ajax({
			            url: application_form_url,
			            type: 'GET',
			            dataType: 'html',
			            data: {schedule_id: schedule_id},
			        })
			        .done(function(response) {
			            state.closest('form').find('img.form_preloader').hide();
			            $('.portlet .contentArea').html(response);
			        });
			    }
			});
		});
	</script>

@endsection

@section('content')

	<h3>{{ trans('citizen.app_form_info') }}</h3>
	<div class="row" style="margin-bottom:15px;">
		{!! Form::open(array('url' => '#', 'class'=>'form', 'method' => 'post')) !!}

			<div class="col-md-4 col-sm-4">
				{!! Form::label('project_id', trans('project.project_or_mohal_name')) !!}
				<select name="project_id" class="form-control" id="tender_project">
					<option value="">{{ trans('common.form_select') }}</option>
					@foreach($project as $key => $pr)
						<option value="{{ $pr->SCHEDULE_ID }}">
							{{ $pr->getSchedule->getProject->PR_NAME_BN }} / 
                            {{ $pr->getSchedule->getCategory->CATE_NAME }}
							@if( !empty($pr->getSchedule->getProjectDetail) )
                             	/ {{ $pr->getSchedule->getProjectType->TYPE_NAME }} / 
                            	{{ $pr->getSchedule->getProjectDetail->PR_SSF_NO }}
                            @endif
						</option>
					@endforeach
				</select>
			</div>
			<div class="col-md-2 col-sm-2">
				<img src="{{ Helpers::asset('assets/img/loader.gif') }}" alt="" class="form_preloader">
			</div>

		{!! Form::close() !!}
	</div>

	<div class="portlet light bordered">
	    <div class="portlet-body contentArea"></div>
	</div>

@endsection