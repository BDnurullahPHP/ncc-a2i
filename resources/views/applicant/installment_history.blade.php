@extends('admin_access.layout.master')

@section('style')
    
    <style type="text/css">
        img.form_preloader{
            display: none;
            margin-top: 20px;
            width: 32px;
        }
    </style>

@endsection

@section('script')

	<script type="text/javascript">

        var installment_url = "{{ Auth::check()?route('citizen_installment_data'):route('applicant_installment_data') }}";

        $(document).ready(function() {

	        $(document.body).on('click', '.payment_confirmation', function(event) {
                event.preventDefault();
                var form = $(this).closest('form');
                swal({   
                    title: "Are you sure?",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#008000",
                    confirmButtonText: "Yes, Pay it!",
                    cancelButtonText: "No, cancel",
                    closeOnConfirm: false, //If false then confirm message will enable
                    closeOnCancel: true
                }, function(isConfirm){
                    if (isConfirm) {
                        form.submit();
                    }
                });
            });
            
            $(document.body).on('change', '#tender_schedule', function() {
                var state = $(this);
                var schedule_id = state.val();
                $('.portlet .contentArea').html('')
                if (schedule_id != '') {
                    state.closest('form').find('img.form_preloader').show();

                    $.ajax({
                        url: installment_url,
                        type: 'GET',
                        dataType: 'html',
                        data: {schedule_id: schedule_id},
                    })
                    .done(function(response) {
                        state.closest('form').find('img.form_preloader').hide();
                        $('.portlet .contentArea').html(response);
                    });
                }
            });

	    });
	</script>

@endsection
@section('content')

	<h3>{{ trans('tender.installment_information') }}</h3>
    <div class="row" style="margin-bottom:15px;">
        {!! Form::open(array('url' => '#', 'class'=>'form', 'method' => 'post')) !!}

            <div class="col-md-4 col-sm-4">
                {!! Form::label('schedule_id', trans('tender.flat_space_shop_list')) !!}
                <select name="schedule_id" class="form-control" id="tender_schedule">
                    <option value="">{{ trans('common.form_select') }}</option>
                    @foreach($schedule as $key => $sc)
                        <option value="{{ $sc->SCHEDULE_ID }}">
                            {{ $sc->getSchedule->getProject->PR_NAME_BN }} / 
                            {{ $sc->getSchedule->getCategory->CATE_NAME }} / 
                            {{ $sc->getSchedule->getProjectType->TYPE_NAME }} / 
                            {{ $sc->getSchedule->getProjectDetail->PR_SSF_NO }}
                        </option>
                    @endforeach
                </select>
            </div>
            <div class="col-md-2 col-sm-2">
                <img src="{{ Helpers::asset('assets/img/loader.gif') }}" alt="" class="form_preloader">
            </div>

        {!! Form::close() !!}
    </div>

	<div class="portlet light bordered">

	    <div class="row">
	    	<div class="col-md-5 col-sm-6 col-xs-12">
	    		<div id="lg_form_message"></div>
	    	</div>
	    </div>
	    <div class="portlet-body contentArea"></div>
	</div>

@endsection