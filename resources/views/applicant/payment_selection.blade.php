{!! Form::open(array('url' => '#', 'id'=>'payment_form', 'class'=>'form-horizontal', 'method' => 'post')) !!}

<!--=============Title for modal===============-->
<div class="modal_top_title">{{ trans('citizen.payment_information') }}</div>
<!--===========End title for modal=============-->

<!--========Show form post message=============-->
<div class="form-group">
    <div class="col-sm-6 form_messsage">

    </div>
</div>
<!--========End form post message=============-->
<div class="form-group">
    <div class="col-md-4 col-sm-4">
        {!! Form::label('TOTAL_AMOUNT', trans('citizen.total_taka')) !!}
    </div>
    <div class="col-md-7 col-sm-7">
        {!! Form::text('TOTAL_AMOUNT', $payment_data->amount, array('class'=>'form-control', 'readonly'=>true)) !!}
        {!! Form::hidden('PARTICULAR_ID', $payment_data->particular, array('id'=>'PARTICULAR_ID', 'class'=>'form-control', 'readonly'=>true)) !!}
    </div>
</div>

<div class="form-group">
    <div class="col-md-4 col-sm-4">
        {!! Form::label('PAYMENT_METHOD', trans('citizen.Payment_Types'))!!} <span style="color: red">*</span>
    </div>
    <div class="col-md-7 col-sm-7">
        @if( Auth::check() && !Auth::user()->hasRole('citizen') )
            {!! Form::select('PAYMENT_METHOD', [1=>'ব্যাংক পে-অর্ডার', 3=>'ক্যাশ'], array(Input::old('PAYMENT_METHOD')), array('id'=>'payment_method', 'class'=>'form-control', 'placeholder' => trans('common.form_select'))) !!}
        @else
            {!! Form::select('PAYMENT_METHOD', [1=>'ব্যাংক পে-অর্ডার', 2=>'অনলাইন'], array(Input::old('PAYMENT_METHOD')), array('id'=>'payment_method', 'class'=>'form-control', 'placeholder' => trans('common.form_select'))) !!}
        @endif
    </div>
</div>

<div class="form-group bank_pay_order">
    <div class="col-md-4 col-sm-4">
        {!! Form::label('B_DRAFT_NO', trans('citizen.Pay_order_number')) !!} <span style="color: red">*</span>
    </div>
    <div class="col-md-7 col-sm-7">
        {!! Form::text('B_DRAFT_NO', Input::old('B_DRAFT_NO'), array('class'=>'form-control', 'placeholder'=>trans('citizen.Enter_the_pay_order_number'))) !!}
    </div>
</div>

<div class="form-group bank_pay_order">
    <div class="col-md-4 col-sm-4">
        {!! Form::label('BANK_ID', trans('citizen.Pay_order_bank_name')) !!} <span style="color: red">*</span>
    </div>
    <div class="col-md-7 col-sm-7">
        {!! Form::select('BANK_ID', $bank_list, array(Input::old('BANK_ID')), array('id'=>'bank_selection', 'class'=>'form-control ', 'placeholder' => trans('common.form_select'))) !!}
    </div>
</div>

<div class="form-group bank_pay_order">
    <div class="col-md-4 col-sm-4">
        {!! Form::label('BRANCH_ID', trans('citizen.Bank_branch_name')) !!}
    </div>
    <div class="col-md-7 col-sm-7">
        {!! Form::select('BRANCH_ID', [], array(Input::old('BRANCH_ID')), array('id'=>'branch_selection', 'class'=>'form-control ', 'placeholder' => trans('common.form_select'))) !!}
    </div>
</div>

<div class="form-group bank_pay_order">
    <div class="col-md-4 col-sm-4">
        {!! Form::label('B_DRAFT_DATE', trans('citizen.Pay_order_date')) !!} <span style="color: red">*</span>
    </div>
    <div class="col-md-7 col-sm-7">
        <div class="input-group">
            {!! Form::text('B_DRAFT_DATE', Input::old('B_DRAFT_DATE'), array('class'=>'form-control date_picker_default', 'placeholder'=>'Ex.'.date('d-m-Y'))) !!}
            <div class="input-group-addon">
                <span class="glyphicon glyphicon-calendar"></span>
            </div>
        </div>
        <div id="error_msg1"></div>
    </div>
</div>

<div class="form-group bank_pay_order">
    <div class="col-md-4 col-sm-4">
        {!! Form::label('BANK_ATTACHMENT', trans('citizen.Pay_order_attachment')) !!} <span style="color: red">*</span>
        <p style="margin:0px;"><small>{{trans('citizen.pay_order_Attachment_type')}}</small></p>
    </div>
    <div class="col-md-7 col-sm-7">
        {!! Form::file('BANK_ATTACHMENT', ['class'=>'form-control']) !!}
    </div>
</div>

<div class="form-group cash_order">
    <div class="col-md-4 col-sm-4">
        {!! Form::label('CHALAN_NO', trans('citizen.Invoice_No')) !!} <span style="color: red">*</span>
    </div>
    <div class="col-md-7 col-sm-7">
        {!! Form::text('CHALAN_NO', Input::old('CHALAN_NO'), array('class'=>'form-control', 'placeholder'=>trans('citizen.Enter_the_invoice_number'))) !!}
    </div>
</div>

<div class="form-group cash_order">
    <div class="col-md-4 col-sm-4">
        {!! Form::label('CASH_DATE', trans('citizen.date')) !!} <span style="color: red">*</span>
    </div>
    <div class="col-md-7 col-sm-7">
        <div class="input-group">
            {!! Form::text('CASH_DATE', Input::old('CASH_DATE'), array('class'=>'form-control date_picker_default', 'placeholder'=>'Ex.'.date('d-m-Y'))) !!}
            <div class="input-group-addon">
                <span class="glyphicon glyphicon-calendar"></span>
            </div>
        </div>
        <div id="error_msg2"></div>
    </div>
</div>

<div class="form-group cash_order">
    <div class="col-md-4 col-sm-4">
        {!! Form::label('CASH_ATTACHMENT', trans('citizen.Attachment')) !!}
        <p style="margin:0px;"><small>{{trans('citizen.Attachment_type')}}</small></p>
    </div>
    <div class="col-md-7 col-sm-7">
        {!! Form::file('CASH_ATTACHMENT', ['class'=>'form-control']) !!}
    </div>
</div>


<div class="form-group">
    <div class="col-md-4 col-sm-4">
        {!! Form::label('COMMENTS', trans('citizen.comment')) !!}
    </div>
    <div class="col-md-7">
        {!! Form::textarea('COMMENTS', null, array('class'=>'form-control', 'rows'=>3, 'placeholder'=>trans('common.your_comment'))) !!}
    </div>
</div>

<div class="form-group">
    <div class="col-md-11" style="padding-top: 15px;">
        <span class="modal_msg pull-left"></span>
        <button type="submit" data-action="{{ route('post_payment', [$payment_data->val[0], $payment_data->val[1], $payment_data->val[2], $payment_data->val[3], $payment_data->val[4]]) }}" class="btn btn-success btn-sm pull-right payment_form_submit" style="margin-left: 5px;">{{ trans('common.form_submit') }}</button>
        <button type="button" class="btn btn-default btn-sm reset_form pull-right">{{ trans('common.form_reset') }}</button>
        <span class="loadingImg"></span>
    </div>
</div>

{!! Form::close() !!}

{!! Form::open(array('url' => $payment_api['dev']?$payment_api['test_url']:$payment_api['url'], 'name' => 'payment_gw', 'method' => 'POST', 'id'=>'payment_gw', 'class' =>'form-horizontal')) !!}

    {!! Form::hidden('total_amount', $payment_data->amount) !!}
    {!! Form::hidden('store_id', $payment_api['dev']?$payment_api['test_store_id']:$payment_api['store_id']) !!}
    {!! Form::hidden('tran_id', 'tran'.str_random(10)) !!}
    {!! Form::hidden('success_url', $payment_data->url) !!}
    {!! Form::hidden('fail_url', $payment_data->url) !!}
    {!! Form::hidden('cancel_url', $payment_data->url) !!}
    {!! Form::hidden('cus_name', $payment_data->name) !!}
    {!! Form::hidden('cus_email', $payment_data->email) !!}
    {!! Form::hidden('cus_phone', $payment_data->phone) !!}
    {!! Form::hidden('version', '2.00') !!}
    {!! Form::hidden('currency', 'BDT') !!}
        
    <button type="submit" style="display:none !important;" class="payment_post_form"></button>

{!! Form::close() !!}