<div class="row">
	<div class="col-md-3">
		<p><strong>নাম</strong></p>
		{{ $applicant_name }}
	</div>
	<div class="col-md-3">
		<p><strong>পিতার/স্বামীর নাম</strong></p>
		{{ $applicant_fname }}
	</div>
	<div class="col-md-4">
		<p><strong>ঠিকানা</strong></p>
		{{ $applicant_address }}
	</div>
	<div class="col-md-2">
		@if($type == 1)
			<a href="#" title="Ownership change" class="btn btn-warning btn-sm" data-toggle="modal" data-target="#modal_big_content" data-form="{{ route('user_ownership_letter', $parameters) }}"><i class="fa fa-pencil"></i> পরিবর্তন করুণ</a>
		@else
			<a href="#" title="Ownership change" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#modal_big_content" data-form="{{ route('user_ownership_letter', $parameters) }}"><i class="fa fa-ban"></i> বাতিল করুণ</a>
		@endif
	</div>
</div>
