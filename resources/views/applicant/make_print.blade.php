<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>শিল্প প্রতিষ্ঠান নিবন্ধন ফরম</title>
    </head>
    <style>
      body {
          font-family: freeserif,sans-serif,vrinda,solaiman-lipi,monospace;
        }
        h1, h2, h3, h4, p, input, th, td{
          font-family: freeserif,sans-serif,vrinda,solaiman-lipi,monospace;
          font-weight: 400;
          white-space: initial;
        }
        .sectionrow
        {
            width: 100%;
            border: 1px ;            
            border-style: solid ;
            font-size: 12px; 
        }
        h3{text-align:center; }
        
    </style>
    <body>
         <h3>বাংলাদেশ ক্ষুদ্র ও কুটির শিল্প করপোরেশন<br>
            প্রধানমন্ত্রীর কার্যালয়ের a2i প্রোগ্রামের আওতায় বাস্তবায়নাধীন বিসিক এর উদ্ভাবনী উদ্যোগ<br>
            ক্ষুদ্র ও কুটির শিল্পের GIS ভিত্তিক অনলাইন ডাটাবেজ
        </h3>
    </body>
</html>