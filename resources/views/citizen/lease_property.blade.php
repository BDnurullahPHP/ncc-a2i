@extends('admin_access.layout.master')

@section('style')

	<style type="text/css">
		img.form_preloader{
			display: none;
			margin-top: 20px;
			width: 32px;
		}
		.applicant_form label{
			font-weight: 700;
		}
	</style>

@endsection

@section('script')

@endsection

@section('content')

	<h3>{{ trans('lease.lease_information') }}</h3>

	<div class="portlet light bordered">
	    <div class="portlet-body contentArea">
	    	@foreach($lease_project as $key => $lp)
				<div class="note note-success">
		    		<p style="margin-bottom: 5px;">
		    			<strong>{{ trans('lease.mahal_bn_name') }}: </strong>
						{{ $lp->getProject->PR_NAME_BN }}
		    		</p>
		    		<p style="margin-bottom: 5px;">
		    			<strong>{{ trans('common.location') }}: </strong>
		    			{!! $lp->getProject->PROJECT_LOCATION !!}
		    		</p>
		    		<p>
		    			<strong>{{ trans('common.duration') }}: </strong>
		    			{{ date('d/m/Y', strtotime($lp->DATE_FROM)) }} {{ trans('common.to') }} {{ date('d/m/Y', strtotime($lp->DATE_TO)) }}
		    		</p>
		    	</div>
	    	@endforeach
	    </div>
	</div>

@endsection