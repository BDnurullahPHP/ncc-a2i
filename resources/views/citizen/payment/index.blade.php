@extends('admin_access.layout.master')

@section('style')
	<style type="text/css">
	    .ui-datepicker{
	        z-index: 99999 !important;
	    }
	    .portlet{
	    	padding-left: 15px !important;
	    	padding-right: 15px !important;
	    }
	    .portlet.box.green>.portlet-title, .portlet.green, .portlet>.portlet-body.green{
	    	background-color: #609513;
	    }
	    .portlet-body{
	    	color: #333;
	    }
	    .portlet-body .row{
	    	padding-bottom: 10px;
	    }
	    .input-group-addon>i {
		    color: #616161;
		}
		.input-group, .form-group{
			width: 100%;
		}
		.form-group{
			padding-bottom: 10px;
		}
		label, .control-label{
			font-weight: 600 !important;
		}
		.input-group-addon{
			width: 42px !important;
		}
		#project_details{
			width: 100% !important;
		}
		ul.rent_check_list{
			padding-left: 0px;
		}
		ul.rent_check_list li{
			display: block;
			background-color: #DADADA;
			width: 100%;
			float: left;
			list-style: none;
			padding-top: 5px;
			padding-bottom: 5px;
			margin-bottom: 2px;
		}
		.calculation hr{
			height: 2px;
		    border-top: 1px solid #9C9C9C;
		    border-bottom: 1px solid #9C9C9C;
		    margin-top: 5px;
		    margin-bottom: 5px;
		}
		.calculation p{
			margin-bottom: 5px;
		}
		span.text-amount{
			display: block;
			padding-top: 8px;
		}
		h3.payment_heading{

		}
		.form-horizontal .form-group{
			margin-bottom: 0px;
		}
		span.extra-padd{
			padding-left: 12px;
		}
		h4.sub_heading{
			padding-top: 5px;
		    padding-bottom: 5px;
		    background-color: #DADADA;
		    padding-left: 12px;
		}
		.borderHr{
			border:0.5px solid #000;
			height: 2px;
			width: 100%;
		}

		.ui-datepicker-calendar {
		    display: none;
		}
		.borderBtm{
			border-bottom:1px dotted gray;
		}
		.borderAppended
		{
			border-bottom:0.5px dotted #000;
			width: 100%;
		}
		.payment_cont img.pre_loader{
			display: none;
		}

	</style>
@endsection

@section('content')

	<h3>{{ trans('citizen.rent_information') }}
    	<small></small>
	</h3>

	<div class="portlet box green">
		<div class="portlet-title">
            <div class="caption">
                {{ $citizen->FULL_NAME }}
            </div>
            <div class="tools">
                <a href="#" class="collapse" title=""> </a>
            </div>
        </div>
        <div class="portlet-body" id="citizen_info">
        	<div class="row">
        		<div class="col-md-3 col-sm-3 col-xs-5"><strong>পিতার/স্বামীর নাম</strong></div>
        		<div class="col-md-9 col-sm-9 col-xs-7">
					@if(!empty($citizen->getCitizen))
        				{{ $citizen->getCitizen->SPOUSE_NAME }}
        			@endif
        		</div>
        	</div>
        	<div class="row">
        		<div class="col-md-3 col-sm-3 col-xs-5"><strong>ঠিকানা</strong></div>
        		<div class="col-md-9 col-sm-9 col-xs-7">
        			@if(!empty($citizen->getCitizen))
        				{{ $citizen->getCitizen->ROAD_NO }}/{{ $citizen->getCitizen->HOLDING_NO }} {{ $thana }}
        			@endif
        		</div>
        	</div>
        </div>
	</div>
	
	<div class="row">
		<div class="col-md-5 col-sm-5 col-xs-12">
			<div class="portlet light bordered">
				<!--========Session message=======-->
	            @if (Session::has('success'))
	                <div class="alert alert-success">
	                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	                  {{ Session::get('success') }}
	                </div>
	            @endif
	            @if (Session::has('error'))
	                <div class="alert alert-danger">
	                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	                  {{ Session::get('error') }}
	                </div>
	            @endif
	            @if (count($errors) > 0)
	                @foreach ($errors->all() as $error)
	                  <div class="alert alert-danger">
	                      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	                      {{ $error }}
	                  </div>
	                @endforeach
	            @endif
	            <!--======end Session message=====-->
			    <h4 class="sub_heading">ভাড়ার খাত</h4>
			    <div class="portlet-body contentArea">
					<form class="form-inline">
						<div class="form-group">
							<label for="">ফ্ল্যাট / স্পেস / দোকানের তালিকা</label>
							<div class="input-group">
								<select name="tender_schedule" class="form-control" id="tender_schedule">
									<option value="">{{ trans('common.form_select') }}</option>
									@foreach($citizen_property as $key => $pr)
										<option value="{{ $pr->SCHEDULE_ID }}">
											{{ $pr->getSchedule->getProject->PR_NAME_BN }} / 
				                            {{ $pr->getSchedule->getCategory->CATE_NAME }}
											@if( !empty($pr->getSchedule->getProjectDetail) )
				                             	/ {{ $pr->getSchedule->getProjectType->TYPE_NAME }} / 
				                            	{{ $pr->getSchedule->getProjectDetail->PR_SSF_NO }}
				                            @endif
										</option>
									@endforeach
								</select>
							</div>
						</div>
						<div class="form-group">
							<label for="">ভাড়া প্রদানের মাস</label>
							<div class="input-group">
								{{-- <select name="rent_pay_month" class="form-control multi_select" multiple="multiple" id="rent_pay_month" data-placeholder="{{ trans('common.form_select') }}">
								</select> --}}
								<select name="rent_pay_month" class="form-control multi_select" id="rent_pay_month">
									<option value="">{{ trans('common.form_select') }}</option>
								</select>
							</div>
						</div>
						<!-- <div class="form-group calculation">
							<div class="col-md-12">
								<p class="text-right" style="color: red;">মোট = 5800 ৳</p>
								<p class="text-right" style="color: green;">পূর্বের জমা  = 0 ৳</p>
							</div>
							<div class="clearfix"></div>
							<hr>
							<div class="col-md-12">
								<p class="text-right" style="color: red;">বাকি = 5800 ৳</p>
							</div>
						</div> -->
					</form>

			    </div>
			</div>
		</div>
		<div class="col-md-7 col-sm-7 col-xs-12 payment_cont">
			<img src="{{ Helpers::asset('assets/img/loader.gif') }}" class="pre_loader center-block" alt="">
			<div id="rent_payment_content"></div>
		</div>
	</div>

@endsection

@section('js_file')

    <script type="text/javascript">
    	var pendingMonthUrl = "{{ route('rent_pending_month') }}";
    	var rentPaymentForm = "{{ route('rent_pay_form') }}";
    </script>
    <script src="{{ Helpers::asset('assets/citizen/payment/payment.js') }}" type="text/javascript"></script>

@endsection

@section('script')
	<script type="text/javascript">
		$(document).ready(function() {
			citizen.init();
		});
	</script>

@endsection