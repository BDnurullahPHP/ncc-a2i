@extends('admin_access.layout.master')

@section('style')
	<style type="text/css">
	    .ui-datepicker{
	        z-index: 99999 !important;
	    }
	    .portlet{
	    	padding-left: 15px !important;
	    	padding-right: 15px !important;
	    }
	    .portlet.box.green>.portlet-title, .portlet.green, .portlet>.portlet-body.green{
	    	background-color: #609513;
	    }
	    .portlet-body{
	    	color: #333;
	    }
	    .portlet-body .row{
	    	padding-bottom: 10px;
	    }
	    .input-group-addon>i {
		    color: #616161;
		}
		.input-group, .form-group{
			width: 100%;
		}
		.form-group{
			padding-bottom: 10px;
		}
		label, .control-label{
			font-weight: 600 !important;
		}
		.input-group-addon{
			width: 42px !important;
		}
		#project_details{
			width: 100% !important;
		}
		ul.rent_check_list{
			padding-left: 0px;
		}
		ul.rent_check_list li{
			display: block;
			background-color: #DADADA;
			width: 100%;
			float: left;
			list-style: none;
			padding-top: 5px;
			padding-bottom: 5px;
			margin-bottom: 2px;
		}
		.calculation hr{
			height: 2px;
		    border-top: 1px solid #9C9C9C;
		    border-bottom: 1px solid #9C9C9C;
		    margin-top: 5px;
		    margin-bottom: 5px;
		}
		.calculation p{
			margin-bottom: 5px;
		}
		span.text-amount{
			display: block;
			padding-top: 8px;
		}
		h3.payment_heading{

		}
		.form-horizontal .form-group{
			margin-bottom: 0px;
		}
		.ui-datepicker-calendar {
		    display: none;
		}
	</style>
@endsection

@section('content')

	<h3>{{ trans('citizen.rent_information') }}
    	<small></small>
	</h3>

	<div class="portlet box green">
		<div class="portlet-title">
            <div class="caption">
                {{ $citizen_user->FULL_NAME }}
            </div>
            <div class="tools">
                <a href="#" class="collapse" title=""> </a>
            </div>
        </div>
        <div class="portlet-body" id="citizen_info">
        	<div class="row">
        		<div class="col-md-3 col-sm-3 col-xs-5"><strong>পিতার/স্বামীর নাম</strong></div>
        		<div class="col-md-9 col-sm-9 col-xs-7">{{ $citizen_user->getCitizen->SPOUSE_NAME }}</div>
        	</div>
        	<div class="row">
        		<div class="col-md-3 col-sm-3 col-xs-5"><strong>ঠিকানা</strong></div>
        		<div class="col-md-9 col-sm-9 col-xs-7">বাসা নং- {{ $citizen_user->getCitizen->HOLDING_NO }}, রাস্তা নং-{{ $citizen_user->getCitizen->ROAD_NO }}</div>
        	</div>
        </div>
	</div>
	
	<div class="portlet light bordered">
	    <div class="portlet-body contentArea">
	        
	        <div class="row">
	        	<div class="col-md-4">
	        		<form class="form-inline">
						<div class="form-group">
							<label for="">খাতের নাম</label>
							<div class="input-group">
							<select name="financialYear" class="form-control finanCialYear">
								<option value="" selected>নির্বাচন করুন </option>
								<option value="">দোকান</option>
							</select>
							</div>
						</div>
					</form>
	        	</div>
                    <div class="col-md-4">
	        		<form class="form-inline">
						<div class="form-group">
							<label for="">অর্থ বছর</label>
							<div class="input-group">
							<select name="financialYear" class="form-control finanCialYear">
								<option value="" selected>অর্থ বছর নির্বাচন করুন </option>
								@for($i=date('Y')+1;$i>=2016;$i--)
									<option value="{{$i}}">{{$i-1}}-{{$i}}</option>
								@endfor
							</select>
								<!--<input type="text" class="form-control datepicker" placeholder="dd/mm/yy">-->
								<div class="input-group-addon"><i class="fa fa-calendar"></i></div>
							</div>
						</div>
						
					</form>
	        	</div>
	        </div>
			
			<table class="table table-striped table-bordered table-hover">
			    <thead>
			        <tr>
			            <th>মাস &#40;<span class="addFinYear">{{date('Y')-1}}-{{date('Y')}}</span>&#41;</th>
			            <th>দাবি (‎৳)</th>
			            <th>ভাউচার নাম্বার</th>
			            <th>তারিখ</th>
			            <th>পরিমাণ (‎৳)</th>
			            <th>অবস্থা </th>
			        </tr>
			    </thead>
			    <tbody>
			    <?php 
			    	for($i=1;$i<=12;$i++){
			    		switch($i){
			    			case 1:
			    				$month='June';
			    				$monthNumber=6;
			    				break;
			    			case 2:
			    				$month='July';
			    				$monthNumber=7;
			    				break;
			    			case 3:
			    				$month='August';
			    				$monthNumber=8;
			    				break;
			    			case 4:
			    				$month='September';
			    				$monthNumber=9;
			    				break;
			    			case 5:
			    				$month='October';
			    				$monthNumber=10;
			    				break;
			    			case 6:
			    				$month='November';
			    				$monthNumber=11;
			    				break;
			    			case 7:
			    				$month='December';
			    				$monthNumber=12;
			    				break;
			    			case 8:
			    				$month='January';
			    				$monthNumber=1;
			    				break;
			    			case 9:
			    				$month='February';
			    				$monthNumber=2;
			    				break;
			    			case 10:
			    				$month='March';
			    				$monthNumber=3;
			    				break;
			    			case 11:
			    				$month='April';
			    				$monthNumber=4;
			    				break;
			    			case 12:
			    				$month='May';
			    				$monthNumber=5;
			    				break;
			    			default:
			    				$month='none';
			    				break; 

			    		}
			    ?>
			    	<tr>
			    		<td>{{$month}}</td>
			    		<td>6000</td>
			    		<td>3822{{$i}}</td>
			    		<td>{{$i+3}}/{{$monthNumber}}/2016</td>
			    		<td>6000</td>

			    		<td>
			    		@if($i<=3)
			    		@if($i%3==0)
			    		<span class="label label-danger"><i class="glyphicon glyphicon-remove-circle"></i></span>
			    		@else 
			    		<span class="label label-success"><i class="glyphicon glyphicon-ok-sign"></i></span>
			    		@endif
			    		@endif
			    		</td>
			    	</tr>
			    	<?php 
			    }
			    	?>
			    	
			    </tbody>
			</table>

	    </div>
	</div>

@endsection

@section('js_file')

    <script src="{{ Helpers::asset('assets/citizen/payment/payment.js') }}" type="text/javascript"></script>

@endsection

@section('script')

	<script type="text/javascript">
		$(document).ready(function() {
			$('.datepicker').datepicker({
	            changeMonth: true,
		        changeYear: true,
		        showButtonPanel: true,
		        dateFormat: 'MM yy',
		        onClose: function(dateText, inst) { 
		            $(this).datepicker('setDate', new Date(inst.selectedYear, inst.selectedMonth, 1));
		        }
	        });
		});
	</script>

@endsection





