@extends('admin_access.layout.master')

@section('style')
	<style type="text/css">
	    .ui-datepicker{
	        z-index: 99999 !important;
	    }
	    .portlet{
	    	padding-left: 15px !important;
	    	padding-right: 15px !important;
	    }
	    .portlet.box.green>.portlet-title, .portlet.green, .portlet>.portlet-body.green{
	    	background-color: #609513;
	    }
	    .portlet-body{
	    	color: #333;
	    }
	    .portlet-body .row{
	    	padding-bottom: 10px;
	    }
	    .input-group-addon>i {
		    color: #616161;
		}
		.input-group, .form-group{
			width: 100%;
		}
		.form-group{
			padding-bottom: 10px;
		}
		label, .control-label{
			font-weight: 600 !important;
		}
		.input-group-addon{
			width: 42px !important;
		}
		#project_details{
			width: 100% !important;
		}
		ul.rent_check_list{
			padding-left: 0px;
		}
		ul.rent_check_list li{
			display: block;
			background-color: #DADADA;
			width: 100%;
			float: left;
			list-style: none;
			padding-top: 5px;
			padding-bottom: 5px;
			margin-bottom: 2px;
		}
		.calculation hr{
			height: 2px;
		    border-top: 1px solid #9C9C9C;
		    border-bottom: 1px solid #9C9C9C;
		    margin-top: 5px;
		    margin-bottom: 5px;
		}
		.calculation p{
			margin-bottom: 5px;
		}
		span.text-amount{
			display: block;
			padding-top: 8px;
		}
		h3.payment_heading{

		}
		.form-horizontal .form-group{
			margin-bottom: 0px;
		}
		.ui-datepicker-calendar {
		    display: none;
		}
		.data_table_wrapper{
			display: none;
		}
	</style>
@endsection

@section('content')

	<h3>{{ trans('citizen.rent_information') }}
    	<small></small>
	</h3>

	<div class="portlet box green">
		<div class="portlet-title">
            <div class="caption">
                {{ $citizen_user->FULL_NAME }}
            </div>
            <div class="tools">
                <a href="#" class="collapse" title=""> </a>
            </div>
        </div>
        <div class="portlet-body" id="citizen_info">
        	<div class="row">
        		<div class="col-md-3 col-sm-3 col-xs-5"><strong>পিতার/স্বামীর নাম</strong></div>
        		<div class="col-md-9 col-sm-9 col-xs-7">{{ $citizen_user->getCitizen->SPOUSE_NAME }}</div>
        	</div>
        	<div class="row">
        		<div class="col-md-3 col-sm-3 col-xs-5"><strong>ঠিকানা</strong></div>
        		<div class="col-md-9 col-sm-9 col-xs-7">বাসা নং- {{ $citizen_user->getCitizen->HOLDING_NO }}, রাস্তা নং-{{ $citizen_user->getCitizen->ROAD_NO }}</div>
        	</div>
        </div>
	</div>
	
	<div class="portlet light bordered">
	    <div class="portlet-body contentArea">
	        
	        <div class="row">
	        	{!! Form::open(array('method'=>'post', 'url'=>'#', 'class'=>'form-inline')) !!}
		        	<div class="col-md-4">
						<div class="form-group">
							<label for="">ফ্ল্যাট / স্পেস / দোকানের তালিকা</label>
							<div class="input-group">
								<select name="tender_schedule" class="form-control schedule">
									<option value="">{{ trans('common.form_select') }}</option>
									@foreach($citizen_property as $key => $pr)
										<option value="{{ $pr->SCHEDULE_ID }}">
											{{ $pr->getSchedule->getProject->PR_NAME_BN }} / 
				                            {{ $pr->getSchedule->getCategory->CATE_NAME }}
											@if( !empty($pr->getSchedule->getProjectDetail) )
				                             	/ {{ $pr->getSchedule->getProjectType->TYPE_NAME }} / 
				                            	{{ $pr->getSchedule->getProjectDetail->PR_SSF_NO }}
				                            @endif
										</option>
									@endforeach
								</select>
							</div>
						</div>
		        	</div>
	                <div class="col-md-4">
						<div class="form-group">
							<label for="">অর্থ বছর</label>
							<div class="input-group">
							<select name="date_range" class="form-control year_range">
								{!! $year_range !!}
							</select>
								<div class="input-group-addon"><i class="fa fa-calendar"></i></div>
							</div>
						</div>
		        	</div>
		        	<div class="col-md-4">
						<div class="form-group">
							<button type="button" class="btn btn-success search_data" style="margin-top: 25px;">
								<i class="fa fa-search" aria-hidden="true"></i> {{ trans('common.search') }}
							</button>
						</div>
		        	</div>
		        {!! Form::close() !!}
	        </div>
			
			<div class="data_table_wrapper">
				<table class="table table-striped table-bordered table-hover search_data_table">
				    <thead>
				        <tr>
				            <th>#</th>
				            <th>মাস</th>
				            <th>ভাউচার নাম্বার</th>
				            <th>তারিখ</th>
				            <th>পরিমাণ (‎৳)</th>
				            <th>অবস্থা </th>
				        </tr>
				    </thead>
				    <tbody></tbody>
				</table>
			</div>
	    </div>
	</div>

@endsection

@section('js_file')
	
	<script type="text/javascript">
		var source_data = "{{ route('rent_history_data') }}";
	</script>
    <script src="{{ Helpers::asset('assets/citizen/payment/payment.js') }}" type="text/javascript"></script>

@endsection

@section('script')

	<script type="text/javascript">
		$(document).ready(function() {
			citizen.init();
		});
	</script>

@endsection





