<div class="portlet light bordered">
	<div class="portlet-body contentArea">
		<h4 class="payment_heading sub_heading">
			<span class="listHeading"> {{ $te_schedule->getProject->PR_NAME_BN }} / {{ $te_schedule->getCategory->CATE_NAME }} / {{ $te_schedule->getProjectType->TYPE_NAME }} / {{ $te_schedule->getProjectDetail->PR_SSF_NO }}</span>
		</h4>

		<?php $payment_api = Config::get('payment'); ?>

		{!! Form::open(array('url' => $payment_api['dev']?$payment_api['test_url']:$payment_api['url'], 'id'=>'payment_gw', 'class'=>'form-horizontal', 'method' => 'post')) !!}
			
			<div class="row">

                {!! Form::hidden('store_id', $payment_api['dev']?$payment_api['test_store_id']:$payment_api['store_id']) !!}
                {!! Form::hidden('tran_id', 'tran'.str_random(10)) !!}
                {!! Form::hidden('success_url', route('rent_payment', [$vchild, $schedule, $citizen_user->getCitizen->CITIZEN_ID]), ['class'=>'getw_param']) !!}
                {!! Form::hidden('fail_url', route('rent_payment', [$vchild, $schedule, $citizen_user->getCitizen->CITIZEN_ID]), ['class'=>'getw_param']) !!}
                {!! Form::hidden('cancel_url', route('rent_payment', [$vchild, $schedule, $citizen_user->getCitizen->CITIZEN_ID]), ['class'=>'getw_param']) !!}
                {!! Form::hidden('version', '2.00') !!}
                {!! Form::hidden('cus_name', $citizen_user->FULL_NAME) !!}
                {!! Form::hidden('cus_email', $citizen_user->EMAIL) !!}
                {!! Form::hidden('cus_phone', $citizen_user->MOBILE_NO) !!}
                {!! Form::hidden('currency', 'BDT') !!}
				
				<div class="col-md-6">
					<!-- <p style="color:red;">* সর্বনিম্ন মাসিক ভাড়া ৫০০ টাকা</p> -->
				</div>		
				<div class="col-md-6">
					<div class="form-group">
						<label for="" class="col-sm-5 control-label">সর্বমোট (৳)</label>
						<div class="col-sm-7">
							<div class="input-group">
								<span class="text-amount"><input type="text" class="totalAmount form-control" name="total_amount" value="{{ $total_amount }}" readonly="readonly" class="form-control"></span>
							</div>
						</div>
					</div>
					<!-- <div class="form-group">
						<label for="" class="col-sm-5 control-label">প্রদান</label>
						<div class="col-sm-7">
							<div class="input-group">
								<input type="text" name="total_amount" value="{{ $total_amount }}" class="payAmount form-control" placeholder="0.00" class="form-control">
							</div>
						</div>
					</div> -->
					<div class="form-group">
						<label for="" class="col-sm-5 control-label">বাকি</label>
						<div class="col-sm-7">
							<div class="input-group">
								<span class="text-amount">
									<input type="text" class="dueAmount form-control"  name="totalAmount" value="0.00" readonly="readonly" class="form-control"/>
								</span>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="form-group">
				<div class="col-md-12">
					<label for="" class="">মন্তব্য</label>
					<textarea class="form-control user_remarks" name="value_a" rows="3"></textarea>
				</div>
				<div class="col-md-12" style="padding-top:20px;">
					<button type="submit" class="btn green btn-sm pull-right payment_confirmation">{{ trans('common.form_submit') }}</button>
				</div>
			</div>

		{!! Form::close() !!}
	</div>
</div>