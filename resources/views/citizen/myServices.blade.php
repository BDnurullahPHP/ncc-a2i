@extends('admin_access.layout.master')

@section('style')
    
    <style type="text/css">
        img.form_preloader{
            display: none;
            margin-top: 20px;
            width: 32px;
        }
    </style>

@endsection

@section('script')

	<script type="text/javascript">
        var service_url = "{{ route('service_data') }}";
		$(document).ready(function() {
	        $(document.body).on('change', '#category', function() {
                var state = $(this);
                var category = state.val();
                $('.portlet .contentArea').html('')
                if (category != '') {
                    state.closest('form').find('img.form_preloader').show();

                    $.ajax({
                        url: service_url,
                        type: 'GET',
                        dataType: 'json',
                        data: {group: category},
                    })
                    .done(function(response) {
                        state.closest('form').find('img.form_preloader').hide();
                        var tableHeaders = '';
                        $.each(response.columns, function(i, val){
                            tableHeaders += "<th>" + val + "</th>";
                        });
                        $('.portlet .contentArea').html('<table class="table table-striped table-bordered table-hover datatable_ajax_header"><thead><tr>'+tableHeaders+'</tr></thead><tbody></tbody></table>');
                        $('table.datatable_ajax_header').dataTable(dataTableSettings(service_url, category));
                    });
                }
            });
	    });
	</script>

@endsection

@section('content')

	<div class="row">
        <div class="col-sm-12">
            <h3 class="page_heading">গৃহীত সেবা 
                <small>{{ trans('admin.dashboard_heading') }}</small>
            </h3>
        </div>
    </div>
    <div class="row" style="margin-bottom:15px;">
        {!! Form::open(array('url' => '#', 'class'=>'form', 'method' => 'get')) !!}

            <div class="col-md-4 col-sm-4">
                {!! Form::label('category', trans('project.category_name')) !!}
                <select name="category" class="form-control" id="category">
                    <option value="">{{ trans('common.form_select') }}</option>
                    @foreach($category as $key => $cat)
                        <option value="{{ $cat->PR_CATEGORY }}">
                            {{ $cat->CATE_NAME }}
                        </option>
                    @endforeach
                </select>
            </div>
            <div class="col-md-2 col-sm-2">
                <img src="{{ Helpers::asset('assets/img/loader.gif') }}" alt="" class="form_preloader">
            </div>

        {!! Form::close() !!}
    </div>
    
    <div class="portlet light bordered">
        <div class="portlet-body contentArea"></div>
    </div>

@endsection