@extends('portal.layout.base')

@section('style')

<style type="text/css">
    .control-label{
        text-align: left !important;
    }
    .form-control{
        height: 34px !important;
        border-radius: 0px;
    }
    .input-group-addon{
        border-radius: 0px;
    }
    textarea#message{
        width: 100%;
        max-width: 100%;
        height: auto !important;
    }
    .btn{
        border-radius: 0px;
    }
    #reset_form{
        margin-right: 10px;
    }
    .contact-heading h3{
        font-size: 24px;
    }
    h3.apply_deading{
        font-size: 22px;
        font-weight: 500;
        margin: 5px 0 15px 0;
    }
    h3.commitment{

    }
    h3.commitment span{
        border-bottom: 1px solid #333;
    }
</style>

@endsection

@section('js_file')

<script src="{{ Helpers::asset('assets/portal/js/online_apply.js') }}" type="text/javascript"></script>

@endsection

@section('script')

<script type="text/javascript">
$(document).ready(function() {
    Register.init();
});
</script>

@endsection

@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="col-lg-1"></div>
        <div class="col-lg-10" style="border: 5px solid purple">
            <div class="contact-heading">
                <h3>নারায়ণগঞ্জ সিটি করপোরেশন নিয়ন্ত্রণাধীন হাট বাজার/সায়রাত মহাল সমূহের ইজারা দরপত্রঃ</h3>
                <!--========Session message=======-->
                @if (Session::has('success'))
                <div class="alert alert-success">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    {{ Session::get('success') }}
                </div>
                @endif
                @if (Session::has('error'))
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    {{ Session::get('error') }}
                </div>
                @endif
                @if (count($errors) > 0)
                @foreach ($errors->all() as $error)
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    {{ $error }}
                </div>
                @endforeach
                @endif
                <!--======end Session message=====-->
            </div>
            <form class="form-horizontal" action="form_layouts.html#">
                <div class="form-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-md-4 control-label">দরদাতার নাম <span class="required">*</span></label>
                                <div class="col-md-8">
                                    <input type="text" name="" id="" placeholder="আপনার নাম এখানে লিখুন" class="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-md-4 control-label">পিতা/স্বামীর নাম <span class="required">*</span></label>
                                <div class="col-md-8">
                                    <input type="text" name="" id="" placeholder="আপনার পিতা/স্বামীর নাম এখানে লিখুন" class="form-control" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-md-4 control-label">মায়ের নাম</label>
                                <div class="col-md-8">
                                    <input type="text" name="" id="" placeholder="আপনার পিতা/স্বামীর নাম এখানে লিখুন" class="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-md-4 control-label">জন্ম তারিখ <span class="required">*</span></label>
                                <div class="col-md-8">
                                    <input type="text" name="" id="" placeholder="আপনার জন্ম তারিখ নির্বাচন করুন" class="form-control" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-md-4 control-label">আমি একজন</label>
                                <div class="col-sm-8">
                                    <label class="radio-inline">
                                        {!! Form::radio('gender', 'male', true, array('class'=>'iech')) !!} পুরুষ
                                    </label>
                                    <label  class="radio-inline">
                                        {!! Form::radio('gender', 'female', false, array('class'=>'iech')) !!} মহিলা
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-md-4 control-label">বৈবাহিক অবস্থা</label>
                                <div class="col-md-8">
                                    <label class="radio-inline">
                                        {!! Form::radio('gender', 'male', false, array('class'=>'iech')) !!} বিবাহিত
                                    </label>
                                    <label  class="radio-inline">
                                        {!! Form::radio('gender', 'female', false, array('class'=>'iech')) !!} অবিবাহিত
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <lave class="col-md-4 control-label">জাতীয়তা</lave>
                                <div class="col-md-8">
                                    <select class="form-control">
                                        <option value="">বাংলাদেশি</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-md-4">জাতীয় পরিচয়পত্র</label>
                                <div class="col-md-8">
                                    <input type="text" name="" id="" placeholder="আপনার জাতীয় পরিচয়পত্রের নাম্বার এখানে লিখুন" class="form-control" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <lavel class="col-md-4">মোবাইল নাম্বার <span class="required">*</span></lavel>
                                <div class="col-md-8">
                                    <input type="text" name="" id="" placeholder="আপনার মোবাইল নাম্বার এখানে লিখুন" class="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <span class="required">
                                প্রাসঙ্গিক তথ্য যে মোবাইল নম্বরে এসএমএস পাঠানো হবে
                            </span>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <h4>বর্তমান ঠিকানা</h4>
                            <div class="form-group">
                                <lave class="col-md-4 control-label">গ্রাম/রাস্তা/বাড়ি নং<span class="required">*</span></lave>
                                <div class="col-md-8">
                                    <textarea name="" id="" class="" rows="1" cols="39"></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">জেলা <span class="required">*</span></label>
                                <div class="col-md-8">
                                    <select class="form-control">
                                        <option value="">নির্বাচন করুন</option>
                                        <option >নারায়াণগঞ্জ</option>
                                        <option >ঢাকা</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">উপজেলা <span class="required">*</span></label>
                                <div class="col-md-8">
                                    <select class="form-control">
                                        <option value="">নির্বাচন করুন</option>
                                        <option >সিদ্ধিরগঞ্জ</option>
                                        <option >নারায়ানগঞ্জ সদর</option>
                                        <option >রূপগঞ্জ</option>
                                        <option >বান্দার</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">পোষ্ট অফিস <span class="required">*</span></label>
                                <div class="col-md-8">
                                    <select class="form-control">
                                        <option value="">নির্বাচন করুন</option>
                                        <option >নারায়াণগঞ্জ</option>
                                        <option >ঢাকা</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-4"><h4>স্থায়ী ঠিকানা</h4></div>
                                <div class="col-md-8">
                                    <lavel >বর্তমান ঠিকানার মতই </lavel>
                                    <input type="checkbox" name="" id="" />
                                </div>
                            </div>
                            <div class="form-group">
                                <lave class="col-md-4 control-label">গ্রাম/রাস্তা/বাড়ি নং<span class="required">*</span></lave>
                                <div class="col-md-8">
                                    <textarea name="" id="" class="" rows="1" cols="39"></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">জেলা <span class="required">*</span></label>
                                <div class="col-md-8">
                                    <select class="form-control">
                                        <option value="">নির্বাচন করুন</option>
                                        <option >নারায়াণগঞ্জ</option>
                                        <option >ঢাকা</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">উপজেলা <span class="required">*</span></label>
                                <div class="col-md-8">
                                    <select class="form-control">
                                        <option value="">নির্বাচন করুন</option>
                                        <option >সিদ্ধিরগঞ্জ</option>
                                        <option >নারায়ানগঞ্জ সদর</option>
                                        <option >রূপগঞ্জ</option>
                                        <option >বান্দার</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">পোষ্ট অফিস <span class="required">*</span></label>
                                <div class="col-md-8">
                                    <select class="form-control">
                                        <option value="">নির্বাচন করুন</option>
                                        <option >নারায়াণগঞ্জ</option>
                                        <option >ঢাকা</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <br/>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-md-4 control-label">টাকা পরিশোধের তথ্য</label>
                                <div class="col-md-8">
                                    <select class="form-control">
                                        <option value="">নির্বাচন করুন</option>
                                        <option>মোবাইল ব্যাংক</option>
                                        <option>অনলাইন ব্যাংক</option>
                                        <option>ব্যাংক চেক</option>
                                        <option>নগদ</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">

                        </div>
                    </div>
                    <div>
                        <div class="row">
                            <div class="col-md-3">
                                <label class="control-label">প্রেরকের নাম্বার</label>
                                <input type="text" name="" id="" class="form-control" placeholder="" />
                            </div>
                            <div class="col-md-3">
                                <label class="control-label">টাকার পরিমাণ</label>
                                <input type="text" name="" id="" class="form-control" placeholder="" />
                            </div>
                            <div class="col-md-3">
                                <label class="control-label">গ্রহীতার নাম্বার</label>
                                <input type="text" name="" id="" class="form-control" placeholder="" />
                            </div>
                            <div class="col-md-3">
                                <label class="control-label">প্রদানের তারিখ</label>
                                <input type="text" name="" id="" class="form-control" placeholder="" />
                            </div>
                        </div>
                        <!----------------->
                        <div class="row">
                            <div class="col-md-3">
                                <label class="control-label">প্রেরকের ব্যাংকের নাম</label>
                                <input type="text" name="" id="" class="form-control" placeholder="" />
                            </div>
                            <div class="col-md-3">
                                <label class="control-label">টাকার পরিমাণ</label>
                                <input type="text" name="" id="" class="form-control" placeholder="" />
                            </div>
                            <div class="col-md-3">
                                <label class="control-label">প্রদানের তারিখ</label>
                                <input type="text" name="" id="" class="form-control" placeholder="" />
                            </div>
                            <div class="col-md-3">
                                <label class="control-label">গ্রহীতার ব্যাংকের নাম</label>
                                <input type="text" name="" id="" class="form-control" placeholder="" />
                            </div>
                        </div>
                        <!----------------->
                        <div class="row">
                            <div class="col-md-3">
                                <label class="control-label">প্রেরকের ব্যাংকের নাম</label>
                                <input type="text" name="" id="" class="form-control" placeholder="" />
                            </div>
                            <div class="col-md-3">
                                <label class="control-label">টাকার পরিমাণ</label>
                                <input type="text" name="" id="" class="form-control" placeholder="" />
                            </div>
                            <div class="col-md-3">
                                <label class="control-label">প্রদানের তারিখ</label>
                                <input type="text" name="" id="" class="form-control" placeholder="" />
                            </div>
                            <div class="col-md-3">
                                <label class="control-label">গ্রহীতার ব্যাংকের নাম</label>
                                <input type="text" name="" id="" class="form-control" placeholder="" />
                            </div>
                            <div class="col-md-3">
                                <label class="control-label">সংযুক্ত নথি</label>
                                <input type="file" name="" id="" class="form-control" placeholder="" />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-2">
                            <label class="control-label"><h4>প্রস্তাবিত দর</h4></label>
                        </div>
                        <div class="col-md-3">
                            <label class="control-label">অংকে</label>
                            <input type="text" name="" id="" class="form-control" placeholder="" />
                        </div>
                        <div class="col-md-4">
                            <label class="control-label">কথায়</label>
                            <input type="text" name="" id="" class="form-control" placeholder="" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-2">
                            <label class="control-label"><h4>জামানতের টাকা</h4></label>
                        </div>
                        <div class="col-md-3">
                            <label class="control-label">অংকে</label>
                            <input type="text" name="" id="" class="form-control" placeholder="" />
                        </div>
                        <div class="col-md-4">
                            <label class="control-label">কথায়</label>
                            <input type="text" name="" id="" class="form-control" placeholder="" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-2">
                            <label class="control-label"><h4>ব্যাংকের তথ্য</h4></label>
                        </div>
                        <div class="col-md-3">
                            <label class="control-label">ব্যাংকের নাম</label>
                            <input type="text" name="" id="" class="form-control" placeholder="" />
                        </div>
                        <div class="col-md-3">
                            <label class="control-label">ব্যাংকের ড্রাফ্‌ট নং</label>
                            <input type="text" name="" id="" class="form-control" placeholder="" />
                        </div>
                        <div class="col-md-4">
                            <label class="control-label">তারিখ</label>
                            <input type="text" name="" id="" class="form-control" placeholder="" />
                        </div>
                    </div>
                </div>
                <br/>
                <div class="form-actions">
                    <div class="row">
                        <div class="col-md-offset-3 col-md-9">
                            <button type="reset" class="btn btn-warning btn-sm" id="reset_form">মুছে ফেলুন</button>
                            <button type="submit" class="btn btn-success btn-sm">দাখিল করুন</button>
                        </div>
                    </div>
                </div>
                <br/>
            </form>
            <div class="col-lg-1"></div>
        </div>
    </div>
    <!--    <div class="col-md-12 col-sm-12 col-xs-12 contact-page-content">
            <div class="row">
                <div class="col-md-9">
                    <div class="contact-heading">
                        <h3>নারায়ণগঞ্জ সিটি করপোরেশন নিয়ন্ত্রণাধীন হাট বাজার/সায়রাত মহাল সমূহের ইজারা দরপত্রঃ</h3>
                        ========Session message=======
                        @if (Session::has('success'))
                        <div class="alert alert-success">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            {{ Session::get('success') }}
                        </div>
                        @endif
                        @if (Session::has('error'))
                        <div class="alert alert-danger">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            {{ Session::get('error') }}
                        </div>
                        @endif
                        @if (count($errors) > 0)
                        @foreach ($errors->all() as $error)
                        <div class="alert alert-danger">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            {{ $error }}
                        </div>
                        @endforeach
                        @endif
                        ======end Session message=====
                    </div>
                </div>
                <div class="col-md-3">
                    <a href="{{ URL::asset('form.pdf') }}" download="{{ URL::asset('form.pdf') }}" class="btn btn-default btn-sm pull-right" style="font-size: 16px;">ফর্ম ডাউনলোড &nbsp;<i class="fa fa-download"></i></a>
                </div>
            </div>
            <div class="row" style="padding-top: 20px;">
                {!! Form::open(array('route' => 'post_application', 'method' => 'post', 'id'=>'application_form', 'class' =>'form-horizontal')) !!}
    
                <div class="col-md-8 col-sm-8 col-xs-12">
                    <div class="row">
    
                        <div class="col-md-12 col-sm-12 col-xs-12">
    
                            <div class="form-group">
                                <div class="col-md-4 control-label">
                                    {!! Form::label('mohal_name', 'প্রস্তাবিত মহালের নাম') !!}
                                    <p>হাট বাজার/সয়ারাত মহালের নাম</p>
                                </div>
                                <div class="col-sm-7">
                                    {!! Form::text('mohal_name', Input::old('mohal_name'), array('class'=>'form-control', 'placeholder'=>'')) !!}
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-4 control-label">
                                    {!! Form::label('', 'ইজারা মেয়াদ/সন') !!}
                                </div>
                                <div class="col-sm-7">
                                    <p>১৪২৩ বাংলা সনের ৩০ শে চৈত্র পর্যন্ত সময়ের জন্য।</p>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-4 control-label">
                                    {!! Form::label('bidder_name', 'দরদাতার নাম') !!}
                                </div>
                                <div class="col-sm-7">
                                    {!! Form::text('bidder_name', Input::old('bidder_name'), array('class'=>'form-control', 'placeholder'=>'')) !!}
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-4 control-label">
                                    {!! Form::label('guardian_name', 'পিতা/স্বামীর নাম') !!}
                                </div>
                                <div class="col-sm-7">
                                    {!! Form::text('guardian_name', Input::old('guardian_name'), array('class'=>'form-control', 'placeholder'=>'')) !!}
                                </div>
                            </div>
    
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <h3 class="apply_deading">ঠিকানা</h3>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <div class="col-md-4 col-sm-4 col-xs-12">
                                    {!! Form::label('district', 'জেলা') !!}
                                    {!! Form::select('district', ['11'=>'নারায়াণগঞ্জ'], null, array('id'=>'district', 'class'=>'form-control', 'placeholder' => 'সিলেক্ট করুন')) !!}
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-12">
                                    {!! Form::label('upazila', 'উপজেলা') !!}
                                    {!! Form::select('upazila', 
                                    array(
                                    '220'=>'আড়াইহাজার', 
                                    '221'=>'সোনারগাঁও', 
                                    '222'=>'বান্দার', 
                                    '223'=>'নারায়ানগঞ্জ সদর', 
                                    '224'=>'রূপগঞ্জ', 
                                    '225'=>'সিদ্ধিরগঞ্জ'
                                    ), 
                                    null, 
                                    array('id'=>'upazila', 'class'=>'form-control', 'placeholder' => 'সিলেক্ট করুন')) 
                                    !!}
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-12">
                                    {!! Form::label('post_office', 'উপজেলা') !!}
                                    {!! Form::select('post_office', 
                                    array(
                                    '2453'=>'আলীরটেক',
                                    '2454'=>'বক্তাবলী',
                                    '2455'=>'এনায়েতনগর',
                                    '2456'=>'ফতুল্লা',
                                    '2457'=>'গোদনাইল',
                                    '2458'=>'গোগনগর',
                                    '2459'=>'কাশিপুর',
                                    '2460'=>'কুতুবপুর',
                                    '2461'=>'সিদ্ধিরগঞ্জ',
                                    '2462'=>'সুমিলপাড়া',
                                    ), 
                                    null, 
                                    array('id'=>'post_office', 'class'=>'form-control', 'placeholder' => 'সিলেক্ট করুন')) 
                                    !!}
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-4 col-sm-4 col-xs-12">
                                    {!! Form::label('village_or_road', 'গ্রাম/রাস্তার নাম') !!}
                                    {!! Form::text('village_or_road', Input::old('village_or_road'), array('class'=>'form-control', 'placeholder'=>'')) !!}
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-12">
                                    {!! Form::label('holding_no', 'হোল্ডিং নং') !!}
                                    {!! Form::text('holding_no', Input::old('holding_no'), array('class'=>'form-control', 'placeholder'=>'')) !!}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <h3 class="apply_deading">প্রস্তাবিত দর</h3>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <div class="col-md-4 col-sm-4 col-xs-12">
                                    {!! Form::label('proposed_rate', 'অংকে') !!}
                                    {!! Form::text('proposed_rate', Input::old('proposed_rate'), array('class'=>'form-control', 'placeholder'=>'')) !!}
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-12">
                                    {!! Form::label('proposed_text', 'কথায়') !!}
                                    {!! Form::text('proposed_text', Input::old('proposed_text'), array('class'=>'form-control', 'placeholder'=>'')) !!}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <h3 class="apply_deading">জামানতের টাকার পরিমান</h3>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <div class="col-md-4 col-sm-4 col-xs-12">
                                    {!! Form::label('pledge_rate', 'অংকে') !!}
                                    {!! Form::text('pledge_rate', Input::old('pledge_rate'), array('class'=>'form-control', 'placeholder'=>'')) !!}
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-12">
                                    {!! Form::label('pledge_text', 'কথায়') !!}
                                    {!! Form::text('pledge_text', Input::old('pledge_text'), array('class'=>'form-control', 'placeholder'=>'')) !!}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <h3 class="apply_deading">ব্যাংকের তথ্য</h3>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <div class="col-md-4 col-sm-4 col-xs-12">
                                    {!! Form::label('bank_name', 'ব্যাংকের নাম') !!}
                                    {!! Form::text('bank_name', Input::old('bank_name'), array('class'=>'form-control', 'placeholder'=>'')) !!}
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-12">
                                    {!! Form::label('bank_draft', 'ব্যাংকের ড্রাফ্‌ট নং') !!}
                                    {!! Form::text('bank_draft', Input::old('bank_draft'), array('class'=>'form-control', 'placeholder'=>'')) !!}
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-12">
                                    {!! Form::label('bank_date', 'তারিখ') !!}
                                    {!! Form::text('bank_date', Input::old('bank_date'), array('class'=>'form-control', 'placeholder'=>'')) !!}
                                </div>
                            </div>
                        </div>
    
                    </div>
                    <div class="row">
    
                        <div class="col-md-12 col-sm-12 col-xs-12 text-center">
                            <h3 class="commitment"><span>অঙ্গীকার নামা</span></h3>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12 commitment_text">
                            <p>আমি <span class="value_text name"></span> পিতা/স্বামী <span class="value_text guardian_name"></span> হোল্ডিং নং <span class="value_text holding_no"></span> গ্রাম/রাস্তার নাম <span class="value_text village_or_road"></span> পোস্টঃ <span class="value_text post"></span> উপজেলা <span class="value_text upazila"></span> জেলা <span class="value_text district"></span> এই মর্মে অঙ্গীকার করেতেছি যে, সিটি করপোরেশন কর্তৃক ১৪২৩ বাংলা সনের ৩০ চৈত্র পর্যন্ত সময়ের জন্য সিটি করপোরেশন নিয়ন্ত্রণাধীন হাট বাজার/সায়রাত মহাল অস্থায়ী ইজারা প্রদানের লক্ষে জারীকৃত বিজ্ঞপ্তির সমুদয় শর্ত, টোল রেইট অবগত হয়ে এবং উহা মেনে নেওয়ার শর্তে <span class="value_text mohal_name"></span> (মহালের নাম) ইজারা গ্রহনের জন্য দরপত্র দাখিল করিলাম। ইজারা বিজ্ঞপ্তিতে বর্ণিত শর্ত ভংগের কারনে সিটি করপোরেশন কর্তৃপক্ষ আমার দাখিলিয় দরপত্র বা প্রাপ্ত ইজারা বাতিল করিলে আমার কোন আপত্তি গ্রহণযোগ্য হবেনা।</p>
                        </div>
    
                    </div>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-12">
    
                </div>
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="form-group" style="margin-top: 25px;">
                        <div class="col-md-8">
                            <button type="button" class="btn btn-warning btn-sm" id="reset_form">বার্তা মুছে ফেলুন</button>
                            <button type="submit" class="btn btn-success btn-sm">দাখিল করুন</button>
                        </div>
                    </div>
                </div>
    
                {!! Form::close() !!}
            </div>
        </div>-->

    @endsection
