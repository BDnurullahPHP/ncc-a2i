@extends('portal.layout.base')

@section('style')

	<style type="text/css">
		.control-label{
	        text-align: left !important;
	    }
	    .form-control{
	        height: 34px !important;
	        border-radius: 0px;
	    }
	    .input-group-addon{
	        border-radius: 0px;
	    }
	    textarea#address{
	        width: 100%;
	        max-width: 100%;
	        height: auto !important;
	    }
	    .btn{
	        border-radius: 0px;
	    }
	    #reset_form{
	        margin-right: 10px;
	    }
        .body_contents{
            background-color: #FFFFFF;
        }
        .table-bordered > thead > tr > th{
            text-align: center;
        }
        .table > tbody > tr > td{
            text-align: center;
        }
        .table > tbody > tr > td:nth-child(2), 
        .table > tbody > tr > td:nth-child(3){
            text-align: left;
        }
	</style>

@endsection

@section('js_file')

    <script src="{{ Helpers::asset('assets/plugins/datatables/datatables.min.js') }}" type="text/javascript"></script>
    <script src="{{ Helpers::asset('assets/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') }}" type="text/javascript"></script>
    <script src="{{ Helpers::asset('assets/portal/js/tenderapplicant.js') }}" type="text/javascript"></script>

@endsection

@section('script')

    <script type="text/javascript">
        $(document).ready(function() {
            TenderApplicant.init();
            @if (Session::has('app_exist'))
                sweetAlert("Oops...", "{{ Session::get('app_exist') }}", "error");
            @endif
        });
    </script>

@endsection

@section('content')

    <div class="col-md-12 col-sm-12 col-xs-12">
        <h3 class="text-center">
        {{trans('portal.Narayanganj_City_Corporation_Regulation_Hat_bazar/_Syarat_Mahal')}} {{ Helpers::numberConvert(date('Y')) }}
        {{trans('portal.year_lease_notification')}} </h3>
        <table class="table table-bordered display" data-source="{{ route('lease_tenderschedule_data') }}">
            <thead>
                <tr>
                    <th>{{trans('portal.sl_no')}}</th>
                    <th>{{trans('portal.Point_no')}}</th>
                    <th>{{trans('portal.Name_of_the_syarat_mahal')}}</th>
                    <th>{{trans('portal.Mahal_location')}}</th>
                    <th>{{trans('portal.The_amount_of_winding_(Suggested_value)_(Percent)')}}</th>
                    <th>{{trans('portal.Tender_price_(Non-refundable)_(Money)')}}</th>
                    <th>{{trans('portal.Details')}}</th>
                    <th>{{trans('portal.Activity')}}</th>
                </tr>
            </thead>
            <tbody>
                
            </tbody>
        </table>
    </div>     

@endsection