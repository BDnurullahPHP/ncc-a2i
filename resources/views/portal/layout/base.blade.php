<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="Narayanganj City Corporation"/>
  <meta name="author" content="ATI Limited"/>
  <link rel="shortcut icon" href="{{ Helpers::asset('assets/img/logo/icon_ncc.png') }}">

  <title>{{ isset($title) ? $title : 'Narayanganj City Corporation' }}</title>

  <!-- CSS Bootstrap & Custom -->
  <link href="{{ Helpers::asset('assets/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
  <link href="{{ Helpers::asset('assets/css/portal.css') }}" rel="stylesheet">
  <link href="{{ Helpers::asset('assets/css/megamenu/yamm.css') }}" rel="stylesheet">
  <link href="{{ Helpers::asset('assets/css/megamenu/demo.css') }}" rel="stylesheet">
  <link href="{{ Helpers::asset('assets/plugins/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" media="screen"/>
  <link href="{{ Helpers::asset('assets/plugins/jquery-ui-1.12.0/jquery-ui.min.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ Helpers::asset('assets/css/animate.css') }}" rel="stylesheet" media="screen"/>
  <link href="{{ Helpers::asset('assets/plugins/iCheck/square/green.css') }}" rel="stylesheet" media="screen"/>
  <link href="{{ Helpers::asset('assets/plugins/sweetalert-master/dist/sweetalert.css') }}" rel="stylesheet" type="text/css" />
  {{-- <link href="{{ Helpers::asset('assets/dist/css/apms.min.css') }}" rel="stylesheet"> --}}

  <!-- JavaScripts -->
  <script src="{{ Helpers::asset('assets/js/jquery-2.1.1.js') }}"></script>

  <!--====Section for individual page css library===-->
  @yield('css_file')

  <link href="{{ Helpers::asset('assets/portal/style.css') }}" rel="stylesheet" media="screen"/>

  <!--====Section for in page css style===-->
  @yield('style')

</head>

<body>
  <div class="container">
   <div class="row">
       <header class="site-header header_top_style">
           <div class="container">
               <div class="row">
                   <div class="col-md-2 col-sm-3 col-xs-6 logo mobile_header_left">
                       <a href="{{ route('home') }}" title="Home" rel="home">
                           <img width="70" src="{{ Helpers::asset('assets/img/logo/logo_ncc.png') }}" alt="logo"/>
                       </a>
                   </div>
                   <div class="col-md-8 col-sm-6 col-xs-12 text-center header_title_text">
                    <h4>
    {{trans('portal.online_flat')}}
                </h4>
                      <h2>নারায়নগঞ্জ সিটি কর্পোরেশন</h2>
                      <h3>NARAYANGANJ CITY CORPORATION</h3>
                  </div>
                  <div class="col-md-2 col-sm-3 col-xs-6 logo mobile_header_right">
                   <img width="70" src="{{ Helpers::asset('assets/portal/images/a2ilogo-final.png') }}" alt="logo"/>
               </div>
           </div>
       </div>
   </header>
   <!--====Include header===-->
   @include('portal.layout.header')

</div>
</div>

<div class="container body_contents">
   <div class="row">
      <!--======Page main content======-->
      @yield('content')

  </div>
</div>

<footer class="site-footer">
   <!--====Include footer===-->
   @include('portal.layout.footer')

</footer>

<!--=======Began Page common modal==========-->
<div class="modal fade" id="modal_add_content" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">{{ trans('common.add_data') }}</h4>
            </div>
            <div class="modal-body">
                <img src="{{ Helpers::asset('assets/img/loader.gif') }}" alt="" class="loader center-block">
                <div id="body-content">

                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">{{ trans('common.modal_close') }}</button>
            </div>
        </div>
    </div>
</div>
<!--=======End Page common modal==========-->

<!--=======Began Page big modal==========-->
<div class="modal fade" id="modal_big_content" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">{{ trans('common.add_data') }}</h4>
            </div>
            <div class="modal-body">
                <img src="{{ Helpers::asset('assets/img/loader.gif') }}" alt="" class="loader center-block">
                <div id="body-content">

                </div>
            </div>
            <div class="modal-footer">
                <!-- <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button> -->
            </div>
        </div>
    </div>
</div>
<!--=======End Page big modal==========-->

<script type="text/javascript">
   var baseUrl = "{{ Config::get('app.url') }}";
</script>
<script src="{{ Helpers::asset('assets/portal/bootstrap/js/bootstrap.min.js') }}" type="text/javascript"></script>
<script src="{{ Helpers::asset('assets/portal/js/plugins.js') }}" type="text/javascript"></script>
<script src="{{ Helpers::asset('assets/plugins/jquery-ui-1.12.0/jquery-ui.min.js') }}" type="text/javascript"></script>
<script src="{{ Helpers::asset('assets/plugins/jquery-validation/dist/jquery.validate.min.js') }}" type="text/javascript"></script>
<script src="{{ Helpers::asset('assets/plugins/jquery-validation/dist/additional-methods.min.js') }}" type="text/javascript"></script>
<script src="{{ Helpers::asset('assets/plugins/jquery-validation/dist/localization/messages_bn_BD.min.js') }}" type="text/javascript"></script>
<script src="{{ Helpers::asset('assets/plugins/iCheck/icheck.min.js') }}" type="text/javascript"></script>
<script src="{{ Helpers::asset('assets/plugins/sweetalert-master/dist/sweetalert.min.js') }}" type="text/javascript"></script>
<script src="{{ Helpers::asset('assets/portal/js/jquery.bootstrap.newsbox.min.js') }}" type="text/javascript"></script>
<script src="{{ Helpers::asset('assets/plugins/input-mask/jquery.inputmask.js') }}" type="text/javascript"></script>
<script src="{{ Helpers::asset('assets/plugins/input-mask/jquery.inputmask.date.extensions.js') }}" type="text/javascript"></script>
<script src="{{ Helpers::asset('assets/plugins/input-mask/jquery.inputmask.extensions.js') }}" type="text/javascript"></script>

<!--====Section for individual page javascript library===-->
@yield('js_file')
<script src="{{ Helpers::asset('assets/portal/js/custom.js') }}" type="text/javascript"></script>

<!--====Section for in page javascript===-->
@yield('script')

</body>
</html>