<div class="container">
    <div class="bottom-footer">
        <div class="row">
            <div class="col-md-12">
                <div class="col-md-8" style="color: white; text-align: left;">
                    {{trans('portal.copyright')}}:  &copy; {{ Helpers::numberConvert(date('Y')) }}</strong>
                    <a href="http://www.ncc.gov.bd/" target="_blank">
                       {{trans('portal.NARAYANGANJ_CITY_CORPORATION')}} 
                    </a> {{trans('portal.o')}}  
                    <a href="http://www.a2i.pmo.gov.bd/" target="_blank">
                       {{trans('portal.a2i')}} 
                    </a>
                </div>
                <div class="col-md-4 pull-right" style="color: white; text-align: right;">
                    {{trans('portal.design_and_development')}}
                    <a href="http://www.atilimited.net" target="_blank">
                        <img style="width: 30px;" src="{{ Helpers::asset('assets/img/logo/logo_ati_large.gif') }}" alt="NCC" class="logo-default" />
                        <span style="color: red;">ATI</span>
                        <span style="color: green;">Limited</span>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>