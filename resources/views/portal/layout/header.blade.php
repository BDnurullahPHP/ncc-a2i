<div class="navbar navbar-default yamm">
    <div class="navbar-header">
        <button type="button" data-toggle="collapse" data-target="#navbar-collapse-grid" class="navbar-toggle">
            <span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>
    </div>
    <div id="navbar-collapse-grid" class="navbar-collapse collapse">
        <ul class="nav navbar-nav">
            <li class="dropdown"><a href="{{ route('home') }}">
            {{trans('portal.home')}}    
            
        </a>
        </li>
            <li class="dropdown">
                <a href="{{ route('contact') }}"> {{trans('portal.contact')}}</a>
            </li>
          {{-- <li class="dropdown">
                <a href="{{ Helpers::asset('assets/portal/tender-notice-n.pdf') }}"> {{trans('portal.tender_notice')}}</a>
            </li> --}}

              <li class="dropdown"><a href="{{ route('get_tender') }}"> {{trans('portal.tender_notice')}}</a></li>
              <li class="dropdown"><a href="{{ route('get_lease') }}"> {{trans('portal.lease_notice')}}</a></li>
			  <li class="dropdown"><a href="{{ route('download_user_manual') }}" target="_blank">{{trans('portal.usage_guidelines')}}</a></li>
            @if( Auth::check() )
                <li class="feedback">
                    <a href="{{ route('public_feedback') }}"> {{trans('portal.views')}}</a>
                </li>
            @endif
        </ul>
        <ul class="nav navbar-nav navbar-right">
            <li class="dropdown language-switch">
                <a title="English" href="{{ route('language_switch', 'en') }}">
                    En
                </a>
            </li>
            <li class="dropdown language-switch">
                <a title="Bangla" href="{{ route('language_switch', 'bn') }}">
                    <img src="{{ Helpers::asset('assets/img/flg/bd.png') }}" class="position-left" alt="Ba">
                </a>
            </li>
            @if( Auth::check() )
                <li>
                    <a href="{{ route('admin_dashboard') }}" class="btn btn-default btn-sm">
                        {{ trans('common.mm_dashboard') }}
                    </a>
                </li>
                <li>
                    <a href="{{ route('get_logout') }}" class="btn btn-default btn-sm">
                        {{ trans('common.logout') }}
                    </a>
                </li>
            @else
                <li>
                    <a href="{{ route('user_login') }}" class="btn btn-default btn-sm">{{ trans('common.login') }}</a>
                </li>
            @endif
            {{-- <li>
                <div class="dropdown login_menu_btn">
                    <a href="{{ route('user_login') }}" class="btn btn-default btn-sm dropdown-toggle login_btn_drp">নিবন্ধন</a>
                    <div class="dropdown-menu login">
                        <div id="login">
                            <div class="login_form">
                                <h3>
                                    <span class="lh1"><strong>ব্যবহারকারী</strong></span> &nbsp;<span class="lh2">
                                    <strong>লগ ইন</strong></span>
                                </h3>
                                {!! Form::open(array('url' => '#', 'method' => 'post', 'id'=>'login-form')) !!}
                                    <div class="form-group">
                                        {!! Form::email('email', Input::old('email'), array('class'=>'form-control', 'required'=>'required', 'placeholder'=>'আপনার ইমেইল')) !!}
                                    </div>
                                    <div class="form-group">
                                        {!! Form::password('password', array('class'=>'form-control inpup_field', 'required'=>'required', 'placeholder'=>'আপনার পাসওয়ার্ড')) !!}
                                    </div>
                                    {!! Form::checkbox('', null, false) !!}<span> আমাকে সাইন ইন রাখুন</span><br>
                                    <input type="submit" class="submit_value" value="Log In"><br>
                                {!! Form::close() !!}
                                <a href="#" class="account_problem">আমি আমার অ্যাকাউন্ট অ্যাক্সেস করতে পারছি না</a>
                            </div>
                            <div class="hr">
                                <div class="hr1"></div>
                                <div class="or">অথবা</div>
                                <div class="hr2"></div>
                            </div>
                            <div class="new_account">
                                <a href="portal/get_register"><strong>নতুন অ্যাকাউন্ট তৈরি করুন</strong></a>
                            </div>
                        </div>
                    </div>
                </div>
            </li> --}}
        </ul>
    </div>
</div>