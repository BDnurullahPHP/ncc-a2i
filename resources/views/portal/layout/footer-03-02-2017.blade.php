<div class="container">
    <div class="bottom-footer">
        <div class="row">
            <div class="col-md-5">
                <strong>&copy; Copyright {{ date("Y") }}. NCC, Designed &amp; developed by
                    <a href="http://www.atilimited.net" target="_blank">
                        <img style="width: 30px;" src="{{ Helpers::asset('assets/img/logo/logo_ati_large.gif') }}" alt="NCC" class="logo-default" />
                        <span style="color: red; font-size: 15px;">ATI</span>
                        <span style="color: green; font-size: 15px;">Limited</span>
                    </a>
                </strong>
            </div>
            <div class="col-md-7">
                <ul class="footer-nav">
                    <li><a href="{{ route('home') }}">হোম</a></li>
                    <li><a href="{{ route('contact') }}">যোগাযোগ</a></li>
                    <li><a href="{{ route('get_register') }}">নিবন্ধন</a></li>
                    <li><a href="{{ route('apply_online') }}">অনলাইনে আবেদন</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>