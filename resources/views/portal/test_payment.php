<form id="payment_gw" name="payment_gw" method="POST"
      action="https://sandbox.sslcommerz.com/gwprocess/v3/process.php">
    <input type="hidden" name="total_amount" value="1150.00" />
    <input type="hidden" name="store_id" value="testbox" />
    <input type="hidden" name="tran_id" value="56f79047ac499" />
    <input type="hidden" name="success_url" value="http://ncc.atilimited.net/tenderapplicant_save_form" />
    <input type="hidden" name="fail_url" value="http://ncc.atilimited.net/tenderapplicant_fail_form" />
    <input type="hidden" name="cancel_url" value="http://ncc.atilimited.net/get_tender" />
    <input type="hidden" name="version" value="2.00" />
    <!-- OPTIONAL SHOPPING CART LIST !-->
    <!-- Customer Information !-->
    <input type="hidden" name="cus_name" value="ABC XYZ">
    <input type="hidden" name="cus_email" value="abc.xyz@mail.com">
    <input type="hidden" name="cus_add1" value="Address Line One">
    <input type="hidden" name="cus_add2" value="Address Line Two">
    <input type="hidden" name="cus_city" value="City Name">
    <input type="hidden" name="cus_state" value="State Name">
    <input type="hidden" name="cus_postcode" value="Post Code">
    <input type="hidden" name="cus_country" value="Country">
    <input type="hidden" name="cus_phone" value="01111111111">
    <input type="hidden" name="cus_fax" value="01711111111">
    <!-- Shipping Information !-->
    <input type="hidden" name="ship_name" value="ABC XYZ">
    <input type="hidden" name="ship_add1" value="Address Line One">
    <input type="hidden" name="ship_add2" value="Address Line Two">
    <input type="hidden" name="ship_city" value="City Name"><input type="hidden" name="ship_state" value="State Name">
    <input type="hidden" name="ship_postcode" value="Post Code">
    <input type="hidden" name="ship_country" value="Country">
    <!-- Optional Parameters which will be stored and returned at the end !-->
    <input type="hidden" name="value_a" value="ref001">
    <input type="hidden" name="value_b" value="ref002">
    <input type="hidden" name="value_c" value="ref003">
    <input type="hidden" name="value_d" value="ref004">
    <!-- PRODUCT 1 !-->
    <input type="hidden" name="cart[0][product]" value="FRESH HOME MADE BREAD 350GM" />
    <input type="hidden" name="cart[0][amount]" value="500.00" />
    <!-- PRODUCT 2 !-->
    <input type="hidden" name="cart[1][product]" value="FRESH HOME MADE BREAD 350GM Quantity(1)" />
    <input type="hidden" name="cart[1][amount]" value="600.00">
    <!-- PRODUCT 3 !-->
    <input type="hidden" name="cart[2][product]" value="SHIPMENT CHARGE" />
    <input type="hidden" name="cart[2][amount]" value="50.00" />
    <input type="hidden" name="card_name" value='mtbl'>
    <input type="hidden" name="show_all_gw" value="1" />
    <!-- SUBMIT REQUEST !-->
    <input type="submit" name="submit" value="Pay Now" />
</form>