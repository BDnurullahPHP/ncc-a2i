<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="description" content="Automated Property Management System for NCC">
        <meta name="author" content="ATI Limited"/>
        <title>NCC | Login</title>
        <!-- Global stylesheets -->
        <link href="{{ Helpers::asset('assets/login/css/icons/icomoon/styles.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ Helpers::asset('assets/login/css/bootstrap.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ Helpers::asset('assets/login/css/core.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ Helpers::asset('assets/login/css/components.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ Helpers::asset('assets/login/css/colors.css') }}" rel="stylesheet" type="text/css">
        <!-- /global stylesheets -->
        
        <link rel="shortcut icon" href="{{ Helpers::asset('assets/img/logo/icon_ncc.png') }}">
        <style type="text/css">
            .bg-green{
                background-color: #609513;
            }
        </style>
    </head>
    <body class="login-cover">
        <div class="page-container login-container">
            <div class="page-content">
                <div class="content-wrapper">
                    <!-- Form with validation -->
                    {!! Form::open(array('route' => ['change_app_password', $id, $type, $token], 'method' => 'post', 'id'=>'authentication_form')) !!}
                        <div class="panel panel-body login-form">
                            <div class="text-center">
                                <a href="{{ route('home') }}">
                                	<img style="width: 120px;" src="{{ Helpers::asset('assets/img/logo/logo_ncc.png') }}">
                                </a>
                                <h5 class="content-group">Recovery your password 
                                	<small class="display-block">Your credentials</small>
                                </h5>
                            </div>
                            <!--========Session message=======-->
							@if (Session::has('success'))
			                    <div class="alert alert-success">
			                      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			                      {{ Session::get('success') }}
			                    </div>
			                @endif
			                @if (Session::has('error'))
			                    <div class="alert alert-danger">
			                      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			                      {{ Session::get('error') }}
			                    </div>
			                @endif
			                @if (count($errors) > 0)
			                    @foreach ($errors->all() as $error)
			                      <div class="alert alert-danger">
			                          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			                          {{ $error }}
			                      </div>
			                    @endforeach
			                @endif
							<!--======end Session message=====-->
                            <div class="form-group has-feedback has-feedback-left">
                                <input type="password" class="form-control" placeholder="Password" name="password" required="required">
                                <div class="form-control-feedback">
                                    <i class="icon-lock2 text-muted"></i>
                                </div>
                            </div>
                            <div class="form-group has-feedback has-feedback-left">
                                <input type="password" class="form-control" placeholder="Retype password" name="password_confirmation" required="required">
                                <div class="form-control-feedback">
                                    <i class="icon-lock2 text-muted"></i>
                                </div>
                            </div>

                            <div class="form-group">
                                <button type="submit" class="btn bg-green btn-block">Reset <i class="icon-arrow-right14 position-right"></i></button>
                            </div>
                        </div>
                    {!! Form::close() !!}
                    <!-- /form with validation -->
                </div>
            </div>
            <div class="footer text-white">
                &copy; 2016. <a href="#" class="text-white">Automated Property Management System of NCC. </a> <strong>
                    <small> Developed By
                        <a href="http://www.atilimited.net" target="_blank">
                            <span style="color: red;">ATI</span>
                            <span style="color: green;">Limited</span>
                        </a>
                    </small>
                </strong>
            </div>
        </div>
		
		<script src="{{ Helpers::asset('assets/js/jquery-2.1.1.js') }}"></script>
		<script src="{{ Helpers::asset('assets/portal/bootstrap/js/bootstrap.min.js') }}" type="text/javascript"></script>

    </body>
</html>
