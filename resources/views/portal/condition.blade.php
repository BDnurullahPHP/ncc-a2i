@extends('portal.layout.base')

@section('style')

<style type="text/css">
    .contact-page-content{
        min-height: 370px;
    }
    ul{
        padding-left: 12px;
    }
</style>

@endsection

@section('script')

<script type="text/javascript">
    $(document).ready(function() {
        $('#condition_agree').click(function(event) {
            var url = $(this).data('url');
            if ($(this).is(':checked'))
                window.location.href = url;
        });
    });
</script>

@endsection

@section('content')

    <div class="contact-page-content">
        <div class="row">
            @if(!empty($conditions))
                <h3 align="center">{{ $conditions->getCategory->CATE_NAME }} {{trans('admin.Its_conditions')}}</h3>
                <div class="col-md-12 col-sm-12 col-xs-12">
                    {!! $conditions->CON_DESC !!}
                </div>
                <!-- <div class="col-md-12">
                    <div class="form-group">
                        <div class="col-md-6 pull-right" style="text-align: center">
                            <p>
                                ({{ $officer->NAME }})
                                <br/>({{ $officer->DESIGNATION }})
                                <br/>({{ $officer->OFFICE }})
                                <br/>({{ $officer->PHONE }})
                                <br/>({{ $officer->EMAIL }})
                            </p>
                        </div>
                    </div>
                </div>-->
<!--                <div class="col-md-12 col-sm-12 col-xs-12" style="margin-top: 100px;">
                    <p>স্মারক নং- {{ $tender->TENDER_NO }}</p>
                    <p>সদয় অবগতি/প্রয়োজনীয় ব্যবস্থা গ্রহণের জন্য অনুলিপি প্রেরণ করা হলোঃ</p>
                    <ul>
                        @foreach($onulipy_location as $key => $loc)
                            <li>{{ $loc->getLocation->LOCATION_NAME }}</li>
                        @endforeach
                    </ul>
                </div>-->
                <div class="clearfix"></div>
            @endif

            <div class="checkbox" style="padding:20px 0 20px 15px; text-align: center">
                <label  class="label label-success"style="font-size:16px;">
                    <input type="checkbox" id="condition_agree" data-url="{{ route('get_tender_application_form', [$t_type, $sc]) }}">{{trans('admin.I_agree')}} 
                </label>
          </div>

        </div>
    </div>

@endsection