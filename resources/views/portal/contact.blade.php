@extends('portal.layout.base')

@section('style')

	<style type="text/css">
		#gmap_canvas img {
            max-width: none !important;
            background: none !important
        }
        .map_overflow_content{
            text-align: center;
        }
         .map_overflow_content p{
            margin-bottom: 0px;
        }
        .contact-map{
        	margin-top: 0px;
        }
        ul.captcha_block{
        	padding-left: 0px;
        	float: left;
        }
        ul.captcha_block li{
        	list-style: none;
        	display: inline-block;
        	padding-right: 10px;
        }
        .form-control{
        	width: 100% !important;
        	max-width: 100% !important;
        	border-radius: 0px !important;
        }
        .contact-form textarea{
        	height: auto !important;
        	color: #333333;
        }
	</style>

@endsection

@section('js_file')
	
	<script src="{{ Helpers::asset('assets/portal/js/feedback.js') }}" type="text/javascript"></script>
	<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>

@endsection

@section('script')

	<script type="text/javascript">
		$(document).ready(function() {
	        Contact.init();
	    });

	    function init_map() {
            var myOptions = {
                zoom: 15,
                center: new google.maps.LatLng(23.622640, 90.499797),
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };
            map = new google.maps.Map(document.getElementById("gmap_canvas"), myOptions);
            marker = new google.maps.Marker({
                map: map,
                position: new google.maps.LatLng(23.622640, 90.499797)
            });
            infowindow = new google.maps.InfoWindow({content: '<div class="map_overflow_content"><p><b>NCC</b></p><p>নারায়ণগঞ্জ</p><p>সিটি করপোরেশন</p></div>'});
            google.maps.event.addListener(marker, "click", function () {
                infowindow.open(map, marker);
            });
            infowindow.open(map, marker);
        }
        google.maps.event.addDomListener(window, 'load', init_map);
	</script>

@endsection

@section('content')

	<div class="row">
		<div class="col-md-5">
		    <div class="contact-map">
		        <div class="google-map-canvas" id="map-canvas" style="">
		            <div style="height:auto;width:100%;">
		                <div id="gmap_canvas" style="height:459px;width:100%;"></div>
		            </div>
		        </div>
		    </div>
		</div>
		<div class="col-md-7">
		    <div class="contact-page-content">
		        <div class="contact-heading">
		            <h3>{{trans('portal.contact_information')}}</h3>
		            <!--========Session message=======-->
					@if (Session::has('success'))
	                    <div class="alert alert-success">
	                      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	                      {{ Session::get('success') }}
	                    </div>
	                @endif
	                @if (Session::has('error'))
	                    <div class="alert alert-danger">
	                      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	                      {{ Session::get('error') }}
	                    </div>
	                @endif
	                @if (count($errors) > 0)
	                    @foreach ($errors->all() as $error)
	                      <div class="alert alert-danger">
	                          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	                          {{ $error }}
	                      </div>
	                    @endforeach
	                @endif
					<!--======end Session message=====-->
		        </div>
		        <div class="contact-form clearfix">
		        	{!! Form::open(array('url' => route('submit_contact'), 'method' => 'post', 'id'=>'contact_form', 'class' => 'form-horizontal')) !!}
		                <div class="form-group">
		                    <div class="col-md-4">
		                    	{!! Form::label('name',trans('portal.name')) !!}
		                        <span class="small-text">{{trans('portal.enter_your_name_here')}}</span>
		                    </div>
		                    <div class="col-md-7">
		                    	{!! Form::text('name', Input::old('name'), array('class' => 'form-control', 'placeholder'=>trans('portal.your_name'))) !!}
		                    </div>
		                </div>

		                <div class="form-group">
		                    <div class="col-md-4">
		                    	{!! Form::label('mobile', trans('portal.mobile_number')) !!}
		                        <span class="small-text">{{trans('portal.enter_your_mobile_number_here')}}</span>
		                    </div>
		                    <div class="col-md-7">
		                    	{!! Form::text('mobile', Input::old('mobile'), array('class' => 'form-control', 'placeholder'=>trans('portal.your_mobile_number'))) !!}
		                    </div>
		                </div>

		                <div class="form-group">
		                    <div class="col-md-4">
		                    	{!! Form::label('email', trans('portal.email')) !!}
		                        <span class="small-text">{{trans('portal.Enter_your_email_here_write')}}</span>
		                    </div>
		                    <div class="col-md-7">
		                    	{!! Form::email('email', Input::old('email'), array('class' => 'form-control', 'placeholder'=>trans('portal.Enter_your_email_here'))) !!}
		                    </div>
		                </div>

		                <div class="form-group">
		                	<div class="col-md-4">
		                		{!! Form::label('message',trans('portal.message')) !!}
		                        <span class="small-text">
		                        {{trans('portal.Enter_your_message_here')}}</span>
		                	</div>
		                	<div class="col-md-7">
		                		{!! Form::textarea('message', null, array('rows' =>'3', 'class' => 'form-control', 'placeholder'=>trans('portal.your_message'))) !!}
		                	</div>
		                </div>
		                <div class="form-group">
							<div class="col-md-4">
	                        	{!! Form::label('captcha', trans('common.verify')) !!}
	                        </div>
	                        <div class="col-md-7">
	                        	<ul class="captcha_block">
	                                <li>{!! Html::image(Captcha::src(), 'Captcha image') !!}</li>
	                                <li>{!! Form::text('captcha') !!}</li>
	                            </ul>
	                            <div id="error_msg1"></div>
	                        </div>
	                    </div>

		                <div class="col-md-7 col-md-offset-4">
		                	<button type="submit" class="btn btn-success btn-sm" style="margin-top: 20px;border-radius: 0px;">{{trans('portal.send')}}</button>
		                </div>

		            {!! Form::close() !!}
		        </div>
		    </div>
		</div>
	</div>

@endsection