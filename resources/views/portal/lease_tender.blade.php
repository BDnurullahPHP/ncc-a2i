@extends('portal.layout.base')

@section('style')

	<style type="text/css">
		.contact-page-content{
			min-height: 370px;
		}
	</style>

@endsection

@section('js_file')

	<script src="{{ Helpers::asset('assets/portal/js/register.js') }}" type="text/javascript"></script>

@endsection

@section('script')

	<script type="text/javascript">
	    $(document).ready(function() {
	        Register.init();
	        // Select2 multiselect
            $(".multi_select").select2();
	    });
	</script>

@endsection

@section('content')
	<div class="row">
	    <div class="col-md-12 col-sm-12 col-xs-12">
	        <div class="contact-page-content">
 				<h3 align="center">দরপত্র ক্রয় ও দাখিলের সময়সূচী</h3>
				<table class="table table-bordered">
				    <thead>
						<tr>
							<th>দফা নং </th>
							<th>দরপত্র বিক্রয়ের শেষ তারিখ ও সময় </th>
							<th>দরপত্র জমাদানের শেষ তারিখ ও সময় </th>
							<th>দরপত্র খোলার তারিখ ও সময় </th>
						</tr>
				    </thead>
				    <tbody>
				    	@if(count($lease_clause) > 0)
					    	@foreach($lease_clause as $key => $lc)
					    		<tr>
					    			<td>{{ $lc->CLAUSE }}</td>
					    			<td>
					    				{{ date('d/m/Y', strtotime($lc->LAST_SELLING_DT_FROM)) }} হতে
					    				{{ date('d/m/Y', strtotime($lc->LAST_SELLING_DT_TO)) }} খ্রিঃ
					    				<br>
					    				{{ date('h:m A', strtotime($lc->LAST_SELLING_DT_FROM)) }} হতে
					    				{{ date('h:m A', strtotime($lc->LAST_SELLING_DT_TO)) }} পর্যন্ত
					    			</td>
					    			<td>
					    				{{ date('d/m/Y', strtotime($lc->TE_LAST_RECIEVE_DT)) }} খ্রিঃ
					    				{{ date('h:m A', strtotime($lc->TE_LAST_RECIEVE_DT)) }} 
					    			</td>
					    			<td>
					    				{{ date('d/m/Y', strtotime($lc->TE_OPENING_DT)) }} খ্রিঃ
					    				{{ date('h:m A', strtotime($lc->TE_OPENING_DT)) }} 
					    			</td>
					    		</tr>
					    	@endforeach
					    @endif
				    </tbody>
				</table>
			</div>
		</div>
	</div>
	        
@endsection