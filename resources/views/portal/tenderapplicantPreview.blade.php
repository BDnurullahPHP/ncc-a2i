@extends('portal.layout.base')

@section('css_file')

	<link href="{{ Helpers::asset('assets/plugins/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ Helpers::asset('assets/plugins/select2/css/select2-bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">

@endsection

@section('style')

	<style type="text/css">
		.control-label{
	        text-align: left !important;
	    }
	    .form-control{
	        height: 34px !important;
	        border-radius: 0px;
	    }
	    .input-group-addon{
	        border-radius: 0px;
	    }
	    textarea#address{
	        width: 100%;
	        max-width: 100%;
	        height: auto !important;
	    }
	    .btn{
	        border-radius: 0px;
	    }
	    #reset_form{
	        margin-right: 10px;
	    }
	    .well{
	        background-color:#D0FFFF;
	        color:#3C6FFF;
	        font-size:18px;
	        border-style: solid;
            border-width: 3px;
            border-color:#33CCFF;
	    }
	    .affix {
			top: 5px;
			bottom: 330px;
			position: fixed;
			height: 100%;
    		max-height: 310px;
			width: 100%;
			max-width: 350px;
			background-color: #FFFFFF;
	  	}
	</style>

@endsection

@section('js_file')

	<script src="{{ Helpers::asset('assets/portal/js/tenderapplicant.js') }}" type="text/javascript"></script>

@endsection

@section('script')

	<script type="text/javascript">
		$(document).ready(function() {
	        TenderApplicant.init();

	        // $('#submit_form').click(function(event) {
	        // 	swal({
		       //      title: "Are you sure?",
		       //      type: "warning",
		       //      showCancelButton: true,
		       //      confirmButtonColor: "#609513",
		       //      confirmButtonText: "Yes, submit it!",
		       //      cancelButtonText: "No, cancel",
		       //      closeOnConfirm: true,
		       //      closeOnCancel: true
		       //  }, function(isConfirm){
		       //      if (isConfirm) {
		       //      	$('#tenderapplicant_form').submit();
		       //      }
		       //  });

	        // });

	    });

	</script>

	<script type="text/javascript">
        function Redirect() {
           window.location="{{ route('get_tender_applicant_preview') }}";
        }
     	function goBack() {
			window.history.back();
		}
    </script>

@endsection

@section('content')

	<div class="row">
	    <div class="col-md-12 col-sm-12 col-xs-12">
	        <div class="contact-page-content">
	            <div class="contact-heading">
	                <!--========Session message=======-->
					@if (Session::has('success'))
	                    <div class="alert alert-success">
	                      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	                      {{ Session::get('success') }}
	                    </div>
	                @endif
	                @if (Session::has('error'))
	                    <div class="alert alert-danger">
	                      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	                      {{ Session::get('error') }}
	                    </div>
	                @endif
	                @if (count($errors) > 0)
	                    @foreach ($errors->all() as $error)
	                      <div class="alert alert-danger">
	                          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	                          {{ $error }}
	                      </div>
	                    @endforeach
	                @endif
					<!--======end Session message=====-->
	            </div>
               
				<div class="well">
					<i class="fa fa-info-circle" aria-hidden="true"></i> আপনার তথ্যগুলো সঠিক কিনা পুনরাই যাচাই করুন <br>
					<i class="fa fa-info-circle" aria-hidden="true"></i> পরবর্তীতে আপনি আপনার ইমেইল
					@if( isset($user_details['APPLICANT_EMAIL']) && !empty($user_details['APPLICANT_EMAIL']) )
				        &#40;{{ $user_details['APPLICANT_EMAIL'] }}&#41;
		            @endif
		             অথবা মোবাইল নাম্বার
		            @if( isset($user_details['APPLICANT_PHONE']) && !empty($user_details['APPLICANT_PHONE']) )
				        &#40;{{ $user_details['APPLICANT_PHONE'] }}&#41;
		            @endif
					 এবং আপনার দেয়া পাসওয়ার্ড দিয়ে লগইন করতে পারবেন
				</div>

	            <div class="contact-form clearfix">
	            	<div class="row">

	            		<div class="col-md-8 col-sm-10 col-xs-12">
	            			<button type="button" class="btn btn-success"  onclick="goBack()">
	            				<i class="fa fa-angle-double-left" aria-hidden="true"></i> আবেদন ফর্ম সংশোধন
	            			</button>
	            		   	<h4 style="font-weight:bold;">আবেদনকারীর তথ্য</h4>
	            		    <hr>
			            	{!! Form::open(array('route' => ['tenderapplicant_save_form', $user_details['TYPE']], 'method' => 'post', 'id'=>'tenderapplicant_form', 'class' =>'form-horizontal')) !!}
								
								{!! Form::hidden('TOTAL_AMOUNT', $user_details['TOTAL_AMOUNT']) !!}
			                    <div class="form-group">
			                        <div class="col-md-4 control-label">
			                        	{!! Form::label('FIRST_NAME', trans('tender.tender_applicant_name')) !!} 
			                           
			                        </div>
			                        <div class="col-sm-7">
										<label>: 
											@if( isset($user_details['FIRST_NAME']) && !empty($user_details['FIRST_NAME']) )
									        	{{ $user_details['FIRST_NAME'] }}
		            		                @endif
		            		            </label>
			                        </div>
			                    </div>

			                    <div class="form-group">
			                        <div class="col-md-4 control-label">
			                        	{!! Form::label('LAST_NAME', trans('tender.tender_applicant_name_last')) !!} 
			                           
			                        </div>
			                        <div class="col-sm-7">
										<label>: 
											@if( isset($user_details['LAST_NAME']) && !empty($user_details['LAST_NAME']) )
									        	{{ $user_details['LAST_NAME'] }}
		            		                @endif
		            		            </label>
			                        </div>
			                    </div>

			                    <div class="form-group">
			                        <div class="col-md-4 control-label">
			                        	{!! Form::label('SPOUSE_NAME', trans('tender.tender_applicant_name_fh')) !!} 
			                        </div>
			                        <div class="col-sm-7">
			                        	<label>: 
			                        		@if( isset($user_details['SPOUSE_NAME']) && !empty($user_details['SPOUSE_NAME']) )
										        {{ $user_details['SPOUSE_NAME'] }}
			            		            @endif
			            		        </label>
			                        </div>
			                    </div>

			                    <div class="form-group">
			                        <div class="col-md-4 control-label">
			                        	{!! Form::label('DIVISION_ENAME', 'বিভাগ:') !!} 
			                        </div>
			                        <div class="col-sm-3">
                                        <label>: 
                                         	@if( isset($user_details['DIVISION_ENAME']) && !empty($user_details['division']) )
										        {{ $user_details['DIVISION_ENAME'] }}
			            		            @endif
			            		        </label>
                                     </div>
			                    </div>

			                    <div class="form-group">
			                        <div class="col-md-4 control-label">
			                        	{!! Form::label('DISTRICT_ENAME', 'জেলা:') !!} 
			                        </div>
			                        <div class="col-sm-3">
                                       <label>: 
                                       		@if( isset($user_details['DISTRICT_ENAME']) && !empty($user_details['DISTRICT_ENAME']) )
										        {{ $user_details['DISTRICT_ENAME'] }}
			            		            @endif
			            		        </label>
			                         </div>
			                    </div>

			                    <div class="form-group">
			                        <div class="col-md-4 control-label">
			                        	{!! Form::label('THANA_ENAME', 'উপজেলা/থানা:') !!} 
			                        </div>
			                        <div class="col-sm-3">
									   	<label>: 
									   		@if( isset($user_details['THANA_ENAME']) && !empty($user_details['THANA_ENAME']) )
									        	{{ $user_details['THANA_ENAME'] }}
		            		                @endif
		            		            </label>
			                        </div>
			                    </div>

			                    <div class="form-group">
			                        <div class="col-md-4 control-label">
			                        	{!! Form::label('ROAD_NO', 'রোড নং:') !!}  
			                        </div>
			                        <div class="col-sm-5">
			                        	<label>: 
			                        	  	@if( isset($user_details['ROAD_NO']) && !empty($user_details['ROAD_NO']) )
										        {{ $user_details['ROAD_NO'] }}
			            		            @endif
			            		        </label>
			                        </div>
			                    </div>

                                <div class="form-group">
			                        <div class="col-md-4 control-label">
			                        	{!! Form::label('HOLDING_NO', 'হোল্ডিং নং:') !!} 
			                        </div>
			                        <div class="col-sm-5">
			                        	<label>: 
			                        		@if( isset($user_details['HOLDING_NO']) && !empty($user_details['HOLDING_NO']) )
										        {{ $user_details['HOLDING_NO'] }}
			            		            @endif
			            		        </label>
			                        </div>
			                    </div>

			                    <div class="form-group">
			                        <div class="col-md-4 control-label">
			                        	{!! Form::label('APPLICANT_PHONE', 'মোবাইল নাম্বার:') !!} 
			                        </div>
			                        <div class="col-sm-4">
			                        	<label>: 
			                        		@if( isset($user_details['APPLICANT_PHONE']) && !empty($user_details['APPLICANT_PHONE']) )
										        {{ $user_details['APPLICANT_PHONE'] }}
			            		            @endif</label>
			                        </div>
			                    </div>

			                    <div class="form-group">
			                        <div class="col-md-4 control-label">
			                        	{!! Form::label('NID', 'জন্মনিবন্ধন / জাতীয় পরিচয়পত্র:') !!} 
			                        </div>
			                        <div class="col-sm-4">
			                        	<label>: 
			                        		@if( isset($user_details['NID']) && !empty($user_details['NID']) )
										        {{ $user_details['NID'] }}
			            		            @endif
			            		        </label>
			                        </div>
			                    </div>

			                    <div class="form-group">
			                        <div class="col-md-4 control-label">
			                        	{!! Form::label('APPLICANT_EMAIL', 'ইমেইল:') !!}
			                        </div>
			                        <div class="col-sm-4">
			                        	<label>: 
			                        		@if( isset($user_details['APPLICANT_EMAIL']) && !empty($user_details['APPLICANT_EMAIL']) )
										        {{ $user_details['APPLICANT_EMAIL'] }}
			            		            @endif
			            		        </label>
			                        </div>
			                    </div>
			                 
			                    <h4 style="font-weight:bold;"> @if(Request::segment(1)=='lease') ইজারার @else দরপত্রের @endif বিবরণ</h4> <hr>

			                    <div class="form-group">
			                        <div class="col-md-4 control-label">
			                        	{!! Form::label('BID_AMOUNT', 'দরদাতা কতৃক প্রদত্ত দর  ') !!} 
			                        </div>
			                        <div class="col-sm-5">
			                           {!! Form::label('BID_AMOUNT', '(অংক)  ') !!} 
			                        	<label>: 
			                        		@if( isset($user_details['BID_AMOUNT']) && !empty($user_details['BID_AMOUNT']) )
										        {{ $user_details['BID_AMOUNT'] }}
			            		            @endif
			            		        </label><br>
			                        	{!! Form::label('BID_AMOUNT_TEXT', '(কথায়) ') !!} 
			                        	<label>: 
			                        		@if( isset($user_details['BID_AMOUNT_TEXT']) && !empty($user_details['BID_AMOUNT_TEXT']) )
										        {{ $user_details['BID_AMOUNT_TEXT'] }}
			            		            @endif
			            		        </label>
			                        </div>
			                    </div>

			                    <div class="form-group">
			                        <div class="col-md-4 control-label">
			                        	{!! Form::label('BG_AMOUNT', 'জামানতের পরিমান') !!}
			                        </div>
			                        <div class="col-sm-5">
			                            {!! Form::label('BG_AMOUNT', '(অংকে)  ') !!} 
			                           	<label>: 
			                           		@if( isset($user_details['BG_AMOUNT']) && !empty($user_details['BG_AMOUNT']) )
										        {{ $user_details['BG_AMOUNT'] }}
			            		            @endif
			            		        </label><br>
			                        	{!! Form::label('BG_AMOUNT_TEXT', '(কথায়)  ') !!}
			                        	<label>: 
			                        		@if( isset($user_details['BG_AMOUNT_TEXT']) && !empty($user_details['BG_AMOUNT_TEXT']) )
										        {{ $user_details['BG_AMOUNT_TEXT'] }}
			            		            @endif
			            		        </label>
			                        </div>
			                    </div>

			                    <div class="form-group" style="margin-top: 25px;">
			                        <div class="col-md-8 col-md-offset-4">
										<button type="submit" class="btn btn-success btn-sm">নিশ্চিত করুণ</button>
			                        </div>
			                    </div>

			                {!! Form::close() !!}
			            </div>
						
						{!! $app_info !!}

					</div>
	            </div>
	        </div>
	    </div>
	</div>

@endsection

