@extends('portal.layout.base')

@section('style')

	<style type="text/css">
		.application_form{
			background-color: #FFFFFF;
			padding-top: 20px;
			padding-bottom: 20px;
		}
		.payment_border{
			border: 1px solid #D8D8D8;
			padding-top: 10px;
			padding-bottom: 10px;
		}
		.form-control, .btn{
			border-radius: 0px;
		}
		ul.payment_method_list{
			padding-left: 0px;
		}
		ul.payment_method_list li{
			list-style: none;
			display: inline-block;
			margin-top: 20px;
			margin-right: 20px;
			margin-bottom: 20px;
		}
	</style>

@endsection

@section('script')

	

@endsection

@section('content')

	<div class="col-md-12 col-sm-12 col-xs-12 application_form">
	    <div class="row">
	        <div class="col-md-9">
	            <div class="contact-heading">
	                <!-- <h3></h3> -->
	                <!--========Session message=======-->
					@if (Session::has('success'))
	                    <div class="alert alert-success">
	                      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	                      {{ Session::get('success') }}
	                    </div>
	                @endif
	                @if (Session::has('error'))
	                    <div class="alert alert-danger">
	                      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	                      {{ Session::get('error') }}
	                    </div>
	                @endif
	                @if (count($errors) > 0)
	                    @foreach ($errors->all() as $error)
	                      <div class="alert alert-danger">
	                          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	                          {{ $error }}
	                      </div>
	                    @endforeach
	                @endif
					<!--======end Session message=====-->
	            </div>
	        </div>
	    </div>
	    <div class="row">
	    	{!! Form::open(array('url' => '#', 'method' => 'post', 'id'=>'application_form', 'class' =>'form-horizontal')) !!}

	            <div class="col-md-12 col-sm-12 col-xs-12">
	                <div class="row">
	                    <div class="col-md-8 col-md-offset-2 col-sm-12 col-xs-12">
	                        <div class="form-group">
	                            <div class="col-md-4 col-sm-4 col-xs-12">
	                            	{!! Form::label('payment_type', 'টাকা প্রদানের ধরন') !!}
	                            	{!! Form::select('payment_type', 
	                            				array(
	                            					'1'=>'শিডিউল পেমেন্ট', 
	                            					'2'=>'জামানতের টাকা'
	                            				), 
	                            				null, 
	                            				array('id'=>'payment_type', 'class'=>'form-control')) 
	                            	!!}
	                            </div>
	                            <div class="col-md-7 col-sm-7 col-xs-12 payment_border">
									<div class="row">
										<div class="col-md-7 col-sm-7 col-xs-12">
											<label>মোট</label>
										</div>
										<div class="col-md-5 col-sm-5 col-xs-12">
											২০০০ টাকা
										</div>
									</div>
									<div class="row">
										<div class="col-md-7 col-sm-7 col-xs-12">
											<label>জামানত</label>
										</div>
										<div class="col-md-5 col-sm-5 col-xs-12">
											৫০০ টাকা
										</div>
									</div>
									<div class="row">
										<div class="col-md-12 col-sm-12 col-xs-12">
											<button type="button" class="btn btn-default btn-sm pull-right">পরিশোধ করুন</button>
										</div>
									</div>
	                            </div>
	                        </div>
	                        <div class="row">
	                        	<div class="col-md-12 col-sm-12 col-xs-12">
	                        		<ul class="payment_method_list">
	                        			<li><label>{!! Form::radio('payment_method', 1, true) !!} B-Kash</label></li>
	                        			<li><label>{!! Form::radio('payment_method', 2, false) !!} Bank</label></li>
	                        		</ul>
	                        	</div>
	                        </div>
	                        <div class="row">
	                        	<div class="col-md-12 col-sm-12 col-xs-12">
	                        		<div class="form-group">
	                        			<div class="col-md-6">
		                        			{!! Form::label('sender_no', 'প্রেরক') !!}
	                						{!! Form::text('sender_no', null, array('id'=>'sender_no', 'class'=>'form-control form-required', 'placeholder'=>'প্রেরকের মোবাইল নম্বর')) !!}
	                					</div>
	                        		</div>
	                        		<div class="form-group">
	                        			<div class="col-md-6">
		                        			{!! Form::label('receiver_no', 'প্রাপক') !!}
	                						{!! Form::text('receiver_no', null, array('id'=>'receiver_no', 'class'=>'form-control form-required', 'placeholder'=>'প্রাপকের মোবাইল নম্বর')) !!}
	                					</div>
	                        		</div>
	                        		<div class="form-group">
	                        			<div class="col-md-6">
		                        			{!! Form::label('date', 'তারিখ') !!}
	                						{!! Form::text('date', null, array('id'=>'date', 'class'=>'form-control form-required', 'placeholder'=>'প্রেরনের তারিখ')) !!}
	                					</div>
	                        		</div>
	                        		<div class="form-group">
								        <div class="col-md-6 col-sm-6 col-xs-12">
								            <button type="submit" data-action="{{ route('land_create') }}" id="land_submit" class="btn btn-success btn-sm pull-right" style="margin-left: 10px">{{ trans('common.land_form_save') }}</button>
								            <button type="button" class="btn yellow btn-sm pull-right reset_form">{{ trans('common.land_form_clear') }}</button>
								        </div>
								    </div>
	                        		 
	                        	</div>
	                        </div>
	                    </div>

	                </div>
	            </div>

	        {!! Form::close() !!}
	    </div>
	</div>

@endsection