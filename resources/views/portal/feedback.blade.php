@extends('portal.layout.base')

@section('style')

	<style type="text/css">
		.control-label{
	        text-align: left !important;
	    }
	    .form-control{
	        height: 34px !important;
	        border-radius: 0px;
	    }
	    .input-group-addon{
	        border-radius: 0px;
	    }
	    textarea#address, textarea#message{
	        width: 100%;
	        max-width: 100%;
	        height: auto !important;
	    }
	    .btn{
	        border-radius: 0px;
	    }
	    #reset_form{
	        margin-right: 10px;
	    }
	    ul.captcha_block{
			padding-left: 0px;
			margin-bottom: 0px;
	    }
	    ul.captcha_block li{
	    	list-style: none;
	    	margin-right: 15px;
	    	float: left;
	    }
	    ul.captcha_block li input{
			height: 34px;
	    }
	    .contact-form textarea{
	    	color: #333333;
	    }
	</style>

@endsection

@section('js_file')

	<script src="{{ Helpers::asset('assets/portal/js/feedback.js') }}" type="text/javascript"></script>

@endsection

@section('script')

	<script type="text/javascript">
	    $(document).ready(function() {
	        Feedback.init();
	    });
	</script>

@endsection

@section('content')

	<div class="row">
	    <div class="col-md-12 col-sm-12 col-xs-12">
	        <div class="contact-page-content">
	            <div class="contact-heading">
	                <!-- <h3>আপনার মতামত</h3> -->
	            </div>
	            <div class="contact-form clearfix">
	            	<div class="row">
	            		<div class="col-md-8 col-md-offset-3 col-sm-10 col-sm-offset-1 col-xs-12">

							<!--========Session message=======-->
							@if (Session::has('success'))
			                    <div class="alert alert-success">
			                      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			                      {{ Session::get('success') }}
			                    </div>
			                @endif
			                @if (Session::has('error'))
			                    <div class="alert alert-danger">
			                      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			                      {{ Session::get('error') }}
			                    </div>
			                @endif
			                @if (count($errors) > 0)
			                    @foreach ($errors->all() as $error)
			                      <div class="alert alert-danger">
			                          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			                          {{ $error }}
			                      </div>
			                    @endforeach
			                @endif
							<!--======end Session message=====-->

			            	{!! Form::open(array('route' => 'post_feedback', 'method' => 'post', 'id'=>'feedback_form', 'class' =>'form-horizontal')) !!}
								
			                    <div class="form-group">
			                        <div class="col-md-2 control-label">
			                        	{!! Form::label('query', trans('common.form_query')) !!}
			                        </div>
			                        <div class="col-sm-7">
			                        	{!! Form::select('query', ['complain'=>trans('common.complain'), 'feedback'=>trans('common.feedback'), 'suggestion'=>trans('common.suggestion')], array(Input::old('query')), array('class'=>'form-control', 'placeholder' => trans('common.form_select'))) !!}
			                        </div>
			                    </div>
			                    <div class="form-group">
			                        <div class="col-md-2 control-label">
			                        	{!! Form::label('name', trans('common.table_head_name')) !!}
			                        </div>
			                        <div class="col-sm-7">
			                        	{!! Form::text('name', Input::old('name'), array('class'=>'form-control', 'placeholder'=>trans('user.name_pl'))) !!}
			                        </div>
			                    </div>
			                    <div class="form-group">
			                        <div class="col-md-2 control-label">
			                        	{!! Form::label('email', trans('user.email')) !!}
			                        </div>
			                        <div class="col-sm-7">
			                        	{!! Form::email('email', Input::old('email'), array('class'=>'form-control', 'placeholder'=>trans('user.email_pl'))) !!}
			                        </div>
			                    </div>
			                    <div class="form-group">
			                        <div class="col-md-2 control-label">
			                        	{!! Form::label('mobile', trans('user.mobile')) !!}
			                        </div>
			                        <div class="col-sm-7">
			                        	{!! Form::text('mobile', Input::old('mobile'), array('class'=>'form-control', 'placeholder'=>trans('user.mobile_pl'))) !!}
			                        </div>
			                    </div>
			                    <div class="form-group">
			                        <div class="col-md-2 control-label">
			                        	{!! Form::label('address', trans('user.address')) !!}
			                        </div>
			                        <div class="col-sm-7">
			                        	{!! Form::textarea('address', null, array('rows' =>'1', 'class'=>'form-control', 'placeholder'=>trans('user.address_pl'))) !!}
			                        </div>
			                    </div>
			                    <div class="form-group">
			                        <div class="col-md-2 control-label">
			                        	{!! Form::label('message',trans('portal.Message')) !!}
			                        </div>
			                        <div class="col-sm-7">
			                        	{!! Form::textarea('message', null, array('rows' =>'3', 'class'=>'form-control', 'placeholder'=>trans('portal.Message_label'))) !!}
			                        </div>
			                    </div>
			                    <div class="form-group">
			                        {!! Form::label('captcha', trans('common.verify'), array('class' => 'col-md-2 control-label')) !!}
			                        <div class="col-sm-7">
			                            <ul class="captcha_block">
			                                <li>{!! Html::image(Captcha::src(), 'Captcha image') !!}</li>
			                                <li>{!! Form::text('captcha') !!}</li>
			                            </ul>
			                        </div>
			                    </div>
			                    <div class="form-group" style="margin-top: 25px;">
			                        <div class="col-md-8 col-md-offset-2">
			                            <button type="submit" class="btn btn-success btn-sm">{{trans('portal.send')}}</button>
			                        </div>
			                    </div>

			                {!! Form::close() !!}
			            </div>
			        </div>
	            </div>
	        </div>
	    </div>
	</div>

@endsection