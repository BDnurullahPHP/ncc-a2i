@extends('portal.layout.base')

@section('style')

<style type="text/css">
    .img_sister_con {
        margin-right: 43px;
    }
    .tender_item .widget-inner {
        padding-top: 10px;
        padding-bottom: 0px;
    }
    ul.widget-inner-list li{
        padding-bottom: 30px;
        position: relative;
    }
    .tender_actions{
        position: absolute;
        top: auto;
        bottom: 5px;
        left: auto;
        right: 0px;
    }
    .tender_actions a{
        margin-left: 10px;
    }
</style>

@endsection

@section('content')

<div class="row contents_row">
    <!-- <div class="col-md-2 col-sm-2 col-xs-6 advertisement_contents" style="padding-left: 15px;">
        <h4>Advertisements</h4>
        <ul>
            <li>
                <img src="{{ Helpers::asset('assets/portal/images/advertisement/1.jpg') }}" class="img-responsive" alt="">
            </li>
            <li>
                <img src="{{ Helpers::asset('assets/portal/images/advertisement/2.gif') }}" class="img-responsive" alt="">
            </li>
            <li>
                <img src="{{ Helpers::asset('assets/portal/images/advertisement/1.jpg') }}" class="img-responsive" alt="">
            </li>
            <li>
                <img src="{{ Helpers::asset('assets/portal/images/advertisement/2.gif') }}" class="img-responsive" alt="">
            </li>
        </ul>
    </div> -->
    <div class="col-md-8 col-sm-8 col-xs-12 xs_contents">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="main-slideshow">
                    <div class="flexslider">
                        <ul class="slides">
                            <li>
                                <img src="{{ Helpers::asset('assets/portal/images/banner/banner-1.jpg') }}"/>

                                <div class="slider-caption">
                                    <h2><a href="#">২০১৫-১৬ অর্থবছরের জন্য এনসিসি বাজেট ঘোষিত</a></h2>
                                    <p>মাননীয় মেয়র সহৃদয়তার সহিত এডিবি কান্ট্রি ডিরেক্টরকে গ্রহন করেছেন</p>
                                </div>
                            </li>
                            <li>
                                <img src="{{ Helpers::asset('assets/portal/images/banner/banner-2.jpg') }}"/>

                                <div class="slider-caption">
                                    <h2><a href="#">২০১৫-১৬ অর্থবছরের জন্য এনসিসি বাজেট ঘোষিত</a></h2>
                                    <p>মাননীয় মেয়র সহৃদয়তার সহিত এডিবি কান্ট্রি ডিরেক্টরকে গ্রহন করেছেন</p>
                                </div>
                            </li>
                            <li>
                                <img src="{{ Helpers::asset('assets/portal/images/banner/banner-3.jpg') }}"/>

                                <div class="slider-caption">
                                    <h2><a href="#">২০১৫-১৬ অর্থবছরের জন্য এনসিসি বাজেট ঘোষিত</a></h2>
                                    <p>মাননীয় মেয়র সহৃদয়তার সহিত এডিবি কান্ট্রি ডিরেক্টরকে গ্রহন করেছেন</p>
                                </div>
                            </li>
                        </ul>
                        <!-- /.slides -->
                    </div>
                    <!-- /.flexslider -->
                </div>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="widget-item" style="margin-top:0px;">
                    <!-- <h2 class="welcome-text">Welcome to NCC</h2> -->

                    <p style="text-align: justify">
                        <strong>নারায়ণগঞ্জ সিটি করপোরেশন</strong> নারায়ণগঞ্জ পৌরসভাকে (মিউনিসিপ্যালিটি) ২০১১ সালের ৫ মে নারায়ণগঞ্জ সিটি করপোরেশনে রূপান্তর করা হয়। নারায়ণগঞ্জ শহর, সিদ্ধিরগঞ্জ পৌর এলাকা এবং বন্দর উপজেলার কমদরসূল পৌরসভাকে নিয়ে নারায়ণগঞ্জ সিটি করপোরেশন গঠিত হয়।</p>
                    <p style="text-align: justify">
                        সিদ্ধিরগঞ্জ পৌরসভাকে বিলুপ্ত করে এর ১ থেকে ৯ নম্বর ওয়ার্ডকে সিটি করপোরেশনের ১ থেকে ৯ নম্বর ওয়ার্ড হিসাবে ঘোষণা করা হয়েছে। সিদ্ধিরগঞ্জের পাইনাদী পূর্ব অংশ মিজমিজি বাতান পাড়াকে ১ নম্বর ওয়ার্ড করা হয়েছে। ১ থেকে ৩ নম্বর ওয়ার্ডকে সংরক্ষিত মহিলা আসন হিসেবে তালিকা ভুক্ত করা হয়েছে। নারায়ণগঞ্জ পৌরসভা বিলুপ্ত করে এর ৯টি ওয়ার্ডকে সিটি করপোরেশনের ১০ থেকে ১৮ নম্বর ওয়ার্ড হিসেবে তালিকাভুক্ত করা হয়েছে। এই পৌরসভার সংরক্ষিত তিনটি মহিলা ওয়ার্ডকে ৪ থেকে ৬ নম্বর ওয়ার্ড হিসাবে ঘোষণা করা হয়। বন্দর উপজেলার কদম রসূল পৌরসভাকে বিলুপ্ত করে এর ৯টি ওয়ার্ডকে সিটি করপোরেশনের ১৯ থেকে ২৭ নম্বর ওয়ার্ড হিসেবে তালিকাভুক্ত করা হয়। এই পৌরসভার তিনাট মহিলা ওয়ার্ডকে সিটি করপোরেশনের ৭ থেকে ৯ নম্বর ওয়ার্ড হিসাবে ঘোষণা করা হয়।</p>
                </div>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="widget-main" style="padding-top: 15px;">
                    <div class="widget-main-title">
                        <h4 class="widget-title">ফটো গ্যালারি</h4>
                    </div>
                    <div class="widget-inner">
                        <div class="gallery-small-thumbs clearfix">
                            @for($i = 1; $i <= 15; $i++)
                            <div class="thumb-small-gallery">
                                <a class="fancybox" rel="gallery1" href="{{ Helpers::asset('assets/portal/images/gallery/'.$i.'.jpg') }}" title="গ্যালারি শিরোনাম">
                                    <img src="{{ Helpers::asset('assets/portal/images/gallery/'.$i.'-small.jpg') }}" alt=""/>
                                </a>
                            </div>
                            @endfor
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-4 col-sm-4 col-xs-12 tender_item xs_contents">
        <div class="portlet box purple">
            <div class="portlet-body">
                <div class="panel-group accordion scrollable" id="accordion2">
                    @foreach($tender_category as $key => $te_cat)
                    <?php 
                    if($te_cat->PR_CATEGORY == 21){
                        $panelColor = 'panel-info';
                    }elseif($te_cat->PR_CATEGORY == 22){
                        $panelColor = 'panel-success';
                    }elseif($te_cat->PR_CATEGORY == 23){
                        $panelColor = 'panel-default';
                    }else{
                        $panelColor = 'panel-warning';
                    }
                    ?>
                    <div class="panel {{ $panelColor }}">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse_2_{{ $key }}"> {{ $te_cat->CATE_NAME }} এর বিজ্ঞপ্তি: {{ Helpers::numberConvert(count($te_cat->getSchedule)) }} টি</a>
                            </h4>
                        </div>
                        <div id="collapse_2_{{ $key }}" class="panel-collapse <?php echo $key == 0 ? 'in' : 'collapse' ?>">
                            <div class="panel-body">
                                <ul class="widget-inner-list" style="max-height: 300px; overflow: auto;">
                                    @if(count($te_cat->getSchedule) > 0)
                                    @foreach($te_cat->getSchedule as $index => $ts)
                                    <li>
                                        @if($te_cat->IS_LEASE == 0)
                                        {{ $ts->getProject->PR_NAME_BN }} / 
                                        {{ $ts->getCategory->CATE_NAME }} / 
                                        {{ $ts->getProjectType->TYPE_NAME }} / 
                                        {{ $ts->getProjectDetail->PR_SSF_NO }}
                                        <div class="tender_actions">
                                            <a class="label label-primary" href="{{ route('apply_form_download', ['tender', $ts->SCHEDULE_ID]) }}">
                                                <i class="fa fa-download"></i> Download
                                            </a>
                                            <a class="label label-success" href="{{ route('get_tender_conditions', [$te_cat->PR_CATEGORY, $ts->TENDER_ID, $ts->SCHEDULE_ID]) }}">
                                                <i class="fa fa-paper-plane"></i> Apply
                                            </a>
                                        </div>
                                        @else
                                            {{ $ts->getProject->PR_NAME_BN }}
                                            <div class="tender_actions">
                                                <a class="label label-primary" href="{{ route('apply_form_download', ['lease', $ts->SCHEDULE_ID]) }}">
                                                    <i class="fa fa-download"></i> Download
                                                </a>
                                                <a class="label label-success" href="{{ route('get_tender_conditions', [$te_cat->PR_CATEGORY, $ts->TENDER_ID, $ts->SCHEDULE_ID]) }}">
                                                    <i class="fa fa-paper-plane"></i> Apply
                                                </a>
                                            </div>
                                        @endif
                                    </li>
                                    @endforeach
                                    @else
                                    <span>"বর্তমানে কোন বিজ্ঞপ্তি নাই"</span>
                                    @endif
                                </ul>
                                @if(count($te_cat->getSchedule) > 0)
                                @if($te_cat->IS_LEASE == 0)
                                <a href="{{ route('get_tender') }}" class="btn btn-more pull-right">সকল</a><br>
                                @else
                                <a href="{{ route('get_lease') }}" class="btn btn-more pull-right">সকল</a><br>
                                @endif
                                @endif
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
        <!--        <div class="row">
                    @if(count($tender_category) > 0)
                    @foreach($tender_category as $key => $te_cat)
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="widget-main">
                            <div class="widget-main-title info_header_style">
                                <h4 class="widget-title">
                                    <i class="fa fa-globe" aria-hidden="true"></i> {{ $te_cat->CATE_NAME }} বিজ্ঞপ্তি
                                </h4>
                            </div>
                            <ul class="widget-inner-list">
                                @foreach($te_cat->getSchedule as $index => $ts)
                                <li>
                                    @if($te_cat->IS_LEASE == 0)
                                    {{ $ts->getProject->PR_NAME_BN }} / 
                                    {{ $ts->getCategory->CATE_NAME }} / 
                                    {{ $ts->getProjectType->TYPE_NAME }} / 
                                    {{ $ts->getProjectDetail->PR_SSF_NO }}
                                    <div class="tender_actions">
                                        <a class="label label-primary" href="{{ route('apply_form_download', ['tender', $ts->SCHEDULE_ID]) }}">
                                            <i class="fa fa-download"></i> Download
                                        </a>
                                        <a class="label label-success" href="{{ route('get_tender_conditions', [$te_cat->PR_CATEGORY, $ts->TENDER_ID, $ts->SCHEDULE_ID]) }}">
                                            <i class="fa fa-paper-plane"></i> Apply
                                        </a>
                                    </div>
                                    @else
                                    {{ $ts->getProject->PR_NAME_BN }}
                                    <a href="{{ route('apply_form_download', ['lease', $ts->SCHEDULE_ID]) }}">
                                        <i class="fa fa-download"></i> Download
                                    </a>
                                    <a href="{{ route('get_tender_conditions', [$te_cat->PR_CATEGORY, $ts->TENDER_ID, $ts->SCHEDULE_ID]) }}">
                                        <i class="fa fa-paper-plane"></i> Apply
                                    </a>
                                    @endif
                                </li>
                                @endforeach
                            </ul>
                            @if(count($te_cat->getSchedule) > 0)
                            @if($te_cat->IS_LEASE == 0)
                            <a href="{{ route('get_tender') }}" class="btn btn-more pull-right">সকল</a><br>
                            @else
                            <a href="{{ route('get_lease') }}" class="btn btn-more pull-right">সকল</a><br>
                            @endif
                            @endif
                        </div>
                    </div>
                    @endforeach
                    @endif	            
                </div>-->
    </div>      
</div>
@endsection