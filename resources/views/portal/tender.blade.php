@extends('portal.layout.base')

@section('css_file')

    <link href="{{ Helpers::asset('assets/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ Helpers::asset('assets/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />

@endsection

@section('style')

	<style type="text/css">
		.control-label{
	        text-align: left !important;
	    }
	    .form-control{
	        height: 34px !important;
	        border-radius: 0px;
	    }
	    .input-group-addon{
	        border-radius: 0px;
	    }
	    textarea#address{
	        width: 100%;
	        max-width: 100%;
	        height: auto !important;
	    }
	    .btn{
	        border-radius: 0px;
	    }
	    #reset_form{
	        margin-right: 10px;
	    }
        .dataTables_wrapper .dataTables_paginate .paginate_button{
            padding: 3px 5px;
        }
        .pagination>li>a, .pagination>li>span{
            padding: 5px 8px;
        }
        .body_contents{
            background-color: #FFFFFF;
        }
	</style>

@endsection

@section('js_file')

    <script src="{{ Helpers::asset('assets/plugins/datatables/datatables.min.js') }}" type="text/javascript"></script>
    <script src="{{ Helpers::asset('assets/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') }}" type="text/javascript"></script>
    <script src="{{ Helpers::asset('assets/portal/js/tenderapplicant.js') }}" type="text/javascript"></script>

@endsection

@section('script')

	<script type="text/javascript">
	    $(document).ready(function() {
	        TenderApplicant.init();
            @if (Session::has('app_exist'))
                sweetAlert("Oops...", "{{ Session::get('app_exist') }}", "error");
            @endif
	    });
	</script>

@endsection

@section('content')

    @foreach($project_category as $key => $pr_cat)
        <div class="col-md-12 col-sm-12 col-xs-12">
            <h3 class="text-center">{{ $pr_cat->CATE_NAME }} {{ trans('common._details') }}</h3>
            <table class="table table-bordered display" data-source="{{ route('tenderschedule_data', $pr_cat->PR_CATEGORY) }}">
                <thead>
                    <tr>
                        <th class="text-center">{{trans('portal.sl_no')}}</th>
                        <th class="text-center">{{trans('portal.market_name')}}</th>
                        <th class="text-center">{{trans('portal.type')}}</th>
                        <th class="text-center">{{trans('portal.measuring')}}</th>
                        <th class="text-center">{{trans('portal.minimum_salami_mount_(money)')}}</th>
                        <th class="text-center">{{trans('portal.every_square_footMonthly_rent_(money)')}}</th>
                        <th class="text-center">{{trans('portal.Schedule_Price_(Taka)_Each_Non-refundable')}}</th>
                        <th class="text-center">{{trans('portal.The_amount_of_security_Submission_bid')}}</th>
                        <th class="text-center" style="width: 150px;">{{trans('portal.Activity')}}</th>
                    </tr>
                </thead>
                <tbody>

                </tbody>
            </table>
        </div>
    @endforeach
	        
@endsection