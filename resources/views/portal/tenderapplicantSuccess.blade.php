@extends('portal.layout.base')

@section('style')

	<style type="text/css">
		.control-label{
	        text-align: left !important;
	    }
	    .form-control{
	        height: 34px !important;
	        border-radius: 0px;
	    }
	    .input-group-addon{
	        border-radius: 0px;
	    }
	    textarea#address{
	        width: 100%;
	        max-width: 100%;
	        height: auto !important;
	    }
	    .btn{
	        border-radius: 0px;
	    }
	    #reset_form{
	        margin-right: 10px;
	    }
	    .well{
	        background-color:#D0FFFF;
	        color:#3C6FFF;
	        font-size:18px;
	        border-style: solid;
            border-width: 3px;
            border-color:#33CCFF;
            margin-top: 25px;
	    }
	    .body_contents{
	    	min-height: 325px;
	    }
	</style>

@endsection

@section('script')

	<script type="text/javascript">
		
	    $(function() {
			setTimeout(function() {
				@if( Session::has('applicant') )
					$('#authentication_form').submit();
				@else
					window.location.href = "{{ route('user_login') }}";
				@endif
			}, 2000);
		});

	</script>

@endsection

@section('content')

	<div class="row">
	    <div class="col-md-12 col-sm-12 col-xs-12">
	    	{!! Form::open(array('route' => 'user_authentication', 'method' => 'post', 'id'=>'authentication_form')) !!}
	    	{!! Form::close() !!}
			<div class="well well-lg" align="center" style="background-color:#D0FFE0; color:#029A02;font-size:18px;border-style: solid;border-width: 3px;border-color:#33CC00;">
				<img src="{{ Helpers::asset('assets/portal/images/banner/Check.png') }}"/><br><br>
				আপনার আবেদনটি গ্রহন করা হল<br><br>পরবর্তী ২৪ ঘণ্টার মধ্যে  আপনার অনলাইন পেমেন্ট যাচাই করে,এসএমএস এর মাধ্যমে জানানো হবে।<br><br>ধন্যবাদ।
	        </div>
	    </div>
	</div>

@endsection

