<div class="col-md-4 col-sm-10 col-xs-12">
    <div class="panel panel-default" data-spy="affix" data-offset-top="95" data-offset-bottom="200">
        <div class="panel-heading" style="background-color:#609513;">
        	@if($type=='lease')
                <p style="color: #FFFFFF;">{{trans('tender.The_details_of_the_lease')}} </p>
            @else
                <p style="color: #FFFFFF;">{{trans('tender.Tender_details')}} </p>
            @endif
        </div>
        <div class="panel-body">
            @if($type=='lease')
                @if( !empty($schedule_data->getProject) )
                    <p><strong>{{trans('tender.Name_of_the_syarat_mahal')}}: </strong>{{ $schedule_data->getProject->PR_NAME_BN }}</p>
                @endif
            @else
                @if( !empty($schedule_data->getProject) )
                    <p><strong>{{trans('tender.project_name')}}: </strong>{{ $schedule_data->getProject->PR_NAME_BN }}</p>
                @endif
                @if( !empty($schedule_data) && !empty($schedule_data->getProjectDetail) )
                    <p><strong>{{trans('tender.measuring')}}: </strong>{{ $schedule_data->getProjectDetail->PR_MEASURMENT }} {{trans('tender.per_square')}} </p>
                @endif
                @if( !empty($schedule_data) && !empty($schedule_data->getProjectType) )
                    <p><strong>{{trans('tender.Type')}}: </strong>{{ $schedule_data->getProjectType->TYPE_NAME }}</p>
                @endif
                @if( !empty($schedule_data) && !empty($schedule_data->getProjectDetail) )
                    <p><strong>{{trans('tender.Position')}}: </strong>{{ Helpers::numberConvert($schedule_data->getProjectDetail->POSITION) }} {{trans('tender.Floor')}} </p>
                @endif
                @if( !empty($schedule_data) && !empty($schedule_data->getProjectDetail) )
                    <p><strong>{{trans('tender.number')}}: </strong>{{ $schedule_data->getProjectDetail->PR_SSF_NO }}</p>
                @endif
            @endif
            <hr style="margin-top: 10px;margin-bottom: 10px;">
            @foreach($tender_particular as $key => $tp)
                <p>
                    <strong>
                        @if($tp->PARTICULAR_ID == 5)
                            প্রতি বর্গফুটের 
                        @endif
                        
                        @if($tp->PARTICULAR_ID == 4 || $tp->PARTICULAR_ID == 7)
                            {{ $tp->getParticular->PARTICULAR_NAME }}&#40;{{trans('tender.Each')}}&#41;&#40;{{trans('tender.Non_refundable')}}&#41;: 
                        @elseif($tp->PARTICULAR_ID == 3 || $tp->PARTICULAR_ID == 6)
                            {{ $tp->getParticular->PARTICULAR_NAME }}&#40;{{trans('tender.Submitted_Rates')}}&#41;: 
                        @else
                            {{ $tp->getParticular->PARTICULAR_NAME }}: 
                        @endif
                        
                    </strong>
                    
                    @if($tp->UOM == 1)
                        {{ Helpers::numberConvert($tp->PARTICULAR_AMT) }} {{trans('tender.percentage')}}
                    @else
                        {{ Helpers::numberConvert(number_format($tp->PARTICULAR_AMT, 2)) }} {{trans('tender.taka')}}
                    @endif
                </p>
            @endforeach
        </div>
    </div>
</div>