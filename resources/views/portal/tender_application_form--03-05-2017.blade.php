@extends('portal.layout.base')

@section('css_file')

	<link href="{{ Helpers::asset('assets/plugins/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ Helpers::asset('assets/plugins/select2/css/select2-bootstrap.min.css') }}" rel="stylesheet" type="text/css" />

@endsection

@section('style')

	<style type="text/css">
		.control-label{
	        text-align: left !important;
	    }
	    .form-control{
	        height: 34px !important;
	        border-radius: 0px;
	    }
	    .input-group-addon{
	        border-radius: 0px;
	    }
	    textarea#address{
	        width: 100%;
	        max-width: 100%;
	        height: auto !important;
	    }
	    .btn{
	        border-radius: 0px;
	    }
	    #reset_form{
	        margin-right: 10px;
	    }
	    .personal_info{
	       background-color:#e8f6f3;
	       padding: 5px;
	    }
	    .tender_info{
			background-color:#e8f6f3;
			padding: 5px;
	    }
	    .tender_schedule{
			background-color:#e8f6f3;
			padding: 5px;
	    }
	    .affix{
			top: 5px;
			bottom: 330px;
			position: fixed;
			height: 100%;
    		max-height: 310px;
			width: 100%;
			max-width: 350px;
			background-color: #FFFFFF;
	  	}
	  	.bank_pay_order, .cash_order{
	  		display: none;
	  	}
	  	.validation_error{
	  		color: red;
	  	}
	</style>

@endsection

@section('js_file')

	<script src="{{ Helpers::asset('assets/plugins/select2/js/select2.full.min.js') }}" type="text/javascript"></script>
    <script src="{{ Helpers::asset('assets/portal/js/tenderapplicant.js') }}" type="text/javascript"></script>

@endsection

@section('script')

	<script type="text/javascript">
		$(document).ready(function() {
	        TenderApplicant.init();
	    });
   
	</script>

@endsection

@section('content')

	<div class="row">
	    <div class="col-md-12 col-sm-12 col-xs-12">
	        <div class="contact-page-content">
	            <div class="contact-heading">
	                <!--========Session message=======-->
					@if (Session::has('success'))
	                    <div class="alert alert-success">
	                      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	                      {{ Session::get('success') }}
	                    </div>
	                @endif
	                @if (Session::has('error'))
	                    <div class="alert alert-danger">
	                      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	                      {{ Session::get('error') }}
	                    </div>
	                @endif
					<!--======end Session message=====-->
	            </div>

	            	<div class="row">

	            		<div class="col-md-8 col-sm-10 col-xs-12">
	            			<a class="btn btn-success" href="{{ route('apply_form_download', [Request::segment(1), $schedule_data->SCHEDULE_ID]) }}"><i class="fa fa-download"></i> ডাউনলোড ফর্ম</a> <br>
	            		  	<h3>নিম্নের ফর্মটি পূরণ করুন</h3>
	            		   	<p class="text-danger">(*) চিহ্নিত তথ্য গুলো  অবশ্যই পূরণ করুন</p>
	            		   	<hr>
	            		   	<h4 style="font-weight:bold;">আবেদনকারীর তথ্য</h4>
	            		  	<hr>
			            	{!! Form::open(array('route' => ['tenderapplicant_create_form', Request::segment(1), $schedule_data->SCHEDULE_ID], 'method' => 'post', 'id'=>'tenderapplicant_form', 'class' =>'form-horizontal', 'files' => true)) !!}

			            		<input type="hidden" name="SCHEDULE_ID" value="{{ $schedule_data->SCHEDULE_ID }}">
			            		<input type="hidden" name="TOTAL_AMOUNT" value="{{ $schedule_price }}">
			            		@if( isset($citizen_data) && !empty($citizen_data->getCitizen) )
			            			<input type="hidden" name="CITIZEN_ID" value="{{ $citizen_data->getCitizen->CITIZEN_ID }}">
			            		@elseif( isset($applicant_data) )
			            			<input type="hidden" name="TE_APPLICANT_ID" value="{{ $applicant_data->TE_APP_ID }}">
			            		@endif
								<div class="personal_info">
			                    
				                    <div class="form-group">
				                        <div class="col-md-4 col-sm-4 col-xs-12">
				                        	{!! Form::label('FIRST_NAME', trans('tender.tender_applicant_name')) !!} <span style="color: red">*</span>
				                        	<p><small>{{ trans('tender.tender_applicant_name_u') }}</small></p>
				                       		@if( isset($citizen_data) && !empty($citizen_data->getCitizen) )
				                           		{!! Form::text('FIRST_NAME', $citizen_data->FIRST_NAME, array('class'=>'form-control form-required', 'readonly'=>'readonly', 'placeholder'=>trans('tender.tender_applicant_name_ex'))) !!}
				                           	@elseif( isset($applicant_data) )
				                           		{!! Form::text('FIRST_NAME', $applicant_data->FIRST_NAME, array('class'=>'form-control form-required', 'readonly'=>'readonly', 'placeholder'=>trans('tender.tender_applicant_name_ex'))) !!}
				                           	@else
				                           		{!! Form::text('FIRST_NAME', Input::old('FIRST_NAME'), array('class'=>'form-control form-required', 'placeholder'=>trans('tender.tender_applicant_name_ex'))) !!}
				                           	@endif
				                        	
				                        	<p class="validation_error">{{ $errors->first('FIRST_NAME') }}</p>
				                        </div>

				                        <div class="col-md-4 col-sm-4 col-xs-12">
				                        	{!! Form::label('LAST_NAME', trans('tender.tender_applicant_name_last')) !!} <span style="color: red">*</span>
				                        	<p><small>{{ trans('tender.tender_applicant_name_last_ex') }}</small></p>
				                        	@if( isset($citizen_data) && !empty($citizen_data->getCitizen) )
												{!! Form::text('LAST_NAME', $citizen_data->LAST_NAME, array('class'=>'form-control form-required', 'readonly'=>'readonly', 'placeholder'=>trans('tender.tender_applicant_name_last_pl'))) !!}
				                        	@elseif( isset($applicant_data) )
												{!! Form::text('LAST_NAME', $applicant_data->LAST_NAME, array('class'=>'form-control form-required', 'readonly'=>'readonly', 'placeholder'=>trans('tender.tender_applicant_name_last_pl'))) !!}
				                        	@else
												{!! Form::text('LAST_NAME', Input::old('LAST_NAME'), array('class'=>'form-control form-required', 'placeholder'=>trans('tender.tender_applicant_name_last_pl'))) !!}
				                        	@endif
				                        	
				                        	<p class="validation_error">{{ $errors->first('LAST_NAME') }}</p>
				                        </div>

				                        <div class="col-md-4 col-sm-4 col-xs-12">
				                        	{!! Form::label('SPOUSE_NAME', trans('tender.tender_applicant_name_fh')) !!} <span style="color: red">*</span>
				                        	<p><small>{{ trans('tender.tender_applicant_name_fh_u') }}</small></p>
				                        	@if( isset($citizen_data) && !empty($citizen_data->getCitizen) && !empty($citizen_data->getCitizen->SPOUSE_NAME) )
				                        		{!! Form::text('SPOUSE_NAME', $citizen_data->getCitizen->SPOUSE_NAME, array('class'=>'form-control', 'readonly'=>'readonly', 'placeholder'=>trans('tender.tender_applicant_name_fh_ex'))) !!}
				                        	@elseif( isset($applicant_data) )
				                        		{!! Form::text('SPOUSE_NAME', $applicant_data->SPOUSE_NAME, array('class'=>'form-control', 'readonly'=>'readonly', 'placeholder'=>trans('tender.tender_applicant_name_fh_ex'))) !!}
				                        	@else
												{!! Form::text('SPOUSE_NAME', Input::old('SPOUSE_NAME'), array('class'=>'form-control', 'placeholder'=>trans('tender.tender_applicant_name_fh_ex'))) !!}
				                        	@endif

				                        	<p class="validation_error">{{ $errors->first('SPOUSE_NAME') }}</p>
				                        </div>
				                    </div>
	                                <div class="form-group">
				                       	<div class="col-md-4 col-sm-6 col-xs-12">
				                        	{!! Form::label('division', 'বিভাগ:') !!} <span style="color: red">*</span>
				                        	<p><small>বিভাগ সিলেক্ট করুন </small></p>
				                       		@if( isset($citizen_data) && !empty($citizen_data->getCitizen) )
												{!! Form::select('division', $division, $citizen_data->getCitizen->DIVISION_ID, array('class'=>'form-control ', 'readonly'=>'readonly', 'style'=>'pointer-events:none;', 'placeholder' => trans('common.form_select'))) !!}
				                       		@elseif( isset($applicant_data) )
												{!! Form::select('division', $division, $applicant_data->DIVISION_ID, array('class'=>'form-control ', 'readonly'=>'readonly', 'style'=>'pointer-events:none;', 'placeholder' => trans('common.form_select'))) !!}
				                       		@else
												{!! Form::select('division', $division, array(Input::old('division')), array('class'=>'form-control ', 'placeholder' => trans('common.form_select'))) !!}
				                       		@endif
	                                       
	                                       	<p class="validation_error">{{ $errors->first('division') }}</p>
	                                    </div>

	                                    <div class="col-md-4 col-sm-6 col-xs-12">
				                        	{!! Form::label('district', 'জেলা:') !!} <span style="color: red">*</span>
				                        	<p><small>জেলা সিলেক্ট করুন</small> </p>
				                        	@if( isset($citizen_data) && !empty($citizen_data->getCitizen) )
												{!! Form::select('district', isset($district)?$district:[], $citizen_data->getCitizen->DISTRICT_ID, array('class'=>'form-control ', 'readonly'=>'readonly', 'style'=>'pointer-events:none;', 'placeholder' => trans('common.form_select'))) !!}
				                        	@elseif( isset($applicant_data) )
				                        		{!! Form::select('district', $district, $applicant_data->DISTRICT_ID, array('class'=>'form-control ', 'readonly'=>'readonly', 'style'=>'pointer-events:none;', 'placeholder' => trans('common.form_select'))) !!}
				                        	@else
				                        		{!! Form::select('district', [], array(Input::old('district')), array('class'=>'form-control ', 'placeholder' => trans('common.form_select'))) !!}
				                        	@endif
	                                        
	                                    	<p class="validation_error">{{ $errors->first('district') }}</p>
				                        		            
				                        </div>

				                        <div class="col-md-4 col-sm-6 col-xs-12">
				                        	{!! Form::label('thana', 'উপজেলা/থানা:') !!} <span style="color: red">*</span>
				                        	<p><small>উপজেলা/থানা সিলেক্ট করুন </small></p>
				                        	@if( isset($citizen_data) && !empty($citizen_data->getCitizen) )
				                        		{!! Form::select('thana', isset($thana)?$thana:[], $citizen_data->getCitizen->THANA_ID, array('class'=>'form-control', 'readonly'=>'readonly', 'style'=>'pointer-events:none;', 'placeholder' => trans('common.form_select'))) !!}
				                        	@elseif( isset($applicant_data) )
				                        		{!! Form::select('thana', $thana, $applicant_data->THANA_ID, array('class'=>'form-control', 'readonly'=>'readonly', 'style'=>'pointer-events:none;', 'placeholder' => trans('common.form_select'))) !!}
				                        	@else
												{!! Form::select('thana', [], array(Input::old('thana')), array('class'=>'form-control', 'placeholder' => trans('common.form_select'))) !!}
				                        	@endif
											
											<p class="validation_error">{{ $errors->first('thana') }}</p>
				                        		            
				                        </div>
	                                     
				                    </div>

				                    <div class="form-group">
				                        <div class="col-md-4 col-sm-6 col-xs-12">
				                        	{!! Form::label('ROAD_NO', 'রোড নং:') !!}  <span style="color: red">*</span>
				                        	<p><small>রোড নং এখানে লিখুন </small></p>
				                            @if( isset($citizen_data) && !empty($citizen_data->getCitizen) && !empty($citizen_data->getCitizen->ROAD_NO) )
				                            	{!! Form::text('ROAD_NO', $citizen_data->getCitizen->ROAD_NO, array('rows' =>'1', 'class'=>'form-control', 'readonly'=>'readonly', 'placeholder'=>'Ex.রোড নং-১২,ঢাকা-১২১৬')) !!}
				                            @elseif( isset($applicant_data) )
				                            	{!! Form::text('ROAD_NO', $applicant_data->ROAD_NO, array('rows' =>'1', 'class'=>'form-control', 'readonly'=>'readonly', 'placeholder'=>'Ex.রোড নং-১২,ঢাকা-১২১৬')) !!}
				                            @else
												{!! Form::text('ROAD_NO', Input::old('ROAD_NO'), array('rows' =>'1', 'class'=>'form-control', 'placeholder'=>'Ex.রোড নং-১২,ঢাকা-১২১৬')) !!}
				                            @endif
				                        	
				                        	<p class="validation_error">{{ $errors->first('ROAD_NO') }}</p>
				                        </div>

				                        <div class="col-md-4 col-sm-6 col-xs-12">
					                        {!! Form::label('HOLDING_NO', 'হোল্ডিং নং:') !!}<span style="color: red">*</span>
					                        <p><small>হোল্ডিং  নং এখানে লিখুন</small></p> 
					                        @if( isset($citizen_data) && !empty($citizen_data->getCitizen) )
					                        	{!! Form::text('HOLDING_NO', $citizen_data->getCitizen->HOLDING_NO, array('rows' =>'1', 'class'=>'form-control', 'readonly'=>'readonly', 'placeholder'=>'Ex.হোল্ডিং নং-১২,ঢাকা-১২১৬')) !!}
					                        @elseif( isset($applicant_data) )
					                        	{!! Form::text('HOLDING_NO', $applicant_data->HOLDING_NO, array('rows' =>'1', 'class'=>'form-control', 'readonly'=>'readonly', 'placeholder'=>'Ex.হোল্ডিং নং-১২,ঢাকা-১২১৬')) !!}
					                        @else
												{!! Form::text('HOLDING_NO', null, array('rows' =>'1', 'class'=>'form-control', 'placeholder'=>'Ex.হোল্ডিং নং-১২,ঢাকা-১২১৬')) !!}
					                        @endif
					                        
					                        <p class="validation_error">{{ $errors->first('HOLDING_NO') }}</p>
				                        </div>
	                                   	<div class="col-md-4 col-sm-6 col-xs-12">
		                                   	{!! Form::label('APPLICANT_PHONE', 'মোবাইল নাম্বার:') !!} <span style="color: red">*</span>
		                                    <p><small>আপনার মোবাইল নাম্বার এখানে লিখুন </small></p>
		                                   	@if( isset($citizen_data) && !empty($citizen_data->getCitizen) )
		                                   		{!! Form::text('APPLICANT_PHONE', $citizen_data->MOBILE_NO, array('id'=>'applicant_phone_no', 'class'=>'form-control', 'readonly'=>'readonly', 'placeholder'=>'Ex.০১৬৮০১২০৫৩৯', 'data-url'=>route('check_unique_phone'))) !!}
		                                   	@elseif( isset($applicant_data) )
		                                   		{!! Form::text('APPLICANT_PHONE', $applicant_data->APPLICANT_PHONE, array('id'=>'applicant_phone_no', 'class'=>'form-control', 'readonly'=>'readonly', 'placeholder'=>'Ex.০১৬৮০১২০৫৩৯', 'data-url'=>route('check_unique_phone'))) !!}
		                                   	@else
												{!! Form::text('APPLICANT_PHONE', Input::old('APPLICANT_PHONE'), array('id'=>'applicant_phone_no', 'class'=>'form-control', 'placeholder'=>'Ex.০১৬৮০১২০৫৩৯', 'data-url'=>route('check_unique_phone'))) !!}
		                                   	@endif
		                                  
		                                    <p class="validation_error">{{ $errors->first('APPLICANT_PHONE') }}</p>
	                                   	</div>
				                    </div>
				                  
				                    <div class="form-group">
				                    	<div class="col-md-6 col-sm-6 col-xs-12">
	                                   		{!! Form::label('NID', 'জাতীয় পরিচয়পত্র / জন্মনিবন্ধন:') !!} <span style="color: red">*</span>
	                                    	<p><small>আপনার জন্মনিবন্ধন অথবা জাতীয় পরিচয়পত্রের নাম্বার লিখুন </small></p>
	                                   		@if( isset($citizen_data) && !empty($citizen_data->getCitizen) )
	                                   			{!! Form::text('NID', $citizen_data->getCitizen->NID, array('id'=>'applicant_nid', 'class'=>'form-control', 'readonly'=>'readonly', 'data-url'=>route('check_unique_nid'), 'placeholder'=>'Ex.2610413965404')) !!}
	                                   		@elseif( isset($applicant_data) )
	                                   			{!! Form::text('NID', $applicant_data->NID, array('id'=>'applicant_nid', 'class'=>'form-control', 'readonly'=>'readonly', 'data-url'=>route('check_unique_nid'), 'placeholder'=>'Ex.2610413965404')) !!}
	                                   		@else
												{!! Form::text('NID', Input::old('NID'), array('id'=>'applicant_nid', 'class'=>'form-control', 'data-url'=>route('check_unique_nid'), 'placeholder'=>'Ex.2610413965404')) !!}
	                                   		@endif
	                                    	
	                                    	<p class="validation_error">{{ $errors->first('NID') }}</p>
	                                   	</div>
				                       	<div class="col-md-6 col-sm-6 col-xs-12">
				                        	{!! Form::label('APPLICANT_EMAIL', 'ইমেইল:') !!}
				                        	<p class="small">আপনার ইমেইল এখানে লিখুন</p>
				                            @if( isset($citizen_data) && !empty($citizen_data->getCitizen) )
				                            	@if( !empty($citizen_data->EMAIL) )
				                            		{!! Form::email('APPLICANT_EMAIL', $citizen_data->EMAIL, array('id'=>'applicant_email_id', 'class'=>'form-control', 'readonly'=>'readonly', 'placeholder'=>'Ex.user@gmail.com', 'data-url'=>route('check_unique_email'))) !!}
				                            	@else
				                            		{!! Form::email('APPLICANT_EMAIL', Input::old('APPLICANT_EMAIL'), array('id'=>'applicant_email_id', 'class'=>'form-control', 'placeholder'=>'Ex.user@gmail.com', 'data-url'=>route('check_unique_email'))) !!}
				                            	@endif
				                            @elseif( isset($applicant_data) )
				                            	@if( !empty($applicant_data->EMAIL) )
				                            		{!! Form::email('APPLICANT_EMAIL', $applicant_data->EMAIL, array('id'=>'applicant_email_id', 'class'=>'form-control', 'readonly'=>'readonly', 'placeholder'=>'Ex.user@gmail.com', 'data-url'=>route('check_unique_email'))) !!}
				                            	@else
				                            		{!! Form::email('APPLICANT_EMAIL', Input::old('APPLICANT_EMAIL'), array('id'=>'applicant_email_id', 'class'=>'form-control', 'placeholder'=>'Ex.user@gmail.com', 'data-url'=>route('check_unique_email'))) !!}
				                            	@endif
				                            @else
												{!! Form::email('APPLICANT_EMAIL', Input::old('APPLICANT_EMAIL'), array('id'=>'applicant_email_id', 'class'=>'form-control', 'placeholder'=>'Ex.user@gmail.com', 'data-url'=>route('check_unique_email'))) !!}
				                            @endif
				                        	
				                        	<p class="validation_error">{{ $errors->first('APPLICANT_EMAIL') }}</p>
				                        </div>
				                    </div>
									
									@if( (isset($citizen_data) && !empty($citizen_data->getCitizen) ) || isset($applicant_data) )
										<div class="form-group" style="display:none;">
									@else
										<div class="form-group">
									@endif
				                        <div class="col-md-6 col-sm-6 col-xs-12">
				                        	{!! Form::label('TE_APP_PASSWORD', 'পাসওয়ার্ড :') !!} <span style="color: red">*</span>
				                        	<p><small>আপনার পাসওয়ার্ড এখানে লিখুন</small></p>
				                        	@if( isset($citizen_data) && !empty($citizen_data->getCitizen) )
				                        		<input type="password" name="TE_APP_PASSWORD" readonly="readonly" value="{{ $citizen_data->PASSWORD }}" id="TE_APP_PASSWORD" class="form-control" placeholder="Ex.@2345%*+A">
				                        	@elseif( isset($applicant_data) )
												<input type="password" name="TE_APP_PASSWORD" readonly="readonly" value="{{ $applicant_data->PASSWORD }}" id="TE_APP_PASSWORD" class="form-control" placeholder="Ex.@2345%*+A">
				                        	@else
												{!! Form::password('TE_APP_PASSWORD', array('class'=>'form-control', 'placeholder'=>'Ex.@2345%*+A')) !!}
				                        	@endif

				                            <p class="validation_error">{{ $errors->first('TE_APP_PASSWORD') }}</p>
				                        </div>
				                        <div class="col-md-6 col-sm-6 col-xs-12">
				                        	{!! Form::label('RE_PASSWORD', 'পুনরায় পাসওয়ার্ড :') !!} <span style="color: red">*</span>
				                        	<p><small>আপনার পাসওয়ার্ড নিশ্চিত করুন</small></p>
				                        	@if( isset($citizen_data) && !empty($citizen_data->getCitizen) )
				                        		<input type="password" name="RE_PASSWORD" readonly="readonly" value="{{ $citizen_data->PASSWORD }}" id="RE_PASSWORD" class="form-control" placeholder="Ex.@2345%*+A">
				                        	@elseif( isset($applicant_data) )
												<input type="password" name="RE_PASSWORD" readonly="readonly" value="{{ $applicant_data->PASSWORD }}" id="RE_PASSWORD" class="form-control" placeholder="Ex.@2345%*+A">
				                        	@else
												{!! Form::password('RE_PASSWORD', array('class'=>'form-control', 'placeholder'=>'Ex.@2345%*+A')) !!}
				                        	@endif

				                            <p class="validation_error">{{ $errors->first('RE_PASSWORD') }}</p>
				                        </div>
				                    </div>

			                    </div>
			                
			                    <h4 style="font-weight:bold;"> @if(Request::segment(1)=='lease') ইজারার @else দরপত্রের @endif বিবরণ</h4> <hr>
                                <div class="tender_info">
			                    <div class="form-group">
			                        <div class="col-md-4 col-sm-6 col-xs-12">
			                        	{!! Form::label('BID_AMOUNT', 'দরদাতা কতৃক প্রদত্ত দর  ') !!} <span style="color: red">*</span>
			                          <p><small>দরদাতা কতৃক প্রদত্ত দর লিখুন</small></p>

			                        </div>
			                        <div class="col-md-4 col-sm-6 col-xs-12">
			                           	{!! Form::label('BID_AMOUNT', '(অংকে লিখুন) : ') !!} <span style="color: red">*</span>
			                           	@if(isset($guarantee))
				                           	<input type="hidden" class="guarantee_percent" value="{{ $guarantee }}">
				                        	{!! Form::text('BID_AMOUNT', Input::old('BID_AMOUNT'), array('id'=>'applicant_bid_amount', 'class'=>'form-control', 'placeholder'=>'Ex.১২০০০')) !!}
				                        @else
				                        	{!! Form::text('BID_AMOUNT', Input::old('BID_AMOUNT'), array('class'=>'form-control', 'placeholder'=>'Ex.১২০০০')) !!}
				                        @endif
			                        	<p class="validation_error">{{ $errors->first('BID_AMOUNT') }}</p>
			                        </div>
			                        <div class="col-md-4 col-sm-6 col-xs-12">
			                        	{!! Form::label('BID_AMOUNT_TEXT', '(কথায় লিখুন) : ') !!} <span style="color: red">*</span>
			                        	
			                        	{!! Form::text('BID_AMOUNT_TEXT', Input::old('BID_AMOUNT_TEXT'), array('class'=>'form-control', 'placeholder'=>'Ex.বার হাজার টাকা মাত্র')) !!}
			                        	<p class="validation_error">{{ $errors->first('BID_AMOUNT_TEXT') }}</p>
			                        </div>
			                    </div>
			                    <div class="form-group">
			                        <div class="col-md-4 col-sm-6 col-xs-12">
			                        	@if(Request::segment(1)=='lease')
			                        		{!! Form::label('BG_AMOUNT', 'বায়নার পরিমান') !!} <span style="color: red">*</span>
				                            <p><small>বায়নার পরিমান লিখুন</small> </p>
			                        	@else
			                        		{!! Form::label('BG_AMOUNT', 'জামানতের পরিমান') !!} <span style="color: red">*</span>
				                            <p><small>জামানতের পরিমান লিখুন</small> </p>
			                        	@endif
			                        </div>
			                        <div class="col-md-4 col-sm-6 col-xs-12">
			                            {!! Form::label('BG_AMOUNT', '(অংকে লিখুন) : ') !!} <span style="color: red">*</span>
			                            @if(isset($guarantee))
			                            	{!! Form::text('BG_AMOUNT', Input::old('BG_AMOUNT'), array('id'=>'applicant_guarantee', 'class'=>'form-control', 'placeholder'=>'Ex.১২০০০', 'readonly'=>'readonly')) !!}
			                            @else
											{!! Form::text('BG_AMOUNT', Input::old('BG_AMOUNT'), array('class'=>'form-control', 'placeholder'=>'Ex.১২০০০')) !!}
			                            @endif
			                            <p class="validation_error">{{ $errors->first('BG_AMOUNT') }}</p>

			                            </div>
			                            <div class="col-md-4 col-sm-6 col-xs-12">
				                        	{!! Form::label('BG_AMOUNT_TEXT', '(কথায় লিখুন) : ') !!} <span style="color: red">*</span>
				                        	{!! Form::text('BG_AMOUNT_TEXT', Input::old('BG_AMOUNT_TEXT'), array('class'=>'form-control', 'placeholder'=>'Ex.বার হাজার টাকা মাত্র')) !!}
				                        	<p class="validation_error">{{ $errors->first('BG_AMOUNT_TEXT') }}</p>
				                        </div>
			                    	</div>

			                    	{{--<div class="form-group">
			                    		<div class="col-md-4 col-sm-6 col-xs-12">
											{!! Form::label('PAYMENT_METHOD', 'জামানতের মূল্য পরিশোধের ধরন: ') !!} <span style="color: red">*</span>
			                    		</div>
			                    		<div class="col-md-4 col-sm-6 col-xs-12">
				                        	{!! Form::select('PAYMENT_METHOD', [1=>'ব্যাংক পে-অর্ডার', 2=>'অনলাইন', 3=>'ক্যাশ', 4=>'পরে অর্থ প্রদান'], array(Input::old('PAYMENT_METHOD')), array('id'=>'payment_method', 'class'=>'form-control', 'placeholder' => trans('common.form_select'))) !!}
				                        	
				                        	<p class="validation_error">{{ $errors->first('PAYMENT_METHOD') }}</p>
				                        </div>
			                    	</div>
                                 	
                                 	@if( empty($errors->first('B_DRAFT_NO')) && empty($errors->first('BANK_ID')) )
                                 		<div class="form-group bank_pay_order">
                                 	@else
										<div class="form-group">
                                 	@endif
				                        <div class="col-md-4 col-sm-4 col-xs-12">
				                        	{!! Form::label('B_DRAFT_NO', 'পে-অর্ডার নাম্বার:') !!} <span style="color: red">*</span>
				                        	<p><small>পে-অর্ডার নাম্বার লিখুন </small></p>
				                        	{!! Form::text('B_DRAFT_NO', Input::old('B_DRAFT_NO'), array('class'=>'form-control', 'placeholder'=>'পে-অর্ডার নাম্বার লিখুন')) !!}
				                            
				                            <p class="validation_error">{{ $errors->first('B_DRAFT_NO') }}</p>
				                        </div>

				                       	<div class="col-md-4 col-sm-4 col-xs-12">
				                       		{!! Form::label('BANK_ID', 'পে-অর্ডার ব্যাংকের নাম:') !!} <span style="color: red">*</span>
				                        	<p><small>পে-অর্ডার ব্যাংকের নাম সিলেক্ট করুণ </small></p>
				                        	{!! Form::select('BANK_ID', $bank_list, array(Input::old('BANK_ID')), array('id'=>'bank_selection', 'class'=>'form-control ', 'placeholder' => trans('common.form_select'))) !!}
				                        	<p class="validation_error">{{ $errors->first('BANK_ID') }}</p>
				                       	</div>

				                       	<div class="col-md-4 col-sm-4 col-xs-12">
				                       		{!! Form::label('BRANCH_ID', 'ব্যাংকের শাখার নাম:') !!}
				                        	<p><small>ব্যাংকের শাখার নাম সিলেক্ট করুণ </small></p>
				                        	{!! Form::select('BRANCH_ID', [], array(Input::old('BRANCH_ID')), array('id'=>'branch_selection', 'class'=>'form-control ', 'placeholder' => trans('common.form_select'))) !!}
				                        	<p class="validation_error">{{ $errors->first('BRANCH_ID') }}</p>
				                       	</div>
				                    </div>
				                    @if( empty($errors->first('B_DRAFT_DATE')) && empty($errors->first('ATTACHMENT')) )
                                 		<div class="form-group bank_pay_order">
                                 	@else
										<div class="form-group">
                                 	@endif
				                    	<div class="col-md-6 col-sm-6 col-xs-12">
				                       		{!! Form::label('B_DRAFT_DATE', 'পে-অর্ডার তারিখ:') !!} <span style="color: red">*</span>
				                       		<p><small>পে-অর্ডার তারিখ সিলেক্ট করুণ</small> </p>
				                            <div class="input-group">
				                            	{!! Form::text('B_DRAFT_DATE', Input::old('B_DRAFT_DATE'), array('class'=>'form-control', 'placeholder'=>'Ex.'.date('d-m-Y'))) !!}
				                                <div class="input-group-addon">
				                                    <span class="glyphicon glyphicon-calendar"></span>
				                                </div>
				                            </div>
				                            <div id="error_msg1"></div>
				                            <p class="validation_error">{{ $errors->first('B_DRAFT_DATE') }}</p>
				                       	</div>
				                       	<div class="col-md-6 col-sm-6 col-xs-12">
				                       		{!! Form::label('ATTACHMENT', 'পে-অর্ডার সংযুক্তি:') !!} <span style="color: red">*</span>
				                       		<p><small>সংযুক্তির ধরন: jpg, jpeg, png</small></p>
				                            <div class="input-group">
				                            	{!! Form::file('ATTACHMENT') !!}
				                            </div>
				                            <p class="validation_error">{{ $errors->first('ATTACHMENT') }}</p>
				                       	</div>
				                    </div>
				                    @if( empty($errors->first('CASH_DATE')) && empty($errors->first('CASH_ATTACHMENT')) )
                                 		<div class="form-group cash_order">
                                 	@else
										<div class="form-group">
                                 	@endif
				                    	<div class="col-md-6 col-sm-6 col-xs-12">
				                       		{!! Form::label('CASH_DATE', 'তারিখ:') !!} <span style="color: red">*</span>
				                       		<p><small>ক্যাশ গ্রহণের তারিখ সিলেক্ট করুণ</small> </p>
				                            <div class="input-group">
				                            	{!! Form::text('CASH_DATE', Input::old('CASH_DATE'), array('class'=>'form-control', 'placeholder'=>'Ex.'.date('d-m-Y'))) !!}
				                                <div class="input-group-addon">
				                                    <span class="glyphicon glyphicon-calendar"></span>
				                                </div>
				                            </div>
				                            <div id="error_msg2"></div>
				                            <p class="validation_error">{{ $errors->first('CASH_DATE') }}</p>
				                       	</div>
				                       	<div class="col-md-6 col-sm-6 col-xs-12">
				                       		{!! Form::label('CASH_ATTACHMENT', 'সংযুক্তি:') !!} <span style="color: red">*</span>
				                       		<p><small>সংযুক্তির ধরন: jpg, jpeg, png</small></p>
				                            <div class="input-group">
				                            	{!! Form::file('CASH_ATTACHMENT') !!}
				                            </div>
				                            <p class="validation_error">{{ $errors->first('CASH_ATTACHMENT') }}</p>
				                       	</div>
				                    </div>--}}
				                    <div class="form-group">
				                    	
				                       	<div class="col-md-12 col-sm-12" style="margin-top:62px;">
				                           <button type="submit" class="btn btn-success btn-sm pull-right">দাখিল করুন</button>
				                           <button type="button" class="btn btn-warning btn-sm pull-right" id="reset_form">মুছে ফেলুন</button>
				                        </div>
				                    </div>
			                	</div>

			                {!! Form::close() !!}
			            </div>
						
						{!! $app_info !!}
					</div>

	        </div>
	    </div>
	</div>

@endsection

