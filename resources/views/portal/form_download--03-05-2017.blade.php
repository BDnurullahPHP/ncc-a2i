<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>দরপত্রের আবেদন পত্র</title>
    </head>
    <style>
        body {
            font-family: freeserif,sans-serif,vrinda,solaiman-lipi,monospace;
        }
        h1, h2, h3, h4, p, input, th, td{
            font-family: freeserif,sans-serif,vrinda,solaiman-lipi,monospace;
            font-weight: 400;
            white-space: initial;
        }
        .sectionrow
        {
            width: 100%;
            border: 1px ;            
            border-style: solid ;
            font-size: 12px; 
        }
        p.hr{
            width: 100px;
            display: inline-block;
            border-bottom: 1px dotted black;
        }
        h4{
            text-align:center; 
        }
        p{
            text-align: justify;
        }

    </style>
    <body>
        <h4>
            <bold>নারায়ণগঞ্জ সিটি করপোরেশন</bold>
            <br/>নগর ভবন, ১০ বঙ্গবন্ধু রোড
            <br/>নারায়ণগঞ্জ
            <br/>www.ncc.gov.bd
        </h4>
        <br><br>
        <table style="width: 100%;">
            <tr>
                @if($type=='lease')
                    <td style="width: 50%;">
                        @if( !empty($schedule_data->getProject) )
                            সায়রাত মহালের নাম: {{ $schedule_data->getProject->PR_NAME_BN }}
                        @endif
                    </td>
                @else
                    <td style="width: 50%;">
                        @if( !empty($schedule_data->getProject) )
                            প্রকল্পের নাম: {{ $schedule_data->getProject->PR_NAME_BN }}
                        @endif
                    </td>
                @endif
            </tr>
            <tr>
                @if($type=='tender')
                    <td style="width: 50%;">
                        @if( !empty($schedule_data) && !empty($schedule_data->getProjectDetail) )
                            <p>পরিমাপ: {{ $schedule_data->getProjectDetail->PR_MEASURMENT }} বর্গফুট</p>
                        @endif
                        @if( !empty($schedule_data) && !empty($schedule_data->getProjectType) )
                            <p>ধরণ: {{ $schedule_data->getProjectType->TYPE_NAME }}</p>
                        @endif
                        @if( !empty($schedule_data) && !empty($schedule_data->getProjectDetail) )
                            <p>অবস্থান: {{ Helpers::numberConvert($schedule_data->getProjectDetail->POSITION) }} তলা</p>
                        @endif
                        @if( !empty($schedule_data) && !empty($schedule_data->getProjectDetail) )
                            <p>নম্বর: {{ $schedule_data->getProjectDetail->PR_SSF_NO }}</p>
                        @endif
                    </td>
                @endif
                <td style="width: 50%;">
                    @foreach($tender_particular as $key => $tp)
                        <p>
                            @if($tp->PARTICULAR_ID == 5)
                                প্রতি বর্গফুটের 
                            @endif
                            
                            @if($tp->PARTICULAR_ID == 4 || $tp->PARTICULAR_ID == 7)
                                {{ $tp->getParticular->PARTICULAR_NAME }}&#40;প্রতিটি&#41;&#40;অফেরতযোগ্য&#41;: 
                            @elseif($tp->PARTICULAR_ID == 3 || $tp->PARTICULAR_ID == 6)
                                {{ $tp->getParticular->PARTICULAR_NAME }}&#40;দাখিলকৃত দরের&#41;: 
                            @else
                                {{ $tp->getParticular->PARTICULAR_NAME }}: 
                            @endif

                            @if($tp->UOM == 1)
                                {{ Helpers::numberConvert($tp->PARTICULAR_AMT) }} শতাংশ
                            @else
                                {{ Helpers::numberConvert(number_format($tp->PARTICULAR_AMT, 2)) }} টাকা
                            @endif
                        </p>
                    @endforeach  
                </td>
            </tr>
        </table>
        <br><br>
        <h3 style="margin-bottom: 5px;">আবেদনকারীর তথ্য</h3>
        <table style="width: 100%;">
            <tr>
                <td style="width: 50%;padding-top: 15px;">
                    <p>নামের প্রথম অংশ: ..............................................................</p>
                </td>
                <td style="width: 50%;padding-top: 15px;">
                    <p>নামের শেষের অংশ: ..............................................................</p>
                </td>
            </tr>
            <tr>
                <td style="width: 50%;padding-top: 15px;">
                    <p>পিতা/স্বামী: .....................................................................</p>
                </td>
                <td style="width: 50%;padding-top: 15px;">
                    <p>বিভাগ: ..............................................................................</p>
                </td>
            </tr>
            <tr>
                <td style="width: 50%;padding-top: 15px;">
                    <p>জেলা: ...........................................................................</p>
                </td>
                <td style="width: 50%;padding-top: 15px;">
                    <p>উপজেলা/থানা: ....................................................................</p>
                </td>
            </tr>
            <tr>
                <td style="width: 50%;padding-top: 15px;">
                    <p>রোড নং: .......................................................................</p>
                </td>
                <td style="width: 50%;padding-top: 15px;">
                    <p>হোল্ডিং নং: .........................................................................</p>
                </td>
            </tr>
            <tr>
                <td style="width: 50%;padding-top: 15px;">
                    <p>মোবাইল নাম্বার: ...............................................................</p>
                </td>
                <td style="width: 50%;padding-top: 15px;">
                    <p>জাতীয় পরিচয়পত্র / জন্মনিবন্ধন: ...............................................</p>
                </td>
            </tr>
            <tr>
                <td style="width: 50%;padding-top: 15px;">
                    <p>ইমেইল: .........................................................................</p>
                </td>
                <td style="width: 50%;padding-top: 15px;">
                    <p>পাসওয়ার্ড: .........................................................................</p>
                </td>
            </tr>
        </table>
        <br>
        <h3 style="margin-bottom: 5px;">দরপত্রের বিবরণ</h3>
        <table style="width: 100%;">
            <tr>
                <td style="width: 50%;padding-top: 15px;">
                    <p>দরদাতা কতৃক প্রদত্ত দর(অংকে লিখুন): ...................................</p>
                </td>
                <td style="width: 50%;padding-top: 15px;">
                    <p>দরদাতা কতৃক প্রদত্ত দর(কথায় লিখুন): .......................................</p>
                </td>
            </tr>
            <tr>
                <td style="width: 50%;padding-top: 15px;">
                    <p>জামানতের পরিমান(অংকে লিখুন): .........................................</p>
                </td>
                <td style="width: 50%;padding-top: 15px;">
                    <p>জামানতের পরিমান(কথায় লিখুন): .............................................</p>
                </td>
            </tr>
        </table>
        <br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
        @if(!empty($conditions))
            <h3 align="center">{{ $conditions->getCategory->CATE_NAME }} এর শর্ত সমূহ</h3>
            {!! $conditions->CON_DESC !!}
        @endif
    </body>
</html>