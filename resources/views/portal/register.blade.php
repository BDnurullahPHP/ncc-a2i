@extends('portal.layout.base')

@section('style')

	<style type="text/css">
		.control-label{
	        text-align: left !important;
	    }
	    .form-control{
	        height: 34px !important;
	        border-radius: 0px;
	    }
	    .input-group-addon{
	        border-radius: 0px;
	    }
	    textarea#address{
	        width: 100%;
	        max-width: 100%;
	        height: auto !important;
	    }
	    .btn{
	        border-radius: 0px;
	    }
	    #reset_form{
	        margin-right: 10px;
	    }
	</style>

@endsection

@section('js_file')

	<script src="{{ Helpers::asset('assets/portal/js/register.js') }}" type="text/javascript"></script>

@endsection

@section('script')

	<script type="text/javascript">
	    $(document).ready(function() {
	        Register.init();
	    });
	</script>

@endsection

@section('content')

	<div class="row">
	    <div class="col-md-12 col-sm-12 col-xs-12">
	        <div class="contact-page-content">
	            <div class="contact-heading">
	                <h3>নিবন্ধন করুন</h3>
	                <!--========Session message=======-->
					@if (Session::has('success'))
	                    <div class="alert alert-success">
	                      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	                      {{ Session::get('success') }}
	                    </div>
	                @endif
	                @if (Session::has('error'))
	                    <div class="alert alert-danger">
	                      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	                      {{ Session::get('error') }}
	                    </div>
	                @endif
	                @if (count($errors) > 0)
	                    @foreach ($errors->all() as $error)
	                      <div class="alert alert-danger">
	                          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	                          {{ $error }}
	                      </div>
	                    @endforeach
	                @endif
					<!--======end Session message=====-->
	            </div>
	            <div class="contact-form clearfix">
	            	<div class="row">
	            		<div class="col-md-8 col-sm-10 col-xs-12">
			            	{!! Form::open(array('url' => '#', 'method' => 'post', 'id'=>'reg_form', 'class' =>'form-horizontal')) !!}
								
			                    <div class="form-group">
			                        <div class="col-md-4 control-label">
			                        	{!! Form::label('name', 'নাম:') !!}
			                            <p>আপনার নাম এখানে লিখুন</p>
			                        </div>
			                        <div class="col-sm-7">
			                        	{!! Form::text('name', Input::old('name'), array('class'=>'form-control', 'placeholder'=>'আপনার নাম')) !!}
			                        </div>
			                    </div>
			                    <div class="form-group">
			                        <div class="col-md-4 control-label">
			                        	{!! Form::label('mobile', 'মোবাইল নাম্বার:') !!}
			                            <p>আপনার মোবাইল নাম্বার এখানে লিখুন</p>
			                        </div>
			                        <div class="col-sm-7">
			                        	{!! Form::text('mobile', Input::old('mobile'), array('class'=>'form-control', 'placeholder'=>'আপনার মোবাইল নাম্বার')) !!}
			                        </div>
			                    </div>
			                    <div class="form-group">
			                        <div class="col-md-4 control-label">
			                        	{!! Form::label('email', 'ইমেইল:') !!}
			                            <p>আপনার ইমেইল এখানে লিখুন</p>
			                        </div>
			                        <div class="col-sm-7">
			                        	{!! Form::email('email', Input::old('email'), array('class'=>'form-control', 'placeholder'=>'আপনার ইমেইল')) !!}
			                        </div>
			                    </div>
			                    <div class="form-group">
			                        <div class="col-md-4 control-label">
			                        	{!! Form::label('password', 'পাসওয়ার্ড:') !!}
			                            <p>আপনার পাসওয়ার্ড এখানে লিখুন</p>
			                        </div>
			                        <div class="col-sm-7">
			                        	{!! Form::password('password', array('class'=>'form-control', 'placeholder'=>'আপনার পাসওয়ার্ড')) !!}
			                        </div>
			                    </div>
			                    <div class="form-group">
			                        <div class="col-md-4 control-label">
			                        	{!! Form::label('repassword', 'আপনার পাসওয়ার্ড নিশ্চিত করুন:') !!}
			                            <p>আপনার পাসওয়ার্ড নিশ্চিত করুন</p>
			                        </div>
			                        <div class="col-sm-7">
			                        	{!! Form::password('repassword', array('class'=>'form-control', 'placeholder'=>'পুনরায় আপনার পাসওয়ার্ড লিখুন')) !!}
			                        </div>
			                    </div>
			                    <div class="form-group">
			                        <div class="col-md-4 control-label">
			                        	{!! Form::label('dob', 'জন্ম তারিখ:') !!}
			                            <p>আপনার জন্ম তারিখ নির্বাচন করুন</p>
			                        </div>
			                        <div class="col-sm-7">
			                            <div class="input-group">
			                            	{!! Form::text('dob', Input::old('dob'), array('class'=>'form-control', 'placeholder'=>'আপনার জন্ম তারিখ')) !!}
			                                <div class="input-group-addon">
			                                    <span class="glyphicon glyphicon-calendar"></span>
			                                </div>
			                            </div>
			                            <div id="error_msg1" class="placement_error"></div>
			                        </div>
			                    </div>
			                    <div class="form-group">
			                        <div class="col-md-4 control-label">
			                        	{!! Form::label('', 'আমি একজন:') !!}
			                            <p>আপনার লিঙ্গ নির্বাচন করুন</p>
			                        </div>
			                        <div class="col-sm-7">
			                            <label class="radio-inline">
			                            	{!! Form::radio('gender', 'male', false, array('class'=>'iech')) !!} পুরুষ
			                            </label>
			                            <label  class="radio-inline">
			                            	{!! Form::radio('gender', 'female', false, array('class'=>'iech')) !!} মহিলা
			                            </label>
			                            <div id="error_msg2" class="placement_error"></div>
			                        </div>
			                    </div>
			                    <div class="form-group">
			                        <div class="col-md-4 control-label">
			                        	{!! Form::label('nid', 'জাতীয় পরিচয়পত্র:') !!}
			                            <p>আপনার জাতীয় পরিচয়পত্রের নাম্বার এখানে লিখুন</p>
			                        </div>
			                        <div class="col-sm-7">
			                        	{!! Form::text('nid', Input::old('nid'), array('class'=>'form-control', 'placeholder'=>'আপনার জাতীয় পরিচয়পত্র')) !!}
			                        </div>
			                    </div>
			                    <div class="form-group">
			                        <div class="col-md-4 control-label">
			                        	{!! Form::label('address', 'ঠিকানা:') !!}
			                            <p>আপনার ঠিকানা এখানে লিখুন</p>
			                        </div>
			                        <div class="col-sm-7">
			                        	{!! Form::textarea('address', null, array('rows' =>'3', 'class'=>'form-control', 'placeholder'=>'আপনার ঠিকানা')) !!}
			                        </div>
			                    </div>
			                    <div class="form-group" style="margin-top: 25px;">
			                        <div class="col-md-8 col-md-offset-4">
			                            <button type="button" class="btn btn-warning btn-sm" id="reset_form">মুছে ফেলুন</button>
			                            <button type="submit" class="btn btn-success btn-sm">দাখিল করুন</button>
			                        </div>
			                    </div>

			                {!! Form::close() !!}
			            </div>
			        </div>
	            </div>
	        </div>
	    </div>
	</div>

@endsection