<!--=============Title for modal===============-->
<div class="modal_top_title">{{ trans('tender.add_data') }}</div>
<!--===========End title for modal=============-->

<div id="form_wizard_1" class="form_wizard_edit">
    <div class="form-wizard">
        <div class="form-body">

            @include('admin_access.tender.wizard_header')

            <div class="tab-content">
                <!--========Show form post message=============-->
                <div class="row">
                    <div class="col-md-6 col-sm-6 form_messsage">

                    </div>
                </div>
                <!--========End form post message=============-->
                <div class="tab-pane active" id="tab1">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="portlet box green <?php if( isset($project_cat) && in_array($category->PR_CATEGORY, $project_cat, true) ) echo "selected" ?>">
                                <input type="hidden" class="tender_id" value="{{ $tender_id }}">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-shopping-bag"></i>
                                       {{ $category->CATE_NAME }} {{ trans('common.lease_information') }}
                                    </div>
                                    <div class="tools">
                                        <a href="#" class="collapse" title=""></a>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <table class="table table-striped table-bordered table-hover" id="schedul_data">
                                        <thead>
                                            <tr>
                                                <th class="project_input">
                                                    <input type="checkbox" class="check_all">
                                                </th>
                                                <th>{{ trans('lease.mahal_number') }}</th>
                                                <th>{{ trans('lease.mahal_bn_name') }}</th>
                                                <th>{{ trans('lease.mahal_address') }}</th>
                                                @foreach ($cat_particular as $key => $cp)
                                                    <th style="width: 15%;" class="schedule_input text-center">
                                                        {{ $cp->getParticular->PARTICULAR_NAME }} <span class="required_field">*</span><br>
                                                        @if($cp->UOM == 1)
                                                            &#40;প্রস্তাবিত মূল্যের&#41;<br>
                                                            &#40;{{ trans('common.percent') }}&#41;
                                                        @elseif($cp->UOM == 2)
                                                            &#40;অফেরতযোগ্য&#41;<br>
                                                            &#40;{{ trans('common.taka') }}&#41;
                                                        @endif
                                                    </th>
                                                @endforeach
                                            </tr>
                                        </thead>
                                        <tbody>

                                        @foreach($project_list as $key => $row)
                                            <tr <?php if( isset($sc_project) && in_array($row->PROJECT_ID, $sc_project, true) ) echo 'class="selected"' ?> >
                                            @if(isset($tender_schedule))
                                                <input type="hidden" value="{{ Helpers::array_key_match($row->PROJECT_ID, $tender_schedule) }}" class="schedule_id">
                                            @endif
                                                <td class="project_input">
                                                    <input type="hidden" class="project_cat" value="{{ $category->PR_CATEGORY }}">
                                                    @if( isset($sc_project) && in_array($row->PROJECT_ID, $sc_project, true) )
                                                        <input type="checkbox" class="project_id remove_schedule" value="{{ $row->PROJECT_ID }}" data-url="{{ route('lease_tender_schedule_delete', $row->PROJECT_ID) }}" checked="checked">
                                                    @else
                                                        <input type="checkbox" class="project_id" value="{{ $row->PROJECT_ID }}">
                                                    @endif
                                                </td>
                                                <td class="data_td td_pr_ssf_no">{{ $row->PR_UD_ID }}</td>
                                                <td class="data_td td_pr_name">{{ $row->PR_NAME_BN }}</td>
                                                <td class="data_td td_position">{!! $row->PROJECT_LOCATION !!}</td>
                                                @foreach ($cat_particular as $k => $cp)
                                                    @if(isset($tendr_part))
                                                        <?php 
                                                            $partcular_arr = Helpers::array_key_match($row->PROJECT_ID, $tendr_part);
                                                        ?>
                                                    @endif
                                                    @if($cp->UOM == 1)
                                                        <td class="data_td schedule_input">
                                                            <input type="hidden" class="particular" value="{{ $cp->PARTICULAR_ID }}">
                                                            <input type="hidden" class="uom" value="{{ $cp->UOM }}">
                                                            @if(isset($tendr_part) && isset($partcular_arr) && isset($partcular_arr[$k]))
                                                                <input type="hidden" value="{{ $partcular_arr[$k] }}" class="particular_id">
                                                            @endif
                                                            {!! Form::text('te_particular_'.$cp->PARTICULAR_ID.$key.$k, $cp->PARTICULAR_AMT, array('class'=>'form-control te_particular schedule-required')) !!}
                                                        </td>
                                                    @elseif($cp->UOM == 2)
                                                        <td class="data_td schedule_input">
                                                            <input type="hidden" class="particular" value="{{ $cp->PARTICULAR_ID }}">
                                                            <input type="hidden" class="uom" value="{{ $cp->UOM }}">
                                                            @if(isset($tendr_part) && isset($partcular_arr) && isset($partcular_arr[$k]))
                                                                <input type="hidden" value="{{ $partcular_arr[$k] }}" class="particular_id">
                                                            @endif
                                                            {!! Form::text('te_particular_'.$cp->PARTICULAR_ID.$key.$k, $cp->PARTICULAR_AMT, array('class'=>'form-control te_particular schedule-required')) !!}
                                                        </td>
                                                    @endif
                                                @endforeach
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row tab-scroll margin-top-20" id="content_body">

                </div>
            </div>
            <div class="tab-pane tab-scroll row" id="tab2">
                @include('admin_access.tender_lease.edit_tender_fields')
            </div>
            <div class="tab-pane tab-scroll row" id="tab3"></div>
            <div class="tab-pane tab-scroll row" id="tab4"></div>
            <div class="tab-pane tab-scroll row" id="tab5">
                @include('admin_access.tender_lease.tender_confirm')
            </div>
        </div>
    </div>
    <div class="form-actions margin-top-20">
        <div class="row">
            <div class="col-md-12">
                <div class="button_container pull-right">
                    <a href="javascript:;" class="btn default button-previous">
                        <i class="fa fa-angle-left"></i> {{ trans('common.previous') }}
                    </a>
                    <a href="javascript:;" class="btn btn-outline green button-next">
                        {{ trans('common.next') }} <i class="fa fa-angle-right"></i>
                    </a>
                    <a href="javascript:;" class="btn green button-submit">
                        {{ trans('common.form_submit') }} <i class="fa fa-check"></i>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
</div>