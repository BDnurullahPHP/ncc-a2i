<div class="tender_view">

    <!--=============Title for modal===============-->
    <div class="modal_top_title">{{ trans('tender.tender_details_info') }}</div>
    <!--===========End title for modal=============-->
    <div class="row padding-bottom-10">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="row">
                {!! Form::label('', trans('tender.tender_name'), array('class'=>'col-sm-3')) !!}
                <div class="col-md-9 col-sm-9">
                    <p>{{ $tender->TENDER_TITLE }}</p>
                </div>
            </div>
        </div>
    </div>
    <div class="row padding-bottom-10">
        <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="row">
                {!! Form::label('', trans('tender.tender_number'), array('class'=>'col-sm-6')) !!}
                <div class="col-md-6 col-sm-6">
                    <p>{{ $tender->TENDER_NO }}</p>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="row">
                {!! Form::label('', trans('tender.tender_date'), array('class'=>'col-sm-6')) !!}
                <div class="col-md-6 col-sm-6">
                    <p>{{ date('d/m/Y', strtotime($tender->TENDER_DT)) }}</p>
                </div>
            </div>
        </div>
    </div>
    <div class="row padding-bottom-10">
        <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="row">
                {!! Form::label('', trans('tender.tender_publish_date'), array('class'=>'col-sm-6')) !!}
                <div class="col-md-6 col-sm-6">
                    <p>{{ date('d/m/Y', strtotime($tender->TENDER_PUBLISH_DT)) }}</p>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="row">
                {!! Form::label('', trans('tender.tender_method'), array('class'=>'col-sm-6')) !!}
                <div class="col-md-6 col-sm-6">
                    <p>@if ($tender->TENDER_METHOD === 1) OTM @else 0 @endif</p>
                </div>
            </div>
        </div>
    </div>
    <div class="row padding-bottom-10">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="row">
                {!! Form::label('', trans('tender.tender_location'), array('class'=>'col-sm-3')) !!}
                <div class="col-md-9 col-sm-9">
                    <ul style="padding-left:18px;">
                        @foreach($lease_location as $key => $loc)
                        <li>{{ $loc->getLocation->LOCATION_NAME }}</li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="row padding-bottom-10">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="row">
                {!! Form::label('', trans('অনুলিপি প্রেরনের স্থান'), array('class'=>'col-sm-3')) !!}
                <div class="col-md-9 col-sm-9">
                    <ul style="padding-left:18px;">
                        @foreach($onulipy_location as $key => $loc)
                        <li>{{ $loc->getLocation->LOCATION_NAME }}</li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="row" id="pro_dtls_sc_overview">
        <div class="col-md-12 col-sm-12 col-xs-12">
            @if(count($tender_date))
            <table class="table table-striped table-bordered table-hover">
                <thead>
                    <tr>
                        <th>{{ trans('tender.tender_article_no') }}</th>
                        <th>{{ trans('tender.sc_last_selling_date') }}</th>
                        <th>{{ trans('tender.sc_last_selling_time') }}</th>
                        <th>{{ trans('tender.te_last_receive_date') }}</th>
                        <th>{{ trans('tender.te_opening_dt') }}</th>
                    </tr>
                </thead>
                <tbody>

                    @foreach($tender_date as $key => $td)
                    <tr>
                        <td>{{ $td->CLAUSE }}</td>
                        <td>{{ date('d/m/Y', strtotime($td->LAST_SELLING_DT_FROM)) }}</td>
                        <td>{{ date('h:m A', strtotime($td->LAST_SELLING_DT_FROM)).' - '.date('h:m A', strtotime($td->LAST_SELLING_DT_TO)) }}</td>
                        <td>{{ date('d/m/Y h:m A', strtotime($td->TE_LAST_RECIEVE_DT)) }}</td>
                        <td>{{ date('d/m/Y h:m A', strtotime($td->TE_OPENING_DT)) }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            @endif
        </div>
        <div class="col-md-12 col-sm-12 col-xs-12">
            <h4>{{ $project_category->getCategory->CATE_NAME }} {{ trans('common._information') }}</h4>
            <table class="table table-striped table-bordered table-hover">
                <thead>
                    <tr>
                        <th>{{ trans('project.project_number') }}</th>
                        <th>{{ trans('project.lease_mohal') }}</th>
                        <th>{{ trans('project.project_location') }}</th>
                        @foreach($particular as $key => $part)
                        <th>
                            {{ $part->getParticular->PARTICULAR_NAME }}<br>
                            @if($part->UOM == 1)
                            &#40;প্রস্তাবিত মূল্যের&#41;<br>
                            &#40;{{ trans('common.percent') }}&#41;
                            @elseif($part->UOM == 2)
                            &#40;অফেরতযোগ্য&#41;<br>
                            &#40;{{ trans('common.taka') }}&#41;
                            @endif
                        </th>
                        @endforeach
                    </tr>
                </thead>
                <tbody>
                    @foreach($tender_schedule as $index => $ts)
                    <tr>
                        <td>{{ $ts->getProject->PR_UD_ID }}</td>
                        <td>{{ $ts->getProject->PR_NAME_BN }}</td>
                        <td>{!! $ts->getProject->PROJECT_LOCATION !!}</td>
                        @foreach($ts->getTenderParticulars as $inx => $tp)
                        <td>{{ number_format($tp->PARTICULAR_AMT) }}</td>
                        @endforeach
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        @if(!empty($tender->TENDER_DESC))
            <div class="col-md-12">
                <div class="portlet-body" style="display: block;">
                    <h4>{{ trans('tender.tender_details') }}</h4>
                    {!! $tender->TENDER_DESC !!}
                </div>
            </div>
        @endif
        @if(count($conditions))
        <div class="col-md-12">
            <div class="portlet-body" style="display: block;">
                <h4>{{ $conditions->getCategory->CATE_NAME }} এর শর্ত সমূহ</h4>
                {!! $conditions->CON_DESC !!}
            </div>
        </div>
        @endif
    </div>
</div>