{!! Form::model($tender_data, array('url' => '#', 'id'=>'tender_form', 'class'=>'form-horizontal', 'method' => 'post')) !!}
    
    <div class="form-group no-margin">
        <div class="col-sm-6">
            {!! Form::label('TENDER_NO', trans('tender.sharok_no')) !!} <span class="required_field">*</span>
            {!! Form::text('TENDER_NO', null, array('id'=>'tender_no', 'class'=>'form-control tender-required', 'placeholder'=>trans('tender.tender_number_pl'))) !!}
        </div>
        <div class="col-sm-6">
            {!! Form::label('TENDER_DT', trans('tender.tender_date')) !!} <span class="required_field">*</span>
            {!! Form::text('TENDER_DT', date('d-m-Y', strtotime($tender_data->TENDER_DT)), array('id'=>'tender_dt', 'class'=>'form-control tender-required date_picker_default', 'placeholder'=>trans('common.date_pl'))) !!}
        </div>
    </div>
    <div class="form-group no-margin">
        <div class="col-sm-6">
            {!! Form::label('TENDER_TITLE', trans('tender.tender_name')) !!} <span class="required_field">*</span>
            {!! Form::text('TENDER_TITLE', null, array('id'=>'tender_title', 'class'=>'form-control tender-required', 'placeholder'=>trans('tender.tender_name'))) !!}
        </div>
        <div class="col-sm-6">
            {!! Form::label('TENDER_METHOD', trans('tender.tender_method')) !!} <span class="required_field">*</span>
            {!! Form::select('TENDER_METHOD', [1=>'Open Tender Method(OTM)'], null, array('id'=>'tender_method', 'class'=>'form-control tender-required', 'placeholder' => trans('common.form_select'))) !!}
        </div>
    </div>
    <div class="form-group no-margin">
        <div class="col-sm-4">
            {!! Form::label('TENDER_PUBLISH_DT', trans('tender.tender_publish_date')) !!} <span class="required_field">*</span>
            {!! Form::text('TENDER_PUBLISH_DT', date('d-m-Y', strtotime($tender_data->TENDER_PUBLISH_DT)), array('id'=>'tender_publish_dt', 'class'=>'form-control date_picker_default tender-required', 'placeholder'=>trans('common.date_pl'))) !!}
        </div>
        <div class="col-sm-4">
            <label for="TENDER_LOCATION">{{ trans('tender.tender_location') }}</label> <span class="required_field">*</span>
            <select name="TENDER_LOCATION" id="tender_location" class="form-control multi_select tender-required tender_location" multiple="multiple" data-placeholder="{{ trans('common.form_select') }}">
                @foreach($location as $key => $lc)
                    @if( in_array($lc->LOCATION_ID, $te_location, true) )
                        <option selected="selected" data-location="{{ $lc->getTenderLocation->TE_LOC_ID }}" value="{{ $lc->LOCATION_ID }}">{{ $lc->LOCATION_NAME }}</option>
                    @else
                        <option value="{{ $lc->LOCATION_ID }}">{{ $lc->LOCATION_NAME }}</option>
                    @endif
                    
                @endforeach
            </select>
            <div id="error_msg1"></div>
        </div>
        <div class="col-sm-4">
            <label for="TENDER_COPY_LOCATION">{{ trans('tender.tender_copy_location') }}</label> <span class="required_field">*</span>
            <select name="TENDER_COPY_LOCATION" id="tender_copy_location" class="form-control multi_select tender-required tender_copy_location" multiple="multiple" data-placeholder="{{ trans('common.form_select') }}">
                @foreach($location as $key => $lc)
                    @if( in_array($lc->LOCATION_ID, $te_copy_location, true) )
                        <option selected="selected" data-location="{{ $lc->getTenderLocation->TE_LOC_ID }}" value="{{ $lc->LOCATION_ID }}">{{ $lc->LOCATION_NAME }}</option>
                    @else
                        <option value="{{ $lc->LOCATION_ID }}">{{ $lc->LOCATION_NAME }}</option>
                    @endif
                    
                @endforeach
            </select>
            <div id="error_msg2"></div>
        </div>
    </div>
    <!--==============================================-->
    <div class="form-group no-margin tender_article_set">
        @if(count($tender_date) > 0)
            @foreach($tender_date as $key => $te_date)
                <div class="row no-margin section_box_gray tender_dt_single_row">
                    <input type="hidden" value="{{ $te_date->TENDER_DT_ID }}" class="date_id">
                    <div class="row no-margin">
                        <div class="col-sm-1 text-center tender_article_no">
                            {!! Form::label('', trans('tender.tender_article_no')) !!}
                            <p>{{ $te_date->CLAUSE }}</p>
                        </div>
                        <div class="col-sm-5 te_s_dt">
                            {!! Form::label('', trans('tender.sc_last_selling_date')) !!} <span class="required_field">*</span>
                            @if($tender_edit)
                                {!! Form::text('tender_last_seling_dt_'.$key, date('d-m-Y', strtotime($te_date->LAST_SELLING_DT_FROM)), array('class'=>'form-control tender_seling_range date_picker_default tender-required', 'placeholder'=>trans('common.date_pl'))) !!}
                            @else
                                {!! Form::text('tender_last_seling_dt_'.$key, date('d-m-Y', strtotime($te_date->LAST_SELLING_DT_FROM)), array('class'=>'form-control tender_seling_range date_picker_default tender-required', 'readonly'=>'readonly', 'placeholder'=>trans('common.date_pl'), 'style'=>'pointer-events:none;')) !!}
                            @endif
                        </div>
                        <div class="col-sm-6 text-center date_range_bx">
                            {!! Form::label('', trans('tender.sc_last_selling_time')) !!} <span class="required_field">*</span>
                            <div class="row">
                                <div class="col-sm-5">
                                    @if($tender_edit)
                                        {!! Form::text('tender_last_seling_time_from_'.$key, date('h:m A', strtotime($te_date->LAST_SELLING_DT_FROM)), array('class'=>'form-control tender_last_seling_time_from time_picker tender-required', 'placeholder'=>trans('common.time_pl'))) !!}
                                    @else
                                        {!! Form::text('tender_last_seling_time_from_'.$key, date('h:m A', strtotime($te_date->LAST_SELLING_DT_FROM)), array('class'=>'form-control tender_last_seling_time_from time_picker tender-required', 'readonly'=>'readonly', 'placeholder'=>trans('common.time_pl'), 'style'=>'pointer-events:none;')) !!}
                                    @endif
                                </div>
                                <div class="col-sm-2">{{ trans('common.to') }}</div>
                                <div class="col-sm-5">
                                    @if($tender_edit)
                                        {!! Form::text('tender_last_seling_time_to_'.$key, date('h:m A', strtotime($te_date->LAST_SELLING_DT_TO)), array('class'=>'form-control tender_last_seling_time_to time_picker tender-required', 'placeholder'=>trans('common.time_pl'))) !!}
                                    @else
                                        {!! Form::text('tender_last_seling_time_to_'.$key, date('h:m A', strtotime($te_date->LAST_SELLING_DT_TO)), array('class'=>'form-control tender_last_seling_time_to time_picker tender-required', 'readonly'=>'readonly', 'placeholder'=>trans('common.time_pl'), 'style'=>'pointer-events:none;')) !!}
                                    @endif
                                </div>
                            </div>
                        </div>
                        {{-- <div class="col-sm-2">
                            <span title="Remove" class="delete_clause" data-url="{{ route('lease_tender_date_delete', $te_date->TENDER_DT_ID) }}"><i class="fa fa-times"></i></span>
                        </div> --}}
                    </div>
                    <div class="row no-margin">
                        <div class="col-sm-5 col-sm-offset-1 te_lst_receive_dt">
                            {!! Form::label('', trans('tender.te_last_receive_date')) !!} <span class="required_field">*</span>
                            @if($tender_edit)
                                {!! Form::text('tender_last_receive_dt_'.$key, date('d-m-Y h:m A', strtotime($te_date->TE_LAST_RECIEVE_DT)), array('class'=>'form-control tender_last_receive_dt date_time_picker tender-required', 'placeholder'=>trans('common.date_time_pl'))) !!}
                            @else
                                {!! Form::text('tender_last_receive_dt_'.$key, date('d-m-Y h:m A', strtotime($te_date->TE_LAST_RECIEVE_DT)), array('class'=>'form-control tender_last_receive_dt date_time_picker tender-required', 'readonly'=>'readonly', 'placeholder'=>trans('common.date_time_pl'), 'style'=>'pointer-events:none;')) !!}
                            @endif
                        </div>
                        <div class="col-sm-6 te_opn_dt">
                            {!! Form::label('', trans('tender.te_opening_dt')) !!} <span class="required_field">*</span>
                            @if($tender_edit)
                                {!! Form::text('tender_opening_dt_'.$key, date('d-m-Y h:m A', strtotime($te_date->TE_OPENING_DT)), array('class'=>'form-control tender_opening_dt date_time_picker tender-required', 'placeholder'=>trans('common.date_time_pl'))) !!}
                            @else
                                {!! Form::text('tender_opening_dt_'.$key, date('d-m-Y h:m A', strtotime($te_date->TE_OPENING_DT)), array('class'=>'form-control tender_opening_dt date_time_picker tender-required', 'readonly'=>'readonly', 'placeholder'=>trans('common.date_time_pl'), 'style'=>'pointer-events:none;')) !!}
                            @endif
                        </div>
                    </div>
                </div>
            @endforeach
        @endif
        @if(!$tender_edit)
            <div class="row no-margin section_box_gray tender_dt_single_row">
                <div class="row no-margin">
                    <div class="col-sm-1 text-center tender_article_no">
                        {!! Form::label('', trans('tender.tender_article_no')) !!}
                        <p>{{ !empty($te_max_clause) ? $te_max_clause+1 : '1' }}</p>
                    </div>
                    <div class="col-sm-5 te_s_dt">
                        {!! Form::label('', trans('tender.sc_last_selling_date')) !!} <span class="required_field">*</span>
                        <input type="text" name="tender_last_seling_dt" class="form-control tender_seling_range date_picker tender-required" placeholder="{{ trans('common.date_pl') }}">
                    </div>
                    <div class="col-sm-6 text-center date_range_bx">
                        {!! Form::label('', trans('tender.sc_last_selling_time')) !!} <span class="required_field">*</span>
                        <div class="row">
                            <div class="col-sm-5">
                                <input type="text" name="tender_last_seling_time_from" class="form-control tender_last_seling_time_from time_picker tender-required" placeholder="{{ trans('common.time_pl') }}">
                            </div>
                            <div class="col-sm-2">{{ trans('common.to') }}</div>
                            <div class="col-sm-5">
                                <input type="text" name="tender_last_seling_time_to" class="form-control tender_last_seling_time_to time_picker tender-required" placeholder="{{ trans('common.time_pl') }}">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row no-margin">
                    <div class="col-sm-5 col-sm-offset-1 te_lst_receive_dt">
                        {!! Form::label('', trans('tender.te_last_receive_date')) !!} <span class="required_field">*</span>
                        <input type="text" name="tender_last_receive_dt" class="form-control tender_last_receive_dt date_time_picker tender-required" placeholder="{{ trans('common.date_time_pl') }}">
                    </div>
                    <div class="col-sm-6 te_opn_dt">
                        {!! Form::label('', trans('tender.te_opening_dt')) !!} <span class="required_field">*</span>
                        <input type="text" name="tender_opening_dt" class="form-control tender_opening_dt date_time_picker  tender-required" placeholder="{{ trans('common.date_time_pl') }}">
                    </div>
                    <!-- <div class="col-sm-2 padding-top-25">
                        <button type="button" class="btn green btn-sm date_add_more btn_margin"> <i class="fa fa-plus"></i> {{ trans('common.add_more') }}</button>
                    </div> -->
                </div>
            </div>
        @endif
    </div>
    <!--==============================================-->
    <div class="form-group no-margin">
        <div class="col-sm-12">
            {!! Form::label('TENDER_DESC', trans('tender.tender_details')) !!}
            {!! Form::textarea('TENDER_DESC', null, array('rows' =>'5', 'id'=>'tender_desc', 'class'=>'form-control reactor_basic', 'placeholder'=>trans('tender.tender_details'))) !!}
        </div>
    </div>

{!! Form::close() !!}