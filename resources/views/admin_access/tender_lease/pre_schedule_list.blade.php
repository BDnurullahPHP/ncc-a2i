@foreach($project_list as $key => $row)
<tr>
    <td class="project_input">
        <input type="hidden" class="project_cat" value="{{ $category->PR_CATEGORY }}">
        <input type="checkbox" class="project_id" value="{{ $row->PROJECT_ID }}">
    </td>
    <td class="data_td td_pr_ssf_no">{{ !empty($tender_id) ? $row->getProject->PR_UD_ID : $row->PR_UD_ID }}</td>
    <td class="data_td td_pr_name">{{ !empty($tender_id) ? $row->getProject->PR_NAME_BN : $row->PR_NAME_BN }}</td>
    <td class="data_td td_position">{!! !empty($tender_id) ? $row->getProject->PROJECT_LOCATION : $row->PROJECT_LOCATION !!}</td>
    @foreach ($cat_particular as $k => $cp)
    @if($cp->UOM == 1)
    <td class="data_td schedule_input">
        <input type="hidden" class="particular" value="{{ $cp->PARTICULAR_ID }}">
        <input type="hidden" class="uom" value="{{ $cp->UOM }}">
        {!! Form::text('te_particular_'.$cp->PARTICULAR_ID.$key.$k, $cp->PARTICULAR_AMT, array('class'=>'form-control te_particular schedule-required')) !!}
    </td>
    @elseif($cp->UOM == 2)
    <td class="data_td td_tender_price schedule_input">
        <input type="hidden" class="particular" value="{{ $cp->PARTICULAR_ID }}">
        <input type="hidden" class="uom" value="{{ $cp->UOM }}">
        {!! Form::text('te_particular_'.$cp->PARTICULAR_ID.$key.$k, $cp->PARTICULAR_AMT, array('class'=>'form-control te_particular schedule-required')) !!}
    </td>
    @endif
    @endforeach
</tr>
@endforeach