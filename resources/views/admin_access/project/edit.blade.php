{!! Form::model($project, array('url' => '#', 'id'=>'project_form', 'class'=>'form-horizontal', 'method' => 'post')) !!}
    
    <!--=============Title for modal===============-->
    <div class="modal_top_title">{{ trans('project.edit_data') }}</div>
    <!--===========End title for modal=============-->

    <!--========Show form post message=============-->
    <div class="form-group">
        <div class="col-sm-6 form_messsage">
            
        </div>
    </div>
    <!--========End form post message=============-->
    
    
    <div class="portlet box yellow">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-tasks"></i> {{ trans('project.project') }} {{ trans('common.create') }}</div>
            <div class="tools">
                <a href="#" class="collapse" title=""> </a>
            </div>
        </div>
        <div class="portlet-body">
            <div class="form-group">
                <div class="col-md-4 col-sm-4 col-xs-6">
                    {!! Form::label('area', trans('land.land_zone')) !!}
                    {!! Form::select('area', $area, array(Input::old('area')), array('class'=>'form-control', 'placeholder' => trans('common.form_select'))) !!}
                </div>
                <div class="col-md-4 col-sm-4 col-xs-6">
                    {!! Form::label('ward', trans('land.land_ward')) !!}
                    {!! Form::select('ward', [], array(Input::old('ward')), array('class'=>'form-control', 'placeholder' => trans('common.form_select'))) !!}
                </div>
                <div class="col-md-4 col-sm-4 col-xs-6">
                    {!! Form::label('mouza', trans('land.land_form_mouza')) !!}
                    {!! Form::select('mouza', [], array(Input::old('mouza')), array('class'=>'form-control', 'placeholder' => trans('common.form_select'))) !!}
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-3">
                    {!! Form::label('LAND_ID', trans('project.project_land')) !!} <span class="required_field">*</span>
                    <select class="form-control multi_select form-required" name="LAND_ID" id="project_lands" multiple="multiple">
                        @foreach ($land as $key => $value)
                            <option value="{{ $value->LAND_ID }}" {{ in_array($value->LAND_ID, (array)$project_land) ? 'selected="selected"' : '' }} >{{ $value->LAND_NUMBER }}</option>
                        @endforeach
                    </select>
                    <div id="error_msg1"></div>
                </div>
                <div class="col-sm-3">
                    {!! Form::label('PR_UD_ID', trans('project.project_number')) !!} <span class="required_field">*</span>
                    {!! Form::text('PR_UD_ID', null, array('id'=>'project_number', 'class'=>'form-control form-required', 'placeholder'=>'Ex. padma-30/07/2016')) !!}
                </div>
                <div class="col-sm-3">
                    {!! Form::label('PR_START_DT', trans('project.project_create_date')) !!} <span class="required_field">*</span>
                    {!! Form::text('PR_START_DT', date('d-m-Y', strtotime($project->PR_START_DT)), array('id'=>'project_start_date', 'class'=>'form-control date_picker_default form-required', 'placeholder'=>trans('common.date_pl'))) !!}
                </div>
                <div class="col-sm-3">
                    {!! Form::label('PR_END_DT', trans('project.project_end_date')) !!} <span class="required_field">*</span>
                    {!! Form::text('PR_END_DT', date('d-m-Y', strtotime($project->PR_END_DT)), array('id'=>'project_end_date', 'class'=>'form-control date_picker_default form-required', 'placeholder'=>trans('common.date_pl'))) !!}
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-6">
                    {!! Form::label('PR_NAME', trans('project.project_name')) !!} <span class="required_field">*</span>
                    {!! Form::text('PR_NAME', null, array('id'=>'project_name', 'class'=>'form-control form-required', 'placeholder'=>trans('project.project_name_pl'))) !!}
                </div>
                <div class="col-sm-6">
                    {!! Form::label('PR_NAME_BN', trans('project.project_bn_name')) !!} <span class="required_field">*</span>
                    {!! Form::text('PR_NAME_BN', null, array('id'=>'project_name_bn', 'class'=>'form-control form-required', 'placeholder'=>trans('project.project_bn_name_pl'))) !!}
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-6">
                    {!! Form::label('PR_DESC', trans('project.project_details')) !!} <span class="required_field">*</span>
                    {!! Form::textarea('PR_DESC', null, array('id'=>'project_details', 'rows' =>'3', 'class'=>'form-control reactor_basic form-required', 'placeholder'=>trans('project.project_details'))) !!}
                </div>
                <div class="col-sm-6">
                    {!! Form::label('PR_LOCATION', trans('project.project_location')) !!} <span class="required_field">*</span>
                    {!! Form::textarea('PR_LOCATION', $project->PROJECT_LOCATION, array('id'=>'project_location', 'rows' =>'5', 'class'=>'form-control form-required', 'placeholder'=>trans('project.project_location'))) !!}
                </div>
            </div>
            <div class="form-group" id="project_files">
                <div class="col-md-12">
                    {!! Form::label('', trans('common.attachment')) !!}
                </div>
                <div class="col-sm-4 attachment_single" style="display: none !important;">
                    {!! Form::file('attachment', array('class'=>'form-control form-attachment-update project_file')) !!}
                </div>
                @if(count($project->getDocuments) > 0)
                    @foreach($project->getDocuments as $key => $doc)
                        @if( !empty($doc->ATTACHMENT) && File::exists(public_path()."/upload/project/$project->ATTACHMENT") )
                            <div class="col-md-4">
                                <p class="sketch_preview">
                                    <a target="_blank" href='{{ Helpers::asset("upload/project/$doc->ATTACHMENT") }}'>{{ $doc->ATTACHMENT }}</a>
                                    <a href="{{ route('project_file_delete', $doc->PR_DOC_ID) }}" title="Remove file" class="remove_project_file">
                                        <i class="fa fa-times"></i>
                                    </a>
                                </p>
                            </div>
                        @else
                            <div class="col-sm-4 attachment_single">
                                {!! Form::file('attachment', array('class'=>'form-control form-attachment-update project_file')) !!}
                            </div>
                        @endif
                    @endforeach
                @else

                @endif
                <div class="col-md-2">
                    <button type="button" class="btn green btn-sm project_add_more btn_margin"> <i class="fa fa-plus"></i> {{ trans('common.add_more') }}</button>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-6">
                    {!! Form::label('is_active', trans('common.is_active')) !!}<br/>
                    @if($project->IS_ACTIVE == 1)
                        {!! Form::checkbox('is_active', 1, true, array('id'=>'project_is_active')) !!}
                    @else
                        {!! Form::checkbox('is_active', 1, false, array('id'=>'project_is_active')) !!}
                    @endif
                </div>
            </div>
        </div>
    </div>

    @foreach ($type_category as $key => $tcat)
        @if($key==0)
            <div class="portlet box green">
        @elseif($key==1)
            <div class="portlet box blue">
        @elseif($key==2)
            <div class="portlet box purple">
        @else
            <div class="portlet box green">
        @endif
            <div class="portlet-title">
                <div class="caption">
                    @if($key==0)
                        <i class="fa fa-shopping-bag"></i>
                    @elseif($key==1)
                        <i class="fa fa-home"></i>
                    @elseif($key==2)
                        <i class="fa fa-building"></i>
                    @else
                        <i class="fa fa-building"></i>
                    @endif
                    {{ $tcat->CATE_NAME }} {{ trans('common.create') }}</div>
                <div class="tools">
                    <a href="#" class="expand" title=""> </a>
                </div>
            </div>
            @if($key==0)
                <div class="portlet-body" id="project_shops" style="display: none;">
            @elseif($key==1)
                <div class="portlet-body" id="project_flats" style="display: none;">
            @elseif($key==2)
                <div class="portlet-body" id="project_spaces" style="display: none;">
            @else
                <div class="portlet-body" style="display: none;">
            @endif
                <div class="form-group">
                    <?php 
                        $project_details = DB::table('project_details')
                                                ->where('PROJECT_ID', $project->PROJECT_ID)
                                                ->where('PR_CATEGORY', $tcat->PR_CATEGORY)
                                                ->get();
                    ?>
                    <div class="col-md-11 col-sm-11 col-xs-11 pr_details_field">
                        @if(count($project_details) > 0)
                            @foreach($project_details as $index => $pr_dtls)
                                <ul class="item_single_list">
                                    <input type="hidden" class="pr_category" value="{{ $tcat->PR_CATEGORY }}">
                                    <input type="hidden" class="pr_details_id" value="{{ $pr_dtls->PR_DETAIL_ID }}">
                                    <li>
                                        {!! Form::label('POSITION_'.$index.$pr_dtls->PR_DETAIL_ID, trans('project.project_details_position')) !!} <span class="help_tooltip" data-toggle="tooltip" data-placement="right" title="{{ trans('project.project_position_help') }}"><i class="fa fa-question-circle" aria-hidden="true"></i></span>
                                        {!! Form::select('POSITION_'.$index.$pr_dtls->PR_DETAIL_ID, $project_floor, $pr_dtls->POSITION, array('id'=>'POSITION_'.$index.$pr_dtls->PR_DETAIL_ID, 'class'=>'form-control pr_position', 'placeholder' => trans('common.form_select'))) !!}
                                    </li>
                                    <li>
                                        {!! Form::label('PR_TYPE_'.$index.$pr_dtls->PR_DETAIL_ID, trans('project.project_details_type')) !!} <span class="help_tooltip" data-toggle="tooltip" data-placement="right" title="{{ trans('project.project_details_type_pl') }}"><i class="fa fa-question-circle" aria-hidden="true"></i></span>
                                        {!! Form::select('PR_TYPE_'.$index.$pr_dtls->PR_DETAIL_ID, $project_type, $pr_dtls->PR_TYPE, array('id'=>'PR_TYPE_'.$index.$pr_dtls->PR_DETAIL_ID, 'class'=>'form-control pr_type', 'placeholder' => trans('common.form_select'))) !!}
                                    </li>
                                    <li>
                                        {!! Form::label('PR_SSF_NO_'.$index.$pr_dtls->PR_DETAIL_ID, trans('common.number')) !!}
                                        {!! Form::text('PR_SSF_NO_'.$index.$pr_dtls->PR_DETAIL_ID, $pr_dtls->PR_SSF_NO, array('id'=>'PR_SSF_NO_'.$index.$pr_dtls->PR_DETAIL_ID, 'class'=>'form-control pr_ssf_no', 'placeholder'=>trans('project.project_pr_ssf_no_pl'))) !!}
                                    </li>
                                    <li>
                                        {!! Form::label('PR_MEASURMENT_'.$index.$pr_dtls->PR_DETAIL_ID, trans('project.project_details_size')) !!}
                                        {!! Form::text('PR_MEASURMENT_'.$index.$pr_dtls->PR_DETAIL_ID, $pr_dtls->PR_MEASURMENT, array('id'=>'PR_MEASURMENT_'.$index.$pr_dtls->PR_DETAIL_ID, 'class'=>'form-control pr_measurement', 'placeholder'=>trans('project.project_details_size_pl'))) !!}
                                    </li>
                                    <li>
                                        {!! Form::label('UOM_'.$index.$pr_dtls->PR_DETAIL_ID, trans('common.unit')) !!}
                                        {!! Form::select('UOM_'.$index.$pr_dtls->PR_DETAIL_ID, [1=>'বর্গ ফুট'], $pr_dtls->UOM, array('id'=>'UOM_'.$index.$pr_dtls->PR_DETAIL_ID, 'class'=>'form-control pr_uom', 'placeholder' => trans('common.form_select'))) !!}
                                    </li>
                                    <li class="list_sm_block">
                                        {!! Form::label('IS_RESERVED_'.$index.$pr_dtls->PR_DETAIL_ID, trans('project.is_reserved')) !!}<br>
                                        @if($pr_dtls->IS_RESERVED == 1)
                                            {!! Form::checkbox('IS_RESERVED_'.$index.$pr_dtls->PR_DETAIL_ID, 1, true, ['class'=>'status pr_is_reserved']) !!}
                                        @else
                                            {!! Form::checkbox('IS_RESERVED_'.$index.$pr_dtls->PR_DETAIL_ID, 1, false, ['class'=>'status pr_is_reserved']) !!}
                                        @endif
                                    </li>
                                    <li class="list_sm_block">
                                        {!! Form::label('IS_BUILT_'.$index.$pr_dtls->PR_DETAIL_ID, trans('project.is_build')) !!}<br>
                                        @if($pr_dtls->IS_BUILT == 1)
                                            {!! Form::checkbox('IS_BUILT_'.$index.$pr_dtls->PR_DETAIL_ID, 1, true, ['class'=>'status pr_is_build']) !!}
                                        @else
                                            {!! Form::checkbox('IS_BUILT_'.$index.$pr_dtls->PR_DETAIL_ID, 1, false, ['class'=>'status pr_is_build']) !!}
                                        @endif
                                    </li>
                                    <li class="list_sm_block">
                                        {!! Form::label('IS_ACTIVE_'.$index.$pr_dtls->PR_DETAIL_ID, trans('common.is_active')) !!}<br>
                                        @if($pr_dtls->IS_ACTIVE == 1)
                                            {!! Form::checkbox('IS_ACTIVE_'.$index.$pr_dtls->PR_DETAIL_ID, 1, true, ['class'=>'status pr_is_active']) !!}
                                        @else
                                            {!! Form::checkbox('IS_ACTIVE_'.$index.$pr_dtls->PR_DETAIL_ID, 1, false, ['class'=>'status pr_is_active']) !!}
                                        @endif
                                    </li>
                                    <li class="remove_item_record" data-url="{{ route('project_details_delete', $pr_dtls->PR_DETAIL_ID) }}"><i class="fa fa-times"></i></li>
                                </ul>
                            @endforeach
                        @endif
                        <ul class="item_single_list" style="display:none;">
                            <input type="hidden" class="pr_category" value="{{ $tcat->PR_CATEGORY }}">
                            <li>
                                {!! Form::label('POSITION_'.$key, trans('project.project_details_position')) !!} <span class="help_tooltip" data-toggle="tooltip" data-placement="right" title="{{ trans('project.project_position_help') }}"><i class="fa fa-question-circle" aria-hidden="true"></i></span>
                                {!! Form::select('POSITION_'.$key, [1=>'First floor',2=>'Second floor', 3=>'Third floor'], null, array('id'=>'POSITION_'.$key, 'class'=>'form-control pr_position', 'placeholder' => trans('common.form_select'))) !!}
                            </li>
                            <li>
                                {!! Form::label('PR_TYPE_'.$key, trans('project.project_details_type')) !!} <span class="help_tooltip" data-toggle="tooltip" data-placement="right" title="{{ trans('project.project_details_type_pl') }}"><i class="fa fa-question-circle" aria-hidden="true"></i></span>
                                {!! Form::select('PR_TYPE_'.$key, $project_type, null, array('id'=>'PR_TYPE_'.$key, 'class'=>'form-control pr_type', 'placeholder' => trans('common.form_select'))) !!}
                            </li>
                            <li>
                                {!! Form::label('PR_SSF_NO_'.$key, trans('common.number')) !!}
                                {!! Form::text('PR_SSF_NO_'.$key, null, array('id'=>'PR_SSF_NO_'.$key, 'class'=>'form-control pr_ssf_no', 'placeholder'=>trans('project.project_pr_ssf_no_pl'))) !!}
                            </li>
                            <li>
                                {!! Form::label('PR_MEASURMENT_'.$key, trans('project.project_details_size')) !!}
                                {!! Form::text('PR_MEASURMENT_'.$key, null, array('id'=>'PR_MEASURMENT_'.$key, 'class'=>'form-control pr_measurement', 'placeholder'=>trans('project.project_details_size_pl'))) !!}
                            </li>
                            <li>
                                {!! Form::label('UOM_'.$key, trans('common.unit')) !!}
                                {!! Form::select('UOM_'.$key, [1=>'বর্গ ফুট'], null, array('id'=>'UOM_'.$key, 'class'=>'form-control pr_uom', 'placeholder' => trans('common.form_select'))) !!}
                            </li>
                            <li class="list_sm_block">
                                {!! Form::label('IS_RESERVED_'.$key, trans('project.is_reserved')) !!}<br>
                                {!! Form::checkbox('IS_RESERVED_'.$key, '1', false, ['class'=>'status pr_is_reserved']) !!}
                            </li>
                            <li class="list_sm_block">
                                {!! Form::label('IS_BUILT_'.$key, trans('project.is_build')) !!}<br>
                                {!! Form::checkbox('IS_BUILT_'.$key, '1', true, ['class'=>'status pr_is_build']) !!}
                            </li>
                            <li class="list_sm_block">
                                {!! Form::label('IS_ACTIVE_'.$key, trans('common.is_active')) !!}<br>
                                {!! Form::checkbox('IS_ACTIVE_'.$key, '1', true, ['class'=>'status pr_is_active']) !!}
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-1 col-sm-1 col-xs-1">
                        <button type="button" class="btn green btn-sm pr_add_more"><i class="fa fa-plus"></i> {{ trans('common.add_more') }}</button>
                    </div>
                </div>
            </div>
        </div>
    @endforeach

    <div class="form-group">
        <div class="col-md-12" style="padding-top: 15px;">
            <span class="modal_msg pull-left"></span>
                <button type="submit" data-action="{{ route('update_project', $project->PROJECT_ID) }}" class="btn btn-success btn-sm pull-right" id="project_submit" style="margin-left: 5px;">{{ trans('common.form_submit') }}</button>
                <button type="button" class="btn btn-default btn-sm reset_form pull-right">{{ trans('common.form_reset') }}</button>
            <span class="loadingImg"></span>
        </div>
    </div>

{!! Form::close() !!}