@extends('admin_access.layout.master')

@section('style')
	<style type="text/css">
	    .radio-inline .radio{
	        padding-top: 2px;
	    }
	    .ui-datepicker{
	        z-index: 99999 !important;
	    }
	    .help-block{
	        color: red;
	    }
	    input[type=file]{
	        border: 1px solid #C2CAD8;
	    }
	    .modal-footer{
	    	display: none;
	    }
	</style>
@endsection

@section('content')

	<h3>{{ trans('project.project_information') }}
    	<small></small>
    	<a href="#" data-toggle="modal" data-target="#modal_add_content" class="btn btn-primary btn-sm pull-right" data-form="{{ route('project_general_form') }}">{{ trans('common.add_new') }} <i class="fa fa-plus"></i></a>
	</h3>
	<div class="portlet light bordered">
	    <!-- <div class="portlet-title">
	        <div class="caption">
	            
	        </div>
	        <div class="tools"> </div>
	    </div> -->
	    <div class="row">
	    	<div class="col-md-5 col-sm-6 col-xs-12">
	    		<div id="lg_form_message"></div>
	    	</div>
	    </div>
	    <div class="portlet-body contentArea">
	        
			<table class="table table-striped table-bordered table-hover" data-source="{{ route('land_data') }}" id="common_table">
			    <thead>
			        <tr>
			            <th>{{ trans('common.table_sl') }}</th>
			            <th>{{ trans('project.project_number') }}</th>
			            <th>{{ trans('project.project_name') }}</th>
			            <th>{{ trans('project.project_details') }}</th>
			            <th>{{ trans('common.table_head_created_at') }}</th>
			            <th>{{ trans('common.attachment') }}</th>
			            <th>{{ trans('common.table_action') }}</th>
			        </tr>
			    </thead>
			    <tbody>
			    	
			    </tbody>
			</table>

	    </div>
	</div>

@endsection

@section('script')
	
	<script src="{{ Helpers::asset('assets/admin/js/project.js') }}" type="text/javascript"></script>
	<script type="text/javascript">
	    $(document).ready(function() {
	        projectManaged.init();
	    });
</script>


@endsection