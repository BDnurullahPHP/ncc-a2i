{!! Form::open(array('url' => '#', 'id'=>'project_form', 'class'=>'form-horizontal', 'method' => 'post')) !!}
    
    <!--=============Title for modal===============-->
    <div class="modal_top_title">{{ trans('project.add_data') }}</div>
    <!--===========End title for modal=============-->

    <!--========Show form post message=============-->
    <div class="form-group">
        <div class="col-sm-5 col-sm-offset-3 form_messsage">
            
        </div>
    </div>
    <!--========End form post message=============-->

    <div class="form-group">
        <div class="col-sm-6">
            {!! Form::label('LAND_ID', trans('project.project_land')) !!}
            {!! Form::select('LAND_ID', [1=>'শিমরাইল/1/1', 2=>'শিমরাইল/1/2'], null, array('class'=>'form-control multi_select form-required', 'multiple'=>'multiple', 'data-placeholder' => trans('common.form_select'))) !!}
        </div>
        <div class="col-sm-6">
            {!! Form::label('PR_UD_ID', trans('project.project_number')) !!}
            {!! Form::text('PR_UD_ID', null, array('class'=>'form-control form-required', 'placeholder'=>'Ex. padma-30/07/2016')) !!}
        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-6">
            {!! Form::label('PR_NAME', trans('project.project_name')) !!}
            {!! Form::text('PR_NAME', null, array('class'=>'form-control', 'placeholder'=>trans('project.project_name_pl'))) !!}
        </div>
        <div class="col-sm-6">
            {!! Form::label('PR_NAME_BN', trans('project.project_bn_name')) !!}
            {!! Form::text('PR_NAME_BN', null, array('class'=>'form-control form-required', 'placeholder'=>trans('project.project_bn_name_pl'))) !!}
        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-6">
            {!! Form::label('PR_START_DT', trans('project.project_create_date')) !!}
            {!! Form::text('PR_START_DT', null, array('class'=>'form-control date_picker form-required', 'placeholder'=>trans('common.date_pl'))) !!}
        </div>
        <div class="col-sm-6">
            {!! Form::label('PR_END_DT', trans('project.project_end_date')) !!}
            {!! Form::text('PR_END_DT', null, array('class'=>'form-control date_picker form-required', 'placeholder'=>trans('common.date_pl'))) !!}
        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-12">
            {!! Form::label('PR_DESC', trans('project.project_details')) !!}
            {!! Form::textarea('PR_DESC', null, array('rows' =>'3', 'class'=>'form-control reactor_basic form-required', 'placeholder'=>trans('project.project_details'))) !!}
        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-6">
            {!! Form::label('PROJECT_SKETCH', trans('common.attachment')) !!}
            {!! Form::file('PROJECT_SKETCH', array('class'=>'form-control project_file')) !!}
        </div>
        <div class="col-sm-6">
            {!! Form::label('IS_ACTIVE', trans('common.is_active')) !!}
            <label class="control-label">
                {!! Form::checkbox('IS_ACTIVE', '1', true, ['class'=>'status']) !!}
            </label>
        </div>
    </div>
    
    <div class="form-group">
        <div class="col-md-12" style="padding-top: 15px;">
            <span class="modal_msg pull-left"></span>
                <button type="submit" data-action="{{ route('user_create') }}" class="btn btn-primary btn-sm pull-right" id="project_submit" style="margin-left: 5px;">{{ trans('common.form_submit') }}</button>
                <button type="button" class="btn btn-default btn-sm reset_form pull-right">{{ trans('common.form_reset') }}</button>
            <span class="loadingImg"></span>
        </div>
    </div>
{!! Form::close() !!}