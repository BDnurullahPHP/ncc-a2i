{!! Form::model($tender, array('url' => '#', 'id'=>'applicant_installment', 'class'=>'form-horizontal', 'method' => 'post')) !!}
    
    <!--=============Title for modal===============-->
    <div class="modal_top_title">{{ trans('common.add_data') }}</div>
    <!--===========End title for modal=============-->

    <!--========Show form post message=============-->
    <div class="form-group">
        <div class="col-sm-6 form_messsage" style="padding-top: 15px;">
            
        </div>
    </div>
    <!--========End form post message=============-->
    <div class="form-group" id="installment">
        <div class="col-md-12">
            <table class="table table-striped table-bordered table-hover" id="installment">
                <thead>
                    <tr>
                        <th>{{ trans('tender.installment_number') }}</th>
                        <th>{{trans('tender.installment_date')}}</th>
                        <th>{{trans('tender.installment_amount')}}</th>
                        <th>{{trans('tender.installment_penalty')}}</th>
                        <th>{{trans('common.comment')}}</th>
                    </tr>
                </thead>
                <tbody>
                     @foreach($installment as $key=>$value)
                     <tr>
                         <td>{{ $value->INSTALLMENT }} <input type="hidden" class="ins_id" name="tender_ins_id" value="{{ $value->TENDER_INS_ID }}"></td>
                         <td>{{ date('d-m-Y', strtotime($value->DATE)) }}</td>
                         <td>{{ $value->AMOUNT }}</td>
                         <td>{{ $value->PUNISHMENT }}</td>
                         <td><input type="text" class="form-control remarks" name="installment_remarks_{{ $value->TENDER_INS_ID }}" value="{{ $value->REMARKS }}" placeholder="মন্তব্য"></td>
                     </tr>
                    @endforeach
                </tbody>
        </table>
        </div>
    </div>
    <div class="form-group">
        <div class="col-md-12" style="padding-top: 15px;">
            <button style="margin-left: 5px;" type="submit" data-action="{{ route('lease_primary_applicant_update', $tender) }}" class="btn btn-success pull-right btn-sm tender_installment_save">{{ trans('common.form_submit') }}</button>
            <button type="button" class="btn btn-default pull-right btn-sm reset_form">{{ trans('common.form_reset') }}</button>
        </div>
    </div>

{!! Form::close() !!}