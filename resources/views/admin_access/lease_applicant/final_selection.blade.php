{!! Form::model($applicant, array('url' => '#', 'id'=>'final_letter', 'class'=>'form-horizontal', 'method' => 'post')) !!}

<input type="hidden" name="applicant_id" value="{{ $type == 'applicant' ? $applicant->TE_APP_ID : $applicant->CITIZEN_ID }}">
<input type="hidden" name="schedule_id" value="{{ $applicant->SCHEDULE_ID }}">
<input type="hidden" name="applicant_email" value="{{ $lease->EMAIL }}">
<!--=============Title for modal===============-->
<div class="modal_top_title">{{ trans('চূড়ান্তভাবে নির্বাচন এর তথ্য') }}</div>
<!--===========End title for modal=============-->

<!--========Show form post message=============-->
<div class="form-group">
    <div class="col-sm-6 form_messsage" style="padding-top: 15px;">

    </div>
</div>
<!--========End form post message=============-->
<div class="row">
    <div class="col-md-12">
        <div class="form-group">
            <div class="col-md-6">
                <div class="form-group">
                    <div class="col-md-4">
                        <label>স্মারক নং- </label><span class="required_field">*</span>
                    </div>
                    <div class="col-md-8">
                        <input type="text" class="form-control form-required" name="sharok_no" id="sharok_no_id" value="" placeholder="৪৬.৪৪৬৭০০.১৪.১৮(৮).১৬/"/>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <div class="col-md-4">
                        <label>স্মারকের তারিখঃ </label><span class="required_field">*</span>
                    </div>
                    <div class="col-md-6">
                        <input type="text" class="form-control form-required date_picker" name="sharok_date" id="sharok_date_id" value="{{ date('d-m-Y') }}" placeholder="Ex.20-07-2016">
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-12">
        <div class="form-group">
            <div class="col-md-2">
                <label>বিষয়ঃ  </label><span class="required_field">*</span>
            </div>
            <div class="col-md-8">
                <input type="text" class="form-control form-required" name="letter_subject" id="letter_subject_id" value="{{ $project_name }} ইজারা প্রদানের কার্যাদেশ পত্র" placeholder="ইজারা প্রদানের কার্যাদেশ পত্র" />
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="form-group">
            <?php
            $sum = 0;
            foreach ($schedule_particulars as $key => $value) {
                $sum+= $applicant->BID_AMOUNT * $value->PARTICULAR_AMT / 100;
            }
            ?>
            <textarea class="form-control reactor_basic form-required" name="letter_body" id="letter_body_id">
                উপর্যুক্ত বিষয়ে জানানো যাচ্ছে যে, নারায়ণগঞ্জ সিটি কর্পোরেশনের নিয়ন্ত্রণাধীন {{ $project_name }} {{ Helpers::numberConvert(date('d/m/Y', strtotime($validity->DATE_FROM))) }} তারিখ হতে {{ Helpers::numberConvert(date('d/m/Y', strtotime($validity->DATE_TO))) }} তারিখ পর্যন্ত ইজারা গ্রহণের জন্য আপনার প্রস্তাবিত দর {{ Helpers::numberConvert(number_format($applicant->BID_AMOUNT)) }}/- ({{ $applicant->BG_AMOUNT_TEXT }}) টাকা সর্বোচ্চ বিবেচিত হওয়ায় তা সিটি কর্পোরেশন কর্তৃক গৃহীত হয়। 

                        আপনি ইজারা মুলা মূল্য বাবদ {{ Helpers::numberConvert(number_format($applicant->BG_AMOUNT)) }}/- টাকা, @foreach ($schedule_particulars as $key => $value)
                                                                            {{ $value->UOM == 1 ? Helpers::numberConvert($value->PARTICULAR_AMT) .'%' : Helpers::numberConvert($value->PARTICULAR_AMT) }} {{ $value->getParticular->PARTICULAR_NAME }} বাবদ {{ Helpers::numberConvert(number_format($applicant->BID_AMOUNT * $value->PARTICULAR_AMT / 100)) }}/- টাকা 
                                                                            @endforeach
                        
                    সর্বমোট {{ Helpers::numberConvert(number_format($sum + $applicant->BG_AMOUNT)) }}/- টাকা পরিশোধ করায় {{ Helpers::numberConvert(date('d/m/Y', strtotime($validity->DATE_FROM))) }} তারিখ হতে {{ Helpers::numberConvert(date('d/m/Y', strtotime($validity->DATE_TO))) }} তারিখ পর্যন্ত বর্ণিত {{ $project_name }} হতে টোল আদায় করার কার্যাদেশ প্রদান করা হলো।
            </textarea>
        </div>
    </div>
    <div class="col-md-12">
        <div class="form-group">
            <textarea class="form-control reactor_basic form-required" name="letter_conditions" id="letter_conditions_id">
                শর্তাবলি ঃ
                ১। {{ $project_name }} পরিচালনার ক্ষেত্রে ইজারার বিজ্ঞপ্তির বর্ণিত শর্তাবলি প্রযোজ্য হবে।
                ২। শুধুমাত্র {{ $project_name }} হতে টোল আদায় করতে হবে।
                ৩। এই কার্যাদেশের মেয়াদ {{ Helpers::numberConvert(date('d/m/Y', strtotime($validity->DATE_FROM))) }} তারিখ হতে {{ Helpers::numberConvert(date('d/m/Y', strtotime($validity->DATE_TO))) }} তারিখ পর্যন্ত।
            </textarea>
        </div>
    </div>

    <div class="col-md-12">
        <div class="form-group">
            <div class="col-md-6 hidden">
                <textarea class="form-control reactor_basic" name="applicant_info" id="applicant_info_id">
                            জনাব {{ $lease->APPLICANT_NAME }}
                            পিতা/স্বামী- {{ $lease->SPOUSE_NAME }}
                            বাসা নং- {{ $lease->HOLDING_NO }}, রাস্তা- {{ $lease->ROAD_NO }}
                            থানা- {{ $applicent_thana }}, জেলা- {{ $applicent_district }}
                </textarea>
            </div>
            <div class="col-md-6">
                <p style="text-align: left">
                    জনাব {{ $lease->APPLICANT_NAME }}
                    <br/>পিতা/স্বামী- {{ $lease->SPOUSE_NAME }}
                    <br/>বাসা নং- {{ $lease->HOLDING_NO }}, রাস্তা- {{ $lease->ROAD_NO }}
                    <br/>থানা- {{ $applicent_thana }}, জেলা- {{ $applicent_district }}
                </p>
            </div>
            <div class="col-md-6">
                <textarea class="form-control reactor_basic form-required" name="officer_info" id="officer_info_id">
                            ({{ $officer->NAME }})
                            ({{ $officer->DESIGNATION }})
                            ({{ $officer->OFFICE }})
                            ({{ $officer->PHONE }})
                            ({{ $officer->EMAIL }})
                </textarea>
            </div>
        </div>
    </div>

    <div class="col-md-12">
        <div class="form-group">
            <textarea class="form-control reactor_basic form-required" name="distribution" id="distribution_id">
                        @foreach($location as $key=>$value)
                        {{ Helpers::numberConvert($key+1) }}। {{ $value->LOCATION_NAME }}<br>
                        @endforeach
            </textarea>
        </div>
    </div>
    <div class="form-group">
        <div class="col-md-12" style="padding-top: 15px; padding-right: 35px;">
            <button style="margin-left: 5px;" type="submit" data-action="{{ route('lease_final_select_insert', [$applicant->SCHEDULE_ID, $type, $type == 'applicant' ? $applicant->TE_APP_ID : $applicant->CITIZEN_ID]) }}" data-print="{{ route('lease_final_letter_print', [$applicant->SCHEDULE_ID, $type, $type == 'applicant' ? $applicant->TE_APP_ID : $applicant->CITIZEN_ID]) }}" class="btn btn-success pull-right btn-sm finally_save">{{ trans('common.form_submit') }}</button>
        </div>
    </div>
</div>
{!! Form::close() !!}
