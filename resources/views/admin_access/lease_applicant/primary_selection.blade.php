{!! Form::model($applicant, array('url' => '#', 'id'=>'lease_validity', 'class'=>'form-horizontal', 'method' => 'post')) !!}

<input type="hidden" name="applicant_id" value="{{ $type == 'applicant' ? $applicant->TE_APP_ID : $applicant->CITIZEN_ID }}">
<input type="hidden" name="schedule_id" value="{{ $applicant->SCHEDULE_ID }}">
<input type="hidden" name="applicant_email" value="{{ $lease->EMAIL }}">
<!--=============Title for modal===============-->
<div class="modal_top_title">{{ trans('tender.primary_select') }}</div>
<!--===========End title for modal=============-->

<!--========Show form post message=============-->
<div class="form-group">
    <div class="col-sm-6 form_messsage" style="padding-top: 15px;">

    </div>
</div>
<!--========End form post message=============-->
<div class="tabbable-custom">
    <div class="tab-content">
        <div class="tab-pane active" id="tab_1"> <!-- 1st panel-->
            <div class="row">
                <div class="col-md-6">
                    <h4>ইজারার বিবরণ</h4>
                    <table class="table table-striped table-bordered table-hover" id="installment">
                        <tbody>
                            <tr>
                                <th>মহালের নাম</th>
                                <td>{{ $project_name }}</td>
                            </tr>
                            <tr>
                                <th>দরপত্র মূল্য</th>
                                <td>{{ Helpers::en2bn(number_format($applicant->TE_SC_AMOUNT,2)) }}৳</td>
                            </tr>
                            <tr>
                                <th>দরদাতা কতৃক প্রদত্ত দর</th><td> {{ Helpers::en2bn(number_format($applicant->BID_AMOUNT,2)) }}৳</td>
                            </tr>
                            <tr>
                                <th>বায়নার পরিমান</th><td>{{ Helpers::en2bn(number_format($applicant->BG_AMOUNT,2)) }}৳</td>
                            </tr>
                            @foreach ($schedule_particulars as $key => $value)
                            <tr>
                                <th>
                                    {{ Helpers::en2bn($value->getParticular->PARTICULAR_NAME) }} {{ $value->UOM == 1 ? '('. Helpers::en2bn($value->PARTICULAR_AMT) .'%)' : '' }}
                                </th>
                                <td>
                                    {{ $total = $value->UOM == 1 ? Helpers::en2bn(number_format($applicant->BID_AMOUNT * $value->PARTICULAR_AMT / 100,2)).'৳' : Helpers::en2bn(number_format($value->PARTICULAR_AMT)).'৳' }}
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="col-md-6">
                    <h4>ইজারার টাকা পরিশোধের তথ্য</h4>
                    <table class="table table-striped table-bordered table-hover">
                        <tbody>
                            <tr>
                                <th>দরপত্র মূল্য</th>
                                <td>
                                    @if ($applicant->IS_TE_SC_BOUGHT == 0)
                                        <span class="label label-danger">বকেয়া</span>
                                    @elseif ($applicant->IS_TE_SC_BOUGHT == 1)
                                        <span class="label label-warning">গ্রহণের অপেক্ষা</span>
                                    @else
                                        <span class="label label-success">পরিশোধিত</span>
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <th>বায়নার পরিমান</th>
                                <td>
                                    @if ($applicant->BG_PAYMENT_STATUS == 0)
                                        <span class="label label-warning">গ্রহণের অপেক্ষায়</span>
                                    @else
                                        <span class="label label-success">পরিশোধিত</span>
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                @if ($applicant->BG_PAYMENT_TYPE == 1)
                                <td>
                                    
                                    <span>ব্যাংকের নাম : </span><span>{{ $applicant->getBank->BANK_NAME }}</span><br/>
                                    <span>বি,ডি/পে-অর্ডার নং : </span><span>{{ $applicant->B_DRAFT_NO }}</span><br/>
                                    <span>তারিখ : </span><span>{{ Helpers::en2bn(date('d/m/Y', strtotime($applicant->B_DRAFT_DATE))) }}</span><br/>
                                </td>
                                <td>
                                    <?php $taxfile = $applicant->B_DRAFT_ATTACHMENT; ?>
                                    @if(!empty($applicant->B_DRAFT_ATTACHMENT) && File::exists(public_path()."/upload/bank/$taxfile") )
                                        <a target="_blank" href='{{ Helpers::asset("upload/bank/$taxfile") }}'>
                                            <img border="1" src="{{ Helpers::asset("upload/bank/$taxfile") }}" width="50" height="30"> 
                                        </a>
                                    @endif
                                </td>
                                @endif
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <h3>ইজারার মেয়াদ</h3>
            <div class="form-group">
                <div class="col-md-2">
                    <label>শুরুর তারিখ</label> <span class="required_field">*</span>
                </div>
                <div class="col-md-3">
                    <input type="text" class="form-control form-required date_picker validDate" id="from_date_id" name="DATE_FROM" placeholder="Ex.20-07-2016">
                </div>
                <div class="col-md-2">
                    <label>শেষ তারিখ</label> <span class="required_field">*</span>
                </div>
                <div class="col-md-3">
                    <input type="text" class="form-control form-required date_picker validDate" id="to_date_id" name="DATE_TO" placeholder="Ex.20-07-2016">
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-12">
                    {!! Form::label('INITIAL_REMARKS', trans('common.comment')) !!}
                </div>
                <div class="col-md-12">
                    {!! Form::textarea('INITIAL_REMARKS', null, array('rows' =>'2', 'class'=>'form-control reactor_basic', 'placeholder'=>trans('common.your_comment'))) !!}
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-12" style="padding-top: 15px;">
                    <a href="#tab_2" style="margin-left: 5px;" class="btn btn-default btn-sm pull-right tab_next">{{ trans('common.next') }} <i class="fa fa-arrow-right"></i></a>
                </div>
            </div>
        </div>

        <div class="tab-pane" id="tab_2"><!--  2nd panel-->
            <br/>
            <div class="col-md-12">
                <div class="form-group">
                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="col-md-4">
                                <label>স্মারক নং- </label><span class="required_field">*</span>
                            </div>
                            <div class="col-md-8">
                                <input type="text" class="form-control form-required" name="sharok_no" id="sharok_no_id" value="" placeholder="৪৬.৪৪৬৭০০.১৪.১৮(৮).১৬/"/>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="col-md-4">
                                <label>স্মারকের তারিখঃ </label><span class="required_field">*</span>
                            </div>
                            <div class="col-md-6">
                                <input type="text" class="form-control form-required date_picker" name="sharok_date" id="sharok_date_id" value="{{ date('d-m-Y') }}" placeholder="Ex.20-07-2016">
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-12">
                <div class="form-group">
                    <div class="col-md-2">
                        <label>বিষয়ঃ  </label><span class="required_field">*</span>
                    </div>
                    <div class="col-md-8">
                        <input type="text" class="form-control form-required" name="letter_subject" id="letter_subject_id" value="ইজারার অবশিষ্ট টাকা পরিশোধ প্রসঙ্গে" placeholder="ইজারার অবশিষ্ট টাকা পরিশোধ প্রসঙ্গে" />
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group">
                    <div class="col-md-2">
                        <label>সুত্রঃ  </label><span class="required_field">*</span>
                    </div>
                    <div class="col-md-8">
                        <input type="hidden" class="form-control form-required" name="letter_sources" id="letter_sources_id" value="গত {{ date('d-m-Y') }} তারিখে আপনার দাখিলকৃত দরপত্র।"/>
                        <span>গত {{ Helpers::en2bn(date('d/m/Y')) }} তারিখে আপনার দাখিলকৃত দরপত্র।</span>
                    </div>
                </div>
            </div>

            <div class="col-md-12">
                <div class="form-group">
                    <?php
                        $sum = 0;
                        foreach($schedule_particulars as $key=>$value)
                        {
                           $sum+= $applicant->BID_AMOUNT * $value->PARTICULAR_AMT / 100;
                        }
                    ?>
                    <div id="text_area_content" class="hidden">
                        <p>উপর্যুক্ত বিষয়ে জানানো যাচ্ছে যে, নারায়ণগঞ্জ সিটি কর্পোরেশনাধীন {{ $project_name }} <span class="start_date"></span> তারিখ হতে <span class="end_date"></span> তারিখ পর্যন্ত ইজারা গ্রহণের জন্য আপনার প্রস্তাবিত দর {{ Helpers::en2bn(number_format($applicant->BID_AMOUNT)) }}/- টাকা সর্বোচ্চ বিবেচিত হওয়ায় তা গৃহিত হয়েছে। আপনি দরপত্রের সাথে পে-অর্ডার দ্বারা {{ Helpers::en2bn(number_format($applicant->BG_AMOUNT)) }}/- টাকা জমা প্রদান করেছেন।</p>
                        <p>এমতাবস্থায়, পত্র প্রাপ্তির ০৭ (সাত) কার্যদিবসের মধ্যে প্রস্তাবিত মূল্যের উপর
                            <span>
                                @foreach ($schedule_particulars as $key => $value)
                                    {{ $value->UOM == 1 ? Helpers::en2bn($value->PARTICULAR_AMT) .'%' : Helpers::en2bn($value->PARTICULAR_AMT) }} {{ $value->getParticular->PARTICULAR_NAME }} বাবদ {{ Helpers::en2bn(number_format($applicant->BID_AMOUNT * $value->PARTICULAR_AMT / 100)) }}/- টাকা 
                                @endforeach
                            </span>
                            সর্বমোট {{ Helpers::en2bn(number_format($sum)) }}/- টাকা সিটি কর্পোরেশন তহবিলে জমা প্রদান করতঃ সিটি কর্পোরেশনের সহিত ৩০০/-টাকা নন জুডিশিয়াল স্ট্যাম্পে চুক্তিপত্র সম্পাদানের জন্য অনুরোধ করা হলো।
                        </p>
                    </div>
                    <textarea class="form-control reactor_basic form-required" name="letter_body" id="letter_body_id">
                    </textarea>
                </div>
            </div>

            <div class="col-md-12">
                <div class="form-group">
                    <div class="col-md-6 hidden">
                        <textarea class="form-control reactor_basic" name="applicant_info" id="applicant_info_id">
                            জনাব {{ $lease->APPLICANT_NAME }}
                            পিতা/স্বামী- {{ $lease->SPOUSE_NAME }}
                            বাসা নং- {{ $lease->HOLDING_NO }}, রাস্তা- {{ $lease->ROAD_NO }}
                            থানা- {{ $applicent_thana }}, জেলা- {{ $applicent_district }}
                        </textarea>
                    </div>
                    <div class="col-md-6">
                        <p style="text-align: left">
                            জনাব {{ $lease->APPLICANT_NAME }}
                            <br/>পিতা/স্বামী- {{ $lease->SPOUSE_NAME }}
                            <br/>বাসা নং- {{ $lease->HOLDING_NO }}, রাস্তা- {{ $lease->ROAD_NO }}
                            <br/>থানা- {{ $applicent_thana }}, জেলা- {{ $applicent_district }}
                        </p>
                    </div>
                    <div class="col-md-6">
                        <textarea class="form-control reactor_basic form-required" name="officer_info" id="officer_info_id">
                            ({{ $officer->NAME }})
                            ({{ $officer->DESIGNATION }})
                            ({{ $officer->OFFICE }})
                            ({{ $officer->PHONE }})
                            ({{ $officer->EMAIL }})
                        </textarea>
                    </div>
                </div>
            </div>

            <div class="col-md-12">
                <div class="form-group">
                    <textarea class="form-control reactor_basic form-required" name="distribution" id="distribution_id">
                        @foreach($location as $key=>$value)
                        {{ $key+1 }}। {{ $value->LOCATION_NAME }}<br>
                        @endforeach
                    </textarea>
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-12" style="padding-top: 15px;">
                    <button style="margin-left: 5px;" type="submit" data-action="{{ route('lease_primary_select_insert', [$applicant->SCHEDULE_ID, $type, $type == 'applicant' ? $applicant->TE_APP_ID : $applicant->CITIZEN_ID]) }}" data-print="{{ route('lease_primary_letter_print', [$applicant->SCHEDULE_ID, $type, $type == 'applicant' ? $applicant->TE_APP_ID : $applicant->CITIZEN_ID]) }}" class="btn btn-success pull-right btn-sm lease_validity_save">{{ trans('common.form_submit') }}</button>
                    <a href="#tab_1" class="btn btn-default btn-sm pull-right tab_previous"><i class="fa fa-arrow-left"></i> {{ trans('common.previous') }}</a>
                </div>
            </div>
        </div>
    </div>
</div>
{!! Form::close() !!}
