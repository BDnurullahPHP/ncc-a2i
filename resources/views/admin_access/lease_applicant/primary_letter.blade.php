{!! Form::model($lease, array('url' => '#', 'id'=>'letter_form', 'class'=>'form-horizontal', 'method' => 'post')) !!}
<input type="hidden" name="te_app_id" value="{{ $lease->TE_APP_ID }}">
<input type="hidden" name="schedule_id" value="{{ $lease->SCHEDULE_ID }}">
<!--=============Title for modal===============-->
<div class="modal_top_title">{{ trans('common.add_data') }}</div>
<!--===========End title for modal=============-->

<!--========Show form post message=============-->
<div class="form-group">
    <div class="col-sm-6 form_messsage" style="padding-top: 15px;">

    </div>
</div>
<!--========End form post message=============-->
<div class="col-md-12">
    <div class="form-group">
        <div class="col-md-6">
            <div class="form-group">
                <div class="col-md-4">
                    <label>স্মারক নং- </label><span class="required_field">*</span>
                </div>
                <div class="col-md-8">
                    <input type="text" class="form-control form-required" name="sharok_no" id="sharok_no_id" value="{{ !empty($letter)?$letter->LATTER_NUMBER : '৪৬.৪৪৬৭০০.১৪.১৮(৮).১৬/' }}">
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <div class="col-md-4">
                    <label>তারিখঃ </label><span class="required_field">*</span>
                </div>
                <div class="col-md-8">
                    <input type="text" class="form-control form-required date_picker" name="sharok_date" id="sharok_date_id" value="{{ !empty($letter)? date('d-m-Y', strtotime($letter->DATE)) : date('d-m-Y') }}">
                </div>
            </div>
        </div>
    </div>
</div>

<div class="col-md-12">
    <div class="form-group">
        <div class="col-md-2">
            <label>বিষয়ঃ  </label><span class="required_field">*</span>
        </div>
        <div class="col-md-8">
            <input type="text" class="form-control form-required" name="letter_subject" value="{{ !empty($letter) ? $letter->SUBJECT : $project_name }}" id="letter_subject_id">
        </div>
    </div>
</div>

<div class="col-md-12">
    <div class="form-group">
        <?php
        $vat = $lease->BG_AMOUNT * 15 / 100;
        $aicor = $lease->BG_AMOUNT * 5 / 100;
        $total = $lease->BG_AMOUNT + $vat + $aicor;
        ?>
        <textarea class="form-control reactor_basic form-required" name="letter_body" id="letter_body_id">
            <?php 
                if (!empty($letter)){
                    echo $letter->BODY;
                }else {?>
                উপর্যুক্ত বিষয়ে জানানো যাচ্ছে যে, নারায়ণগঞ্জ সিটি কর্পোরেশনাধীন {{ $project_name }} ১৪২৩ বাংলা সনের ১ বৈশাখ হতে ৩০ চৈত্র পর্যন্ত এক বত্সর মেয়াদে ইজারা গ্রহণের জন্য আপনার প্রস্তাবিত দর {{ $lease->BID_AMOUNT }}/-টাকা সর্বোচ্চ বিবেচিত হওয়ায় তা গ্রেহিত হয়েছে। আপনি দরপত্রের সাথে পে-অর্ডার দ্বারা {{ $lease->BG_AMOUNT }} টাকা জমা প্রদান করেছেন।

                এমতাবস্তায়, পত্র প্রাপ্তির ০৭ (সাত) কার্যদিবসের মধ্যে প্রস্তাবিত মূল্য এর উপর ১৫% ভ্যাট বাবদ {{ $vat }}/-টাকা, ৫% আয়কর বাবদ {{ $aicor }}/-টাকা এবং ৫% জামানত বাবদ {{ $aicor }}/-টাকাসহ সর্বমোট {{ $total }} টাকা সিটি কর্পোরেশন তহবিলে জমা প্রদান করতঃ সিটি কর্পোরেশনের সহিত ৩০০/-টাকা নন জুডিশিয়াল স্ট্যাম্পে চুক্তিপত্র সম্পাদানের জন্য অনুরোধ করা হলো।
            <?php } ?>
        </textarea>
    </div>
</div>

<div class="col-md-12">
    <div class="form-group">
        <div class="col-md-6 hidden">
            <textarea class="form-control reactor_basic" name="applicant_info" id="applicant_info_id">
                        জনাব {{ $lease->APPLICANT_NAME }}
                        পিতা/স্বামী- {{ $lease->SPOUSE_NAME }}
                        বাসা নং- {{ $lease->SPOUSE_NAME }}, রাস্তা- {{ $lease->SPOUSE_NAME }}
                        থানা- {{ $applicent_thana }}, জেলা- {{ $applicent_district }}
            </textarea>
        </div>
        <div class="col-md-6">
            <p style="text-align: left">
                জনাব {{ $lease->APPLICANT_NAME }}
                <br/>পিতা/স্বামী- {{ $lease->SPOUSE_NAME }}
                <br/>বাসা নং- {{ $lease->SPOUSE_NAME }}, রাস্তা- {{ $lease->SPOUSE_NAME }}
                <br/>থানা- {{ $applicent_thana }}, জেলা- {{ $applicent_district }}
            </p>
        </div>
        <div class="col-md-6">
            <textarea class="form-control reactor_basic form-required" name="officer_info" id="officer_info_id">
                    ({{ $officer->NAME }})
                    ({{ $officer->DESIGNATION }})
                    ({{ $officer->OFFICE }})
                    ({{ $officer->PHONE }})
                    ({{ $officer->EMAIL }})
            </textarea>
        </div>
    </div>
</div>

<div class="col-md-12">
    <div class="form-group">
        <textarea class="form-control reactor_basic form-required" name="distribution" id="distribution_id">
                <?php
                  if (!empty($letter)){
                    echo $letter->DISTRIBUTION;
                  }else{?>
                    @foreach($location as $key=>$value)
                    {{ $key+1 }}। {{ $value->LOCATION_NAME }}<br>
                    @endforeach
                <?php }?>
        </textarea>
    </div>
</div>
<div class="form-group">
    <div class="col-md-12" style="padding-top: 15px;">
        <a href="{{ route('lease_primary_letter_print', $lease->TE_APP_ID) }}" target="_blank" class="btn btn-default pull-right"><i class="glyphicon glyphicon-print"></i></a> 
        <button style="margin-left: 5px;" type="submit" data-action="{{ route('lease_primary_letter_update', $lease->TE_APP_ID) }}" class="btn btn-success pull-right btn-sm form_submit">{{ trans('common.form_submit') }}</button>
        <button type="button" class="btn btn-default pull-right btn-sm reset_form">{{ trans('common.form_reset') }}</button>
    </div>
</div>
{!! Form::close() !!}