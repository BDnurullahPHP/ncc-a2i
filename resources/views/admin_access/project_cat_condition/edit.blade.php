{!! Form::model($condition, array('url' => '#', 'id'=>'project_cat_con_form', 'class'=>'form-horizontal', 'method' => 'post')) !!}
    <!--=============Title for modal===============-->
    <div class="modal_top_title">{{ trans('project.edit_data_condition') }}</div>
    <!--===========End title for modal=============-->
    
    <!--========Show form post message=============-->
    <div class="form-group">
        <div class="col-sm-8 col-sm-offset-1 form_messsage">
            
        </div>
    </div>
    <!--========End form post message=============-->
     <!--========End form post message=============-->
      <div class="form-group">
        <label for="PR_CATEGORY" class="col-sm-3 control-label">{{ trans('project.project_category_name') }} <span style="color: red">*</span></label>
         <span class="help_tooltip" data-toggle="tooltip" data-placement="top" title="{{ trans('project.project_category_name_ex') }}"><i class="fa fa-question-circle" aria-hidden="true"></i></span>
   
        <div class="col-sm-5">
            <select name="PR_CATEGORY" id="PR_CATEGORY" class="form-control">
                <option value="">{{ trans('common.form_select') }}</option>
                @foreach($category as $cat)
                  <option {{ $condition->PR_CATEGORY==$cat->PR_CATEGORY?'selected="selected"':'' }} value="{{ $cat->PR_CATEGORY }}">{{ $cat->CATE_NAME }}</option>
                @endforeach
            </select>
        </div>
    </div>

    <div class="form-group">
        <label for="CON_TITLE" class="col-sm-3 control-label">{{ trans('project.project_cat_con_name') }} <span style="color: red">*</span> </label>
        <div class="col-sm-7">
            {!! Form::text('CON_TITLE', Input::old('CON_TITLE'), array('class'=>'form-control form-required', 'placeholder'=>trans('project.project_cat_con_name_ex'))) !!}
        </div>
    </div>
    <div class="form-group">
        <label for="CON_DESC" class="col-sm-3 control-label">{{ trans('project.project_cat_con_des') }}</label>
        <div class="col-sm-9">
            {!! Form::textarea('CON_DESC', Input::old('CON_DESC'), array('rows' =>'2','class'=>'form-control reactor_basic', 'placeholder'=>trans('project.project_cat_con_des_ex'))) !!}
        </div>
    </div>
    <div class="hr-line-dashed"></div>
    <div class="form-group">
        <label for="IS_ACTIVE" class="col-sm-3 control-label">{{ trans('common.is_active') }}</label>
        <div class="col-sm-7">
            <label class="control-label">
                {!! Form::checkbox('IS_ACTIVE', '1', true, ['class'=>'status']) !!}
            </label>
        </div>
    </div>
  <div class="form-group">
        <div class="col-lg-offset-3 col-lg-10">
            <span class="modal_msg pull-left"></span>
                <button type="button" class="btn btn-default btn-sm reset_form">{{ trans('common.form_reset') }}</button>
                <button type="submit" data-action="{{ route('project_cat_con_update', $condition->CATE_CON_ID) }}" class="btn btn-success btn-sm form_submit">{{ trans('common.form_submit') }}</button>
            <span class="loadingImg"></span>
        </div>
    </div>
{!! Form::close() !!}