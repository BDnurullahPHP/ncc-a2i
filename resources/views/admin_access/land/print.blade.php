<!DOCTYPE html>
<html lang="en">
    <head>
        <title>{{ $land->LAND_NUMBER }}</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
    </head>
    <style>
        body {
            font-family: freeserif,sans-serif,vrinda,solaiman-lipi,monospace;
        }
        h1, h2, h3, h4, p, input, th, td{
            font-family: freeserif,sans-serif,vrinda,solaiman-lipi,monospace;
            font-weight: 400;
            white-space: initial;
        }
        .block{
            width: 100%;
        }
        .info{
            width: 40%;
            padding-right: 30px;
            float: left;
        }
        .col-3{
            width: 25%;
            padding-right: 30px;
            float: left;
        }
        h3{
            font-size: 22px;
            margin-bottom: 10px;
            margin-top: 15px;
        }
        h4{
            font-size: 18px;
            margin-bottom: 10px;
            margin-top: 15px;
        }
        strong{
            font-size: 16px;
            font-weight: 700 !important;
        }
        p{
            margin-top: 0px;
            margin-bottom: 5px;
            text-align: justify;
        }
        table, th, td {
            border: .25px solid;
            border-collapse: collapse;
            text-align: center;
        }

    </style>
    <body>
        
        <div class="block">
            <h3>{{ trans('land.land') }}</h3>
            <div class="info">
                <p><strong>{{ trans('land.land_number') }}: </strong>{{ Helpers::en2bn($land->LAND_NUMBER) }}</p>
                <p><strong>{{ trans('land.land_ward') }}: </strong>{{ $land->getWord->WARD_NAME_BN }}</p>
                <p><strong>{{ trans('land.land_form_is_case') }}: </strong>{{ count($land_case) > 0 ? 'হ্যাঁ' : 'না' }}</p>
            </div>
            <div class="info">
                <p>
                    <strong>{{ trans('land.land_form_owner') }}: 
                    @if($land->LAND_OWNER == 1)
                        সিটি কর্পোরেশন
                    @else
                        অন্যরা
                    @endif
                </p>
                <p><strong>{{ trans('land.land_zone') }}: </strong>{{ Helpers::en2bn($land->getArea->WARD_NAME_BN) }}</p>
                <p><strong>{{ trans('land.land_form_mouza') }}: </strong>{{ $land->getMouza->MOUZA_NAME_BN }}</p>
            </div>
        </div>
        
        <div class="block">
            <h3>{{ trans('land.land_dags') }}</h3>
            @foreach($land_rs_cs as $key => $rs_cs)
                <h4>{{ $rs_cs['TYPE_NAME'] }}</h4>
                <table style="width: 100%;">
                    <thead>
                        <tr>
                            <th>{{ trans('land.land_form_cs') }}</th>
                            <th>{{ trans('land.land_form_sa') }}</th>
                            <th>{{ trans('land.land_form_rs') }}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($rs_cs['LAND_DAG'] as $index => $val)  
                        <tr>
                            <td>{{ Helpers::en2bn($val['CS']) }}</td>
                            <td>{{ Helpers::en2bn($val['SA']) }}</td>
                            <td>{{ Helpers::en2bn($val['RS']) }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            @endforeach
        </div>

        <div class="block">
            <h3>{{ trans('land.land_use') }}</h3>
            <div class="col-3">
                <p><strong>{{ trans('land.land_total_land') }}: </strong>{{ Helpers::en2bn($land->TOTAL_LAND) }}</p>
            </div>
            <div class="col-3">
                <p><strong>{{ trans('land.land_total_possession') }}: </strong>{{ $land->POSSESSION_LAND }}</p>
            </div>
            <div class="col-3">
                <p><strong>{{ trans('land.land_ousted_land') }}: </strong>{{ Helpers::en2bn($land->OUSTED_LAND) }}</p>
            </div>
        </div>

        <div class="block">
            <h4>{{ trans('land.land_form_land_use') }}</h4>
            <table style="width: 100%;">
                <thead>
                    <tr>
                        <th style="text-align: center">{{ trans('land.land_form_category') }}</th>
                        <th style="text-align: center">{{ trans('land.land_form_subcategory') }}</th>
                        <th style="text-align: center">{{ trans('land.land_form_land_use') }}</th>
                    </tr>
                </thead>
                <tbody>
                    @if(count($land_use) > 0)
                        @foreach($land_use as $key => $lu)
                        <tr>
                            <td>{{ $lu->getCaregory->CAT_NAME_BN }}</td>
                            <td>{{ $lu->getSubCaregory->CAT_NAME_BN }}</td>
                            <td>{{ Helpers::en2bn($lu->USES) }}</td>
                        </tr>
                        @endforeach
                    @endif
                    <tr class="total_row" style="display: table-row;">
                        <td colspan="2"><strong>{{ trans('common.form_total') }}</strong></td>
                        <td class="total_uses"><strong>{{ Helpers::en2bn($total_use['total_land']) }}</strong></td>
                    </tr>
                </tbody>
            </table>
        </div>

        <div class="block">
            <h3>{{ trans('land.land_doc') }}</h3>
            @foreach($land_dolil as $key => $ld)
                <h4>{{ $ld['LABEL'] }}</h4>
                    @foreach($ld as $index => $fs)
                        @if( !empty($fs['DOC']) && File::exists(public_path()."/upload/land/".$fs['DOC']) )
                            <img src='{{ Helpers::asset("upload/land/".$fs['DOC']) }}' style="max-width: 100%;" alt="">
                        @endif
                    @endforeach
            @endforeach
        </div>

        <div class="block">
            <h3>{{ trans('land.land_form_tax_receipt') }}</h3>
            @foreach($land_tax as $key => $lt)
                <table style="width: 100%;">
                    <thead>
                        <tr>
                            <th>{{ trans('land.land_form_tax_receipt') }}</th>
                            <th>{{ trans('land.land_form_tax_date') }}</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>
                                <?php 
                                    $tax_file = $lt->RECEIPT.'.'.$lt->FILE_TYPE;
                                ?>
                                @if( File::exists(public_path()."/upload/land/".$tax_file) )
                                    <img src='{{ Helpers::asset("upload/land/".$tax_file) }}' style="max-width: 100%;" alt="">
                                @endif
                            </td>
                            <td>{{ Helpers::en2bn(date('d/m/Y', strtotime($lt->DATE))) }}</td>
                        </tr>
                    </tbody>
                </table>
            @endforeach
        </div>
        
        @if(count($land_case) > 0)
            <div class="block">
                <h3>{{ trans('land.land_case_info') }}</h3>
                <div class="info">
                    <p><strong>{{ trans('land.case_number') }}: </strong>{{ Helpers::en2bn($land_case->CASE_NUMBER) }}</p>
                    <p><strong>{{ trans('land.case_defendant') }}: </strong>{{ Helpers::en2bn($land_case->DEFENDANT) }}</p>
                </div>
                <div class="info">
                    <p><strong>{{ trans('land.case_complainant') }}: </strong>{{ $land_case->COMPLAINANT }}</p>
                    <p><strong>{{ trans('land.case_settlement_date') }}: </strong>
                        {{ Helpers::en2bn(date('d/m/Y', strtotime($land_case->SETTLEMENT_DATE))) }}
                    </p>
                </div>
            </div>
            <div class="block">
                <p><strong>{{ trans('land.court_document') }}</strong></p>
                @if( !empty($land_case->COURT_DOCUMENT) && File::exists(public_path()."/upload/land/".$land_case->COURT_DOCUMENT) )
                    <img src="{{ Helpers::asset("upload/land/$land_case->COURT_DOCUMENT") }}" style="max-width: 100%;" alt="">
                @endif
            </div>
            <div class="block">
                <p><strong>{{ trans('common.comment') }}: </strong>{!! $land_case->REMARKS !!}</p>
            </div>
        @endif
        
        <div class="block">
            <h3>{{ trans('land.land_form_details') }}</h3>
            {!! $land->DETAILS !!}
        </div>

        <div class="block">
            <h3>{{ trans('common.comment') }}</h3>
            {!!$land->REMARKS!!}
        </div>

    </body>
</html>

