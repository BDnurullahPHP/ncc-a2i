<!-- Nurullah/17-april-2017 -->
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>জমির তথ্য </title>
    </head>
    <style>
        body {
            font-family: freeserif,sans-serif,vrinda,solaiman-lipi,monospace;
        }
        h1, h2, h3, h4, p, input, th, td{
            font-family: freeserif,sans-serif,vrinda,solaiman-lipi,monospace;
            font-weight: 400;
            white-space: initial;
        }
        .sectionrow
        {
            width: 100%;
            border: 1px ;            
            border-style: solid ;
            font-size: 12px; 
        }
/*        h4{
            text-align:center; 
        }*/
        p{
            text-align: justify;
        }
        table#dataTable, th.datathtd, td.datathtd {
            border: .25px solid;
            border-collapse: collapse;
            text-align: center;
        }
    </style>
    <body>
        <table style="width:100%">
            <tr>
                <th style="width: 20%"><img style="width:80px;height:80px;" src="{{ Helpers::asset('assets/img/logo/logo_ncc.png') }}"></th>
                <th style="width: 60%; text-align:center;">
                    <bold>নারায়ণগঞ্জ সিটি করপোরেশন</bold>
                    <br/>নগর ভবন, ১০ বঙ্গবন্ধু রোড
                    <br/>নারায়ণগঞ্জ
                    <br/>www.ncc.gov.bd
                </th>
                <th style="width: 20%"></th>
            </tr>
        </table>
        <h3>জমির তথ্যের বিবরণ:-</h3>
        <table id="dataTable" style="width:100%">
            <thead>
                <tr>
                    <th class="datathtd">ক্রঃনং</th>
                    <th class="datathtd">{{ trans('land.land_number') }}</th>
                    <th class="datathtd">{{ trans('land.land_ward') }}</th>
                    <th class="datathtd">{{ trans('land.land_form_mouza') }}</th>
                    <th class="datathtd" style="width: 80px;">{{ trans('land.land_total_land') }}</th>
                    <th class="datathtd">{{ trans('common.table_head_created_at') }}</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                $totalLand = 0;
                ?>
                @foreach($land_data as $key => $value)
                <?php echo $totalLand += $value->TOTAL_LAND; ?>
                <tr>
                    <td class="datathtd">{{ Helpers::en2bn($key+1) }}</td>
                    <td class="datathtd">{{ Helpers::en2bn($value->LAND_NUMBER) }}</td>
                    <td class="datathtd">{{ Helpers::en2bn($value->getWord->WARD_NAME_BN) }}</td>
                    <td class="datathtd">{{ Helpers::en2bn($value->getMouza->MOUZA_NAME_BN) }}</td>
                    <td class="datathtd">{{ Helpers::en2bn($value->TOTAL_LAND) }}</td>
                    <td class="datathtd">{{ Helpers::en2bn(date('d/m/Y', strtotime($value->CREATED_AT))) }}</td>
                </tr>
                @endforeach
                <tr>
                    <td class="datathtd" colspan="4" style="text-align: right;">মোট জমি</td>
                    <td class="datathtd" colspan="2" style="text-align: center;">{{ Helpers::en2bn($totalLand) }} (শতাংশ)</td>
                </tr>
            </tbody>
        </table>
    </body>
</html>