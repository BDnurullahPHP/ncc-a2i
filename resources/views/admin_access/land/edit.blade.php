{!! Form::open(array('url' => '#', 'id'=>'land_form', 'class'=>'form-horizontal', 'method' => 'post')) !!}
    
    <!--=============Title for modal===============-->
    <div class="modal_top_title">{{ trans('land.add_data') }}</div>
    <!--===========End title for modal=============-->

    <!--========Show form post message=============-->
    <div class="form-group" style="margin-bottom: 10px;">
        <div class="col-sm-5 form_messsage">
            
        </div>
    </div>
    <!--========End form post message=============-->
    
    <div class="land_block_box land_box_gray">
        <div class="form-group">
            <div class="col-md-3 col-sm-3 col-xs-6">
                {!! Form::label('area', trans('land.land_zone')) !!} <span class="required_field">*</span>
                {!! Form::select('area', $area, $land_data->AREA, array('class'=>'form-control form-required', 'placeholder' => trans('common.form_select'))) !!}
            </div>
            <div class="col-md-3 col-sm-3 col-xs-6">
                {!! Form::label('ward', trans('land.land_ward')) !!} <span class="required_field">*</span>
                {!! Form::select('ward', $ward, $land_data->WARD_ID, array('class'=>'form-control form-required', 'placeholder' => trans('common.form_select'))) !!}
            </div>
            <div class="col-md-3 col-sm-3 col-xs-6">
                {!! Form::label('mouza', trans('land.land_form_mouza')) !!} <span class="required_field">*</span>
                {!! Form::select('mouza', $mouza, $land_data->MOUZA_ID, array('class'=>'form-control form-required', 'placeholder' => trans('common.form_select'))) !!}
            </div>
            <div class="col-md-3 col-sm-3 col-xs-6">
                {!! Form::label('land_owner', trans('land.land_form_owner')) !!} <span class="required_field">*</span>
                {!! Form::select('land_owner', [1=>'সিটি কর্পোরেশন', 2=>'অন্যরা'], $land_data->LAND_OWNER, array('id'=>'land_owner', 'class'=>'form-control form-required', 'placeholder' => trans('common.form_select'))) !!}
            </div>
        </div>
    </div>
    
    @if(count($land_rs_cs) > 0)
        @foreach($land_rs_cs as $key => $rs_cs)
            
            <div class="land_block_box land_box_gray">

                <div class="form-group row_group land_rs_cs_row" id="{{ $rs_cs['ATTR_ID'] }}">
                    {!! Form::hidden('', $rs_cs['TYPE'], array('class'=>'type')) !!}
                    <div class="col-md-12 col-sm-12">
                        <h4>{{ $rs_cs['TYPE_NAME'] }} <span class="required_field">*</span></h4>
                    </div>
                    <div class="col-md-9 col-sm-9 col-xs-12 row_data_content">
                        @foreach($rs_cs['LAND_DAG'] as $index => $val)
                            <div class="row row_single">
                                <input type="hidden" class="dag_id" value="{{ $val['DAG_ID'] }}">
                                <div class="col-md-4 col-sm-4 col-xs-6">
                                    {!! Form::label('', trans('land.land_form_cs')) !!}
                                    {!! Form::text('land_cs', $val['CS'], array('class'=>'land_cs form-control', 'placeholder'=>trans('land.land_form_cs_pl'))) !!}
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-6">
                                    {!! Form::label('', trans('land.land_form_sa')) !!}
                                    {!! Form::text('land_sa', $val['SA'], array('class'=>'land_sa form-control', 'placeholder'=>trans('land.land_form_sa_pl'))) !!}
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-6">
                                    {!! Form::label('', trans('land.land_form_rs')) !!}
                                    {!! Form::text('land_rs', $val['RS'], array('class'=>'land_rs form-control', 'placeholder'=>trans('land.land_form_rs_pl'))) !!}
                                </div>
                                <span class="remove_item_record" data-url="{{ route('land_dag_delete', $val['DAG_ID']) }}" title="Remove"><i class="fa fa-times"></i></span>
                            </div>
                        @endforeach
                        <div class="row row_single" style="display:none;">
                            <div class="col-md-4 col-sm-4 col-xs-6">
                                {!! Form::label('', trans('land.land_form_cs')) !!}
                                {!! Form::text('land_cs', Input::old('land_cs'), array('class'=>'land_cs form-control form-number-optional', 'placeholder'=>trans('land.land_form_cs_pl'))) !!}
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-6">
                                {!! Form::label('', trans('land.land_form_sa')) !!}
                                {!! Form::text('land_sa', Input::old('land_sa'), array('class'=>'land_sa form-control form-number-optional', 'placeholder'=>trans('land.land_form_sa_pl'))) !!}
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-6">
                                {!! Form::label('', trans('land.land_form_rs')) !!} <span class="required_field">*</span>
                                {!! Form::text('land_rs', Input::old('land_rs'), array('class'=>'land_rs form-control form-number-optional', 'placeholder'=>trans('land.land_form_rs_pl'))) !!}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-3 col-xs-6">
                        <button type="button" class="btn green btn-sm add_more btn_margin"> <i class="fa fa-plus"></i> {{ trans('common.add_more') }}</button>
                    </div>
                </div>

            </div>

        @endforeach
    @endif

    <div class="form-group">
    	<div class="col-md-3 col-sm-3 col-xs-6">
            {!! Form::label('possession_land', trans('land.land_total_possession')) !!} <span class="required_field">*</span>
            {!! Form::text('possession_land', $land_data->POSSESSION_LAND, array('class'=>'form-control allow_numeric form-number', 'placeholder'=>trans('land.land_total_possession_pl'))) !!}
        </div>
        <div class="col-md-3 col-sm-3 col-xs-6">
            {!! Form::label('ousted_land', trans('land.land_ousted_land')) !!} <span class="required_field">*</span>
            {!! Form::text('ousted_land', $land_data->OUSTED_LAND, array('class'=>'form-control allow_numeric form-number', 'placeholder'=>trans('land.land_ousted_land_pl'))) !!}
        </div>
        <div class="col-md-3 col-sm-3 col-xs-6">
            {!! Form::label('total_land', trans('land.land_total_land')) !!} <span class="required_field">*</span>
            {!! Form::text('total_land', $land_data->TOTAL_LAND, array('class'=>'form-control allow_numeric form-number', 'readonly'=>'true', 'placeholder'=>trans('land.land_total_land_pl'))) !!}
        </div>
    </div>

    <div class="form-group">
        <div class="col-md-3 col-sm-3 col-xs-6">
            {!! Form::label('category', trans('land.land_form_category')) !!}
            {!! Form::select('category', $caregory, array(Input::old('category')), array('class'=>'form-control', 'placeholder' => trans('common.form_select'))) !!}
        </div>
        <div class="col-md-3 col-sm-3 col-xs-6">
            {!! Form::label('subcategory', trans('land.land_form_subcategory')) !!}
            {!! Form::select('subcategory', [], array(Input::old('subcategory')), array('class'=>'form-control', 'placeholder' => trans('common.form_select'))) !!}
        </div>
        <div class="col-md-3 col-sm-3 col-xs-6">
            {!! Form::label('land_use', trans('land.land_form_land_use')) !!}
            {!! Form::text('land_use', Input::old('land_use'), array('class'=>'form-control allow_numeric', 'placeholder'=>trans('land.land_form_land_use'))) !!}
        </div>
        <div class="col-md-3 col-sm-3 col-xs-6">
            <button type="button" class="btn green btn-sm land_type_add"> <i class="fa fa-plus"></i> {{ trans('common.add_more') }}</button>
        </div>
    </div>

    <div class="form-group">
    	<div class="col-md-9 col-sm-9 col-xs-12">
    		<table class="table table-striped table-bordered table-hover gridTable" id="land_use_table">
                <tbody>
                    @if(count($land_use) > 0)
                        @foreach($land_use as $key => $lu)
                            <tr class="single_row" data-landuses="{{ $lu->LU_ID }}">
                                <td class="category_single" data-category="{{ $lu->LAND_CATEGORY }}">
                                    {{ $lu->getCaregory->CAT_NAME_BN }}
                                </td>
                                <td class="subcategory_single" data-subcategory="{{ $lu->LAND_SUBCATEGORY }}">
                                    {{ $lu->getSubCaregory->CAT_NAME_BN }}
                                </td>
                                <td class="single_use" data-uses="{{ $lu->USES }}">
                                    {{ $lu->USES }}
                                    <span class="remove_land_type_use" data-url="{{ route('land_use_delete', $lu->LU_ID) }}">
                                        <i class="fa fa-times"></i>
                                    </span>
                                </td>
                            </tr>
                        @endforeach
                    @endif
                    <tr class="total_row" style="display: table-row;">
                        <td colspan="2"><strong>{{ trans('common.form_total') }}</strong></td>
                        <td class="total_uses"><strong>{{ $total_use['total_land'] }}</strong></td>
                    </tr>
                </tbody>
            </table>
    	</div>
    </div>
    
    <div class="land_block_box land_box_gray">
        <div class="form-group">
            @foreach($land_dolil as $key => $ld)
                <div class="col-md-3 col-sm-4 col-xs-6 row_group">
                    {!! Form::label('', $ld['LABEL']) !!}
                    <div class="row row_data_content" id="{{ $ld['ATTR_ID'] }}">
                        @foreach($ld as $index => $fs)
                            @if( !empty($fs['DOC']) && File::exists(public_path()."/upload/land/".$fs['DOC']) )
                                <div class="col-md-12 col-sm-12 col-xs-12 doc_single">
                                    <p style="margin-top:0px;margin-bottom:5px;">
                                        <a target="_blank" href='{{ Helpers::asset("upload/land/".$fs['DOC']) }}'>{{ $fs['DOC'] }}</a>
                                        <span class="remove_doc_record fa fa-times" data-url="{{ route('land_document_delete', [$fs['DOC_ID'], $ld['NAME']]) }}"></span>
                                    </p>
                                </div>
                            @endif
                        @endforeach
                        <div class="col-md-12 col-sm-12 col-xs-12 row_single">
                            {!! Form::file($ld['NAME'], array('class'=>'form-control multi_file attachment-optional '.$ld['NAME'])) !!}
                        </div>
                    </div>
                    <div class="row append_btn_margin">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <button type="button" class="btn green btn-sm file_add_more"> <i class="fa fa-plus"></i> {{ trans('common.add_more') }}</button>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>

        <div class="form-group row_group" id="tax_multiple">
            <div class="col-md-12 col-sm-12 col-xs-12">
                {!! Form::label('', trans('land.land_form_tax_receipt')) !!}
            </div>
            <div class="col-md-9 col-sm-9 col-xs-12 row_data_content">
                @if(count($land_tax) > 0)
                    @foreach($land_tax as $key => $lt)
                        <div class="row row_single tax_item">
                            <?php 
                                $tax_file = $lt->RECEIPT.'.'.$lt->FILE_TYPE;
                            ?>
                            <input type="hidden" class="tax_id" value="{{ $lt->TAX_ID }}">
                            <div class="col-md-6 col-sm-6 col-xs-6">
                                @if( File::exists(public_path()."/upload/land/".$tax_file) )
                                    <a target="_blank" href="{{ Helpers::asset("upload/land/$tax_file") }}">{{ $tax_file }}</a>
                                @endif
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-6">
                                <div class="input-group">
                                    {!! Form::text('tax_date', date('d-m-Y', strtotime($lt->DATE)), array('class'=>'form-control date_multiple date_picker_default', 'placeholder'=>trans('land.land_form_tax_date'))) !!}
                                    <div class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i></div>
                                </div>
                            </div>
                            <span class="remove_item_record" data-url="{{ route('land_tax_delete', $lt->TAX_ID) }}" title="Remove"><i class="fa fa-times"></i></span>
                        </div>
                    @endforeach
                @endif
                <div class="row row_single" style="display:none;">
                    <div class="col-md-6 col-sm-6 col-xs-6">
                        {!! Form::file('tax', array('class'=>'form-control multi_file tax attachment-optional')) !!}
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-6">
                        <div class="input-group">
                            {!! Form::text('tax_date', Input::old('tax_date'), array('class'=>'form-control date_multiple date_picker_default', 'placeholder'=>trans('land.land_form_tax_date'))) !!}
                            <div class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-3 col-xs-6">
                <button type="button" class="btn green btn-sm add_more"> <i class="fa fa-plus"></i> {{ trans('common.add_more') }}</button>
            </div>
        </div>

    </div>

    <div class="form-group">
        <div class="col-md-6 col-sm-6 col-xs-12">
            {!! Form::label('land_details', trans('land.land_form_details')) !!} <span class="required_field">*</span>
            {!! Form::textarea('land_details', $land_data->DETAILS, array('rows' =>'3', 'class'=>'form-control reactor_basic form-required', 'placeholder'=>trans('land.land_form_details_pl'))) !!}
        </div>
        <div class="col-md-6 col-sm-6 col-xs-12">
            {!! Form::label('comments', trans('common.comment')) !!}
            {!! Form::textarea('comments', $land_data->REMARKS, array('rows' =>'3', 'class'=>'form-control reactor_basic', 'placeholder'=>trans('common.your_comment'))) !!}
        </div>
    </div>

    <!--===================================================-->
    <div class="form-group">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="row">
                <div class="col-md-6 col-sm-6 col-xs-12">
                    {!! Form::label('', trans('land.land_form_is_case')) !!}<br/>
                    @if(!empty($land_data->landCase))
                        <label class="radio-inline">
                            {!! Form::radio('is_case', 1, true) !!} {{ trans('common.yes') }}
                        </label>
                        <label class="radio-inline">
                            {!! Form::radio('is_case', 0, false) !!} {{ trans('common.no') }}
                        </label>
                    @else
                        <label class="radio-inline">
                            {!! Form::radio('is_case', 1, false) !!} {{ trans('common.yes') }}
                        </label>
                        <label class="radio-inline">
                            {!! Form::radio('is_case', 0, true) !!} {{ trans('common.no') }}
                        </label>
                    @endif
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    {!! Form::label('is_active', trans('common.is_active')) !!}<br/>
                    @if($land_data->IS_ACTIVE == 1)
                        {!! Form::checkbox('is_active', 1, true) !!}
                    @else
                        {!! Form::checkbox('is_active', 1, false) !!}
                    @endif
                </div>
            </div>
            @if(!empty($land_data->landCase))
                <div class="row case_details_field" style="display:block;">
                    {!! Form::hidden('', $land_data->landCase->LAND_CASE_ID, array('class'=>'land_case_id')) !!}
            @else
                <div class="row case_details_field">
            @endif
                <div class="row">
                    <div class="col-md-3 col-sm-3 col-xs-6">
                        {!! Form::label('case_number', trans('land.case_number')) !!}
                        {!! Form::text('case_number', !empty($land_data->landCase)?$land_data->landCase->CASE_NUMBER:null, array('class'=>'form-control case-required case_number', 'placeholder'=>trans('land.case_number'))) !!}
                    </div>
                    <div class="col-md-3 col-sm-3 col-xs-6">
                        {!! Form::label('case_complainant', trans('land.case_complainant')) !!}
                        {!! Form::text('case_complainant', !empty($land_data->landCase)?$land_data->landCase->COMPLAINANT:null, array('class'=>'form-control case-required case_complainant', 'placeholder'=>trans('land.case_complainant'))) !!}
                    </div>
                    <div class="col-md-3 col-sm-3 col-xs-6">
                        {!! Form::label('case_defendant', trans('land.case_defendant')) !!}
                        {!! Form::text('case_defendant', !empty($land_data->landCase)?$land_data->landCase->DEFENDANT:null, array('class'=>'form-control case-required case_defendant', 'placeholder'=>trans('land.case_defendant'))) !!}
                    </div>
                    <div class="col-md-3 col-sm-3 col-xs-6">
                        {!! Form::label('settlement_date', trans('land.case_settlement_date')) !!}
                        {!! Form::text('settlement_date', !empty($land_data->landCase)?date('d-m-Y', strtotime($land_data->landCase->SETTLEMENT_DATE)):null, array('class'=>'form-control case-required settlement_date date_picker_default', 'placeholder'=>trans('land.case_settlement_date'))) !!}
                    </div>
                </div>
                <div class="row" style="margin-top:15px;">
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        {!! Form::label('court_document', trans('land.court_document')) !!}<br>
                        <?php 
                            if (!empty($land_data->landCase)) {
                                $court_doc = $land_data->landCase->COURT_DOCUMENT;
                            }
                        ?>
                        @if( isset($court_doc) && File::exists(public_path()."/upload/land/".$court_doc) )
                            <a target="_blank" href="{{ Helpers::asset("upload/land/$court_doc") }}">{{ $court_doc }}</a>
                        @endif
                        {!! Form::file('court_document', array('class'=>'form-control attachment-optional court_document')) !!}
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        {!! Form::label('case_details', trans('common.comment')) !!}
                        {!! Form::textarea('case_details', !empty($land_data->landCase)?$land_data->landCase->REMARKS:null, array('rows' =>'3', 'class'=>'form-control case_remarks reactor_basic', 'placeholder'=>trans('common.your_comment'))) !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--===================================================-->

    <div class="form-group">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <button type="submit" data-action="{{ route('land_update', $land_data->LAND_ID) }}" id="land_submit" class="btn btn-success btn-sm pull-right" style="margin-left: 10px">{{ trans('common.form_submit') }}</button>
            <button type="button" class="btn default btn-sm pull-right reset_form">{{ trans('common.form_reset') }}</button>
        </div>
    </div>

{!! Form::close() !!}