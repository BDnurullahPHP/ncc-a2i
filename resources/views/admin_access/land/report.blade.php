@extends('admin_access.layout.master')

@section('style')

<link rel="stylesheet" href="{{ Helpers::asset('assets/admin/css/land.css') }}">
<style type="text/css">
    .body_purple .table-bordered>thead>tr>th{
        font-weight: 700;
    }
    .table-bordered>tbody>tr>td{
        font-weight: 300;
    }
    label{
        font-weight: bold;
    }
    /*        .search_data_btn{
                margin-top: 10px;
            }
            @media only screen and (min-width: 768px) {
                 .search_data_btn{
                    min-width: 152px;
                    float: right;
                } 
            }
            @media only screen and (max-width: 767px) {
                 .search_data_btn{
                    display: block;
                    width: 100%;
                } 
            }*/
</style>

@endsection

@section('content')

<h3>{{ trans('land.land_information') }} 
    <small></small>
</h3>
{!! Form::open(array('url' => route('land_report_data'), 'id'=>'search_form', 'method' => 'get')) !!}

<div class="row">
    <div class="col-md-9">
        <div class="row">
            <div class="col-md-3">
                <div class="form-group">
                    {!! Form::label('area', trans('land.land_zone')) !!}
                    {!! Form::select('area', $area, array(Input::old('area')), array('class'=>'form-control form-search', 'placeholder' => trans('common.form_select'))) !!}
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    {!! Form::label('ward', trans('land.land_ward')) !!}
                    {!! Form::select('ward', [], array(Input::old('ward')), array('class'=>'form-control form-search', 'placeholder' => trans('common.form_select'))) !!}
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    {!! Form::label('mouza', trans('land.land_form_mouza')) !!}
                    {!! Form::select('mouza', [], array(Input::old('mouza')), array('class'=>'form-control form-search', 'placeholder' => trans('common.form_select'))) !!}
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    {!! Form::label('land_owner', trans('land.land_form_owner')) !!}
                    {!! Form::select('land_owner', [1=>'NCC', 2=>'Others'], array(Input::old('land_owner')), array('id'=>'land_owner', 'class'=>'form-control form-search', 'placeholder' => trans('common.form_select'))) !!}
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3">
                <div class="form-group">
                    {!! Form::label('land_number', trans('land.land_number')) !!}
                    {!! Form::text('land_number', Input::old('land_number'), array('id'=>'land_number', 'class'=>'form-control form-search', 'placeholder'=>trans('land.land_number'))) !!}
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    {!! Form::label('category', trans('land.land_form_category')) !!}
                    {!! Form::select('category', $caregory, array(Input::old('category')), array('class'=>'form-control form-search', 'placeholder' => trans('common.form_select'))) !!}
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    {!! Form::label('subcategory', trans('land.land_form_subcategory')) !!}
                    {!! Form::select('subcategory', [], array(Input::old('subcategory')), array('class'=>'form-control form-search', 'placeholder' => trans('common.form_select'))) !!}
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    {!! Form::label('is_case', trans('land.land_form_is_case')) !!}
                    {!! Form::select('is_case', ['no'=>trans('common.no'), 'yes'=>trans('common.yes')], array(Input::old('is_case')), array('id'=>'is_case', 'class'=>'form-control form-search', 'placeholder' => trans('common.form_select'))) !!}
                </div>
            </div>
        </div>
    </div>
    <div  class="col-md-3"><!--Nurullah /17-april-2017-->
        <div class="form-group" style="padding-top: 65px; ">
            <label class="control-label"></label><b/>
            @if($access->READ == 1)
            <button type="button" class="btn btn-sm blue search_data_btn" style="width: 50px;"><i class="fa fa-search"></i> সার্চ</button>
            <a href="{{ route('land_report_print') }}" class="btn btn-sm btn-default search_report_btn"><i class="fa fa-file-pdf-o"></i> pdf</a>
            <a href="{{ route('land_report_excel') }}" class="btn btn-sm btn-success search_report_btn"><i class="fa fa-file-excel-o"></i> Excel</a>
            @endif   
        </div>
    </div>
</div>

{!! Form::close() !!}

<div class="portlet light bordered" style="display: none;">
    <div class="row">
        <div class="col-md-5 col-sm-6 col-xs-12">
            <div id="lg_form_message"></div>
        </div>
    </div>
    <div class="portlet-body contentArea">
        <table class="table table-striped table-bordered table-hover search_table_data">
            <thead>
                <tr>
                    <th>{{ trans('common.table_sl') }}</th>
                    <th>{{ trans('land.land_number') }}</th>
                    <th>{{ trans('land.land_ward') }}</th>
                    <th>{{ trans('land.land_form_mouza') }}</th>
                    <th>{{ trans('land.land_total_land') }}</th>
                    <th>{{ trans('common.table_head_created_at') }}</th>
                    <th>{{ trans('common.table_action') }}</th>
                </tr>
            </thead>
            <tbody>

            </tbody>
        </table>
    </div>
</div>

@endsection

@section('script')
<script src="{{ Helpers::asset('assets/admin/js/land.js') }}" type="text/javascript"></script>
<script src="{{ Helpers::asset('assets/admin/js/search.js') }}" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function() {
    landManaged.init();
    AdvanceSearch.init();
});
</script>
@endsection