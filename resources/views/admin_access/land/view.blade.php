<div class="project_details">
    <!--=============Title for modal===============-->
    <div class="modal_top_title">{{ trans('land.land_details_info') }}</div>
    <!--===========End title for modal=============-->

    <div class="col-md-12">
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-folder-o"></i>{{ trans('land.land') }}
                </div>
                <div class="tools">
                    <a href="javascript:;" class="collapse"> </a>
                    <a title="" data-original-title="" href="javascript:;" class="remove"> </a>
                </div>
            </div>
            <div class="portlet-body" style="display: block;"><!--Panal-->

                <div class="row padding-bottom-10">
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="row">
                            {!! Form::label('', trans('land.land_number'), array('class'=>'col-sm-6')) !!}
                            <div class="col-md-6 col-sm-6">
                                <p>{{ Helpers::en2bn($land->LAND_NUMBER) }}</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="row">
                            {!! Form::label('', trans('land.land_form_owner'), array('class'=>'col-sm-6')) !!}
                            <div class="col-md-6 col-sm-6">
                                <p>
                                    @if($land->LAND_OWNER == 1)
                                        সিটি কর্পোরেশন
                                    @else
                                        অন্যরা
                                    @endif
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row padding-bottom-10">
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="row">
                            {!! Form::label('', trans('land.land_zone'), array('class'=>'col-sm-6')) !!}
                            <div class="col-md-6 col-sm-6">
                                <p>{{$land->getArea->WARD_NAME_BN }}</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="row">
                            {!! Form::label('', trans('land.land_ward'), array('class'=>'col-sm-6')) !!}
                            <div class="col-md-6 col-sm-6">
                                <p>{{$land->getWord->WARD_NAME_BN }}</p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row padding-bottom-10">
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="row">
                            {!! Form::label('', trans('land.land_form_mouza'), array('class'=>'col-sm-6')) !!}
                            <div class="col-md-6 col-sm-6">
                                <p>{{$land->getMouza->MOUZA_NAME_BN }}</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="row">
                            {!! Form::label('', trans('land.land_form_is_case'), array('class'=>'col-sm-6')) !!}
                            <div class="col-md-6 col-sm-6">
                                <p>{{ count($land_case) > 0 ? 'হ্যাঁ' : 'না' }} </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <div class="col-md-12">
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-cubes"></i>{{ trans('land.land_dags') }}
                </div>
                <div class="tools">
                    <a href="javascript:;" class="collapse"> </a>
                    <a title="" data-original-title="" href="javascript:;" class="remove"> </a>
                </div>
            </div>
            <div class="portlet-body" style="display: block;">
                <div class="tabbable-custom nav-justified">
                    <ul class="nav nav-tabs nav-justified">
                        @foreach($land_rs_cs as $key => $rs_cs)
                        <li class="{{ $key==0 ? 'active' : '' }}">
                            <a href="#tab_{{ $key }}" aria-controls="tab_link_{{ $key }}" role="tab" data-toggle="tab"><h4>{{ $rs_cs['TYPE_NAME'] }}</h4></a>
                        </li>
                        @endforeach
                    </ul>
                    <div class="tab-content">
                        @foreach($land_rs_cs as $key => $rs_cs)
                        <div role="tabpanel" class="tab-pane {{ $key==0?'active':'' }}" id="tab_{{ $key }}">
                            <table class="table table-striped table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th>{{ trans('land.land_form_cs') }}</th>
                                        <th>{{ trans('land.land_form_sa') }}</th>
                                        <th>{{ trans('land.land_form_rs') }}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($rs_cs['LAND_DAG'] as $index => $val)  
                                    <tr>
                                        <td>{{ Helpers::en2bn($val['CS']) }}</td>
                                        <td>{{ Helpers::en2bn($val['SA']) }}</td>
                                        <td>{{ Helpers::en2bn($val['RS']) }}</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-12">
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-table"></i>{{ trans('land.land_use') }}
                </div>
                <div class="tools">
                    <a href="javascript:;" class="collapse"> </a>
                    <a title="" data-original-title="" href="javascript:;" class="remove"> </a>
                </div>
            </div>
            <div class="portlet-body" style="display: block;"><!--Panal-->
                <div class="row padding-bottom-10">
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="row">
                            {!! Form::label('', trans('land.land_total_land'), array('class'=>'col-sm-6')) !!}
                            <div class="col-md-6 col-sm-6">
                                <p>{{ Helpers::en2bn($land->TOTAL_LAND) }}</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12">

                    </div>
                </div>
                <div class="row padding-bottom-10">
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="row">
                            {!! Form::label('', trans('land.land_total_possession'), array('class'=>'col-sm-6')) !!}
                            <div class="col-md-6 col-sm-6">
                                <p>{{ Helpers::en2bn($land->POSSESSION_LAND) }}</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="row">
                            {!! Form::label('', trans('land.land_ousted_land'), array('class'=>'col-sm-6')) !!}
                            <div class="col-md-6 col-sm-6">
                                <p>{{ Helpers::en2bn($land->OUSTED_LAND) }}</p>
                            </div>
                        </div>
                    </div>
                </div>
                <h3>{{ trans('land.land_form_land_use') }}</h3>
                <table class="table table-striped table-bordered table-hover gridTable" id="land_use_table">
                    <thead>
                        <tr>
                            <th style="text-align: center">{{ trans('land.land_form_category') }}</th>
                            <th style="text-align: center">{{ trans('land.land_form_subcategory') }}</th>
                            <th style="text-align: center">{{ trans('land.land_form_land_use') }}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(count($land_use) > 0)
                        @foreach($land_use as $key => $lu)
                        <tr>
                            <td>
                                {{ $lu->getCaregory->CAT_NAME_BN }}
                            </td>
                            <td>
                                {{ $lu->getSubCaregory->CAT_NAME_BN }}
                            </td>
                            <td>
                                {{ Helpers::en2bn($lu->USES) }}
                            </td>
                        </tr>
                        @endforeach
                        @endif
                        <tr class="total_row" style="display: table-row;">
                            <td colspan="2"><strong>{{ trans('common.form_total') }}</strong></td>
                            <td class="total_uses"><strong>{{ Helpers::en2bn($total_use['total_land']) }}</strong></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-copy"></i>{{ trans('land.land_doc') }}
                </div>
                <div class="tools">
                    <a href="javascript:;" class="collapse"> </a>
                    <a title="" data-original-title="" href="javascript:;" class="remove"> </a>
                </div>
            </div>
            <div class="portlet-body" style="display: block;"><!--Panal-->
                <div class="tabbable-custom nav-justified">
                    <ul class="nav nav-tabs nav-justified">
                        @foreach($land_dolil as $key => $ld)
                        <li class="{{ $key== 'dolil' ? 'active' : '' }}">
                            <a href="#tab_{{ $key }}" role="tab" data-toggle="tab"><h4>{{ $ld['LABEL'] }}</h4></a>
                        </li>
                        @endforeach
                    </ul>
                    <div class="tab-content">
                        @foreach($land_dolil as $key => $ld)
                        <div role="tabpanel" class="tab-pane {{ $key=='dolil' ? 'active' : '' }}" id="tab_{{ $key }}">
                            @foreach($ld as $index => $fs)
                            @if(!empty($fs['DOC']) && File::exists(public_path()."/upload/land/".$fs['DOC']))
                            <table class="table table-striped table-bordered table-hover">
                                <tbody>
                                    <tr>
                                        <td>
                                            <a target="_blank" href='{{ Helpers::asset("upload/land/".$fs['DOC']) }}'>{{ $fs['DOC'] }}</a>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            @endif
                            @endforeach
                        </div>
                        @endforeach
                    </div>
                </div>
                <h3>{{ trans('land.land_form_tax_receipt') }}</h3>
                <table class="table table-striped table-bordered table-hover gridTable" id="land_use_table">
                    <thead>
                        <tr>
                            <th style="text-align: center">করের রশিদ</th>
                            <th style="text-align: center">রাশিদের তারিখ</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($land_tax as $key => $doc)
                        <tr>
                            <td>
                                <p>
                                    <?php
                                    $taxfile = $doc->RECEIPT . '.' . $doc->FILE_TYPE;
                                    ?>
                                    <a target="_blank" href='{{ Helpers::asset("upload/land/$taxfile") }}'>{{ $doc->RECEIPT }}</a>
                                </p>
                            </td>
                            <td>{{ Helpers::en2bn(date('d/m/Y', strtotime($doc->DATE))) }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    
    @if(count($land_case) >0)
        <div class="col-md-12">
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-file"></i>{{ trans('land.land_case_info') }}
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"> </a>
                        <a title="" data-original-title="" href="javascript:;" class="remove"> </a>
                    </div>
                </div>
                <div class="portlet-body" style="display: block;"><!--Panal-->
                    <div class="row padding-bottom-10">
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="row">
                                {!! Form::label('', trans('land.case_number'), array('class'=>'col-sm-6')) !!}
                                <div class="col-md-6 col-sm-6">
                                    <p>{{ Helpers::en2bn($land_case->CASE_NUMBER) }}</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="row">
                                {!! Form::label('', trans('land.case_complainant'), array('class'=>'col-sm-6')) !!}
                                <div class="col-md-6 col-sm-6">
                                    <p>{{ $land_case->COMPLAINANT }}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row padding-bottom-10">
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="row">
                                {!! Form::label('', trans('land.case_defendant'), array('class'=>'col-sm-6')) !!}
                                <div class="col-md-6 col-sm-6">
                                    <p>{{ $land_case->DEFENDANT }}</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="row">
                                {!! Form::label('', trans('land.case_settlement_date'), array('class'=>'col-sm-6')) !!}
                                <div class="col-md-6 col-sm-6">
                                    <p>
                                        {{ Helpers::en2bn(date('d/m/Y', strtotime($land_case->SETTLEMENT_DATE))) }}
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row padding-bottom-10">
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="row">
                                {!! Form::label('', trans('land.court_document'), array('class'=>'col-sm-6')) !!}
                                <div class="col-md-6 col-sm-6">
                                    <a target="_blank" href='{{ Helpers::asset("upload/land/".$land_case->COURT_DOCUMENT) }}'>{{ $land_case->COURT_DOCUMENT }}</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <h3>{{ trans('common.comment') }}</h3>
                    {!! $land_case->REMARKS !!}
                </div>
            </div>
        </div>
    @endif
    
    <div class="col-md-12">
        <div class="portlet-body" style="display: block;">
            <h3>{{ trans('land.land_form_details') }}</h3>
            {!! $land->DETAILS !!}
            <h3>{{ trans('common.comment') }}</h3>
            {!!$land->REMARKS!!}
        </div>
    </div>
    
</div>

