{!! Form::open(array('url' => '#', 'id'=>'land_form', 'class'=>'form-horizontal', 'method' => 'post')) !!}
    
    <!--=============Title for modal===============-->
    <div class="modal_top_title">{{ trans('land.add_data') }}</div>
    <!--===========End title for modal=============-->

    <!--========Show form post message=============-->
    <div class="form-group" style="margin-bottom: 10px;">
        <div class="col-sm-5 form_messsage">
            
        </div>
    </div>
    <!--========End form post message=============-->
    
    <div class="land_block_box land_box_gray">
        <div class="form-group">
            <div class="col-md-3 col-sm-3 col-xs-6">
                {!! Form::label('area', trans('land.land_zone')) !!} <span class="required_field">*</span>
                {!! Form::select('area', $area, array(Input::old('area')), array('class'=>'form-control form-required', 'placeholder' => trans('common.form_select'))) !!}
            </div>
            <div class="col-md-3 col-sm-3 col-xs-6">
                {!! Form::label('ward', trans('land.land_ward')) !!} <span class="required_field">*</span>
                {!! Form::select('ward', [], array(Input::old('ward')), array('class'=>'form-control form-required', 'placeholder' => trans('common.form_select'))) !!}
            </div>
            <div class="col-md-3 col-sm-3 col-xs-6">
                {!! Form::label('mouza', trans('land.land_form_mouza')) !!} <span class="required_field">*</span>
                {!! Form::select('mouza', [], array(Input::old('mouza')), array('class'=>'form-control form-required', 'placeholder' => trans('common.form_select'))) !!}
            </div>
            <div class="col-md-3 col-sm-3 col-xs-6">
                {!! Form::label('land_owner', trans('land.land_form_owner')) !!} <span class="required_field">*</span>
                {!! Form::select('land_owner', [1=>'সিটি কর্পোরেশন', 2=>'অন্যরা'], 1, array('id'=>'land_owner', 'class'=>'form-control form-required', 'placeholder' => trans('common.form_select'))) !!}
            </div>
        </div>

    </div>
    
    <div class="land_block_box land_box_gray">

        <div class="form-group row_group land_rs_cs_row" id="khatian_multiple">
            {!! Form::hidden('', 1, array('class'=>'type')) !!}
            <div class="col-md-12 col-sm-12">
                <h4>{{ trans('land.khatian_no_rs') }} <span class="required_field">*</span></h4>
            </div>
            <div class="col-md-9 col-sm-9 col-xs-12 row_data_content">
                <div class="row row_single">
                    <div class="col-md-4 col-sm-4 col-xs-6">
                        {!! Form::label('', trans('land.land_form_cs')) !!}
                        {!! Form::text('khatian_cs', null, array('class'=>'land_cs form-control', 'placeholder'=>trans('land.land_form_cs_pl'))) !!}
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-6">
                        {!! Form::label('', trans('land.land_form_sa')) !!}
                        {!! Form::text('khatian_sa', null, array('class'=>'land_sa form-control', 'placeholder'=>trans('land.land_form_sa_pl'))) !!}
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-6">
                        {!! Form::label('', trans('land.land_form_rs')) !!}
                        {!! Form::text('khatian_rs', null, array('class'=>'land_rs form-control', 'placeholder'=>trans('land.land_form_rs_pl'))) !!}
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-3 col-xs-6">
                <button type="button" class="btn green btn-sm add_more btn_margin"> <i class="fa fa-plus"></i> {{ trans('common.add_more') }}</button>
            </div>
        </div>

    </div>
    
    <div class="land_block_box land_box_gray">

        <div class="form-group row_group land_rs_cs_row" id="dag_multiple">
            {!! Form::hidden('', 2, array('class'=>'type')) !!}
            <div class="col-md-12 col-sm-12">
                <h4>{{ trans('land.dag_no_rs') }} <span class="required_field">*</span></h4>
            </div>
            <div class="col-md-9 col-sm-9 col-xs-12 row_data_content">
                <div class="row row_single">
                    <div class="col-md-4 col-sm-4 col-xs-6">
                        {!! Form::label('', trans('land.land_form_cs')) !!}
                        {!! Form::text('dag_cs', null, array('class'=>'land_cs form-control', 'placeholder'=>trans('land.land_form_cs_pl'))) !!}
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-6">
                        {!! Form::label('', trans('land.land_form_sa')) !!}
                        {!! Form::text('dag_sa', null, array('class'=>'land_sa form-control', 'placeholder'=>trans('land.land_form_sa_pl'))) !!}
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-6">
                        {!! Form::label('', trans('land.land_form_rs')) !!}
                        {!! Form::text('dag_rs', null, array('class'=>'land_rs form-control', 'placeholder'=>trans('land.land_form_rs_pl'))) !!}
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-3 col-xs-6">
                <button type="button" class="btn green btn-sm add_more btn_margin"> <i class="fa fa-plus"></i> {{ trans('common.add_more') }}</button>
            </div>
        </div>

    </div>

    <div class="land_block_box land_box_gray">

        <div class="form-group row_group land_rs_cs_row" id="jl_multiple">
            {!! Form::hidden('', 3, array('class'=>'type')) !!}
            <div class="col-md-12 col-sm-12">
                <h4>{{ trans('land.jl_no_rs') }} <span class="required_field">*</span></h4>
            </div>
            <div class="col-md-9 col-sm-9 col-xs-12 row_data_content">
                <div class="row row_single">
                    <div class="col-md-4 col-sm-4 col-xs-6">
                        {!! Form::label('', trans('land.land_form_cs')) !!}
                        {!! Form::text('jl_cs', null, array('class'=>'land_cs form-control', 'placeholder'=>trans('land.land_form_cs_pl'))) !!}
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-6">
                        {!! Form::label('', trans('land.land_form_sa')) !!}
                        {!! Form::text('jl_sa', null, array('class'=>'land_sa form-control', 'placeholder'=>trans('land.land_form_sa_pl'))) !!}
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-6">
                        {!! Form::label('', trans('land.land_form_rs')) !!}
                        {!! Form::text('jl_rs', null, array('class'=>'land_rs form-control', 'placeholder'=>trans('land.land_form_rs_pl'))) !!}
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-3 col-xs-6">
                <button type="button" class="btn green btn-sm add_more btn_margin"> <i class="fa fa-plus"></i> {{ trans('common.add_more') }}</button>
            </div>
        </div>

    </div>

    <div class="land_block_box land_box_gray">

        <div class="form-group row_group land_rs_cs_row" id="owner_multiple">
            {!! Form::hidden('', 4, array('class'=>'type')) !!}
            <div class="col-md-12 col-sm-12">
                <h4>{{ trans('land.owner_rs') }} <span class="required_field">*</span></h4>
            </div>
            <div class="col-md-9 col-sm-9 col-xs-12 row_data_content">
                <div class="row row_single">
                    <div class="col-md-4 col-sm-4 col-xs-6">
                        {!! Form::label('', trans('land.land_form_cs')) !!}
                        {!! Form::text('owner_cs', null, array('class'=>'land_cs form-control', 'placeholder'=>trans('land.land_form_cs_pl'))) !!}
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-6">
                        {!! Form::label('', trans('land.land_form_sa')) !!}
                        {!! Form::text('owner_sa', null, array('class'=>'land_sa form-control', 'placeholder'=>trans('land.land_form_sa_pl'))) !!}
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-6">
                        {!! Form::label('', trans('land.land_form_rs')) !!}
                        {!! Form::text('owner_rs', null, array('class'=>'land_rs form-control', 'placeholder'=>trans('land.land_form_rs_pl'))) !!}
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-3 col-xs-6">
                <button type="button" class="btn green btn-sm add_more btn_margin"> <i class="fa fa-plus"></i> {{ trans('common.add_more') }}</button>
            </div>
        </div>

    </div>
        
    <div class="form-group">
    	<div class="col-md-3 col-sm-3 col-xs-6">
            {!! Form::label('possession_land', trans('land.land_total_possession')) !!} <span class="required_field">*</span>
            {!! Form::text('possession_land', Input::old('possession_land'), array('class'=>'form-control allow_numeric form-number', 'placeholder'=>trans('land.land_total_possession_pl'))) !!}
        </div>
        <div class="col-md-3 col-sm-3 col-xs-6">
            {!! Form::label('ousted_land', trans('land.land_ousted_land')) !!} <span class="required_field">*</span>
            {!! Form::text('ousted_land', Input::old('ousted_land'), array('class'=>'form-control allow_numeric form-number', 'placeholder'=>trans('land.land_ousted_land_pl'))) !!}
        </div>
        <div class="col-md-3 col-sm-3 col-xs-6">
            {!! Form::label('total_land', trans('land.land_total_land')) !!} <span class="required_field">*</span>
            {!! Form::text('total_land', Input::old('total_land'), array('class'=>'form-control allow_numeric form-number', 'readonly'=>'true', 'placeholder'=>trans('land.land_total_land_pl'))) !!}
        </div>
    </div>

    <div class="form-group">
        <div class="col-md-3 col-sm-3 col-xs-6">
            {!! Form::label('category', trans('land.land_form_category')) !!}
            {!! Form::select('category', $caregory, array(Input::old('category')), array('class'=>'form-control', 'placeholder' => trans('common.form_select'))) !!}
        </div>
        <div class="col-md-3 col-sm-3 col-xs-6">
            {!! Form::label('subcategory', trans('land.land_form_subcategory')) !!}
            {!! Form::select('subcategory', [], array(Input::old('subcategory')), array('class'=>'form-control', 'placeholder' => trans('common.form_select'))) !!}
        </div>
        <div class="col-md-3 col-sm-3 col-xs-6">
            {!! Form::label('land_use', trans('land.land_form_land_use')) !!}
            {!! Form::text('land_use', Input::old('land_use'), array('class'=>'form-control allow_numeric', 'placeholder'=>trans('land.land_form_land_use'))) !!}
        </div>
        <div class="col-md-3 col-sm-3 col-xs-6">
            <button type="button" class="btn green btn-sm land_type_add"> <i class="fa fa-plus"></i> {{ trans('land.add_land_use') }}</button>
        </div>
    </div>

    <div class="form-group">
    	<div class="col-md-9 col-sm-9 col-xs-12">
    		<table class="table table-striped table-bordered table-hover gridTable" id="land_use_table">
                <tbody>
                    <tr class="total_row">
                        <td colspan="2"><strong>{{ trans('common.form_total') }}</strong></td>
                        <td class="total_uses"><strong>0</strong></td>
                    </tr>
                </tbody>
            </table>
    	</div>
    </div>
    
    <div class="land_block_box land_box_gray">
        <div class="form-group">
            <div class="col-md-3 col-sm-4 col-xs-6 row_group">
                {!! Form::label('', trans('land.land_form_document')) !!}
                <div class="row row_data_content" id="dolil_multiple">
                    <div class="col-md-12 col-sm-12 col-xs-12 row_single">
                        {!! Form::file('dolil', array('class'=>'form-control attachment-optional multi_file dolil')) !!}
                    </div>
                </div>
                <div class="row append_btn_margin">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <button type="button" class="btn green btn-sm file_add_more"> <i class="fa fa-plus"></i> {{ trans('common.add_more') }}</button>
                    </div>
                </div>
            </div>
            
            <div class="col-md-3 col-sm-4 col-xs-6 row_group">
                {!! Form::label('', trans('land.land_form_dcr')) !!}
                <div class="row row_data_content" id="dcr_multiple">
                    <div class="col-md-12 col-sm-12 col-xs-12 row_single">
                        {!! Form::file('dcr', array('class'=>'form-control attachment-optional multi_file dcr')) !!}
                    </div>
                </div>
                <div class="row append_btn_margin">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <button type="button" class="btn green btn-sm file_add_more"> <i class="fa fa-plus"></i> {{ trans('common.add_more') }}</button>
                    </div>
                </div>
            </div>

            <div class="col-md-3 col-sm-4 col-xs-6 row_group">
                {!! Form::label('', trans('land.land_form_ledger')) !!}
                <div class="row row_data_content" id="ledger_multiple">
                    <div class="col-md-12 col-sm-12 col-xs-12 row_single">
                        {!! Form::file('ledger', array('class'=>'form-control attachment-optional multi_file ledger')) !!}
                    </div>
                </div>
                <div class="row append_btn_margin">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <button type="button" class="btn green btn-sm file_add_more"> <i class="fa fa-plus"></i> {{ trans('common.add_more') }}</button>
                    </div>
                </div>
            </div>

            <div class="col-md-3 col-sm-4 col-xs-6 row_group">
                {!! Form::label('', trans('land.land_form_bahia')) !!}
                <div class="row row_data_content" id="bahia_multiple">
                    <div class="col-md-12 col-sm-12 col-xs-12 row_single">
                        {!! Form::file('bahia', array('class'=>'form-control attachment-optional multi_file bahia')) !!}
                    </div>
                </div>
                <div class="row append_btn_margin">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <button type="button" class="btn green btn-sm file_add_more"> <i class="fa fa-plus"></i> {{ trans('common.add_more') }}</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="form-group row_group" id="tax_multiple">
            <div class="col-md-12 col-sm-12 col-xs-12">
                {!! Form::label('', trans('land.land_form_tax_receipt')) !!}
            </div>
            <div class="col-md-9 col-sm-9 col-xs-12 row_data_content">
                <div class="row row_single">
                    <div class="col-md-6 col-sm-6 col-xs-6">
                        {!! Form::file('tax', array('class'=>'form-control attachment-optional multi_file tax')) !!}
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-6">
                        <div class="input-group">
                            {!! Form::text('tax_date', Input::old('tax_date'), array('class'=>'form-control date_multiple date_picker_default', 'placeholder'=>trans('land.land_form_tax_date'))) !!}
                            <div class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-3 col-xs-6">
                <button type="button" class="btn green btn-sm add_more"> <i class="fa fa-plus"></i> {{ trans('common.add_more') }}</button>
            </div>
        </div>

    </div>

    <div class="form-group">
        <div class="col-md-6 col-sm-6 col-xs-12">
            {!! Form::label('land_details', trans('land.land_form_details')) !!}  <span class="required_field">*</span>
            {!! Form::textarea('land_details', null, array('rows' =>'3', 'class'=>'form-control reactor_basic form-required', 'placeholder'=>trans('land.land_form_details_pl'))) !!}
        </div>
        <div class="col-md-6 col-sm-6 col-xs-12">
            {!! Form::label('comments', trans('common.comment')) !!}
            {!! Form::textarea('comments', null, array('rows' =>'3', 'class'=>'form-control reactor_basic', 'placeholder'=>trans('common.your_comment'))) !!}
        </div>
    </div>

    <div class="form-group">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="row">
                <div class="col-md-6 col-sm-6 col-xs-12">
                    {!! Form::label('', trans('land.land_form_is_case')) !!}<br/>
                    <label class="radio-inline">
                        {!! Form::radio('is_case', 1, false) !!} {{ trans('common.yes') }}
                    </label>
                    <label class="radio-inline">
                        {!! Form::radio('is_case', 0, true) !!} {{ trans('common.no') }}
                    </label>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    {!! Form::label('is_active', trans('common.is_active')) !!}<br/>
                    {!! Form::checkbox('is_active', 1, true) !!}
                </div>
            </div>
            <div class="row case_details_field">
                <div class="row">
                    <div class="col-md-3 col-sm-3 col-xs-6">
                        {!! Form::label('case_number', trans('land.case_number')) !!}
                        {!! Form::text('case_number', NULL, array('class'=>'form-control case-required case_number', 'placeholder'=>trans('land.case_number'))) !!}
                    </div>
                    <div class="col-md-3 col-sm-3 col-xs-6">
                        {!! Form::label('case_complainant', trans('land.case_complainant')) !!}
                        {!! Form::text('case_complainant', NULL, array('class'=>'form-control case-required case_complainant', 'placeholder'=>trans('land.case_complainant'))) !!}
                    </div>
                    <div class="col-md-3 col-sm-3 col-xs-6">
                        {!! Form::label('case_defendant', trans('land.case_defendant')) !!}
                        {!! Form::text('case_defendant', NULL, array('class'=>'form-control case-required case_defendant', 'placeholder'=>trans('land.case_defendant'))) !!}
                    </div>
                    <div class="col-md-3 col-sm-3 col-xs-6">
                        {!! Form::label('settlement_date', trans('land.case_settlement_date')) !!}
                        {!! Form::text('settlement_date', NULL, array('class'=>'form-control case-required settlement_date date_picker_default', 'placeholder'=>trans('land.case_settlement_date'))) !!}
                    </div>
                </div>
                <div class="row" style="margin-top:15px;">
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        {!! Form::label('court_document', trans('land.court_document')) !!}
                        {!! Form::file('court_document', array('class'=>'form-control attachment-optional court_document')) !!}
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        {!! Form::label('case_details', trans('common.comment')) !!}
                        {!! Form::textarea('case_details', null, array('rows' =>'3', 'class'=>'form-control case_remarks reactor_basic', 'placeholder'=>trans('common.your_comment'))) !!}
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="form-group">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <button type="submit" data-action="{{ route('land_create') }}" id="land_submit" class="btn green btn-sm pull-right" style="margin-left: 10px">{{ trans('common.form_submit') }}</button>
            <button type="button" class="btn default btn-sm pull-right reset_form">{{ trans('common.form_reset') }}</button>
        </div>
    </div>

{!! Form::close() !!}