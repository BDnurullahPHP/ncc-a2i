@extends('admin_access.layout.master')

@section('style')
<link rel="stylesheet" href="{{ Helpers::asset('assets/admin/css/land.css') }}">
<style type="text/css">

</style>
@endsection

@section('content')

<h3>{{ trans('land.land_information') }}
    <small></small>
    @if($access->CREATE == 1)
    <a href="#" data-toggle="modal" data-target="#modal_big_content" class="btn btn-primary btn-sm pull-right" data-form="{{ route('land_create_form') }}">{{ trans('common.add_new') }} <i class="fa fa-plus"></i></a>
    @endif	
</h3>
<div class="portlet light bordered">
    <!-- <div class="portlet-title">
        <div class="caption">
            
        </div>
        <div class="tools"> </div>
    </div> -->
    <div class="row">
        <div class="col-md-5 col-sm-6 col-xs-12">
            <div id="lg_form_message"></div>
        </div>
    </div>
    <div class="portlet-body contentArea">

        <table class="table table-striped table-bordered table-hover" data-source="{{ route('land_data') }}" id="common_table">
            <thead>
                <tr>
                    <th>{{ trans('common.table_sl') }}</th>
                    <th>{{ trans('land.land_number') }}</th>
                    <th>{{ trans('land.land_ward') }}</th>
                    <th>{{ trans('land.land_form_mouza') }}</th>
                    <th>{{ trans('land.land_total_land') }}</th>
                    <th>{{ trans('common.table_head_created_at') }}</th>
                    <th>{{ trans('common.table_action') }}</th>
                </tr>
            </thead>
            <tbody>

            </tbody>
        </table>

    </div>
</div>

@endsection

@section('script')

<script src="{{ Helpers::asset('assets/admin/js/land.js') }}" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function() {
    landManaged.init();
});
</script>


@endsection