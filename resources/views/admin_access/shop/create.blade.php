{!! Form::open(array('url' => '#', 'id'=>'word_form', 'class'=>'form-horizontal', 'method' => 'post')) !!}
       <div class="form-group">
        <div class="col-sm-5 col-sm-offset-3 form_messsage">
            
        </div>
    </div>
    
    <div class="form-group">
        <label for="name" class="col-sm-3 control-label">{{ trans('common.porject_name') }} <span style="color: red">*</span></label>
        <div class="col-sm-5">
            {!! Form::select('parent_id', $zone, null, ['class'=>'form-control form-required', 'placeholder' => 'নির্বাচন করুন']) !!}
        </div>
    </div>

    <div class="form-group">
        <label for="name" class="col-sm-3 control-label">{{ trans('common.shop_type') }} </label>
        <div class="col-sm-5">
            <select class="form-control">
                <option value="">নির্বাচন করুন</option>
                <option value="1">টাইপ-A</option>
                <option value="2">টাইপ-B</option>
                <option value="3">টাইপ-C</option>
                <option value="4">টাইপ-D</option>
            </select>
        </div>
    </div>
    <div class="form-group">
        <label for="bn_name" class="col-sm-3 control-label">{{ trans('common.shop_area') }} <span style="color: red">*</span></label>
        <div class="col-sm-5">
            {!! Form::text('bn_name', Input::old('bn_name'), array('class'=>'form-control form-required', 'placeholder'=>trans('common.shop_area')))!!}
        </div>
    </div>
    <div class="hr-line-dashed"></div>
    <div class="form-group">
        <label for="bn_name" class="col-sm-3 control-label">{{ trans('common.floor_no') }} <span style="color: red">*</span></label>
        <div class="col-sm-5">
            {!! Form::text('bn_name', Input::old('bn_name'), array('class'=>'form-control form-required', 'placeholder'=>trans('common.floor_no')))!!}
        </div>
    </div>
    <div class="hr-line-dashed"></div>
    <div class="hr-line-dashed"></div>
    <div class="form-group">
        <label for="bn_name" class="col-sm-3 control-label">{{ trans('common.shop_no') }} <span style="color: red">*</span></label>
        <div class="col-sm-5">
            {!! Form::text('bn_name', Input::old('bn_name'), array('class'=>'form-control form-required', 'placeholder'=>trans('common.shop_no')))!!}
        </div>
    </div>
    <div class="form-group">
        <label for="bn_name" class="col-sm-3 control-label">{{ trans('common.facility') }} <span style="color: red">*</span></label>
        <div class="col-sm-8">
            <textarea rows="2" cols="100" class="form-control"></textarea>
        </div>
    </div>
    <div class="hr-line-dashed"></div>
    <div class="form-group">
        <label for="bn_name" class="col-sm-3 control-label">{{ trans('common.attach') }} <span style="color: red">*</span></label>
        <div class="col-sm-5">
            <input type="file" class="form-control">
        </div>
    </div>
    <div class="hr-line-dashed"></div>
    <div class="form-group">
        <label for="bn_name" class="col-sm-3 control-label">{{ trans('common.remarks') }} <span style="color: red">*</span></label>
        <div class="col-sm-8">
            <textarea rows="2" cols="100" class="form-control"></textarea>
        </div>
    </div>
    <div class="hr-line-dashed"></div>
    <div class="form-group">
        <label for="is_active" class="col-sm-3 control-label">{{ trans('common.status') }}</label>
        <div class="col-sm-7">
            <label class="control-label">
                {!! Form::checkbox('is_active', '1', true, ['class'=>'status']) !!}
            </label>
        </div>
    </div>
    <div class="form-group">
        <div class="col-lg-offset-3 col-lg-10">
            <span class="modal_msg pull-left"></span>
                <button type="button" class="btn btn-default btn-sm reset_form">Reset</button>
                <button type="submit" data-action="{{ route('word_save') }}" class="btn btn-primary btn-sm form_submit">{{ trans('common.form_submit') }}</button>
            <span class="loadingImg"></span>
        </div>
    </div>
{!! Form::close() !!}