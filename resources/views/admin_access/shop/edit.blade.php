   {!! Form::model($word, array('url' => '#', 'id'=>'word_form', 'class'=>'form-horizontal', 'method' => 'post')) !!}
     <div class="form-group">
        <div class="col-sm-5 col-sm-offset-3 form_messsage">
            
        </div>
    </div>
    

      <div class="form-group">
        <label for="parent_id" class="col-sm-3 control-label">{{ trans('common.zone_gen_name') }} <span style="color: red">*</span></label>
        <div class="col-sm-5">
            <select name="parent_id" id="parent_id" class="form-control form-required">
                <option value="">{{ trans('common.form_select') }}</option>
                @foreach ($zone as $zn)
                    <option {{ $zn->id==$word->parent_id?'selected="selected"':'' }} value="{{ $zn->id }}">{{ $zn->bn_name }}</option>
                @endforeach
            </select>
        </div>
    </div>

    <div class="form-group">
        <label for="name" class="col-sm-3 control-label">{{ trans('common.word_name') }} </label>
        <div class="col-sm-5">
            {!! Form::text('name', Input::old('name'), array('class'=>'form-control', 'placeholder'=>trans('common.word_name'))) !!}
        </div>
    </div>
    <div class="form-group">
        <label for="bn_name" class="col-sm-3 control-label">{{ trans('common.word_bn_name') }}<span style="color: red">*</span></label>
        <div class="col-sm-5">
            {!! Form::text('bn_name', Input::old('bn_name'), array('class'=>'form-control', 'placeholder'=>trans('common.word_bn_name')))!!}
        </div>
    </div>
    <div class="hr-line-dashed"></div>
    <div class="form-group">
        <label for="is_active" class="col-sm-3 control-label">Active?</label>
        <div class="col-sm-7">
            <label class="control-label">
                {!! Form::checkbox('is_active', '1', true, ['class'=>'status']) !!}
            </label>
        </div>
    </div>
    <div class="form-group">
        <div class="col-lg-offset-3 col-lg-10">
            <span class="modal_msg pull-left"></span>
                <button type="button" class="btn btn-default btn-sm reset_form">Reset</button>
               <button type="submit" data-action="{{ route('word_edit', $word->id) }}" class="btn btn-primary btn-sm form_submit">{{ trans('common.form_submit') }}</button>
            <span class="loadingImg"></span>
        </div>
    </div>
{!! Form::close() !!}