@extends('admin_access.layout.master')

@section('style')

@endsection

@section('script')

	<script type="text/javascript">
		$(function () {
			$("#common_table1").DataTable();
		});
	</script>

@endsection

@section('content')

	<h3>{{ trans('common.shop') }}
    	<small></small>
	</h3>
	<div class="portlet light bordered">
	    <div class="portlet-title">
	        <div class="caption">
	            <a href="#" data-toggle="modal" data-target="#modal_add_content" class="btn btn-primary btn-sm" data-form="{{ route('shop_create_form') }}">{{ trans('common.add_new') }} <i class="fa fa-plus"></i></a>
	        </div>
	        <div class="tools"> </div>
	    </div>
	    <div class="portlet-body contentArea">
            <table class="table table-striped table-bordered table-hover" id="common_table1">
			    <thead>
			        <tr>
			            <th>{{ trans('common.table_sl') }}</th>
                        <th>{{ trans('common.porject_name') }}</th>
			            <th>{{ trans('common.shop_type') }}</th>
			            <th>{{ trans('common.shop_no') }}</th>
			            <th>{{ trans('common.shop_area') }}</th>
			            <th>{{ trans('common.table_action') }}</th>
			        </tr>
			    </thead>
			    <tbody>
                    <tr>
                        <td>১</td>
                        <td>পদ্ম সিটি প্লাজা -৩ মহিম গাঙ্গুলী রোড</td>
                        <td>টাইপ-A</td>
                        <td>১</td>
                        <td>৯৬৮.৮২ বর্গফুট</td>
                        <td>
                            <a class="label label-default openModal" id=""
                               title="Update District Information" data-action="setup/districtFormUpdate" data-type="edit"><i class="fa fa-pencil"></i>
                            </a>

                            <a class="label label-danger deleteItem" id="" title="Click For Delete"
                               data-type="delete" data-field="DISTRICT_ID" data-tbl="sa_districts"><i class="fa fa-times"></i></a>

                            <a class="itemStatus" id=""
                               data-status="" data-fieldId="DISTRICT_ID" data-field="ACTIVE_FLAG"
                               data-tbl="sa_districts" data-su-url="setup/districtById">
                            </a>
                        </td>
                    </tr>
                    <tr>
                        <td>২</td>
                        <td>মন্ডলপাড়া হার্র্ট ফাউন্ডেশন সংলগ্ন স্থানে কর্মাশিয়াল ভবন </td>
                        <td>টাইপ-B</td>
                        <td>০৯</td>
                        <td>৭৩৬.০০ বর্গফুট</td>
                        <td>
                            <a class="label label-default openModal" id=""
                               title="Update District Information" data-action="setup/districtFormUpdate" data-type="edit"><i class="fa fa-pencil"></i>
                            </a>

                            <a class="label label-danger deleteItem" id="" title="Click For Delete"
                               data-type="delete" data-field="DISTRICT_ID" data-tbl="sa_districts"><i class="fa fa-times"></i></a>

                            <a class="itemStatus" id=""
                               data-status="" data-fieldId="DISTRICT_ID" data-field="ACTIVE_FLAG"
                               data-tbl="sa_districts" data-su-url="setup/districtById">
                            </a>
                        </td>
                    </tr>
			    </tbody>
            </table>
	    </div>
	</div>

@endsection