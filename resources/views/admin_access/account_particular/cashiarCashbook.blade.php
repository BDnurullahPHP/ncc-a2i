@extends('admin_access.layout.master')

@section('style')
    <style type="text/css">

    </style>
@endsection

@section('content')
    <h3>ক্যাশবুক রেজিস্টার  <small></small>
   
    </h3>
    <div class="portlet light bordered">
       {{--  <div class="portlet-title">
            <div class="caption">
                
            </div>
            <div class="tools"> </div>
        </div> --}}
        <div class="row">
            <div class="col-md-5 col-sm-6 col-xs-12">
                <div id="lg_form_message"></div>
            </div>
        </div>
        <div class="portlet-body contentArea">

            <table class="table table-striped table-bordered table-hover" class="" data-source="" id="">
                <thead>
                <tr>
                    <th>ক্রমিক</th>
                    <th>ভাউচার নাম্বার</th>
                    <th>গ্রহিতার নাম</th>
                    <th>খাত</th>
                    <th>টাকার পরিমান</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                	<td>1</td>
                	<td>112211</td>
                	<td>তারাপদ আচার্য</td>
                	<td>ভাড়া </td>
                	<td>5,000</td>
                </tr>
                  <tr>
                	<td>2</td>
                	<td>112213</td>
                	<td>হরিসংখ বাবু</td>
                	<td>অন্যান্য</td>
                	<td>4,000</td>
                </tr>
                  <tr>
                	<td>3</td>
                	<td>112214</td>
                	<td>সুজন আহাম্মেদ</td>
                	<td>ভাড়া </td>
                	<td>3,000</td>
                </tr>
                </tbody>
            </table>

        </div>
    </div>
@endsection

@section('script')

    <script type="text/javascript">
        $(document).ready(function() {

        });
    </script>

@endsection