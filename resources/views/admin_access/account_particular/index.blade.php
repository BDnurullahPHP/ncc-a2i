@extends('admin_access.layout.master')

@section('style')

@endsection

@section('content')

<h3>{{ trans('account.particular_list') }}
    <small></small>
    @if($access->CREATE == 1)
    <a href="#" data-toggle="modal" title="{{ trans('account.particular_title') }}" data-target="#modal_add_content" class="btn btn-primary btn-sm pull-right" data-form="{{ route('particular_create_form') }}">{{ trans('common.add_new') }} <i class="fa fa-plus"></i></a>
    @endif
</h3>
<div class="portlet light bordered">
    {{-- <div class="portlet-title">
        <div class="caption">
            
        </div>
        <div class="tools"> </div>
    </div> --}}
    <div class="portlet-body contentArea">
        <table class="table table-striped table-bordered table-hover" data-source="{{ route('particular_data') }}" id="common_table">
            <thead>
                <tr>
                    <th>{{ trans('common.table_sl') }}</th>
                    <th>{{ trans('account.particular_name') }}</th>
                    <th>{{ trans('account.particular_description') }}</th>
                    <th>{{ trans('common.table_action') }}</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
</div>
@endsection