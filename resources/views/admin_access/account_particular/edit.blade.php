    {!! Form::model($particular, array('url' => '#', 'id'=>'particular_form', 'class'=>'form-horizontal', 'method' => 'post')) !!}
    <!--=============Title for modal===============-->
    <div class="modal_top_title">{{ trans('account.particular_title_edit') }}</div>
    <!--===========End title for modal=============-->
    
    <div class="form-group">
        <div class="col-sm-5 col-sm-offset-3 form_messsage">
            
        </div>
    </div>

    <div class="form-group">
        <label for="PARTICULAR_NAME" class="col-sm-3 control-label">{{ trans('account.particular_name') }} <span style="color: red">*</span></label>
        <div class="col-sm-5">
            {!! Form::text('PARTICULAR_NAME', Input::old('PARTICULAR_NAME'), array('class'=>'form-control form-required', 'placeholder'=> trans('account.eg_particular_name')))!!}
        </div>
    </div>
    <div class="form-group">
        <label for="PARTICULAR_DESC" class="col-sm-3 control-label">{{ trans('account.particular_description') }} </label>
        <div class="col-sm-8">
            {!! Form::textarea('PARTICULAR_DESC', Input::old('PARTICULAR_DESC'), array('rows' =>'2','class'=>'form-control reactor_basic', 'placeholder'=>trans('account.particular_description'))) !!}
        </div>
    </div>
    <div class="hr-line-dashed"></div>
    <div class="form-group">
        <label for="IS_ACTIVE" class="col-sm-3 control-label">{{ trans('common.is_active') }}</label>
        <div class="col-sm-7">
            <label class="control-label">
                @if($particular->IS_ACTIVE == 1)
                    {!! Form::checkbox('IS_ACTIVE', '1', true, ['class'=>'status']) !!}
                @else
                    {!! Form::checkbox('IS_ACTIVE', '0', false, ['class'=>'status']) !!}    
                @endif
            </label>
        </div>
    </div>
    <div class="form-group">
        <div class="col-lg-offset-3 col-lg-10">
            <span class="modal_msg pull-left"></span>
            <button type="reset" class="btn btn-default btn-sm reset_form">{{ trans('common.form_reset') }}</button>
                 <button type="submit" data-action="{{ route('particular_update', $particular->PARTICULAR_ID) }}" class="btn btn-success btn-sm form_submit">{{ trans('common.form_submit') }}</button>
            <span class="loadingImg"></span>
        </div>
    </div>
{!! Form::close() !!}