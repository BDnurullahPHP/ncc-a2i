{!! Form::open(array('url' => '#', 'id'=>'ac_particular_form', 'class'=>'form-horizontal', 'method' => 'post')) !!}
    <!--=============Title for modal===============-->
    <div class="modal_top_title">{{ trans('account.particular_title') }}</div>
    <!--===========End title for modal=============-->
    
<div class="form-group">
    <div class="col-sm-8 col-sm-offset-1 form_messsage"></div>
</div>
<div class="form-group">
    <label for="PARTICULAR_NAME" class="col-sm-3 control-label">{{ trans('account.particular_name') }} <span style="color: red">*</span></label>
    <div class="col-sm-6">
        {!! Form::text('PARTICULAR_NAME', Input::old('PARTICULAR_NAME'), array('class'=>'form-control form-required', 'placeholder'=> trans('account.eg_particular_name'))) !!}
    </div>
    </div>

    <div class="form-group">
        <label for="PARTICULAR_DESC" class="col-sm-3 control-label">{{ trans('account.particular_description') }} </label>
        <div class="col-sm-9">
            {!! Form::textarea('PARTICULAR_DESC', Input::old('PARTICULAR_DESC'), array('rows' =>'2','class'=>'form-control reactor_basic', 'placeholder'=>trans('account.particular_description'))) !!}
        </div>
    </div>

<div class="hr-line-dashed"></div>
<div class="form-group">
    <label for="IS_ACTIVE" class="col-sm-3 control-label">{{ trans('common.is_active') }}</label>
    <div class="col-sm-7">
        <label class="control-label">
            {!! Form::checkbox('IS_ACTIVE', '1', true, ['class'=>'status']) !!}
        </label>
    </div>
</div>
<div class="form-group">
    <div class="col-lg-offset-3 col-lg-10">
        <span class="modal_msg pull-left"></span>
        <button type="reset" class="btn btn-default btn-sm">{{ trans('common.form_reset') }}</button>
        <button type="submit" data-action="{{ route('particular_save') }}" class="btn btn-success btn-sm form_submit">{{ trans('common.form_submit') }}</button>
        <span class="loadingImg"></span>
    </div>
</div>
{!! Form::close() !!}