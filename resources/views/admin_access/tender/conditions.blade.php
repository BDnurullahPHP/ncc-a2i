<form id="tender_rules" class="form-horizontal" action="#" method="post">
	<div class="form-group no-margin" id="tender_rules_data">
		@if(count($conditions) > 0)
			@foreach($conditions as $key => $con)
				<div class="col-md-12 col-sm-12 col-xs-12 data_single_row">
					@if(isset($tender_id))
						<input type="hidden" class="te_con_id" value="{{ $con->TE_CON_ID }}">
					@endif
					<h4>{{ $con->getCategory->CATE_NAME }} {{ trans('common._condition') }}</h4>
					<input type="hidden" class="condition_cat" value="{{ $con->PR_CATEGORY }}">
					<textarea name="TENDER_CONDITIONS_{{ $key }}" rows="5" class="form-control condition_text">{!! $con->CON_DESC !!}</textarea>
				</div>
			@endforeach
		@else
			<div class="col-md-12 col-sm-12 col-xs-12 data_single_row">
				<h4>{{ $cat_name }} {{ trans('common._condition') }}</h4>
				<input type="hidden" class="condition_cat" value="{{ $category }}">
				<textarea name="TENDER_CONDITIONS" rows="5" class="form-control condition_text"></textarea>
			</div>
		@endif
	</div>
</form>