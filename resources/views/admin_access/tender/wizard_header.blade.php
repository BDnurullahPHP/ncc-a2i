<ul class="nav nav-pills nav-justified steps">
    <li>
        <a href="#tab1" data-toggle="tab" class="step">
            <span class="number">{{ trans('common.one') }}</span>
            <span class="desc">
                <i class="fa fa-check"></i>
                @if(Request::segment(2) == 'lease')
                    {{ trans('lease.mohal_list') }}
                @else
                    {{ trans('project.project_information') }}
                @endif
            </span>
        </a>
    </li>
    <li>
        <a href="#tab2" data-toggle="tab" class="step">
            <span class="number">{{ trans('common.two') }}</span>
            <span class="desc"><i class="fa fa-check"></i>{{ trans('tender.tender_information') }}</span>
        </a>
    </li>
    <li>
        <a href="#tab3" data-toggle="tab" class="step active">
            <span class="number">{{ trans('common.three') }}</span>
            <span class="desc"><i class="fa fa-check"></i>{{ trans('tender.tender_schedule_info') }}</span>
        </a>
    </li>
    <li>
        <a href="#tab4" data-toggle="tab" class="step active">
            <span class="number">{{ trans('common.four') }}</span>
            <span class="desc"><i class="fa fa-check"></i>{{ trans('tender.tender_condition') }}</span>
        </a>
    </li>
    <li>
        <a href="#tab5" data-toggle="tab" class="step">
            <span class="number">{{ trans('common.five') }}</span>
            <span class="desc"><i class="fa fa-check"></i>{{ trans('common.confirm') }}</span>
        </a>
    </li>
</ul>
<div id="bar" class="progress progress-striped" role="progressbar">
    <div class="progress-bar progress-bar-success"></div>
</div>