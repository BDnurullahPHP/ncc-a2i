<div class="tender_view">

    <!--=============Title for modal===============-->
    <div class="modal_top_title">{{ trans('tender.tender_details_info') }}</div>
    <!--===========End title for modal=============-->
    <br/>
    <div class="row padding-bottom-10">
        <div class="col-md-12 col-sm-6 col-xs-12">
            <div class="row">
                {!! Form::label('', trans('tender.tender_project'), array('class'=>'col-sm-3')) !!}
                <div class="col-md-9 col-sm-9">
                    <p>{{ $schedule->getProject->PR_NAME_BN }}</p>
                </div>
            </div>
        </div>
    </div>
    <div class="row padding-bottom-10">
        <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="row">
                {!! Form::label('', trans('tender.tender_number'), array('class'=>'col-sm-6')) !!}
                <div class="col-md-6 col-sm-6">
                    <p>{{ $tender->TENDER_NO }}</p>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="row">
                {!! Form::label('', trans('tender.tender_date'), array('class'=>'col-sm-6')) !!}
                <div class="col-md-6 col-sm-6">
                    <p>{{ date('d/m/Y', strtotime($tender->TENDER_DT)) }}</p>
                </div>
            </div>
        </div>
    </div>

    <div class="row padding-bottom-10">
        <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="row">
                {!! Form::label('', trans('tender.tender_name'), array('class'=>'col-sm-6')) !!}
                <div class="col-md-6 col-sm-6">
                    <p>{{ $tender->TENDER_TITLE }}</p>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="row">
                {!! Form::label('', trans('tender.tender_method'), array('class'=>'col-sm-6')) !!}
                <div class="col-md-6 col-sm-6">
                    <p>@if ($tender->TENDER_METHOD === 1) OTM @else 0 @endif</p>
                </div>
            </div>
        </div>
    </div>

    <div class="row padding-bottom-10">

        <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="row">
                {!! Form::label('', trans('tender.tender_publish_date'), array('class'=>'col-sm-6')) !!}
                <div class="col-md-6 col-sm-6">
                    <p>{{ date('d/m/Y', strtotime($tender->TENDER_PUBLISH_DT)) }}</p>
                </div>
            </div>
        </div>
    </div>

    <div class="row padding-bottom-10">
        <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="row">
                {!! Form::label('', trans('tender.sc_last_selling_date'), array('class'=>'col-sm-6')) !!}
                <div class="col-md-6 col-sm-6">
                    <p>{{ date('d/m/Y', strtotime($tender_date->LAST_SELLING_DT_FROM)).' - '.date('d/m/Y', strtotime($tender_date->LAST_SELLING_DT_TO)) }}</p>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="row">
                {!! Form::label('', trans('tender.sc_last_selling_time'), array('class'=>'col-sm-6')) !!}
                <div class="col-md-6 col-sm-6">
                    <p>{{ date('h:m A', strtotime($tender_date->LAST_SELLING_DT_FROM)).' - '.date('h:m A', strtotime($tender_date->LAST_SELLING_DT_TO)) }}</p>
                </div>
            </div>
        </div>
    </div>

    <div class="row padding-bottom-10">
        <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="row">
                {!! Form::label('', trans('tender.te_last_receive_date'), array('class'=>'col-sm-6')) !!}
                <div class="col-md-6 col-sm-6">
                    <p>{{ date('d/m/Y h:m A', strtotime($tender_date->TE_LAST_RECIEVE_DT)) }}</p>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="row">
                {!! Form::label('', trans('tender.te_opening_dt'), array('class'=>'col-sm-6')) !!}
                <div class="col-md-6 col-sm-6">
                    <p>{{ date('d/m/Y h:m A', strtotime($tender_date->TE_OPENING_DT)) }}</p>
                </div>
            </div>
        </div>
    </div>
    <div class="row padding-bottom-10">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="row">
                {!! Form::label('', trans('tender.tender_location'), array('class'=>'col-sm-3')) !!}
                <div class="col-md-9 col-sm-9">
                    <ul style="padding-left:18px;">
                        @foreach($tender_location as $key => $loc)
                        <li>{{ $loc->getLocation->LOCATION_NAME }}</li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="row padding-bottom-10">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="row">
                {!! Form::label('', trans('tender.tender_copy_location'), array('class'=>'col-sm-3')) !!}
                <div class="col-md-9 col-sm-9">
                    <ul style="padding-left:18px;">
                        @foreach($onulipy_location as $key => $loc)
                        <li>{{ $loc->getLocation->LOCATION_NAME }}</li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="row padding-bottom-10">
        <div class="col-md-12 col-sm-12 col-xs-12" style="text-align: justify">
            {!! Form::label('', trans('tender.tender_details')) !!}
            {!! $tender->TENDER_DESC !!}
        </div>
    </div>
    <div class="row tabbable-custom nav-justified" id="pro_dtls_sc_overview">
        @if(count($project_category))
        <ul class="nav nav-tabs nav-justified" role="tablist">
            @foreach($project_category as $key => $pr_cat)
            <li role="presentation" class="{{ $key==0?'active':'' }}">
                <a href="#tab_link_{{ $key }}" aria-controls="tab_link_{{ $key }}" role="tab" data-toggle="tab"><h4>{{ $pr_cat->getCategory->CATE_NAME }} {{ trans('common._information') }}</h4></a>
            </li>
            @endforeach
        </ul>
        <div class="tab-content">
            @foreach($project_category as $key => $pr_cat)
            <div role="tabpanel" class="tab-pane {{ $key==0?'active':'' }}" id="tab_link_{{ $key }}">
                <table class="table table-striped table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>{{ trans('common.number') }}</th>
                            <th>{{ trans('common.location') }}</th>
                            <th>{{ trans('common.measurement') }}</th>
                            <th>
                                {{ trans('tender.min_bid_amount') }}<br>
                                &#40;{{ trans('common.taka') }}&#41;
                            </th>
                            <?php
                            $cat_particular = DB::table('sa_category_particulars')
                                    ->where('PR_CATEGORY', $pr_cat->PR_CATEGORY)
                                    ->orderBy('PARTICULAR_ID', 'desc')
                                    ->get();
                            ?>
                            @foreach($cat_particular as $k => $cat_prt)
                            <?php
                            $particular_name = DB::table('ac_particular')
                                    ->where('PARTICULAR_ID', $cat_prt->PARTICULAR_ID)
                                    ->pluck('PARTICULAR_NAME');
                            ?>
                            <th>{{ $particular_name }}</th>
                            @endforeach
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($project_details as $index => $pr_dtls)
                        <?php
                        $details = DB::table('project_details')
                                ->where('PR_CATEGORY', $pr_cat->PR_CATEGORY)
                                ->where('PR_DETAIL_ID', $pr_dtls->PR_DETAIL_ID)
                                ->first();
                        ?>
                        @if(!empty($details))
                        <tr>
                            <td>{{ $details->PR_SSF_NO }}</td>
                            <td>{{ $details->POSITION }} floor</td>
                            <td>{{ $details->PR_MEASURMENT }} Sq</td>
                            <td>{{ number_format($pr_dtls->LOWEST_TENDER_MONEY,2) }}</td>
                            <?php
                            $tender_particular = DB::table('tender_particulars')
                                    ->where('SCHEDULE_ID', $pr_dtls->SCHEDULE_ID)
                                    ->get();
                            ?>
                            @foreach($tender_particular as $inx => $tp)
                            <td>
                                {{ $tp->PARTICULAR_AMT }}
                                @if($tp->UOM == 1)
                                {{ trans('common.percent') }}
                                @elseif($tp->UOM == 2)
                                {{ trans('common.taka') }}
                                @endif
                            </td>
                            @endforeach
                        </tr>
                        @endif
                        @endforeach
                    </tbody>
                </table>
            </div>
            @endforeach
        </div>
        @endif
    </div>
    <!--    =============================================-->
    <div class="row tabbable-custom nav-justified" id="pro_dtls_sc_overview">
        @if(count($conditions))
        <ul class="nav nav-tabs nav-justified" role="tablist">
            @foreach($conditions as $key => $condi)
            <li role="presentation" class="{{ $key==0?'active':'' }}">
                <a href="#tab_link_{{ $key }}" aria-controls="tab_link_{{ $key }}" role="tab" data-toggle="tab"><h4>{{ $condi->getCategory->CATE_NAME }} এর শর্ত সমূহ</h4></a>
            </li>
            @endforeach
        </ul>
        <div class="tab-content">
            @foreach($conditions as $key => $condi)
            <div role="tabpanel" class="tab-pane {{ $key==0?'active':'' }}" id="tab_link_{{ $key }}">
                <ul style="padding-left:18px;">
                    <li>{!! $condi->CON_DESC !!}</li>
                </ul>
            </div>
            @endforeach
        </div>
        @endif
    </div>
</div>