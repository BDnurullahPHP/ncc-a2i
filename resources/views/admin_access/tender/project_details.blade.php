<table class="table table-striped table-bordered table-hover">
    <thead>
        <tr>
            <th class="project_input"><input type="checkbox" class="check_all"></th>
            <th>{{ trans('project.market_name') }}</th>
            <th>{{ trans('common.number') }}</th>
            <th>{{ trans('common.location') }}</th>
            <th>{{ trans('common.measurement') }}</th>
            <th style="width: 22%;" class="schedule_input text-center">
                {{ trans('tender.min_bid_amount') }} <span class="required_field">*</span><br>
                &#40;{{ trans('common.taka') }}&#41;
            </th>
            @foreach ($cat_particular as $key => $cp)
                <th style="width: 15%;" class="schedule_input text-center">
                    {{ $cp->getParticular->PARTICULAR_NAME }} <span class="required_field">*</span><br>
                    @if($cp->UOM == 1)
                        &#40;{{ trans('common.percent') }}&#41;
                    @elseif($cp->UOM == 2)
                        &#40;{{ trans('common.taka') }}&#41;
                    @endif
                </th>
            @endforeach
        </tr>
    </thead>
    <tbody>
        @foreach($project_details as $key => $details)
            <tr <?php if( isset($sc_project) && in_array($details->PR_DETAIL_ID, $sc_project, true) ) echo 'class="selected"' ?> >
                @if(isset($tender_schedule))
                    <input type="hidden" value="{{ Helpers::array_key_match($details->PR_DETAIL_ID, $tender_schedule) }}" class="schedule_id">
                @endif
                <td class="project_input">
                    <label>
                        @if( isset($sc_project) && in_array($details->PR_DETAIL_ID, $sc_project, true) )
                            <input type="checkbox" data-url="{{ route('tender_schedule_delete', $details->PR_DETAIL_ID) }}" checked="checked" class="remove_schedule">
                        @else
                            <input type="checkbox">
                        @endif
                    </label>
                </td>
                <td class="data_td td_project_name">{{$details->getProject->PR_NAME_BN}}</td>
                <td class="data_td td_pr_ssf_no">
                    <input type="hidden" class="project_type" value="{{ $details->PR_TYPE }}">
                    <input type="hidden" class="project_details" value="{{ $details->PROJECT_ID }},{{ $details->PR_DETAIL_ID }}">
                    <input type="hidden" class="project_category" value="{{ $details->PR_CATEGORY }}">
                    <span>{{ Helpers::numberConvert($details->PR_SSF_NO) }}</span>
                </td>
                <td class="data_td td_position">
                    <span>{{ $details->getFloor->FLOOR_NUMBER_BN }}</span>
                </td>
                <td class="data_td td_pr_measurment">
                    <input type="hidden" value="1">
                    <span>{{ Helpers::numberConvert(number_format($details->PR_MEASURMENT,2)) }}</span>
                </td>
                <td class="data_td td_min_bid_amount schedule_input">
                    <input type="hidden" class="uom" value="2">
                    @if(isset($bid_amount))
                        {!! Form::text('min_bid_amount_'.$details->PR_CATEGORY.$key, Helpers::array_key_match($details->PR_DETAIL_ID, $bid_amount), array('class'=>'form-control min_bid_amount schedule-required')) !!}
                    @else
                        {!! Form::text('min_bid_amount_'.$details->PR_CATEGORY.$key, null, array('class'=>'form-control min_bid_amount schedule-required')) !!}
                    @endif
                </td>
                @foreach ($cat_particular as $k => $cp)
                    @if(isset($tendr_part))
                        <?php 
                            $partcular_arr = Helpers::array_key_match($details->PR_DETAIL_ID, $tendr_part);
                        ?>
                    @endif
                    <?php 
                        if (isset($tendr_part)){
                        $te_sc_id = Helpers::array_key_match($details->PR_DETAIL_ID, $tender_schedule);
                        $particular_mt = DB::table('tender_particulars')
                                            ->where('SCHEDULE_ID', $te_sc_id)
                                            ->where('PARTICULAR_ID', $cp->PARTICULAR_ID)
                                            ->pluck('PARTICULAR_AMT');
                        }  else {
                            $particular_mt = $cp->PARTICULAR_AMT;
                        }
                    ?>
                    @if($cp->UOM == 1)
                        <td class="data_td td_guarantee schedule_input">
                            <input type="hidden" class="particular" value="{{ $cp->PARTICULAR_ID }}">
                            <input type="hidden" class="uom" value="{{ $cp->UOM }}">
                            @if(isset($tendr_part) && isset($partcular_arr) && isset($partcular_arr[$k]))
                                <input type="hidden" value="{{ $partcular_arr[$k] }}" class="particular_id">
                            @endif
                            {!! Form::text('guarantee_'.$details->PR_CATEGORY.$key, $particular_mt, array('class'=>'form-control guarantee schedule-required')) !!}
                        </td>
                    @elseif($cp->UOM == 2)
                        <td class="data_td td_schedule_price schedule_input">
                            <input type="hidden" class="particular" value="{{ $cp->PARTICULAR_ID }}">
                            <input type="hidden" class="uom" value="{{ $cp->UOM }}">
                            @if(isset($tendr_part) && isset($partcular_arr) && isset($partcular_arr[$k]))
                                <input type="hidden" value="{{ $partcular_arr[$k] }}" class="particular_id">
                            @endif
                            {!! Form::text('schedule_price_'.$details->PR_CATEGORY.$key, $particular_mt, array('class'=>'form-control schedule_price schedule-required')) !!}
                        </td>
                    @endif
                @endforeach
            </tr>
        @endforeach
    </tbody>
</table>