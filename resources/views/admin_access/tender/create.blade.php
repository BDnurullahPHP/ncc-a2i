<!--=============Title for modal===============-->
<div class="modal_top_title">{{ trans('tender.add_data') }}</div>
<!--===========End title for modal=============-->

<div id="form_wizard_1">
    <div class="form-wizard">
        <div class="form-body">

            @include('admin_access.tender.wizard_header')

            <div class="tab-content">
                <!--========Show form post message=============-->
                <div class="row">
                    <div class="col-md-6 col-sm-6 form_messsage">
                        
                    </div>
                </div>
                <!--========End form post message=============-->
                <div class="tab-pane active" id="tab1">
                    <div class="row">
                        <div class="col-sm-6" style="padding-bottom:10px;">
                            {!! Form::label('is_it_retender', trans('lease.re_tender')) !!}
                            {!! Form::checkbox('is_it_retender', '1', false, ['id'=>'is_it_retender']) !!}
                        </div>
                    </div>
                    <div class="row">
                        <span id="project_list">
                        <div class="col-sm-6">
                            {!! Form::label('PROJECT_ID', trans('tender.tender_project')) !!} <span class="required_field">*</span>
                            {!! Form::select('PROJECT_ID[]', $project_list, null, array('id'=>'project_id_add', 'class'=>'form-control multi_select form-required', 'multiple'=>'multiple')) !!}
                        </div>
                        <div class="col-sm-6">
                            <span class="btn btn-info" id="project_id_add">Search</span>
                        </div>
                        </span>
                        <!-- <div class="col-sm-6" id="project_list">
                            {!! Form::label('PROJECT_ID', trans('tender.tender_project')) !!} <span class="required_field">*</span>
                            {!! Form::select('PROJECT_ID', $project_list, null, array('id'=>'project_id', 'class'=>'form-control form-required', 'placeholder' => trans('common.form_select'))) !!}
                        </div> -->
                        <div class="col-sm-6 hidden" id="tender_list">
                            {!! Form::label('PRE_TENDER_ID', trans('tender.tender_number')) !!} <span class="required_field">*</span>
                            <select name="PRE_TENDER_ID" id="pre_tender_list" class="form-control">
                                <option value="">{{ trans('common.form_select') }}</option>
                                @foreach($tender_list as $key => $tender)
                                    <option value="{{ $tender->TENDER_ID }}">{{ $tender->TENDER_NO }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-sm-2">
                            <img src="{{ Helpers::asset('assets/img/loader.gif') }}" alt="" class="form_loader">
                        </div>
                    </div>
                    <div class="row tab-scroll margin-top-20" id="content_body">
                        
                    </div>
                </div>
                <div class="tab-pane tab-scroll row" id="tab2">
                    @include('admin_access.tender.tender_fields')
                </div>
                <div class="tab-pane tab-scroll row" id="tab3"></div>
                <div class="tab-pane tab-scroll row" id="tab4"></div>
                <div class="tab-pane tab-scroll row" id="tab5">
                    @include('admin_access.tender.tender_confirm')
                </div>
            </div>
        </div>
        <div class="form-actions margin-top-20">
            <div class="row">
                <div class="col-md-12">
                    <div class="button_container pull-right">
                        <a href="javascript:;" class="btn default button-previous">
                            <i class="fa fa-angle-left"></i> {{ trans('common.previous') }}
                        </a>
                        <a href="javascript:;" class="btn btn-outline green button-next">
                            {{ trans('common.next') }} <i class="fa fa-angle-right"></i>
                        </a>
                        <a href="javascript:;" class="btn green button-submit">
                            {{ trans('common.form_submit') }} <i class="fa fa-check"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>