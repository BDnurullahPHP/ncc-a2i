{!! Form::open(array('url' => '#', 'id'=>'project_confirm', 'class'=>'form-horizontal', 'method' => 'post')) !!}
	
	<div class="col-md-12 col-sm-12 col-xs-12">
		<h4>{{ trans('tender.tender_information') }}</h4>
	</div>
	<div class="row no-margin">
		<div class="col-md-6 col-sm-6 col-xs-12">
			<div class="form-group" id="tc_project_name">
				{!! Form::label('', trans('tender.tender_project'), array('class'=>'col-sm-6')) !!}
				<div class="col-md-6 col-sm-6">
			        <p></p>
			    </div>
			</div>
		</div>
		<div class="col-md-6 col-sm-6 col-xs-12">
			<div class="form-group" id="tc_number">
				{!! Form::label('', trans('tender.tender_number'), array('class'=>'col-sm-6')) !!}
				<div class="col-md-6 col-sm-6">
			        <p></p>
			    </div>
			</div>
		</div>
	</div>

	<div class="row no-margin">
		<div class="col-md-6 col-sm-6 col-xs-12">
			<div class="form-group" id="tc_method">
				{!! Form::label('', trans('tender.tender_method'), array('class'=>'col-sm-6')) !!}
				<div class="col-md-6 col-sm-6">
			        <p></p>
			    </div>
			</div>
		</div>
		<div class="col-md-6 col-sm-6 col-xs-12">
			<div class="form-group" id="tc_name">
				{!! Form::label('', trans('tender.tender_name'), array('class'=>'col-sm-6')) !!}
				<div class="col-md-6 col-sm-6">
			        <p></p>
			    </div>
			</div>
		</div>
	</div>

	<div class="row no-margin">
		<div class="col-md-6 col-sm-6 col-xs-12">
			<div class="form-group" id="tc_date">
				{!! Form::label('', trans('tender.tender_date'), array('class'=>'col-sm-6')) !!}
				<div class="col-md-6 col-sm-6">
			        <p></p>
			    </div>
			</div>
		</div>
		<div class="col-md-6 col-sm-6 col-xs-12">
			<div class="form-group" id="tc_publish_date">
				{!! Form::label('', trans('tender.tender_publish_date'), array('class'=>'col-sm-6')) !!}
				<div class="col-md-6 col-sm-6">
			        <p></p>
			    </div>
			</div>
		</div>
	</div>

	<div class="row no-margin">
		<div class="col-md-6 col-sm-6 col-xs-12">
			<div class="form-group" id="tc_last_selling_date">
				{!! Form::label('', trans('tender.sc_last_selling_date'), array('class'=>'col-sm-6')) !!}
				<div class="col-md-6 col-sm-6">
			        <p></p>
			    </div>
			</div>
		</div>
		<div class="col-md-6 col-sm-6 col-xs-12">
			<div class="form-group" id="tc_last_selling_time">
				{!! Form::label('', trans('tender.sc_last_selling_time'), array('class'=>'col-sm-6')) !!}
				<div class="col-md-6 col-sm-6">
			        <p></p>
			    </div>
			</div>
		</div>
	</div>

	<div class="row no-margin">
		<div class="col-md-6 col-sm-6 col-xs-12">
			<div class="form-group" id="tc_last_receive_date">
				{!! Form::label('', trans('tender.te_last_receive_date'), array('class'=>'col-sm-6')) !!}
				<div class="col-md-6 col-sm-6">
			        <p></p>
			    </div>
			</div>
		</div>
		<div class="col-md-6 col-sm-6 col-xs-12">
			<div class="form-group" id="tc_opening_date">
				{!! Form::label('', trans('tender.te_opening_dt'), array('class'=>'col-sm-6')) !!}
				<div class="col-md-6 col-sm-6">
			        <p></p>
			    </div>
			</div>
		</div>
	</div>

    <div class="row no-margin">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="form-group" id="tc_location">
				{!! Form::label('', trans('tender.tender_sc_location'), array('class'=>'col-sm-3')) !!}
				<div class="col-md-6 col-sm-9">
			        <ul style="padding-left:18px;"></ul>
			    </div>
			</div>
		</div>
	</div>

	<div class="row no-margin">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="form-group" id="tc_copy_location">
				{!! Form::label('', trans('tender.tender_copy_location'), array('class'=>'col-sm-3')) !!}
				<div class="col-md-6 col-sm-9">
			        <ul style="padding-left:18px;"></ul>
			    </div>
			</div>
		</div>
	</div>

	{{-- <div class="row no-margin">
		<div class="col-md-6 col-sm-6 col-xs-12">
			<div class="form-group" id="tc_no_of_installment">
				{!! Form::label('', trans('tender.no_of_installment'), array('class'=>'col-sm-6')) !!}
				<div class="col-md-6 col-sm-6">
			        <p></p>
			    </div>
			</div>
		</div>
		<div class="col-md-6 col-sm-6 col-xs-12">
			<div class="form-group" id="tc_days_btn_installment">
				{!! Form::label('', trans('tender.days_btn_installment'), array('class'=>'col-sm-6')) !!}
				<div class="col-md-6 col-sm-6">
			        <p></p>
			    </div>
			</div>
		</div>
	</div> --}}
	
	<!--==========Table data=======-->
	<div class="form-group no-margin" id="pro_dtls_sc_overview"></div>
	<div class="form-group no-margin" id="te_rules_overview"></div>

{!! Form::close() !!}