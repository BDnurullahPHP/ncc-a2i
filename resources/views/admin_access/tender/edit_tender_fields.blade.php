{!! Form::model($tender_data, array('url' => '#', 'id'=>'tender_form', 'class'=>'form-horizontal', 'method' => 'post')) !!}
    
    <div class="form-group no-margin">
        <div class="col-sm-6">
            {!! Form::label('TENDER_NO', trans('tender.tender_number')) !!} <span class="required_field">*</span>
            {!! Form::text('TENDER_NO', null, array('id'=>'tender_no', 'class'=>'form-control tender-required', 'placeholder'=>trans('tender.tender_number_pl'))) !!}
        </div>
        <div class="col-sm-6">
            {!! Form::label('TENDER_DT', trans('tender.tender_date')) !!} <span class="required_field">*</span>
            {!! Form::text('TENDER_DT', date('d-m-Y', strtotime($tender_data->TENDER_DT)), array('id'=>'tender_dt', 'class'=>'form-control tender-required date_picker_default', 'placeholder'=>trans('common.date_pl'))) !!}
        </div>
    </div>
    <div class="form-group no-margin">
        <div class="col-sm-6">
            {!! Form::label('TENDER_TITLE', trans('tender.tender_name')) !!} <span class="required_field">*</span>
            {!! Form::text('TENDER_TITLE', null, array('id'=>'tender_title', 'class'=>'form-control tender-required', 'placeholder'=>trans('tender.tender_name'))) !!}
        </div>
        <div class="col-sm-6">
            {!! Form::label('TENDER_METHOD', trans('tender.tender_method')) !!} <span class="required_field">*</span>
            {!! Form::select('TENDER_METHOD', [1=>'Open Tender Method(OTM)'], null, array('id'=>'tender_method', 'class'=>'form-control tender-required', 'placeholder' => trans('common.form_select'))) !!}
        </div>
    </div>
    <div class="form-group no-margin">
        <div class="col-sm-4">
            {!! Form::label('TENDER_PUBLISH_DT', trans('tender.tender_publish_date')) !!} <span class="required_field">*</span>
            {!! Form::text('TENDER_PUBLISH_DT', !empty($tender_date)?date('d-m-Y', strtotime($tender_data->TENDER_PUBLISH_DT)):null, array('id'=>'tender_publish_dt', 'class'=>'form-control date_picker_default tender-required', 'placeholder'=>trans('common.date_pl'))) !!}
        </div>
        <div class="col-sm-4">
            <label for="TENDER_LOCATION">{{ trans('tender.tender_location') }}</label> <span class="required_field">*</span>
            <select name="TENDER_LOCATION" id="tender_location" class="form-control multi_select tender-required tender_location" multiple="multiple" data-placeholder="{{ trans('common.form_select') }}">
                @foreach($location as $key => $lc)
                    @if( in_array($lc->LOCATION_ID, $te_location, true) )
                        <option selected="selected" data-location="{{ $lc->getTenderLocation->TE_LOC_ID }}" value="{{ $lc->LOCATION_ID }}">{{ $lc->LOCATION_NAME }}</option>
                    @else
                        <option value="{{ $lc->LOCATION_ID }}">{{ $lc->LOCATION_NAME }}</option>
                    @endif
                    
                @endforeach
            </select>
            <div id="error_msg1"></div>
        </div>
        <div class="col-sm-4">
            <label for="TENDER_COPY_LOCATION">{{ trans('tender.tender_copy_location') }}</label> <span class="required_field">*</span>
            <select name="TENDER_COPY_LOCATION" id="tender_copy_location" class="form-control multi_select tender-required tender_copy_location" multiple="multiple" data-placeholder="{{ trans('common.form_select') }}">
                @foreach($location as $key => $lc)
                    @if( in_array($lc->LOCATION_ID, $te_copy_location, true) )
                        <option selected="selected" data-location="{{ $lc->getTenderLocation->TE_LOC_ID }}" value="{{ $lc->LOCATION_ID }}">{{ $lc->LOCATION_NAME }}</option>
                    @else
                        <option value="{{ $lc->LOCATION_ID }}">{{ $lc->LOCATION_NAME }}</option>
                    @endif
                    
                @endforeach
            </select>
            <div id="error_msg2"></div>
        </div>
    </div>
    <!--==============================================-->
    <div class="form-group no-margin">
        @if(!empty($tender_date))
            <input type="hidden" class="date_id" value="{{ $tender_date->TENDER_DT_ID }}">
        @endif
        <div class="col-sm-6 te_s_dt">
            {!! Form::label('', trans('tender.sc_last_selling_date')) !!} <span class="required_field">*</span>
            {!! Form::text('tender_last_seling_dt', !empty($tender_date)?date('d-m-Y', strtotime($tender_date->LAST_SELLING_DT_FROM)):null, array('class'=>'form-control tender_seling_range date_picker_default tender-required', 'placeholder'=>trans('common.date_pl'))) !!}
        </div>
        <div class="col-sm-6 date_range_bx">
            {!! Form::label('', trans('tender.sc_last_selling_time')) !!} <span class="required_field">*</span>
            <div class="row">
                <div class="col-sm-5">
                    {!! Form::text('tender_last_seling_time_from', !empty($tender_date)?date('h:m A', strtotime($tender_date->LAST_SELLING_DT_FROM)):null, array('class'=>'form-control tender_last_seling_time_from time_picker tender-required', 'placeholder'=>trans('common.time_pl'))) !!}
                </div>
                <div class="col-sm-2">{{ trans('common.to') }}</div>
                <div class="col-sm-5">
                    {!! Form::text('tender_last_seling_time_to', !empty($tender_date)?date('h:m A', strtotime($tender_date->LAST_SELLING_DT_TO)):null, array('class'=>'form-control tender_last_seling_time_to time_picker tender-required', 'placeholder'=>trans('common.time_pl'))) !!}
                </div>
            </div>
        </div>
    </div>
    <div class="form-group no-margin">
        <div class="row no-margin">
            <div class="col-sm-6 te_lst_receive_dt">
                {!! Form::label('', trans('tender.te_last_receive_date')) !!} <span class="required_field">*</span>
                {!! Form::text('tender_last_receive_dt', !empty($tender_date)?date('d-m-Y h:m A', strtotime($tender_date->TE_LAST_RECIEVE_DT)):null, array('class'=>'form-control tender_last_receive_dt date_time_picker tender-required', 'placeholder'=>trans('common.date_time_pl'))) !!}
            </div>
            <div class="col-sm-6 te_opn_dt">
                {!! Form::label('', trans('tender.te_opening_dt')) !!} <span class="required_field">*</span>
                {!! Form::text('tender_opening_dt', !empty($tender_date)?date('d-m-Y h:m A', strtotime($tender_date->TE_OPENING_DT)):null, array('class'=>'form-control tender_opening_dt date_time_picker tender-required', 'placeholder'=>trans('common.date_time_pl'))) !!}
            </div>
        </div>
    </div>
    <!--==============================================-->
    <div class="form-group no-margin">
        <div class="col-sm-12">
            {!! Form::label('TENDER_DESC', trans('tender.tender_details')) !!}
            {!! Form::textarea('TENDER_DESC', null, array('rows' =>'5', 'id'=>'tender_desc', 'class'=>'form-control reactor_basic', 'placeholder'=>trans('tender.tender_details'))) !!}
        </div>
    </div>
    {{-- <div class="form-group no-margin">
        <div class="col-sm-6">
            {!! Form::label('IS_ACTIVE', trans('tender.is_current')) !!}
            <label class="control-label">
                {!! Form::checkbox('IS_ACTIVE', 1, null, ['class'=>'status']) !!}
            </label>
        </div>
    </div> --}}

{!! Form::close() !!}