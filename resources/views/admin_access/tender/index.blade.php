@extends('admin_access.layout.master')

@section('css_file')

<link href="{{ Helpers::asset('assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ Helpers::asset('assets/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css') }}" rel="stylesheet" type="text/css" />

@endsection

@section('style')

<link rel="stylesheet" href="{{ Helpers::asset('assets/admin/css/tender.css') }}">
<style type="text/css">
    .project_details label{
        font-weight: 700;
    }
    .project_details p{
        margin: 0px;
    }
</style>

@endsection

@section('js_file')

<script src="{{ Helpers::asset('assets/global/plugins/moment.min.js') }}" type="text/javascript"></script>
<script src="{{ Helpers::asset('assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js') }}" type="text/javascript"></script>
<script src="{{ Helpers::asset('assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js') }}" type="text/javascript"></script>
<script src="{{ Helpers::asset('assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js') }}" type="text/javascript"></script>
<script src="{{ Helpers::asset('assets/global/plugins/bootstrap-wizard/jquery.bootstrap.wizard.min.js') }}" type="text/javascript"></script>

@endsection

@section('script')

<script type="text/javascript">
var project_category_url = "{{ route('tender_project_category') }}";
var project_details_url = "{{ route('tender_project_details') }}";
var cat_condition_url = "{{ route('category_conditions') }}";
var tender_create_url = "{{ route('tender_create') }}";
</script>
<script src="{{ Helpers::asset('assets/admin/js/tender.js') }}" type="text/javascript"></script>

@endsection

@section('content')

<h3>{{ trans('tender.tender_list') }}
    <small></small>
    @if($access->CREATE == 1)
    <a href="#" data-toggle="modal" data-target="#tender_add_content" class="btn btn-primary btn-sm pull-right" data-form="{{ route('tender_create_form') }}">{{ trans('common.add_new') }} <i class="fa fa-plus"></i></a>
    @endif	
</h3>

<div class="portlet light bordered">
    <!-- <div class="portlet-title">
        <div class="caption">
            
        </div>
        <div class="tools"> </div>
    </div> -->
    <div class="row">
        <div class="col-md-5 col-sm-6 col-xs-12">
            <div id="lg_form_message"></div>
        </div>
    </div>
    <div class="portlet-body contentArea">

        <table class="table table-striped table-bordered table-hover" data-source="{{ route('tender_data') }}" id="common_table">
            <thead>
                <tr>
                    <th>{{ trans('common.table_sl') }}</th>
                    <th style="width: 18%;">{{ trans('tender.tender_number') }} / {{ trans('tender.tender_date') }}</th>
                    <th style="width: 18%;">{{ trans('tender.tender_name') }}</th>
                    <th style="width: 12%;">{{ trans('tender.tender_publish_date') }}</th>
                    <th>{{ trans('tender.sc_last_selling_date') }}</th>
                    <th>{{ trans('common.table_action') }}</th>
                </tr>
            </thead>
            <tbody>

            </tbody>
        </table>

    </div>
</div>

<!--==============Tender wizard modal=================-->
<div class="modal fade" id="tender_add_content" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">{{ trans('common.add_data') }}</h4>
            </div>
            <div class="modal-body">
                <img src="{{ Helpers::asset('assets/img/loader.gif') }}" alt="" class="loader center-block">
                <div id="body-content">

                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">{{ trans('common.modal_close') }}</button>
            </div>
        </div>
    </div>
</div>
<!--===========End Tender wizard modal================-->

@endsection