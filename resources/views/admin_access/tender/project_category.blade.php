@if(count($project_category) > 0)
@foreach($project_category as $key=> $category)
<div class="col-md-12 col-sm-12 col-xs-12 category_items">

    @if($key==0)
    <div class="portlet box green <?php if (isset($project_cat) && in_array ( $category->PR_CATEGORY, $project_cat, true)) echo "selected" ?>">
        @elseif($key==1)
        <div class="portlet box blue <?php if (isset($project_cat) && in_array($category->PR_CATEGORY, $project_cat, true)) echo "selected" ?>
    ">
            @elseif($key==2)
            <div class="portlet box purple <?php if (isset($project_cat) && in_array($category->PR_CATEGORY, $project_cat, true)) echo "selected" ?>
    ">
                @else
                <div class="portlet box green <?php if (isset ($project_cat) && in_array ( $category->PR_CATEGORY, $project_cat, true)) echo "selected" ?>">
                    @endif
                    <div class="portlet-title">
                        <div class="caption">
                            @if($key==0)
                            <i class="fa fa-shopping-bag"></i>
                            @elseif($key==1)
                            <i class="fa fa-home"></i>
                            @elseif($key==2)
                            <i class="fa fa-building"></i>
                            @else
                            <i class="fa fa-building"></i>
                            @endif
                            {{ $category->getCategory->CATE_NAME }} {{ trans('common._information') }}
                        </div>
                        <div class="tools">
                            <input type="hidden" class="project_id" value="{{ $projects }}">
                            <input type="hidden" class="category_id" value="{{ $category->PR_CATEGORY }}">
                            <a href="#" class="expand" title=""> </a>
                        </div>
                    </div>
                    <div class="portlet-body" style="display: none;">
                        <img src="{{ Helpers::asset('assets/img/loader.gif') }}" alt="" class="box_loader">
                        <div class="project_details_body"></div>
                    </div>
                </div>

            </div>
            @endforeach
            @endif