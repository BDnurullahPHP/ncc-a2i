<!DOCTYPE html>
<html lang="en">
    <head>
        <title></title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
    </head>
    <style>
        body {
            font-family: freeserif,sans-serif,vrinda,solaiman-lipi,monospace;
        }
        h1, h2, h3, h4, p, input, th, td{
            font-family: freeserif,sans-serif,vrinda,solaiman-lipi,monospace;
            font-weight: 400;
            white-space: initial;
        }
        h3{
            font-size: 22px;
            margin-bottom: 10px;
            margin-top: 15px;
        }
        h4{
            font-size: 18px;
            margin-bottom: 10px;
            margin-top: 15px;
            text-align:center; 
        }
        table, th, td {
            border: .1px solid;
            border-collapse: collapse;
            text-align: center;
        }
        #info_table th{
            text-align: left;
            width: 20%;
        }
        #info_table td{
            text-align: left;
            width: 30%;            
        }

    </style>
    <body>
        <h4>
            <bold>নারায়ণগঞ্জ সিটি করপোরেশন</bold>
            <br/>নগর ভবন, ১০ বঙ্গবন্ধু রোড
            <br/>নারায়ণগঞ্জ
            <br/>www.ncc.gov.bd
        </h4>
        <table style="border:none;">
            <tr style="border:none;">
                <td style="width: 80%; text-align: left">স্মারক নং- {{ $tender->TENDER_NO }}</td>
                <td style="width: 20%; text-align: right;">তারিখঃ {{ Helpers::en2bn(date('d/m/Y', strtotime($tender->TENDER_DT))) }} খ্রিঃ</td>
            </tr>
        </table>
        <h3 style="text-align: center;">{{ $tender->TENDER_TITLE }} :-</h3>
        <p>
            এতদ্বারা সর্বসাধারণের অবগতির জন্য জানানো যাচ্ছে যে, নারায়ণগঞ্জ সিটি কর্পোরেশনের মালিকানাধীন নির্মিত/ নির্মাণাধীন নি¤ড়ব বর্ণিত
            মার্কেটের দোকান/স্পেস সর্বোচ্চ সালামী ও নির্ধারিত ভাড়ায় বরাদ্দ প্রদানের লক্ষ্যে প্রকৃত ব্যবসায়ী/আগ্রহী ব্যক্তিগণের নিকট হতে দরপত্র
            আহবান করা যাচ্ছে।
        </p>
        <h4>দোকান/স্পেসের বিবরণ</h4>
        <p>{!! $tender->TENDER_DESC !!}</p>

        <table id="info_table" style="width: 100%">
            <tr>
                <th>{{  trans('tender.tender_publish_date') }}:</th>
                <td>{{ Helpers::en2bn(date('d/m/Y', strtotime($tender->TENDER_PUBLISH_DT))) }}</td>
                <th>{{ trans('tender.sc_last_selling_date') }}:</th>
                <td>{{ Helpers::en2bn(date('d/m/Y', strtotime($tender_date->LAST_SELLING_DT_FROM))).' - '.Helpers::en2bn(date('d/m/Y', strtotime($tender_date->LAST_SELLING_DT_TO))) }}</td>
            </tr>
            <tr>
                <th>{{  trans('tender.sc_last_selling_time') }} :</th>
                <td>{{ Helpers::en2bn(date('h:m A', strtotime($tender_date->LAST_SELLING_DT_FROM))).' - '.Helpers::en2bn(date('h:m A', strtotime($tender_date->LAST_SELLING_DT_TO))) }}</td>
                <th>{{ trans('tender.te_last_receive_date') }} :</th>
                <td>{{ Helpers::en2bn(date('d/m/Y h:m A', strtotime($tender_date->TE_LAST_RECIEVE_DT))) }}</td>
            </tr>
            <tr>
                <th>{{  trans('tender.te_opening_dt') }}: </th>
                <td>{{ Helpers::en2bn(date('d/m/Y h:m A', strtotime($tender_date->TE_OPENING_DT))) }}</td>
                <th>{{ trans('tender.tender_location') }}</th>
                <td>
                    <ul style="padding-left:18px;">
                        @foreach($tender_location as $key => $loc)
                        <li>{{ $loc->getLocation->LOCATION_NAME }}</li>
                        @endforeach
                    </ul>
                </td>
            </tr>
        </table>
        @foreach($project_category as $key => $pr_cat)
        <h4>{{ $pr_cat->getCategory->CATE_NAME }} {{ trans('common._information') }}</h4>
        <table style="width: 100%">
            <thead>
                <tr>
                    <th>{{ trans('common.number') }}</th>
                    <th>{{ trans('common.location') }}</th>
                    <th>{{ trans('common.measurement') }}</th>
                    <th>
                        {{ trans('tender.min_bid_amount') }}<br>
                        &#40;{{ trans('common.taka') }}&#41;
                    </th>
                    <?php
                    $cat_particular = DB::table('sa_category_particulars')
                            ->where('PR_CATEGORY', $pr_cat->PR_CATEGORY)
                            ->orderBy('PARTICULAR_ID', 'desc')
                            ->get();
                    ?>
                    @foreach($cat_particular as $k => $cat_prt)
                    <?php
                    $particular_name = DB::table('ac_particular')
                            ->where('PARTICULAR_ID', $cat_prt->PARTICULAR_ID)
                            ->pluck('PARTICULAR_NAME');
                    ?>
                    <th>{{ $particular_name }}</th>
                    @endforeach
                </tr>
            </thead>
            <tbody>
                @foreach($project_details as $index => $pr_dtls)
                <?php
                $details = DB::table('project_details')
                        ->where('PR_CATEGORY', $pr_cat->PR_CATEGORY)
                        ->where('PR_DETAIL_ID', $pr_dtls->PR_DETAIL_ID)
                        ->first();
                ?>
                @if(!empty($details))
                <tr>
                    <td>{{ Helpers::en2bn($details->PR_SSF_NO) }}</td>
                    <td>{{ Helpers::en2bn($details->POSITION) }} তলা</td>
                    <td>{{ Helpers::en2bn($details->PR_MEASURMENT) }}</td>
                    <td>{{ Helpers::en2bn(number_format($pr_dtls->LOWEST_TENDER_MONEY,2)) }}</td>
                    <?php
                    $tender_particular = DB::table('tender_particulars')
                            ->where('SCHEDULE_ID', $pr_dtls->SCHEDULE_ID)
                            ->get();
                    ?>
                    @foreach($tender_particular as $inx => $tp)
                    <td>
                        {{ Helpers::en2bn($tp->PARTICULAR_AMT) }}
                        @if($tp->UOM == 1)
                        {{ trans('common.percent') }}
                        @elseif($tp->UOM == 2)
                        {{ trans('common.taka') }}
                        @endif
                    </td>
                    @endforeach
                </tr>
                @endif
                @endforeach
            </tbody>
        </table>
        @endforeach
        <br/>
        @foreach($conditions as $key => $condi)
        <h4>{{ $condi->getCategory->CATE_NAME }} এর শর্ত সমূহ</h4>
        <ul style="padding-left:18px;">
            <li>{!! $condi->CON_DESC !!}</li>
        </ul>
        @endforeach
        <p style="text-decoration: underline;"> 
            সদয় অবগতি/প্রয়োজনীয় ব্যবস্থা গ্রহণের জন্য অনুলিপি প্রেরণ করা হলো
        </p>
        <ul style="padding-left:18px;">
            @foreach($onulipy_location as $key => $loc)
            <li>{{ $loc->getLocation->LOCATION_NAME }}</li>
            @endforeach
        </ul>
    </body>
</html>




