<!--=============Title for modal===============-->
<div class="modal_top_title">{{ trans('tender.add_data') }}</div>
<!--===========End title for modal=============-->

<div id="form_wizard_1" class="form_wizard_edit">
    <div class="form-wizard">
        <div class="form-body">

            @include('admin_access.tender.wizard_header')

            <div class="tab-content">
                <!--========Show form post message=============-->
                <div class="row">
                    <div class="col-md-6 col-sm-6 form_messsage">
                        
                    </div>
                </div>
                <!--========End form post message=============-->
                <div class="tab-pane active" id="tab1">
                    <div class="row">
<!--                        <div class="col-sm-6">
                            <input type="hidden" class="tender_id" value="{{ $tender_id }}">
                            {!! Form::label('PROJECT_ID', trans('tender.tender_project')) !!} <span class="required_field">*</span>
                            {!! Form::select('PROJECT_ID', $project_list, $schedule->PROJECT_ID, array('id'=>'project_id', 'class'=>'form-control form-required', 'placeholder' => trans('common.form_select'))) !!}
                        </div>-->
                         <div class="col-sm-6">
                             <input type="hidden" class="tender_id" value="{{ $tender_id }}">
                             {!! Form::label('PROJECT_ID', trans('tender.tender_project')) !!} <span class="required_field">*</span>
                            <select class="form-control form-required multi_select" name="PROJECT_ID" id="project_id" multiple="multiple">
                                @foreach ($project_list as $key => $value)
                                    <option value="{{ $value->PROJECT_ID }}"  {{ in_array($value->PROJECT_ID, (array)$p_id) ? 'selected="selected"' : '' }} > {{ $value->PR_NAME_BN }} </option>
                                @endforeach
                            </select>
<!--                            <div id="error_msg1"></div>-->
                        </div>
                        <div class="col-sm-6">
                            <img src="{{ Helpers::asset('assets/img/loader.gif') }}" alt="" class="form_loader">
                        </div>
                    </div>
                    <div class="row tab-scroll margin-top-20" id="content_body">
                        
                    </div>
                </div>
                <div class="tab-pane tab-scroll row" id="tab2">
                    @include('admin_access.tender.edit_tender_fields')
                </div>
                <div class="tab-pane tab-scroll row" id="tab3"></div>
                <div class="tab-pane tab-scroll row" id="tab4"></div>
                <div class="tab-pane tab-scroll row" id="tab5">
                    @include('admin_access.tender.tender_confirm')
                </div>
            </div>
        </div>
        <div class="form-actions margin-top-20">
            <div class="row">
                <div class="col-md-12">
                    <div class="button_container pull-right">
                        <a href="javascript:;" class="btn default button-previous">
                            <i class="fa fa-angle-left"></i> {{ trans('common.previous') }}
                        </a>
                        <a href="javascript:;" class="btn btn-outline green button-next">
                            {{ trans('common.next') }} <i class="fa fa-angle-right"></i>
                        </a>
                        <a href="javascript:;" class="btn green button-submit">
                            {{ trans('common.form_submit') }} <i class="fa fa-check"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>