{!! Form::model($leaseDemesne, array('url' => '#', 'id'=>'demesne_form', 'class'=>'form-horizontal', 'method' => 'post')) !!}
<!--=============Title for modal===============-->
<div class="modal_top_title">{{ trans('lease.edit_mahal_title') }}</div>
<!--===========End title for modal=============-->

<!--========Show form post message=============-->
<div class="form-group">
    <div class="col-sm-6 form_messsage">

    </div>
</div>
<!--========End form post message=============-->

<div class="form-group">
    <div class="col-md-3">
        {!! Form::label('tender_no', trans('lease.tender_number')) !!} <span class="required_field">*</span>
    </div>
    <div class="col-md-6">
        <select class="form-control form-required" id="tender_no" name="tender_no">
                <option value="">{{ trans('common.form_select') }}</option>
                @foreach ($tender as $value)
                    <option <?php if($value->TENDER_ID==$tender_id) echo 'selected="selected"'; ?> value="{{ $value->TENDER_ID }}">{{ $value->TENDER_NO }}</option>
                @endforeach
        </select>
        <div id="error_msg1"></div>
    </div>
</div>
<div class="form-group">
    <div class="col-md-3">
        {!! Form::label('SCHEDULE_ID', trans('lease.mahal_bn_name')) !!} <span class="required_field">*</span>
    </div>
    <div class="col-md-6">
        <select name="SCHEDULE_ID" id="mahal_id" class="form-control form-required">
                    <option value="">{{ trans('common.form_select') }}</option>
                @foreach ($mohal as $value)
                    <option <?php if($value->SCHEDULE_ID == $leaseDemesne->SCHEDULE_ID) echo 'selected="selected"';?> value="{{$value->SCHEDULE_ID}}">{{$value->getProject->PR_NAME_BN}}</option>
                @endforeach
            </select>
        <div id="error_msg1"></div>
    </div>
</div>
<div class="form-group">
    <div class="col-md-3">
        {!! Form::label('NAME', trans('lease.collector_name')) !!} <span class="required_field">*</span>
    </div>
    <div class="col-md-6">
        {!! Form::text('NAME', null, array('class'=>'form-control form-required', 'placeholder'=>'মোঃ করিম')) !!}
    </div>
</div>
<div class="form-group">
    <div class="col-md-3">
        {!! Form::label('PHONE', trans('lease.collector_mobile')) !!} <span class="required_field">*</span>
    </div>
    <div class="col-md-6">
        {!! Form::text('PHONE', null, array('class'=>'form-control form-required', 'placeholder'=>'+880')) !!}
    </div>
</div>
<div class="form-group">
    <div class="col-md-3">
        {!! Form::label('ADDRESS', trans('lease.collector_address')) !!} <span class="required_field">*</span>
    </div>
    <div class="col-md-9">
        {!! Form::textarea('ADDRESS', null, array('class'=>'form-control reactor_basic form-required', 'placeholder'=>'বঙ্গবন্ধু সড়ক, নারায়ণগঞ্জ, বাংলাদেশ')) !!}
    </div>
</div>
<h3>খাসের  মেয়াদ</h3>
<div class="form-group">
    <div class="col-md-3">
        <label>শুরুর তারিখ</label> <span class="required_field">*</span>
    </div>
    <div class="col-md-3">
        <input type="text" class="form-control form-required date_picker validDate" id="from_date_id" value="{{ $leaseDemesne->getValidity->DATE_FROM != '' ? date_format(date_create($leaseDemesne->getValidity->DATE_FROM), 'd-m-Y'): '' }}" name="DATE_FROM" placeholder="Ex.20-07-2016">
    </div>
    <div class="col-md-3">
        <label>শেষ তারিখ</label> <span class="required_field">*</span>
    </div>
    <div class="col-md-3">
        <input type="text" class="form-control form-required date_picker validDate" id="to_date_id" value="{{ $leaseDemesne->getValidity->DATE_TO != '' ? date_format(date_create($leaseDemesne->getValidity->DATE_TO), 'd-m-Y') : '' }}" name="DATE_TO" placeholder="Ex.20-07-2016">
    </div>
</div>
<div class="form-group">
    <div class="col-md-2">
        {!! Form::label('is_active', trans('common.is_active')) !!}<br/>
    </div>
    <div class="col-md-6">
        @if($leaseDemesne->IS_ACTIVE == 1)
        {!! Form::checkbox('IS_ACTIVE', 1, true, array('id'=>'project_is_active')) !!}
        @else
        {!! Form::checkbox('IS_ACTIVE', 1, false, array('id'=>'project_is_active')) !!}
        @endif
    </div>
</div>

<div class="form-group">
    <div class="col-md-12" style="padding-top: 15px;">
        <span class="modal_msg pull-left"></span>
        <button type="submit" data-action="{{ route('lease_demesne_update', $leaseDemesne->LEASE_DE_ID) }}"" class="btn btn-success btn-sm pull-right form_submit" style="margin-left: 5px;">{{ trans('common.form_submit') }}</button>
        <button type="reset" class="btn btn-default btn-sm reset_form pull-right">{{ trans('common.form_reset') }}</button>
        <span class="loadingImg"></span>
    </div>
</div>

{!! Form::close() !!}