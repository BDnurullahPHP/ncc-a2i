{!! Form::model($ward, array('url' => '#', 'id'=>'ward_form', 'class'=>'form-horizontal', 'method' => 'post')) !!}
    
    <!--========Show form post message=============-->
    <div class="form-group">
        <div class="col-sm-5 col-sm-offset-3 form_messsage">
            
        </div>
    </div>
    <!--========End form post message=============-->
    
    <div class="form-group">
        <label for="zone" class="col-sm-3 control-label">{{ trans('common.table_head_category') }} <span style="color: red">*</span></label>
        <div class="col-sm-5">
            <select name="zone" id="zone" class="form-control form-required">
                <option value="">{{ trans('common.form_select') }}</option>
                @foreach ($zone as $zon)
                    <option {{ $zon->WARD_ID==$ward->PARENT_ID?'selected="selected"':'' }} value="{{ $zon->WARD_ID }}">{{ $zon->WARD_NAME_BN }}</option>
                @endforeach
            </select>
        </div>
         <span class="help_tooltip" data-toggle="tooltip" data-placement="top" title="{{ trans('land.zone_bn_name_ex') }}"><i class="fa fa-question-circle" aria-hidden="true"></i></span>
    </div>

  <div class="form-group">
        <label for="WARD_NAME" class="col-sm-3 control-label">{{ trans('common.table_head_name') }} <span style="color: red">*</span></label>
        <div class="col-sm-5">
            {!! Form::text('WARD_NAME', Input::old('WARD_NAME'), array('class'=>'form-control form-required', 'placeholder'=>trans('land.ward_name_ex'))) !!}
        </div>
    </div>
    <div class="form-group">
        <label for="WARD_NAME_BN" class="col-sm-3 control-label">{{ trans('common.table_head_bn_name') }} <span style="color: red">*</span></label>
        <div class="col-sm-5">
            {!! Form::text('WARD_NAME_BN', Input::old('WARD_NAME_BN'), array('class'=>'form-control form-required', 'placeholder'=>trans('land.ward_bn_name_ex'))) !!}
        </div>
    </div>
    <div class="hr-line-dashed"></div>
    <div class="form-group">
        <label for="IS_ACTIVE" class="col-sm-3 control-label">{{ trans('common.is_active') }}</label>
        <div class="col-sm-7">
            <label class="control-label">
                {!! Form::checkbox('IS_ACTIVE', '1', true, ['class'=>'status']) !!}
            </label>
        </div>
    </div>
    <div class="form-group">
        <div class="col-lg-offset-3 col-lg-10">
            <span class="modal_msg pull-left"></span>
                <button type="button" class="btn btn-default btn-sm reset_form">{{ trans('common.form_reset') }}</button>
                <button type="submit" data-action="{{ route('ward_update', $ward->WARD_ID) }}" class="btn btn-success btn-sm form_submit">{{ trans('common.form_submit') }}</button>
            <span class="loadingImg"></span>
        </div>
    </div>
{!! Form::close() !!}