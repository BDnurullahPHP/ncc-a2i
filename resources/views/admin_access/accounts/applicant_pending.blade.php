@extends('admin_access.layout.master')

@section('style')

@endsection

@section('script')

    <script type="text/javascript">
        $(document).ready(function() {
            $(document.body).on('click', 'a.approve_post', function(event) {
                event.preventDefault();
                
                var parent_row  = $(this).closest('tr');
                var get_url     = $(this).attr('href').split('=');
                
                swal({   
                    title: "Are you sure?",
                    type: "warning", 
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes, approve it!",
                    cancelButtonText: "No, cancel",
                    closeOnConfirm: false, //If false then confirm message will enable
                    closeOnCancel: true
                }, function(isConfirm){
                    if (isConfirm) {

                        $.ajax({
                            url: get_url,
                            type: 'GET',
                            dataType: 'html',
                            data: false,
                        })
                        .done(function(res) {
                            parent_row.remove();
                            swal("Approved", res, "success");
                        });

                    }
                });
            });
        });
    </script>

@endsection

@section('content')

    <h3>{{ trans('account.installment_pay_list') }}</h3>
    <div class="row">
        <div class="col-md-4 col-sm-4 col-xs-12" style="padding:10px 15px;">
            {!! Form::label('tender_id', trans('tender.tender_select'), array('style'=>'font-size:16px;')) !!}
            {!! Form::select('tender_id', $tender, null, array('class'=>'form-control list_select','placeholder' => trans('common.form_select'))) !!}
        </div>
    </div>
    <div class="portlet light bordered" id="result_content">
        {{-- <div class="portlet-title">
            <div class="caption">
                
            </div>
            <div class="tools"> </div>
        </div> --}}
        <div class="portlet-body contentArea">
            <table class="table table-striped table-bordered table-hover datatable_ajax" data-source="{{ route('installment_pending_data') }}" id="common_table">
                <thead>
                    <tr>
                        <th>{{ trans('common.table_sl') }}</th>
                        <th>{{ trans('tender.tender_applicant_name_address') }}</th>
                        <th>{{ trans('tender.tender_number') }}</th>
                        <th>{{ trans('user.mobile') }}</th>
                        <th>{{ trans('project.project_dropdown_category') }}</th>
                        <th>{{ trans('common.table_action') }}</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>

@endsection