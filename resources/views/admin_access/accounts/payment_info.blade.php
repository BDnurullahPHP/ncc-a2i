<!--=============Title for modal===============-->
    <div class="modal_top_title">{{ trans('account.payment_info') }}</div>
<!--===========End title for modal=============-->

<div class="clearfix"></div>
    
@if($type != 'lease_demesne')
    <div class="row padding-bottom-10">
        <div class="col-md-5 col-sm-5 col-xs-5">
            <strong>লেনদেনের ধরন:</strong> 
        </div>
        <div class="col-md-7 col-sm-7 col-xs-7">
            @if($applicant_property->BG_PAYMENT_TYPE == 2)
                অনলাইন
            @else
                ব্যাংক ড্রাফট
            @endif
        </div>
    </div>
    @if($applicant_property->BG_PAYMENT_TYPE == 2)
        <div class="row padding-bottom-10">
            <div class="col-md-5 col-sm-5 col-xs-12">
                <strong>ব্যাংক ট্রানজিশন সংখ্যা</strong>
            </div>
            <div class="col-md-7 col-sm-7 col-xs-12">
                {{ $payment_info->BANK_TRAN_ID }}
            </div>
        </div>
        <div class="row padding-bottom-10">
            <div class="col-md-5 col-sm-5 col-xs-12">
                <strong>সময়</strong>
            </div>
            <div class="col-md-7 col-sm-7 col-xs-12">
                {{ $payment_info->TRAN_DATE }}
            </div>
        </div>
    @else
        <div class="row padding-bottom-10">
            <div class="col-md-5 col-sm-5 col-xs-12">
                <strong>ব্যাংক</strong>
            </div>
            <div class="col-md-7 col-sm-7 col-xs-12">
                {{ $applicant_property->getBank->BANK_NAME }}
            </div>
        </div>
        <div class="row padding-bottom-10">
            <div class="col-md-5 col-sm-5 col-xs-12">
                <strong>তারিখ</strong>
            </div>
            <div class="col-md-7 col-sm-7 col-xs-12">
                {{ $ledger->VLEDGER_DT }}
            </div>
        </div>
        <div class="row padding-bottom-10">
            <div class="col-md-5 col-sm-5 col-xs-12">
                <strong>{{ trans('common.attachment') }}</strong>
            </div>
            <div class="col-md-7 col-sm-7 col-xs-12">
                <?php $taxfile = $applicant_property->B_DRAFT_ATTACHMENT; ?>
                @if(!empty($applicant_property->B_DRAFT_ATTACHMENT) && File::exists(public_path()."/upload/bank/$taxfile") )
                    <a target="_blank" href='{{ Helpers::asset("upload/bank/$taxfile") }}'>
                        <img src="{{ Helpers::asset("upload/bank/$taxfile") }}" width="150">
                    </a>
                @endif
            </div>
        </div>
    @endif
@else
    <div class="row padding-bottom-10">
        <div class="col-md-5 col-sm-5 col-xs-12">
            <strong>টাকা</strong>
        </div>
        <div class="col-md-7 col-sm-7 col-xs-12">
            {{ number_format($ledger->DR_AMT, 2) }}
        </div>
    </div>
    <div class="row padding-bottom-10">
        <div class="col-md-5 col-sm-5 col-xs-12">
            <strong>তারিখ</strong>
        </div>
        <div class="col-md-7 col-sm-7 col-xs-12">
            {{ $ledger->VLEDGER_DT }}
        </div>
    </div>
@endif