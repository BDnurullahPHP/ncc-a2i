{!! Form::open(array('url' => '#', 'id'=>'refund_letter', 'class'=>'form-horizontal', 'method' => 'post')) !!}

<!--=============Title for modal===============-->
<div class="modal_top_title">জামানতের টাকা ফেরত</div>
<!--===========End title for modal=============-->

<!--========Show form post message=============-->
<div class="form-group">
    <div class="col-sm-6 form_messsage" style="padding-top: 15px;">

    </div>
</div>
<!--========End form post message=============-->
<div class="tabbable-custom">
    <div class="tab-content">
        <div class="tab-pane active">
            <div class="col-md-12">
                <table class="table table-striped table-bordered table-hover">
                    <tr>
                        <th>নাম</th>
                        <td style="text-align: left">{{ $user_type == 'applicant' ? $applicant->APPLICANT_NAME : $applicant->getTenderCitizen->FULL_NAME }}</td>
                    </tr>
                    <tr>
                        <th>পিতা/স্বামী</th>
                        <td style="text-align: left">{{ $applicant->SPOUSE_NAME }}</td>
                    </tr>
                    <tr>
                        <th>ঠিকানা</th>
                        <td style="text-align: left">
                            বাসা নং- {{ $applicant->HOLDING_NO }}, রাস্তা- {{ $applicant->ROAD_NO }}
                            <br/>থানা- {{ $applicent_thana }}, জেলা- {{ $applicent_district }}
                        </td>
                    </tr>
                </table>
            </div>
            <div class="col-md-12">
                <div class="col-md-4">
                    <label>জামানত&#40;টাকা&#41;</label>
                </div>
                <div class="col-sm-6">
                    <input type="text" class="form-control form-required" name="security_money" value="15500" readonly="readonly">
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-12" style="padding-top: 15px;">
                    <button style="margin-left: 5px;" type="submit" data-action="{{ route('security_money_update', [$user_id, $user_type, $schedule_id]) }}" data-print="{{ route('security_money_letter_print', [$user_id, $user_type, $schedule_id]) }}" class="btn btn-success pull-right btn-sm refund_save">{{ trans('common.form_submit') }}</button>
                </div>
            </div>
        </div>
    </div>
</div>
{!! Form::close() !!}