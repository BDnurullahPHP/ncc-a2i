<table class="table table-striped table-bordered table-hover">
    <thead>
        <tr>
            <th>{{ trans('common.table_sl') }}</th>
            <th>{{ trans('tender.installment_number') }}</th>
            <th>{{ trans('tender.installment_amount') }}</th>
            <th>{{ trans('common.approve') }}</th>
        </tr>
    </thead>
    <tbody>
        @if(count($installment) > 0)
            @foreach($installment as $key => $ins)
                <tr>
                    <td>{{ $key+1 }}</td>
                    <td>{{ $ins->INSTALLMENT }}</td>
                    <td>{{ $ins->AMOUNT }}</td>
                    <td>
                        <a href="{{ route('approve_installment', $ins->TENDER_INS_ID) }}" class="label label-success approve_post"><i class="fa fa-check-square-o"></i></a>
                    </td>
                </tr>
            @endforeach
        @endif
    </tbody>
</table>