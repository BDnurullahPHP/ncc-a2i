@extends('admin_access.layout.master')

@section('script')

<script src="{{ Helpers::asset('assets/admin/js/tender.js') }}" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function() {
    tenderSecurityReturn.init();
});
</script>

@endsection

@section('style')

<link rel="stylesheet" href="{{ Helpers::asset('assets/admin/css/tender.css') }}">

<style type="text/css">
    table>thead>tr>th{
        text-align: center;
        white-space: inherit;
    }
    table>tbody>tr>td{
        text-align: center;
    }
</style>

@endsection

@section('content')

<h3>{{ trans('tender.unselected_applicant_list') }}
    <small></small>
</h3>
<div class="row">
    <div class="col-md-6">
        <div id="notification_show"></div>
        <!--========Session message=======-->
        @if (Session::has('success'))
        <div class="alert alert-success">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            {{ Session::get('success') }}
        </div>
        @endif
        @if (Session::has('error'))
        <div class="alert alert-danger">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            {{ Session::get('error') }}
        </div>
        @endif
        @if (count($errors) > 0)
        @foreach ($errors->all() as $error)
        <div class="alert alert-danger">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            {{ $error }}
        </div>
        @endforeach
        @endif
        <!--======end Session message=====-->
    </div>
</div>
<div class="row">
    <div class="col-md-4 col-sm-4 col-xs-12" style="padding:10px 15px;">
        {!! Form::label('tender_id', trans('tender.tender_select'), array('style'=>'font-size:16px;')) !!}
        {!! Form::select('tender_id', $tender, null, array('class'=>'form-control list_select','placeholder' => trans('common.form_select'))) !!}
    </div>
</div>

<div class="portlet light bordered" id="result_content">
    <!-- <div class="portlet-title">
        <div class="caption">
            
        </div>
        <div class="tools"> </div>
    </div> -->
    <div class="row">
        <div class="col-md-5 col-sm-6 col-xs-12">
            <div id="lg_form_message"></div>
        </div>
    </div>
    <div class="portlet-body contentArea">

        <table class="table table-striped table-bordered table-hover datatable_ajax" data-source="{{ route('security_money_data') }}">
            <thead>
                <tr>
                    <th>{{ trans('common.table_sl') }}</th>
                    <th>{{ trans('account.applicant_name') }}</th>
                    <th>{{ trans('tender.tender_number') }}</th>
                    <th>{{ trans('account.project_name') }}</th>
                    <th>{{ trans('tender.returnable_taka') }}</th>
                    <th>{{ trans('common.table_action') }}</th>
                </tr>
            </thead>
            <tbody>

            </tbody>
        </table>

    </div>
</div>

@endsection