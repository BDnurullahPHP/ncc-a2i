{!! Form::model($project_details, array('url' => '#', 'id'=>'mouza_form', 'class'=>'form-horizontal', 'method' => 'post')) !!}

<!--=============Title for modal===============-->
<div class="modal_top_title">Update</div>
<!--===========End title for modal=============-->

<!--========Show form post message=============-->
<div class="form-group">
    <div class="col-sm-6 form_messsage">

    </div>
</div>
<!--========End form post message=============-->
<div class="form-group">
    <label class="col-sm-3 control-label">প্রকল্পের নাম<span style="color: red">*</span></label>
    <div class="col-sm-5">
        <select class="form-control form-required" id="project_id" name="project">
            <option value="">{{ trans('common.form_select') }}</option>
            @foreach ($project as $value)
            <option <?php if ($value->PROJECT_ID == $project_details->PROJECT_ID) echo 'selected="selected"'; ?> value="{{ $value->PROJECT_ID }}">{{ $value->PR_NAME_BN }}</option>
            @endforeach
        </select>
    </div>
</div>
<div class="form-group">
    <label class="col-sm-3 control-label">প্রকল্পের শ্রেণী<span style="color: red">*</span></label>
    <div class="col-sm-5">
        <select class="form-control form-required" id="category_id" name="category">
            <option value="">{{ trans('common.form_select') }}</option>
            @foreach ($category as $value)
            <option <?php if ($value->PR_CATEGORY == $project_details->PR_CATEGORY) echo 'selected="selected"'; ?> value="{{ $value->PR_CATEGORY }}">{{ $value->CATE_NAME }}</option>
            @endforeach
        </select>
    </div>
</div>

<table class="table table-striped table-bordered table-hover pDetailsTable" id="myTable">
    <thead>
        <tr>
            <th>{{trans('project.project_details_position')}} <span style="color: red">*</span></th>
            <th>{{trans('project.project_details_type')}}</th>
            <th>{{trans('common.number')}} <span style="color: red">*</span></th>
            <th>{{trans('project.project_details_size')}} <span style="color: red">*</span></th>
            <th>{{trans('project.is_reserved')}}</th>
        </tr>
    </thead>
    <tbody class="pDetailsTbody">
        <tr class="pDetailsTr">
            <td>
                <select class="form-control form-required" id="POSITION_ID" name="POSITION">
                    <option value="">{{ trans('common.form_select') }}</option>
                    @foreach ($project_floor as $value)
                    <option <?php if ($value->FLOOR_ID == $project_details->POSITION) echo 'selected="selected"'; ?> value="{{ $value->FLOOR_ID }}">{{ $value->FLOOR_NUMBER_BN }}</option>
                    @endforeach
                </select>
            </td>
            <td>
                <select class="form-control form-required" id="PR_TYPE_ID" name="PR_TYPE">
                    <option value="">{{ trans('common.form_select') }}</option>
                    @foreach ($project_type as $value)
                    <option <?php if ($value->PR_TYPE == $project_details->PR_TYPE) echo 'selected="selected"'; ?> value="{{ $value->PR_TYPE }}">{{ $value->TYPE_NAME }}</option>
                    @endforeach
                </select>
            </td>
            <td>
                {!! Form::text('PR_SSF_NO', Input::old('PR_SSF_NO'), array('id'=>'PR_SSF_NO_1', 'class'=>'form-control required pr_ssf_no sfsNumber', 'placeholder'=>trans('project.project_pr_ssf_no_pl'))) !!}
                <span id="project_number" class="hidden" style="color: red;">এই নাম্বারটি ইতিমধ্যে বিদ্যমান</span>
            </td>
            <td>
                {!! Form::text('PR_MEASURMENT', Input::old('PR_MEASURMENT'), array('id'=>'PR_MEASURMENT_1', 'class'=>'form-control required pr_measurement numbersOnlys', 'placeholder'=>trans('project.project_details_size_pl'))) !!}
                <span id="number" style="color: red;" action="{{ route('get_market_number') }}"></span>
            </td>
            <td>
                {!! Form::checkbox('IS_RESERVED', '1', false, ['class'=>'status pr_is_reserved']) !!}
            </td>
        </tr>
    </tbody>
</table>

<div class="form-group">
    <div class="col-md-12" style="padding-top: 15px;">
        <span class="modal_msg pull-left"></span>
        <button type="submit" data-action="{{ route('market_details_edit', $project_details->PR_DETAIL_ID) }}" class="btn btn-success btn-sm pull-right form_submit"  style="margin-left: 5px;">{{ trans('common.form_submit') }}</button>
        <button type="button" class="btn btn-default btn-sm reset_form pull-right">{{ trans('common.form_reset') }}</button>
        <span class="loadingImg"></span>
    </div>
</div>

{!! Form::close() !!}