<div class="project_details">

	<!--=============Title for modal===============-->
    <div class="modal_top_title">{{ trans('project.project_details_info') }}</div>
    <!--===========End title for modal=============-->

	<div class="row padding-bottom-10">
		<div class="col-md-6 col-sm-6 col-xs-12">
			<div class="row">
				{!! Form::label('', trans('project.project_land'), array('class'=>'col-sm-6')) !!}
				<div class="col-md-6 col-sm-6">
			        <p>
                        @foreach($projectLand as $key => $land)
                            {{ $land->getLand->LAND_NUMBER }}
                        @endforeach
                    </p>
			    </div>
			</div>
		</div>
		<div class="col-md-6 col-sm-6 col-xs-12">
			<div class="row">
				{!! Form::label('', trans('project.project_number'), array('class'=>'col-sm-6')) !!}
				<div class="col-md-6 col-sm-6">
			        <p>{{ $project->PR_UD_ID }}</p>
			    </div>
			</div>
		</div>
	</div>
	<div class="row padding-bottom-10">
		<div class="col-md-6 col-sm-6 col-xs-12">
			<div class="row">
				{!! Form::label('', trans('project.project_create_date'), array('class'=>'col-sm-6')) !!}
				<div class="col-md-6 col-sm-6">
			        <p>{{ date('d-m-Y', strtotime($project->PR_START_DT)) }}</p>
			    </div>
			</div>
		</div>
		<div class="col-md-6 col-sm-6 col-xs-12">
			<div class="row">
				{!! Form::label('', trans('project.project_end_date'), array('class'=>'col-sm-6')) !!}
				<div class="col-md-6 col-sm-6">
			        <p>{{ date('d-m-Y', strtotime($project->PR_END_DT)) }}</p>
			    </div>
			</div>
		</div>
	</div>
	<div class="row padding-bottom-10">
		<div class="col-md-6 col-sm-6 col-xs-12">
			<div class="row">
				{!! Form::label('', trans('project.project_name'), array('class'=>'col-sm-6')) !!}
				<div class="col-md-6 col-sm-6">
			        <p>{{ $project->PR_NAME }}</p>
			    </div>
			</div>
		</div>
		<div class="col-md-6 col-sm-6 col-xs-12">
			<div class="row">
				{!! Form::label('', trans('project.project_bn_name'), array('class'=>'col-sm-6')) !!}
				<div class="col-md-6 col-sm-6">
			        <p>{{ $project->PR_NAME_BN }}</p>
			    </div>
			</div>
		</div>
	</div>
	<div class="row padding-bottom-10">
		<div class="col-md-6 col-sm-6 col-xs-12">
			<div class="row">
				{!! Form::label('', trans('project.project_location'), array('class'=>'col-sm-6')) !!}
				<div class="col-md-6 col-sm-6">
			        <p>{{ $project->PROJECT_LOCATION }}</p>
			    </div>
			</div>
		</div>
	</div>
	@if(count($project->getDocuments) > 0)
		<div class="row padding-bottom-10">
			<div class="col-md-12">
				{!! Form::label('', trans('common.attachment')) !!}
			</div>
			@foreach($project->getDocuments as $key => $doc)
                @if( !empty($doc->ATTACHMENT) && File::exists(public_path()."/upload/project/$project->ATTACHMENT") )
                    <div class="col-md-3">
                    	<a target="_blank" href='{{ Helpers::asset("upload/project/$doc->ATTACHMENT") }}'>{{ $doc->ATTACHMENT }}</a>
                    </div>
                @endif
            @endforeach
		</div>
	@endif
	<div class="row padding-bottom-10">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="row">
				<div class="col-md-6">
					{!! Form::label('', trans('project.project_details')) !!}
				</div>
				<div class="col-md-12 col-sm-12">
			        <p>{!! $project->PR_DESC !!}</p>
			    </div>
			</div>
		</div>
	</div>
        <div class="row tabbable-custom nav-justified" id="pro_dtls_sc_overview">
            @if(count($category) > 0)
                <ul class="nav nav-tabs nav-justified" role="tablist">
                    @foreach($category as $key => $cat)
                        <li role="presentation" class="{{ $key==0?'active':'' }}">
                            <a href="#tab_link_{{ $key }}" aria-controls="tab_link_{{ $key }}" role="tab" data-toggle="tab"><h4>{{ $cat->getCategory->CATE_NAME }} {{ trans('common._information') }}</h4></a>
                        </li>
                    @endforeach
                </ul>
                <div class="tab-content">
                    @foreach($category as $key => $cat)
                        <div role="tabpanel" class="tab-pane {{ $key==0?'active':'' }}" id="tab_link_{{ $key }}">
                            <table class="table table-striped table-bordered table-hover">
								<thead>
                                    <tr>
                                        <th>{{ trans('project.project_details_type') }}</th>
                                        <th>{{ trans('project.project_details_position') }}</th>
                                        <th>{{ trans('common.number') }}</th>
                                        <th>{{ trans('project.project_details_size') }}</th>
                                        <th>{{ trans('project.is_reserved') }}</th>
                                        <th>{{ trans('project.is_build') }}</th>
                                    </tr>
								</thead>
								<tbody>
                                    <?php 
                                        $project_details = DB::table('project_details')
                                                            ->where('PROJECT_ID', $cat->PROJECT_ID)
                                                            ->where('PR_CATEGORY', $cat->PR_CATEGORY)
                                                            ->get();
                                    ?>
                                    @if(count($project_details) > 0)
                                        @foreach($project_details as $key => $pr_dtls)
                                            <tr>
                                                <?php 
                                                    $project_type = DB::table('sa_type')
                                                                        ->where('PR_TYPE', $pr_dtls->PR_TYPE)
                                                                        ->first();
                                                    $project_floor = DB::table('sa_floor')
                                                    					->where('FLOOR_ID', $pr_dtls->POSITION)
                                                    					->first();
                                                ?>
                                                <td>{{ $project_type->TYPE_NAME }}</td>
                                                @if(!empty($project_floor))
                                                	<td>{{ $project_floor->FLOOR_NUMBER_BN }}</td>
                                                @else
													<td>{{ $pr_dtls->POSITION }} floor</td>
                                                @endif
                                                <td>{{ $pr_dtls->PR_SSF_NO }}</td>
                                                <td>
                                                    {{ $pr_dtls->PR_MEASURMENT }}
                                                    @if($pr_dtls->UOM == 1)
                                                        বর্গ ফুট
                                                    @else
                                                    @endif
                                                </td>
                                                <td>
                                                    @if($pr_dtls->IS_RESERVED == 1)
                                                        Yes
                                                    @else
                                                        No
                                                    @endif
                                                </td>
                                                <td>
                                                	{{ date_format(date_create($pr_dtls->CREATED_AT), 'd-m-Y') }}
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif
								</tbody>
                            </table>
                        </div>
                    @endforeach
                </div>
            @endif
	</div>
</div>