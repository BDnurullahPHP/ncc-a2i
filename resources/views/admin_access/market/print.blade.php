<!DOCTYPE html>
<html lang="en">
    <head>
        <title>{{ $project->PR_NAME_BN }}</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
    </head>
    <style>
        body {
            font-family: freeserif,sans-serif,vrinda,solaiman-lipi,monospace;
        }
        h1, h2, h3, h4, p, input, th, td{
            font-family: freeserif,sans-serif,vrinda,solaiman-lipi,monospace;
            font-weight: 400;
            white-space: initial;
        }
        h3{
            font-size: 22px;
            margin-bottom: 10px;
            margin-top: 15px;
        }
        h4{
            font-size: 18px;
            margin-bottom: 10px;
            margin-top: 15px;
            text-align:center; 
        }
        table, th, td {
            border: .1px solid;
            border-collapse: collapse;
            text-align: center;
        }
        #info_table th{
            text-align: left;
            width: 20%;
        }
        #info_table td{
            text-align: left;
            width: 30%;            
        }

    </style>
    <body>
        <h4>
            <bold>নারায়ণগঞ্জ সিটি করপোরেশন</bold>
            <br/>নগর ভবন, ১০ বঙ্গবন্ধু রোড
            <br/>নারায়ণগঞ্জ
            <br/>www.ncc.gov.bd
        </h4>
        <h3>{{ trans('project.project_details_info') }}:-</h3>
        <table id="info_table" style="width: 100%">
            <tr>
                <th>{{  trans('project.project_number') }} :</th>
                <td>{{ Helpers::numberConvert($project->PR_UD_ID) }}</td>
                <th>{{ trans('project.project_location') }}:</th>
                <td>{{ $project->PROJECT_LOCATION }}</td>
            </tr>
            <tr>
                <th>{{  trans('project.project_bn_name') }} :</th>
                <td>{{ $project->PR_NAME_BN }}</td>
                <th>{{  trans('project.project_name') }} :</th>
                <td>{{ $project->PR_NAME }}</td>
            </tr>
            <tr>
                <th>{{  trans('project.project_create_date') }}: </th>
                <td>{{ Helpers::numberConvert(date('d-m-Y', strtotime($project->PR_START_DT))) }}</td>
                <th>{{ trans('project.project_end_date') }}: </th>
                <td>{{ Helpers::numberConvert(date('d-m-Y', strtotime($project->PR_END_DT))) }}</td>
            </tr>
        </table>
        <p>{{ trans('project.project_details') }}: {!! $project->PR_DESC !!}</p>
        @foreach($category as $key => $cat)
        <h4 style="text-align: left;">{{ $cat->getCategory->CATE_NAME }} {{ trans('common._information') }}:-</h4>
        <table style="width: 100%">
            <thead>
                <tr>
                    <th style="width: 10px;">#</th>
                    <th>{{ trans('project.project_details_type') }}</th>
                    <th>{{ trans('project.project_details_position') }}</th>
                    <th>{{ trans('common.number') }}</th>
                    <th>{{ trans('project.project_details_size') }}</th>
                    <th>{{ trans('project.is_reserved') }}</th>
                    <th>{{ trans('project.is_build') }}</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $project_details = DB::table('project_details')
                        ->where('PROJECT_ID', $cat->PROJECT_ID)
                        ->where('PR_CATEGORY', $cat->PR_CATEGORY)
                        ->get();
                ?>
                @if(count($project_details) > 0)
                @foreach($project_details as $key => $pr_dtls)
                <tr>
                    <?php
                    $project_type = DB::table('sa_type')
                            ->where('PR_TYPE', $pr_dtls->PR_TYPE)
                            ->first();
                    $project_floor = DB::table('sa_floor')
                            ->where('FLOOR_ID', $pr_dtls->POSITION)
                            ->first();
                    ?>
                    <td>{{ Helpers::numberConvert($key +1) }}</td>
                    <td>{{ $project_type->TYPE_NAME }}</td>
                    @if(!empty($project_floor))
                    <td>{{ $project_floor->FLOOR_NUMBER_BN }}</td>
                    @else
                    <td>{{ Helpers::numberConvert($pr_dtls->POSITION) }} floor</td>
                    @endif
                    <td>{{ Helpers::numberConvert($pr_dtls->PR_SSF_NO) }}</td>
                    <td>
                        {{ Helpers::numberConvert($pr_dtls->PR_MEASURMENT) }}
                        @if($pr_dtls->UOM == 1)
                        বর্গ ফুট
                        @else
                        @endif
                    </td>
                    <td>
                        @if($pr_dtls->IS_RESERVED == 1)
                        Yes
                        @else
                        No
                        @endif
                    </td>
                    <td>
                        {{ Helpers::numberConvert(date_format(date_create($pr_dtls->CREATED_AT), 'd-m-Y')) }}
                    </td>
                </tr>
                @endforeach
                @endif
            </tbody>
        </table>
        @endforeach
        <div class="block">
            @foreach($project->getDocuments as $key => $doc)
            @if( !empty($doc->ATTACHMENT) && File::exists(public_path()."/upload/project/$project->ATTACHMENT") )
            <img src="{{ Helpers::asset("upload/project/$doc->ATTACHMENT") }}" style="max-width: 100%;" alt="">
            @endif
            @endforeach
        </div>
    </body>
</html>
