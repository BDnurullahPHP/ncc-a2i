{!! Form::open(array('url' => '#', 'id'=>'project_form', 'class'=>'form-horizontal', 'method' => 'post')) !!}

<!--=============Title for modal===============-->
<div class="modal_top_title">{{ trans('project.add_data') }}</div>
<!--===========End title for modal=============-->

<!--========Show form post message=============-->
<div class="form-group">
    <div class="col-sm-6 form_messsage">

    </div>
</div>
<!--========End form post message=============-->
<div class="form-group">
    <label class="col-sm-3 control-label">{{trans('project.project_name_select')}}<span style="color: red">*</span></label>
    <div class="col-sm-5">
        {!! Form::select('project', $project, array(Input::old('project')), array('class'=>'form-control form-required', 'id'=>'project_id', 'placeholder' => trans('common.form_select'))) !!}
    </div>
</div>
<div class="form-group">
    <label class="col-sm-3 control-label">{{trans('project.project_dropdown_category')}}<span style="color: red">*</span></label>
    <div class="col-sm-5">
        {!! Form::select('category', $category, array(Input::old('category')), array('class'=>'form-control form-required', 'id'=>'category_id', 'placeholder' => trans('common.form_select'))) !!}
    </div>
</div>

<table class="table table-striped table-bordered table-hover pDetailsTable" id="myTable">
    <thead>
        <tr>
            <th>{{trans('project.project_details_position')}} <span style="color: red">*</span></th>
            <th>{{trans('project.project_details_type')}}</th>
            <th>{{trans('common.number')}} <span style="color: red">*</span></th>
            <th>{{trans('project.project_details_size')}} <span style="color: red">*</span></th>
            <th>{{trans('project.is_reserved')}}</th>
            <th>
                <span type="button" class="btn green btn-sm" id="add_more"><i class="fa fa-plus"></i></span>
            </th>
        </tr>
    </thead>
    <tbody class="pDetailsTbody">
        <tr class="pDetailsTr">
            <td>
                {!! Form::select('POSITION[]', $project_floor, null, array('id'=>'POSITION_1', 'class'=>'form-control required pr_position', 'placeholder' => trans('common.form_select'))) !!}
            </td>
            <td>
                {!! Form::select('PR_TYPE[]', $project_type, null, array('id'=>'PR_TYPE_1', 'class'=>'form-control pr_type', 'placeholder' => trans('common.form_select'))) !!}
            </td>
            <td>
                {!! Form::text('PR_SSF_NO[]', null, array('id'=>'PR_SSF_NO_1', 'class'=>'form-control required pr_ssf_no sfsNumber', 'placeholder'=>trans('project.project_pr_ssf_no_pl'))) !!}
                <span id="project_number" class="hidden" style="color: red;">এই নাম্বারটি ইতিমধ্যে বিদ্যমান</span>
            </td>
            <td>
                {!! Form::text('PR_MEASURMENT[]', null, array('id'=>'PR_MEASURMENT_1', 'class'=>'form-control required pr_measurement numbersOnlys', 'placeholder'=>trans('project.project_details_size_pl'))) !!}
                <span id="number" style="color: red;" action="{{ route('get_market_number') }}"></span>
            </td>
            <td>
                {!! Form::checkbox('IS_RESERVED[]', '1', false, ['class'=>'status pr_is_reserved']) !!}
            </td>
        </tr>
    </tbody>
</table>

<div class="form-group">
    <div class="col-md-12" style="padding-top: 15px;">
        <span class="modal_msg pull-left"></span>
        <button type="submit" data-action="{{ route('create_details_market') }}" class="btn btn-success btn-sm pull-right form_submit"  style="margin-left: 5px;">{{ trans('common.form_submit') }}</button>
        <button type="button" class="btn btn-default btn-sm reset_form pull-right">{{ trans('common.form_reset') }}</button>
        <span class="loadingImg"></span>
    </div>
</div>

{!! Form::close() !!}