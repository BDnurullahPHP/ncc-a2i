{!! Form::model($project, array('url' => '#', 'id'=>'project_form', 'class'=>'form-horizontal', 'method' => 'post')) !!}

<!--=============Title for modal===============-->
<div class="modal_top_title">{{ trans('project.edit_data') }}</div>
<!--===========End title for modal=============-->

<!--========Show form post message=============-->
<div class="form-group">
    <div class="col-sm-6 form_messsage">

    </div>
</div>
<!--========End form post message=============-->


<div class="portlet box yellow">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-tasks"></i> {{ trans('project.project') }} {{ trans('common.create') }}</div>
        <div class="tools">
            <a href="#" class="collapse" title=""> </a>
        </div>
    </div>
    <div class="portlet-body">
        <div class="form-group">
            <div class="col-md-4 col-sm-4 col-xs-6">
                {!! Form::label('area', trans('land.land_zone')) !!}
                {!! Form::select('area', $area, array(Input::old('area')), array('class'=>'form-control', 'placeholder' => trans('common.form_select'))) !!}
            </div>
            <div class="col-md-4 col-sm-4 col-xs-6">
                {!! Form::label('ward', trans('land.land_ward')) !!}
                {!! Form::select('ward', [], array(Input::old('ward')), array('class'=>'form-control', 'placeholder' => trans('common.form_select'))) !!}
            </div>
            <div class="col-md-4 col-sm-4 col-xs-6">
                {!! Form::label('mouza', trans('land.land_form_mouza')) !!}
                {!! Form::select('mouza', [], array(Input::old('mouza')), array('class'=>'form-control', 'placeholder' => trans('common.form_select'))) !!}
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-3">
                {!! Form::label('LAND_ID', trans('project.project_land')) !!}
                <select class="form-control multi_select " name="LAND_ID" id="project_lands" multiple="multiple">
                    @foreach ($land as $key => $value)
                    <option value="{{ $value->LAND_ID }}" {{ in_array($value->LAND_ID, (array)$project_land) ? 'selected="selected"' : '' }} >{{ $value->LAND_NUMBER }}</option>
                    @endforeach
                </select>
                <div id="error_msg1"></div>
            </div>
            <div class="col-sm-3">
                {!! Form::label('PR_UD_ID', trans('project.project_number')) !!} <span class="required_field">*</span>
                {!! Form::text('PR_UD_ID', null, array('id'=>'project_number', 'class'=>'form-control form-required', 'placeholder'=>'Ex. padma-30/07/2016')) !!}
            </div>
            <div class="col-sm-3">
                {!! Form::label('PR_START_DT', trans('project.project_create_date')) !!} <span class="required_field">*</span>
                {!! Form::text('PR_START_DT', date('d-m-Y', strtotime($project->PR_START_DT)), array('id'=>'project_start_date', 'class'=>'form-control date_picker_default form-required', 'placeholder'=>trans('common.date_pl'))) !!}
            </div>
            <div class="col-sm-3">
                {!! Form::label('PR_END_DT', trans('project.project_end_date')) !!} <span class="required_field">*</span>
                {!! Form::text('PR_END_DT', date('d-m-Y', strtotime($project->PR_END_DT)), array('id'=>'project_end_date', 'class'=>'form-control date_picker_default form-required', 'placeholder'=>trans('common.date_pl'))) !!}
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-6">
                {!! Form::label('PR_NAME', trans('project.project_name')) !!} <span class="required_field">*</span>
                {!! Form::text('PR_NAME', null, array('id'=>'project_name', 'class'=>'form-control form-required', 'placeholder'=>trans('project.project_name_pl'))) !!}
            </div>
            <div class="col-sm-6">
                {!! Form::label('PR_NAME_BN', trans('project.project_bn_name')) !!} <span class="required_field">*</span>
                {!! Form::text('PR_NAME_BN', null, array('id'=>'project_name_bn', 'class'=>'form-control form-required', 'placeholder'=>trans('project.project_bn_name_pl'))) !!}
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-6">
                {!! Form::label('PR_DESC', trans('project.project_details')) !!} <span class="required_field">*</span>
                {!! Form::textarea('PR_DESC', null, array('id'=>'project_details', 'rows' =>'3', 'class'=>'form-control reactor_basic form-required', 'placeholder'=>trans('project.project_details'))) !!}
            </div>
            <div class="col-sm-6">
                {!! Form::label('PR_LOCATION', trans('project.project_location')) !!} <span class="required_field">*</span>
                {!! Form::textarea('PR_LOCATION', $project->PROJECT_LOCATION, array('id'=>'project_location', 'rows' =>'5', 'class'=>'form-control form-required', 'placeholder'=>trans('project.project_location'))) !!}
            </div>
        </div>
        <div class="form-group" id="project_files">
            <div class="col-md-12">
                {!! Form::label('', trans('common.attachment')) !!}
            </div>
            <div class="col-sm-4 attachment_single" style="display: none !important;">
                {!! Form::file('attachment', array('class'=>'form-control form-attachment-update project_file')) !!}
            </div>
            @if(count($project->getDocuments) > 0)
            @foreach($project->getDocuments as $key => $doc)
            @if( !empty($doc->ATTACHMENT) && File::exists(public_path()."/upload/project/$project->ATTACHMENT") )
            <div class="col-md-4">
                <p class="sketch_preview">
                    <a target="_blank" href='{{ Helpers::asset("upload/project/$doc->ATTACHMENT") }}'>{{ $doc->ATTACHMENT }}</a>
                    <a href="{{ route('project_file_delete', $doc->PR_DOC_ID) }}" title="Remove file" class="remove_project_file">
                        <i class="fa fa-times"></i>
                    </a>
                </p>
            </div>
            @else
            <div class="col-sm-4 attachment_single">
                {!! Form::file('attachment', array('class'=>'form-control form-attachment-update project_file')) !!}
            </div>
            @endif
            @endforeach
            @else

            @endif
            <div class="col-md-2">
                <button type="button" class="btn green btn-sm project_add_more btn_margin"> <i class="fa fa-plus"></i> {{ trans('common.add_more') }}</button>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-6">
                {!! Form::label('is_active', trans('common.is_active')) !!}<br/>
                @if($project->IS_ACTIVE == 1)
                {!! Form::checkbox('is_active', 1, true, array('id'=>'project_is_active')) !!}
                @else
                {!! Form::checkbox('is_active', 1, false, array('id'=>'project_is_active')) !!}
                @endif
            </div>
        </div>
    </div>
</div>

<div class="form-group">
    <div class="col-md-12" style="padding-top: 15px;">
        <span class="modal_msg pull-left"></span>
        <button type="submit" data-action="{{ route('update_market', $project->PROJECT_ID) }}" class="btn btn-success btn-sm pull-right" id="project_submit" style="margin-left: 5px;">{{ trans('common.form_submit') }}</button>
        <button type="button" class="btn btn-default btn-sm reset_form pull-right">{{ trans('common.form_reset') }}</button>
        <span class="loadingImg"></span>
    </div>
</div>

{!! Form::close() !!}