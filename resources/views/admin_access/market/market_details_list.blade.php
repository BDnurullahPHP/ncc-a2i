@extends('admin_access.layout.master')

@section('style')
<style type="text/css">
    .radio-inline .radio{
        padding-top: 2px;
    }
    .ui-datepicker{
        z-index: 99999 !important;
    }
    .help-block{
        color: red;
    }
    .form-control{
        padding: 5px 5px;
    }
    .item_single_list label{
        color: #333;
    }
    .tooltip {
        z-index: 2000000 !important;
    }
    input[type=file]{
        border: 1px solid #C2CAD8;
    }
    .modal-footer{
        display: none;
    }
    .portlet>.portlet-title{
        min-height: 20px;
    }
    .portlet.box>.portlet-title>.caption, 
    .portlet>.portlet-title>.tools{
        padding-top: 5px;
        padding-bottom: 5px;
    }
    .portlet>.portlet-title>.caption{
        font-size: 16px;
    }
    ul.item_single_list{
        position: relative;
        padding-left: 0px;
        width: 100%;
        float: left;
    }
    ul.item_single_list li{
        width: 15%;
        padding-right: 5px;
        list-style: none;
        display: inline-block;
        float: left;
    }
    ul.item_single_list li.list_sm_block{
        width: 7%;
    }
    ul.item_single_list li.remove_item_data, 
    ul.item_single_list li.remove_item_record{
        position: absolute;
        top: -10px;
        right: 15px;
        width: 22px;
        height: 22px;
        line-height: 22px;
        padding-right: 0px;
        -webkit-border-radius: 50% !important;
        -moz-border-radius: 50% !important;
        border-radius: 50% !important;
        border: 1px solid #B55858;
        text-align: center;
        color: #B55858;
        cursor: pointer;
    }
    .pr_add_more{
        position: absolute;
        top: 3px;
        left: -15px;
        right: auto;
    }
    ul.item_single_list .form-control{

    }
    .project_details label{
        font-weight: 700;
    }
    .project_details p{
        margin: 0px;
    }
    .remove_project_file{
        display: inline-block;
        width: 20px;
        height: 20px;
        color: #B55858;
        text-decoration: none;
        border: 1px solid #B55858;
        border-radius: 50% !important;
        font-size: 14px;
        text-align: center;
    }
    span.remove_file{
        width: 20px;
        height: 20px;
        position: absolute;
        top: -10px;
        left: auto;
        right: 5px;
        line-height: 20px;
        border: 1px solid #B55858;
        border-radius: 50% !important;
        font-size: 14px;
        text-align: center;
        color: #B55858;
        cursor: pointer;
        z-index: 1000;
    }
    .project_add_more{
        margin-top: 10px;
    }
    .attachment_single{
        margin-top: 10px;
        margin-bottom: 10px;
    }

    @media (min-width: 992px){
        .modal-lg {
            width: 1010px;
        }
    }
    @media only screen and (min-width: 992px) and (max-width: 1009px){
        .modal-lg {
            width: 950px !important;
        }
    }
</style>
@endsection

@section('content')

<h3>{{ trans('project.project_information') }}
    <small></small>
    @if($access->CREATE == 1)
    <a href="#" data-toggle="modal" data-target="#modal_add_content" class="btn btn-primary btn-sm pull-right" data-form="{{ route('market_details_create_form') }}">{{ trans('common.add_new') }} <i class="fa fa-plus"></i></a>
    @endif
</h3>
<div class="portlet light bordered">
    <div class="row">
        <div class="col-md-5 col-sm-6 col-xs-12">
            <div id="lg_form_message" class="form_messsage"></div>
        </div>
    </div>
    <div class="portlet-body contentArea">

        <table class="table table-striped table-bordered table-hover" data-source="{{ route('market_details_data') }}" id="common_table">
            <thead>
                <tr>
                    <th>{{ trans('common.table_sl') }}</th>
                    <th>{{ trans('project.project_bn_name') }}</th>
                    <th>{{ trans('project.project_location') }}</th>
                    <th>{{ trans('project.project_dropdown_category') }}</th>
                    <th>{{ trans('project.project_place_number') }}</th>
                    <th>{{ trans('project.project_measurement') }}</th>
                    <th>{{ trans('common.table_action') }}</th>
                </tr>
            </thead>
            <tbody>

            </tbody>
        </table>

    </div>
</div>

@endsection

@section('script')

<script src="{{ Helpers::asset('assets/admin/js/project.js') }}" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function() {
    projectManaged.init();


    //*******************************/
    $(document.body).on('click', '#add_more', function(event) {
        var append = 'Yes';
        $("#project_form .required").each(function() {
            if ($(this).val() == "") {
                $(this).css("border", "1px solid red");
                append = 'No';
            } else {
                $(this).css("border", "");
            }
        });
        if (append == 'Yes') {
            var row_data = $(this).closest('.pDetailsTable').find('.pDetailsTbody .pDetailsTr').last().html();
            $(this).closest('.pDetailsTable').find('.pDetailsTbody').prepend('<tr class="pDetailsTr">' + row_data + '<td class="remove_item_data"><i class="fa fa-times"></i></td></tr>');
        } else {

        }
    });
    $(document.body).on('click', 'td.remove_item_data', function(event) {
        $(this).closest('tr').remove();
    });
    $(document.body).on('keypress', '.numbersOnlys', function(e) {// Only number entry validation
        //if the letter is not digit then display error and don't type anything
        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
            //display error message
            $("#number").html("শুধুমাত্র সংখ্যা").show().fadeOut("slow");
            return false;
        }
    })
    // check this number is exist 
    var place = 0;
    $(document.body).on('blur', '.sfsNumber', function(e) {
        var project_id = $('#project_id').val();
        var project_type = $('#category_id').val();
        var number = $(this).val();
        place = $(this).parent().prev().prev().find('select').val();
        var url = $('#number').attr('action');
        $.ajax({
            url: url,
            type: 'GET',
            dataType: 'json',
            data: {project_id: project_id, project_type: project_type, place: place, number: number},
            success: function(data) {
                if (data == 1) {
                    $("#project_number").removeClass('hidden');
                } else {
                    $("#project_number").addClass('hidden');
                }
            }
        });
    })
});
</script>


@endsection