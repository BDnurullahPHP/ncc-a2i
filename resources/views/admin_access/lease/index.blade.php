@extends('admin_access.layout.master')

@section('style')
<style type="text/css">
    .radio-inline .radio{
        padding-top: 2px;
    }
    .append_item{
        padding-top: 24px;
    }
    .append_item span.remove_file{
        width: 20px;
        height: 20px;
        position: absolute;
        top: 13px;
        left: auto;
        right: 7px;
        line-height: 20px;
        border: 1px solid #B55858;
        border-radius: 50% !important;
        font-size: 14px;
        text-align: center;
        color: #B55858;
        cursor: pointer;
        z-index: 1000;
    }
    .append_item span.remove_file.remove_recept{
        top: 0;
        right: -5px;
        width: 22px;
        height: 22px;
        line-height: 20px;
        border: 1px solid #B55858;
        text-align: center;
        border-radius: 50% !important;
    }
    #land_form input[type=file]{
        border: 1px solid #C2CAD8;
    }
    #land_form .input-group-addon>i{
        color: #333;
    }
    .ui-datepicker{
        z-index: 99999 !important;
    }
    .help-block{
        color: red;
    }
    #land_use_table tbody>tr>td{
        position: relative;
        text-align: center;
    }
    #land_use_table tbody tr.total_row{
        display: none;
    }
    span.land_type_use{
        position: absolute;
        top: -13px;
        left: auto;
        right: -11px;
        width: 22px;
        height: 22px;
        line-height: 20px;
        border: 1px solid #B55858;
        text-align: center;
        border-radius: 50% !important;
        cursor: pointer;
    }

    .project_details label{
        font-weight: 700;
    }
    .project_details p{
        margin: 0px;
    }
    @media only screen and (min-width: 768px){
        button.land_type_add{
            margin-top: 25px;
        }
    }
</style>
@endsection

@section('content')

<h3>{{ trans('lease.sayarata_mahal_list') }}
    <small></small>
    @if($access->CREATE == 1)
    <a href="#" data-toggle="modal" data-target="#modal_add_content" class="btn btn-primary btn-sm pull-right" data-form="{{ route('lease_create_form') }}">{{ trans('common.add_new') }} <i class="fa fa-plus"></i></a>
    @endif
</h3>

<div class="portlet light bordered">
    <div class="row">
        <div class="col-md-5 col-sm-6 col-xs-12">
            <div id="lg_form_message"></div>
        </div>
    </div>
    <div class="portlet-body contentArea">

        <table class="table table-striped table-bordered table-hover" data-source="{{ route('lease_data_table') }}" id="common_table">
            <thead>
                <tr>
                    <th style="width: 10px;">{{ trans('common.table_sl') }}</th>
                    <th>{{ trans('lease.mahal_number') }}</th>
                    <th>{{ trans('lease.mahal_bn_name') }}</th>
                    <th>{{ trans('lease.mahal_address')}}</th>
                    <th>{{ trans('common.table_action') }}</th>
                </tr>
            </thead>
            <tbody>

            </tbody>
        </table>

    </div>
</div>

@endsection

@section('script')

<script src="{{ Helpers::asset('assets/admin/js/project.js') }}" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function() {
projectManaged.init();
});
</script>



@endsection