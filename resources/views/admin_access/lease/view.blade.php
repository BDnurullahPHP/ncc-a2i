<div class="project_details">

    <!--=============Title for modal===============-->
    <div class="modal_top_title">{{ trans('lease.mahal_details_info') }}</div>
    <!--===========End title for modal=============-->

    <div class="row padding-bottom-10">
        <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="row">
                {!! Form::label('', trans('lease.mahal_lend'), array('class'=>'col-sm-6')) !!}
                <div class="col-md-6 col-sm-6">
                    <p>
                        @foreach($projectLand as $key => $land)
                        {{ $land->getLand->LAND_NUMBER }}
                        @endforeach
                    </p>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="row">
                {!! Form::label('', trans('lease.mahal_number'), array('class'=>'col-sm-6')) !!}
                <div class="col-md-6 col-sm-6">
                    <p>{{ $project->PR_UD_ID }}</p>
                </div>
            </div>
        </div>
    </div>
    <div class="row padding-bottom-10">
        <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="row">
                {!! Form::label('', trans('lease.mahal_bn_name'), array('class'=>'col-sm-6')) !!}
                <div class="col-md-6 col-sm-6">
                    <p>{{ $project->PR_NAME_BN }}</p>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="row">
                {!! Form::label('', trans('lease.mahal_en_name'), array('class'=>'col-sm-6')) !!}
                <div class="col-md-6 col-sm-6">
                    <p>{{ $project->PR_NAME }}</p>
                </div>
            </div>
        </div>
    </div>
    <div class="row padding-bottom-10">

        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="row">
                {!! Form::label('', trans('lease.mahal_address'), array('class'=>'col-sm-3')) !!}
                <div class="col-md-6 col-sm-8">
                    {!!$project->PROJECT_LOCATION!!}
                </div>
            </div>
        </div>
    </div>
</div>