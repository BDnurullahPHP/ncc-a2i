{!! Form::open(array('url' => '#', 'id'=>'project_form', 'class'=>'form-horizontal', 'method' => 'post')) !!}

<!--=============Title for modal===============-->
<div class="modal_top_title">{{ trans('lease.add_mahal_title') }}</div>
<!--===========End title for modal=============-->

<!--========Show form post message=============-->
<div class="form-group">
    <div class="col-sm-6 form_messsage">

    </div>
</div>
<!--========End form post message=============-->
<div class="form-group">
    <div class="col-md-4 col-sm-4 col-xs-6">
        {!! Form::label('area', trans('land.land_zone')) !!}
        {!! Form::select('area', $area, array(Input::old('area')), array('class'=>'form-control', 'placeholder' => trans('common.form_select'))) !!}
    </div>
    <div class="col-md-4 col-sm-4 col-xs-6">
        {!! Form::label('ward', trans('land.land_ward')) !!}
        {!! Form::select('ward', [], array(Input::old('ward')), array('class'=>'form-control', 'placeholder' => trans('common.form_select'))) !!}
    </div>
    <div class="col-md-4 col-sm-4 col-xs-6">
        {!! Form::label('mouza', trans('land.land_form_mouza')) !!}
        {!! Form::select('mouza', [], array(Input::old('mouza')), array('class'=>'form-control', 'placeholder' => trans('common.form_select'))) !!}
    </div>
</div>

<div class="form-group">
    <div class="col-md-6">
        {!! Form::label('LAND_ID', trans('lease.mahal_lend')) !!} <span class="required_field">*</span>
        <select name="LAND_ID[]" class="form-control multi_select form-required" id="project_lands" data-placeholder="{{ trans('common.form_select') }}" multiple="multiple">
        </select>
        <div id="error_msg1"></div>
    </div>
    <div class="col-md-6">
        {!! Form::label('PR_UD_ID', trans('lease.mahal_number')) !!} <span class="required_field">*</span>
        {!! Form::text('PR_UD_ID', null, array('class'=>'form-control form-required', 'placeholder'=>'Ex. padma-30/07/2016')) !!}
    </div>
</div>

<div class="form-group">
    <div class="col-md-6">
        {!! Form::label('PR_NAME_BN', trans('lease.mahal_bn_name')) !!} <span class="required_field">*</span>
        {!! Form::text('PR_NAME_BN', null, array('class'=>'form-control form-required', 'placeholder'=>trans('lease.mahal_bn_ex'))) !!}
    </div>
    <div class="col-md-6">
        {!! Form::label('PR_NAME', trans('lease.mahal_en_name')) !!} <span class="required_field">*</span>
        {!! Form::text('PR_NAME', null, array('class'=>'form-control form-required', 'placeholder'=>trans('lease.mahal_ex'))) !!}
    </div>
</div>

<div class="form-group">
    <div class="col-md-12">
        {!! Form::label('PROJECT_LOCATION', trans('lease.mahal_address')) !!} <span class="required_field">*</span>
        {!! Form::textarea('PROJECT_LOCATION', null, array('class'=>'form-control reactor_basic form-required', 'placeholder'=>'বঙ্গবন্ধু সড়ক, নারায়ণগঞ্জ, বাংলাদেশ')) !!}
    </div>
</div>

<div class="form-group">
    <div class="col-md-6">
        {!! Form::label('is_active', trans('common.is_active')) !!}<br/>
        {!! Form::checkbox('IS_ACTIVE', 1, true, ['class'=>'status']) !!}
    </div>
</div>

<div class="form-group">
    <div class="col-md-12" style="padding-top: 15px;">
        <span class="modal_msg pull-left"></span>
        <button type="submit" data-action="{{ route('lease_create') }}" class="btn btn-success btn-sm pull-right form_submit" style="margin-left: 5px;">{{ trans('common.form_submit') }}</button>
        <button type="button" class="btn btn-default btn-sm reset_form pull-right">{{ trans('common.form_reset') }}</button>
        <span class="loadingImg"></span>
    </div>
</div>

{!! Form::close() !!}