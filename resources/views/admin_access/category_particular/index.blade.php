@extends('admin_access.layout.master')

@section('content')

<h3>{{ trans('account.category_particular') }}
    <small></small>
    @if($access->CREATE == 1)
    <a href="#" data-toggle="modal" data-target="#modal_add_content" class="btn btn-primary btn-sm pull-right" data-form="{{ route('category_particular_create_form') }}">{{ trans('common.add_new') }} <i class="fa fa-plus"></i></a>
    @endif	
</h3>
<div class="portlet light bordered">
    <div class="row">
        <div class="col-md-5 col-sm-6 col-xs-12">
            <div id="lg_form_message"></div>
        </div>
    </div>
    <div class="portlet-body contentArea">

        <table class="table table-striped table-bordered table-hover" data-source="{{ route('category_particular_data') }}" id="common_table">
            <thead>
                <tr>
                    <th>{{ trans('common.table_sl') }}</th>
                    <th>{{ trans('account.category_name') }}</th>
                    <th>{{ trans('account.particular_name') }}</th>
                    <th>{{ trans('common.amount') }}</th>
                    <th>{{ trans('common.table_head_created_at') }}</th>
                    <th>{{ trans('common.table_action') }}</th>
                </tr>
            </thead>
            <tbody>

            </tbody>
        </table>

    </div>
</div>

@endsection

@section('script')


@endsection