{!! Form::open(array('url' => '#', 'id'=>'category_particular_form', 'class'=>'form-horizontal', 'method' => 'post')) !!}
    <!--=============Title for modal===============-->
    <div class="modal_top_title">{{ trans('account.particular_cra_title') }}</div>
    <!--===========End title for modal=============-->
    
    <!--========Show form post message=============-->
    <div class="form-group">
        <div class="col-sm-6 col-sm-offset-3 form_messsage">
            
        </div>
    </div>
    <!--========End form post message=============-->

    <div class="form-group">
        <div class="col-md-3 text-right">
            {!! Form::label('PR_CATEGORY', trans('account.category_name'), array('class'=>'control-label')) !!}
            <span class="required_field">*</span>
        </div>
        <div class="col-sm-6">
            {!! Form::select('PR_CATEGORY', $category, null, array('id'=>'project_category', 'class'=>'form-control form-required', 'placeholder' => trans('common.form_select'))) !!}
            <p class="form-example-helper">{{ trans('common.example') }}: দোকান</p>
        </div>
    </div>
    <div class="form-group">
        <div class="col-md-3 text-right">
            {!! Form::label('PARTICULAR_ID', trans('account.particular_name'), array('class'=>'control-label')) !!}
            <span class="required_field">*</span>
        </div>
        <div class="col-sm-6">
            {!! Form::select('PARTICULAR_ID', $particular, null, array('id'=>'particular_id', 'class'=>'form-control form-required', 'placeholder' => trans('common.form_select'))) !!}
            <p class="form-example-helper">{{ trans('common.example') }}: ভ্যাট</p>
        </div>
    </div>
    <div class="form-group">
        <div class="col-md-3 text-right">
            {!! Form::label('PARTICULAR_AMT', trans('common.amount'), array('class'=>'control-label')) !!}
            <span class="required_field">*</span>
        </div>
        <div class="col-sm-6">
            {!! Form::text('PARTICULAR_AMT', null, array('class'=>'form-control allow_numeric form-required', 'placeholder'=>trans('account.particular_amount_pl'))) !!}
            <p class="form-example-helper">{{ trans('common.example') }}: {{ trans('account.particular_amount_ex') }}</p>
        </div>
    </div>
    <div class="form-group">
        <div class="col-md-3 text-right">
            {!! Form::label('UOM', trans('common.uom'), array('class'=>'control-label')) !!}
            <span class="required_field">*</span>
        </div>
        <div class="col-sm-6">
            {!! Form::select('UOM', [1=>'শতকরা', 2=>'টাকা'], null, array('class'=>'form-control form-required', 'placeholder' => trans('common.form_select'))) !!}
            <p class="form-example-helper">{{ trans('common.example') }}: শতকরা</p>
        </div>
    </div>
    <div class="form-group">
        {!! Form::label('REMARKS', trans('common.comment'), array('class'=>'col-md-3 control-label')) !!}
        <div class="col-sm-9">
            {!! Form::textarea('REMARKS', null, array('rows' =>'3', 'class'=>'form-control reactor_basic', 'placeholder'=>trans('common.your_comment'))) !!}
        </div>
    </div>
    <div class="form-group">
        <div class="col-md-3 text-right">
            {!! Form::label('IS_ACTIVE', trans('common.is_active')) !!}
        </div>
        <div class="col-sm-6">
            {!! Form::checkbox('IS_ACTIVE', 1, true) !!}
        </div>
    </div>
    <!--==========================================-->
    <div class="form-group">
        <div class="col-md-offset-3 col-md-5" style="padding-top: 15px;">
            <span class="modal_msg pull-left"></span>
                <button type="button" class="btn btn-default btn-sm reset_form">{{ trans('common.form_reset') }}</button>
                <button type="submit" data-action="{{ route('category_particular_create') }}" class="btn btn-success btn-sm form_submit">{{ trans('common.form_submit') }}</button>
        </div>
    </div>
{!! Form::close() !!}