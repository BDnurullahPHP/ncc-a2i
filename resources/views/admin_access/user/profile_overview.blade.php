<div class="row">
    <div class="col-md-4 col-sm-4 col-xs-12">
        <strong>{{ trans('user.name') }}</strong>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12">
        {{ $user_info->FULL_NAME }}
    </div>
</div>
<div class="row">
    <div class="col-md-4 col-sm-4 col-xs-12">
        <strong>{{ trans('user.user_name') }}</strong>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12">
        {{ $user_info->USERNAME }}
    </div>
</div>
<div class="row">
    <div class="col-md-4 col-sm-4 col-xs-12">
        <strong>{{ trans('user.email') }}</strong>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12">
        {{ $user_info->EMAIL }}
    </div>
</div>
<div class="row">
    <div class="col-md-4 col-sm-4 col-xs-12">
        <strong>{{ trans('user.mobile') }}</strong>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12">
        {{ $user_info->MOBILE_NO }}
    </div>
</div>
@if(!empty($user_info->getDetails))
    @if(!empty($user_info->getDetails->DESIGNATION))
        <div class="row">
            <div class="col-md-4 col-sm-4 col-xs-12">
                <strong>{{ trans('user.designation') }}</strong>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12">
                {{ $user_info->getDetails->DESIGNATION }}
            </div>
        </div>
    @endif
    @if(!empty($user_info->getDetails->ADDRESS))
        <div class="row">
            <div class="col-md-4 col-sm-4 col-xs-12">
                <strong>{{ trans('user.address') }}</strong>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12">
                {{ $user_info->getDetails->ADDRESS }}
            </div>
        </div>
    @endif
@endif