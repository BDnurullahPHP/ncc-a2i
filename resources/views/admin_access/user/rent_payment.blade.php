<div class="row" style="margin-left: -20px;margin-right: -20px;">
    <div class="col-md-12">
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-gift"></i>ফ্ল্যাট / দোকান / প্লট সমূহের এর বিবরণ
                </div>
                <div class="tools">
                    <a href="javascript:;" class="collapse"> </a>
                    <a title="" data-original-title="" href="javascript:;" class="remove"> </a>
                </div>
            </div>
            <div class="portlet-body">
                
                <div class="tabbable-custom nav-justified">
                    <ul class="nav nav-tabs nav-justified">
                        <li class="active">
                            <a href="ui_tabs_accordions_navs.html#tab_1_1_1" data-toggle="tab">ফ্ল্যাট</a>
                        </li>
                        
                        <li>
                            <a href="ui_tabs_accordions_navs.html#tab_1_1_2" data-toggle="tab">দোকান</a>
                        </li>
                        <li>
                            <a href="ui_tabs_accordions_navs.html#tab_1_1_3" data-toggle="tab">স্পেস</a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab_1_1_1">
                            <div class="table-scrollable">
                                <table class="table table-hover">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>ফ্ল্যাটের নাম</th>
                                            <th>ধরন</th>
                                            <th>পরিমান</th>
                                            <th>মাসিক ভাড়া</th>
                                            <th>বিস্তারিত</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>১</td>
                                            <td>পদ্ম সিটি প্লাজা-১ এসএম মালেহ রোড</td>
                                            <td>টাইপ-A</td>
                                            <td>১৩৬২ বর্গফুট</td>
                                            <td>১০,০০০৳</td>
                                            <td>
                                                <a class="btn btn-default btn-xs openBigModal" id=""
                                                   data-action="#" data-type="edit" title="বিস্তারিত"><i
                                                        class="fa fa-eye"></i>
                                                </a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>২</td>
                                            <td>পদ্ম সিটি প্লাজা -২ মহিম গাঙ্গুলী রোড</td>
                                            <td>টাইপ-D</td>
                                            <td>১৩০৬ বর্গফুট</td>
                                            <td>১১,০০০৳</td>
                                            <td>
                                                <a class="btn btn-default btn-xs openBigModal" id=""
                                                   data-action="#" data-type="edit" title="বিস্তারিত"><i
                                                        class="fa fa-eye"></i>
                                                </a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>৩</td>
                                            <td>পদ্ম সিটি প্লাজা -৩ মহিম গাঙ্গুলী রোড</td>
                                            <td>টাইপ-B</td>
                                            <td>১৩০৬ বর্গফুট</td>
                                            <td>১১,৩০০৳</td>
                                            <td>
                                                <a class="btn btn-default btn-xs openBigModal" id=""
                                                   data-action="#" data-type="edit" title="বিস্তারিত"><i
                                                        class="fa fa-eye"></i>
                                                </a>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="box-footer clearfix">
                                <a class="btn btn-sm btn-default btn-flat pull-right" href="#">আরো দেখুন</a>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab_1_1_2">
                            <div class="tab-pane active" id="tab_1_1_1">
                                <div class="table-scrollable">
                                    <table class="table table-hover">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>মার্কেটের নাম</th>
                                            <th>দোকান নং</th>
                                            <th>পরিমান</th>
                                            <th>মাসিক ভাড়া </th>
                                            <th>বিস্তারিত</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>১</td>
                                            <td>পদ্ম সিটি প্লাজা -৪ মহিম গাঙ্গুলী রোড</td>
                                            <td>১</td>
                                            <td>৬২ বর্গফুট</td>
                                            <td>১০,০০০৳</td>
                                            <td>
                                                <a class="btn btn-default btn-xs openBigModal" id=""
                                                   data-action="#" data-type="edit" title="বিস্তারিত"><i
                                                        class="fa fa-eye"></i>
                                                </a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>২</td>
                                            <td>পদ্ম সিটি প্লাজা -৫ মহিম গাঙ্গুলী রোড</td>
                                            <td>২</td>
                                            <td>৭২.৫৫ বর্গফুট</td>
                                            <td>১০,০০০৳</td>
                                            <td>
                                                <a class="btn btn-default btn-xs openBigModal" id=""
                                                   data-action="#" data-type="edit" title="বিস্তারিত"><i
                                                        class="fa fa-eye"></i>
                                                </a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>৩</td>
                                            <td>পদ্ম সিটি প্লাজা -৬ মহিম গাঙ্গুলী রোড</td>
                                            <td>৩</td>
                                            <td>৬৯.৭৮ বর্গফুট</td>
                                            <td>১৫,০০০৳</td>
                                            <td>
                                                <a class="btn btn-default btn-xs openBigModal" id=""
                                                   data-action="#" data-type="edit" title="বিস্তারিত"><i
                                                        class="fa fa-eye"></i>
                                                </a>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                </div>
                            </div>
                            <div class="box-footer clearfix">
                                <a class="btn btn-sm btn-default btn-flat pull-right" href="#">আরো দেখুন</a>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab_1_1_3">
                            <div class="tab-pane active" id="tab_1_1_1">
                                <div class="table-scrollable">
                                    <table class="table table-hover">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>স্পেস এর  অবস্থান  </th>
                                            <th>স্পেসের ধরন </th>
                                            <th>পরিমান</th>
                                            <th>স্পেসের ভাড়া (প্রতি বর্গফুট)</th>
                                            <th>বিস্তারিত</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>১</td>
                                            <td>পদ্ম সিটি প্লাজা -৭ মহিম গাঙ্গুলী রোড</td>
                                            <td> ইউনিট A</td>
                                            <td>৬২ বর্গফুট</td>
                                            <td>৬.০০৳</td>
                                            <td>
                                                <a class="btn btn-default btn-xs openBigModal" id=""
                                                   data-action="#" data-type="edit" title="বিস্তারিত"><i
                                                        class="fa fa-eye"></i>
                                                </a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>২</td>
                                            <td>পদ্ম সিটি প্লাজা -৮ মহিম গাঙ্গুলী রোড</td>
                                            <td>ইউনিট B</td>
                                            <td>৭২.৫৫ বর্গফুট</td>
                                            <td>৬.০০৳</td>
                                            <td>
                                                <a class="btn btn-default btn-xs openBigModal" id=""
                                                   data-action="#" data-type="edit" title="বিস্তারিত"><i
                                                        class="fa fa-eye"></i>
                                                </a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>৩</td>
                                            <td>পদ্ম সিটি প্লাজা -৯ মহিম গাঙ্গুলী রোড</td>
                                            <td>ইউনিট C</td>
                                            <td>৬৯.৭৮ বর্গফুট</td>
                                            <td>৬.০০৳</td>
                                            <td>
                                                <a class="btn btn-default btn-xs openBigModal" id=""
                                                   data-action="#" data-type="edit" title="বিস্তারিত"><i
                                                        class="fa fa-eye"></i>
                                                </a>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                </div>
                            </div>
                            <div class="box-footer clearfix">
                                <a class="btn btn-sm btn-default btn-flat pull-right" href="#">আরো দেখুন</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>