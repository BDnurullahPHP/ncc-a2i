@extends('admin_access.layout.master')

@section('style')
<style type="text/css">
    .help-block{
        color: red;
    }
    .disable_submit{
        pointer-events: none;
        opacity: 0.45;
    }
    .form-has-error{
        border: 1px solid red;
    }
</style>
@endsection

@section('script')
<script type="text/javascript">
    $(document).on("change", "#userGroup", function() {
        var groupID = $(this).val();
        var action = $('#groupTolevel').val();
        $.ajax({
            type: "GET",
            url: action,
            data: {groupID: groupID},
            success: function(result) {
                $('#userGroupLevel').html(result);
            }
        });
    });
</script>
@endsection

@section('content')

<h3>{{ trans('user.all_user') }}
    <small></small>
    @if($access->CREATE == 1)
    <a href="#" data-toggle="modal" data-target="#modal_add_content" class="btn btn-primary btn-sm pull-right" data-form="{{ route('user_create_form') }}">{{ trans('common.add_new') }} <i class="fa fa-plus"></i></a>
    @endif
</h3>
<div class="portlet light bordered">
    <!-- <div class="portlet-title">
        <div class="caption">
            
        </div>
        <div class="tools"> </div>
    </div> -->
    <div class="row">
        <div class="col-md-5 col-sm-6 col-xs-12">
            <div id="lg_form_message"></div>
        </div>
    </div>
    <div class="portlet-body contentArea">

        <table class="table table-striped table-bordered table-hover" data-source="{{ route('user_data') }}" id="common_table">
            <thead>
                <tr>
                    <th>{{ trans('common.table_sl') }}</th>
                    <th>{{ trans('user.name') }}</th>
                    <th>{{ trans('user.user_name') }}</th>
                    <th>{{ trans('user.email') }}</th>
                    <th>{{ trans('user.role') }}</th>
                    <th>{{ trans('user.group') }}</th>
                    <th>{{ trans('user.level') }}</th>
<!--                    <th>{{ trans('common.table_head_created_at') }}</th>-->
                    <th>{{ trans('common.table_action') }}</th>
                </tr>
            </thead>
            <tbody>

            </tbody>
        </table>

    </div>
</div>

@endsection