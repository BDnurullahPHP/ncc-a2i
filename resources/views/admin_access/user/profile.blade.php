@extends('admin_access.layout.master')

@section('css_file')

    <link href="{{ Helpers::asset('assets/pages/css/profile.min.css') }}" rel="stylesheet" type="text/css" />
	
@endsection

@section('style')

	<link href="{{ Helpers::asset('assets/admin/css/user.css') }}" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .membership_date{
            font-size: 12px;
            /* font-style: italic; */
            color: #757575;
        }
    </style>

@endsection

@section('content')

	<h3 class="page-header">{{ trans('user.profile') }}
    	<small></small>
	</h3>

	<div class="row">
        <div class="col-md-12">
            <!-- BEGIN PROFILE SIDEBAR -->
            <div class="profile-sidebar">
                <!-- PORTLET MAIN -->
                <div class="portlet light profile-sidebar-portlet ">
                    <!-- SIDEBAR USERPIC -->
                    <?php 
                        if ( !empty($user_info->getDetails) ) {
                            $image_file = $user_info->getDetails->IMAGE.'.'.$user_info->getDetails->CONTENT_TYPE;
                        } else {
                            $image_file = '';
                        }
                    ?>
                    <div class="profile-userpic">
                        @if( !empty($user_info->getDetails) && !empty($user_info->getDetails->IMAGE) && File::exists(public_path()."/upload/user/$image_file") )
                            <img src='{{ Helpers::asset("upload/user/$image_file") }}' class="profile_image img-responsive" alt="">
                        @else
                            <img src="{{ Helpers::asset('assets/img/logo/profile_icon.jpg') }}" class="profile_image img-responsive" alt="">
                        @endif
                    </div>
                    <!-- END SIDEBAR USERPIC -->
                    <!-- SIDEBAR USER TITLE -->
                    <div class="profile-usertitle">
                        <div class="profile-usertitle-name">
                            {{ $user_info->FULL_NAME }}
                        </div>
                        <div class="membership_date">Member since {{ date('M Y', strtotime($user_info->CREATED_AT)) }}</div>
                        @if($user_info->USER_TYPE == 'C')
                            <div class="row citizen_user_info">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="col-md-5 col-sm-5 col-xs-12">
                                        <label for="">{{ trans('user.name') }}</label>
                                    </div>
                                    <div class="col-md-7 col-sm-7 col-xs-12">
                                        <p>{{ $user_info->FULL_NAME }}</p>
                                    </div>
                                </div>
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="col-md-5 col-sm-5 col-xs-12">
                                        <label for="">{{ trans('user.user_name') }}</label>
                                    </div>
                                    <div class="col-md-7 col-sm-7 col-xs-12">
                                        <p>{{ $user_info->USERNAME }}</p>
                                    </div>
                                </div>
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="col-md-5 col-sm-5 col-xs-12">
                                        <label for="">{{ trans('user.email') }}</label>
                                    </div>
                                    <div class="col-md-7 col-sm-7 col-xs-12">
                                        <p>{{ $user_info->EMAIL }}</p>
                                    </div>
                                </div>
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="col-md-5 col-sm-5 col-xs-12">
                                        <label for="">{{ trans('user.mobile') }}</label>
                                    </div>
                                    <div class="col-md-7 col-sm-7 col-xs-12">
                                        <p>{{ $user_info->MOBILE_NO }}</p>
                                    </div>
                                </div>
                                @if(!empty($user_info->getDetails))
                                    @if(!empty($user_info->getDetails->DESIGNATION))
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="col-md-5 col-sm-5 col-xs-12">
                                                <label for="">{{ trans('user.designation') }}</label>
                                            </div>
                                            <div class="col-md-7 col-sm-7 col-xs-12">
                                                <p>{{ $user_info->getDetails->DESIGNATION }}</p>
                                            </div>
                                        </div>
                                    @endif
                                    @if(!empty($user_info->getDetails->ADDRESS))
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="col-md-5 col-sm-5 col-xs-12">
                                                <label for="">{{ trans('user.address') }}</label>
                                            </div>
                                            <div class="col-md-7 col-sm-7 col-xs-12">
                                                <p>{{ $user_info->getDetails->ADDRESS }}</p>
                                            </div>
                                        </div>
                                    @endif
                                @endif
                            </div>
                        @endif
                    </div>
                    
                    <!-- END SIDEBAR USER TITLE -->
                </div>
                <!-- END PORTLET MAIN -->
            </div>
            <!-- END BEGIN PROFILE SIDEBAR -->
            <!-- BEGIN PROFILE CONTENT -->
            <div class="profile-content">
                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet light ">
                            <div class="portlet-title tabbable-line">
                                <ul class="nav nav-tabs">
                                	<li class="active">
                                        <a href="#account_overview" data-toggle="tab">
                                            {{ trans('user.overview') }}
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#change_password" data-toggle="tab">
                                            {{ trans('user.change_password') }}
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#change_info" data-toggle="tab">
                                            {{ trans('user.change_information') }}
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#change_picture" data-toggle="tab">
                                            {{ trans('user.change_picture') }}
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <div class="portlet-body">
                                <div class="tab-content">
                                    <!--== OVERVIEW TAB ==-->
                                    <div class="tab-pane active" id="account_overview">
                                        
                                    </div>
                                    <!--== END OVERVIEW TAB ==-->
                                    <!-- CHANGE PASSWORD TAB -->
                                    <div class="tab-pane" id="change_password">
                                        {!! Form::open(array('url' => '#', 'method' => 'post', 'id' => 'password_form')) !!}
                                            <!--========Show form post message=============-->
                                            <div class="form-group">
                                                <div class="col-md-6 col-sm-6 col-xs-12 form_messsage" style="padding:0;">
                                                    
                                                </div>
                                            </div>
                                            <!--========End form post message=============-->
                                            <div class="clearfix"></div>
                                            
                                            <div class="form-group">
                                                {!! Form::label('current_password', trans('user.current_password'), array('class'=>'control-label')) !!}
                                                {!! Form::password('current_password', array('class'=>'form-control form-password', 'placeholder'=>trans('user.current_password_pl'))) !!}
                                            </div>
                                            <div class="form-group">
                                                {!! Form::label('new_password', trans('user.new_password'), array('class'=>'control-label')) !!}
                                                {!! Form::password('new_password', array('class'=>'form-control form-password', 'placeholder'=>trans('user.new_password_pl'))) !!}
                                            </div>
                                            <div class="form-group">
                                                {!! Form::label('retype_password', trans('user.retype_password'), array('class'=>'control-label')) !!}
                                                {!! Form::password('retype_password', array('class'=>'form-control form-repassword', 'placeholder'=>trans('user.retype_password_pl'))) !!}
                                            </div>
                                            <div class="margin-top-10">
                                                <button type="button" class="btn default reset_form"> {{ trans('common.form_reset') }} </button>
                                                <button type="submit" class="btn green info_change"> {{ trans('common.update') }} </button>
                                            </div>
                                        {!! Form::close() !!}
                                    </div>
                                    <!-- END CHANGE PASSWORD TAB -->
                                    <!-- PERSONAL INFO TAB -->
                                    <div class="tab-pane" id="change_info">
                                        {!! Form::model($user_info, array('url' => '#', 'method' => 'post', 'id' => 'information_form')) !!}
                                            <!--========Show form post message=============-->
                                            <div class="form-group">
                                                <div class="col-md-6 col-sm-6 col-xs-12 form_messsage" style="padding:0;">
                                                    
                                                </div>
                                            </div>
                                            <!--========End form post message=============-->
                                            <div class="clearfix"></div>

                                            <div class="form-group">
                                                {!! Form::label('FIRST_NAME', trans('user.first_name'), array('class'=>'control-label')) !!}
                                                {!! Form::text('FIRST_NAME', Input::old('FIRST_NAME'), array('class'=>'form-control form-required', 'placeholder'=>trans('user.first_name_pl'))) !!}
                                            </div>
                                            <div class="form-group">
                                                {!! Form::label('LAST_NAME', trans('user.last_name'), array('class'=>'control-label')) !!}
                                                {!! Form::text('LAST_NAME', Input::old('LAST_NAME'), array('class'=>'form-control', 'placeholder'=>trans('user.last_name_pl'))) !!}
                                            </div>
                                            <div class="form-group">
                                                {!! Form::label('USERNAME', trans('user.user_name'), array('class'=>'control-label')) !!}
                                                {!! Form::text('USERNAME', Input::old('USERNAME'), array('class'=>'form-control', 'placeholder'=>'user123')) !!}
                                            </div>
                                            <div class="form-group">
                                                {!! Form::label('DESIGNATION', trans('user.designation'), array('class'=>'control-label')) !!}
                                                @if( !empty($user_info->getDetails) && !empty($user_info->getDetails->DESIGNATION) )
                                                    {!! Form::text('DESIGNATION', $user_info->getDetails->DESIGNATION, array('class'=>'form-control', 'placeholder'=>trans('user.designation_pl'))) !!}
                                                @else
                                                    {!! Form::text('DESIGNATION', Input::old('DESIGNATION'), array('class'=>'form-control', 'placeholder'=>trans('user.designation_pl'))) !!}
                                                @endif
                                            </div>
                                            <div class="form-group">
                                                {!! Form::label('MOBILE_NO', trans('user.mobile'), array('class'=>'control-label')) !!}
                                                {!! Form::text('MOBILE_NO', Input::old('MOBILE_NO'), array('class'=>'form-control', 'placeholder'=>'01xxx xxxxxx')) !!}
                                            </div>
                                            <div class="form-group">
                                                {!! Form::label('ADDRESS', trans('user.address'), array('class'=>'control-label')) !!}
                                                @if( !empty($user_info->getDetails) && !empty($user_info->getDetails->ADDRESS) )
                                                    {!! Form::textarea('ADDRESS', $user_info->getDetails->ADDRESS, array('rows' =>'3', 'class'=>'form-control', 'placeholder'=>trans('user.address_pl'))) !!}
                                                @else
                                                    {!! Form::textarea('ADDRESS', Input::old('ADDRESS'), array('rows' =>'3', 'class'=>'form-control', 'placeholder'=>trans('user.address_pl'))) !!}
                                                @endif
                                            </div>
                                            <div class="margiv-top-10">
                                                <button type="button" class="btn default reset_form"> {{ trans('common.form_reset') }} </button>
                                                <button type="submit" class="btn green info_change"> {{ trans('common.update') }} </button>
                                            </div>
                                        {!! Form::close() !!}
                                    </div>
                                    <!-- END PERSONAL INFO TAB -->
                                    <!-- CHANGE AVATAR TAB -->
                                    <div class="tab-pane" id="change_picture">
                                        {!! Form::open(array('url' => '#', 'method' => 'post')) !!}
                                            <!--========Show form post message=============-->
                                            <div class="form-group">
                                                <div class="col-md-6 col-sm-6 col-xs-12 form_messsage" style="padding:0;">
                                                    
                                                </div>
                                            </div>
                                            <!--========End form post message=============-->
                                            <div class="clearfix"></div>

                                            <div class="form-group">
                                                <div class="home_thumb">
                                                    <h5 style="margin-top:10px;margin-bottom:15px;">
                                                        {{ trans('user.profile_file_type') }}
                                                    </h5>
                                                    <div id="image_error"></div>
                                                    <div class="thumb_img_box">
                                                        @if( !empty($user_info->getDetails) && !empty($user_info->getDetails->IMAGE) && File::exists(public_path()."/upload/user/$image_file") )
                                                            <img class="img-responsive center-block" src='{{ Helpers::asset("upload/user/$image_file") }}' alt="">
                                                            <button type="button" class="btn btn-primary btn-xs fileclick"><i class="fa fa-plus"></i> {{ trans('common.change') }}</button>
                                                        @else
                                                            <img src="#" class="img-responsive center-block" alt="" style="display:none;">
                                                            <button type="button" class="btn btn-primary btn-xs fileclick"><i class="fa fa-plus"></i> {{ trans('common.add') }}</button>
                                                        <button type="button" class="btn btn-danger btn-xs hide_comimg"><i class="fa fa-times"></i> {{ trans('common.remove') }}</button>
                                                        @endif
                                                        
                                                    </div>
                                                        <input type="file" id="profile_image" name="image" style="display:none;">
                                                </div>
                                            </div>
                                            <div class="margiv-top-10">
                                                <button type="button" class="btn green change_image"> {{ trans('common.update') }} </button>
                                            </div>
                                        {!! Form::close() !!}
                                    </div>
                                    <!-- END CHANGE AVATAR TAB -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END PROFILE CONTENT -->
        </div>
    </div>

@endsection

@section('script')
    
    <script type="text/javascript">
        var get_overview_src    = "{{ route('profile_overview', $user_info->USER_ID) }}";
        var update_pass_url     = "{{ route('update_password', $user_info->USER_ID) }}";
        var update_profile_url  = "{{ route('update_profile', $user_info->USER_ID) }}";
        var update_picture_url  = "{{ route('update_profile_picture', $user_info->USER_ID) }}";
    </script>
    <script src="{{ Helpers::asset('assets/admin/js/user_profile.js') }}" type="text/javascript"></script>

@endsection