{!! Form::open(array('url' => '#', 'id'=>'role_form', 'class'=>'form-horizontal', 'method' => 'post')) !!}
    
<input type="hidden" id="groupTolevel" value="{{ route('get_group_to_level') }}">
    <!--=============Title for modal===============-->
    <div class="modal_top_title">{{ trans('user.user_add_data') }}</div>
    <!--===========End title for modal=============-->

    <!--========Show form post message=============-->
    <div class="form-group">
        <div class="col-sm-6 col-sm-offset-3 form_messsage">
            
        </div>
    </div>
    <!--========End form post message=============-->
<span style="color: red">সব ফিল্ড বাধ্যতামূলক</span>
    <div class="form-group">
        {!! Form::label('FIRST_NAME', trans('user.first_name'), array('class'=>'col-sm-3 control-label')) !!}
        <div class="col-sm-6">
            {!! Form::text('FIRST_NAME', Input::old('FIRST_NAME'), array('class'=>'form-control form-required', 'placeholder'=>trans('user.first_name_pl'))) !!}
            <p class="form-example-helper">{{ trans('user.example_first_name') }}</p>
        </div>
    </div>
    <div class="form-group">
        {!! Form::label('LAST_NAME', trans('user.last_name'), array('class'=>'col-sm-3 control-label')) !!}
        <div class="col-sm-6">
            {!! Form::text('LAST_NAME', Input::old('LAST_NAME'), array('class'=>'form-control form-required', 'placeholder'=>trans('user.last_name'))) !!}
            <p class="form-example-helper">{{ trans('user.example_last_name') }}</p>
        </div>
    </div>
    <div class="form-group">
        {!! Form::label('GENDER', trans('user.gender'), array('class'=>'col-sm-3 control-label')) !!}
        <div class="col-sm-6">
            <label class="radio-inline">
                {!! Form::radio('GENDER', 'm', true, ['class'=>'form-required']) !!} {{ trans('user.male') }}
            </label>
            <label class="radio-inline">
                {!! Form::radio('GENDER', 'f', false, ['class'=>'form-required']) !!} {{ trans('user.female') }}
            </label>
        </div>
    </div>
    <div class="form-group">
        {!! Form::label('MOBILE_NO', trans('user.mobile'), array('class'=>'col-sm-3 control-label')) !!}
        <div class="col-sm-6">
            {!! Form::text('MOBILE_NO', Input::old('MOBILE_NO'), array('class'=>'form-control form-required', 'placeholder'=>'01xxx xxxxxx')) !!}
        </div>
    </div>
    <div class="form-group">
        {!! Form::label('EMAIL', trans('user.email'), array('class'=>'col-sm-3 control-label')) !!}
        <div class="col-sm-6">
            {!! Form::email('EMAIL', Input::old('EMAIL'), array('class'=>'form-control form-email', 'placeholder'=>trans('user.email_pl'))) !!}
            <p class="form-example-helper">{{ trans('user.example_email') }}</p>
        </div>
    </div>
    <div class="form-group">
        {!! Form::label('USERNAME', trans('user.user_name'), array('class'=>'col-sm-3 control-label')) !!}
        <div class="col-sm-6">
            {!! Form::text('USERNAME', Input::old('USERNAME'), array('id'=>'user_unique', 'class'=>'form-control form-required', 'placeholder'=>trans('user.user_name_pl'))) !!}
            <p class="form-example-helper">{{ trans('user.example_user_name') }}</p>
        </div>
    </div>
    <div class="form-group">
        {!! Form::label('PASSWORD', trans('user.password'), array('class'=>'col-sm-3 control-label')) !!}
        <div class="col-sm-6">
            {!! Form::password('PASSWORD', array('id'=>'password', 'class'=>'form-control form-required form-password', 'placeholder'=>trans('user.password_pl'))) !!}
        </div>
    </div>
    <div class="form-group">
        {!! Form::label('repassword', trans('user.repassword'), array('class'=>'col-sm-3 control-label')) !!}
        <div class="col-sm-6">
            {!! Form::password('repassword', array('class'=>'form-control form-required form-repassword', 'placeholder'=>trans('user.repassword_pl'))) !!}
        </div>
    </div>
    <div class="form-group">
        {!! Form::label('USER_TYPE', trans('user.user_type'), array('class'=>'col-sm-3 control-label')) !!}
        <div class="col-sm-6">
            {!! Form::select('USER_TYPE', ['N'=>'Internal', 'C'=>'Citizen'], null, array('class'=>'form-control form-required', 'placeholder' => trans('common.form_select'))) !!}
        </div>
    </div>
    <div class="form-group">
        {!! Form::label('role_id', trans('user.group'), array('class'=>'col-sm-3 control-label')) !!}
        <div class="col-sm-6">
            {!! Form::select('group_id', $group, null, array('class'=>'form-control form-required', 'id'=>'userGroup', 'placeholder' => trans('common.form_select'))) !!}
        </div>
    </div>
    <div class="form-group">
        {!! Form::label('role_id', trans('user.level'), array('class'=>'col-sm-3 control-label')) !!}
        <div class="col-sm-6">
            <select id="userGroupLevel" name="groupLevel" class="form-control form-required">
                <option value="">নির্বাচন করুন</option>
            </select>
        </div>
    </div>
    <div class="form-group">
        {!! Form::label('role_id', trans('user.role'), array('class'=>'col-sm-3 control-label')) !!}
        <div class="col-sm-6">
            {!! Form::select('role_id', $role, null, array('class'=>'form-control form-required', 'placeholder' => 'Not define')) !!}
            <p class="form-example-helper">{{ trans('user.example_role') }}</p>
        </div>
    </div>
    <div class="form-group">
        {!! Form::label('IS_ACTIVE', trans('common.is_active'), array('class'=>'col-sm-3 control-label')) !!}
        <div class="col-sm-6">
            <label class="radio-inline">
                {!! Form::radio('IS_ACTIVE', 1, ['class'=>'form-required']) !!} {{ trans('land.yes') }}
            </label>
            <label class="radio-inline">
                {!! Form::radio('IS_ACTIVE', 0, ['class'=>'form-required']) !!} {{ trans('land.no') }}
            </label>
        </div>
    </div>
    
    <div class="form-group">
        <div class="col-md-offset-3 col-md-5" style="padding-left: 22px; padding-top: 15px;">
            <span class="modal_msg pull-left"></span>
                <button type="button" class="btn btn-default btn-sm reset_form">{{ trans('common.form_reset') }}</button>
                <button type="submit" data-action="{{ route('user_create') }}" class="btn btn-success btn-sm form_submit">{{ trans('common.form_submit') }}</button>
            <span class="loadingImg"></span>
        </div>
    </div>
{!! Form::close() !!}