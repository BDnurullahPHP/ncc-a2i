{!! Form::open(array('url' => '#', 'id'=>'project_category_form', 'class'=>'form-horizontal', 'method' => 'post')) !!}
    <!--=============Title for modal===============-->
    <div class="modal_top_title">{{ trans('project.add_data_category') }}</div>
    <!--===========End title for modal=============-->
    
    <!--========Show form post message=============-->
    <div class="form-group">
        <div class="col-sm-8 col-sm-offset-1 form_messsage">
            
        </div>
    </div>
    <!--========End form post message=============-->
     <!--========End form post message=============-->

    <div class="form-group">
        <label for="CATE_NAME" class="col-sm-3 control-label">{{ trans('project.project_category_name') }} <span style="color: red">*</span> </label>
        <div class="col-sm-5">
            {!! Form::text('CATE_NAME', Input::old('CATE_NAME'), array('class'=>'form-control form-required', 'placeholder'=>trans('project.project_category_name_ex'))) !!}
        </div>
    </div>
    <div class="form-group">
        <label for="CATE_DESC" class="col-sm-3 control-label">{{ trans('project.project_category_des') }} </label>
        <div class="col-sm-9">
            {!! Form::textarea('CATE_DESC', Input::old('CATE_DESC'), array('rows' =>'2','class'=>'form-control reactor_basic', 'placeholder'=>trans('project.project_category_name_des_ex'))) !!}
        </div>
    </div>
    <div class="form-group">
        <label for="IS_ACTIVE" class="col-sm-3 control-label">{{ trans('land.is_lease') }}</label>
        <div class="col-sm-7">
            <label class="control-label">
                {!! Form::checkbox('IS_LEASE', '1', false, ['class'=>'status']) !!}
            </label>
        </div>
    </div>
    <div class="form-group">
        <label for="IS_ACTIVE" class="col-sm-3 control-label">{{ trans('common.is_active') }}</label>
        <div class="col-sm-7">
            <label class="control-label">
                {!! Form::checkbox('IS_ACTIVE', '1', true, ['class'=>'status']) !!}
            </label>
        </div>
    </div>
    <div class="form-group">
        <div class="col-lg-offset-3 col-lg-10">
            <span class="modal_msg pull-left"></span>
                <button type="button" class="btn btn-default btn-sm reset_form">{{ trans('common.form_reset') }}</button>
                <button type="submit" data-action="{{ route('project_category_save') }}" class="btn btn-success btn-sm form_submit">{{ trans('common.form_submit') }}</button>
            <span class="loadingImg"></span>
        </div>
    </div>
{!! Form::close() !!}