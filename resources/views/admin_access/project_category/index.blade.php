@extends('admin_access.layout.master')

@section('style')

@endsection

@section('content')

<h3>{{ trans('project.project_heading_categorys') }}
    <small></small>
    @if($access->CREATE == 1)
    <a href="#" data-toggle="modal" data-target="#modal_add_content" class="btn btn-primary btn-sm pull-right" data-form="{{ route('project_category_create_form') }}">{{ trans('common.add_new') }} <i class="fa fa-plus"></i></a>
    @endif
</h3>
<div class="portlet light bordered">
    {{-- <div class="portlet-title">
            <div class="caption">
                
            </div>
            <div class="tools"> </div>
        </div> --}}
    <div class="portlet-body contentArea">

        <table class="table table-striped table-bordered table-hover" data-source="{{ route('project_category_data') }}" id="common_table">
            <thead>
                <tr>
                    <th>{{ trans('common.table_sl') }}</th>
                    <th>{{ trans('project.project_category_name') }}</th>
                    <th>{{ trans('project.project_category_des') }}</th>
                    <th>{{ trans('common.table_head_created_at') }}</th>
                    <th>{{ trans('common.table_action') }}</th>
                </tr>
            </thead>
            <tbody>

            </tbody>
        </table>

    </div>
</div>

@endsection