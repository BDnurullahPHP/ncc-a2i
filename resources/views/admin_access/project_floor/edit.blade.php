{!! Form::model($floor, array('url' => '#', 'id'=>'project_floor_form', 'class'=>'form-horizontal', 'method' => 'post')) !!}
    <!--=============Title for modal===============-->
    <div class="modal_top_title">{{ trans('project.add_data_floor_edit') }}</div>
    <!--===========End title for modal=============-->
    
    <!--========Show form post message=============-->
    <div class="form-group">
        <div class="col-sm-5 col-sm-offset-3 form_messsage">
            
        </div>
    </div>
    <!--========End form post message=============-->
     <!--========End form post message=============-->

    <div class="form-group">
        <label for="FLOOR_NUMBER" class="col-sm-3 control-label">{{ trans('project.floor_number') }} <span style="color: red">*</span> </label>
        <div class="col-sm-5">
            {!! Form::text('FLOOR_NUMBER', Input::old('FLOOR_NUMBER'), array('class'=>'form-control form-required', 'placeholder'=>trans('project.project_floor_name_ex'))) !!}
        </div>
    </div>
     
     <div class="form-group">
        <label for="FLOOR_NUMBER_BN" class="col-sm-3 control-label">{{ trans('project.floor_number_bn') }} <span style="color: red">*</span> </label>
        <div class="col-sm-5">
            {!! Form::text('FLOOR_NUMBER_BN', Input::old('FLOOR_NUMBER_BN'), array('class'=>'form-control form-required', 'placeholder'=>trans('project.project_floor_bn_name_ex'))) !!}
        </div>
    </div>
     
    <div class="form-group">
        <label for="DETAILS" class="col-sm-3 control-label">{{ trans('project.project_type_des') }}</label>
        <div class="col-sm-9">
            {!! Form::textarea('DETAILS', Input::old('DETAILS'), array('rows' =>'2','class'=>'form-control reactor_basic')) !!}
        </div>
    </div>
    <div class="hr-line-dashed"></div>
    <div class="form-group">
        <label for="IS_ACTIVE" class="col-sm-3 control-label">{{ trans('common.is_active') }}</label>
        <div class="col-sm-7">
            <label class="control-label">
                {!! Form::checkbox('IS_ACTIVE', '1', true, ['class'=>'status']) !!}
            </label>
        </div>
    </div>
  <div class="form-group">
        <div class="col-lg-offset-3 col-lg-10">
            <span class="modal_msg pull-left"></span>
                <button type="button" class="btn btn-default btn-sm reset_form">{{ trans('common.form_reset') }}</button>
                <button type="submit" data-action="{{ route('project_floor_update', $floor->FLOOR_ID) }}" class="btn btn-success btn-sm form_submit">{{ trans('common.form_submit') }}</button>
            <span class="loadingImg"></span>
        </div>
    </div>
{!! Form::close() !!}