{!! Form::open(array('url' => '#', 'id'=>'lease_category_form', 'class'=>'form-horizontal', 'method' => 'post')) !!}
    
    <!--========Show form post message=============-->
    <div class="form-group">
        <div class="col-sm-5 col-sm-offset-3 form_messsage">
            
        </div>
    </div>
    <!--========End form post message=============-->
    
    <div class="form-group">
        <label for="category" class="col-sm-3 control-label">{{ trans('common.lease_category') }} <span style="color: red">*</span></label>
        <div class="col-sm-5">
            {!! Form::select('category', $category, array(Input::old('category')), array('class'=>'form-control form-required', 'placeholder' => trans('common.form_select'))) !!}
        </div>
    </div>
    <div class="form-group">
        <label for="name" class="col-sm-3 control-label">{{ trans('common.lease_subcategory') }} </label>
        <div class="col-sm-5">
            {!! Form::text('name', Input::old('name'), array('class'=>'form-control', 'placeholder'=>trans('common.lease_subcategory'))) !!}
        </div>
    </div>
    <div class="form-group">
        <label for="bn_name" class="col-sm-3 control-label">{{ trans('common.lease_bn_subcategory') }} <span style="color: red">*</span></label>
        <div class="col-sm-5">
            {!! Form::text('bn_name', Input::old('bn_name'), array('class'=>'form-control form-required', 'placeholder'=>trans('common.lease_bn_subcategory'))) !!}
        </div>
    </div>
    <div class="hr-line-dashed"></div>
    <div class="form-group">
        <label for="is_active" class="col-sm-3 control-label">Active?</label>
        <div class="col-sm-7">
            <label class="control-label">
                {!! Form::checkbox('is_active', '1', true, ['class'=>'status']) !!}
            </label>
        </div>
    </div>
    <div class="form-group">
        <div class="col-lg-offset-3 col-lg-10">
            <span class="modal_msg pull-left"></span>
                <button type="button" class="btn btn-default btn-sm reset_form">{{ trans('common.form_reset') }}</button>
                <button type="submit" data-action="{{ route('lease_subcategory_create') }}" class="btn btn-primary btn-sm form_submit">{{ trans('common.form_submit') }}</button>
            <span class="loadingImg"></span>
        </div>
    </div>
{!! Form::close() !!}