<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>রাজস্ব আদায় এর বিবরণ</title>
    </head>
    <style>
        body {
            font-family: freeserif,sans-serif,vrinda,solaiman-lipi,monospace;
        }
        h1, h2, h3, h4, p, input, th, td{
            font-family: freeserif,sans-serif,vrinda,solaiman-lipi,monospace;
            font-weight: 400;
            white-space: initial;
        }
        .sectionrow
        {
            width: 100%;
            border: 1px ;            
            border-style: solid ;
            font-size: 12px; 
        }
        h4{
            text-align:center; 
        }
        p{
            text-align: justify;
        }
        table, th, td {
            border: .25px solid;
            border-collapse: collapse;
            text-align: center;
        }

    </style>
    <body>
        <h4>
            <bold>নারায়ণগঞ্জ সিটি করপোরেশন</bold>
            <br/>নগর ভবন, ১০ বঙ্গবন্ধু রোড
            <br/>নারায়ণগঞ্জ
            <br/>www.ncc.gov.bd
        </h4>
        <h3>{{trans('citizen.description_of_revenue_collection')}}:-</h3>
        <table style="width:100%">
            <thead>
                <tr>
                    <th>{{trans('citizen.sl_no')}}</th>
                    <th>{{trans('citizen.customer_name')}} </th>
                    <th>{{trans('citizen.memo_no_date')}}</th>
                    <th>{{trans('citizen.income_source')}}</th>
                    <th>{{trans('citizen.month')}}</th>
                    <th>{{trans('citizen.year')}}</th>
                    <th>{{trans('citizen.payable_money')}}</th>
                    <th>{{trans('citizen.money_received')}}</th>
                    <th>{{trans('citizen.due_money')}} </th>
                </tr>
            </thead>
            <tbody>
                @foreach($revenue_data as $key => $value)
                <tr>
                    <td>{{ $key+1 }}</td>
                    <td>
                        @if (!empty($value->getCitizen))
                            {{ $value->getCitizen->getTenderCitizen->FULL_NAME }}
                        @elseif (!empty($value->getApplicant))
                            {{ $value->getApplicant->APPLICANT_NAME }}
                        @elseif (!empty($value->getLeaseDemesne))
                            {{ $value->getLeaseDemesne->NAME }}
                        @else
                            
                        @endif
                    </td>
                    <td>
                        {{ $value->VOUCHER_NO }}<br/>
                        {{ date('d/m/Y', strtotime($value->TRX_TRAN_DT)) }}
                    </td>
                    <td>{{ $value->getParticular->PARTICULAR_NAME . '&#40;' . $value->getCategory->CATE_NAME .'&#41;' }}</td>
                    <td>{{ date('F', strtotime($value->TRX_TRAN_DT)) }}</td>
                    <td>{{ date('Y', strtotime($value->TRX_TRAN_DT)) }}</td>
                    <td>{{ number_format($value->TOTAL_PENDING,2) }}</td>
                    <td>{{ number_format($value->TOTAL_PAID,2) }}</td>
                    <td>{{ number_format($value->TOTAL_PENDING - $value->TOTAL_PAID,2) }}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </body>
</html>