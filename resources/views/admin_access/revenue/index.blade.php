@extends('admin_access.layout.master')

@section('content')

<h3>{{trans('citizen.revenue_sector_list')}}  
    <small></small>
</h3>
{!! Form::open(array('url' => route('revenue_data'), 'id'=>'search_form', 'method' => 'post')) !!}
    <div class="row">
        <div class="col-md-2">
            <div class="form-group">
                <label class="control-label"><strong>{{trans('citizen.year')}} </strong></label>
                <select name="year" class="form-control form-search" required="required">
                    <option value="">{{trans('citizen.select')}} </option>
                    <?php
                        for ($i = 2015; $i <= date('Y'); $i++) {
                            echo '<option value="' . $i . '">' . Helpers::en2bn($i) . '</option>';
                        }
                    ?>
                </select>
            </div>
        </div>
        <div class="col-md-2">
            <div class="form-group">
                <label class="control-label"><strong>{{trans('citizen.month')}} </strong></label>
                <select name="month" class="form-control form-search">
                    <option value="">{{trans('citizen.select')}}</option>
                    <option value="1">{{trans('citizen.january')}} </option>
                    <option value="2">{{trans('citizen.february')}} </option>
                    <option value="3">{{trans('citizen.march')}} </option>
                    <option value="4">{{trans('citizen.april')}} </option>
                    <option value="5">{{trans('citizen.may')}} </option>
                    <option value="6">{{trans('citizen.jun')}} </option>
                    <option value="7">{{trans('citizen.july')}} </option>
                    <option value="8">{{trans('citizen.august')}} </option>
                    <option value="9">{{trans('citizen.septembar')}} </option>
                    <option value="10">{{trans('citizen.octobar')}}</option>
                    <option value="11">{{trans('citizen.nobember')}}</option>
                    <option value="12">{{trans('citizen.december')}}</option>
                </select>
            </div>
        </div>
        <div class="col-md-2">
            <div class="form-group">
                <label class="control-label"><strong>{{trans('citizen.sector_name')}}  </strong></label>
                {!! Form::select('CATEGORY', $category, null, array('id'=>'category', 'class'=>'form-control form-search', 'placeholder' => trans('common.form_select'))) !!}
            </div>
        </div>
        <div class="col-md-2">
            <div class="form-group">
                <label class="control-label"><strong>{{trans('citizen.income_source')}}  </strong></label>
                {!! Form::select('PARTICULAR_ID', $particular, null, array('id'=>'particular', 'class'=>'form-control form-search', 'placeholder' => trans('common.form_select'))) !!}
            </div>
        </div>
        <div class="col-md-2">
            <div class="form-group">
                <label class="control-label"><strong>{{trans('citizen.customer_name')}}  </strong></label>
                <select name="APPLICANT_ID" id="applicant" class="form-control form-search">
                    <option value="">{{ trans('common.form_select') }}</option>
                    @foreach($te_applicant as $key => $app)
                        <option value="{{ $app->TYPE }}_{{ $app->ID }}">
                            {{ $app->NAME }}&#40;{{ $app->TYPE }}&#41;
                        </option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="col-md-2">
            <div class="form-group" style="padding-top: 25px; ">
                <label class="control-label"></label><b/>
                @if($access->READ == 1)
                <button type="button" class="btn btn-sm blue search_data_btn"><i class="fa fa-search"></i>{{trans('citizen.search')}} </button>
                <a href="{{ route('revenue_data_print') }}" class="btn btn-sm btn-default search_report_btn"><i class="fa fa-print"></i> pdf</a> 
            @endif
            </div>
        </div>
    </div>
{!! Form::close() !!}
<div class="portlet light bordered" style="display: none;">
    <div class="row">
        <div class="col-md-5 col-sm-6 col-xs-12">
            <div id="lg_form_message"></div>
        </div>
    </div>
    <div class="portlet-body contentArea">
        <table class="table table-striped table-bordered table-hover search_table_data">
            <thead>
                <tr>
                    <th>{{trans('citizen.sl_no')}}</th>
                    <th>{{trans('citizen.customer_name')}}</th>
                    <th>{{trans('citizen.memo_no_date')}}</th>
                    <th>{{trans('citizen.income_source')}}</th>
                    <th>{{trans('citizen.month')}}</th>
                    <th>{{trans('citizen.year')}}</th>
                    <th>{{trans('citizen.payable_money')}}</th>
                    <th>{{trans('citizen.money_received')}}</th>
                    <th>{{trans('citizen.due_money')}} </th>
                </tr>
            </thead>
            <tbody>

            </tbody>
        </table>
    </div>
</div>

@endsection

@section('script')
    <script src="{{ Helpers::asset('assets/admin/js/search.js') }}" type="text/javascript"></script>
    <script type="text/javascript">
        jQuery(document).ready(function($) {
            AdvanceSearch.init();
        });
    </script>
@endsection