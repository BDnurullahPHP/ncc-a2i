@extends('admin_access.layout.master')

@section('content')

<h3>{{ trans('account.citizen_collection_ledger') }} 
    <small></small>
</h3>
{!! Form::open(array('url' => route('citizen_ledger_data'), 'id'=>'search_form', 'method' => 'post')) !!}
<input type="hidden" id="user_tender_schedule_url" value="{{ route('user_tender_schedule') }}">
<input type="hidden" id="user_id_val" class="form-search" name="APPLICANT_USER" value="">
<div class="row">
    <div class="col-md-10">
        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label class="control-label"><strong>{{trans('citizen.start_date')}}</strong></label>
                    <input type="text" name="START_DATE" class="form-control date_picker_default form-search" placeholder="{{ trans('common.date_pl') }}">
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label class="control-label"><strong>{{trans('citizen.end_date')}} </strong></label>
                    <input type="text" name="END_DATE" class="form-control date_picker_default form-search" placeholder="{{ trans('common.date_pl') }}">
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label class="control-label"><strong>{{trans('citizen.customer_mobile')}} </strong></label>
                    {!! Form::text('MOBILE_NO', null, array('id' => 'mobile_no', 'class'=>'form-control form-search auto_search_no', 'placeholder'=>'01XXXXXXXXX')) !!}
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label class="control-label"><strong>{{trans('citizen.sector_name')}} </strong></label>
                    {!! Form::select('CATEGORY', $category, null, array('id'=>'category', 'class'=>'form-control form-search', 'placeholder' => trans('common.form_select'))) !!}
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label class="control-label"><strong>{{trans('citizen.tender')}}</strong></label>
                    {!! Form::select('SCHEDULE_ID', [], null, array('id'=>'tender_schedule', 'class'=>'form-control form-search', 'placeholder' => trans('common.form_select'))) !!}
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label class="control-label"><strong>{{trans('citizen.income_source')}}</strong></label>
                    {!! Form::select('PARTICULAR_ID', $particular, null, array('id'=>'particular', 'class'=>'form-control form-search', 'placeholder' => trans('common.form_select'))) !!}
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-2">
        <div class="form-group" style="padding-top: 25px; ">
            <label class="control-label"></label><b/>
            @if($access->READ == 1)
            <button type="button" class="btn btn-sm blue search_data_btn"><i class="fa fa-search"></i>{{trans('citizen.search')}} </button>
            <a href="{{ route('citizen_ledger_print') }}" class="btn btn-sm btn-default search_report_btn"><i class="fa fa-print"></i> pdf</a> 
            @endif
        </div>
    </div>
</div>
{!! Form::close() !!}
<div class="portlet light bordered" style="display: none;">
    <div class="row">
        <div class="col-md-5 col-sm-6 col-xs-12">
            <div id="lg_form_message"></div>
        </div>
    </div>
    <div class="portlet-body contentArea">
        <table class="table table-striped table-bordered table-hover search_table_data">
            <thead>
                <tr>
                    <th>{{trans('citizen.sl_no')}}</th>
                    <th>{{trans('citizen.customer_name')}} </th>
                    <th>{{trans('citizen.memo_no_date')}}</th>
                    <th>{{trans('citizen.income_source')}} </th>
                    <th>{{trans('citizen.month')}}</th>
                    <th>{{trans('citizen.year')}}</th>
                    <th>{{trans('citizen.payable_money')}}</th>
                    <th>{{trans('citizen.money_received')}}</th>
                    <th>{{trans('citizen.due_money')}} </th>
                </tr>
            </thead>
            <tbody>

            </tbody>
        </table>
    </div>
</div>

@endsection

@section('style')
<style type="text/css">
    .ui-autocomplete{
        max-height: 220px;
        overflow-x: hidden;
        overflow-y: scroll;
    }
</style>
@endsection

@section('script')
<script type="text/javascript">
    var citizen_number = "{{ route('citizen_number_search') }}";
</script>
<script src="{{ Helpers::asset('assets/admin/js/search.js') }}" type="text/javascript"></script>
<script type="text/javascript">
    jQuery(document).ready(function($) {
        AdvanceSearch.init();
        MoneyCollection.init();
    });
</script>
@endsection