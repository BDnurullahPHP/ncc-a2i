{!! Form::open(array('url' => '#', 'id'=>'project_form', 'class'=>'form-horizontal', 'method' => 'post')) !!}
    
    <!--========Show form post message=============-->
    <div class="form-group">
        <div class="col-sm-5 col-sm-offset-3 form_messsage">
            
        </div>
    </div>
    <!--========End form post message=============-->

    <div class="form-group">
        <div class="col-sm-6">
            {!! Form::label('PROJECT_ID', trans('project.project_name')) !!}
            {!! Form::select('PROJECT_ID', [1=>'পদ্ম সিটি প্লাজা-১', 2=>'পদ্ম সিটি প্লাজা-৩'], null, array('class'=>'form-control form-required', 'placeholder' => trans('common.form_select'))) !!}
        </div>
        <div class="col-sm-6">
            {!! Form::label('PR_CATEGORY', trans('project.project_dropdown_category')) !!}
            {!! Form::select('PR_CATEGORY', $project_cat, null, array('class'=>'form-control form-required', 'placeholder' => trans('common.form_select'))) !!}
        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-6">
            {!! Form::label('PR_TYPE', trans('project.project_details_type')) !!} <span class="help_tooltip" data-toggle="tooltip" data-placement="right" title="{{ trans('project.project_details_type_pl') }}"><i class="fa fa-question-circle" aria-hidden="true"></i></span>
            {!! Form::select('PR_TYPE', $project_type, null, array('class'=>'form-control form-required', 'placeholder' => trans('common.form_select'))) !!}
        </div>
        <div class="col-sm-6">
            {!! Form::label('POSITION', trans('project.project_details_position')) !!} <span class="help_tooltip" data-toggle="tooltip" data-placement="right" title="{{ trans('project.project_position_help') }}"><i class="fa fa-question-circle" aria-hidden="true"></i></span>
            {!! Form::text('POSITION', null, array('class'=>'form-control form-required', 'placeholder'=>trans('project.project_details_position_pl'))) !!}
        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-4">
            {!! Form::label('PR_SSF_NO', trans('project.project_pr_ssf_no')) !!}
            {!! Form::text('PR_SSF_NO', null, array('class'=>'form-control form-required', 'placeholder'=>trans('project.project_pr_ssf_no_pl'))) !!}
        </div>
        <div class="col-sm-4">
            {!! Form::label('PR_MEASURMENT', trans('project.project_details_size')) !!}
            {!! Form::text('PR_MEASURMENT', null, array('class'=>'form-control form-required', 'placeholder'=>trans('project.project_details_size_pl'))) !!}
        </div>
        <div class="col-sm-4">
            {!! Form::label('UOM', trans('common.unit')) !!}
            {!! Form::select('UOM', [1=>'বর্গ ফুট'], null, array('class'=>'form-control form-required', 'placeholder' => trans('common.form_select'))) !!}
        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-4">
            {!! Form::label('IS_RESERVED', trans('project.is_reserved')) !!}
            <label class="control-label">
                {!! Form::checkbox('IS_RESERVED', '1', false, ['class'=>'status']) !!}
            </label>
        </div>
        <div class="col-sm-4">
            {!! Form::label('IS_BUILT', trans('project.is_build')) !!}
            <label class="control-label">
                {!! Form::checkbox('IS_BUILT', '1', true, ['class'=>'status']) !!}
            </label>
        </div>
        <div class="col-sm-4">
            {!! Form::label('IS_ACTIVE', trans('common.is_active')) !!}
            <label class="control-label">
                {!! Form::checkbox('IS_ACTIVE', '1', true, ['class'=>'status']) !!}
            </label>
        </div>
    </div>
    
    <div class="form-group">
        <div class="col-md-12" style="padding-top: 15px;">
            <span class="modal_msg pull-left"></span>
                <button type="submit" data-action="{{ route('role_create') }}" class="btn btn-primary btn-sm pull-right form_submit" style="margin-left: 5px;">{{ trans('common.form_submit') }}</button>
                <button type="button" class="btn btn-default btn-sm pull-right reset_form">{{ trans('common.form_reset') }}</button>
            <span class="loadingImg"></span>
        </div>
    </div>
{!! Form::close() !!}