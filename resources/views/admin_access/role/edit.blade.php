{!! Form::model($role, array('url' => '#', 'id'=>'role_form', 'class'=>'form-horizontal', 'method' => 'post')) !!}
    
    <!--========Show form post message=============-->
    <div class="form-group">
        <div class="col-sm-5 col-sm-offset-3 form_messsage">
            
        </div>
    </div>
    <!--========End form post message=============-->

    <div class="form-group">
        {!! Form::label('NAME', trans('security.role_name'), array('class'=>'col-sm-3 control-label')) !!}
        <div class="col-sm-5">
            {!! Form::text('NAME', Input::old('NAME'), array('class'=>'form-control form-required', 'placeholder'=>trans('security.role_name'))) !!}
            <p class="form-example-helper">{{ trans('common.example') }}: land-manager</p>
        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-3 control-label">
            {!! Form::label('DISPLAY_NAME', trans('security.display_name'), array('style'=>'margin-bottom:0px;')) !!}
            <!-- <p class="form-example-helper">&#40;{{ trans('common.optional') }}&#41;</p> -->
        </div>
        <div class="col-sm-5">
            {!! Form::text('DISPLAY_NAME', Input::old('DISPLAY_NAME'), array('class'=>'form-control form-required', 'placeholder'=>trans('security.display_name'))) !!}
            <p class="form-example-helper">{{ trans('common.example') }}: Land manager</p>
        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-3 control-label">
            {!! Form::label('DESCRIPTION', trans('security.role_details')) !!}
            <!-- <p class="form-example-helper">&#40;{{ trans('common.optional') }}&#41;</p> -->
        </div>
        <div class="col-md-5">
            {!! Form::textarea('DESCRIPTION', null, array('rows' =>'2', 'class'=>'form-control form-required', 'placeholder'=>trans('security.role_details'))) !!}
            <p class="form-example-helper">{{ trans('common.example') }}: Manage all land operation</p>
        </div>
    </div>
    <div class="form-group">
        <div class="col-md-offset-3 col-md-5" style="padding-top: 15px;">
            <button type="button" class="btn btn-default btn-sm reset_form">{{ trans('common.form_reset') }}</button>
            <button type="submit" data-action="{{ route('role_update', $role->ROLE_ID) }}" class="btn btn-success btn-sm form_submit">{{ trans('common.form_submit') }}</button>
        </div>
    </div>
{!! Form::close() !!}