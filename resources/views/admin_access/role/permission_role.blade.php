@extends('admin_access.layout.master')

@section('style')
	<style type="text/css">
	    .help-block{
	        color: red;
	    }
	    .portlet>.portlet-title>.caption{
	    	min-width: 325px;
	    }
	    .portlet>.portlet-title>.caption .form-control{
	    	max-width: 270px;
	    	float: left;
	    }
	    #content_body .checkbox-inline{
	    	min-width: 200px;
	    	margin-left: 0px !important;
	    	padding-bottom: 10px;
	    }
	    input#check_all{
	    	cursor: pointer;
	    }
	    p.ps_dtls{
	    	font-size: 12px;
	    	color: #656565;
	    	margin-bottom: 10px;
	    }
	</style>
@endsection

@section('content')

	<h3>{{ trans('security.assign_permission') }}
    	<small></small>
	</h3>
	<div class="portlet light bordered">
	    <div class="portlet-title">
	        <div class="caption">
            	{!! Form::select('role', $roles, array(Input::old('role')), array('id'=>'role_select', 'class'=>'form-control', 'placeholder' => trans('security.role_select'))) !!}
            	<img src="{{ Helpers::asset('assets/img/loader.gif') }}" alt="" class="form_loader">
	            
	        </div>
	        <div class="tools"> </div>
	    </div>
	    <div class="row">
	    	<div class="col-md-5 col-sm-6 col-xs-12">
	    		<div id="lg_form_message"></div>
	    	</div>
	    </div>
	    <div class="portlet-body contentArea">
			<div class="row">
				<div class="col-md-8 col-sm-8 col-xs-12" id="content_body">
					
				</div>
			</div>
	    </div>
	</div>

@endsection

@section('script')

	<script type="text/javascript">
		$(document).ready(function() {

			$('#role_select').change(function(event) {
				var role = $(this).val();
				$('#lg_form_message').html('');
				$('#content_body').html('');

				if (role != '') {
					$('img.form_loader').show();

					$.ajax({
						url: "{{ route('role_permissions') }}",
						type: 'GET',
						dataType: 'html',
						data: {role: role},
					})
					.done(function(res) {
						$('#content_body').html(res);
						$('img.form_loader').hide();
					});
				}
			});

			$(document.body).on('change', '#check_all', function(event) {
				$("input:checkbox").prop('checked', $(this).prop("checked"));
			});

			$(document.body).on('click', '#update_permission_role', function(event) {

				event.preventDefault();
				$('img.form_loader').show();
				$('#lg_form_message').html('');
				var role_id 	= $('#role_select').val();
				var token 		= $(this).closest('form').find('input[name=_token]').val();
				var permission 	= $(this).closest('form').find('.checkbox-inline');

				var form_data 	= new FormData();
				form_data.append('_token', token);
				form_data.append('role_id', role_id);

				permission.each(function(index, el) {
					if ($(this).find('input').is(':checked')) {
						form_data.append('permission_id['+index+']', $(this).find('input').val());
					}
                });

				$.ajax({
					url: "{{ route('assign_permissions') }}",
                    dataType: 'html',
                    cache: false,
                    contentType: false,
                    processData: false,
                    async: false,
                    data: form_data,
                    type: 'post'
				})
				.done(function(res) {
					$('img.form_loader').hide();
					$('#lg_form_message').html('<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'+res+'</div>');
				});
			});

		});
	</script>

</script>


@endsection