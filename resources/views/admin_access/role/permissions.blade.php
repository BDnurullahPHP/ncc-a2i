{!! Form::open(array('url' => '#', 'id'=>'role_form', 'class'=>'form-horizontal', 'method' => 'post')) !!}
    
    <label><input type="checkbox" id="check_all"/> {{ trans('common.check_all') }}</label>
	<!--========Show form post message=============-->
	<div class="form-group">
	    <div class="col-sm-5 col-sm-offset-3 form_messsage">
	        
	    </div>
	</div>
	<!--========End form post message=============-->
	@if(count($permissions) > 0)
		@foreach($permissions as $key => $perm)
			<label class="checkbox-inline">
				<input name="permission_id[]" type="checkbox" <?php if( in_array($perm->PERM_ID, (array)$permission_role, true) ) echo 'checked ="checked"' ?> value="{{ $perm->PERM_ID }}"> {{ $perm->DISPLAY_NAME }}
				<p class="ps_dtls">{{ $perm->DESCRIPTION }}</p>
			</label>
		@endforeach
		<?php unset($perm); ?>
	@endif

	<div class="form-group" style="margin-top: 15px">
	    <button type="submit" data-action="{{ route('land_create') }}" id="update_permission_role" class="btn green btn-sm" style="margin-left: 15px">{{ trans('common.save') }}</button>
	</div>

{!! Form::close() !!}