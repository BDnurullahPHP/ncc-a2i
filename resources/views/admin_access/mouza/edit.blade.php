{!! Form::model($mouza, array('url' => '#', 'id'=>'mouza_form', 'class'=>'form-horizontal', 'method' => 'post')) !!}
    <!--=============Title for modal===============-->
    <div class="modal_top_title">{{ trans('land.mouza_title_edit') }}</div>
    <!--===========End title for modal=============-->

    <!--========Show form post message=============-->
    <div class="form-group">
        <div class="col-sm-5 col-sm-offset-3 form_messsage">
            
        </div>
    </div>
    <!--========End form post message=============-->
    <div class="form-group">
        <label for="zone" class="col-sm-3 control-label">{{ trans('land.select_zone') }} <span style="color: red">*</span></label>
        <div class="col-sm-5">
            <select class="form-control form-required" id="zone_id" name="zone">
                <option value="">{{ trans('common.form_select') }}</option>
                @foreach ($zone as $z)
                    <option <?php if($z->WARD_ID==$zone_id) echo 'selected="selected"'; ?> value="{{ $z->WARD_ID }}">{{ $z->WARD_NAME_BN }}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="form-group">
        <label for="word" class="col-sm-3 control-label">{{ trans('land.select_word') }} <span style="color: red">*</span></label>
        <div class="col-sm-5">
            <select class="form-control form-required" id="word_id" name="WARD_ID">
                    <option value="">{{ trans('common.form_select') }}</option>
                @foreach ($wards as $ward)
                    <option <?php if($ward->WARD_ID == $mouza->WARD_ID) echo 'selected="selected"';?> value="{{$ward->WARD_ID}}">{{$ward->WARD_NAME_BN}}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="form-group">
        <label for="MOUZA_NAME" class="col-sm-3 control-label">{{ trans('land.mouza_name') }} <span style="color: red">*</span></label>
        <div class="col-sm-5">
            {!! Form::text('MOUZA_NAME', Input::old('MOUZA_NAME'), array('class'=>'form-control form-required', 'placeholder'=>trans('common.table_head_name'))) !!}
        </div>
    </div>
    <div class="form-group">
        <label for="MOUZA_NAME_BN" class="col-sm-3 control-label">{{ trans('land.mouza_bn_name') }} <span style="color: red">*</span></label>
        <div class="col-sm-5">
            {!! Form::text('MOUZA_NAME_BN', Input::old('MOUZA_NAME_BN'), array('class'=>'form-control form-required', 'placeholder'=>trans('common.table_head_bn_name'))) !!}
        </div>
    </div>
    <div class="hr-line-dashed"></div>
    <div class="form-group">
        <label for="IS_ACTIVE" class="col-sm-3 control-label">{{ trans('common.is_active') }}</label>
        <div class="col-sm-7">
            <label class="control-label">
                @if($mouza->IS_ACTIVE == 1)
                    {!! Form::checkbox('IS_ACTIVE', '1', true, ['class'=>'status']) !!}
                @else
                    {!! Form::checkbox('IS_ACTIVE', '1', false, ['class'=>'status']) !!}
                @endif
            </label>
        </div>
    </div>
    <div class="form-group">
        <div class="col-lg-offset-3 col-lg-10">
            <span class="modal_msg pull-left"></span>
                <button type="button" class="btn btn-default btn-sm reset_form">{{ trans('common.form_reset') }}</button>
                <button type="submit" data-unique="{{ route('mouza_update_unique', $mouza->MOUZA_ID) }}" data-action="{{ route('mouza_update', $mouza->MOUZA_ID) }}" class="btn btn-success btn-sm form_submit">{{ trans('common.form_submit') }}</button>
            <span class="loadingImg"></span>
        </div>
    </div>
{!! Form::close() !!}