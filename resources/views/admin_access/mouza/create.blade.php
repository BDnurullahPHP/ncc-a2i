{!! Form::open(array('url' => '#', 'id'=>'mouza_form', 'class'=>'form-horizontal', 'method' => 'post')) !!}
<!--=============Title for modal===============-->
    <div class="modal_top_title">{{ trans('land.mouza_title') }}</div>
<!--===========End title for modal=============-->
    
<!--========Show form post message=============-->
<div class="form-group">
    <div class="col-sm-5 col-sm-offset-3 form_messsage">

    </div>
</div>
<!--========End form post message=============-->
<div class="form-group">
    <label for="zone" class="col-sm-3 control-label">{{ trans('land.select_zone') }} <span style="color: red">*</span></label>
    <div class="col-sm-5">
        {!! Form::select('zone', $zone, array(Input::old('zone')), array('class'=>'form-control form-required', 'id'=>'zone_id', 'placeholder' => trans('common.form_select'))) !!}
    </div>
    <span class="help_tooltip" data-toggle="tooltip" data-placement="top" title="{{ trans('land.zone_bn_name_ex') }}"><i class="fa fa-question-circle" aria-hidden="true"></i></span>
</div>
<div class="form-group">
    <label for="word" class="col-sm-3 control-label">{{ trans('land.select_word') }} <span style="color: red">*</span></label>
    <div class="col-sm-5">
        {!! Form::select('WARD_ID', [], array(Input::old('word')), array('class'=>'form-control form-required', 'id'=>'word_id', 'placeholder' => trans('common.form_select'))) !!}
    </div>
        <span class="help_tooltip" data-toggle="tooltip" data-placement="top" title="{{ trans('land.ward_bn_name_ex') }}"><i class="fa fa-question-circle" aria-hidden="true"></i></span>
</div>
<div class="form-group">
    <label for="mouza_name" class="col-sm-3 control-label">{{ trans('land.mouza_name') }} </label>
    <div class="col-sm-5">
        {!! Form::text('MOUZA_NAME', Input::old('MOUZA_NAME'), array('class'=>'form-control', 'placeholder'=>trans('land.mouza_name_ex'))) !!}
    </div>
</div>
<div class="form-group">
    <label for="mouza_bn_name" class="col-sm-3 control-label">{{ trans('land.mouza_bn_name') }} <span style="color: red">*</span></label>
    <div class="col-sm-5">
        {!! Form::text('MOUZA_NAME_BN', Input::old('MOUZA_NAME_BN'), array('class'=>'form-control form-required', 'placeholder'=>trans('land.mouza_bn_name_ex'))) !!}
    </div>
</div>
<div class="hr-line-dashed"></div>
<div class="form-group">
    <label for="IS_ACTIVE" class="col-sm-3 control-label">{{ trans('common.is_active') }}</label>
    <div class="col-sm-7">
        <label class="control-label">
            {!! Form::checkbox('IS_ACTIVE', '1', true, ['class'=>'status']) !!}
        </label>
    </div>
</div>
<div class="form-group">
    <div class="col-lg-offset-3 col-lg-10">
        <span class="modal_msg pull-left"></span>
        <button type="button" class="btn btn-default btn-sm reset_form">{{ trans('common.form_reset') }}</button>
        <button type="submit" data-unique="{{ route('mouza_unique') }}" data-action="{{ route('mouza_save') }}" class="btn btn-success btn-sm form_submit">{{ trans('common.form_submit') }}</button>
        <span class="loadingImg"></span>
    </div>
</div>
{!! Form::close() !!}