@extends('admin_access.layout.master')

@section('style')

@endsection

@section('content')

<h3>{{ trans('land.mouza_list') }}
    <small></small>
    @if($access->CREATE == 1)
    <a href="#" data-toggle="modal" title="Add New Mouza" data-target="#modal_add_content" class="btn btn-primary btn-sm pull-right" data-form="{{ route('mouza_create_form') }}">{{ trans('common.add_new') }} <i class="fa fa-plus"></i></a>
    @endif
</h3>
<div class="portlet light bordered">
    <!-- <div class="portlet-title">
        <div class="caption">
            
        </div>
        <div class="tools"> </div>
    </div> -->
    <div class="portlet-body contentArea">

        <table class="table table-striped table-bordered table-hover" data-source="{{ route('mouza_data') }}" id="common_table">
            <thead>
                <tr>
                    <th>{{ trans('common.table_sl') }}</th>
                    <th>{{ trans('land.mouza_name') }}</th>
                    <th>{{ trans('land.mouza_bn_name') }}</th>
                    <th>{{ trans('land.select_word') }}</th>
                    <th>{{ trans('land.select_zone') }}</th>
                    <th>{{ trans('common.table_action') }}</th>
                </tr>
            </thead>
            <tbody>

            </tbody>
        </table>

    </div>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        $(document.body).on('change', '#zone_id', function(event) {
            var zone = $(this).val();
            $('#word_id').val('');
            if (zone != '') {
                $.ajax({
                    url: baseUrl + 'get-ward',
                    type: 'GET',
                    dataType: 'html',
                    data: {zone: zone},
                })
                        .done(function(res) {
                            $('#word_id').html(res);
                        });
            }
        });
    });
</script>
@endsection