@extends('admin_access.layout.master')

@section('css_file')

	<link href="{{ Helpers::asset('assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ Helpers::asset('assets/global/plugins/morris/morris.css') }}" rel="stylesheet" type="text/css" />

@endsection

@section('style')

    <style type="text/css">
        .dashboard-stat2{
            padding: 5px 15px 0px;
            margin-bottom: 8px;
        }
        img.chart_loader{
            
        }
        svg{
          width: 100% !important
        }
    </style>
    
@endsection

@section('js_file')

	<script src="{{ Helpers::asset('assets/global/plugins/moment.min.js') }}" type="text/javascript"></script>
    <script src="{{ Helpers::asset('assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js') }}" type="text/javascript"></script>
    <script src="{{ Helpers::asset('assets/global/plugins/morris/morris.min.js') }}" type="text/javascript"></script>
    <script src="{{ Helpers::asset('assets/global/plugins/morris/raphael-min.js') }}" type="text/javascript"></script>
    <script src="{{ Helpers::asset('assets/global/plugins/counterup/jquery.waypoints.min.js') }}" type="text/javascript"></script>
    <script src="{{ Helpers::asset('assets/global/plugins/counterup/jquery.counterup.min.js') }}" type="text/javascript"></script>
	<script src="{{ Helpers::asset('assets/global/plugins/jquery-easypiechart/jquery.easypiechart.min.js') }}" type="text/javascript"></script>
    <script type="text/javascript">
        var chart_data = "{{ route('rent_chart_data') }}";
    </script>

@endsection

@section('script')

	<script type="text/javascript">
		$(document).ready(function() {
	        var cyear = new Date().getFullYear();
            if ($('#abc').length > 0) {
                getChartdata(cyear);
            }

            $('#rent_year').change(function(event) {
                var year = $(this).val();
                $('img.chart_loader').css('display', 'block');
                $('#abc').html('');
                getChartdata(year);
            });
	    });
        function getChartdata(year){
            $.ajax({
                url: chart_data,
                type: 'GET',
                dataType: 'json',
                data: {year: year},
            })
            .done(function(res) {
                $('img.chart_loader').hide();
                new Morris.Bar({
                    element: "abc",
                    data: res.data,
                    xkey: "y",
                    ykeys: ["a", "b"],
                    labels: ["বকেয়া", "ভাড়া প্রদান"]
                });
            });
        }
	</script>

@endsection

@section('content')

	<div class="row">
        <div class="col-sm-12">
            <div class="col-sm-6">
                <h3 class="page_heading">{{ trans('admin.dashboard') }}
                    <small>{{ trans('admin.dashboard_heading') }}</small>
                </h3>
            </div>
        </div>
    </div>
    <!-- END PAGE TITLE-->
    <!-- END PAGE HEADER-->
    <!-- BEGIN DASHBOARD STATS 1-->
    <div class="row">
        <div class="col-md-12">
            <div class="note note-success">
                <h4 class="block">নোটিশ </h4>
                <p> 
                   এত দ্বারা সকল সম্মানিত গ্রাহক গনের অবগতির জন্য জানানো যাচ্ছে যে........................ 
                </p>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <a class="dashboard-stat dashboard-stat-v2 green" href="{{ route('my_services') }}" title="যেসব সেবা গ্রহণ করেছেন তার তালিকা">
                <div class="visual">
                    <i class="fa fa-bar-chart-o"></i>
                </div>
                <div class="details">
                    <div class="number">
                        <span data-counter="counterup" data-value="{{ Helpers::numberConvert($ctz_service) }}"></span> টি </div>
                    <div class="desc">সেবা গ্রহণ</div>
                </div>
            </a>
        </div>
        @if($is_tender)
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <a class="dashboard-stat dashboard-stat-v2 blue" href="{{ route('citizen_rent_history') }}" title="আপনার পরিশধিত ভাড়ার তালিকা">
                    <div class="visual">
                        <i class="fa fa-money"></i>
                    </div>
                    <div class="details">
                        <div class="number">
                            <span>{{ Helpers::numberConvert($rent_payment->TOTAL_PAID) }}</span>৳
                        </div>
                        <div class="desc">মোট পরিশোধিত ভাড়া </div>
                    </div>
                </a>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <a class="dashboard-stat dashboard-stat-v2 red" href="{{ route('citizen_rent_history') }}" title="আপনার বকেয়া ভাড়ার তালিকা">
                    <div class="visual">
                        <i class="fa fa-shopping-cart"></i>
                    </div>
                    <div class="details">
                        <div class="number">
                            <span data-counter="counterup" data-value="{{ Helpers::numberConvert($rent_payment->TOTAL_PENDING) }}">
                                {{ Helpers::numberConvert($rent_payment->TOTAL_PENDING) }}
                            </span>৳
                        </div>
                        <div class="desc">বকেয়া ভাড়া </div>
                    </div>
                </a>
            </div>
        @endif
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="dashboard-stat dashboard-stat-v2 purple">
                <div class="visual">
                    <i class="fa fa-globe"></i>
                </div>
                <div class="details">    
                    <div class="desc">
                        <h4> <b>সাম্প্রতিক প্রকাশিত</b></h4>
                    </div>
                    <a target="_blank" href="{{ route('get_tender') }}" class="desc"> 
                        <i class="fa fa-share"></i> {{ Helpers::numberConvert($current_tender) }} টি নতুন টেন্ডার
                    </a><br>
                    <a target="_blank" href="{{ route('get_lease') }}" class="desc"> 
                        <i class="fa fa-share"></i> {{ Helpers::numberConvert($current_lease) }} টি নতুন ইজারা
                    </a>
                </div>
            </div>
        </div>

    <div class="clearfix"></div>
    @if($is_tender)
        <div class="row">
            <div class="col-md-12">
                <div class="portlet light portlet-fit bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class=" icon-layers font-green-sharp"></i>
                            <span class="caption-subject font-green-sharp bold uppercase">পরিশোধিত ভাড়ার বিবরণ</span>
                        </div>
                        <div class="actions">
                            <select id="rent_year">
                            {!! $rent_years !!}
                        </select>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <img src="{{ Helpers::asset('assets/img/loader.gif') }}" alt="loader" class="chart_loader center-block">
                        <div id="abc" style="height:300px;"></div>
                    </div>
                </div>
            </div>
        </div>
    @endif
    
    <!-- END DASHBOARD STATS 1-->
    @if($is_tender)
        <div class="row">
            <div class="col-md-12">
                <div class="portlet box blue">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-gift"></i>ফ্ল্যাট / দোকান / স্পেস সমূহের এর বিবরণ
                        </div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse"> </a>
                            <a title="" data-original-title="" href="javascript:;" class="remove"> </a>
                        </div>
                    </div>
                    <div class="portlet-body">
                        @if(count($cat_property) > 0)
                            <div class="tabbable-custom nav-justified" role="tablist">
                                <ul class="nav nav-tabs nav-justified">
                                    @foreach($cat_property as $index => $cat)
                                        <li class="{{ $index == 0 ? 'active' : '' }}">
                                            <a href="#tab_1_{{ $cat->PR_CATEGORY }}" data-toggle="tab">
                                                {{ $cat->CATEGORY_NAME }}
                                            </a>
                                        </li>
                                    @endforeach
                                </ul>
                                <div class="tab-content">
                                    @foreach($cat_property as $key => $cat)
                                        <div class="tab-pane {{ $key == 0 ? 'active' : '' }}" id="tab_1_{{ $cat->PR_CATEGORY }}" role="tabpanel">
                                            <div class="table-scrollable">
                                                <table class="table table-hover">
                                                    <thead>
                                                        <tr>
                                                            <th>#</th>
                                                            <th>{{ $cat->CATEGORY_NAME }} এর  অবস্থান</th>
                                                            <th>{{ $cat->CATEGORY_NAME }} এর ধরন </th>
                                                            <th>পরিমান</th>
                                                            <th>ভাড়া (প্রতি বর্গফুট)</th>
                                                            <th>বিস্তারিত</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @foreach($cat->ITEMS as $inx => $property)
                                                            <tr>
                                                                <td>{{ Helpers::numberConvert($inx+1) }}</td>
                                                                <td>{!! $property->getProject->PROJECT_LOCATION !!}</td>
                                                                <td>{{ $property->getProjectType->TYPE_NAME }}</td>
                                                                <td>
                                                                    {{ Helpers::numberConvert($property->projectDetails->PR_MEASURMENT) }}
                                                                    @if($property->projectDetails->UOM == 1)
                                                                        বর্গফুট
                                                                    @else

                                                                    @endif
                                                                </td>
                                                                <td>{{ Helpers::numberConvert($property->tenderRentParticular->PARTICULAR_AMT) }}৳</td>
                                                                <td>
                                                                    <a href="#" data-toggle="modal" data-target="#modal_big_content" data-form="{{ route('tender_view', $property->TENDER_ID) }}" class="btn btn-default btn-xs" title="বিস্তারিত">
                                                                        <i class="fa fa-eye"></i>
                                                                    </a>
                                                                </td>
                                                            </tr>
                                                        @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                            <div class="box-footer clearfix">
                                                <a class="btn btn-sm btn-default btn-flat pull-right" href="{{ route('my_services') }}">আরো দেখুন</a>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
            </div> 
        </div>
    @endif

@endsection