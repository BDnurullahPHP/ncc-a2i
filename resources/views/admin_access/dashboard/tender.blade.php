@extends('admin_access.layout.master')

@section('css_file')

	<link href="{{ Helpers::asset('assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ Helpers::asset('assets/global/plugins/morris/morris.css') }}" rel="stylesheet" type="text/css" />

@endsection

@section('style')

    <style type="text/css">
        .dashboard-stat2{
            padding: 5px 15px 0px;
            margin-bottom: 8px;
        }
        .ui-datepicker-calendar {
            display: none;
        }
    </style>

@endsection

@section('js_file')

	<script src="{{ Helpers::asset('assets/global/plugins/moment.min.js') }}" type="text/javascript"></script>
    <script src="{{ Helpers::asset('assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js') }}" type="text/javascript"></script>
    <script src="{{ Helpers::asset('assets/global/plugins/morris/morris.min.js') }}" type="text/javascript"></script>
    <script src="{{ Helpers::asset('assets/global/plugins/morris/raphael-min.js') }}" type="text/javascript"></script>
    <script src="{{ Helpers::asset('assets/global/plugins/counterup/jquery.waypoints.min.js') }}" type="text/javascript"></script>
    <script src="{{ Helpers::asset('assets/global/plugins/counterup/jquery.counterup.min.js') }}" type="text/javascript"></script>
	<script src="{{ Helpers::asset('assets/global/plugins/jquery-easypiechart/jquery.easypiechart.min.js') }}" type="text/javascript"></script>
    <script type="text/javascript">
        var applicant_data = "{{ route('tender_application_data') }}";
    </script>
	<script src="{{ Helpers::asset('assets/pages/scripts/dashboard.min.js') }}" type="text/javascript"></script>

@endsection

@section('script')

	<script type="text/javascript">
		$(document).ready(function() {
	        Dashboard.init();

            $(function() {
                $('#date_month').datepicker({
                    changeMonth: true,
                    changeYear: true,
                    showButtonPanel: true,
                    dateFormat: 'MM yy',
                    onClose: function(dateText, inst) { 
                        $(this).datepicker('setDate', new Date(inst.selectedYear, inst.selectedMonth, 1));
                    }
                }).datepicker("setDate", new Date());
            });

            var cmonth = moment().format('MMMM YYYY');
            getApplicantData(cmonth);

            $('#date_month').blur(function(event) {
                var month = $(this).val();
                $('img.chart_loader').css('display', 'block');
                $('#tender_app').html('');
                getApplicantData(month);
                $marr = month.split(" ");
                $('span.applicnt_month').text($marr[0]);
                $('span.applicnt_year').text($marr[1]);
            });
	    });

        function getApplicantData(month){
            $.ajax({
                url: applicant_data,
                type: 'GET',
                dataType: 'json',
                data: {month: month, is_lease: 0},
            })
            .done(function(res) {
                $('img.chart_loader').hide();
                new Morris.Bar({
                    element: "tender_app",
                    data: res.data,
                    xkey: "y",
                    ykeys: ["a"],
                    labels: ["আবেদনকারীর সংখ্যা"]
                });
            });
        }
	</script>

@endsection

@section('content')

	<div class="row">
        <div class="col-sm-12">
            <div class="col-sm-6">
                <h3 class="page_heading">{{ trans('admin.dashboard') }}
<!--                    <small>{{ trans('admin.dashboard_heading') }}</small>-->
                </h3>
            </div>
        </div>
    </div>
    <!-- END PAGE TITLE-->
    <!-- END PAGE HEADER-->
    <!-- BEGIN DASHBOARD STATS 1-->
    <div class="row">
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <a href="{{ route('tender_list') }}" class="dashboard-stat dashboard-stat-v2 blue">
                <div class="visual">
                    <i class="icon-layers"></i>
                </div>
                <div class="details">
                    <div class="number">
                        <span data-counter="counterup" data-value="{{ Helpers::numberConvert($tender) }}">
                            {{ Helpers::numberConvert($tender) }}
                        </span>
                    </div>
                    <div class="desc">{{ trans('tender.total_tender') }}</div>
                </div>
            </a>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <a href="{{ route('tender_list') }}" class="dashboard-stat dashboard-stat-v2 green">
                <div class="visual">
                    <i class="icon-layers"></i>
                </div>
                <div class="details">
                    <div class="number">
                        <span data-counter="counterup" data-value="{{ Helpers::numberConvert($current_tender) }}">
                            {{ Helpers::numberConvert($current_tender) }}
                        </span>
                    </div>
                    <div class="desc">{{ trans('tender.current_tender') }}</div>
                </div>
            </a>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <a href="{{ route('tender_applicant_list') }}" class="dashboard-stat dashboard-stat-v2 red">
                <div class="visual">
                    <i class="fa fa-users"></i>
                </div>
                <div class="details">
                    <div class="number">
                        <span data-counter="counterup" data-value="{{ Helpers::numberConvert($applicant + $citizen) }}">
                            {{ Helpers::numberConvert($applicant + $citizen) }}
                        </span>
                    </div>
                    <div class="desc">{{trans('admin.The_initially_selected_applicant')}}</div>
                </div>
            </a>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <a href="{{ route('tender_applicant_list') }}" class="dashboard-stat dashboard-stat-v2 purple">
                <div class="visual">
                    <i class="fa fa-users"></i>
                </div>
                <div class="details">
                    <div class="number">
                        <span data-counter="counterup" data-value="{{ Helpers::numberConvert($applicant_final + $citizen_final) }}">
                            {{ Helpers::numberConvert($applicant_final + $citizen_final) }}
                        </span>
                    </div>
                    <div class="desc">{{trans('admin.Finally_selected_applicant')}}</div>
                </div>
            </a>
        </div>
    </div>

<!--    <div class="clearfix"></div>
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light portlet-fit bordered">
                <div class="portlet-title">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="caption">
                                <i class=" icon-layers font-green-sharp"></i>
                                <span class="caption-subject font-green-sharp bold uppercase"><span class="applicnt_year">{{ Helpers::numberConvert(date('Y')) }}</span> সালের <span class="applicnt_month">চলতি</span> মাসের আবেদনকারীর বিবরণ</span>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <input type="text" class="pull-right" id="date_month">
                        </div>
                    </div>
                </div>
                <div class="portlet-body">
                    <img src="{{ Helpers::asset('assets/img/loader.gif') }}" alt="loader" class="chart_loader center-block">
                    <div id="tender_app" style="height:300px;"></div>
                </div>
            </div>
        </div>
    </div>-->

    <div class="row">
        <div class="col-md-12">
            <div class="portlet box purple-soft">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-th"></i>{{trans('admin.Details_of_ongoing_tender')}}
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"> </a>
                        <a title="" data-original-title="" href="javascript:;" class="remove"> </a>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="tabbable-custom nav-justified">
                        <div class="table-scrollable">
                            <table class="table table-hover table-bordered">
                                <thead>
                                    <tr>
                                        <th>{{trans('admin.gradual')}}</th>
                                        <th>{{trans('admin.Market_name')}}</th>
                                        <th>{{trans('admin.Flat_shop_Space_no')}}</th>
                                        <th>{{trans('admin.Measure_square_feet')}}</th>
                                        <th>{{trans('admin.Minimum_salami_Amount')}}</th>
                                        <th>{{trans('admin.Every_square_foot_Monthly_rent')}}</th>
                                        <th>{{trans('admin.Schedule_Price')}}</th>
                                        <th>{{trans('admin.The_amount_of_security')}}</th>
                                        <th>{{trans('admin.Application_deadline')}}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($schedule_info as $key=>$value)
                                    <tr>
                                        <td>{{ Helpers::numberConvert($key+1) }}</td>
                                        <td>{{ $value->getProject->PR_NAME_BN }}</td>
                                        <td>{{ $value->getCategory->CATE_NAME }} - {{ Helpers::numberConvert($value->getProjectDetail->PR_SSF_NO) }}</td>
                                        <td>{{ Helpers::numberConvert($value->getProjectDetail->PR_MEASURMENT) }} বর্গফুট</td>
                                        <td>{{ Helpers::numberConvert(number_format($value->LOWEST_TENDER_MONEY,2)) }}</td>
                                        @foreach($value->getTenderParticulars as $index=>$particular)
                                        <td>{{ $particular->UOM == 1 ? Helpers::numberConvert($particular->PARTICULAR_AMT).' %' : Helpers::numberConvert(number_format($particular->PARTICULAR_AMT,2)).' ৳' }}</td>
                                        @endforeach
                                        <td>{{ Helpers::numberConvert(date('d/m/Y', strtotime($value->LAST_SELLING_DT_TO)))}}</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="box-footer clearfix">
                            <a class="btn btn-sm btn-default btn-flat pull-right" href="{{ route('tender_list') }}">{{trans('admin.View_more')}}</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection