@extends('admin_access.layout.master')

@section('css_file')

	<link href="{{ Helpers::asset('assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ Helpers::asset('assets/global/plugins/morris/morris.css') }}" rel="stylesheet" type="text/css" />

@endsection

@section('style')
    
    <style type="text/css">
        .dashboard-stat2{
            padding: 5px 15px 0px;
            margin-bottom: 8px;
        }
        .ui-datepicker-calendar {
            display: none;
        }
    </style>

@endsection

@section('js_file')

	<script src="{{ Helpers::asset('assets/global/plugins/moment.min.js') }}" type="text/javascript"></script>
    <script src="{{ Helpers::asset('assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js') }}" type="text/javascript"></script>
    <script src="{{ Helpers::asset('assets/global/plugins/morris/morris.min.js') }}" type="text/javascript"></script>
    <script src="{{ Helpers::asset('assets/global/plugins/morris/raphael-min.js') }}" type="text/javascript"></script>
    <script src="{{ Helpers::asset('assets/global/plugins/counterup/jquery.waypoints.min.js') }}" type="text/javascript"></script>
    <script src="{{ Helpers::asset('assets/global/plugins/counterup/jquery.counterup.min.js') }}" type="text/javascript"></script>
	<script src="{{ Helpers::asset('assets/global/plugins/jquery-easypiechart/jquery.easypiechart.min.js') }}" type="text/javascript"></script>
    <script type="text/javascript">
        var applicant_data = "{{ route('tender_application_data') }}";
    </script>
	<script src="{{ Helpers::asset('assets/pages/scripts/dashboard.min.js') }}" type="text/javascript"></script>

@endsection

@section('script')

	<script type="text/javascript">
		$(document).ready(function() {
            Dashboard.init();

            $(function() {
                $('#date_month').datepicker({
                    changeMonth: true,
                    changeYear: true,
                    showButtonPanel: true,
                    dateFormat: 'MM yy',
                    onClose: function(dateText, inst) { 
                        $(this).datepicker('setDate', new Date(inst.selectedYear, inst.selectedMonth, 1));
                    }
                }).datepicker("setDate", new Date());
            });

            var cmonth = moment().format('MMMM YYYY');
            getApplicantData(cmonth);

            $('#date_month').blur(function(event) {
                var month = $(this).val();
                $('img.chart_loader').css('display', 'block');
                $('#tender_app').html('');
                getApplicantData(month);
                $marr = month.split(" ");
                $('span.applicnt_month').text($marr[0]);
                $('span.applicnt_year').text($marr[1]);
            });
        });

        function getApplicantData(month){
            $.ajax({
                url: applicant_data,
                type: 'GET',
                dataType: 'json',
                data: {month: month, is_lease: 1},
            })
            .done(function(res) {
                $('img.chart_loader').hide();
                new Morris.Bar({
                    element: "tender_app",
                    data: res.data,
                    xkey: "y",
                    ykeys: ["a"],
                    labels: ["আবেদনকারীর সংখ্যা"]
                });
            });
        }
	</script>

@endsection

@section('content')

	<div class="row">
        <div class="col-sm-12">
            <div class="col-sm-6">
                <h3 class="page_heading">{{ trans('admin.dashboard') }}
                    <small>{{ trans('admin.dashboard_heading') }}</small>
                </h3>
            </div>
        </div>
    </div>
    <!-- END PAGE TITLE-->
    <!-- END PAGE HEADER-->
    <!-- BEGIN DASHBOARD STATS 1-->
    <div class="row">
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <a href="{{ route('lease_notice') }}" class="dashboard-stat dashboard-stat-v2 blue">
                <div class="visual">
                    <i class="icon-layers"></i>
                </div>
                <div class="details">
                    <div class="number">
                        <span data-counter="counterup" data-value="{{ $lease }}">{{ $lease }}</span>
                    </div>
                    <div class="desc">{{trans('admin.Total_Tender')}}</div>
                </div>
            </a>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <a href="{{ route('lease_notice') }}" class="dashboard-stat dashboard-stat-v2 green">
                <div class="visual">
                    <i class="icon-layers"></i>
                </div>
                <div class="details">
                    <div class="number">
                        <span data-counter="counterup" data-value="{{ $current_lease }}">{{ $current_lease }}</span></div>
                    <div class="desc">{{trans('admin.Running_tender')}}</div>
                </div>
            </a>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <a href="{{ route('lease_primary_selected_applicant_list') }}" class="dashboard-stat dashboard-stat-v2 red">
                <div class="visual">
                    <i class="fa fa-users"></i>
                </div>
                <div class="details">
                    <div class="number">
                        <span data-counter="counterup" data-value="{{ $applicant + $citizen }}">{{ $applicant + $citizen }}</span>
                    </div>
                    <div class="desc">{{trans('admin.The_initially_selected_applicant')}}</div>
                </div>
            </a>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <a href="{{ route('lease_final_selected_applicant_list') }}" class="dashboard-stat dashboard-stat-v2 purple">
                <div class="visual">
                    <i class="fa fa-usd"></i>
                </div>
                <div class="details">
                    <div class="number">
                        <span data-counter="counterup" data-value="{{ $applicant_final + $citizen_final }}">{{ $applicant_final + $citizen_final }}</span>
                    </div>
                    <div class="desc">{{trans('admin.Finally_selected_applicant')}}</div>
                </div>
            </a>
        </div>
    </div>

    <div class="clearfix"></div>
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light portlet-fit bordered">
                <div class="portlet-title">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="caption">
                                <i class=" icon-layers font-green-sharp"></i>
                                <span class="caption-subject font-green-sharp bold uppercase"><span class="applicnt_year">{{ Helpers::numberConvert(date('Y')) }}</span> {{trans('admin.Of_the_year')}} <span class="applicnt_month">{{trans('admin.Current')}}</span> {{trans('admin.Month_applicant_details')}}</span>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <input type="text" class="pull-right" id="date_month">
                        </div>
                    </div>
                </div>
                <div class="portlet-body">
                    <img src="{{ Helpers::asset('assets/img/loader.gif') }}" alt="loader" class="chart_loader center-block">
                    <div id="tender_app" style="height:300px;"></div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="portlet box purple-soft">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-th"></i>{{trans('admin.Description_of_leases')}}
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"> </a>
                        <a title="" data-original-title="" href="javascript:;" class="remove"> </a>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="tabbable-custom nav-justified">
                        <div class="table-scrollable">
                            <table class="table table-hover table-bordered">
                                <thead>
                                    <tr>
                                        <th>{{trans('admin.gradual')}}</th>
                                        <th>{{trans('admin.Mahal_name')}}</th>
                                        <th>{{trans('admin.Mahal_address')}}</th>
                                        <th>{{trans('admin.Security')}}</th>
                                        <th>{{trans('admin.Earnest_money')}}</th>
                                        <th>{{trans('admin.Tender_price')}}</th>
                                        <th>{{trans('admin.VAT')}}</th>
                                        <th>{{trans('admin.Income_tax')}}</th>
<!--                                        <th>কার্যকলাপ</th>-->
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($schedule_info as $key=>$value)
                                    <tr>
                                        <td>{{ $key+1 }}</td>
                                        <td>{{ $value->getProject->PR_NAME_BN }}</td>
                                        <td>{{ $value->getCategory->CATE_NAME }}</td>
                                        @foreach($value->getTenderParticulars as $index=>$particular)
                                        <td>{{ $particular->UOM == 1 ? Helpers::numberConvert($particular->PARTICULAR_AMT).' %' : Helpers::numberConvert(number_format($particular->PARTICULAR_AMT,2)).' ৳' }}</td>
                                        @endforeach
<!--                                        <td></td>-->
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="box-footer clearfix">
                            <a class="btn btn-sm btn-default btn-flat pull-right" href="{{ route('lease_notice') }}">{{trans('admin.View_more')}}</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection