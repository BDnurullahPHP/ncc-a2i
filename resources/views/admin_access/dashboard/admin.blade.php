@extends('admin_access.layout.master')

@section('css_file')

<link href="{{ Helpers::asset('assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ Helpers::asset('assets/global/plugins/morris/morris.css') }}" rel="stylesheet" type="text/css" />

@endsection

@section('style')

    <style type="text/css">
        .dashboard-stat2{
            padding: 5px 15px 0px;
            margin-bottom: 8px;
        }
        img.chart_loader{
            
        }
        svg{
          width: 100% !important
        }
        #chat_out{
            display: none !important;
            opacity: 0;
        }
    </style>

@endsection

@section('js_file')

<script src="{{ Helpers::asset('assets/global/plugins/moment.min.js') }}" type="text/javascript"></script>
<script src="{{ Helpers::asset('assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js') }}" type="text/javascript"></script>
<script src="{{ Helpers::asset('assets/global/plugins/morris/morris.min.js') }}" type="text/javascript"></script>
<script src="{{ Helpers::asset('assets/global/plugins/morris/raphael-min.js') }}" type="text/javascript"></script>
<script src="{{ Helpers::asset('assets/global/plugins/counterup/jquery.waypoints.min.js') }}" type="text/javascript"></script>
<script src="{{ Helpers::asset('assets/global/plugins/counterup/jquery.counterup.min.js') }}" type="text/javascript"></script>
<script src="{{ Helpers::asset('assets/global/plugins/jquery-easypiechart/jquery.easypiechart.min.js') }}" type="text/javascript"></script>
<!--<script src="https://cdn.socket.io/socket.io-1.3.4.js"></script>-->

<script type="text/javascript">
    var chart_data = "{{ route('get_chart_data') }}";
</script>
<script src="{{ Helpers::asset('assets/pages/scripts/dashboard.min.js') }}" type="text/javascript"></script>

@endsection

@section('script')

<script type="text/javascript">

    $(document).ready(function() {
        Dashboard.init();
        var cyear = new Date().getFullYear();
        getChartdata(cyear);

        $('#rev_year').change(function(event) {
            var year = $(this).val();
            $('img.chart_loader').css('display', 'block');
            $('#abc').html('');
            getChartdata(year);
        });

        $('.enter-chat-msg').keypress(function(event) {
            var key = event.which;
            var data = $(this).val();
            if (key == 13 && data != '') {
                appareMessage(data);
                $(this).val('');
            }
        });

        $('a.post-chat-link').click(function(event) {
            event.preventDefault();
            var input = $(this).closest('.chat-form').find('input.enter-chat-msg')
            var msg = input.val();
            if (msg != '') {
                appareMessage(msg);
                input.val('');
            }
        });

    });

    // Start socket***************************************
    // var socket = io.connect('http://localhost:8890');
    // socket.on('message', function (data) {
    //     $( ".message").find('span.body').append(data);
    // });

    /*var socket = io('http://localhost:3000');
    socket.on('globalNotificationChannel:WroteOnWall', function(data) {
        // (Notification Received)
        //console.log(data.message)
    }.bind(this));
    socket.on('chat message', function(msg){
        console.log(msg);
    });*/
    // End socket*****************************************

    function getChartdata(year){
        $.ajax({
            url: chart_data,
            type: 'GET',
            dataType: 'json',
            data: {year: year},
        })
        .done(function(res) {
            $('img.chart_loader').hide();
            new Morris.Bar({
                element: "abc",
                data: res.data,
                xkey: "y",
                ykeys: ["a", "b"],
                labels: ["প্রত্যাশিত রাজস্ব", "রাজস্ব আদায়"]
            });
        });
    }

    function appareMessage(data){
        var ctime = currentTime();
        $('#chat_out').find('.datetime span').text(ctime);
        $('#chat_out').find('.message .body').text(data);
        var ghtml = $('#chat_out').html();
        $('ul.chats').append('<li class="out">'+ghtml+'</li>');
        console.log(data);
    }

</script>

@endsection

@section('content')

<div class="row">
    <div class="col-sm-12">
        <h3 class="page_heading">{{ trans('admin.dashboard') }}
            <!--<small>{{ trans('admin.dashboard_heading') }}</small>-->
        </h3>
    </div>
</div>
<!-- END PAGE TITLE-->
<!-- END PAGE HEADER-->
<!-- BEGIN DASHBOARD STATS 1-->
<div class="row">
    <div class="col-lg-4 col-md-3 col-sm-6 col-xs-12">
        <a href="{{ route('revenue_list') }}" class="dashboard-stat dashboard-stat-v2 blue">
            <div class="visual">
                <i class="fa fa-money"></i>
            </div>
            <div class="details">
                <div class="number">
                    <span>{{ Helpers::numberConvert(number_format($revenue_cr, 2)) }}</span>৳
                </div>
                <div class="desc">{{ trans('admin.total_expeted_revenue') }}</div>
            </div>
        </a>
    </div>
    <div class="col-lg-4 col-md-3 col-sm-6 col-xs-12">
        <a href="{{ route('revenue_list') }}" class="dashboard-stat dashboard-stat-v2 green">
            <div class="visual">
                <i class="fa fa-bar-chart-o"></i>
            </div>
            <div class="details">
                <div class="number">
                    <span data-counter="counterup" data-value="{{ Helpers::numberConvert(number_format($revenue_dr, 2)) }}">0</span>৳</div>
                <div class="desc">{{ trans('admin.total_collect_revenue') }}</div>
            </div>
        </a>
    </div>
    <div class="col-lg-4 col-md-3 col-sm-6 col-xs-12">
        <a href="{{ route('revenue_list') }}" class="dashboard-stat dashboard-stat-v2 red">
            <div class="visual">
                <i class="fa fa-shopping-cart"></i>
            </div>
            <div class="details">
                <div class="number">
                    <span data-counter="counterup" data-value="{{ Helpers::numberConvert(number_format($revenue_cr - $revenue_dr, 2)) }}">0</span>৳
                </div>
                <div class="desc">{{ trans('admin.total_due_revenue') }}</div>
            </div>
        </a>
    </div>
</div>
<div class="clearfix"></div>
<div class="row">
    <div class="col-md-12">
        <div class="portlet light portlet-fit bordered">
            <div class="portlet-title">
                <div class="caption">
                    <i class=" icon-layers font-green-sharp"></i>
                    <span class="caption-subject font-green-sharp bold uppercase">
                    {{trans('admin.Comparative_description_of_the_expected_and_realized_revenue')}}</span>
                </div>
                <div class="actions">
                    <select id="rev_year">
                        {!! $rev_years !!}
                    </select>
                </div>
            </div>
            <div class="portlet-body">
                <img src="{{ Helpers::asset('assets/img/loader.gif') }}" alt="loader" class="chart_loader center-block">
                <div id="abc" style="height:300px;"></div>
            </div>
        </div>
    </div>
</div>
<!-- END DASHBOARD STATS 1-->
<div class="row">
    <!--    ফ্ল্যাট   ==============================-->
    <div class="col-md-6">
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-gift"></i>{{trans('admin.details_about_the_flats')}}
                </div>
                <div class="tools">
                    <a href="javascript:;" class="collapse"> </a>
                    <a title="" data-original-title="" href="javascript:;" class="remove"> </a>
                </div>
            </div>
            <div class="portlet-body">
                <div class="row">
                    <div class="col-lg-4 col-md-3 col-sm-6 col-xs-12">
                        <div class="dashboard-stat2 ">
                            <div class="display">
                                <div class="number">
                                    <h3 class="font-green-sharp">
                                        <span data-value="{{ count($ready_flat) }}" data-counter="counterup"><i class="icon-pie-chart"></i>{{ count($ready_flat) }}</span>
                                        <small class="font-green-sharp">{{trans('admin.tt')}}</small>
                                    </h3>
									<small><a class="title" href="{{ route('all_projects') }}">{{trans('admin.ready_flat')}} <i class="icon-arrow-right"></i></a></small>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-3 col-sm-6 col-xs-12">
                        <div class="dashboard-stat2 ">
                            <div class="display">
                                <div class="number">
                                    <h3 class="font-red-haze">
                                        <span data-value="{{ count($construction_flat) }}" data-counter="counterup">{{ count($construction_flat) }}</span>
                                        <small class="font-green-sharp">{{trans('admin.tt')}}</small>
                                    </h3>
									<small><a class="title" href="{{ route('all_projects') }}">{{trans('admin.under_construction')}} <i class="icon-arrow-right"></i></a></small>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-3 col-sm-6 col-xs-12">
                        <div class="dashboard-stat2 ">
                            <div class="display">
                                <div class="number">
                                    <h3 class="font-blue-sharp">
                                        <span data-value="{{ count($allotment_flat) }}" data-counter="counterup">{{ count($allotment_flat) }}</span>
                                        <small class="font-green-sharp">{{trans('admin.tt')}}</small>
                                    </h3>
									<small><a class="title" href="{{ route('all_projects') }}">{{trans('admin.allotment')}}<i class="icon-arrow-right"></i></a></small>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--<div class="tabbable-custom nav-justified">
                    <ul class="nav nav-tabs nav-justified">
                        <li class="active">
                            <a href="ui_tabs_accordions_navs.html#tab_1_1_1" data-toggle="tab">রেডি ফ্ল্যাট</a>
                        </li>
                        <li>
                            <a href="ui_tabs_accordions_navs.html#tab_1_1_2" data-toggle="tab">নির্মানাধীন ফ্ল্যাট</a>
                        </li>
                        <li>
                            <a href="ui_tabs_accordions_navs.html#tab_1_1_3" data-toggle="tab">বরাদ্দ</a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab_1_1_1">
                            <div class="table-scrollable">
                                <table class="table table-hover">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>ফ্ল্যাটের ঠিকানা</th>
                                            <th>ধরন</th>
                                            <th>নাম্বার</th>
                                            <th>পরিমান</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($ready_flat as $key=>$value)
                                        <tr>
                                            <td>{{ $key+1 }}</td>
                                            <td>{{ $value->getProject->PR_NAME_BN }}</td>
                                            <td>{{ $value->getType->TYPE_NAME }}</td>
                                            <td>{{ $value->PR_SSF_NO }}</td>
                                            <td>{{ $value->PR_MEASURMENT }} বর্গফুট</td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <div class="box-footer clearfix">
                                <a class="btn btn-sm btn-default btn-flat pull-right" href="{{ route('all_projects') }}">আরো দেখুন</a>
                            </div>
                        </div>

                        <div class="tab-pane" id="tab_1_1_2">
                            <div class="tab-pane active" id="tab_1_1_1">
                                <div class="table-scrollable">
                                    <table class="table table-hover">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>ফ্ল্যাটের ঠিকানা</th>
                                                <th>ধরন</th>
                                                <th>নাম্বার</th>
                                                <th>পরিমান</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($construction_flat as $key=>$value)
                                            <tr>
                                                <td>{{ $key+1 }}</td>
                                                <td>{{ $value->getProject->PR_NAME_BN }}</td>
                                                <td>{{ $value->getType->TYPE_NAME }}</td>
                                                <td>{{ $value->PR_SSF_NO }}</td>
                                                <td>{{ $value->PR_MEASURMENT }} বর্গফুট</td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="box-footer clearfix">
                                <a class="btn btn-sm btn-default btn-flat pull-right" href="{{ route('all_projects') }}">আরো দেখুন</a>
                            </div>
                        </div>

                        <div class="tab-pane" id="tab_1_1_3">
                            <div class="tab-pane active" id="tab_1_1_1">
                                <div class="table-scrollable">
                                    <table class="table table-hover">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>গ্রাহক</th>
                                                <th>ফ্ল্যাটের ঠিকানা</th>
                                                <th>ধরন</th>
                                                <th>নাম্বার</th>
                                                <th>পরিমান</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($allotment_flat as $key=>$value)
                                            <tr>
                                                <td>{{ $key+1 }}</td>
                                                <td>
                                                    @if(!empty($value->getCitizen))
                                                        {{ $value->getCitizen->getTenderCitizen->FULL_NAME }}
                                                    @endif
                                                </td>
                                                <td>{{ $value->getProject->PR_NAME_BN }}</td>
                                                <td>{{ $value->projectDetails->getType->TYPE_NAME }}</td>
                                                <td>{{ $value->projectDetails->PR_SSF_NO }}</td>
                                                <td>{{ $value->projectDetails->PR_MEASURMENT }} বর্গফুট</td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="box-footer clearfix">
                                <a class="btn btn-sm btn-default btn-flat pull-right" href="{{ route('all_projects') }}">আরো দেখুন</a>
                            </div>
                        </div>
                    </div>
                </div>-->
            </div>
        </div>
    </div>

    <!--    দোকান   ==============================-->
    <div class="col-md-6">
        <div class="portlet box green">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-gift"></i>{{trans('admin.Store_details')}}
                </div>
                <div class="tools">
                    <a href="javascript:;" class="collapse"> </a>
                    <a title="" data-original-title="" href="javascript:;" class="remove"> </a>
                </div>
            </div>
            <div class="portlet-body">
                <div class="row">
                    <div class="col-lg-4 col-md-3 col-sm-6 col-xs-12">
                        <div class="dashboard-stat2 ">
                            <div class="display">
                                <div class="number">
                                    <h3 class="font-green-sharp">
                                        <span data-value="{{ count($ready_shop) }}" data-counter="counterup"><i class="icon-pie-chart"></i>{{ count($ready_shop) }}</span>
                                        <small class="font-green-sharp">{{trans('admin.tt')}}</small>
                                    </h3>
									<small><a class="title" href="{{ route('all_projects') }}">{{trans('admin.Ready_Store')}} <i class="icon-arrow-right"></i></a></small>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-3 col-sm-6 col-xs-12">
                        <div class="dashboard-stat2 ">
                            <div class="display">
                                <div class="number">
                                    <h3 class="font-red-haze">
                                        <span data-value="{{ count($construction_shop) }}" data-counter="counterup">{{ count($construction_shop) }}</span>
                                        <small class="font-green-sharp">{{trans('admin.tt')}}</small>
                                    </h3>
									<small><a class="title" href="{{ route('all_projects') }}">{{trans('admin.under_construction')}} <i class="icon-arrow-right"></i></a></small>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-3 col-sm-6 col-xs-12">
                        <div class="dashboard-stat2 ">
                            <div class="display">
                                <div class="number">
                                    <h3 class="font-blue-sharp">
                                        <span data-value="{{ count($allotment_shop) }}" data-counter="counterup">{{ count($allotment_shop) }}</span>
                                        <small class="font-green-sharp">{{trans('admin.tt')}}</small>
                                    </h3>
									<small><a class="title" href="{{ route('all_projects') }}">{{trans('admin.Store_allocation')}} <i class="icon-arrow-right"></i></a></small>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--<div class="tabbable-custom nav-justified">
                    <ul class="nav nav-tabs nav-justified">
                        <li class="active">
                            <a href="ui_tabs_accordions_navs.html#tab_1_1_4" data-toggle="tab">রেডি দোকান</a>
                        </li>
                        <li>
                            <a href="ui_tabs_accordions_navs.html#tab_1_1_5" data-toggle="tab">নির্মানাধীন দোকান</a>
                        </li>
                        <li>
                            <a href="ui_tabs_accordions_navs.html#tab_1_1_6" data-toggle="tab">দোকান বরাদ্দ</a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab_1_1_4">
                            <div class="table-scrollable">
                                <table class="table table-hover">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>মার্কেটের নাম</th>
                                            <th>ধরন</th>
                                            <th>নাম্বার</th>
                                            <th>পরিমান</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($ready_shop as $key=>$value)
                                        <tr>
                                            <td>{{ $key+1 }}</td>
                                            <td>{{ $value->getProject->PR_NAME_BN }}</td>
                                            <td>{{ $value->getType->TYPE_NAME }}</td>
                                            <td>{{ $value->PR_SSF_NO }}</td>
                                            <td>{{ $value->PR_MEASURMENT }} বর্গফুট</td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <div class="box-footer clearfix">
                                <a class="btn btn-sm btn-default btn-flat pull-right" href="{{ route('all_projects') }}">আরো দেখুন</a>
                            </div>
                        </div>

                        <div class="tab-pane" id="tab_1_1_5">
                            <div class="tab-pane active" id="tab_1_1_4">
                                <div class="table-scrollable">
                                    <table class="table table-hover">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>মার্কেটের নাম</th>
                                                <th>ধরন</th>
                                                <th>নাম্বার</th>
                                                <th>পরিমান</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($construction_shop as $key=>$value)
                                            <tr>
                                                <td>{{ $key+1 }}</td>
                                                <td>{{ $value->getProject->PR_NAME_BN }}</td>
                                                <td>{{ $value->getType->TYPE_NAME }}</td>
                                                <td>{{ $value->PR_SSF_NO }}</td>
                                                <td>{{ $value->PR_MEASURMENT }} বর্গফুট</td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="box-footer clearfix">
                                <a class="btn btn-sm btn-default btn-flat pull-right" href="{{ route('all_projects') }}">আরো দেখুন</a>
                            </div>
                        </div>

                        <div class="tab-pane" id="tab_1_1_6">
                            <div class="tab-pane active" id="tab_1_1_4">
                                <div class="table-scrollable">
                                    <table class="table table-hover">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>গ্রাহক</th>
                                                <th>মার্কেটের নাম</th>
                                                <th>ধরন</th>
                                                <th>নাম্বার</th>
                                                <th>পরিমান</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($allotment_shop as $key=>$value)
                                            <tr>
                                                <td>{{ $key+1 }}</td>
                                                <td>
                                                    @if(!empty($value->getCitizen))
                                                        {{ $value->getCitizen->getTenderCitizen->FULL_NAME }}
                                                    @endif
                                                </td>
                                                <td>{{ $value->getProject->PR_NAME_BN }}</td>
                                                <td>{{ $value->projectDetails->getType->TYPE_NAME }}</td>
                                                <td>{{ $value->projectDetails->PR_SSF_NO }}</td>
                                                <td>{{ $value->projectDetails->PR_MEASURMENT }} বর্গফুট</td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="box-footer clearfix">
                                <a class="btn btn-sm btn-default btn-flat pull-right" href="{{ route('all_projects') }}">আরো দেখুন</a>
                            </div>
                        </div>
                    </div>
                </div>-->
            </div>
        </div>
    </div>
</div>
<!-- স্পেস ===============-->
<div class="row">
    <div class="col-md-6">
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-gift"></i>{{trans('admin.details_of_the_spaces')}}
                </div>
                <div class="tools">
                    <a href="javascript:;" class="collapse"> </a>
                    <a title="" data-original-title="" href="javascript:;" class="remove"> </a>
                </div>
            </div>
            <div class="portlet-body">
                <div class="row">
                    <div class="col-lg-4 col-md-3 col-sm-6 col-xs-12">
                        <div class="dashboard-stat2 ">
                            <div class="display">
                                <div class="number">
                                    <h3 class="font-green-sharp">
                                        <span data-value="{{ count($ready_space) }}" data-counter="counterup"><i class="icon-pie-chart"></i>{{ count($ready_space) }}</span>
                                        <small class="font-green-sharp">{{trans('admin.tt')}}</small>
                                    </h3>
									<small><a class="title" href="{{ route('all_projects') }}">{{trans('admin.ready_space')}} <i class="icon-arrow-right"></i></a></small>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-3 col-sm-6 col-xs-12">
                        <div class="dashboard-stat2 ">
                            <div class="display">
                                <div class="number">
                                    <h3 class="font-red-haze">
                                        <span data-value="{{ count($construction_space) }}" data-counter="counterup">{{ count($construction_space) }}</span>
                                        <small class="font-green-sharp">{{trans('admin.tt')}}</small>
                                    </h3>
									<small><a class="title" href="{{ route('all_projects') }}">{{trans('admin.under_construction')}}<i class="icon-arrow-right"></i></a></small>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-3 col-sm-6 col-xs-12">
                        <div class="dashboard-stat2 ">
                            <div class="display">
                                <div class="number">
                                    <h3 class="font-blue-sharp">
                                        <span data-value="{{ count($allotment_space) }}" data-counter="counterup">{{ count($allotment_space) }}</span>
                                        <small class="font-green-sharp">{{trans('admin.tt')}}</small>
                                    </h3>
									<small><a class="title" href="{{ route('all_projects') }}">{{trans('admin.Space_allocation')}} <i class="icon-arrow-right"></i></a></small>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--<div class="tabbable-custom nav-justified">
                    <ul class="nav nav-tabs nav-justified">
                        <li class="active">
                            <a href="ui_tabs_accordions_navs.html#tab_1_1_7" data-toggle="tab">রেডি স্পেস</a>
                        </li>
                        <li>
                            <a href="ui_tabs_accordions_navs.html#tab_1_1_8" data-toggle="tab">নির্মানাধীন স্পেস</a>
                        </li>
                        <li>
                            <a href="ui_tabs_accordions_navs.html#tab_1_1_9" data-toggle="tab">স্পেস বরাদ্দ</a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab_1_1_7">
                            <div class="table-scrollable">
                                <table class="table table-hover">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>স্পেসের ঠিকানা</th>
                                            <th>ধরন</th>
                                            <th>নাম্বার</th>
                                            <th>পরিমান</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($ready_space as $key=>$value)
                                        <tr>
                                            <td>{{ $key+1 }}</td>
                                            <td>{{ $value->getProject->PR_NAME_BN }}</td>
                                            <td>{{ $value->getType->TYPE_NAME }}</td>
                                            <td>{{ $value->PR_SSF_NO }}</td>
                                            <td>{{ $value->PR_MEASURMENT }} বর্গফুট</td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <div class="box-footer clearfix">
                                <a class="btn btn-sm btn-default btn-flat pull-right" href="{{ route('all_projects') }}">আরো দেখুন</a>
                            </div>
                        </div>

                        <div class="tab-pane" id="tab_1_1_8">
                            <div class="tab-pane active" id="tab_1_1_7">
                                <div class="table-scrollable">
                                    <table class="table table-hover">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>স্পেসের ঠিকানা</th>
                                                <th>ধরন</th>
                                                <th>নাম্বার</th>
                                                <th>পরিমান</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($construction_space as $key=>$value)
                                            <tr>
                                                <td>{{ $key+1 }}</td>
                                                <td>{{ $value->getProject->PR_NAME_BN }}</td>
                                                <td>{{ $value->getType->TYPE_NAME }}</td>
                                                <td>{{ $value->PR_SSF_NO }}</td>
                                                <td>{{ $value->PR_MEASURMENT }} বর্গফুট</td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="box-footer clearfix">
                                <a class="btn btn-sm btn-default btn-flat pull-right" href="{{ route('all_projects') }}">আরো দেখুন</a>
                            </div>
                        </div>

                        <div class="tab-pane" id="tab_1_1_9">
                            <div class="tab-pane active" id="tab_1_1_7">
                                <div class="table-scrollable">
                                    <table class="table table-hover">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>গ্রাহক</th>
                                                <th>স্পেসের ঠিকানা</th>
                                                <th>ধরন</th>
                                                <th>নাম্বার</th>
                                                <th>পরিমান</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($allotment_space as $key=>$value)
                                            <tr>
                                                <td>{{ $key+1 }}</td>
                                                <td>
                                                    @if(!empty($value->getCitizen))
                                                        {{ $value->getCitizen->getTenderCitizen->FULL_NAME }}
                                                    @endif
                                                </td>
                                                <td>{{ $value->getProject->PR_NAME_BN }}</td>
                                                <td>{{ $value->projectDetails->getType->TYPE_NAME }}</td>
                                                <td>{{ $value->projectDetails->PR_SSF_NO }}</td>
                                                <td>{{ $value->projectDetails->PR_MEASURMENT }} বর্গফুট</td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="box-footer clearfix">
                                <a class="btn btn-sm btn-default btn-flat pull-right" href="{{ route('all_projects') }}">আরো দেখুন</a>
                            </div>
                        </div>
                    </div>
                </div>-->
            </div>
        </div>
    </div>
    <!-- সায়রাত মহালের তালিকা ===============-->
    <div class="col-md-6">
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-gift"></i>{{trans('admin.description_of_the_syarat_mahal')}}
                </div>
                <div class="tools">
                    <a href="javascript:;" class="collapse"> </a>
                    <a title="" data-original-title="" href="javascript:;" class="remove"> </a>
                </div>
            </div>
            <div class="portlet-body">
                <div class="row">
                    <div class="col-lg-4 col-md-3 col-sm-6 col-xs-12">
                        <div class="dashboard-stat2 ">
                            <div class="display">
                                <div class="number">
                                    <h3 class="font-green-sharp">
                                        <span data-value="{{ count($ready_lease) }}" data-counter="counterup"><i class="icon-pie-chart"></i>{{ count($ready_lease) }}</span>
                                        <small class="font-green-sharp">{{trans('admin.tt')}}</small>
                                    </h3>
									<small><a class="title" href="{{ route('lease_list') }}">{{trans('admin.Syarat_Mahal')}} <i class="icon-arrow-right"></i></a></small>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-3 col-sm-6 col-xs-12">
                        <div class="dashboard-stat2 ">
                            <div class="display">
                                <div class="number">
                                    <h3 class="font-green-sharp">
                                        <span data-value="{{ count($lease) }}" data-counter="counterup"><i class="icon-pie-chart"></i>{{ count($lease) }}</span>
                                        <small class="font-green-sharp">{{trans('admin.tt')}}</small>
                                    </h3>
                                    <small><a class="title" href="{{ route('lease_list') }}">{{trans('admin.Lease')}} <i class="icon-arrow-right"></i></a></small>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--<div class="tabbable-custom nav-justified">
                    <ul class="nav nav-tabs nav-justified">
                        <li class="active">
                            <a href="ui_tabs_accordions_navs.html#tab_1_1_10" data-toggle="tab">সায়রাত মহালের তালিকা</a>
                        </li>
                        <li>
                            <a href="ui_tabs_accordions_navs.html#tab_1_1_11" data-toggle="tab">ইজারা</a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab_1_1_10">
                            <div class="table-scrollable">
                                <table class="table table-hover">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>মহালের নাম</th>
                                            <th>মহালের ঠিকানা</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($ready_lease as $key=>$value)
                                        <tr>
                                            <td>{{ $key+1 }}</td>
                                            <td>{{ $value->PR_NAME_BN }}</td>
                                            <td>{!! $value->PROJECT_LOCATION !!}</td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <div class="box-footer clearfix">
                                <a class="btn btn-sm btn-default btn-flat pull-right" href="{{ route('lease_list') }}">আরো দেখুন</a>
                            </div>
                        </div>

                        <div class="tab-pane" id="tab_1_1_11">
                            <div class="tab-pane active" id="tab_1_1_10">
                                <div class="table-scrollable">
                                    <table class="table table-hover">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>গ্রাহক</th>
                                                <th>মহালের নাম</th>
                                                <th>মহালের ঠিকানা</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($lease as $key=>$value)
                                            <tr>
                                                <td>{{ $key+1 }}</td>
                                                <td>
                                                    @if($value->getCitizen)
                                                        {{ $value->getCitizen->getTenderCitizen->FULL_NAME }}
                                                    @endif
                                                </td>
                                                <td>{{ $value->getProject->PR_NAME_BN }}</td>
                                                <td>{!! $value->getProject->PROJECT_LOCATION !!}</td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="box-footer clearfix">
                                <a class="btn btn-sm btn-default btn-flat pull-right" href="{{ route('lease_list') }}">আরো দেখুন</a>
                            </div>
                        </div>
                    </div>
                </div>-->
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="portlet box green">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-cloud"></i>{{trans('admin.Land_information')}}
                </div>
                <div class="tools">
                    <a href="javascript:;" class="collapse"> </a>
                    <a title="" data-original-title="" href="javascript:;" class="remove"> </a>
                </div>
            </div>
            <div class="portlet-body">
                <div class="row">
                    <div class="col-lg-4 col-md-3 col-sm-6 col-xs-12">
                        <div class="easy-pie-chart">
                            <div data-percent="{{ empty($land_ousted) && empty($land_possession)?0:100 }}" class="number visits">
                                <span>{{ empty($land_ousted) && empty($land_possession)?Helpers::numberConvert(number_format(0,2)):Helpers::numberConvert(number_format($land_ousted + $land_possession,2)) }}</span>% <canvas height="75" width="75"></canvas></div>
                            <a href="{{ route('land_list') }}" class="title">{{trans('admin.Total_land_(percent)')}}
                                <i class="icon-arrow-right"></i>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-3 col-sm-6 col-xs-12">
                        <div class="easy-pie-chart">
                            <div data-percent="{{ empty($land_ousted) && empty($land_possession)?0:(100 * $land_possession) / ($land_ousted + $land_possession)  }}" class="number bounce">
                                <span>{{ empty($land_ousted) && empty($land_possession)?Helpers::numberConvert(number_format(0,2)):Helpers::numberConvert(number_format($land_possession,2)) }}</span>% <canvas height="75" width="75"></canvas></div>
                            <a href="{{ route('land_list') }}" class="title">{{trans('admin.Occupancy_land_(percent)')}}
                                <i class="icon-arrow-right"></i>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-3 col-sm-6 col-xs-12">
                        <div class="easy-pie-chart">
                            <div data-percent="{{ empty($land_ousted) && empty($land_possession)?0:(100 * $land_ousted) / ($land_ousted + $land_possession) }}" class="number transactions">
                                <span>{{ empty($land_ousted) && empty($land_possession)?Helpers::numberConvert(number_format(0,2)):Helpers::numberConvert(number_format($land_ousted,2)) }}</span>% <canvas height="75" width="75"></canvas></div>
                            <a href="{{ route('land_list') }}" class="title">{{trans('admin.Occupied_land')}}
                                <i class="icon-arrow-right"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
{{-- <div class="row">
    <div class="col-md-6 col-sm-6">
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption">
                    <i class="icon-bubble font-hide hide"></i>
                    <span class="caption-subject font-hide bold uppercase">Chats</span>
                </div>
                <div class="actions">
                    <div class="portlet-input input-inline">
                        <div class="input-icon right">
                            <i class="icon-magnifier"></i>
                            <input type="text" class="form-control input-circle" placeholder="search..."> </div>
                    </div>
                </div>
            </div>
            <div class="portlet-body" id="chats">
                <div class="scroller" style="height: 400px;" data-always-visible="1" data-rail-visible1="1">
                    <ul class="chats">
                        <li class="in">
                            <img class="avatar" alt="" src="{{ Helpers::asset('assets/pages/img/avatars/team6.jpg') }}">
                            <div class="message">
                                <span class="arrow"> </span>
                                <a href="javascript:;" class="name"> Bob Nilson </a>
                                <span class="datetime"> at 20:30 </span>
                                <span class="body"> Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. </span>
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="chat-form">
                    <div class="input-cont">
                        <input class="form-control enter-chat-msg" type="text" placeholder="Type a message here..." /> </div>
                    <div class="btn-cont">
                        <span class="arrow"> </span>
                        <a href="#" class="btn blue icn-only post-chat-link">
                            <i class="fa fa-check icon-white"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> --}}

<div id="chat_out">
    @if( !empty($user->getDetails) && !empty($user->getDetails->IMAGE) && File::exists(public_path()."/upload/user/".$user->getDetails->IMAGE.'.'.$user->getDetails->CONTENT_TYPE) )
        <img src='{{ Helpers::asset("upload/user/".$user->getDetails->IMAGE.'.'.$user->getDetails->CONTENT_TYPE) }}' class="avatar img-rounded" alt="User">
    @else
        <img src="{{ Helpers::asset('assets/img/logo/profile_icon.jpg') }}" class="avatar" alt="User">
    @endif
    <div class="message">
        <span class="arrow"> </span>
        <a href="javascript:;" class="name">{{ $user->FULL_NAME }}</a>
        <span class="datetime"> at <span></span></span>
        <span class="body"></span>
    </div>
</div>

@endsection