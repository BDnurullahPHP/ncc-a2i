@extends('admin_access.layout.master')

@section('css_file')

	<link href="{{ Helpers::asset('assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ Helpers::asset('assets/global/plugins/morris/morris.css') }}" rel="stylesheet" type="text/css" />

@endsection

@section('style')

    <style type="text/css">
        .dashboard-stat2{
            padding: 5px 15px 0px;
            margin-bottom: 8px;
        }
        img.chart_loader{
            
        }
        .ui-datepicker-calendar {
            display: none;
        }
    </style>

@endsection

@section('js_file')

	<script src="{{ Helpers::asset('assets/global/plugins/moment.min.js') }}" type="text/javascript"></script>
    <script src="{{ Helpers::asset('assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js') }}" type="text/javascript"></script>
    <script src="{{ Helpers::asset('assets/global/plugins/morris/morris.min.js') }}" type="text/javascript"></script>
    <script src="{{ Helpers::asset('assets/global/plugins/morris/raphael-min.js') }}" type="text/javascript"></script>
    <script src="{{ Helpers::asset('assets/global/plugins/counterup/jquery.waypoints.min.js') }}" type="text/javascript"></script>
    <script src="{{ Helpers::asset('assets/global/plugins/counterup/jquery.counterup.min.js') }}" type="text/javascript"></script>
	<script src="{{ Helpers::asset('assets/global/plugins/jquery-easypiechart/jquery.easypiechart.min.js') }}" type="text/javascript"></script>

    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/highcharts-3d.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>
    
    <script type="text/javascript">
        var chart_data = "{{ route('revenue_chart_data') }}";
        var pie_data = {!! $revenue_pie !!};
    </script>
	<script src="{{ Helpers::asset('assets/pages/scripts/dashboard.min.js') }}" type="text/javascript"></script>

@endsection

@section('script')

	<script type="text/javascript">
		$(document).ready(function() {
            
            Dashboard.init();

            $(function() {
                $('#revenue_month').datepicker({
                    changeMonth: true,
                    changeYear: true,
                    showButtonPanel: true,
                    dateFormat: 'MM yy',
                    onClose: function(dateText, inst) { 
                        $(this).datepicker('setDate', new Date(inst.selectedYear, inst.selectedMonth, 1));
                    }
                }).datepicker("setDate", new Date());
            });

            var cmonth = moment().format('MMMM YYYY');
            getRevenueData(cmonth);

            $('#revenue_month').blur(function(event) {
                var month = $(this).val();
                $('img.chart_loader').css('display', 'block');
                $('#account_overview').html('');
                getRevenueData(month);
            });

	    });

        function getRevenueData(month){
            $.ajax({
                url: chart_data,
                type: 'GET',
                dataType: 'json',
                data: {month: month},
            })
            .done(function(res) {
                $('img.chart_loader').hide();
                new Morris.Bar({
                    element: "account_overview",
                    data: res.data,
                    xkey: "y",
                    ykeys: ["a"],
                    labels: ["মোট আয়"]
                });
            });
        }

	</script>

@endsection

@section('content')

	<div class="row">
        <div class="col-sm-12">
            <h3>{{ trans('admin.dashboard') }}
                <small>{{ trans('admin.dashboard_heading') }}</small>
            </h3>
            
        </div>
    </div>
    <!-- END PAGE TITLE-->
    <!-- END PAGE HEADER-->
    <!-- BEGIN DASHBOARD STATS 1-->
    <div class="row">
        <div class="col-lg-4 col-md-3 col-sm-6 col-xs-12">
            <a href="{{ route('revenue_list') }}" class="dashboard-stat dashboard-stat-v2 blue">
                <div class="visual">
                    <i class="fa fa-money"></i>
                </div>
                <div class="details">
                    <div class="number">
                        <span data-counter="counterup" data-value="{{ Helpers::numberConvert(number_format($total_revenue_cr,2)) }}">{{ Helpers::numberConvert(number_format($total_revenue_cr,2)) }}</span>৳
                    </div>
                    <div class="desc">{{ trans('admin.total_expeted_revenue') }}</div>
                </div>
            </a>
        </div>
        <div class="col-lg-4 col-md-3 col-sm-6 col-xs-12">
            <a href="{{ route('revenue_list') }}" class="dashboard-stat dashboard-stat-v2 green">
                <div class="visual">
                    <i class="fa fa-money"></i>
                </div>
                <div class="details">
                    <div class="number">
                        <span data-counter="counterup" data-value="{{ Helpers::numberConvert(number_format($total_revenue_dr,2)) }}">{{ Helpers::numberConvert(number_format($total_revenue_dr,2)) }}</span>৳
                    </div>
                    <div class="desc">{{ trans('admin.total_collect_revenue') }}</div>
                </div>
            </a>
        </div>
        <div class="col-lg-4 col-md-3 col-sm-6 col-xs-12">
            <a href="{{ route('revenue_list') }}" class="dashboard-stat dashboard-stat-v2 red">
                <div class="visual">
                    <i class="fa fa-money"></i>
                </div>
                <div class="details">
                    <div class="number">
                        <span data-counter="counterup" data-value="{{ Helpers::numberConvert(number_format($total_revenue_cr - $total_revenue_dr,2)) }}">{{ Helpers::numberConvert(number_format($total_revenue_cr - $total_revenue_dr,2)) }}</span>৳</div>
                    <div class="desc">{{ trans('admin.total_due_revenue') }}</div>
                </div>
            </a>
        </div>
    </div>
    <div class="clearfix"></div>
    <!-- END DASHBOARD STATS 1-->

    <div class="row">
        <div class="col-md-12">
            <div class="portlet light portlet-fit bordered">
                <div class="portlet-title">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="caption">
                                <i class=" icon-layers font-green-sharp"></i>
                                <span class="caption-subject font-green-sharp bold uppercase" style="font-size: 16px;">{{trans('account.Daily_income_statement')}}</span>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <input type="text" class="pull-right" id="revenue_month">
                        </div>
                    </div>
                </div>
                <div class="portlet-body">
                    <img src="{{ Helpers::asset('assets/img/loader.gif') }}" alt="loader" class="chart_loader center-block">
                    <div id="account_overview" style="height:300px;"></div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-cloud"></i>{{trans('account.Store_space_flat_lease_statistics')}}
                    </div>
                    <div class="tools">
                        <a class="collapse" href="javascript:;" data-original-title="" title=""> </a>
                        <a class="remove" href="javascript:;" data-original-title="" title=""> </a>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="row">
                        @if(count($category_total) > 0)
                            @foreach($category_total as $key => $cat)
                                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                    <div class="dashboard-stat2 ">
                                        <div class="display">
                                            <div class="number">
                                                @if($key == 0)
                                                    <h3 class="font-green-sharp">
                                                @elseif($key == 1)
                                                    <h3 class="font-red-haze">
                                                @else
                                                    <h3 class="font-blue-sharp">
                                                @endif
                                                    <span data-value="{{ $cat->total }}" data-counter="counterup"><i class="icon-pie-chart"></i>{{ $cat->total }}</span>
                                                    <small class="font-green-sharp">{{trans('account.tt')}}</small>
                                                </h3>
                                                <small>{{ $cat->getCategory->CATE_NAME }}</small>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        @endif
                    </div>
                    
                    @if(count($category_revenue) > 0)
                        <div class="tabbable-custom nav-justified">
                            <ul class="nav nav-tabs nav-justified">
                                @foreach($category_revenue as $inx => $ctr)
                                    <li class="{{ $inx==0?'active':''}}">
                                        <a href="#tab_{{ $ctr->PR_CATEGORY }}" data-toggle="tab">{{ $ctr->getCategory->CATE_NAME }}</a>
                                    </li>
                                @endforeach
                            </ul>
                            <div class="tab-content">
                                @foreach($category_revenue as $index => $ctr)
                                    <div class="tab-pane {{ $index==0?'active':'' }}" id="tab_{{ $ctr->PR_CATEGORY }}">
                                        <div class="table-scrollable">
                                            <table class="table table-hover">
                                                <thead>
                                                    <tr>
                                                        <td>{{trans('account.Total_expected')}}</td>
                                                        <td>{{ Helpers::numberConvert(number_format($ctr->CR_AMT,2)) }} ৳</td>
                                                    </tr>
                                                    <tr>
                                                        <td class="text-success">{{trans('account.Gross_income')}} </td>
                                                        <td class="text-success">{{ Helpers::numberConvert(number_format($ctr->DR_AMT,2)) }} ৳</td>
                                                    </tr>
                                                    <tr>
                                                        <td class="text-danger">{{trans('account.Total_owes')}}</td>
                                                        <td class="text-danger">{{ Helpers::numberConvert(number_format($ctr->CR_AMT - $ctr->DR_AMT,2)) }}{{trans('account.Daily_income_statement')}}</td>
                                                    </tr>
                                                </thead>
                                            </table>
                                        </div>
                                        <div class="box-footer clearfix">
                                            <a class="btn btn-sm btn-default btn-flat pull-right" href="{{ route('revenue_list') }}">{{trans('account.View_more')}}</a>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    @endif

                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="portlet box green">
                <div class="portlet-title">
                    <div class="caption">
                        {{trans('account.Playlist_Citizens_List')}}
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"> </a>
                        <a title="" data-original-title="" href="javascript:;" class="remove"> </a>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="tabbable-custom nav-justified">
                        <div class="table-scrollable">
                            <table class="table table-hover table-bordered">
                                <thead>
                                <tr>
                                    <th>{{trans('account.sl_no')}}</th>
                                    <th>{{trans('account.name')}} </th>
                                    <th>{{trans('account.project')}}</th>
                                    <th>{{trans('account.Amount_of_money')}} </th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>১</td>
                                    <td>১ নং ডি আই টি মার্কেট সংলগ্ন সিটি বিপণী বিতান</td>
                                    <td>দোকান</td>
                                    <td>১,০০০ ৳ </td>
                                </tr>
                                <tr>
                                    <td>২ </td>
                                    <td>১ নং ডি আই টি মার্কেট সংলগ্ন সিটি বিপণী বিতান</td>
                                    <td>দোকান</td>
                                    <td>৩,০০০ ৳ </td>
                                </tr>
                                <tr>
                                    <td>৩ </td>
                                    <td>পদ্ম সিটি প্লাজা-৪ </td>
                                    <td>দোকান</td>
                                    <td>২,০০০ ৳ </td>
                                </tr>
                                <tr>
                                    <td>৪ </td>
                                    <td>পদ্ম সিটি প্লাজা-৪ ন</td>
                                    <td>দোকান</td>
                                    <td>১,০০০ ৳ </td>
                                </tr>
                                <tr>
                                    <td>৫ </td>
                                    <td>পদ্ম সিটি প্লাজা-৪ </td>
                                    <td>দোকান</td>
                                    <td>২,০০০ ৳ </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="box-footer clearfix">
                            <a class="btn btn-sm btn-default btn-flat pull-right" href="#">{{trans('account.View_more')}}আরো দেখুন</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box purple">
                <div class="portlet-title">
                    <div class="caption">
                        {{ Helpers::numberConvert(date('Y')) }}{{trans('account.In_store_space_flat_lease_income_statistics_in_year')}} 
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"> </a>
                        <a title="h" data-original-title="" href="javascript:;" class="remove"> </a>
                    </div>
                </div>
                <div class="portlet-body">
                    <div id="pie_container" style="height: 400px"></div>
                </div>
            </div>
        </div>
    </div>

@endsection