@extends('admin_access.layout.master')
@section('content')

<div class="row">
    <div class="col-sm-12">
        <h3 class="page_heading">{{ trans('admin.dashboard') }}
        </h3>
    </div>
</div>
<div class="row">
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <a href="{{ route('all_projects') }}" class="dashboard-stat dashboard-stat-v2 blue">
            <div class="visual">
                <i class="fa fa-tasks"></i>
            </div>
            <div class="details">
                <div class="number">
                    <span data-counter="counterup" data-value="{{ $project }}">{{ $project }}</span>{{trans('admin.tt')}}
                </div>
                <div class="desc">{{ trans('admin.total_project') }}</div>
            </div>
        </a>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <a href="#" class="dashboard-stat dashboard-stat-v2 green">
            <div class="visual">
                <i class="fa fa-tasks"></i>
            </div>
            <div class="details">
                <div class="number">
                    <span data-counter="counterup" data-value="{{ $shop }}">{{ $shop }}</span>{{trans('admin.tt')}}</div>
                <div class="desc">{{ trans('admin.Total_store') }}</div>
            </div>
        </a>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <a href="{{ route('all_projects') }}" class="dashboard-stat dashboard-stat-v2 blue">
            <div class="visual">
                <i class="fa fa-tasks"></i>
            </div>
            <div class="details">
                <div class="number">
                    <span data-counter="counterup" data-value="{{ $flat }}">{{ $flat }}</span> {{trans('admin.tt')}}
                </div>
                <div class="desc">{{ trans('admin.Total_Flat') }}</div>
            </div>
        </a>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <a href="#" class="dashboard-stat dashboard-stat-v2 red">
            <div class="visual">
                <i class="fa fa-tasks"></i>
            </div>
            <div class="details">
                <div class="number">
                    <span data-counter="counterup" data-value="{{ $space }}">{{ $space }}</span>{{trans('admin.tt')}}
                </div>
                <div class="desc">{{trans('admin.Total_Space')}}</div>
            </div>
        </a>
    </div>
</div>
@endsection