@extends('admin_access.layout.master')

@section('css_file')



@endsection

@section('style')


@endsection

@section('js_file')
<script type="text/javascript">
    $(document).ready(function() {
        $(document.body).on('click', 'a.approve_post', function(event) {
            event.preventDefault();

            var parent_row = $(this).closest('tr');
            var get_url = $(this).attr('href').split('=');

            swal({
                title: "Are you sure?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, approve it!",
                cancelButtonText: "No, cancel",
                closeOnConfirm: false, //If false then confirm message will enable
                closeOnCancel: true
            }, function(isConfirm) {
                if (isConfirm) {

                    $.ajax({
                        url: get_url,
                        type: 'GET',
                        dataType: 'html',
                        data: false,
                    })
                    .done(function(res) {
                        parent_row.remove();
                        swal("Approved", res, "success");
                    });

                }
            });
        });
    });
</script>

@endsection

@section('script')


@endsection

@section('content')

<h3>অপেক্ষারত অর্থ গ্রহণের তালিকা
    <small></small>
</h3>

<div class="portlet light bordered">
    <!-- <div class="portlet-title">
        <div class="caption">
            
        </div>
        <div class="tools"> </div>
    </div> -->
    <div class="row">
        <div class="col-md-5 col-sm-6 col-xs-12">
            <div id="lg_form_message"></div>
        </div>
    </div>
    <div class="portlet-body contentArea">

        <table class="table table-striped table-bordered table-hover" data-source="{{ route('payment_pending_data') }}" id="common_table">
            <thead>
                <tr>
                    <th>{{ trans('common.table_sl') }}</th>
                    <th>যাহার নিকট হইতে প্রাপ্ত</th>
                    <th>রশিদ নং</th>
                    <th>রশিদের তারিখ</th>
                    <th>কি বাবদ পাওয়া গেল</th>
                    <th>টাকার পরিমাণ</th>
                    <th>{{ trans('common.table_action') }}</th>
                </tr>
            </thead>
            <tbody>

            </tbody>
        </table>
    </div>
</div>
@endsection