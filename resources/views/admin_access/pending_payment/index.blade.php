@extends('admin_access.layout.master')

@section('css_file')



@endsection

@section('style')


@endsection

@section('js_file')
<script type="text/javascript">
    $(document).ready(function() {
        $(document.body).on('click', 'a.approve_post', function(event) {
            event.preventDefault();

            var parent_row = $(this).closest('tr');
            var get_url = $(this).attr('href').split('=');

            swal({
                title: "আপনি কি নিশ্চিত?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "হ্যাঁ!",
                cancelButtonText: "না",
                closeOnConfirm: false, //If false then confirm message will enable
                closeOnCancel: true
            }, function(isConfirm) {
                if (isConfirm) {

                    $.ajax({
                        url: get_url,
                        type: 'GET',
                        dataType: 'html',
                        data: false,
                    })
                    .done(function(res) {
                        parent_row.remove();
                        swal("Approved", res, "success");
                    });

                }
            });
        });
    });
</script>

@endsection

@section('script')


@endsection

@section('content')

<h3>{{ trans('account.receive_register') }}
    <small></small>
</h3>

<div class="portlet light bordered">
    <!-- <div class="portlet-title">
        <div class="caption">
            
        </div>
        <div class="tools"> </div>
    </div> -->
    <div class="row">
        <div class="col-md-5 col-sm-6 col-xs-12">
            <div id="lg_form_message"></div>
        </div>
    </div>
    <div class="portlet-body contentArea">

        <table class="table table-striped table-bordered table-hover" data-source="{{ route('payment_pending_data') }}" id="common_table">
            <thead>
                <tr>
                    <th>{{ trans('common.table_sl') }}</th>
                    <th>{{ trans('account.getting_from_whom') }}</th>
                    <th>{{ trans('account.receipt_no') }}</th>
                    <th>{{ trans('account.receipt_date') }}</th>
                    <th>{{ trans('account.receipt_type') }}</th>
                    <th>{{ trans('account.amount_money') }}</th>
                    <th>{{ trans('common.table_action') }}</th>
                </tr>
            </thead>
            <tbody>

            </tbody>
        </table>
    </div>
</div>
@endsection