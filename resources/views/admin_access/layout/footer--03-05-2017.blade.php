<div class="pull-right" style="color: gray">
    <strong style="font-weight: 400;"> Developed By
        <a href="http://www.atilimited.net" target="_blank">
            <img style="width: 30px;" src="{{ Helpers::asset('assets/img/logo/logo_ati_large.gif') }}" alt="NCC" class="logo-default" />
            <span style="color: red;">ATI</span>
            <span style="color: green;">Limited</span>
        </a>
    </strong>
</div>
<div style="color: gray">
    <strong style="font-weight: 400;">Copyright</strong> NCC &copy; {{ date('Y') }}
</div>
<div class="scroll-to-top">
    <i class="icon-arrow-up"></i>
</div>