<div class="col-md-12">
    <div class="col-md-4" style="color: gray">
        <strong style="font-weight: 400;">{{trans('admin.overall_collaboration')}} 
            <a href="http://www.a2i.pmo.gov.bd/" target="_blank">
                <img style="width: 30px;" src="{{ Helpers::asset('assets/portal/images/a2ilogo-final.png') }}" alt="a2i" class="logo-default" />
            </a>
        </strong>
    </div>
    <div class="col-md-4" style="color: gray; text-align: center;">
           <strong style="font-weight: 400;">{{trans('admin.Copyright')}}:  &copy; {{ Helpers::numberConvert(date('Y')) }}</strong>{{trans('admin.Narayanganj_City_Corporation_and_A2i')}} 
    </div>
    <div class="col-md-4 pull-right" style="color: gray; text-align: right;">
        <strong style="font-weight: 400;">{{trans('admin.Design_and_development')}} 
            <a href="http://www.atilimited.net" target="_blank">
                <img style="width: 30px;" src="{{ Helpers::asset('assets/img/logo/logo_ati_large.gif') }}" alt="NCC" class="logo-default" />
                <span style="color: red;">ATI</span>
                <span style="color: green;">Limited</span>
            </a>
        </strong>
    </div>
</div>
<!--<div class="pull-right" style="color: gray">
    <strong style="font-weight: 400;"> Developed By
        <a href="http://www.atilimited.net" target="_blank">
            <img style="width: 30px;" src="{{ Helpers::asset('assets/img/logo/logo_ati_large.gif') }}" alt="NCC" class="logo-default" />
            <span style="color: red;">ATI</span>
            <span style="color: green;">Limited</span>
        </a>
    </strong>
</div>
<div style="color: gray">
    <strong style="font-weight: 400;">Copyright</strong> NCC &copy; {{ date('Y') }}
</div>-->
<div class="scroll-to-top">
    <i class="icon-arrow-up"></i>
</div>