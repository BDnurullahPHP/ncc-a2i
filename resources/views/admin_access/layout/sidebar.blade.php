<div class="page-sidebar-wrapper">
    <div class="page-sidebar navbar-collapse collapse">
        <ul class="page-sidebar-menu  page-header-fixed " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 20px">
            <li class="sidebar-toggler-wrapper hide">
                <div class="sidebar-toggler"> </div>
            </li>
            @if(Auth::check())
<!--            <li class="nav-item">
                <a href="{{ route('admin_dashboard') }}" class="nav-link nav-toggle">
                    <i class="glyphicon glyphicon-dashboard"></i>
                    <span class="title">{{ trans('common.mm_dashboard') }}</span>
                    <span class="selected"></span>
                </a>
            </li>
            @if(!Entrust::hasRole('admin'))
            <li class="nav-item">
                <a href="{{ route('user_profile', $user->USER_ID) }}" class="nav-link nav-toggle">
                    <i class="fa fa-user"></i>
                    <span class="title">{{ trans('user.my_profile') }}</span>
                    <span class="selected"></span>
                </a>
            </li>
            @endif

            ==================Land management===================
            @if(Entrust::hasRole('admin'))
            <li class="nav-item ">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="fa fa-clone"></i>
                    <span class="title">{{ trans('land.land_dropdown_label') }}</span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu">
                    <li class="nav-item">
                        <a href="{{ route('land_list') }}" class="nav-link">
                            <span class="title">{{ trans('land.land_dropdown_list') }}</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('land_category') }}" class="nav-link">
                            <span class="title">{{ trans('land.land_dropdown_category') }}</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('land_subcategory') }}" class="nav-link">
                            <span class="title">{{ trans('land.land_dropdown_subcategory') }}</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('zone_list') }}" class="nav-link">
                            <span class="title">{{ trans('land.zone') }}</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('word_list') }}" class="nav-link">
                            <span class="title">{{ trans('land.ward') }}</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('mouza_list') }}" class="nav-link">
                            <span class="title">{{ trans('land.mouza_list') }}</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('land_report') }}" class="nav-link">
                            <span class="title">{{ trans('land.land_report') }}</span>
                        </a>
                    </li>
                </ul>
            </li>

            <li class="nav-item ">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="fa fa-clone"></i>
                    <span class="title">Security & Access</span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu">
                    <li class="nav-item">
                        <a href="{{ route('module_list') }}" class="nav-link">
                            <span class="title">Modules</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('module_link_list') }}" class="nav-link">
                            <span class="title">Module Link</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('module_org') }}" class="nav-link">
                            <span class="title">ORG</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('all_group') }}" class="nav-link">
                            <span class="title">User Group</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('assign_to_group') }}" class="nav-link">
                            <span class="title">Assign Module </span>
                        </a>
                    </li>
                </ul>
            </li>
            @endif

            ==================project menu===================
            @if(Entrust::hasRole(['admin', 'project-manager']))
            <li class="nav-item ">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="fa fa-building"></i>
                    <span class="title">{{ trans('project.project_management') }}</span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu">
                                                <li class="nav-item">
                                                    <a href="{{ route('all_projects') }}" class="nav-link">
                                                        <span class="title">{{ trans('project.project_dropdown_list') }}</span>
                                                    </a>
                                                </li>
                    <li class="nav-item">
                        <a href="{{ route('all_market') }}" class="nav-link">
                            <span class="title">{{ trans('project.project_dropdown_list') }}</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('all_market_details') }}" class="nav-link">
                            <span class="title">প্রকল্পের বিবরণ</span>
                        </a>
                    </li>

                    <li class="nav-item">
                        <a href="{{ route('project_category') }}" class="nav-link">
                            <span class="title">{{ trans('project.project_dropdown_category') }}</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('project_type') }}" class="nav-link">
                            <span class="title">{{ trans('project.project_dropdown_type') }}</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('project_location') }}" class="nav-link">
                            <span class="title">{{ trans('project.project_dropdown_location') }}</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('project_cat_con') }}" class="nav-link">
                            <span class="title">{{ trans('project.project_dropdown_cat_con') }}</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('floor') }}" class="nav-link">
                            <span class="title">{{ trans('project.project_floor') }}</span>
                        </a>
                    </li>
                </ul>
            </li>
            @endif

            ==================Tender menu===================
            @if(Entrust::hasRole(['admin', 'tender-manager']))
            ==================Tender management===================
            <li class="nav-item ">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="fa fa-list-alt"></i>
                    <span class="title">{{ trans('tender.tender_dropdown_label') }}</span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu">
                    <li class="nav-item">
                        <a href="{{ route('tender_list') }}" class="nav-link">
                            <span class="title">{{ trans('tender.tender_list') }}</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('tender_applicant_comparison') }}" class="nav-link">
                            <span class="title">{{ trans('tender.tender_comparison_list') }}</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('tender_applicant_list') }}" class="nav-link">
                            <span class="title">{{ trans('tender.tender_applicant_list') }}</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('tender_primary_selected_applicant_list') }}" class="nav-link">
                            <span class="title">{{ trans('tender.tender_primary_selected_applicant_list') }}</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('tender_final_selected_applicant_list') }}" class="nav-link">
                            <span class="title">{{ trans('tender.final_selected_applicant_list') }}</span>
                        </a>
                    </li>
                </ul>
            </li>
            @endif

            ==================Lease menu===================
            @if(Entrust::hasRole(['admin', 'lease-manager']))
            <li class="nav-item ">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="fa fa-clone"></i>
                    <span class="title">{{ trans('common.mm_lease_management') }}</span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu">
                    <li class="nav-item">
                        <a href="{{ route('lease_list') }}" class="nav-link">
                            <span class="title">{{ trans('lease.sayarata_mahal') }}</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('lease_notice') }}" class="nav-link">
                            <span class="title">{{ trans('lease.lease_notice') }}</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('lease_demesne') }}" class="nav-link">
                            <span class="title">{{ trans('lease.demesne_list') }}</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('demesne_money') }}" class="nav-link">
                            <span class="title">{{ trans('lease.demesne_money') }}</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('lease_comparison') }}" class="nav-link">
                            <span class="title">{{ trans('lease.lease_comparison_list') }}</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('lease_applicant_list') }}" class="nav-link">
                            <span class="title">{{ trans('lease.lease_applicant_list') }}</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('lease_primary_selected_applicant_list') }}" class="nav-link">
                            <span class="title">{{ trans('tender.tender_primary_selected_applicant_list') }}</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('lease_final_selected_applicant_list') }}" class="nav-link">
                            <span class="title">{{ trans('tender.final_selected_applicant_list') }}</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('lease_revenue_list') }}" class="nav-link">
                            <span class="title">{{ trans('lease.lease_repor') }}</span>
                        </a>
                    </li>
                </ul>
            </li>
            @endif

            @if(Entrust::hasRole(['admin', 'account-manager']))
            ==================Account management===================
            <li class="nav-item ">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="fa fa-money"></i> 
                    <span class="title">{{ trans('account.account_management') }}</span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu">
                    <li class="nav-item">
                        <a href="{{ route('particular_list') }}" class="nav-link">
                            <span class="title">{{ trans('account.particular_list') }}</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('category_particular') }}" class="nav-link">
                            <span class="title">{{ trans('account.category_particular') }}</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('bank_list') }}" class="nav-link">
                            <span class="title">{{ trans('bank.bank_list') }}</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('payment_pending_list') }}" class="nav-link">
                            <span class="title">{{ trans('account.receive_register') }}</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="#" class="nav-link">
                            <span class="title"> একাউন্ট রেজিস্টার </span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('cashiar_cashboak') }}" class="nav-link">
                            <span class="title"> ক্যাশবুক রেজিস্টার  </span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('revenue_list') }}" class="nav-link">
                            <span class="title">{{ trans('account.revenue_list') }}</span>
                        </a>
                    </li>
                     <li class="nav-item">
                        <a href="{{ route('applicant_pending_list') }}" class="nav-link">
                            <span class="title">{{ trans('account.tender_ins_payment') }}</span>
                        </a>
                    </li> 
                    <li class="nav-item">
                        <a href="{{ route('applicant_security_money') }}" class="nav-link">
                            <span class="title">{{ trans('tender.security_money_return') }}</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('citizen_ledger') }}" class="nav-link">
                            <span class="title">{{ trans('account.citizen_collection_ledger') }}</span>
                        </a>
                    </li>
                </ul>
            </li>
            @endif-->

<!--            @if(Entrust::hasRole(['admin', 'project-manager']))
            <li class="nav-item">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="fa fa-gift"></i>
                    <span class="title">{{ trans('citizen.citizen_service') }}</span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu">
                    <li class="nav-item">
                        <a href="{{ route('notice_list') }}" class="nav-link">
                            <span class="title">{{ trans('account.tender_apply') }}</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('money_collection') }}" class="nav-link">
                            <span class="title">{{ trans('account.money_collection') }}</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('ownership_change') }}" class="nav-link">
                            <span class="title">{{ trans('citizen.ownership_change_or_cancel') }}</span>
                        </a>
                    </li>
                </ul>
            </li>
            @endif-->

            @role('citizen')
            <!--==================Citizen management===================-->
            <?php
            $c_application = DB::table('sa_citizen_application')
                    ->where('CITIZEN_ID', $user->getCitizen->CITIZEN_ID)
                    ->pluck('CITIZEN_APP_ID');
            $tender_aply_exist = DB::table('tender_installment')
                    ->where('CITIZEN_ID', $user->getCitizen->CITIZEN_ID)
                    ->where('IS_ACTIVE', 1)
                    ->pluck('TENDER_INS_ID');
            $c_property = DB::table('sa_citizen_property')
                    ->where('CITIZEN_ID', $user->getCitizen->CITIZEN_ID)
                    ->where('IS_ACTIVE', 1)
                    ->first();
            ?>
            @if(!empty($c_property) && $c_property->IS_LEASE == 1)
            <li class="nav-item">
                <a href="{{ route('citizen_lease_property') }}" class="nav-link nav-toggle">
                    <i class="fa fa-bars" aria-hidden="true"></i>
                    <span class="title">{{ trans('lease.lease_information') }}</span>
                    <span class="selected"></span>
                </a>
            </li>
            @endif
            @if(!empty($c_application))
            <li class="nav-item">
                <a href="{{ route('citizen_form_history') }}" class="nav-link nav-toggle">
                    <i class="fa fa-book" aria-hidden="true"></i>
                    <span class="title">{{ trans('citizen.app_form_info') }}</span>
                    <span class="selected"></span>
                </a>
            </li>
            <li class="nav-item">
                <a href="{{ route('citizen_app_notification') }}" class="nav-link nav-toggle">
                    <i class="fa fa-clock-o" aria-hidden="true"></i>
                    <span class="title">{{ trans('citizen.application_status') }}</span>
                    <span class="selected"></span>
                </a>
            </li>
            @endif
            <li class="nav-item">
                <a href="{{ route('citizen_pay_history') }}" class="nav-link nav-toggle">
                    <i class="fa fa-money" aria-hidden="true"></i>
                    <span class="title">{{ trans('citizen.payment_history') }}</span>
                    <span class="selected"></span>
                </a>
            </li>
            @if(!empty($tender_aply_exist))
            <li class="nav-item">
                <a href="{{ route('citizen_installment_history') }}" class="nav-link nav-toggle">
                    <i class="fa fa-th-large" aria-hidden="true"></i>
                    <span class="title">{{ trans('citizen.installment_history') }}</span>
                    <span class="selected"></span>
                </a>
            </li>
            @endif

            {{-- @if(!empty($c_property) && $c_property->IS_LEASE == 0) --}}
<!--            <li class="nav-item ">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="fa fa-tags"></i> 
                    <span class="title">{{ trans('citizen.rent_management') }}</span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu">
                    <li class="nav-item">
                        <a href="{{ route('citizen_rent_payment') }}" class="nav-link">
                            <span class="title">{{ trans('citizen.rent_payment') }}</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('citizen_rent_history') }}" class="nav-link">
                            <span class="title">{{ trans('citizen.rent_history') }}</span>
                        </a>
                    </li>
                </ul>
            </li>-->
            {{-- @endif --}}
            <!--==============End of Citizen management=================-->
            @endrole

            <!--==================User management==============-->
            @role('admin')
<!--            <li class="nav-item">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="fa fa-users"></i>
                    <span class="title">{{ trans('user.user_management') }}</span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu">
                    <li class="nav-item">
                        <a href="{{ route('user_list') }}" class="nav-link">
                            <span class="title">{{ trans('user.user_list') }}</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('user_profile', $user->USER_ID) }}" class="nav-link">
                            <span class="title">{{ trans('user.my_profile') }}</span>
                        </a>
                    </li>
                </ul>
            </li>-->

            <!--================Access control===================-->
<!--            <li class="nav-item">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="fa fa-cogs"></i>
                    <span class="title">{{ trans('security.mm_security_access') }}</span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu">
                    <li class="nav-item">
                        <a href="{{ route('role_list') }}" class="nav-link">
                            <span class="title">{{ trans('security.role_list') }}</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('permission_list') }}" class="nav-link">
                            <span class="title">{{ trans('security.permission_list') }}</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('role_permission') }}" class="nav-link">
                            <span class="title">{{ trans('security.permission_role') }}</span>
                        </a>
                    </li>
                </ul>
            </li>-->

            <!--================Log viewer===================-->
<!--            <li class="nav-item">
                <a href="{{ route('log-viewer::dashboard') }}" class="nav-link nav-toggle">
                    <i class="fa fa-fw fa-book"></i>
                    <span class="title">{{ trans('security.system_logs') }}</span>
                    <span class="selected"></span>
                </a>
            </li>-->
            @endrole
            @else
            <li class="nav-item">
                <a href="{{ route('applicant_dashboard') }}" class="nav-link nav-toggle">
                    <i class="glyphicon glyphicon-dashboard"></i>
                    <span class="title">{{ trans('common.mm_dashboard') }}</span>
                    <span class="selected"></span>
                </a>
            </li>
            <li class="nav-item">
                <a href="{{ route('application_form_history') }}" class="nav-link nav-toggle">
                    <i class="fa fa-book" aria-hidden="true"></i>
                    <span class="title">{{ trans('citizen.app_form_info') }}</span>
                    <span class="selected"></span>
                </a>
            </li>
            <li class="nav-item">
                <a href="{{ route('te_app_notification') }}" class="nav-link nav-toggle">
                    <i class="fa fa-clock-o" aria-hidden="true"></i>
                    <span class="title">{{ trans('citizen.application_status') }}</span>
                    <span class="selected"></span>
                </a>
            </li>
            <li class="nav-item">
                <a href="{{ route('applicant_pay_history') }}" class="nav-link nav-toggle">
                    <i class="fa fa-money" aria-hidden="true"></i>
                    <span class="title">{{ trans('citizen.payment_history') }}</span>
                    <span class="selected"></span>
                </a>
            </li>
            <?php
            $tender_exist = DB::table('tender_installment')
                    ->where('TE_APP_ID', $user->TE_APP_ID)
                    ->where('IS_ACTIVE', 1)
                    ->pluck('TENDER_INS_ID');
            ?>
            @if(!empty($tender_exist))
            <li class="nav-item">
                <a href="{{ route('applicant_installment_history') }}" class="nav-link nav-toggle">
                    <i class="fa fa-th-large" aria-hidden="true"></i>
                    <span class="title">{{ trans('citizen.installment_history') }}</span>
                    <span class="selected"></span>
                </a>
            </li>
            @endif
            @endif
            
<!-----------------Dynamic Menu------------------------------->

            @if(Auth::check())
            <li class="nav-item">
                <a href="{{ route('admin_dashboard') }}" class="nav-link nav-toggle">
                    <i class="glyphicon glyphicon-dashboard"></i>
                    <span class="title">{{ trans('common.mm_dashboard') }}</span>
                    <span class="selected"></span>
                </a>
            </li>
            @endif
            <?php
            $locale = App::getLocale();
            $session_info = Auth::user();
            $org = $session_info['ORG_ID'];
            $user = $session_info['USER_ID'];
            $org_group = $session_info['USERGRP_ID'];
            $org_group_level = $session_info['USERLVL_ID'];

            $org_mlink = DB::table('sa_uglw_mlink')
                    ->leftJoin('sa_modules', 'sa_uglw_mlink.MODULE_ID', '=', 'sa_modules.MODULE_ID')
                    ->where('sa_uglw_mlink.ORG_ID', $org)
                    ->where('sa_uglw_mlink.USERGRP_ID', $org_group)
                    ->where('sa_uglw_mlink.UG_LEVEL_ID', $org_group_level)
                    ->Where('sa_uglw_mlink.USER_ID', $user)
                    ->orWhere('sa_uglw_mlink.CREATE', "1")
                    ->orWhere('sa_uglw_mlink.READ', "1")
                    ->orWhere('sa_uglw_mlink.UPDATE', "1")
                    ->orWhere('sa_uglw_mlink.DELETE', "1")
                    ->orWhere('sa_uglw_mlink.STATUS', "1")
                    ->distinct()
                    ->get(['sa_modules.MODULE_ID', 'sa_modules.MODULE_NAME', 'sa_modules.MODULE_NAME_BN', 'sa_modules.MODULE_ICON']);

            foreach ($org_mlink as $key =>$value) {
                $module_id = $value->MODULE_ID;
                if ($session_info["USERGRP_ID"] != "") {
                    $links = DB:: table('sa_uglw_mlink')
                            ->leftJoin('sa_module_links', 'sa_uglw_mlink.LINK_ID', '=', 'sa_module_links.LINK_ID')
                            ->where('sa_uglw_mlink.ORG_ID', $org)
                            ->where('sa_uglw_mlink.USERGRP_ID', $org_group)
                            ->where('sa_uglw_mlink.UG_LEVEL_ID', $org_group_level)
                            ->Where('sa_uglw_mlink.USER_ID', $user)
                            ->where('sa_uglw_mlink.MODULE_ID', '=', $module_id)
                            ->where(function($query) {
                                return $query
                                        ->where('sa_uglw_mlink.CREATE', '=', 1)
                                        ->orWhere('sa_uglw_mlink.READ', '=', 1)
                                        ->orWhere('sa_uglw_mlink.UPDATE', '=', 1)
                                        ->orWhere('sa_uglw_mlink.DELETE', '=', 1)
                                        ->orWhere('sa_uglw_mlink.STATUS', '=', 1);
                            })
                            ->get(['sa_uglw_mlink.LINK_ID', 'sa_module_links.LINK_NAME', 'sa_module_links.LINK_NAME_BN', 'sa_module_links.LINK_URI']);
                } else {
                    //$links_user = $this->careProvider_model->get_all_module_links_from_user($modid);
                }
                if (!empty($links)) {
                    ?>
                    <li class="nav-item">
                        <a href="javascript:;" class="nav-link nav-toggle">
                            <i class="<?php echo ($value->MODULE_ICON != '') ? $value->MODULE_ICON : "fa fa-th-large" ?>"></i>
                            <span class="title"><?php echo ($locale == "bn") ? $value->MODULE_NAME_BN : $value->MODULE_NAME; ?></span>
                            
                                <span class="<?php echo ($links[0]->LINK_URI != '') ? 'arrow' : ''?>"></span>
                            
                        </a>

                        <ul class="sub-menu">
                            <?php foreach ($links as $key =>$link) { ?>
                                <li class="nav-item">
                                    <a href="{{ route($link->LINK_URI) }}" class="nav-link">
                                        <span class="title"><?php echo ($locale == "bn" ? $link->LINK_NAME_BN : $link->LINK_NAME); ?></span>
                                    </a>
                                </li>
                            <?php } ?>
                        </ul>
                    </li>
                    <?php
                }
            }
            ?>
        </ul>
    </div>
</div>