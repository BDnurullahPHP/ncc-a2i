<div class="page-header navbar navbar-fixed-top">
    <div class="page-header-inner ">
        <div class="page-logo">
            <div class="menu-toggler sidebar-toggler"> </div>
        </div>
        <a href="{{ route('home') }}">
            <img style="width: 37px; margin-top: 3px;" src="{{ Helpers::asset('assets/img/logo/logo_ncc.png') }}" alt="NCC" class="logo-default" />

        </a><span style="color: #fff; font-size: 20px; line-height: 1.5;">{{trans('admin.Narayanganj_City_Corporation')}}</span>
        <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse"> </a>
        <div class="top-menu">
            <ul class="nav navbar-nav pull-right">
                <li class="dropdown language-switch">
                    <a title="English" href="{{ route('language_switch', 'en') }}">
                        En
                    </a>
                </li>
                <li class="dropdown language-switch">
                    <a title="Bangla" href="{{ route('language_switch', 'bn') }}">
                        <img src="{{ Helpers::asset('assets/img/flg/bd.png') }}" class="position-left" alt="Ba">
                    </a>
                </li>
                @if( Auth::check() && Entrust::hasRole(['citizen']) )
                    <?php 
                        /*$ctznft = DB::table('sa_notifications')->where('CITIZEN_ID', $user->getCitizen->CITIZEN_ID)
                                        ->orderBy('NOTIFICATION_ID', 'desc')
                                        ->take(5)
                                        ->get();
                        $ctnf_unseen = DB::table('sa_notifications')->where('CITIZEN_ID', $user->getCitizen->CITIZEN_ID)
                                            ->where('IS_VIEW', 0)
                                            ->count();
                        $ctnf_total = DB::table('sa_notifications')->where('CITIZEN_ID', $user->getCitizen->CITIZEN_ID)->count();*/

                    ?>
                    {{--<li class="dropdown dropdown-extended dropdown-notification" id="header_notification_bar">
                        <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                            <i class="icon-bell"></i>
                            <span class="badge badge-default">{{ Helpers::numberConvert($ctnf_unseen) }}</span>
                        </a>
                        <ul class="dropdown-menu">
                            <li class="external">
                                <h3>
                                    <span class="bold">
                                        {{ Helpers::numberConvert($ctnf_total) }}{{ trans('common.the') }}
                                    </span> {{ trans('common.notification') }}
                                </h3>
                                <a href="#">{{ trans('common.all') }}</a>
                            </li>
                            <li>
                                <ul class="dropdown-menu-list scroller" style="height: 250px;" data-handle-color="#637283">
                                    @if(count($ctznft) > 0)
                                        @foreach($ctznft as $t => $cnf)
                                            <li>
                                                <a href="#" class="{{ $cnf->IS_VIEW==0?'unseen':'' }}" data-toggle="modal" data-target="#modal_add_content" data-form="{{ route('notification_view', $cnf->NOTIFICATION_ID) }}">
                                                    <span class="time">{{ Helpers::getTime($cnf->CREATED_AT) }}</span>
                                                    <span class="details">
                                                    <span class="label label-sm label-icon label-warning">
                                                        <i class="fa fa-bell-o"></i>
                                                    </span>{{ $cnf->TITLE }}</span>
                                                </a>
                                            </li>
                                        @endforeach
                                    @endif
                                </ul>
                            </li>
                        </ul>
                    </li>--}}
                @endif
                <li class="dropdown dropdown-user">
                    <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                        @if( !empty($user->getDetails) && !empty($user->getDetails->IMAGE) && File::exists(public_path()."/upload/user/$image_file") )
                            <img src='{{ Helpers::asset("upload/user/$image_file") }}' class="profile_image img-rounded" alt="User">
                        @else
                            <img src="{{ Helpers::asset('assets/img/logo/profile_icon.jpg') }}" class="profile_image img-rounded" alt="User">
                        @endif
                        
                        @if( Auth::check() )
                            <span class="username username-hide-on-mobile"> {{ $user->FULL_NAME }} </span>
                        @else
                            <span class="username username-hide-on-mobile"> {{ $user->APPLICANT_NAME }} </span>
                        @endif
                        <i class="fa fa-angle-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-menu-default">
                        @if( Auth::check() )
                            <li>
                                <a href="{{ route('user_profile', $user->USER_ID) }}"> <i class="icon-user"></i> {{ trans('user.my_profile') }} </a>
                            </li>
                        @endif
                        <li>
                            <a href="{{ route('get_logout') }}"> <i class="icon-key"></i> {{ trans('common.logout') }} </a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</div>