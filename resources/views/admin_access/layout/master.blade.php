<!DOCTYPE html>
{{-- 
Author: ATI Limited
Website: http://www.atilimited.net/
Contact: info@atilimited.net
--}}
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>{{ $title or 'NCC | Automated Property Management System' }}</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <meta name="description" content="Automated Property Management System for NCC"/>
        <meta name="author" content="ATI Limited"/>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <link rel="shortcut icon" href="{{ Helpers::asset('assets/img/logo/icon_ncc.png') }}">
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="{{ Helpers::asset('assets/plugins/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ Helpers::asset('assets/plugins/simple-line-icons/simple-line-icons.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ Helpers::asset('assets/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ Helpers::asset('assets/plugins/sweetalert-master/dist/sweetalert.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ Helpers::asset('assets/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ Helpers::asset('assets/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ Helpers::asset('assets/plugins/jquery-ui-1.12.0/jquery-ui.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ Helpers::asset('assets/plugins/redactor/redactor.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ Helpers::asset('assets/plugins/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ Helpers::asset('assets/plugins/select2/css/select2-bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ Helpers::asset('assets/global/css/components.min.css') }}" rel="stylesheet" id="style_components" type="text/css" />
        <link href="{{ Helpers::asset('assets/global/css/plugins.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ Helpers::asset('assets/layouts/layout/css/layout.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ Helpers::asset('assets/layouts/layout/css/themes/darkblue.min.css') }}" rel="stylesheet" type="text/css" id="style_color" />
        <link href="{{ Helpers::asset('assets/layouts/layout/css/custom.min.css') }}" rel="stylesheet" type="text/css" />
        
        <script src="{{ Helpers::asset('assets/js/jquery-2.1.1.js') }}" type="text/javascript"></script>
        <!-- END THEME LAYOUT STYLES -->

        <!-- BEGIN PAGE SPECIFIC LEVEL PLUGINS -->
        @yield('css_file')
        <!-- END PAGE SPECIFIC LEVEL PLUGINS -->
        <link href="{{ Helpers::asset('assets/admin/css/common.css') }}" rel="stylesheet" type="text/css" />
        
        @yield('style')

    </head>
    <!-- END HEAD -->

    <body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white body_purple">
        <?php 
            if (Auth::check()) {
                $user = Auth::user()->load(['getDetails', 'getCitizen']);
                if ( !empty($user->getDetails) ) {
                    $image_file = $user->getDetails->IMAGE.'.'.$user->getDetails->CONTENT_TYPE;
                } else {
                    $image_file = '';
                }
            } else {
                $user = Auth::user('user')->load('getApplication');
            }
        ?>
        <!-- BEGIN HEADER -->
        @include('admin_access.layout.header')
        <!-- END HEADER -->
        <div class="clearfix"> </div>
        <!-- END HEADER & CONTENT DIVIDER -->
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <!-- BEGIN SIDEBAR -->
            @include('admin_access.layout.sidebar')
            <!-- END SIDEBAR -->
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    @yield('content')
                </div>
            </div>
            <!-- END CONTENT -->
        </div>
        <!-- END CONTAINER -->
        <!-- BEGIN FOOTER -->
        <div class="page-footer">
            @include('admin_access.layout.footer')
        </div>
        <!-- END FOOTER -->
        
        <!--=======Began Page common modal==========-->
        <div class="modal fade" id="modal_add_content" tabindex="-1" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h4 class="modal-title">{{ trans('common.add_data') }}</h4>
                    </div>
                    <div class="modal-body">
                        <img src="{{ Helpers::asset('assets/img/loader.gif') }}" alt="" class="loader center-block">
                        <div id="body-content">
                            
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" data-dismiss="modal">{{ trans('common.modal_close') }}</button>
                    </div>
                </div>
            </div>
        </div>
        <!--=======End Page common modal==========-->

        <!--=======Began Page big modal==========-->
        <div class="modal fade" id="modal_big_content" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h4 class="modal-title">{{ trans('common.add_data') }}</h4>
                    </div>
                    <div class="modal-body">
                        <img src="{{ Helpers::asset('assets/img/loader.gif') }}" alt="" class="loader center-block">
                        <div id="body-content">
                            
                        </div>
                    </div>
                    <div class="modal-footer">
                        <!-- <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button> -->
                    </div>
                </div>
            </div>
        </div>
        <!--=======End Page big modal==========-->

        <!-- BEGIN CORE PLUGINS -->
        <script type="text/javascript">
            var baseUrl = "{{ Config::get('app.url') }}";
        </script>

        <script src="{{ Helpers::asset('assets/plugins/bootstrap/js/bootstrap.min.js') }}" type="text/javascript"></script>
        <script src="{{ Helpers::asset('assets/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js') }}" type="text/javascript"></script>
        <script src="{{ Helpers::asset('assets/plugins/sweetalert-master/dist/sweetalert.min.js') }}" type="text/javascript"></script>
        <script src="{{ Helpers::asset('assets/plugins/datatables/datatables.min.js') }}" type="text/javascript"></script>
        <script src="{{ Helpers::asset('assets/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') }}" type="text/javascript"></script>
        <script src="{{ Helpers::asset('assets/plugins/jquery-validation/dist/jquery.validate.min.js') }}" type="text/javascript"></script>
        <script src="{{ Helpers::asset('assets/plugins/jquery-validation/dist/additional-methods.min.js') }}" type="text/javascript"></script>
        <script src="{{ Helpers::asset('assets/plugins/jquery-validation/dist/localization/messages_bn_BD.min.js') }}" type="text/javascript"></script>
        <script src="{{ Helpers::asset('assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js') }}" type="text/javascript"></script>
        <script src="{{ Helpers::asset('assets/plugins/jquery-ui-1.12.0/jquery-ui.min.js') }}" type="text/javascript"></script>
        <script src="{{ Helpers::asset('assets/plugins/redactor/redactor.min.js') }}" type="text/javascript"></script>
        <script src="{{ Helpers::asset('assets/plugins/select2/js/select2.full.min.js') }}" type="text/javascript"></script>
        <script src="{{ Helpers::asset('assets/global/scripts/app.min.js') }}" type="text/javascript"></script>
        <script src="{{ Helpers::asset('assets/layouts/layout/scripts/layout.min.js') }}" type="text/javascript"></script>
        <script src="{{ Helpers::asset('assets/layouts/global/scripts/quick-sidebar.min.js') }}" type="text/javascript"></script>
        <!-- BEGIN THEME LAYOUT SCRIPTS -->

        <!-- BEGIN PAGE SPECIFIC LEVEL SCRIPTS -->
        @yield('js_file')
        <!-- END PAGE SPECIFIC LEVEL SCRIPTS -->
        <script src="{{ Helpers::asset('assets/admin/js/common.js') }}" type="text/javascript"></script>

        @yield('script')

    </body>
</html>