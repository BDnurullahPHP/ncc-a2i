{!! Form::open(array('url' => '#', 'id' => 'frmBankInfo', 'class' => 'form-horizontal', 'method' => 'post')) !!}
<!--=============Title for modal===============-->
    <div class="modal_top_title">{{ trans('bank.bank_title') }}</div>
    <!--===========End title for modal=============-->

<!--========Show form post message=============-->
<div class="form-group" style="margin-bottom: 0px;">
    <div class="col-sm-5 col-sm-offset-3 form_messsage">

    </div>
</div>
<!--========End form post message=============-->

<div class="land_block_box land_box_gray">
    <div class="form-group">
        <div class="col-md-3">
            {!! Form::label('txtBankName', trans('bank.bank_name')) !!}<span style="color: red">*</span>
        </div>
        <div class="col-md-6">
        {!! Form::text('txtBankName', '', array('id' => 'txtBankName', 'class' => 'form-control form-required', 'placeholder' => trans('bank.bank_name_ex'))) !!}
         </div>
    </div>
</div>
<div class="form-group">
    <div class="col-md-3">
            {!! Form::label('cmbParentBank', trans('bank.parent_bank')) !!}
        </div>
        <span class="help_tooltip" data-toggle="tooltip" data-placement="top" title="{{ trans('bank.bank_name_ex') }}"><i class="fa fa-question-circle" aria-hidden="true"></i></span>
        <div class="col-md-5">
            {!! Form::select('cmbParentBank', $bank_list, array(Input::old('cmbParentBank')), array('id' => 'txtBankName', 'class' => 'form-control', 'placeholder' => trans('bank.select_bank_name'))) !!}
        </div>
    </div>
    <div class="form-group">
        <div class="col-md-3">
            {!! Form::label('txtBankAddress', trans('bank.bank_address')) !!}<span style="color: red">*</span>
        </div>
        <div class="col-md-9">
            {!! Form::textarea('txtBankAddress', '', array('id' => 'txtBankAddress', 'class' => 'form-control form-required reactor_basic', 'placeholder' => trans('bank.bank_address_ex'))) !!}
        </div>
    </div><div class="hr-line-dashed"></div>
      <div class="form-group">
        <label for="IS_ACTIVE" class="col-sm-3 control-label">{{ trans('common.is_active') }}</label>
        <div class="col-sm-7">
            <label class="control-label">
                {!! Form::checkbox('IS_ACTIVE', '1', true, ['class'=>'status']) !!}
            </label>
        </div>
    </div>
</div>
 <div class="form-group">
        <div class="col-lg-offset-3 col-lg-10">
            <span class="modal_msg pull-left"></span>
                <button type="button" class="btn btn-default btn-sm reset_form">{{ trans('common.form_reset') }}</button>
                <button type="submit" data-action="{{ route('create_bank') }}" class="btn btn-success btn-sm form_submit">{{ trans('common.form_submit') }}</button>
            <span class="loadingImg"></span>
        </div>
    </div>

{!! Form::close() !!}
