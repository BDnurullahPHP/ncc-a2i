{!! Form::model($bank_list_s, array('url' => '#', 'id'=>'land_category_form', 'class'=>'form-horizontal', 'method' => 'post')) !!}
<!--=============Title for modal===============-->
    <div class="modal_top_title">{{ trans('bank.bank_title_edit') }}</div>
    <!--===========End title for modal=============-->

<!--========Show form post message=============-->
<div class="form-group" style="margin-bottom: 0px;">
    <div class="col-sm-5 col-sm-offset-3 form_messsage">

    </div>
</div>
<!--========End form post message=============-->

<div class="land_block_box land_box_gray">
    <div class="form-group">
        <div class="col-md-3">
            {!! Form::label('BANK_NAME', trans('bank.bank_name')) !!}<span style="color: red">*</span>
        </div>
        <div class="col-md-5">
            {!! Form::text('BANK_NAME', null, array('id' => 'BANK_NAME', 'class' => 'form-control form-required', 'placeholder' => trans('bank.bank_name_ex'))) !!}
           
        </div>
    </div>
  {{--   <div class="form-group">
        <div class="col-md-3">
            {!! Form::label('cmbParentBank', trans('bank.parent_bank')) !!}
        </div>
        <div class="col-md-8">
            {!! Form::select('cmbParentBank', $bank_list_c, array(Input::old('cmbParentBank')), array('id' => 'txtBankName', 'class' => 'form-control', 'placeholder' => trans('bank.select_bank_name'))) !!}
        </div>
    </div> --}}

  

    <div class="form-group">
        <label for="B_PARENT_ID" class="col-sm-3">{{ trans('bank.parent_bank') }} </label>
        <span class="help_tooltip" data-toggle="tooltip" data-placement="top" title="{{ trans('bank.bank_name_ex') }}"><i class="fa fa-question-circle" aria-hidden="true"></i></span>
        <div class="col-sm-5">
            <select name="B_PARENT_ID" id="B_PARENT_ID" class="form-control">
                <option value="">{{ trans('common.form_select') }}</option>
                @foreach ($bank_list_c as $cat)
                    <option {{ $cat->BANK_ID==$bank_list_s->B_PARENT_ID?'selected="selected"':'' }} value="{{ $cat->BANK_ID }}">{{ $cat->BANK_NAME }}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="form-group">
        <div class="col-md-3">
            {!! Form::label('ADDRESS', trans('bank.bank_address')) !!} <span style="color: red">*</span>
        </div>
        <div class="col-md-9">
            {!! Form::textarea('ADDRESS', null, array('id' => 'ADDRESS', 'class' => 'form-control form-required reactor_basic', 'placeholder' => trans('bank.enter_bank_address'))) !!}
        </div>
    </div><div class="hr-line-dashed"></div>
      <div class="form-group">
        <label for="IS_ACTIVE" class="col-sm-3 control-label">{{ trans('common.is_active') }}</label>
        <div class="col-sm-7">
            <label class="control-label">
                {!! Form::checkbox('IS_ACTIVE', '1', true, ['class'=>'status']) !!}
            </label>
        </div>
    </div>
</div>
 <div class="form-group">
        <div class="col-lg-offset-3 col-lg-10">
            <span class="modal_msg pull-left"></span>
                <button type="button" class="btn btn-default btn-sm reset_form">{{ trans('common.form_reset') }}</button>
                <button type="submit" data-action="{{ route('bank_update', $bank_list_s->BANK_ID) }}" class="btn btn-success btn-sm form_submit">{{ trans('common.form_submit') }}</button>
            <span class="loadingImg"></span>
        </div>
    </div>

{!! Form::close() !!}
