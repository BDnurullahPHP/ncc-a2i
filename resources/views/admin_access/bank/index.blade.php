@extends('admin_access.layout.master')

@section('style')
<style type="text/css">

</style>
@endsection

@section('content')
<h3>{{ trans('bank.bank_list') }} <small></small>
    @if($access->CREATE == 1)
    <a href="#" data-toggle="modal" data-target="#modal_add_content" class="btn btn-primary btn-sm pull-right" data-form="{{'bank/bank-create-form'}}">{{ trans('common.add_new') }} <i class="fa fa-plus"></i></a>
    @endif
</h3>
<div class="portlet light bordered">
    {{--  <div class="portlet-title">
            <div class="caption">
                
            </div>
            <div class="tools"> </div>
        </div> --}}
    <div class="row">
        <div class="col-md-5 col-sm-6 col-xs-12">
            <div id="lg_form_message"></div>
        </div>
    </div>
    <div class="portlet-body contentArea">

        <table class="table table-striped table-bordered table-hover" data-source="{{ route('bank_data') }}" id="common_table">
            <thead>
                <tr>
                    <th>{{ trans('common.table_sl') }}</th>
                    <th>{{ trans('bank.bank_name') }}</th>
                    <th>{{ trans('bank.parent_bank') }}</th>
                    <th>{{ trans('bank.bank_address') }}</th>
                    <th>{{ trans('common.table_action') }}</th>
                </tr>
            </thead>
            <tbody>

            </tbody>
        </table>

    </div>
</div>
@endsection

@section('script')

<script type="text/javascript">
    $(document).ready(function() {

    });
</script>

@endsection