<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>ইজারার রাজস্ব আদায় এর বিবরণ</title>
    </head>
    <style>
        body {
            font-family: freeserif,sans-serif,vrinda,solaiman-lipi,monospace;
        }
        h1, h2, h3, h4, p, input, th, td{
            font-family: freeserif,sans-serif,vrinda,solaiman-lipi,monospace;
            font-weight: 400;
            white-space: initial;
        }
        .sectionrow
        {
            width: 100%;
            border: 1px ;            
            border-style: solid ;
            font-size: 12px; 
        }
        h4{
            text-align:center; 
        }
        p{
            text-align: justify;
        }
        table, th, td {
            border: .25px solid;
            border-collapse: collapse;
            text-align: center;
        }

    </style>
    <body>
        <h4>
            <bold>নারায়ণগঞ্জ সিটি করপোরেশন</bold>
            <br/>নগর ভবন, ১০ বঙ্গবন্ধু রোড
            <br/>নারায়ণগঞ্জ
            <br/>www.ncc.gov.bd
        </h4>
        <h3>{{trans('lease.revenue_collection_details')}}:-</h3>
        <table style="width:100%">
            <thead>
                <tr>
                    <th>{{trans('lease.sl_no')}}</th>
                    <th>{{trans('lease.mahal_name')}}</th>
                    <th>{{trans('lease.lease_customer_name')}}</th>
                    @foreach($lease_part as $key => $part)
                        @if($part->PARTICULAR_ID == 6)
                            <th>{{trans('lease.lease_value')}}</th>
                        @else
                            <th>{{ $part->PARTICULAR_NAME }}</th>
                        @endif
                    @endforeach
                    <th>মন্তব্য</th>
                </tr>
            </thead>
            <tbody>
                @foreach($report_data as $key=>$value)
                <tr>
                    <td>{{ $key+1 }}</td>
                    <td>{{ $value->getSchedule->getProject->PR_NAME_BN }}</td>
                    <td>
                        @if (!empty($value->TE_APP_ID))
                        {{ $value->getApplicant->APPLICANT_NAME }}
                        @elseif ($value->CITIZEN_ID)
                        {{ $value->getCitizen->getTenderCitizen->FULL_NAME }}
                        @elseif ($value->LEASE_DE_ID)
                        {{ $value->getLeaseDemesne->NAME }}
                        @endif
                    </td>
                    @foreach ($value->voucherSchedule as $index => $vsc)
                        <td>
                            @if ($vsc->PUNIT_PRICE !='')
                                {{ Helpers::numberConvert(number_format($vsc->PUNIT_PRICE, 2)) }}
                            @else
                                {{ '' }}
                            @endif
                        </td>
                    @endforeach
                    @if ($index < 5)
                        @for ($i=1; $i < (5 - $index); $i++)
                            <td>--</td>
                        @endfor
                    @endif
                    <td>--</td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </body>
</html>