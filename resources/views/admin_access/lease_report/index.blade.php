@extends('admin_access.layout.master')

@section('content')

<h3>{{trans('citizen.details_of_the_revenue')}} 
    <small></small>
</h3>
{!! Form::open(array('url' => route('lease_revenue_data'), 'id'=>'search_form', 'method' => 'post')) !!}
<div class="row">
    <div class="col-md-2">
        <div class="form-group">
            <label class="control-label"><strong>{{trans('citizen.c_year')}}</strong></label>
            <select name="year" class="form-control form-search" required="required">
                <option value="">{{trans('citizen.select')}}</option>
                <?php
                $i = 2015;
                for ($i; $i <= date('Y'); $i++) {
                    echo '<option value="' . $i . '">' . Helpers::en2bn($i) . '</option>';
                }
                ?>
            </select>
        </div>
    </div>
    <div class="col-md-2">
        <div class="form-group">
            <label class="control-label"><strong>{{trans('citizen.month')}}</strong></label>
            <select name="month" class="form-control form-search">
                <option value="">{{trans('citizen.select')}} </option>
                <option value="1">{{trans('citizen.january')}}</option>
                <option value="2">{{trans('citizen.february')}}</option>
                <option value="3">{{trans('citizen.march')}}</option>
                <option value="4">{{trans('citizen.april')}}</option>
                <option value="5">{{trans('citizen.may')}}</option>
                <option value="6">{{trans('citizen.jun')}}</option>
                <option value="7">{{trans('citizen.july')}}</option>
                <option value="8">{{trans('citizen.august')}}</option>
                <option value="9">{{trans('citizen.september')}}</option>
                <option value="10">{{trans('citizen.october')}}</option>
                <option value="11">{{trans('citizen.november')}}</option>
                <option value="12">{{trans('citizen.december')}}</option>
            </select>
        </div>
    </div>
    {{-- <div class="col-md-2">
        <div class="form-group">
            <label class="control-label"><strong>{{trans('citizen.mahal_name')}}</strong></label>
            <select class="form-control form-search" name="SCHEDULE_ID" id="SCHEDULE_ID">
                <option value="">{{trans('citizen.select')}}</option>
                 @foreach ($project as $key => $value)
                 <option value="{{ $value->SCHEDULE_ID }}">{{ $value->getProject->PR_NAME_BN }}</option>
@endforeach
</select>
</div>
</div> --}}
<div class="col-md-2">
    <div class="form-group">
        <label class="control-label"><strong>{{trans('citizen.customer_name')}}</strong></label>
        <select class="form-control form-search" name="citizen_name" id="citizen_name">
            <option value="">{{trans('citizen.select')}}</option>
            @foreach($applicant_data as $key => $value)
            <option value="{{ !empty($value->getApplicant)?'applicant/'.$value->getApplicant->TE_APP_ID:'citizen/'.$value->getCitizen->CITIZEN_ID }}">
                @if(!empty($value->getApplicant))
                {{ $value->getApplicant->APPLICANT_NAME }}
                @elseif(!empty($value->getCitizen))
                {{ $value->getCitizen->getTenderCitizen->FULL_NAME }}
                @endif
            </option>
            @endforeach
            {{-- <option value="{{ $value->IS_CITIZEN == 0 ? 'applicant/'.$value->TE_APP_ID : 'citizen/'.$value->CITIZEN_ID }}">{{ $value->IS_CITIZEN == 0 ? $value->getApplicant->APPLICANT_NAME : $value->getCitizen->getTenderCitizen->FULL_NAME }}</option>
            @endforeach --}}
        </select>
    </div>
</div>
<div class="col-md-2">
    <div class="form-group" style="padding-top: 25px; ">
        <label class="control-label"></label><b/>
        @if($access->READ == 1)
        <button type="button" class="btn btn-sm blue search_data_btn"><i class="fa fa-search"></i>{{trans('citizen.details_of_the_revenue')}}</button>
        <a href="{{ route('lease_revenue_data_print') }}" class="btn btn-sm btn-default search_report_btn"><i class="fa fa-print"></i> pdf</a> 
        @endif
    </div>
</div>
</div>
{!! Form::close() !!}
<div class="portlet light bordered" style="display: none;">
    <div class="row">
        <div class="col-md-5 col-sm-6 col-xs-12">
            <div id="lg_form_message"></div>
        </div>
    </div>
    <div class="portlet-body contentArea">
        <table class="table table-striped table-bordered table-hover search_table_data">
            <thead>
                <tr>
                    <th>{{trans('citizen.sl_no')}}</th>
                    <th>{{trans('citizen.mahal_name')}} </th>
                    <th>{{trans('citizen.name_of_the_lease_holder')}}</th>
                    <!-- <th>ইজারার মূল্য</th>
                    <th>ভ্যাট</th>
                    <th>আয়কর</th>
                    <th>সালামী</th>
                    <th>মুক্তিযোদ্ধা<br/>কল্যাণ তহবিল</th> -->
                    @foreach($lease_part as $key => $part)
                    @if($part->PARTICULAR_ID == 6)
                    <th>{{trans('citizen.lease_value')}} </th>
                    @else
                    <th>{{ $part->PARTICULAR_NAME }}</th>
                    @endif
                    @endforeach
                    <!-- <th>মোট টাকা</th>
                    <th>মন্তব্য</th> -->
                </tr>
            </thead>
            <tbody>

            </tbody>
        </table>
    </div>
</div>

@endsection

@section('script')
<script src="{{ Helpers::asset('assets/admin/js/search.js') }}" type="text/javascript"></script>
<script type="text/javascript">
jQuery(document).ready(function($) {
    AdvanceSearch.init();
});
</script>
@endsection