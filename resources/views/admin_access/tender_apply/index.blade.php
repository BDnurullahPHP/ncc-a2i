@extends('admin_access.layout.master')

@section('script')
	
	<script src="{{ Helpers::asset('assets/admin/js/tender_apply.js') }}" type="text/javascript"></script>

@endsection

@section('style')

	<link rel="stylesheet" href="{{ Helpers::asset('assets/admin/css/tender.css') }}">

	<style type="text/css">
		table.datatable_ajax>thead>tr>th{
			text-align: center;
			white-space: inherit;
		}
		#application_content{

		}
		.control-label{
	        text-align: left !important;
	    }
	    .form-control{
	        height: 34px !important;
	        border-radius: 0px;
	    }
	    .input-group-addon{
	        border-radius: 0px;
	    }
	    textarea#address{
	        width: 100%;
	        max-width: 100%;
	        height: auto !important;
	    }
	    #reset_form{
	        margin-right: 10px;
	    }
	    .personal_info{
	       background-color:#e8f6f3;
	       padding: 5px;
	    }
	    .tender_info{
			background-color:#e8f6f3;
			padding: 5px;
	    }
	    .tender_schedule{
			background-color:#e8f6f3;
			padding: 5px;
	    }
	  	.bank_pay_order, .cash_order{
	  		display: none;
	  	}
	  	.validation_error{
	  		color: red;
	  	}
	  	#application_form label{
	  		
	  	}
	  	#application_form p{
	  		margin-top: 0px;
	  		margin-bottom: 5px;
	  	}
	  	#application_form .form-group, 
	  	#application_form .personal_info{
	  		margin-bottom: 10px;
	  	}
	</style>

@endsection

@section('content')

	<h3>{{trans('tender.tender_lease')}} 
    	<small></small>
	</h3>

    <div class="row">
		<div class="col-md-4 col-sm-4 col-xs-12" style="padding:10px 15px;">
			<input type="hidden" class="data_source" value="{{ route('notice_table_select') }}">
            {!! Form::select('tender_category', ['tender' => 'দরপত্র', 'lease' => 'ইজারা'], null, array('class'=>'form-control category_application','placeholder' => trans('common.form_select'))) !!}
		</div>
		<div class="col-md-1">
			<img src="{{ Helpers::asset('assets/img/loader.gif') }}" alt="" class="form_loader">
		</div>
	</div>

	<div id="application_content"></div>

@endsection