{!! Form::open(array('url' => '#', 'id' => 'application_form', 'class' => 'form-horizontal', 'method' => 'post')) !!}
    <!--=============Title for modal===============-->
        <div class="modal_top_title">{{trans('citizen.Application_form')}}</div>
        <!--===========End title for modal=============-->

    <!--========Show form post message=============-->
    <div class="form-group" style="margin-bottom: 0px;">
        <div class="col-sm-5 col-sm-offset-3 form_messsage">

        </div>
    </div>
    <!--========End form post message=============-->
    {!! Form::hidden('SCHEDULE_ID', $schedule_id, array('class'=>'form-control form-value')) !!}
    {!! Form::hidden('TOTAL_AMOUNT', $schedule_price, array('class'=>'form-control form-value')) !!}
    
    {!! Form::hidden('BID_AMOUNT_TEXT', 'Bid amount', array('class'=>'form-control form-value')) !!}
    {!! Form::hidden('BG_AMOUNT_TEXT', 'bg amount', array('class'=>'form-control form-value')) !!}
    {!! Form::hidden('PAYMENT_METHOD', 0,array('class'=>'form-control form-value')) !!}

    <div class="tab-content">
        <h4 style="margin-bottom:15px;">{{trans('citizen.Applicant_Information')}}</h4>
        <div class="row">
            <div class="col-md-3">
                {!! Form::label('FIRST_NAME', trans('tender.tender_applicant_name')) !!} <span style="color: red">*</span>
                {!! Form::text('FIRST_NAME', Input::old('FIRST_NAME'), array('class'=>'form-control form-value', 'placeholder'=>trans('tender.tender_applicant_name_ex'))) !!}
                <p class="validation_error"></p>
            </div>
            <div class="col-md-3">
                {!! Form::label('LAST_NAME', trans('tender.tender_applicant_name_last')) !!} <span style="color: red">*</span>
                {!! Form::text('LAST_NAME', Input::old('LAST_NAME'), array('class'=>'form-control form-value', 'placeholder'=>trans('tender.tender_applicant_name_last_pl'))) !!}
                <p class="validation_error"></p>
            </div>
            <div class="col-md-3">
                {!! Form::label('SPOUSE_NAME', trans('tender.tender_applicant_name_fh')) !!} <span style="color: red">*</span>
                {!! Form::text('SPOUSE_NAME', Input::old('SPOUSE_NAME'), array('class'=>'form-control form-value', 'placeholder'=>trans('tender.tender_applicant_name_fh_ex'))) !!}
                <p class="validation_error"></p>
            </div>
            <div class="col-md-3">
                {!! Form::label('NID', trans('citizen.National_identity_card_birth_registration')) !!} <span style="color: red">*</span>
                {!! Form::text('NID', Input::old('NID'), array('id'=>'applicant_nid', 'class'=>'form-control form-value', 'data-url'=>route('check_unique_nid'), 'placeholder'=>'Ex.2610413965404')) !!}
                <p class="validation_error"></p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3">
                {!! Form::label('division', trans('citizen.division')) !!} <span style="color: red">*</span>
                {!! Form::select('division', $division, array(Input::old('division')), array('class'=>'form-control form-value', 'placeholder' => trans('common.form_select'))) !!}
                <p class="validation_error"></p>
            </div>
            <div class="col-md-3">
                {!! Form::label('district', trans('citizen.District')) !!} <span style="color: red">*</span>
                {!! Form::select('district', [], array(Input::old('district')), array('class'=>'form-control form-value', 'placeholder' => trans('common.form_select'))) !!}
                <p class="validation_error"></p>
            </div>
            <div class="col-md-3">
                {!! Form::label('thana', trans('citizen.Upazila_Thana')) !!} <span style="color: red">*</span>
                {!! Form::select('thana', [], array(Input::old('thana')), array('class'=>'form-control form-value', 'placeholder' => trans('common.form_select'))) !!}
                <p class="validation_error"></p>
            </div>
            <div class="col-md-3">
                {!! Form::label('ROAD_NO', trans('citizen.road_no')) !!}  <span style="color: red">*</span>
                {!! Form::text('ROAD_NO', Input::old('ROAD_NO'), array('rows' =>'1', 'class'=>'form-control form-value', 'placeholder'=>'Ex.রোড নং-১২,ঢাকা-১২১৬')) !!}
                <p class="validation_error"></p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3">
                {!! Form::label('HOLDING_NO', trans('citizen.holding_no')) !!}<span style="color: red">*</span>
                {!! Form::text('HOLDING_NO', null, array('rows' =>'1', 'class'=>'form-control form-value', 'placeholder'=>'Ex.হোল্ডিং নং-১২,ঢাকা-১২১৬')) !!}
                <p class="validation_error"></p>
            </div>
            <div class="col-md-3">
                {!! Form::label('APPLICANT_PHONE', trans('citizen.mobile_number')) !!} <span style="color: red">*</span>
                {!! Form::text('APPLICANT_PHONE', Input::old('APPLICANT_PHONE'), array('id'=>'applicant_phone_no', 'class'=>'form-control form-value form-mobile', 'placeholder'=>'Ex.০১৬৮০১২০৫৩৯', 'data-url'=>route('check_unique_phone'))) !!}
                <p class="validation_error"></p>
            </div>
            <div class="col-md-3">
                {!! Form::label('APPLICANT_EMAIL', trans('citizen.email')) !!}
                {!! Form::email('APPLICANT_EMAIL', Input::old('APPLICANT_EMAIL'), array('id'=>'applicant_email_id', 'class'=>'form-control form-value form-email', 'placeholder'=>'Ex.user@gmail.com', 'data-url'=>route('check_unique_email'))) !!}
                <p class="validation_error"></p>
            </div>
            <div class="col-md-3">
                {!! Form::label('TE_APP_PASSWORD', trans('citizen.password')) !!} <span style="color: red">*</span>
                {!! Form::password('TE_APP_PASSWORD', array('class'=>'form-control form-password form-value', 'placeholder'=>'Ex.@2345%*+A')) !!}
                <p class="validation_error"></p>
            </div>
        </div>
        <hr>
         <h4 style="margin-bottom:15px;">@if($type=='lease') ইজারার @else দরপত্রের @endif বিবরণ</h4>
        <div class="row">
            <div class="col-md-4">
                {!! Form::label('BID_AMOUNT', trans('citizen.Bidding_rate_by_the_borrower')) !!} <span style="color: red">*</span>
                {!! Form::text('BID_AMOUNT', Input::old('BID_AMOUNT'), array('class'=>'form-control form-value', 'placeholder'=>'Ex.১২০০০')) !!}
                <p class="validation_error">{{ $errors->first('BID_AMOUNT') }}</p>
            </div>
            <div class="col-md-4">
                @if($type=='lease')
                    {!! Form::label('BG_AMOUNT', trans('citizen.The amount_of_winding')) !!} <span style="color: red">*</span>
                @else
                    {!! Form::label('BG_AMOUNT', trans('citizen.The_amount_of_security')) !!} <span style="color: red">*</span>
                @endif
                {!! Form::text('BG_AMOUNT', Input::old('BG_AMOUNT'), array('class'=>'form-control form-value', 'placeholder'=>'Ex.১২০০০')) !!}
                <p class="validation_error">{{ $errors->first('BG_AMOUNT') }}</p>
            </div>
<!--            <div class="col-md-4">
                {!! Form::label('PAYMENT_METHOD', 'জামানতের মূল্য পরিশোধের ধরন: ') !!}
                {!! Form::select('PAYMENT_METHOD', [1=>'ব্যাংক পে-অর্ডার', 3=>'ক্যাশ'], array(Input::old('PAYMENT_METHOD')), array('id'=>'payment_method', 'class'=>'form-control form-value', 'placeholder' => trans('common.form_select'))) !!}
                <p class="validation_error">{{ $errors->first('PAYMENT_METHOD') }}</p>
            </div>-->
        </div>
        <div class="row bank_pay_order">
            <div class="col-md-4">
                {!! Form::label('B_DRAFT_NO', 'পে-অর্ডার নাম্বার:') !!} <span style="color: red">*</span>
                {!! Form::text('B_DRAFT_NO', Input::old('B_DRAFT_NO'), array('class'=>'form-control form-value', 'placeholder'=>'পে-অর্ডার নাম্বার লিখুন')) !!}
                <p class="validation_error">{{ $errors->first('B_DRAFT_NO') }}</p>
            </div>
            <div class="col-md-4">
                {!! Form::label('BANK_ID', 'পে-অর্ডার ব্যাংকের নাম:') !!} <span style="color: red">*</span>
                {!! Form::select('BANK_ID', $bank_list, array(Input::old('BANK_ID')), array('id'=>'bank_selection', 'class'=>'form-control form-value', 'placeholder' => trans('common.form_select'))) !!}
                <p class="validation_error">{{ $errors->first('BANK_ID') }}</p>
            </div>
            <div class="col-md-4">
                {!! Form::label('BRANCH_ID', 'ব্যাংকের শাখার নাম:') !!}
                {!! Form::select('BRANCH_ID', [], array(Input::old('BRANCH_ID')), array('id'=>'branch_selection', 'class'=>'form-control form-value', 'placeholder' => trans('common.form_select'))) !!}
                <p class="validation_error">{{ $errors->first('BRANCH_ID') }}</p>
            </div>
        </div>
        <div class="row bank_pay_order">
            <div class="col-md-4">
                {!! Form::label('B_DRAFT_DATE', 'পে-অর্ডার তারিখ:') !!} <span style="color: red">*</span>
                <div class="input-group">
                    {!! Form::text('B_DRAFT_DATE', Input::old('B_DRAFT_DATE'), array('class'=>'form-control date_picker_default form-value', 'placeholder'=>'Ex.'.date('d-m-Y'))) !!}
                    <div class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </div>
                </div>
                <div id="error_msg1"></div>
                <p class="validation_error">{{ $errors->first('B_DRAFT_DATE') }}</p>
            </div>
            <div class="col-md-4">
                {!! Form::label('ATTACHMENT', 'পে-অর্ডার সংযুক্তি:') !!} <span style="color: red">*</span>
                <div class="input-group">
                    {!! Form::file('ATTACHMENT', array('class' => 'form-control form-file')) !!}
                </div>
                <p class="validation_error">{{ $errors->first('ATTACHMENT') }}</p>
            </div>
        </div>
        <div class="row cash_order">
            <div class="col-md-4">
                {!! Form::label('CASH_DATE', 'তারিখ:') !!} <span style="color: red">*</span>
                <div class="input-group">
                    {!! Form::text('CASH_DATE', Input::old('CASH_DATE'), array('class'=>'form-control date_picker_default form-value', 'placeholder'=>'Ex.'.date('d-m-Y'))) !!}
                    <div class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </div>
                </div>
                <div id="error_msg2"></div>
                <p class="validation_error">{{ $errors->first('CASH_DATE') }}</p>
            </div>
            <div class="col-md-4">
                {!! Form::label('CASH_ATTACHMENT', 'সংযুক্তি:') !!} <span style="color: red">*</span>
                <div class="input-group">
                    {!! Form::file('CASH_ATTACHMENT', array('class' => 'form-control form-file')) !!}
                </div>
                <p class="validation_error">{{ $errors->first('CASH_ATTACHMENT') }}</p>
            </div>
        </div>
    </div>

    <div class="form-group" style="padding-top:20px;">
        <div class="col-md-12">
            <button type="submit" data-action="{{ route('tender_apply_data', [$schedule_id, $type]) }}" class="btn btn-success btn-sm pull-right tender_apply_submit">{{ trans('common.form_submit') }}</button>
            <button type="button" class="btn btn-default btn-sm reset_form pull-right" style="margin-right: 5px;">{{ trans('common.form_reset') }}</button>
        </div>
    </div>

{!! Form::close() !!}
