<div class="portlet light bordered">
	<div class="portlet-title hide">
		<div class="caption">

		</div>
		<div class="tools"> </div>
	</div>
	<div class="row">
		<div class="col-md-5 col-sm-6 col-xs-12">
			<div id="lg_form_message"></div>
		</div>
	</div>
	<div class="portlet-body contentArea">
		@if($category == 'lease')
		<table class="table table-striped table-bordered table-hover datatable_ajax" data-source="{{ route('notice_table_data') }}">
			<thead>
				<tr>
					<th>{{ trans('common.table_sl') }}</th>
					<th>{{trans('tender.round_no')}} </th>
					<th>{{trans('tender.name_of_the_syarat_maha')}}</th>
					<th>{{trans('tender.mahal_location')}}</th>
					<th>{{trans('tender.the_amount_of_winding_suggested_prices_%')}}

</th>
					<th>{{trans('tender.tender_price_non_refundable_mony')}}</th>
					<th>{{trans('tender.details')}}</th>
					<th>{{ trans('common.table_action') }}</th>
				</tr>
			</thead>
			<tbody>

			</tbody>
		</table>
		@else
		<table class="table table-striped table-bordered table-hover datatable_ajax" data-source="{{ route('notice_table_data') }}">
			<thead>
				<tr>
					<th>{{ trans('common.table_sl') }}</th>
					<th>{{{trans('tender.class')}}} </th>
					<th>{{{trans('tender.market_name')}}} </th>
					<th>{{{trans('tender.Type')}}} </th>
					<th>{{{trans('tender.measurement_square_feet')}}}</th>
					<th>{{{trans('tender.lowest_salami')}}} Lowest Salami </th>
					<th>{{{trans('tender.every_square_foot_monthly_rend')}}} </th>
					<th>{{{trans('tender.schedure_price_each_non_refndable')}}} 

						</th>
					<th>{{{trans('tender.amount_of_securit')}}}

						 </th>
					<th>{{ trans('common.table_action') }}</th>
				</tr>
			</thead>
			<tbody>

			</tbody>
		</table>
		@endif
	</div>
</div>