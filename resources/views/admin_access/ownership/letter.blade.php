{!! Form::open(array('url' => '#', 'id'=>'ownership_letter', 'class'=>'form-horizontal', 'method' => 'post')) !!}

<!--=============Title for modal===============-->
<div class="modal_top_title">{{ $type==1?'মালিকানা পরিবর্তন':'মালিকানা বাতিল' }}</div>
<!--===========End title for modal=============-->

<!--========Show form post message=============-->
<div class="form-group">
    <div class="col-sm-6 form_messsage" style="padding-top: 15px;">

    </div>
</div>
<!--========End form post message=============-->
<div class="tabbable-custom">
    <div class="tab-content">
        @if($type == 1)
            <h4 style="margin-bottom:15px;">নতুন মালিকানার তথ্য</h4>
            <div class="row">
                <div class="col-md-3">
                    {!! Form::label('FIRST_NAME', trans('tender.tender_applicant_name')) !!} <span style="color: red">*</span>
                    {!! Form::text('FIRST_NAME', Input::old('FIRST_NAME'), array('class'=>'form-control form-required', 'placeholder'=>trans('tender.tender_applicant_name_ex'))) !!}
                    <p class="validation_error"></p>
                </div>
                <div class="col-md-3">
                    {!! Form::label('LAST_NAME', trans('tender.tender_applicant_name_last')) !!} <span style="color: red">*</span>
                    {!! Form::text('LAST_NAME', Input::old('LAST_NAME'), array('class'=>'form-control form-required', 'placeholder'=>trans('tender.tender_applicant_name_last_pl'))) !!}
                    <p class="validation_error"></p>
                </div>
                <div class="col-md-3">
                    {!! Form::label('SPOUSE_NAME', trans('tender.tender_applicant_name_fh')) !!} <span style="color: red">*</span>
                    {!! Form::text('SPOUSE_NAME', Input::old('SPOUSE_NAME'), array('class'=>'form-control form-required', 'placeholder'=>trans('tender.tender_applicant_name_fh_ex'))) !!}
                    <p class="validation_error"></p>
                </div>
                <div class="col-md-3">
                    {!! Form::label('NID', 'জাতীয় পরিচয়পত্র / জন্মনিবন্ধন:') !!} <span style="color: red">*</span>
                    {!! Form::text('NID', Input::old('NID'), array('id'=>'applicant_nid', 'class'=>'form-control form-required', 'data-url'=>route('check_unique_nid'), 'placeholder'=>'Ex.2610413965404')) !!}
                    <p class="validation_error"></p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3">
                    {!! Form::label('division', 'বিভাগ:') !!} <span style="color: red">*</span>
                    {!! Form::select('division', $division, array(Input::old('division')), array('class'=>'form-control form-required', 'placeholder' => trans('common.form_select'))) !!}
                    <p class="validation_error"></p>
                </div>
                <div class="col-md-3">
                    {!! Form::label('district', 'জেলা:') !!} <span style="color: red">*</span>
                    {!! Form::select('district', [], array(Input::old('district')), array('class'=>'form-control form-required', 'placeholder' => trans('common.form_select'))) !!}
                    <p class="validation_error"></p>
                </div>
                <div class="col-md-3">
                    {!! Form::label('thana', 'উপজেলা/থানা:') !!} <span style="color: red">*</span>
                    {!! Form::select('thana', [], array(Input::old('thana')), array('class'=>'form-control form-required', 'placeholder' => trans('common.form_select'))) !!}
                    <p class="validation_error"></p>
                </div>
                <div class="col-md-3">
                    {!! Form::label('ROAD_NO', 'রোড নং:') !!}  <span style="color: red">*</span>
                    {!! Form::text('ROAD_NO', Input::old('ROAD_NO'), array('rows' =>'1', 'class'=>'form-control form-required', 'placeholder'=>'Ex.রোড নং-১২,ঢাকা-১২১৬')) !!}
                    <p class="validation_error"></p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3">
                    {!! Form::label('HOLDING_NO', 'হোল্ডিং নং:') !!}<span style="color: red">*</span>
                    {!! Form::text('HOLDING_NO', null, array('rows' =>'1', 'class'=>'form-control form-required', 'placeholder'=>'Ex.হোল্ডিং নং-১২,ঢাকা-১২১৬')) !!}
                    <p class="validation_error"></p>
                </div>
                <div class="col-md-3">
                    {!! Form::label('APPLICANT_PHONE', 'মোবাইল নাম্বার:') !!} <span style="color: red">*</span>
                    {!! Form::text('APPLICANT_PHONE', Input::old('APPLICANT_PHONE'), array('id'=>'applicant_phone_no', 'class'=>'form-control form-mobile', 'placeholder'=>'Ex.০১৬৮০১২০৫৩৯', 'data-url'=>route('check_unique_phone'))) !!}
                    <p class="validation_error"></p>
                </div>
                <div class="col-md-3">
                    {!! Form::label('APPLICANT_EMAIL', 'ইমেইল:') !!}
                    {!! Form::email('APPLICANT_EMAIL', Input::old('APPLICANT_EMAIL'), array('id'=>'applicant_email_id', 'class'=>'form-control form-email', 'placeholder'=>'Ex.user@gmail.com', 'data-url'=>route('check_unique_email'))) !!}
                    <p class="validation_error"></p>
                </div>
                <div class="col-md-3">
                    {!! Form::label('TE_APP_PASSWORD', 'পাসওয়ার্ড :') !!} <span style="color: red">*</span>
                    {!! Form::password('TE_APP_PASSWORD', array('class'=>'form-control form-password', 'placeholder'=>'Ex.@2345%*+A')) !!}
                    <p class="validation_error"></p>
                </div>
            </div>
            <hr>

        @endif
        <div class="tab-pane active">
            @if($type != 1)
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="col-md-4">
                            <label>সম্পত্তির মূল্য&#40;টাকা&#41;</label>
                            <input type="text" class="form-control form-required" name="property_price" id="property_price" value="{{ $property_price }}" readonly="readonly">
                        </div>
                        <div class="col-md-4">
                            <label>কর্তনের হার&#40;শতকরা&#41;</label>
                            <input type="text" class="form-control form-required" name="cutting_rate" id="cutting_rate" value="5" >
                        </div>
                        <div class="col-md-4">
                            <label>ফেরতযোগ্য&#40;টাকা&#41;</label>
                            <input type="text" class="form-control form-required" name="returnable_amount" id="returnable_amount" value="{{ ($property_price*5)/100 }}" >
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
                <hr>
            @else
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="col-md-4">
                            <label>সম্পত্তির মূল্য&#40;টাকা&#41;</label>
                            <input type="text" class="form-control form-required" name="property_price" id="property_price" value="{{ $property_price }}" readonly="readonly">
                        </div>
                        <div class="col-md-4">
                            <label>মালিকানা পরিবর্তন ফি&#40;শতকরা&#41;</label>
                            <input type="text" class="form-control form-required" name="cutting_rate" id="cutting_rate" value="5" >
                        </div>
                        <div class="col-md-4">
                            <label>ফেরতযোগ্য&#40;টাকা&#41;</label>
                            <input type="text" class="form-control form-required" name="returnable_amount" id="returnable_amount" value="{{ ($property_price*5)/100 }}" >
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
                <hr>
            @endif
            <div class="col-md-12">
                <div class="form-group">
                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="col-md-4">
                                <label>স্মারক নং- </label><span class="required_field">*</span>
                            </div>
                            <div class="col-md-8">
                                <input type="text" class="form-control form-required" name="sharok_no" id="sharok_no_id" value="" placeholder="৪৬.৪৪৬৭০০.১৪.১৮(৮).১৬/">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="col-md-4">
                                <label>তারিখঃ </label><span class="required_field">*</span>
                            </div>
                            <div class="col-md-8">
                                <input type="text" class="form-control form-required date_picker" name="sharok_date" id="sharok_date_id" value="{{ date('d-m-Y') }}" placeholder="Ex.20-07-2016">
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-12">
                <div class="form-group">
                    <div class="col-md-2">
                        <label>বিষয়ঃ </label><span class="required_field">*</span>
                    </div>
                    <div class="col-md-8">
                        <input type="text" class="form-control form-required" name="letter_subject" value="{{ $type==1?'মালিকানা পরিবর্তন':'মালিকানা বাতিল' }}" id="letter_subject_id" placeholder="বিষয়">
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group">
                    <textarea class="form-control reactor_basic form-required" name="letter_body" id="letter_body_id">
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; উপর্যুক্ত বিষয়ে জানানো যাচ্ছে যে, আপনার অধীনে থাকা নারায়ণগঞ্জ সিটি কর্পোরেশনাধীন পদ্ম সিটি প্লাজা-১ এর ১/এ/৩০০ নং {{ $type==1?'দোকানটি উল্লেখিত বাক্তির কাছে হস্তান্তর':'দোকানটির মালিকানা বাতিল' }} করা হল।
                    </textarea>
                </div>
            </div>

            <div class="col-md-12">
                <div class="form-group">
                    <div class="col-md-6">
                        <p style="text-align: left">
                            জনাব {{ $user_type == 'applicant' ? $applicant->APPLICANT_NAME : $applicant->getTenderCitizen->FULL_NAME }}
                            <br/>পিতা/স্বামী- {{ $applicant->SPOUSE_NAME }}
                            <br/>বাসা নং- {{ $applicant->HOLDING_NO }}, রাস্তা- {{ $applicant->ROAD_NO }}
                            <br/>থানা- {{ $applicent_thana }}, জেলা- {{ $applicent_district }}
                        </p>
                    </div>
                    <div class="col-md-6">
                        <textarea class="form-control reactor_basic form-required" name="officer_info" id="officer_info_id">
                            &#40;{{ $officer->NAME }}&#41;
                            &#40;{{ $officer->DESIGNATION }}&#41;
                            &#40;{{ $officer->OFFICE }}&#41;
                            &#40;{{ $officer->PHONE }}&#41;
                            &#40;{{ $officer->EMAIL }}&#41;
                        </textarea>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-12" style="padding-top: 15px;">
                    <button style="margin-left: 5px;" type="submit" data-action="{{ route('user_ownership_save', [$user_id, $user_type, $schedule_id, $type]) }}" data-print="{{ route('user_ownership_print', [$user_id, $user_type, $schedule_id, $type]) }}" class="btn btn-success pull-right btn-sm property_ownership_save">{{ trans('common.form_submit') }}</button>
                </div>
            </div>
        </div>
    </div>
</div>
{!! Form::close() !!}