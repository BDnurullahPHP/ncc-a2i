@extends('admin_access.layout.master')

@section('content')

<h3>{{ trans('citizen.ownership_change_or_cancel') }}
    <small></small>
</h3>
<div id="info_msg_pring"></div>
{!! Form::open(array('url' => route('user_ownership_info'), 'id'=>'search_form', 'method' => 'post')) !!}
    <input type="hidden" id="user_tender_schedule_url" value="{{ route('user_tender_schedule') }}">
    <div class="row">
        <div class="col-md-2">
            <div class="form-group">
                <label class="control-label"><strong>{{trans('account.client_mobile')}}</strong></label>
                {!! Form::text('MOBILE_NO', null, array('id' => 'mobile_no', 'class'=>'form-control form-search', 'placeholder'=>'01XXXXXXXXX')) !!}
            </div>
        </div>
        <div class="col-md-2">
            <div class="form-group">
                <label class="control-label"><strong>{{trans('account.paper_name')}}</strong></label>
                {!! Form::select('CATEGORY', $category, null, array('id'=>'category', 'class'=>'form-control form-search', 'placeholder' => trans('common.form_select'))) !!}
            </div>
        </div>
        <div class="col-md-2">
            <div class="form-group">
                <label class="control-label"><strong>{{trans('account.tender')}}</strong></label>
                {!! Form::select('SCHEDULE_ID', [], null, array('id'=>'tender_schedule', 'class'=>'form-control form-search', 'placeholder' => trans('common.form_select'))) !!}
            </div>
        </div>
        <div class="col-md-2">
            <div class="form-group">
                <label class="control-label"><strong>{{trans('account.type')}}</strong></label>
                {!! Form::select('SERVICE_TYPE', [1=>'মালিকানা পরিবর্তন', 2=>'মালিকানা বাতিল'], null, array('id'=>'type', 'class'=>'form-control form-search', 'placeholder' => trans('common.form_select'))) !!}
            </div>
        </div>
        <div class="col-md-2">
            <div class="form-group" style="padding-top: 25px; ">
                @if($access->READ == 1)
                <button type="button" class="btn btn-sm blue search_citizen_property"><i class="fa fa-search"></i> সার্চ</button>
                @endif
                <img src="{{ Helpers::asset('assets/img/loader.gif') }}" alt="" class="form_loader" style="margin-right: 20px">
            </div>
        </div>
    </div>
{!! Form::close() !!}
<div class="portlet light bordered" style="display: none;">
    <div class="row">
        <div class="col-md-5 col-sm-6 col-xs-12">
            <div id="lg_form_message"></div>
        </div>
    </div>
    <div class="portlet-body contentArea">
        
    </div>
</div>

@endsection

@section('style')
    <style type="text/css">
        p.validation_error{
            margin: 5px 0px;
        }
    </style>
@endsection

@section('script')
    <script src="{{ Helpers::asset('assets/portal/js/address.js') }}" type="text/javascript"></script>
    <script src="{{ Helpers::asset('assets/admin/js/search.js') }}" type="text/javascript"></script>
    <script type="text/javascript">
        jQuery(document).ready(function($) {
            MoneyCollection.init();
            OwnershipChange.init();
        });
    </script>
@endsection