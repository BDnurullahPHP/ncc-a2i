{!! Form::open(array('url' => '#', 'id'=>'demesne_form', 'class'=>'form-horizontal', 'method' => 'post')) !!}

<!--=============Title for modal===============-->
<div class="modal_top_title">{{ trans('lease.demesne_title') }}</div>
<!--===========End title for modal=============-->

<!--========Show form post message=============-->
<div class="form-group">
    <div class="col-sm-6 form_messsage">

    </div>
</div>
<!--========End form post message=============-->

<div class="form-group">
    <div class="col-md-3">
        {!! Form::label('SCHEDULE_ID', trans('lease.mahal_bn_name')) !!} <span class="required_field">*</span>
    </div>
    <div class="col-md-6">
        <select class="form-control form-required" id="schedule_id" name="SCHEDULE_ID">
            <option value="">{{ trans('common.form_select') }}</option>
            @foreach ($schedule as $value)
            <option value="{{ $value->SCHEDULE_ID }}">{{ $value->getSchedule->getProject->PR_NAME_BN }}</option>
            @endforeach
        </select>
        <div id="error_msg1"></div>
    </div>
</div>
<div class="form-group">
    <div class="col-md-12" id="demesne_info">
    </div>
</div>

<div class="form-group">
    <div class="col-md-3">
        {!! Form::label('amount', trans('lease.demesne_money_collection')) !!} <span class="required_field">*</span>
    </div>
    <div class="col-md-6">
        {!! Form::text('amount', null, array('class'=>'form-control form-required', 'placeholder'=>'৳')) !!}
    </div>
</div>
<div class="form-group">
    <div class="col-md-3">
        <label>{{ trans('lease.date') }}</label> <span class="required_field">*</span>
    </div>
    <div class="col-md-6">
        <input type="text" class="form-control form-required date_picker validDate" id="from_date_id" name="VOUCHER_DT" placeholder="Ex.20-07-2016">
    </div>
</div>

<div class="form-group">
    <div class="col-md-12" style="padding-top: 15px;">
        <span class="modal_msg pull-left"></span>
        <button type="submit" data-action="{{ route('demesne_money_save') }}" class="btn btn-success btn-sm pull-right form_submit" style="margin-left: 5px;">{{ trans('common.form_submit') }}</button>
        <button type="button" class="btn btn-default btn-sm reset_form pull-right">{{ trans('common.form_reset') }}</button>
        <span class="loadingImg"></span>
    </div>
</div>

{!! Form::close() !!}