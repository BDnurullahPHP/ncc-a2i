<input type="hidden" name="LEASE_DE_ID" value="{{ $demesne_info->LEASE_DE_ID }}">
<table class="table table-striped table-bordered table-hover">
    <tbody>
        <tr>
            <th>আদায় কারির নাম</th>
            <td>{{ $demesne_info->NAME }}</td>
        </tr>
        <tr>            
            <th>আদায় কারির মোবাইল</th>
            <td>{{ $demesne_info->PHONE }}</td>
        </tr>
        <tr>
            <th>আদায় কারির ঠিকানা</th>
            <td>{!! $demesne_info->ADDRESS !!}</td>
        </tr>
    </tbody>
</table>