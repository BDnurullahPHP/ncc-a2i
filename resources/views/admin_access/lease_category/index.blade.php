@extends('admin_access.layout.master')

@section('style')

@endsection

@section('content')

	<h3>{{ trans('common.lease_dropdown_category') }}
    	<small></small>
	</h3>
	<div class="portlet light bordered">
	    <div class="portlet-title">
	        <div class="caption">
	            <a href="#" data-toggle="modal" data-target="#modal_add_content" class="btn btn-primary btn-sm" data-form="{{ route('lease_category_form') }}">{{ trans('common.add_new') }} <i class="fa fa-plus"></i></a>
	        </div>
	        <div class="tools"> </div>
	    </div>
	    <div class="portlet-body contentArea">
	        
			<table class="table table-striped table-bordered table-hover" data-source="{{ route('lease_category_data') }}" id="common_table">
			    <thead>
			        <tr>
			            <th>{{ trans('common.table_sl') }}</th>
			            <th>{{ trans('common.lease_category') }}</th>
			            <th>{{ trans('common.lease_ban_category') }}</th>
			            <th>{{ trans('common.table_head_created_at') }}</th>
			            <th>{{ trans('common.table_action') }}</th>
			        </tr>
			    </thead>
			    <tbody>
			    	
			    </tbody>
			</table>

	    </div>
	</div>

@endsection