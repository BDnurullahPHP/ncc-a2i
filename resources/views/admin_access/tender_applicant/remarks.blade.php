{!! Form::model($tender, array('url' => '#', 'id'=>'remarks_form', 'class'=>'form-horizontal', 'method' => 'post')) !!}
    
    <!--=============Title for modal===============-->
    <div class="modal_top_title">{{ trans('tender.remarks') }}</div>
    <!--===========End title for modal=============-->

    <!--========Show form post message=============-->
    <div class="form-group">
        <div class="col-sm-6 form_messsage" style="padding-top: 15px;">
            
        </div>
    </div>
    <!--========End form post message=============-->
    
    <div class="form-group">
        <div class="col-md-12">
            {!! Form::label('REMARKS', trans('common.comment')) !!} <span class="required_field">*</span>
        </div>
        <div class="col-md-12">
            {!! Form::textarea('REMARKS', null, array('rows' =>'2', 'class'=>'form-control reactor_basic form-required', 'placeholder'=>trans('common.your_comment'))) !!}
        </div>
    </div>
    
    <div class="form-group">
        <div class="col-md-12" style="padding-top: 15px;">
            <button style="margin-left: 5px;" type="submit" data-action="{{ route('tender_remarks_update', [$type, $type == 'applicant' ? $tender->TE_APP_ID : $tender->CITIZEN_ID]) }}" class="btn btn-success pull-right btn-sm form_submit">{{ trans('common.form_submit') }}</button>
            <button type="button" class="btn btn-default pull-right btn-sm reset_form">{{ trans('common.form_reset') }}</button>
        </div>
    </div>

{!! Form::close() !!}