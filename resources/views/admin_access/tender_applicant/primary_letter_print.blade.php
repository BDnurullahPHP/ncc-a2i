<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>প্রাথমিক ভাবে নির্বাচিত আবেদনকারী চিঠি</title>
    </head>
    <style>
        body {
            font-family: freeserif,sans-serif,vrinda,solaiman-lipi,monospace;
        }
        h1, h2, h3, h4, p, input, th, td{
            font-family: freeserif,sans-serif,vrinda,solaiman-lipi,monospace;
            font-weight: 400;
            white-space: initial;
        }
        .sectionrow
        {
            width: 100%;
            border: 1px ;            
            border-style: solid ;
            font-size: 12px; 
        }
        h4{
            text-align:center; 
        }
        p{
            text-align: justify;
        }

    </style>
    <body>
        <h4>
            <bold>নারায়ণগঞ্জ সিটি করপোরেশন</bold>
            <br/>নগর ভবন, ১০ বঙ্গবন্ধু রোড
            <br/>নারায়ণগঞ্জ
            <br/>www.ncc.gov.bd
        </h4>
        <table>
            <tr>
                <td style="width: 80%;">স্মারক নং- {{ $letter->LATTER_NUMBER }}</td>
                <td style="width: 20%; text-align: right;">তারিখঃ {{ Helpers::en2bn(date('d/m/Y', strtotime($letter->DATE))) }} খ্রিঃ</td>
            </tr>
            <tr>
                <td>
                    <h3>বিষয়ঃ {{ $letter->SUBJECT }}।</h3>
                </td>
            </tr>
            <tr>
                <td>
                    <h3>সুত্রঃ {{ Helpers::en2bn($letter->LETTER_SOURCE) }}।</h3>
                </td>
            </tr>
        </table>
                {!! $letter->BODY !!}
            <br/>
            <br/>
        <table>
            <tr>
                <td>
                    {!! $letter->APPLICANT_INFO !!}
                </td>
                <td style="text-align: center">
                    {!! $letter->OFFICER_INFO !!}
                </td>
            </tr>
            <tr>
                <td>
                    অনুলিপিঃ<br>
                    {!! $letter->DISTRIBUTION !!}
                </td>
            </tr>
    </table>
</body>
</html>