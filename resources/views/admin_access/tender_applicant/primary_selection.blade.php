{!! Form::model($tender, array('url' => '#', 'id'=>'applicant_installment', 'class'=>'form-horizontal', 'method' => 'post')) !!}

<input type="hidden" name="applicant_id" value="{{ $type == 'applicant' ? $tender->TE_APP_ID : $tender->CITIZEN_ID }}">
<input type="hidden" name="schedule_id" value="{{ $tender->SCHEDULE_ID }}">
<input type="hidden" class="applicant_bid_amount" value="{{ $tender->BID_AMOUNT - $tender->BG_AMOUNT }}">
<input type="hidden" name="applicant_email" value="{{ $applicant->EMAIL }}">
<!--=============Title for modal===============-->
<div class="modal_top_title">{{ trans('tender.tender_installment_title') }}</div>
<!--===========End title for modal=============-->

<!--========Show form post message=============-->
<div class="form-group">
    <div class="col-sm-6 form_messsage" style="padding-top: 15px;">

    </div>
</div>
<!--========End form post message=============-->
<div class="tabbable-custom">
    <div class="tab-content">
        <div class="tab-pane active" id="tab_1"> <!-- 1st panel-->
            <br/>
            <div class="form-group">
                <div class="col-md-2">
                    <label>{{ trans('tender.tender_installment') }}</label> <span class="required_field">*</span>
                </div>
                <div class="col-md-3">
                    <input type="number" class="form-control validField" name="INSTALLMENT" id="installment_id" min="1" placeholder="{{ trans('tender.tender_installment') }}">
                </div>
            </div>
            <div class="form-group" id="installment">
                <div class="col-md-12">
                    <table class="table table-striped table-bordered table-hover" id="installment">
                        <thead>
                            <tr>
                                <th>{{ trans('tender.installment_number') }}</th>
                                <th>{{trans('tender.installment_date')}}</th>
                                <th>{{trans('tender.installment_amount')}}</th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-2">
                    <label>{{trans('tender.installment_penalty')}}</label> <span class="required_field">*</span>
                </div>
                <div class="col-md-3">
                    <input type="text" class="form-control validField" name="punishment" id="punishment_id" placeholder="%">
                </div>
                <div class="col-md-7">
                    <span>কিস্তির টাকা নির্দিষ্ট সময়ে পরিশোধ করতে ব্যর্থ হলে জরিমানার পরিমাণ (শতাংশ)</span>
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-12">
                    {!! Form::label('INITIAL_REMARKS', trans('common.comment')) !!}
                </div>
                <div class="col-md-12">
                    {!! Form::textarea('INITIAL_REMARKS', null, array('rows' =>'2', 'class'=>'form-control reactor_basic', 'placeholder'=>trans('common.your_comment'))) !!}
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-12" style="padding-top: 15px;">
                    <a href="#tab_2" style="margin-left: 5px;" class="btn btn-default btn-sm pull-right tab_next">{{ trans('common.next') }} <i class="fa fa-arrow-right"></i></a>
                </div>
            </div>
        </div>
        <div class="tab-pane" id="tab_2"><!--  2nd panel-->
            <br/>
            <div class="col-md-12">
                <div class="form-group">
                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="col-md-4">
                                <label>স্মারক নং- </label><span class="required_field">*</span>
                            </div>
                            <div class="col-md-8">
                                <input type="text" class="form-control form-required" name="sharok_no" id="sharok_no_id" value="" placeholder="৪৬.৪৪৬৭০০.১৪.১৮(৮).১৬/">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="col-md-4">
                                <label>তারিখঃ </label><span class="required_field">*</span>
                            </div>
                            <div class="col-md-8">
                                <input type="text" class="form-control form-required date_picker" name="sharok_date" id="sharok_date_id" value="{{ date('d-m-Y') }}" placeholder="Ex.20-07-2016">
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-12">
                <div class="form-group">
                    <div class="col-md-2">
                        <label>বিষয়ঃ  </label><span class="required_field">*</span>
                    </div>
                    <div class="col-md-8">
                        <input type="text" class="form-control form-required" name="letter_subject" value="{{ $project_name }}" id="letter_subject_id" placeholder="অবশিষ্ট টাকা পরিশোধ প্রসঙ্গে">
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group">
                    <div class="col-md-2">
                        <label>সুত্রঃ  </label><span class="required_field">*</span>
                    </div>
                    <div class="col-md-8">
                        <input type="hidden" class="form-control form-required" name="letter_sources" id="letter_sources_id" value="গত {{ date('d-m-Y') }} তারিখে আপনার দাখিলকৃত দরপত্র।"/>
                        <span>গত {{ Helpers::en2bn(date('d/m/Y', strtotime($tender->CREATED_AT))) }} তারিখে আপনার দাখিলকৃত দরপত্র।</span>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group">
                    <textarea class="form-control reactor_basic form-required" name="letter_body" id="letter_body_id">
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; উপর্যুক্ত বিষয়ে জানানো যাচ্ছে যে, নারায়ণগঞ্জ সিটি কর্পোরেশনাধীন {{ $project_name }} ১৪২৩ বাংলা সনের ১ বৈশাখ হতে ৩০ চৈত্র পর্যন্ত এক বত্সর মেয়াদে ইজারা গ্রহণের জন্য আপনার প্রস্তাবিত দর {{ Helpers::en2bn($tender->BID_AMOUNT) }}/-টাকা সর্বোচ্চ বিবেচিত হওয়ায় তা গ্রেহিত হয়েছে। আপনি দরপত্রের সাথে পে-অর্ডার দ্বারা {{ Helpers::en2bn($tender->BG_AMOUNT) }} টাকা জমা প্রদান করেছেন।

                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; এমতাবস্তায়, পত্র প্রাপ্তির ০৭ (সাত) কার্যদিবসের মধ্যে প্রস্তাবিত মূল্য এর উপর ১৫% ভ্যাট বাবদ {{ Helpers::en2bn(5000) }}/-টাকা, ৫% আয়কর বাবদ {{ Helpers::en2bn(8966) }}/-টাকা এবং ৫% জামানত বাবদ {{ Helpers::en2bn(500) }}/-টাকা সহ সর্বমোট {{ Helpers::en2bn(5963) }} টাকা সিটি কর্পোরেশন তহবিলে জমা প্রদান করতঃ সিটি কর্পোরেশনের সহিত ৩০০/-টাকা নন জুডিশিয়াল স্ট্যাম্পে চুক্তিপত্র সম্পাদানের জন্য অনুরোধ করা হলো।
                    </textarea>
                </div>
            </div>

            <div class="col-md-12">
                <div class="form-group">
                    <div class="col-md-6 hidden">
                        <textarea class="form-control reactor_basic" name="applicant_info" id="applicant_info_id">
                                জনাব {{ $applicant->APPLICANT_NAME }}
                                পিতা/স্বামী- {{ $applicant->SPOUSE_NAME }}
                                বাসা নং- {{ $applicant->HOLDING_NO }}, রাস্তা- {{ $applicant->ROAD_NO }}
                                থানা- {{ $applicent_thana }}, জেলা- {{ $applicent_district }}
                        </textarea>
                    </div>
                    <div class="col-md-6">
                        <p style="text-align: left">
                            জনাব {{ $applicant->APPLICANT_NAME }}
                            <br/>পিতা/স্বামী- {{ $applicant->SPOUSE_NAME }}
                            <br/>বাসা নং- {{ $applicant->HOLDING_NO }}, রাস্তা- {{ $applicant->ROAD_NO }}
                            <br/>থানা- {{ $applicent_thana }}, জেলা- {{ $applicent_district }}
                        </p>
                    </div>
                    <div class="col-md-6">
                        <textarea class="form-control reactor_basic form-required" name="officer_info" id="officer_info_id">
                                ({{ $officer->NAME }})
                                ({{ $officer->DESIGNATION }})
                                ({{ $officer->OFFICE }})
                                ({{ $officer->PHONE }})
                                ({{ $officer->EMAIL }})
                        </textarea>
                    </div>
                </div>
            </div>

            <div class="col-md-12">
                <div class="form-group">
                    <textarea class="form-control reactor_basic form-required" name="distribution" id="distribution_id">
                            @foreach($location as $key=>$value)
                            {{ Helpers::en2bn($key+1) }}। {{ $value->LOCATION_NAME }}<br>
                            @endforeach
                    </textarea>
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-12" style="padding-top: 15px;">
                    <button style="margin-left: 5px;" type="submit" data-action="{{ route('tender_primary_select_insert', [$tender->SCHEDULE_ID, $type, $type == 'applicant' ? $tender->TE_APP_ID : $tender->CITIZEN_ID]) }}" data-print="{{ route('tender_primary_letter_print', [$tender->SCHEDULE_ID, $type, $type == 'applicant' ? $tender->TE_APP_ID : $tender->CITIZEN_ID]) }}" class="btn btn-success pull-right btn-sm tender_installment_save">{{ trans('common.form_submit') }}</button>
                    <a href="#tab_1" class="btn btn-default btn-sm pull-right tab_previous"><i class="fa fa-arrow-left"></i> {{ trans('common.previous') }}</a>
                </div>
            </div>
        </div>
    </div>
</div>
{!! Form::close() !!}