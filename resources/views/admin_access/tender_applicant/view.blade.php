<div class="tender_view">

    <!--=============Title for modal===============-->
    <div class="modal_top_title">{{ trans('tender.tender_applicant_details') }}</div>
    <!--===========End title for modal=============-->
    <h3>আবেদনকারীর তথ্য</h3>

    @if($type != 'lease_demesne')
        <div class="row padding-bottom-10">
            <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="row">
                    {!! Form::label('', trans('tender.tender_applicant_full_name'), array('class'=>'col-sm-6')) !!}
                    <div class="col-md-6 col-sm-6">
                        @if($type === 'citizen')
                            <p>{{ $applicant->getTenderCitizen->FULL_NAME }}</p>
                        @else
                            <p>{{ $applicant->APPLICANT_NAME }}</p>
                        @endif
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="row">
                    {!! Form::label('', trans('tender.tender_applicant_name_fh'), array('class'=>'col-sm-6')) !!}
                    <div class="col-md-6 col-sm-6">
                        <p>{{ $applicant->SPOUSE_NAME }}</p>
                    </div>
                </div>
            </div>
        </div>

        <div class="row padding-bottom-10">
            <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="row">
                    {!! Form::label('', trans('tender.tender_applicant_mobile'), array('class'=>'col-sm-6')) !!}
                    <div class="col-md-6 col-sm-6">
                        @if($type === 'citizen')
                            <p>{{ $applicant->getTenderCitizen->MOBILE_NO }}</p>
                        @else
                            <p>{{ $applicant->APPLICANT_PHONE }}</p>
                        @endif
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="row">
                    {!! Form::label('', trans('tender.tender_applicant_email'), array('class'=>'col-sm-6')) !!}
                    <div class="col-md-6 col-sm-6">
                        @if($type === 'citizen')
                            <p>{{ $applicant->getTenderCitizen->EMAIL }}</p>
                        @else
                            <p>{{ $applicant->EMAIL }}</p>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <div class="row padding-bottom-10">
            <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="row">
                    {!! Form::label('', trans('tender.tender_applicant_nid'), array('class'=>'col-sm-6')) !!}
                    <div class="col-md-6 col-sm-6">
                        <p>{{ $applicant->NID }}</p>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="row">
                    {!! Form::label('', trans('tender.tender_applicant_district'), array('class'=>'col-sm-6')) !!}
                    <div class="col-md-6 col-sm-6">
                        <p>{{ $applicant_district }}</p>
                    </div>
                </div>
            </div>
        </div>

        <div class="row padding-bottom-10">
            <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="row">
                    {!! Form::label('', trans('tender.tender_applicant_thana'), array('class'=>'col-sm-6')) !!}
                    <div class="col-md-6 col-sm-6">
                        <p>{{ $applicant_thana }}</p>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="row">
                    {!! Form::label('', trans('tender.tender_applicant_holding'), array('class'=>'col-sm-6')) !!}
                    <div class="col-md-6 col-sm-6">
                        <p>{{ $applicant->HOLDING_NO }}</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row padding-bottom-10">
            <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="row">
                    {!! Form::label('', trans('tender.tender_applicant_road'), array('class'=>'col-sm-6')) !!}
                    <div class="col-md-6 col-sm-6">
                        <p>{{ $applicant->ROAD_NO }}</p>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12">

            </div>
        </div>
        <div class="row padding-bottom-10">
            <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="row">
                    {!! Form::label('', trans('tender.tender_applicant_rate'), array('class'=>'col-sm-6')) !!}
                    <div class="col-md-6 col-sm-6">
                        <p>{{ number_format($applicant_property->BID_AMOUNT,2) }}</p>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="row">
                    {!! Form::label('', trans('tender.tender_applicant_sec'), array('class'=>'col-sm-6')) !!}
                    <div class="col-md-6 col-sm-6">
                        <p>{{ number_format($applicant_property->BG_AMOUNT,2) }}</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row padding-bottom-10">
            <div class="col-md-12 col-sm-12 col-xs-12">
                @if ($applicant_property->IS_LEASE === 1)
                    <h3>{{ $applicant_category }}র দরপত্রের বিবরণ</h3>
                    <table class="table table-striped table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>সায়রাত মহালের নাম</th>
                                <th>মহালের অবস্থান</th>
                                @foreach($tender_particular as $key=>$value)
                                    <th>
                                        {{ $value->getParticular->PARTICULAR_NAME }} {{ $value->UOM == 1 ? '('.trans('common.percent').')' : '' }}
                                    </th>
                                @endforeach
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>{{ $project_name->PR_NAME_BN }}</td>
                                <td>{!! $project_name->PROJECT_LOCATION !!}</td>
                                @foreach($tender_particular as $key=>$value)
                                    <td>{{ $value->PARTICULAR_AMT }}</td>
                                @endforeach
                            </tr>
                        </tbody>
                    </table>
                @else
                    <h3>{{ $applicant_category }} এর দরপত্রের বিবরণ</h3>
                    <table class="table table-striped table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>প্রকল্প এর নাম</th>
                                <th>নাম্বার</th>
                                <th>অবস্থান</th>
                                <th>পরিমাপ</th>
                                <th style="width: 22%;" class="schedule_input text-center">
                                    সর্বনিম্ন সালামীর পরিমাণ (টাকা)
                                </th>
                                <th style="width: 15%;" class="schedule_input text-center">
                                    মাসিক ভাড়া (টাকা)
                                </th>
                                <th style="width: 15%;" class="schedule_input text-center">
                                    সিডিউলের মূল্য  (টাকা)
                                </th>
                                <th style="width: 15%;" class="schedule_input text-center">
                                    জামানত (শতাংশ)
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>{{ $project_name->PR_NAME_BN }}</td>
                                <td>{{ $project_details->PR_SSF_NO }}</td>
                                <td>{{ $project_details->POSITION }}</td>
                                <td>{{ $project_details->PR_MEASURMENT }}</td>
                                <td>{{ number_format($schedule_info->LOWEST_TENDER_MONEY) }}</td>
                                @foreach($tender_particular as $key=>$value)
                                <td>{{ $value->PARTICULAR_AMT }}</td>
                                @endforeach
                            </tr>
                        </tbody>
                    </table>
                @endif
            </div>
        </div>

        <div class="row padding-bottom-10">
            {!! Form::label('', trans('tender.tender_applicant_pay_info'), array('class'=>'col-sm-3')) !!}
            <div class="col-md-9 col-sm-9">
                @if ($applicant_property->IS_TE_SC_BOUGHT == 0)
                    <span class="label label-danger">বকেয়া</span>
                @elseif ($applicant_property->IS_TE_SC_BOUGHT == 1)
                    <span class="label label-warning">গ্রহণের অপেক্ষা</span>
                @else
                    <span class="label label-success">পরিশোধিত</span>
                @endif
            </div>
        </div>
        <div class="row padding-bottom-10">
            {!! Form::label('', trans('জামানতের মূল্য পরিশোধের তথ্য'), array('class'=>'col-sm-3')) !!}
            <div class="col-md-9 col-sm-9">
                @if ($applicant_property->BG_PAYMENT_TYPE == 1)
                    <p>পে-অর্ডার নাম্বার: {{ $applicant_property->B_DRAFT_NO }}, তারিখ: {{ date('d/m/Y', strtotime($applicant_property->B_DRAFT_DATE)) }}, ব্যাংকের নাম: {{ $applicant_bank }} </p>
                @else
                    @if ($applicant_property->BG_PAYMENT_STATUS == 0)
                        <span class="label label-danger">বকেয়া</span>
                    @elseif ($applicant_property->BG_PAYMENT_STATUS == 1)
                        <span class="label label-warning">গ্রহণের অপেক্ষা</span>
                    @else
                        <span class="label label-success">পরিশোধিত</span>
                    @endif
                @endif
            </div>
        </div>
        <div class="row padding-bottom-10">
            {!! Form::label('', trans('tender.tender_pay_info_attach'), array('class'=>'col-sm-3')) !!}
            <div class="col-md-6 col-sm-6">
                <?php $taxfile = $applicant_property->B_DRAFT_ATTACHMENT; ?>
                @if(!empty($applicant_property->B_DRAFT_ATTACHMENT) && File::exists(public_path()."/upload/bank/$taxfile") )
                    <a target="_blank" href='{{ Helpers::asset("upload/bank/$taxfile") }}'>
                        <img src="{{ Helpers::asset("upload/bank/$taxfile") }}" width="150">
                    </a>
                @endif
            </div>
        </div>
    @else
        <div class="row padding-bottom-10">
            <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="row">
                    {!! Form::label('', trans('tender.tender_applicant_full_name'), array('class'=>'col-sm-6')) !!}
                    <div class="col-md-6 col-sm-6">
                        <p>{{ $applicant_property->NAME }}</p>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="row">
                    {!! Form::label('', trans('tender.tender_applicant_mobile'), array('class'=>'col-sm-6')) !!}
                    <div class="col-md-6 col-sm-6">
                        <p>{{ $applicant_property->PHONE }}</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row padding-bottom-10">
            <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="row">
                    {!! Form::label('', trans('common.lease_address'), array('class'=>'col-sm-6')) !!}
                    <div class="col-md-6 col-sm-6">
                        <p>{!! $applicant_property->ADDRESS !!}</p>
                    </div>
                </div>
            </div>
        </div>

        <div class="row padding-bottom-10">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <h3>খাসের বিবরণ</h3>
                <table class="table table-striped table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>সায়রাত মহালের নাম</th>
                            <th>মহালের অবস্থান</th>
                            <th>খাসের সময়কাল</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>{{ $applicant_property->getSchedule->getProject->PR_NAME_BN }}</td>
                            <td>{!! $applicant_property->getSchedule->getProject->PROJECT_LOCATION !!}</td>
                            <td>
                                {{ date('d/m/Y', strtotime($applicant_property->getValidity->DATE_FROM)) }} - 
                                {{ date('d/m/Y', strtotime($applicant_property->getValidity->DATE_TO)) }}
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    @endif

</div>