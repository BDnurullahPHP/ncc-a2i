{!! Form::model($tender, array('url' => '#', 'id'=>'final_letter', 'class'=>'form-horizontal', 'method' => 'post')) !!}

<input type="hidden" name="applicant_id" value="{{ $type == 'applicant' ? $tender->TE_APP_ID : $tender->CITIZEN_ID }}">
<input type="hidden" name="schedule_id" value="{{ $tender->SCHEDULE_ID }}">
<input type="hidden" class="applicant_bid_amount" value="{{ $tender->BID_AMOUNT - $tender->BG_AMOUNT }}">
<input type="hidden" name="applicant_email" value="{{ $type == 'applicant' ? $applicant->EMAIL : $citizen->getTenderCitizen->EMAIL }}">
<!--=============Title for modal===============-->
<div class="modal_top_title">{{ trans('tender.tender_installment_title') }}</div>
<!--===========End title for modal=============-->

<!--========Show form post message=============-->
<div class="form-group">
    <div class="col-sm-6 form_messsage" style="padding-top: 15px;">

    </div>
</div>
<!--========End form post message=============-->
<div class="tabbable-custom">
    <div class="tab-content">

        <div class="col-md-12">
            <div class="form-group">
                <div class="col-md-6">
                    <div class="form-group">
                        <div class="col-md-4">
                            <label>স্মারক নং- </label><span class="required_field">*</span>
                        </div>
                        <div class="col-md-8">
                            <input type="text" class="form-control form-required" name="sharok_no" id="sharok_no_id" value="" placeholder="৪৬.৪৪৬৭০০.১৪.১৮(৮).১৬/">
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <div class="col-md-4">
                            <label>তারিখঃ </label><span class="required_field">*</span>
                        </div>
                        <div class="col-md-8">
                            <input type="text" class="form-control form-required date_picker" name="sharok_date" id="sharok_date_id" value="{{ date('d-m-Y') }}" placeholder="Ex.20-07-2016">
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-12">
            <div class="form-group">
                <div class="col-md-2">
                    <label>বিষয়ঃ  </label><span class="required_field">*</span>
                </div>
                <div class="col-md-8">
                    <input type="text" class="form-control form-required" name="letter_subject" value="{{ $project_name }}" id="letter_subject_id" placeholder="অবশিষ্ট টাকা পরিশোধ প্রসঙ্গে">
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="form-group">
                <div class="col-md-2">
                    <label>সুত্রঃ  </label><span class="required_field">*</span>
                </div>
                <div class="col-md-8">
                    <input type="hidden" class="form-control form-required" name="letter_sources" id="letter_sources_id" value="গত {{ date('d-m-Y') }} তারিখে আপনার দাখিলকৃত দরপত্র।"/>
                    <span>গত {{ Helpers::en2bn(date('d/m/Y', strtotime($tender->CREATED_AT))) }} তারিখে আপনার দাখিলকৃত দরপত্র।</span>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="form-group">
                <textarea class="form-control reactor_basic form-required" name="letter_body" id="letter_body_id">
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; উপর্যুক্ত বিষয়ে জানানো যাচ্ছে যে, নারায়ণগঞ্জ সিটি কর্পোরেশনাধীন {{ $project_name }} ১৪২৩ বাংলা সনের ১ বৈশাখ হতে ৩০ চৈত্র পর্যন্ত এক বত্সর মেয়াদে ইজারা গ্রহণের জন্য আপনার প্রস্তাবিত দর {{ Helpers::en2bn($tender->BID_AMOUNT) }}/-টাকা সর্বোচ্চ বিবেচিত হওয়ায় তা গ্রেহিত হয়েছে। আপনি দরপত্রের সাথে পে-অর্ডার দ্বারা {{ Helpers::en2bn($tender->BG_AMOUNT) }} টাকা জমা প্রদান করেছেন।

                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; এমতাবস্তায়, পত্র প্রাপ্তির ০৭ (সাত) কার্যদিবসের মধ্যে প্রস্তাবিত মূল্য এর উপর ১৫% ভ্যাট বাবদ {{ Helpers::en2bn(5000) }}/-টাকা, ৫% আয়কর বাবদ {{ Helpers::en2bn(8966) }}/-টাকা এবং ৫% জামানত বাবদ {{ Helpers::en2bn(500) }}/-টাকা সহ সর্বমোট {{ Helpers::en2bn(5963) }} টাকা সিটি কর্পোরেশন তহবিলে জমা প্রদান করতঃ সিটি কর্পোরেশনের সহিত ৩০০/-টাকা নন জুডিশিয়াল স্ট্যাম্পে চুক্তিপত্র সম্পাদানের জন্য অনুরোধ করা হলো।
                </textarea>
            </div>
        </div>
        <div class="col-md-12">
            <div class="form-group">
                <textarea class="form-control reactor_basic form-required" name="letter_conditions" id="letter_conditions_id">
                শর্তাবলি ঃ
                ১। {{ $project_name }} পরিচালনার ক্ষেত্রে ইজারার বিজ্ঞপ্তির বর্ণিত শর্তাবলি প্রযোজ্য হবে।
                ২। শুধুমাত্র {{ $project_name }} হতে টোল আদায় করতে হবে।
                ৩। এই কার্যাদেশের মেয়াদ {{ Helpers::en2bn(date('d/m/Y')) }} তারিখ হতে {{ Helpers::en2bn(date('d/m/Y')) }} তারিখ পর্যন্ত।
                </textarea>
            </div>
        </div>

        <div class="col-md-12">
            <div class="form-group">
                <div class="col-md-6 hidden">
                    <textarea class="form-control reactor_basic" name="applicant_info" id="applicant_info_id">
                        জনাব {{ $type == 'applicant' ? $applicant->APPLICANT_NAME : $citizen->getTenderCitizen->FULL_NAME }}
                        পিতা/স্বামী- {{ $type == 'applicant' ? $applicant->SPOUSE_NAME : $citizen->SPOUSE_NAME }}
                        বাসা নং- {{ $type == 'applicant' ? $applicant->HOLDING_NO : $citizen->HOLDING_NO }}, রাস্তা- {{ $type == 'applicant' ? $applicant->ROAD_NO : $citizen->ROAD_NO }}
                        থানা- {{ $applicent_thana }}, জেলা- {{ $applicent_district }}
                    </textarea>
                </div>
                <div class="col-md-6">
                    <p style="text-align: left">
                        জনাব {{ $type == 'applicant' ? $applicant->APPLICANT_NAME : $citizen->getTenderCitizen->FULL_NAME }}
                        <br/>পিতা/স্বামী- {{ $type == 'applicant' ? $applicant->SPOUSE_NAME : $citizen->SPOUSE_NAME }}
                        <br/>বাসা নং- {{ $type == 'applicant' ? $applicant->HOLDING_NO : $citizen->HOLDING_NO }}, রাস্তা- {{ $type == 'applicant' ? $applicant->ROAD_NO : $citizen->ROAD_NO }}
                        <br/>থানা- {{ $applicent_thana }}, জেলা- {{ $applicent_district }}
                    </p>
                </div>
                <div class="col-md-6">
                    <textarea class="form-control reactor_basic form-required" name="officer_info" id="officer_info_id">
                        &#40;{{ $officer->NAME }}&#41;
                        &#40;{{ $officer->DESIGNATION }}&#41;
                        &#40;{{ $officer->OFFICE }}&#41;
                        &#40;{{ $officer->PHONE }}&#41;
                        &#40;{{ $officer->EMAIL }}&#41;
                    </textarea>
                </div>
            </div>
        </div>

        <div class="col-md-12">
            <div class="form-group">
                <textarea class="form-control reactor_basic form-required" name="distribution" id="distribution_id">
                    @foreach($location as $key=>$value)
                    {{ Helpers::en2bn($key+1) }}। {{ $value->LOCATION_NAME }}<br>
                    @endforeach
                </textarea>
            </div>
        </div>
        
        <div class="form-group">
            <div class="col-md-12" style="padding-top: 15px;">
                <button style="margin-left: 5px;" type="submit" data-action="{{ route('tender_final_select_insert', [$tender->SCHEDULE_ID, $type, $type == 'applicant' ? $applicant->TE_APP_ID : $citizen->CITIZEN_ID]) }}" data-print="{{ route('tender_final_letter_print', [$tender->SCHEDULE_ID, $type, $type == 'applicant' ? $applicant->TE_APP_ID : $citizen->CITIZEN_ID]) }}" class="btn btn-success pull-right btn-sm finally_tender_save">{{ trans('common.form_submit') }}</button>
            </div>
        </div>
    </div>
</div>
{!! Form::close() !!}
