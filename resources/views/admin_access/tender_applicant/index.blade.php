@extends('admin_access.layout.master')

@section('script')

<script src="{{ Helpers::asset('assets/admin/js/tender.js') }}" type="text/javascript"></script>
<script type="text/javascript">
jQuery(document).ready(function($) {
    tenderPrimarySelect.init();
});
</script>

@endsection

@section('style')

<link rel="stylesheet" href="{{ Helpers::asset('assets/admin/css/tender.css') }}">

<style type="text/css">
    table#common_table>thead>tr>th{
        text-align: center;
        white-space: inherit;
    }
    table#common_table>tbody>tr>td:first-child, 
    table#common_table>tbody>tr>td:nth-child(3), 
    table#common_table>tbody>tr>td:nth-child(4), 
    table#common_table>tbody>tr>td:last-child{
        text-align: center;
    }
</style>

@endsection

@section('content')

<h3>{{ trans('tender.tender_comparison_info') }}
    <small></small>
</h3>

<div class="row">
    <div class="col-md-4 col-sm-4 col-xs-12" style="padding:10px 15px;">
        {!! Form::label('tender_id', trans('tender.tender_select'), array('style'=>'font-size:16px;')) !!}
        {!! Form::select('tender_id', $tender, null, array('class'=>'form-control list_select','placeholder' => trans('common.form_select'))) !!}
    </div>
<!--    <div class="col-md-4 col-sm-4 col-xs-12 hidden" style="padding:10px 15px;" id="schedule_list" data-action="{{ route('tender_schedule_list') }}">
        <label for="tender_id" style="font-size:16px;">সিডিউল নির্বাচন করুন</label>
        <select class="form-control list_select" id="schedule_id" name="schedule_id">
            <option selected="selected" value="">নির্বাচন করুন</option>
            <option value="18">দরপত্রের নাম</option>
            <option value="19"> স্মারক নাম্বার * স্মারকের তারিখ * দরপত্রের নাম *</option>
        </select>
    </div>-->
</div>

<div class="portlet light bordered" id="result_content">
    <!-- <div class="portlet-title">
        <div class="caption">
            
        </div>
        <div class="tools"> </div>
    </div> -->
    <div class="row">
        <div class="col-md-5 col-sm-6 col-xs-12">
            <div id="lg_form_message"></div>
        </div>
    </div>
    <div class="portlet-body contentArea">

        <table class="table table-striped table-bordered table-hover datatable_ajax" data-source="{{ route('tender_comparison_data') }}">
            <thead>
                <tr>
                    <th style="width:50px;">{{ trans('common.table_sl') }}</th>
                    <th style="width:25%;">{{ trans('tender.tender_applicant_name_address') }}</th>
                    <th style="width: 18%;">{{ trans('tender.tender_number') }}</th>
                    <th style="width:15%;">{{ trans('tender.city_corporation_fixed_rate') }}</th>
                    <th style="width:15%;">{{ trans('tender.tender_applicant_rate') }}</th>
                    {{-- <th style="width:10px;">{{ trans('tender.pay_order_bank_name') }}</th> --}}
                    <th>{{ trans('tender.schedule_price') }}</th>
                    <th style="width:5%;">{{ trans('common.table_action') }}</th>
                </tr>
            </thead>
            <tbody>

            </tbody>
        </table>

    </div>
</div>

@endsection