<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<title>Contact request</title>
	</head>
	<body>
		@if(isset($query))
			<p><strong>Type: </strong>{{ $query }}</p>
		@endif
		<p><strong>Name: </strong>{{ $name }}</p>
		<p><strong>Mobile: </strong>{{ $mobile }}</p>
		@if(isset($address))
			<p><strong>Address: </strong>{{ $address }}</p>
		@endif
		<p><strong>Message: </strong>{!! $text !!}</p>
	</body>
</html>