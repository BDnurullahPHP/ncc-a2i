<?php

namespace App\Console\Commands;

use App\Models\AccountVaucherChd;
use App\Models\AccountVaucherMst;
use App\Models\AccountVnLedger;
use App\Models\CategoryParticular;
use App\Models\CitizenProperty;
use App\Models\Particular;
use Illuminate\Console\Command;

class CitizenVoucher extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ncc:rent-voucher';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command for create citizen voucher';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $citizen = CitizenProperty::with(['getCitizen' => function ($query) {
            $query->with(['getCitizenVoucher' => function ($q) {
                $q->where('VOUCHER_DT', '<', date('Y-m-d'));
            }])
                                        ->where('IS_ACTIVE', 1);
        }])
                                    ->where('IS_LEASE', 0)
                                    ->where('IS_ACTIVE', 1)
                                    ->get();

        foreach ($citizen as $key => $ctz) {
            $ac_vaucher_mst = new AccountVaucherMst();
            $ac_vaucher_mst->VOUCHER_DT = date('Y-m-d H:i:s');
            $ac_vaucher_mst->CITIZEN_ID = $ctz->CITIZEN_ID;
            $ac_vaucher_mst->IS_ACTIVE = 1;
            $ac_vaucher_mst->save();

            $vaucher_mst_id = $ac_vaucher_mst->VOUCHER_NO;
            $particular_id = Particular::where('TYPE', 2)->pluck('PARTICULAR_ID');
            $particular_amt = CategoryParticular::where('PARTICULAR_ID', $particular_id)->pluck('PARTICULAR_AMT');

            $ac_vaucher_chd = new AccountVaucherChd();
            $ac_vaucher_chd->TRX_TRAN_DT = date('Y-m-d H:i:s');
            $ac_vaucher_chd->VOUCHER_NO = $vaucher_mst_id;
            $ac_vaucher_chd->SCHEDULE_ID = $ctz->SCHEDULE_ID;
            $ac_vaucher_chd->PARTICULAR_ID = $particular_id;
            $ac_vaucher_chd->PUNIT_PRICE = $particular_amt;
            $ac_vaucher_chd->IS_ACTIVE = 1;
            $ac_vaucher_chd->save();

            $vn_ledger = new AccountVnLedger();
            $vn_ledger->VLEDGER_DT = date('Y-m-d H:i:s');
            $vn_ledger->TRX_CODE_ID = 4;
            $vn_ledger->TRX_TRAN_NO = $ac_vaucher_chd->TRX_TRAN_NO;
            $vn_ledger->VOUCHER_NO = $vaucher_mst_id;
            $vn_ledger->CR_AMT = $particular_amt;
            $vn_ledger->IS_ACTIVE = 1;
            $vn_ledger->save();
        }
    }
}
