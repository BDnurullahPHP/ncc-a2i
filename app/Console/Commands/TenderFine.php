<?php

namespace App\Console\Commands;

use App\Models\TenderInstallment;
use Illuminate\Console\Command;

class TenderFine extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ncc:tender-fine';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create voucher for due tender installment';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $installment = TenderInstallment::where('DATE', '<', date('Y-m-d'))
                                    ->where('IS_PAID', 0)
                                    ->where('IS_ACTIVE', 1)
                                    ->get();

        foreach ($installment as $key => $ins) {
            $ac_vaucher_mst = new AccountVaucherMst();
            $ac_vaucher_mst->VOUCHER_DT = date('Y-m-d H:i:s');
            if (!empty($ins->CITIZEN_ID)) {
                $ac_vaucher_mst->CITIZEN_ID = $ins->CITIZEN_ID;
            } else {
                $ac_vaucher_mst->TE_APP_ID = $ins->TE_APP_ID;
            }

            $ac_vaucher_mst->IS_ACTIVE = 1;
            $ac_vaucher_mst->save();

            $vaucher_mst_id = $ac_vaucher_mst->VOUCHER_NO;
            $particular_amt = ((float) $ins->AMOUNT * (float) $ins->PUNISHMENT) / 100;

            $ac_vaucher_chd = new AccountVaucherChd();
            $ac_vaucher_chd->TRX_TRAN_DT = date('Y-m-d H:i:s');
            $ac_vaucher_chd->VOUCHER_NO = $vaucher_mst_id;
            $ac_vaucher_chd->SCHEDULE_ID = $ins->SCHEDULE_ID;
            $ac_vaucher_chd->PARTICULAR_ID = 12;
            $ac_vaucher_chd->PUNIT_PRICE = $particular_amt;
            $ac_vaucher_chd->IS_ACTIVE = 1;
            $ac_vaucher_chd->save();

            $vn_ledger = new AccountVnLedger();
            $vn_ledger->VLEDGER_DT = date('Y-m-d H:i:s');
            $vn_ledger->TRX_CODE_ID = 4;
            $vn_ledger->TRX_TRAN_NO = $ac_vaucher_chd->TRX_TRAN_NO;
            $vn_ledger->VOUCHER_NO = $vaucher_mst_id;
            $vn_ledger->CR_AMT = $particular_amt;
            $vn_ledger->IS_ACTIVE = 1;
            $vn_ledger->save();
        }
    }
}
