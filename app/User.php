<?php

namespace App;

use App\Models\LeaseDemesne;
use App\Models\TenderApplicant;
use Config;
use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Zizaco\Entrust\Traits\EntrustUserTrait;

class User extends Model implements
    AuthenticatableContract,
                                    AuthorizableContract,
                                    CanResetPasswordContract
{
    use Authenticatable, Authorizable, CanResetPassword,
                EntrustUserTrait {
                    EntrustUserTrait::can insteadof Authorizable;
                }

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'sa_users';
    protected $primaryKey = 'USER_ID';
    const CREATED_AT = 'CREATED_AT';
    const UPDATED_AT = 'UPDATED_AT';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
                            'USER_TYPE',
                            'USERGRP_ID',
                            'USERLVL_ID',
                            'ORG_ID',
                            'FIRST_NAME',
                            'LAST_NAME',
                            'FULL_NAME',
                            'GENDER',
                            'USERNAME',
                            'EMAIL',
                            'PASSWORD',
                            'MOBILE_NO',
                            'IS_ACTIVE',
                            'RESET_TOKEN',
                            'CREATED_BY',
                            'UPDATED_BY',
                        ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['PASSWORD', 'REMEMBER_TOKEN'];

    // Override required, otherwise existing Authentication system will not match credentials
    public function getAuthPassword()
    {
        return $this->PASSWORD;
    }

    /**
     * User data collection.
     *
     * @param array $input
     *
     * @return Collection
     */
    public static function getData($input)
    {
        $data = self::with('roles')
                    ->orderBy('USER_ID', 'asc')
                    ->take($input['length'])
                    ->skip($input['start'])
                    ->get();

        $total = self::count();

        return ['data' => $data, 'total' => $total, 'filtered' => $total];
    }

    /**
     * Get user details.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function getDetails()
    {
        return $this->hasOne('App\Models\UserDetails', 'USER_ID', 'USER_ID');
    }

    /**
     * Get citizen details.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function getCitizen()
    {
        return $this->hasOne('App\Models\Citizen', 'USER_ID', 'USER_ID');
    }

    /**
     * Many-to-Many relations with Role.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function roles()
    {
        return $this->belongsToMany(Config::get('entrust.role'), Config::get('entrust.role_user_table'), 'USER_ID', 'ROLE_ID');
    }

    /**
     * Checks if the user has a role by its name.
     *
     * @param string|array $name       Role name or array of role names.
     * @param bool         $requireAll All roles in the array are required.
     *
     * @return bool
     */
    public function hasRole($name, $requireAll = false)
    {
        if (is_array($name)) {
            foreach ($name as $roleName) {
                $hasRole = $this->hasRole($roleName);

                if ($hasRole && !$requireAll) {
                    return true;
                } elseif (!$hasRole && $requireAll) {
                    return false;
                }
            }

            // If we've made it this far and $requireAll is FALSE, then NONE of the roles were found
            // If we've made it this far and $requireAll is TRUE, then ALL of the roles were found.
            // Return the value of $requireAll;
            return $requireAll;
        } else {
            foreach ($this->roles as $role) {
                if ($role->NAME == $name) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * Check if user has a permission by its name.
     *
     * @param string|array $permission Permission string or array of permissions.
     * @param bool         $requireAll All permissions in the array are required.
     *
     * @return bool
     */
    public function can($permission, $requireAll = false)
    {
        if (is_array($permission)) {
            foreach ($permission as $permName) {
                $hasPerm = $this->can($permName);

                if ($hasPerm && !$requireAll) {
                    return true;
                } elseif (!$hasPerm && $requireAll) {
                    return false;
                }
            }

            // If we've made it this far and $requireAll is FALSE, then NONE of the perms were found
            // If we've made it this far and $requireAll is TRUE, then ALL of the perms were found.
            // Return the value of $requireAll;
            return $requireAll;
        } else {
            foreach ($this->roles as $role) {
                // Validate against the Permission table
                foreach ($role->perms as $perm) {
                    if ($perm->NAME == $permission) {
                        return true;
                    }
                }
            }
        }

        return false;
    }

    /**
     * Get all system user.
     *
     * @return array
     */
    public static function getUserAll()
    {
        $user_data = [];
        $apt_data = [];
        $dem_data = [];
        $data['ctz_user'] = self::with('getCitizen')->where('IS_ACTIVE', 1)->where('USER_TYPE', 'C')->get();
        $data['applicant'] = TenderApplicant::where('IS_ACTIVE', 1)->get();
        $data['demesne'] = LeaseDemesne::where('IS_ACTIVE', 1)->get();

        foreach ($data['ctz_user'] as $key => $ctz) {
            if (!empty($ctz->getCitizen)) {
                $cobj = new \stdClass();
                $cobj->TYPE = 'C';
                $cobj->ID = $ctz->getCitizen->CITIZEN_ID;
                $cobj->NAME = $ctz->FULL_NAME;
                $user_data[] = $cobj;
            }
        }
        foreach ($data['applicant'] as $k => $ta) {
            $cobj = new \stdClass();
            $cobj->TYPE = 'T';
            $cobj->ID = $ta->TE_APP_ID;
            $cobj->NAME = $ta->APPLICANT_NAME;
            $apt_data[] = $cobj;
        }
        foreach ($data['demesne'] as $i => $ld) {
            $cobj = new \stdClass();
            $cobj->TYPE = 'L';
            $cobj->ID = $ld->LEASE_DE_ID;
            $cobj->NAME = $ld->NAME;
            $dem_data[] = $cobj;
        }

        return array_merge($user_data, $apt_data, $dem_data);
    }
}
