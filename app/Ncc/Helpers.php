<?php

// Code within app\Helpers\Helper.php

namespace App\Ncc;

use Config;
use Lang;
use Mail;
use mPDF;

/**
 * Global helper methods.
 *
 * @author Abdul awal <abdulawal@atilimited.net>
 */
class Helpers
{
    /**
     * Bangla array number.
     */
    public static $bn = ['১', '২', '৩', '৪', '৫', '৬', '৭', '৮', '৯', '০'];

    /**
     * English array number.
     */
    public static $en = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '0'];

    /**
     * Bangla to English number.
     *
     * @param string $number
     *
     * @return string
     */
    public static function bn2en($number)
    {
        return str_replace(self::$bn, self::$en, $number);
    }

    /**
     * English to Bangla number.
     *
     * @param string $number
     *
     * @return string
     */
    public static function en2bn($number)
    {
        return str_replace(self::$en, self::$bn, $number);
    }

    /**
     * Convert number based on locale.
     *
     * @param string $number
     *
     * @return string
     */
    public static function numberConvert($number)
    {
        $lang = Lang::locale();

        return $lang == 'bn' ? self::en2bn($number) : self::bn2en($number);
    }

    /**
     * Search array key within an array and return matched.
     *
     * @param string|int $key
     * @param array      $array
     *
     * @return mixed
     */
    public static function array_key_match($key, $array)
    {
        return isset($array[$key]) ? $array[$key] : false;
    }

    /**
     * Format input AM/PM based time and return 24 hours time with minute.
     *
     * @param string $input (dd-mm-YYY h:m A)
     *
     * @return string datetime
     */
    public static function full_time_format($input)
    {
        $input_format = str_replace('-', '/', $input);
        list($day, $month, $year, $hour, $minute, $dayType) = preg_split('/[\/\s:]+/', $input_format);

        return $year.'-'.$month.'-'.$day.' '.($dayType == 'PM' ? $hour + 12 : $hour).':'.$minute.':00';
    }

    /**
     * Make mPDF.
     *
     * @param string $pdf_data  HTML format of pdf content
     * @param string $file_name Name of pdf file
     * @param string $dest      View/download file
     *
     * @return pdf output
     */
    public static function makePDF($file_name, $pdf_data, $dest = 'I')
    {
        $mpdf = new mPDF();
        $mpdf->ignore_invalid_utf8 = true;
        $mpdf->setDefaultFont('freeserif,sans-serif,vrinda,solaiman-lipi,monospace');
        $mpdf->SetFont('freeserif,sans-serif,vrinda,solaiman-lipi,monospace');
        $mpdf->WriteHTML($pdf_data);

        return $mpdf->Output($file_name, $dest);
    }

    /**
     * Send email.
     *
     * @param array $data
     *
     * @return bool
     */
    public static function sendEmail($data, $from = false)
    {
        $from = $from ? $from : Config::get('mail.from');

        Mail::send('emails.'.$data['template'], $data, function ($m) use ($from, $data) {
            $m->from($from['address'], $from['name']);
            $m->to($data['to_email'], $data['to_name']);
            $m->subject($data['subject']);
        });

        return Mail::failures() > 0 ? false : true;
    }

    /**
     * Send SMS.
     *
     * @param string $number Recever SMS number
     * @param string $sms    SMS content
     * @param bool   $bn     Bangla/English SMS
     *
     * @return response
     */
    public static function sendSMS($number, $sms, $bn = true)
    {
        $config = Config::get('sms');
        $random = self::random_str(20);

        $param = 'user='.$config['user'].'&pass='.$config['password'];
        if ($bn) {
            $param .= '&sms[0][0]= '.$number.' &sms[0][1]=';
            $param .= strtoupper(bin2hex(iconv('UTF-8', 'UCS-2BE', $sms)));
            $param .= '&sms[0][2]='.$random.'&sid='.$config['sidbn'];
        } else {
            $param .= '&sms[0][0]= '.$number.' &sms[0][1]='.urlencode($sms);
            $param .= '&sms[0][2]='.$random.'&sid='.$config['sid'];
        }

        $crl = curl_init();
        curl_setopt($crl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($crl, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($crl, CURLOPT_URL, $config['url']);
        curl_setopt($crl, CURLOPT_HEADER, 0);
        curl_setopt($crl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($crl, CURLOPT_POST, 1);
        curl_setopt($crl, CURLOPT_POSTFIELDS, $param);
        $response = curl_exec($crl);
        curl_close($crl);

        return $response;
    }

    /**
     * Generate a random string, using a cryptographically secure
     * pseudorandom number generator (random_int).
     *
     * For PHP 7, random_int is a PHP core function
     * For PHP 5.x, depends on https://github.com/paragonie/random_compat
     *
     * @param int    $length   How many characters do we want?
     * @param string $keyspace A string of all possible characters
     *                         to select from
     *
     * @return string
     */
    public static function random_str($length, $keyspace = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ')
    {
        $str = '';
        $max = mb_strlen($keyspace, '8bit') - 1;
        for ($i = 0; $i < $length; $i++) {
            $str .= $keyspace[random_int(0, $max)];
        }

        return $str;
    }

    /**
     * Get time duration from a given date.
     *
     * @param string $date
     *
     * @return string
     */
    public static function getTime($date)
    {
        $diff = date_diff(date_create($date), date_create(date('Y-m-d H:i:s')));
        $min = $diff->format('%i');
        $sec = $diff->format('%s');
        $hour = $diff->format('%h');
        $mon = $diff->format('%m');
        $day = $diff->format('%d');
        $year = $diff->format('%y');

        if ($diff->format('%i%h%d%m%y') == '00000') {
            return $sec.' seconds ago';
        } elseif ($diff->format('%h%d%m%y') == '0000') {
            return $min.' minutes ago';
        } elseif ($diff->format('%d%m%y') == '000') {
            return $hour.' hours ago';
        } elseif ($diff->format('%m%y') == '00') {
            return $day.' days ago';
        } elseif ($diff->format('%y') == '0') {
            return $mon.' months ago';
        } else {
            return $year.' years ago';
        }
    }

    /**
     * Generate an asset path for the application.
     *
     * @param string $path
     * @param bool   $secure
     *
     * @return string
     */
    public static function asset($path, $secure = null)
    {
        return app('url')->asset(Config::get('app.base_asset').'/'.$path, $secure);
    }

    /**
     * Get the application base path.
     *
     * @param string $path
     *
     * @return string
     */
    public static function base_path($path = '')
    {
        return getcwd().($path ? DIRECTORY_SEPARATOR.$path : $path);
    }

    /**
     * Get the base asset path.
     *
     * @param string $path
     *
     * @return string
     */
    public static function asset_path($path = '')
    {
        return self::base_path().DIRECTORY_SEPARATOR.'public'.($path ? DIRECTORY_SEPARATOR.$path : $path);
    }
}
