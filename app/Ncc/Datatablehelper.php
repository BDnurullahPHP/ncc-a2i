<?php

namespace App\Ncc;

use App\Models\ApplicantProperty;
use App\Models\Bank;
use App\Models\Citizen;
use App\Models\CitizenProperty;
use App\Models\LandCategory;
use App\Models\LeaseCategory;
use App\Models\LeaseDemesne;
use App\Models\LeaseValidity;
use App\Models\Module;
use App\Models\ModuleGroup;
use App\Models\ModuleGroupLevel;
use App\Models\Particular;
use App\Models\Project;
use App\Models\ProjectCategory;
use App\Models\ProjectDetails;
use App\Models\Tender;
use App\Models\TenderDateTime;
use App\Models\TenderInstallment;
use App\Models\TenderSchedule;
use App\Models\Ward;
use App\Models\Zone;
use DB;

/**
 * Datatable helper methods.
 *
 * @author Abdul awal <abdulawal@atilimited.net>
 */
class Datatablehelper
{
    /**
     * Land datatable Output.
     *
     * @return array
     */
    public static function landOutput($input, $collection)
    {
        $data = [];
        $link_url = 'land_list'; // Module url
        $access = Module::checkPrevilege($link_url); // get access info by Module url
        if ($collection) {
            foreach ($collection['data'] as $key => $item) {
                $row = [];
                $row[] = $key + 1;
                $row[] = $item->LAND_NUMBER;
                $row[] = $item->getWord->WARD_NAME_BN;
                $row[] = $item->getMouza->MOUZA_NAME_BN;
                $row[] = $item->TOTAL_LAND;
                $row[] = date_format(date_create($item->CREATED_AT), 'd/m/Y');
                $action = '';

                if ($access->STATUS == 1) {
                    if ($item->IS_ACTIVE == 1) {
                        $action .= '<a href="'.route('land_status', $item->LAND_ID).'" data-isactive="1" class="label label-warning change_status">'.trans('common.table_inactive').'</a>';
                    } else {
                        $action .= '<a href="'.route('land_status', $item->LAND_ID).'" data-isactive="0" class="label label-success change_status">'.trans('common.table_active').'</a>';
                    }
                }
                if ($access->UPDATE == 1) {
                    $action .= '<a href="#" title="Edit land" class="label label-default" data-toggle="modal" data-target="#modal_big_content" data-form="'.route('land_edit_form', $item->LAND_ID).'"><i class="fa fa-pencil"></i></a>';
                }
                if ($access->READ == 1) {
                    $action .= '<a target="_blank" href="'.route('land_print', $item->LAND_ID).'" title="Print land" class="label label-success"><i class="fa fa-print"></i></a>';
                    $action .= '<a href="#" title="View land" class="label label-primary" data-toggle="modal" data-target="#modal_big_content" data-form="'.route('land_view', $item->LAND_ID).'"><i class="fa fa-eye"></i></a>';
                }
                if ($access->DELETE == 1) {
                    $action .= '<a href="'.route('land_delete', $item->LAND_ID).'" title="Delete land" class="label label-danger delete_post"><i class="fa fa-times"></i></a>';
                }
                $row[] = $action;
                $data[] = $row;
            }
        }

        return [
            'draw'              => $input['draw'],
            'recordsTotal'      => $collection['total'],
            'recordsFiltered'   => $collection['filtered'],
            'data'              => $data,
        ];
    }

    /**
     * Land report datatable Output.
     *
     * @return array
     */
    public static function landReportOutput($input, $collection)
    {
        $data = [];
        if ($collection) {
            foreach ($collection['data'] as $key => $item) {
                $row = [];
                $row[] = Helpers::en2bn($key + 1);
                $row[] = Helpers::en2bn($item->LAND_NUMBER);
                $row[] = Helpers::en2bn($item->getWord->WARD_NAME_BN);
                $row[] = Helpers::en2bn($item->getMouza->MOUZA_NAME_BN);
                $row[] = Helpers::en2bn($item->TOTAL_LAND);
                $row[] = Helpers::en2bn(date('d/m/Y', strtotime($item->CREATED_AT)));
                $action = '';

                $action .= '<a target="_blank" href="'.route('land_print', $item->LAND_ID).'" title="Print land" class="label label-warning"><i class="fa fa-print"></i></a>';
                $action .= '<a href="#" title="View land" class="label label-primary" data-toggle="modal" data-target="#modal_big_content" data-form="'.route('land_view', $item->LAND_ID).'"><i class="fa fa-eye"></i></a>';

                $row[] = $action;
                $data[] = $row;
            }
        }

        return [
            'draw'              => $input['draw'],
            'recordsTotal'      => $collection['total'],
            'recordsFiltered'   => $collection['filtered'],
            'data'              => $data,
        ];
    }

    /**
     * Tender schedule datatable Output.
     *
     * @return array
     */
    public static function tenderscheduleOutput($input, $collection)
    {
        $sl = 1;
        $data = [];
        $link_url = 'notice_list'; // module url
        $access = Module::checkPrevilege($link_url); // get access info by module url
        if (count($collection) > 0) {
            foreach ($collection['data'] as $key => $item) {
                $row = [];
                $row[] = $sl;
                if (isset($input['from_admin']) && $input['from_admin'] == true) {
                    $row[] = $item->getCategory->CATE_NAME;
                }
                $row[] = $item->getProject->PR_NAME_BN;
                $row[] = !empty($item->getProjectType) ? $item->getProjectType->TYPE_NAME : '';
                $row[] = !empty($item->getProjectDetail) ? Helpers::numberConvert(number_format($item->getProjectDetail->PR_MEASURMENT)) : '';
                $row[] = Helpers::numberConvert(number_format($item->LOWEST_TENDER_MONEY, 2));
                $row[] = Helpers::numberConvert(number_format($item->getCategoryParticulars->PARTICULAR_AMT, 2));

                if (count($item->getTenderParticulars) > 0) {
                    foreach ($item->getTenderParticulars as $index => $tp) {
                        if ($tp->UOM == 1) {
                            $uom = ' শতাংশ';
                        } elseif ($tp->UOM == 2) {
                            $uom = ' টাকা';
                        } else {
                            $uom = '';
                        }

                        if ($tp->PARTICULAR_ID == 4) {
                            $row[] = Helpers::numberConvert(number_format($tp->PARTICULAR_AMT, 2));
                        } elseif ($tp->PARTICULAR_ID == 3) {
                            $row[] = Helpers::numberConvert($tp->PARTICULAR_AMT).$uom;
                        } else {
                        }
                    }
                } else {
                    $row[] = '';
                    $row[] = '';
                }
                $action = '';
                $action .= '<a target="_blank" href="'.route('tender_print_portal', $item->TENDER_ID).'" title="Print" class="btn btn-info btn-xs"><i class="fa fa-print"></i>'.trans('tender.Details').'</a>';
                //$row[]  = '<a href="'.route('get_tender_conditions', [$item->getProjectDetail->PR_CATEGORY, $item->TENDER_ID]).'" title="show application Form" class="btn btn-success btn-xs">শর্ত সমূহ</a>';
                if (isset($input['from_admin']) && $input['from_admin'] == true) {
                    if ($access->CREATE == 1) {
                        $action .= '<a href="#" title="Apply" class="btn btn-success btn-xs" data-toggle="modal" data-target="#modal_big_content" data-form="'.route('tender_apply_form', [$item->SCHEDULE_ID, 'tender']).'">'.trans('citizen.Apply').'</a>';
                    }
                } else {
                    $action .= '<a href="'.route('get_tender_conditions', [$item->PR_CATEGORY, $item->TENDER_ID, $item->SCHEDULE_ID]).'" title="show application Form" class="btn btn-success btn-xs">'.trans('tender.Apply').'</a>';
                }
                $row[] = $action;
                $data[] = $row;
                $sl++;
            }
        }

        return [
            'draw'              => $input['draw'],
            'recordsTotal'      => $collection['total'],
            'recordsFiltered'   => $collection['filtered'],
            'data'              => $data,
        ];
    }

    /**
     * Lease tender schedule datatable Output.
     *
     * @return array
     */
    public static function leaseTenderscheduleOutput($input, $collection)
    {
        $data = [];
        $link_url = 'notice_list'; // module url
        $access = Module::checkPrevilege($link_url); // get access info by module url
        if (count($collection) > 0) {
            foreach ($collection['data'] as $key => $item) {
                $row = [];
                $row[] = $key + 1;
                $row[] = $item->CLAUSE;
                $row[] = !empty($item->getProject) ? $item->getProject->PR_NAME_BN : '';
                $row[] = !empty($item->getProject) ? $item->getProject->PROJECT_LOCATION : '';

                if (count($item->getTenderParticulars) > 0) {
                    foreach ($item->getTenderParticulars as $index => $tp) {
                        if ($tp->PARTICULAR_ID == 6 || $tp->PARTICULAR_ID == 7) {
                            $row[] = $tp->PARTICULAR_AMT;
                        }
                    }
                } else {
                    $row[] = '';
                    $row[] = '';
                }

                $row[] = '<a target="blank" href="'.route('get_lease_tender', $item->TENDER_ID).'" class="btn btn-info btn-xs" role="button">বিস্তারিত</a>';
                //$row[]  = '<a href="'.route('get_tender_conditions', [24,$item->TENDER_ID]).'" title="show Condition" class="btn btn-success btn-xs">শর্ত সমূহ</a>';
                $action = '';
                if (isset($input['from_admin']) && $input['from_admin'] == true) {
                    if ($access->CREATE == 1) {
                        $action .= '<a href="#" title="Apply" class="btn btn-success btn-xs" data-toggle="modal" data-target="#modal_big_content" data-form="'.route('tender_apply_form', [$item->SCHEDULE_ID, 'lease']).'">আবেদন করুন</a>';
                    }
                } else {
                    $action .= '<a href="'.route('get_tender_conditions', [$item->PR_CATEGORY, $item->TENDER_ID, $item->SCHEDULE_ID]).'" title="show application Form" class="btn btn-success btn-xs">আবেদন করুন</a>';
                }
                $row[] = $action;
                $data[] = $row;
            }
        }

        return [
            'draw'              => $input['draw'],
            'recordsTotal'      => $collection['total'],
            'recordsFiltered'   => $collection['filtered'],
            'data'              => $data,
        ];
    }

    /**
     * Lease datatable Output.
     *
     * @return array
     */
    public static function leaseOutput1($input, $collection)
    {
        $sl = 1;
        $data = [];
        if ($collection) {
            foreach ($collection['data'] as $key => $item) {
                $row = [];
                $row[] = $sl;
                $row[] = $item->land_number;
                $row[] = $item->land_owner;
                $row[] = $item->getArea->bn_name;
                $row[] = $item->getWord->bn_name;
                $row[] = $item->getMouza->bn_name;
                $row[] = $item->jl_no;
                $row[] = $item->khatian_no;
                $row[] = $item->total_land;
                $row[] = $item->possession_land;
                $row[] = $item->ousted_land;
                $dt = date_create($item->created_at);
                $dt = date_format($dt, 'd/m/Y');
                $row[] = $dt;

                $action = '';
                if ($item->is_active == 1) {
                    $action .= '<a href="#" data-isactive="1" class="label label-warning change_status">'.trans('common.table_inactive').'</a>';
                } else {
                    $action .= '<a href="#" data-isactive="0" class="label label-success change_status">'.trans('common.table_active').'</a>';
                }
                $action .= '<a href="#" class="label label-default" data-toggle="modal" data-target="#modal_add_content" data-form="#"><i class="fa fa-pencil"></i></a>';
                $action .= '<a href="#" class="label label-danger delete_post"><i class="fa fa-times"></i></a>';

                $row[] = $action;
                $data[] = $row;
                $sl++;
            }
        }

        $output = [
            'draw'            => $input['draw'],
            'recordsTotal'    => $collection['total'],
            'recordsFiltered' => $collection['filtered'],
            'data'            => $data,
        ];

        return $output;
    }

    /**
     * Lease datatable Output.
     *
     * @return array
     */
    public static function leaseOutput($input, $collection)
    {
        $sl = 1;
        $data = [];
        $link_url = 'lease_list'; // module url
        $access = Module::checkPrevilege($link_url); // get access info by module url
        if ($collection) {
            foreach ($collection['data'] as $key => $item) {
                $row = [];
                $row[] = $sl;
                $row[] = $item->PR_UD_ID;
                $row[] = $item->PR_NAME_BN;
                $row[] = $item->PROJECT_LOCATION;
                $action = '';

                if ($access->STATUS == 1) {
                    if ($item->IS_ACTIVE == 1) {
                        $action .= '<a href="'.route('project_status', $item->PROJECT_ID).'" data-isactive="1" class="label label-warning change_status">'.trans('common.table_inactive').'</a>';
                    } else {
                        $action .= '<a href="'.route('project_status', $item->PROJECT_ID).'" data-isactive="0" class="label label-success change_status">'.trans('common.table_active').'</a>';
                    }
                }
                if ($access->UPDATE == 1) {
                    $action .= '<a href="#" title="Edit project" class="label label-default" data-toggle="modal" data-target="#modal_add_content" data-form="'.route('lease_list_edit_form', $item->PROJECT_ID).'"><i class="fa fa-pencil"></i></a>';
                }
                if ($access->READ == 1) {
                    $action .= '<a href="#" title="View project" class="label label-primary" data-toggle="modal" data-target="#modal_add_content" data-form="'.route('lease_view', $item->PROJECT_ID).'"><i class="fa fa-eye"></i></a>';
                }
                if ($access->DELETE == 1) {
                    $action .= '<a href="'.route('project_delete', $item->PROJECT_ID).'" title="Delete project" class="label label-danger delete_post"><i class="fa fa-times"></i></a>';
                }
                $row[] = $action;
                $data[] = $row;
                $sl++;
            }
        }

        $output = [
            'draw'              => $input['draw'],
            'recordsTotal'      => $collection['total'],
            'recordsFiltered'   => $collection['filtered'],
            'data'              => $data,
        ];

        return $output;
    }

    /**
     * Land Category datatable Output.
     *
     * @return array
     */
    public static function datatableOutput($input, $collection)
    {
        $sl = 1;
        $data = [];
        $link_url = 'land_category'; // module url
        $access = Module::checkPrevilege($link_url); // get access info by module url
        if ($collection) {
            foreach ($collection['data'] as $key => $item) {
                $row = [];
                $row[] = $sl;
                $row[] = $item->CAT_NAME;
                $row[] = $item->CAT_NAME_BN;
                $dt = date_create($item->CREATED_AT);
                $dt = date_format($dt, 'd/m/Y');
                $row[] = $dt;

                $action = '';
                if ($access->STATUS == 1) {
                    if ($item->IS_ACTIVE == 1) {
                        $action .= '<a href="'.route('land_category_status', $item->CAT_ID).'" data-isactive="1" class="label label-warning change_status">'.trans('common.table_inactive').'</a>';
                    } else {
                        $action .= '<a href="'.route('land_category_status', $item->CAT_ID).'" data-isactive="0" class="label label-success change_status">'.trans('common.table_active').'</a>';
                    }
                }
                if ($access->UPDATE == 1) {
                    $action .= '<a href="#" class="label label-default" data-toggle="modal" data-target="#modal_add_content" data-form="'.route('category_edit_form', $item->CAT_ID).'"><i class="fa fa-pencil"></i></a>';
                }
                if ($access->DELETE == 1) {
                    $action .= '<a href="'.route('land_category_delete', $item->CAT_ID).'" class="label label-danger delete_post"><i class="fa fa-times"></i></a>';
                }
                $row[] = $action;
                $data[] = $row;
                $sl++;
            }
        }

        $output = [
            'draw'              => $input['draw'],
            'recordsTotal'      => $collection['total'],
            'recordsFiltered'   => $collection['filtered'],
            'data'              => $data,
        ];

        return $output;
    }

    /**
     * Project datatable Output.
     *
     *@author      Nurullah <nurul@atilimited.net>
     *
     * @return array
     */
    public static function marketOutput($input, $collection)
    {
        $sl = 1;
        $link_url = 'all_market'; // module url
        $access = Module::checkPrevilege($link_url); // get access info by module url
        $data = [];
        if ($collection) {
            foreach ($collection['data'] as $key => $item) {
                $row = [];
                $total_sfs = DB::table('project_details as pd')
                                ->leftJoin('sa_category as c', 'pd.PR_CATEGORY', '=', 'c.PR_CATEGORY')
                                ->select('CATE_NAME', DB::raw('count(*) as category_total'))
                                ->where('pd.PROJECT_ID', $item->PROJECT_ID)
                                ->where('c.IS_LEASE', 0)
                                ->groupBy('pd.PR_CATEGORY')
                                ->get();
                $row[] = $sl;
                $row[] = $item->PR_NAME_BN;
                $row[] = $item->PROJECT_LOCATION;
                $sfs = '';
                foreach ($total_sfs as $key => $value) {
                    $sfs .= $value->CATE_NAME.' = '.Helpers::numberConvert($value->category_total).'টি ';
                }
                $row[] = $sfs;
                $action = '';
                if ($access->STATUS == 1) {
                    if ($item->IS_ACTIVE == 1) {
                        $action .= '<a href="'.route('market_status', $item->PROJECT_ID).'" data-isactive="1" class="label label-warning change_status">'.trans('common.table_inactive').'</a>';
                    } else {
                        $action .= '<a href="'.route('market_status', $item->PROJECT_ID).'" data-isactive="0" class="label label-success change_status">'.trans('common.table_active').'</a>';
                    }
                }
                if ($access->UPDATE == 1) {
                    $action .= '<a href="#" title="Edit project" class="label label-default" data-toggle="modal" data-target="#modal_big_content" data-form="'.route('market_list_edit_form', $item->PROJECT_ID).'"><i class="fa fa-pencil"></i></a>';
                }
                if ($access->READ == 1) {
                    $action .= '<a href="#" title="View project" class="label label-primary" data-toggle="modal" data-target="#modal_big_content" data-form="'.route('market_view', $item->PROJECT_ID).'"><i class="fa fa-eye"></i></a>';
                    $action .= '<a target="_blank" href="'.route('market_print', $item->PROJECT_ID).'" title="Print" class="label label-success"><i class="fa fa-print"></i></a>';
                }
                if ($access->DELETE == 1) {
                    $action .= '<a href="'.route('market_delete', $item->PROJECT_ID).'" title="Delete project" class="label label-danger delete_post"><i class="fa fa-times"></i></a>';
                }
                $row[] = $action;
                $data[] = $row;
                $sl++;
            }
        }

        return [
            'draw'              => $input['draw'],
            'recordsTotal'      => $collection['total'],
            'recordsFiltered'   => $collection['filtered'],
            'data'              => $data,
        ];
    }

    /**
     * Project datatable Output.
     *
     *@author      Nurullah <nurul@atilimited.net>
     *
     * @return array
     */
    public static function marketDetailsOutput($input, $collection)
    {
        $sl = 1;
        $data = [];
        $link_url = 'all_market_details'; // module url
        $access = Module::checkPrevilege($link_url); // get access info by module url
        if ($collection) {
            foreach ($collection['data'] as $key => $item) {
                $row = [];
                $row[] = Helpers::numberConvert($sl);
                $row[] = $item->getProject->PR_NAME_BN;
                $row[] = $item->getProject->PROJECT_LOCATION;
                $row[] = $item->getCategory->CATE_NAME;
                $row[] = Helpers::numberConvert($item->getFloor->FLOOR_NUMBER_BN).' /'.Helpers::numberConvert($item->PR_SSF_NO);
                $row[] = Helpers::numberConvert($item->PR_MEASURMENT, 2).' sft';
                $action = '';

                if ($access->STATUS == 1) {
                    if ($item->IS_ACTIVE == 1) {
                        $action .= '<a href="'.route('market-details-status', $item->PR_DETAIL_ID).'" data-isactive="1" class="label label-warning change_status">'.trans('common.table_inactive').'</a>';
                    } else {
                        $action .= '<a href="'.route('market-details-status', $item->PR_DETAIL_ID).'" data-isactive="0" class="label label-success change_status">'.trans('common.table_active').'</a>';
                    }
                }
                if ($access->UPDATE == 1) {
                    $action .= '<a href="#" title="Edit project" class="label label-default" data-toggle="modal" data-target="#modal_big_content" data-form="'.route('market_details_edit_form', $item->PR_DETAIL_ID).'"><i class="fa fa-pencil"></i></a>';
                }
                if ($access->READ == 1) {
                    $action .= '<a href="#" title="View project" class="label label-primary" data-toggle="modal" data-target="#modal_big_content" data-form="'.route('project_view', $item->PROJECT_ID).'"><i class="fa fa-eye"></i></a>';
                }
                if ($access->DELETE == 1) {
                    $action .= '<a href="'.route('project_details_delete', $item->PR_DETAIL_ID).'" title="Delete project" class="label label-danger delete_post"><i class="fa fa-times"></i></a>';
                }
                $row[] = $action;
                $data[] = $row;
                $sl++;
            }
        }

        return [
            'draw'              => $input['draw'],
            'recordsTotal'      => $collection['total'],
            'recordsFiltered'   => $collection['filtered'],
            'data'              => $data,
        ];
    }

    /**
     * Project datatable Output.
     *
     * @return array
     */
    public static function projectOutput($input, $collection)
    {
        $sl = 1;
        $data = [];
        if ($collection) {
            foreach ($collection['data'] as $key => $item) {
                $row = [];
                $row[] = $sl;
                $row[] = $item->PR_UD_ID;
                $row[] = $item->PR_NAME_BN;
                $row[] = $item->PROJECT_LOCATION;
                $dt = date_create($item->PR_END_DT);
                $dt = date_format($dt, 'd/m/Y');
                $row[] = $dt;
                $action = '';

                if ($item->IS_ACTIVE == 1) {
                    $action .= '<a href="'.route('project_status', $item->PROJECT_ID).'" data-isactive="1" class="label label-warning change_status">'.trans('common.table_inactive').'</a>';
                } else {
                    $action .= '<a href="'.route('project_status', $item->PROJECT_ID).'" data-isactive="0" class="label label-success change_status">'.trans('common.table_active').'</a>';
                }
                $action .= '<a href="#" title="Edit project" class="label label-default" data-toggle="modal" data-target="#modal_big_content" data-form="'.route('project_list_edit_form', $item->PROJECT_ID).'"><i class="fa fa-pencil"></i></a>';
                $action .= '<a href="#" title="View project" class="label label-primary" data-toggle="modal" data-target="#modal_big_content" data-form="'.route('project_view', $item->PROJECT_ID).'"><i class="fa fa-eye"></i></a>';
                $action .= '<a href="'.route('project_delete', $item->PROJECT_ID).'" title="Delete project" class="label label-danger delete_post"><i class="fa fa-times"></i></a>';

                $row[] = $action;
                $data[] = $row;
                $sl++;
            }
        }

        return [
            'draw'              => $input['draw'],
            'recordsTotal'      => $collection['total'],
            'recordsFiltered'   => $collection['filtered'],
            'data'              => $data,
        ];
    }

    /**
     * Tender datatable Output.
     *
     * @return array
     */
    public static function tenderOutput($input, $collection)
    {
        $sl = 1;
        $data = [];
        $link_url = 'tender_list'; // module url
        $access = Module::checkPrevilege($link_url); // get access info by module url
        if ($collection) {
            foreach ($collection['data'] as $key => $item) {
                $row = [];
                $row[] = Helpers::numberConvert($sl);
                $row[] = Helpers::numberConvert($item->TENDER_NO).'<br/>তারিখ:- '.Helpers::numberConvert(date_format(date_create($item->TENDER_DT), 'd/m/Y'));
                //$row[]  = Helpers::numberConvert(date_format(date_create($item->TENDER_DT), 'd/m/Y'));
                $row[] = $item->TENDER_TITLE;
                $row[] = Helpers::numberConvert(date_format(date_create($item->TENDER_PUBLISH_DT), 'd/m/Y'));
                $row[] = Helpers::numberConvert(date_format(date_create($item->LAST_SELLING_DT_FROM), 'd/m/Y'));
                $action = '';

                if ($access->STATUS == 1) {
                    if ($item->IS_ACTIVE == 1) {
                        $action .= '<a href="'.route('tender_status', $item->TENDER_ID).'" data-isactive="1" class="label label-warning change_status">'.trans('common.table_inactive').'</a>';
                    } else {
                        $action .= '<a href="'.route('tender_status', $item->TENDER_ID).'" data-isactive="0" class="label label-success change_status">'.trans('common.table_active').'</a>';
                    }
                }
                if ($access->UPDATE == 1) {
                    $action .= '<a href="#" title="Edit tender" class="label label-default" data-toggle="modal" data-target="#tender_add_content" data-form="'.route('tender_edit', $item->TENDER_ID).'"><i class="fa fa-pencil"></i></a>';
                }
                if ($access->READ == 1) {
                    $action .= '<a target="_blank" href="'.route('tender_print', $item->TENDER_ID).'" title="Print" class="label label-success"><i class="fa fa-print"></i></a>';
                    $action .= '<a href="#" title="View tender" class="label label-primary" data-toggle="modal" data-target="#modal_big_content" data-form="'.route('tender_view', $item->TENDER_ID).'"><i class="fa fa-eye"></i></a>';
                }
                if ($access->DELETE == 1) {
                    $action .= '<a href="'.route('tender_delete', $item->TENDER_ID).'" title="Delete tender" class="label label-danger delete_post"><i class="fa fa-times"></i></a>';
                }
                $row[] = $action;
                $data[] = $row;
                $sl++;
            }
        }

        return [
            'draw'              => $input['draw'],
            'recordsTotal'      => $collection['total'],
            'recordsFiltered'   => $collection['filtered'],
            'data'              => $data,
        ];
    }

    /**
     * Lease datatable Output.
     *
     * @return array
     */
    public static function leaseTenderOutput($input, $collection)
    {
        $sl = 1;
        $data = [];
        $link_url = 'lease_notice'; // module url
        $access = Module::checkPrevilege($link_url); // get access info by module url
        if ($collection) {
            foreach ($collection['data'] as $key => $item) {
                $tender_date = TenderDateTime::where('TENDER_ID', $item->TENDER_ID)->first();
                $row = [];
                $row[] = $sl;
                $row[] = $item->TENDER_NO.'<br/>('.Helpers::numberConvert(date_format(date_create($item->TENDER_DT), 'd/m/Y')).')';
                $row[] = $item->TENDER_TITLE;
                $row[] = Helpers::numberConvert(date_format(date_create($item->TENDER_PUBLISH_DT), 'd/m/Y'));
                $row[] = Helpers::numberConvert(date_format(date_create($tender_date->LAST_SELLING_DT_FROM), 'd/m/Y'));
                $row[] = Helpers::numberConvert(date_format(date_create($tender_date->TE_OPENING_DT), 'd/m/Y H:i'));
                $action = '';

                if ($access->STATUS == 1) {
                    if ($item->IS_ACTIVE == 1) {
                        $action .= '<a href="'.route('lease_tender_status', $item->TENDER_ID).'" data-isactive="1" class="label label-warning change_status">'.trans('common.table_inactive').'</a>';
                    } else {
                        $action .= '<a href="'.route('lease_tender_status', $item->TENDER_ID).'" data-isactive="0" class="label label-success change_status">'.trans('common.table_active').'</a>';
                    }
                }
                if ($access->UPDATE == 1) {
                    $action .= '<a href="#" title="Edit tender" class="label label-default" data-toggle="modal" data-target="#tender_add_content" data-form="'.route('lease_tender_edit', $item->TENDER_ID).'"><i class="fa fa-pencil"></i></a>';
                }
                if ($access->READ == 1) {
                    $action .= '<a href="#" title="View tender" class="label label-primary" data-toggle="modal" data-target="#modal_big_content" data-form="'.route('lease_tender_view', $item->TENDER_ID).'"><i class="fa fa-eye"></i></a>';
                }
                if ($access->DELETE == 1) {
                    $action .= '<a href="'.route('lease_tender_delete', $item->TENDER_ID).'" title="Delete tender" class="label label-danger delete_post"><i class="fa fa-times"></i></a>';
                }
                $row[] = $action;
                $data[] = $row;
                $sl++;
            }
        }

        return [
            'draw'              => $input['draw'],
            'recordsTotal'      => $collection['total'],
            'recordsFiltered'   => $collection['filtered'],
            'data'              => $data,
        ];
    }

    /**
     * Tender bid comparison datatable Output.
     *
     * @param $input
     * @param $collection
     *
     * @return array
     */
    public static function tenderComparisonOutput($input, $collection)
    {
        $sl = 1;
        $data = [];
        $link_url = 'tender_applicant_comparison'; // module url
        $access = Module::checkPrevilege($link_url); // get access info by module url
        if ($collection) {
            foreach ($collection['data'] as $key => $item) {
                $row = [];
                $row[] = $sl;
                if ($item->IS_CITIZEN == 0) {
                    if (!empty($item->getApplicant->getThana->THANA_ENAME)) {
                        $thana = $item->getApplicant->getThana->THANA_ENAME.', ';
                        $adrs = $thana.$item->getApplicant->ROAD_NO.', '.$item->getApplicant->HOLDING_NO;
                    } else {
                        $thana = '';
                    }
                } else {
                    if (!empty($item->getCitizen->getThana->THANA_ENAME)) {
                        $thana = $item->getCitizen->getThana->THANA_ENAME.', ';
                        $adrs = $thana.$item->getCitizen->ROAD_NO.', '.$item->getCitizen->HOLDING_NO;
                    } else {
                        $thana = '';
                    }
                }
                if ($item->IS_CITIZEN == 0) {
                    $row[] = '<a href="#" title="View Applicant Details" data-toggle="modal" data-target="#modal_big_content" data-form="'.route('tender_applicant_view', ['applicant', $item->TE_APP_ID, $item->SCHEDULE_ID]).'">'
                             .$item->getApplicant->APPLICANT_NAME.'</a><br>'.$adrs;
                } else {
                    $row[] = '<a href="#" title="View Applicant Details" data-toggle="modal" data-target="#modal_big_content" data-form="'.route('tender_applicant_view', ['citizen', $item->CITIZEN_ID, $item->SCHEDULE_ID]).'">'
                             .$item->getCitizen->getTenderCitizen->FULL_NAME.'</a><br>'.$adrs;
                }
                $row[] = '<a href="#" title="Tender Info" data-toggle="modal" data-target="#modal_big_content" data-form="'.route('tender_view', $item->TENDER_ID).'">
                         '.$item->getTender->TENDER_NO.'</a>';

                $row[] = Helpers::numberConvert(number_format($item->cityCorporationRate->LOWEST_TENDER_MONEY, 2));
                $row[] = Helpers::numberConvert(number_format($item->BID_AMOUNT, 2));

                if ($item->IS_TE_SC_BOUGHT == 1) {
                    $row[] = '<span class="label label-success">'.trans('common.paid').'</span>';
                } else {
                    $row[] = '<span class="label label-danger">'.trans('common.unpaid').'</span>';
                }
                $action = '';
                if ($access->STATUS == 1) {
                    if ($item->IS_CANCEL == 1) {
                        $action .= '<a href="#" class="label label-success" data-toggle="modal" data-target="#modal_add_content" data-form="'.route('tender_cancel_form', [$item->IS_CITIZEN == 0 ? 'applicant/'.$item->TE_APP_ID : 'citizen/'.$item->CITIZEN_ID]).'">'.trans('common.table_active').'</a>';
                    } else {
                        $action .= '<a href="#" class="label label-warning" data-toggle="modal" data-target="#modal_add_content" data-form="'.route('tender_cancel_form', [$item->IS_CITIZEN == 0 ? 'applicant/'.$item->TE_APP_ID : 'citizen/'.$item->CITIZEN_ID]).'">'.trans('common.cancel').'</a>';
                    }
                }
                if ($access->UPDATE == 1) {
                    $action .= '<a href="#" title="Tender remarks" class="label label-default" data-toggle="modal" data-target="#modal_add_content" data-form="'.route('tender_remarks_form', [$item->IS_CITIZEN == 0 ? 'applicant/'.$item->TE_APP_ID : 'citizen/'.$item->CITIZEN_ID]).'"><i class="fa fa-commenting"></i></a>';
                }
                if ($access->CREATE == 1) {
                    $action .= '<a href="#" title="Select Primary Applicant" class="label label-success" data-toggle="modal" data-target="#modal_big_content" data-form="'.route('tender_primary_select_form', [$item->SCHEDULE_ID, $item->IS_CITIZEN == 0 ? 'applicant/'.$item->TE_APP_ID : 'citizen/'.$item->CITIZEN_ID]).'"><i class="fa fa-check"></i></a>';
                }
                $row[] = $action;
                $data[] = $row;
                $sl++;
            }
        }

        return [
            'draw'              => $input['draw'],
            'recordsTotal'      => $collection['total'],
            'recordsFiltered'   => $collection['filtered'],
            'data'              => $data,
        ];
    }

    /**
     * lease bid comparison datatable Output.
     *
     * @param $input
     * @param $collection
     *
     * @return array
     */
    public static function leaseComparisonOutput($input, $collection)
    {
        $sl = 1;
        $data = [];
        $link_url = 'lease_comparison'; // module url
        $access = Module::checkPrevilege($link_url); // get access info by module url
        if ($collection) {
            foreach ($collection['data'] as $key => $item) {
                $row = [];
                $row[] = $sl;
                if ($item->IS_CITIZEN == 0) {
                    if (!empty($item->getApplicant->getThana->THANA_ENAME)) {
                        $thana = $item->getApplicant->getThana->THANA_ENAME.', ';
                        $adrs = $thana.$item->getApplicant->ROAD_NO.', '.$item->getApplicant->HOLDING_NO;
                    } else {
                        $thana = '';
                    }
                } else {
                    if (!empty($item->getCitizen->getThana->THANA_ENAME)) {
                        $thana = $item->getCitizen->getThana->THANA_ENAME.', ';
                        $adrs = $thana.$item->getCitizen->ROAD_NO.', '.$item->getCitizen->HOLDING_NO;
                    } else {
                        $thana = '';
                    }
                }

                if ($item->IS_CITIZEN == 0) {
                    $row[] = '<a href="#" title="View Applicant Details" data-toggle="modal" data-target="#modal_big_content" data-form="'.route('tender_applicant_view', ['applicant', $item->TE_APP_ID, $item->SCHEDULE_ID]).'">'
                             .$item->getApplicant->APPLICANT_NAME.'</a><br>'.$adrs;
                } else {
                    $row[] = '<a href="#" title="View Applicant Details" data-toggle="modal" data-target="#modal_big_content" data-form="'.route('tender_applicant_view', ['citizen', $item->CITIZEN_ID, $item->SCHEDULE_ID]).'">'
                             .$item->getCitizen->getTenderCitizen->FULL_NAME.'</a><br>'.$adrs;
                }

                $row[] = '<a href="#" title="Tender Info" data-toggle="modal" data-target="#modal_big_content" data-form="'.route('lease_tender_view', $item->TENDER_ID).'">
                         '.$item->getTender->TENDER_NO.'</a>';
                $row[] = $item->getProject->PR_NAME_BN;
                $row[] = Helpers::numberConvert(number_format($item->BID_AMOUNT, 2));
                $bnk_info = $item->B_DRAFT_NO.', <br>তারিখ: '.Helpers::numberConvert(date_format(date_create($item->B_DRAFT_DATE), 'd/m/Y'));

                if ($item->IS_TE_SC_BOUGHT == 1) {
                    $row[] = '<span class="label label-success">'.trans('common.paid').'</span>';
                } else {
                    $row[] = '<span class="label label-danger">'.trans('common.unpaid').'</span>';
                }
                $action = '';
                if ($access->STATUS == 1) {
                    if ($item->IS_CANCEL == 1) {
                        $action .= '<a href="#" class="label label-success" data-toggle="modal" data-target="#modal_add_content" data-form="'.route('lease_cancel_form', [$item->IS_CITIZEN == 0 ? 'applicant/'.$item->TE_APP_ID : 'citizen/'.$item->CITIZEN_ID]).'">'.trans('common.table_active').'</a>';
                    } else {
                        $action .= '<a href="#" class="label label-warning" data-toggle="modal" data-target="#modal_add_content" data-form="'.route('lease_cancel_form', [$item->IS_CITIZEN == 0 ? 'applicant/'.$item->TE_APP_ID : 'citizen/'.$item->CITIZEN_ID]).'">'.trans('common.cancel').'</a>';
                    }
                }
                if ($access->UPDATE == 1) {
                    $action .= '<a href="#" title="Tender remarks" class="label label-default" data-toggle="modal" data-target="#modal_add_content" data-form="'.route('lease_remarks_form', [$item->IS_CITIZEN == 0 ? 'applicant/'.$item->TE_APP_ID : 'citizen/'.$item->CITIZEN_ID]).'"><i class="fa fa-commenting"></i></a>';
                }
                if ($access->CREATE == 1) {
                    $action .= '<a href="#" title="Select Primary Applicant" class="label label-success" data-toggle="modal" data-target="#modal_big_content" data-form="'.route('lease_primary_select_form', [$item->SCHEDULE_ID, $item->IS_CITIZEN == 0 ? 'applicant/'.$item->TE_APP_ID : 'citizen/'.$item->CITIZEN_ID]).'"><i class="fa fa-check"></i></a>';
                }
                $row[] = $action;
                $data[] = $row;
                $sl++;
            }
        }

        return [
            'draw'              => $input['draw'],
            'recordsTotal'      => $collection['total'],
            'recordsFiltered'   => $collection['filtered'],
            'data'              => $data,
        ];
    }

    /**
     * Tender bid comparison datatable Output.
     *
     * @param $input
     * @param $collection
     *
     * @return array
     */
    public static function tenderPrimaryOutput($input, $collection)
    {
        $sl = 1;
        $data = [];
        $link_url = 'tender_primary_selected_applicant_list'; // module url
        $access = Module::checkPrevilege($link_url); // get access info by module url
        if ($collection) {
            foreach ($collection['data'] as $key => $item) {
                $row = [];
                $row[] = $sl;
                if ($item->IS_CITIZEN == 0) {
                    if (!empty($item->getApplicant->getThana->THANA_ENAME)) {
                        $thana = $item->getApplicant->getThana->THANA_ENAME.', ';
                        $adrs = $thana.$item->getApplicant->ROAD_NO.', '.$item->getApplicant->HOLDING_NO;
                    } else {
                        $thana = '';
                    }
                } else {
                    if (!empty($item->getCitizen->getThana->THANA_ENAME)) {
                        $thana = $item->getCitizen->getThana->THANA_ENAME.', ';
                        $adrs = $thana.$item->getCitizen->ROAD_NO.', '.$item->getCitizen->HOLDING_NO;
                    } else {
                        $thana = '';
                    }
                }
                if ($item->IS_CITIZEN == 0) {
                    $row[] = '<a href="#" title="View Applicant Details" data-toggle="modal" data-target="#modal_big_content" data-form="'.route('tender_applicant_view', ['applicant', $item->TE_APP_ID, $item->SCHEDULE_ID]).'">'
                             .$item->getApplicant->APPLICANT_NAME.'</a><br>'.$adrs;
                } else {
                    $row[] = '<a href="#" title="View Applicant Details" data-toggle="modal" data-target="#modal_big_content" data-form="'.route('tender_applicant_view', ['citizen', $item->CITIZEN_ID, $item->SCHEDULE_ID]).'">'
                             .$item->getCitizen->getTenderCitizen->FULL_NAME.'</a><br>'.$adrs;
                }
                $row[] = '<a href="#" title="Tender Info" data-toggle="modal" data-target="#modal_big_content" data-form="'.route('tender_view', $item->TENDER_ID).'">
                         '.$item->getTender->TENDER_NO.'</a>';
                $row[] = Helpers::numberConvert(number_format($item->cityCorporationRate->LOWEST_TENDER_MONEY, 2));
                $row[] = Helpers::numberConvert(number_format($item->BID_AMOUNT, 2));
                if ($item->IS_TE_SC_BOUGHT == 1) {
                    $row[] = '<span class="label label-success">'.trans('common.paid').'</span>';
                } else {
                    $row[] = '<span class="label label-danger">'.trans('common.unpaid').'</span>';
                }
                $action = '';
                if ($access->READ == 1) {
                    $action .= '<a href="'.route('tender_primary_letter_print', [$item->SCHEDULE_ID, $item->IS_CITIZEN == 0 ? 'applicant/'.$item->TE_APP_ID : 'citizen/'.$item->CITIZEN_ID]).'" target="_blank" title="Primary Select Letter print" class="label label-success"><i class="fa fa-envelope"></i></a>';
                }
                $row[] = $action;

                $data[] = $row;
                $sl++;
            }
        }

        return [
            'draw'              => $input['draw'],
            'recordsTotal'      => $collection['total'],
            'recordsFiltered'   => $collection['filtered'],
            'data'              => $data,
        ];
    }

    /**
     * Tender bid comparison datatable Output.
     *
     * @param $input
     * @param $collection
     *
     * @return array
     */
    public static function leasePrimaryOutput($input, $collection)
    {
        $sl = 1;
        $data = [];
        $link_url = 'lease_primary_selected_applicant_list'; // module url
        $access = Module::checkPrevilege($link_url); // get access info by module url
        if ($collection) {
            foreach ($collection['data'] as $key => $item) {
                $row = [];
                $row[] = $sl;
                if ($item->IS_CITIZEN == 0) {
                    if (!empty($item->getApplicant->getThana->THANA_ENAME)) {
                        $thana = $item->getApplicant->getThana->THANA_ENAME.', ';
                        $adrs = $thana.$item->getApplicant->ROAD_NO.', '.$item->getApplicant->HOLDING_NO;
                    } else {
                        $thana = '';
                    }
                } else {
                    if (!empty($item->getCitizen->getThana->THANA_ENAME)) {
                        $thana = $item->getCitizen->getThana->THANA_ENAME.', ';
                        $adrs = $thana.$item->getCitizen->ROAD_NO.', '.$item->getCitizen->HOLDING_NO;
                    } else {
                        $thana = '';
                    }
                }
                if ($item->IS_CITIZEN == 0) {
                    $row[] = '<a href="#" title="View Applicant Details" data-toggle="modal" data-target="#modal_big_content" data-form="'.route('tender_applicant_view', ['applicant', $item->TE_APP_ID, $item->SCHEDULE_ID]).'">'
                             .$item->getApplicant->APPLICANT_NAME.'</a><br>'.$adrs;
                } else {
                    $row[] = '<a href="#" title="View Applicant Details" data-toggle="modal" data-target="#modal_big_content" data-form="'.route('tender_applicant_view', ['citizen', $item->CITIZEN_ID, $item->SCHEDULE_ID]).'">'
                             .$item->getCitizen->getTenderCitizen->FULL_NAME.'</a><br>'.$adrs;
                }

                $row[] = '<a href="#" title="Lease Info" data-toggle="modal" data-target="#modal_big_content" data-form="'.route('lease_tender_view', $item->TENDER_ID).'">
                         '.$item->getTender->TENDER_NO.'</a>';
                $row[] = $item->getProject->PR_NAME_BN;
                $row[] = number_format($item->BID_AMOUNT, 2);
                if ($item->IS_TE_SC_BOUGHT == 1) {
                    $row[] = '<span class="label label-success">'.trans('common.paid').'</span>';
                } else {
                    $row[] = '<span class="label label-danger">'.trans('common.unpaid').'</span>';
                }
                $action = '';
                if ($access->READ == 1) {
                    $action .= '<a href="'.route('lease_primary_letter_print', [$item->SCHEDULE_ID, $item->IS_CITIZEN == 0 ? 'applicant/'.$item->TE_APP_ID : 'citizen/'.$item->CITIZEN_ID]).'" target="_blank" title="Primary Select Letter print" class="label label-success pull-right"><i class="fa fa-envelope"></i></a> ';
                }
                $row[] = $action;
                $data[] = $row;
                $sl++;
            }
        }

        return [
            'draw'              => $input['draw'],
            'recordsTotal'      => $collection['total'],
            'recordsFiltered'   => $collection['filtered'],
            'data'              => $data,
        ];
    }

    /**
     * Tender bid comparison datatable Output.
     *
     * @param $input
     * @param $collection
     *
     * @return array
     */
    public static function tenderListOutput($input, $collection)
    {
        $sl = 1;
        $data = [];
        if ($collection) {
            foreach ($collection['data'] as $key => $item) {
                $row = [];
                $row[] = $sl;
                if ($item->IS_CITIZEN == 0) {
                    if (!empty($item->getApplicant->getThana->THANA_ENAME)) {
                        $thana = $item->getApplicant->getThana->THANA_ENAME.', ';
                        $adrs = $thana.$item->getApplicant->ROAD_NO.', '.$item->getApplicant->HOLDING_NO;
                    } else {
                        $thana = '';
                    }
                } else {
                    if (!empty($item->getCitizen->getThana->THANA_ENAME)) {
                        $thana = $item->getCitizen->getThana->THANA_ENAME.', ';
                        $adrs = $thana.$item->getCitizen->ROAD_NO.', '.$item->getCitizen->HOLDING_NO;
                    } else {
                        $thana = '';
                    }
                }
                if ($item->IS_CITIZEN == 0) {
                    $row[] = '<a href="#" title="View Applicant Details" data-toggle="modal" data-target="#modal_big_content" data-form="'.route('tender_applicant_view', ['applicant', $item->TE_APP_ID, $item->SCHEDULE_ID]).'">'
                             .$item->getApplicant->APPLICANT_NAME.'</a><br>'.$adrs;
                } else {
                    $row[] = '<a href="#" title="View Applicant Details" data-toggle="modal" data-target="#modal_big_content" data-form="'.route('tender_applicant_view', ['citizen', $item->CITIZEN_ID, $item->SCHEDULE_ID]).'">'
                             .$item->getCitizen->getTenderCitizen->FULL_NAME.'</a><br>'.$adrs;
                }
                $row[] = '<a href="#" title="Tender Info" data-toggle="modal" data-target="#modal_big_content" data-form="'.route($item->IS_LEASE == 1 ? 'lease_tender_view' : 'tender_view', $item->TENDER_ID).'">
                         '.$item->getTender->TENDER_NO.'</a>';
                $row[] = $item->getProject->PR_NAME_BN;
                $row[] = $item->IS_CITIZEN == 0 ? $item->getApplicant->EMAIL : $item->getCitizen->getTenderCitizen->EMAIL;
                $row[] = $item->IS_CITIZEN == 0 ? $item->getApplicant->APPLICANT_PHONE : $item->getCitizen->getTenderCitizen->MOBILE_NO;
                if ($item->IS_TE_SC_BOUGHT == 1) {
                    $row[] = '<span class="label label-success">'.trans('common.paid').'</span>';
                } else {
                    $row[] = '<span class="label label-danger">'.trans('common.unpaid').'</span>';
                }
                $data[] = $row;
                $sl++;
            }
        }

        return [
            'draw'              => $input['draw'],
            'recordsTotal'      => $collection['total'],
            'recordsFiltered'   => $collection['filtered'],
            'data'              => $data,
        ];
    }

    /**
     * Tender applicant payment history datatable Output.
     *
     * @return array
     */
    public static function applicantPaymentOutput($input, $collection)
    {
        $sl = 1;
        $data = [];
        if ($collection) {
            foreach ($collection['data'] as $key => $item) {
                $row = [];
                $row[] = $sl;

                if ($item->PAYMENT_TYPE == 1) {
                    $row[] = trans('common.bank_draft');
                    $row[] = trans('common.bank_draft_number').': '.$item->B_DRAFT_NO;
                } elseif ($item->PAYMENT_TYPE == 3) {
                    $row[] = trans('common.cash');
                    $row[] = trans('common.challan_no').': '.$item->B_DRAFT_NO;
                } else {
                    $row[] = trans('common.online');
                    $row[] = trans('common.bank_tran_number').': '.$item->BANK_TRAN_ID;
                }

                $row[] = $item->getVoucherChd->getParticular->PARTICULAR_NAME;
                $row[] = number_format($item->AMOUNT, 2).' '.trans('common.taka');
                $row[] = date('d/m/Y g:i A', strtotime($item->TRAN_DATE));
                // if ($item->STATUS == 'VALID') {
                //     $row[] = '<span class="label label-success">'.trans('common.paid').'</span>';
                // } else {
                //     $row[]  = '<span class="label label-danger">'.strtolower($item->STATUS).'</span>';
                // }
                //$row[]  = '<a href="#" title="Details" class="label label-default"><i class="fa fa-eye"></i></a>';
                $data[] = $row;
                $sl++;
            }
        }

        return [
            'draw'              => $input['draw'],
            'recordsTotal'      => $collection['total'],
            'recordsFiltered'   => $collection['filtered'],
            'data'              => $data,
        ];
    }

    /**
     * Tender datatable Output.
     *
     * @return array
     */
    public static function categoryParticularOutput($input, $collection)
    {
        $sl = 1;
        $data = [];
        $link_url = 'category_particular'; // module url
        $access = Module::checkPrevilege($link_url); // get access info by module url
        if ($collection) {
            foreach ($collection['data'] as $key => $item) {
                $row = [];
                $row[] = $sl;
                $row[] = $item->getCategory->CATE_NAME;
                $row[] = $item->getParticular->PARTICULAR_NAME;
                if ($item->UOM == 1) {
                    $uom = '%';
                } elseif ($item->UOM == 2) {
                    $uom = trans('common.taka');
                } else {
                    $uom = '';
                }
                $row[] = $item->PARTICULAR_AMT.' '.$uom;
                $row[] = date_format(date_create($item->CREATED_AT), 'd/m/Y');
                $action = '';

                if ($access->STATUS == 1) {
                    if ($item->IS_ACTIVE == 1) {
                        $action .= '<a href="'.route('category_particular_status', $item->CAT_PART_ID).'" data-isactive="1" class="label label-warning change_status">'.trans('common.table_inactive').'</a>';
                    } else {
                        $action .= '<a href="'.route('category_particular_status', $item->CAT_PART_ID).'" data-isactive="0" class="label label-success change_status">'.trans('common.table_active').'</a>';
                    }
                }
                if ($access->UPDATE == 1) {
                    $action .= '<a href="#" title="Edit category particular" class="label label-default" data-toggle="modal" data-target="#modal_add_content" data-form="'.route('category_particular_edit_form', $item->CAT_PART_ID).'"><i class="fa fa-pencil"></i></a>';
                }
                if ($access->DELETE == 1) {
                    $action .= '<a href="'.route('category_particular_delete', $item->CAT_PART_ID).'" title="Delete category particular" class="label label-danger delete_post"><i class="fa fa-times"></i></a>';
                }
                $row[] = $action;
                $data[] = $row;
                $sl++;
            }
        }

        return [
            'draw'              => $input['draw'],
            'recordsTotal'      => $collection['total'],
            'recordsFiltered'   => $collection['filtered'],
            'data'              => $data,
        ];
    }

    /**
     * zone datatable Output.
     *
     * @return array
     */
    public static function zonedatatableOutput($input, $collection)
    {
        $sl = 1;
        $data = [];
        $link_url = 'zone_list'; // module url
        $access = Module::checkPrevilege($link_url); // get access info by module url
        if ($collection) {
            foreach ($collection['data'] as $key => $item) {
                $row = [];
                $row[] = $sl;
                $row[] = $item->WARD_NAME;
                $row[] = $item->WARD_NAME_BN;
                $dt = date_create($item->CREATED_AT);
                $dt = date_format($dt, 'd/m/Y');
                $row[] = $dt;

                $action = '';
                if ($access->STATUS == 1) {
                    if ($item->IS_ACTIVE == 1) {
                        $action .= '<a href="'.route('zone_status', $item->WARD_ID).'" data-isactive="1" class="label label-warning change_status">'.trans('common.table_inactive').'</a>';
                    } else {
                        $action .= '<a href="'.route('zone_status', $item->WARD_ID).'" data-isactive="0" class="label label-success change_status">'.trans('common.table_active').'</a>';
                    }
                }
                if ($access->UPDATE == 1) {
                    $action .= '<a href="#" class="label label-default" data-toggle="modal" data-target="#modal_add_content" data-form="'.route('zone_edit_form', $item->WARD_ID).'"><i class="fa fa-pencil"></i></a>';
                }
                if ($access->DELETE == 1) {
                    $action .= '<a href="'.route('zone_delete', $item->WARD_ID).'" class="label label-danger delete_post"><i class="fa fa-times"></i></a>';
                }
                $row[] = $action;
                $data[] = $row;
                $sl++;
            }
        }

        $output = [
            'draw'              => $input['draw'],
            'recordsTotal'      => $collection['total'],
            'recordsFiltered'   => $collection['filtered'],
            'data'              => $data,
        ];

        return $output;
    }

    /**
     * Land Subcategory datatable Output.
     *
     * @return array
     */
    public static function subCategoryOutput($input, $collection)
    {
        $sl = 1;
        $data = [];
        $link_url = 'land_subcategory'; // module url
        $access = Module::checkPrevilege($link_url); // get access info by module url
        if ($collection) {
            foreach ($collection['data'] as $key => $item) {
                $row = [];
                $row[] = $sl;
                $category = LandCategory::where('CAT_ID', $item->PARENT_ID)->first();
                $row[] = $item->CAT_NAME;
                $row[] = $item->CAT_NAME_BN;
                $row[] = $category->CAT_NAME_BN;
                $dt = date_create($item->CREATED_AT);
                $dt = date_format($dt, 'd/m/Y');
                $row[] = $dt;
                $action = '';
                if ($access->STATUS == 1) {
                    if ($item->IS_ACTIVE == 1) {
                        $action .= '<a href="'.route('land_category_status', $item->CAT_ID).'" data-isactive="1" class="label label-warning change_status">'.trans('common.table_inactive').'</a>';
                    } else {
                        $action .= '<a href="'.route('land_category_status', $item->CAT_ID).'" data-isactive="0" class="label label-success change_status">'.trans('common.table_active').'</a>';
                    }
                }
                if ($access->UPDATE == 1) {
                    $action .= '<a href="#" class="label label-default" data-toggle="modal" data-target="#modal_add_content" data-form="'.route('subcategory_edit_form', $item->CAT_ID).'"><i class="fa fa-pencil"></i></a>';
                }
                if ($access->DELETE == 1) {
                    $action .= '<a href="'.route('land_category_delete', $item->CAT_ID).'" class="label label-danger delete_post"><i class="fa fa-times"></i></a>';
                }
                $row[] = $action;
                $data[] = $row;
                $sl++;
            }
        }

        $output = [
            'draw'              => $input['draw'],
            'recordsTotal'      => $collection['total'],
            'recordsFiltered'   => $collection['filtered'],
            'data'              => $data,
        ];

        return $output;
    }

    /**
     * ward datatable Output.
     *
     * @return array
     */
    public static function wardOutput($input, $collection)
    {
        $sl = 1;
        $data = [];
        $link_url = 'word_list'; // module url
        $access = Module::checkPrevilege($link_url); // get access info by module url
        if ($collection) {
            foreach ($collection['data'] as $key => $item) {
                $row = [];
                $row[] = $sl;
                $zone = Zone::where('WARD_ID', $item->PARENT_ID)->first();
                $row[] = $item->WARD_NAME;
                $row[] = $item->WARD_NAME_BN;
                $row[] = !empty($zone->WARD_NAME_BN) ? $zone->WARD_NAME_BN : '';
                $dt = date_create($item->CREATED_AT);
                $dt = date_format($dt, 'd/m/Y');
                $row[] = $dt;
                $action = '';
                if ($access->STATUS == 1) {
                    if ($item->IS_ACTIVE == 1) {
                        $action .= '<a href="'.route('zone_status', $item->WARD_ID).'" data-isactive="1" class="label label-warning change_status">'.trans('common.table_inactive').'</a>';
                    } else {
                        $action .= '<a href="'.route('zone_status', $item->WARD_ID).'" data-isactive="0" class="label label-success change_status">'.trans('common.table_active').'</a>';
                    }
                }
                if ($access->UPDATE == 1) {
                    $action .= '<a href="#" class="label label-default" data-toggle="modal" data-target="#modal_add_content" data-form="'.route('ward_edit_form', $item->WARD_ID).'"><i class="fa fa-pencil"></i></a>';
                }
                if ($access->DELETE == 1) {
                    $action .= '<a href="'.route('zone_delete', $item->WARD_ID).'" class="label label-danger delete_post"><i class="fa fa-times"></i></a>';
                }
                $row[] = $action;
                $data[] = $row;
                $sl++;
            }
        }

        $output = [
            'draw'              => $input['draw'],
            'recordsTotal'      => $collection['total'],
            'recordsFiltered'   => $collection['filtered'],
            'data'              => $data,
        ];

        return $output;
    }

    /**
     * Lease category datatable Output.
     *
     * @return array
     */
    public static function leasecatdatatableOutput($input, $collection)
    {
        $sl = 1;
        $data = [];
        if ($collection) {
            foreach ($collection['data'] as $key => $item) {
                $row = [];
                $row[] = $sl;
                $row[] = $item->name;
                $row[] = $item->bn_name;
                $dt = date_create($item->created_at);
                $dt = date_format($dt, 'd/m/Y');
                $row[] = $dt;

                $action = '';
                if ($item->is_active == 1) {
                    $action .= '<a href="'.route('lease_category_status', $item->id).'" data-isactive="1" class="label label-warning change_status">'.trans('common.table_inactive').'</a>';
                } else {
                    $action .= '<a href="'.route('lease_category_status', $item->id).'" data-isactive="0" class="label label-success change_status">'.trans('common.table_active').'</a>';
                }
                $action .= '<a href="#" class="label label-default" data-toggle="modal" data-target="#modal_add_content" data-form="'.route('lease_edit_form', $item->id).'"><i class="fa fa-pencil"></i></a>';
                $action .= '<a href="'.route('lease_category_delete', $item->id).'" class="label label-danger delete_post"><i class="fa fa-times"></i></a>';

                $row[] = $action;
                $data[] = $row;
                $sl++;
            }
        }

        $output = [
            'draw'            => $input['draw'],
            'recordsTotal'    => $collection['total'],
            'recordsFiltered' => $collection['filtered'],
            'data'            => $data,
        ];

        return $output;
    }

    public static function projectcatdatatableOutput($input, $collection)
    {
        $sl = 1;
        $data = [];
        $link_url = 'project_category'; // module url
        $access = Module::checkPrevilege($link_url); // get access info by module url
        if ($collection) {
            foreach ($collection['data'] as $key => $item) {
                $row = [];
                $row[] = $sl;
                $row[] = $item->CATE_NAME;
                $row[] = $item->CATE_DESC;
                $dt = date_create($item->CREATED_AT);
                $dt = date_format($dt, 'd/m/Y');
                $row[] = $dt;

                $action = '';
                if ($access->STATUS == 1) {
                    if ($item->IS_ACTIVE == 1) {
                        $action .= '<a href="'.route('project_category_status', $item->PR_CATEGORY).'" data-isactive="1" class="label label-warning change_status">'.trans('common.table_inactive').'</a>';
                    } else {
                        $action .= '<a href="'.route('project_category_status', $item->PR_CATEGORY).'" data-isactive="0" class="label label-success change_status">'.trans('common.table_active').'</a>';
                    }
                }
                if ($access->UPDATE == 1) {
                    $action .= '<a href="#" class="label label-default" data-toggle="modal" data-target="#modal_add_content" data-form="'.route('project_edit_form', $item->PR_CATEGORY).'"><i class="fa fa-pencil"></i></a>';
                }
                if ($access->DELETE == 1) {
                    $action .= '<a href="'.route('project_category_delete', $item->PR_CATEGORY).'" class="label label-danger delete_post"><i class="fa fa-times"></i></a>';
                }
                $row[] = $action;
                $data[] = $row;
                $sl++;
            }
        }

        $output = [
            'draw'            => $input['draw'],
            'recordsTotal'    => $collection['total'],
            'recordsFiltered' => $collection['filtered'],
            'data'            => $data,
        ];

        return $output;
    }

    public static function projecttypedatatableOutput($input, $collection)
    {
        $sl = 1;
        $data = [];
        $link_url = 'project_type'; // module url
        $access = Module::checkPrevilege($link_url); // get access info by module url
        if ($collection) {
            foreach ($collection['data'] as $key => $item) {
                $row = [];
                $row[] = $sl;
                $row[] = $item->TYPE_NAME;
                $row[] = $item->TYPE_DESC;
                $dt = date_create($item->CREATED_AT);
                $dt = date_format($dt, 'd/m/Y');
                $row[] = $dt;

                $action = '';
                if ($access->STATUS == 1) {
                    if ($item->IS_ACTIVE == 1) {
                        $action .= '<a href="'.route('project_type_status', $item->PR_TYPE).'" data-isactive="1" class="label label-warning change_status">'.trans('common.table_inactive').'</a>';
                    } else {
                        $action .= '<a href="'.route('project_type_status', $item->PR_TYPE).'" data-isactive="0" class="label label-success change_status">'.trans('common.table_active').'</a>';
                    }
                }
                if ($access->UPDATE == 1) {
                    $action .= '<a href="#" class="label label-default" data-toggle="modal" data-target="#modal_add_content" data-form="'.route('project_type_edit_form', $item->PR_TYPE).'"><i class="fa fa-pencil"></i></a>';
                }
                if ($access->DELETE == 1) {
                    $action .= '<a href="'.route('project_type_delete', $item->PR_TYPE).'" class="label label-danger delete_post"><i class="fa fa-times"></i></a>';
                }
                $row[] = $action;
                $data[] = $row;
                $sl++;
            }
        }

        $output = [
            'draw'            => $input['draw'],
            'recordsTotal'    => $collection['total'],
            'recordsFiltered' => $collection['filtered'],
            'data'            => $data,
        ];

        return $output;
    }

    public static function projectfloordatatableOutput($input, $collection)
    {
        $sl = 1;
        $data = [];
        $link_url = 'floor'; // module url
        $access = Module::checkPrevilege($link_url); // get access info by module url
        if ($collection) {
            foreach ($collection['data'] as $key => $item) {
                $row = [];
                $row[] = $sl;
                $row[] = $item->FLOOR_NUMBER;
                $row[] = $item->FLOOR_NUMBER_BN;
                $row[] = $item->DETAILS;
                $dt = date_create($item->CREATED_AT);
                $dt = date_format($dt, 'd/m/Y');
                $row[] = $dt;

                $action = '';
                if ($access->STATUS == 1) {
                    if ($item->IS_ACTIVE == 1) {
                        $action .= '<a href="'.route('project_floor_status', $item->FLOOR_ID).'" data-isactive="1" class="label label-warning change_status">'.trans('common.table_inactive').'</a>';
                    } else {
                        $action .= '<a href="'.route('project_floor_status', $item->FLOOR_ID).'" data-isactive="0" class="label label-success change_status">'.trans('common.table_active').'</a>';
                    }
                }
                if ($access->UPDATE == 1) {
                    $action .= '<a href="#" class="label label-default" data-toggle="modal" data-target="#modal_add_content" data-form="'.route('project_floor_edit_form', $item->FLOOR_ID).'"><i class="fa fa-pencil"></i></a>';
                }
                if ($access->DELETE == 1) {
                    $action .= '<a href="'.route('project_floor_delete', $item->FLOOR_ID).'" class="label label-danger delete_post"><i class="fa fa-times"></i></a>';
                }
                $row[] = $action;
                $data[] = $row;
                $sl++;
            }
        }

        $output = [
            'draw'            => $input['draw'],
            'recordsTotal'    => $collection['total'],
            'recordsFiltered' => $collection['filtered'],
            'data'            => $data,
        ];

        return $output;
    }

    public static function projectlocationdatatableOutput($input, $collection)
    {
        $sl = 1;
        $data = [];
        $link_url = 'project_location'; // module url
        $access = Module::checkPrevilege($link_url); // get access info by module url
        if ($collection) {
            foreach ($collection['data'] as $key => $item) {
                $row = [];
                $row[] = $sl;
                $row[] = $item->LOCATION_NAME;
                $row[] = $item->LOCATION_DESC;
                $dt = date_create($item->CREATED_AT);
                $dt = date_format($dt, 'd/m/Y');
                $row[] = $dt;

                $action = '';
                if ($access->STATUS == 1) {
                    if ($item->IS_ACTIVE == 1) {
                        $action .= '<a href="'.route('project_location_status', $item->LOCATION_ID).'" data-isactive="1" class="label label-warning change_status">'.trans('common.table_inactive').'</a>';
                    } else {
                        $action .= '<a href="'.route('project_location_status', $item->LOCATION_ID).'" data-isactive="0" class="label label-success change_status">'.trans('common.table_active').'</a>';
                    }
                }
                if ($access->UPDATE == 1) {
                    $action .= '<a href="#" class="label label-default" data-toggle="modal" data-target="#modal_add_content" data-form="'.route('project_location_edit_form', $item->LOCATION_ID).'"><i class="fa fa-pencil"></i></a>';
                }
                if ($access->DELETE == 1) {
                    $action .= '<a href="'.route('project_location_delete', $item->LOCATION_ID).'" class="label label-danger delete_post"><i class="fa fa-times"></i></a>';
                }
                $row[] = $action;
                $data[] = $row;
                $sl++;
            }
        }

        $output = [
            'draw'            => $input['draw'],
            'recordsTotal'    => $collection['total'],
            'recordsFiltered' => $collection['filtered'],
            'data'            => $data,
        ];

        return $output;
    }

    public static function projectcatconditiondatatableOutput($input, $collection)
    {
        $sl = 1;
        $data = [];
        $link_url = 'project_cat_con'; // module url
        $access = Module::checkPrevilege($link_url); // get access info by module url
        if ($collection) {
            foreach ($collection['data'] as $key => $item) {
                $row = [];
                $row[] = $sl;
                $category = ProjectCategory::where('PR_CATEGORY', $item->PR_CATEGORY)->first();
                $row[] = !empty($category->CATE_NAME) ? $category->CATE_NAME : '';
                $row[] = $item->CON_TITLE;
                $row[] = str_limit($item->CON_DESC, 200);
                $dt = date_create($item->CREATED_AT);
                $dt = date_format($dt, 'd/m/Y');
                $row[] = $dt;

                $action = '';
                if ($access->STATUS == 1) {
                    if ($item->IS_ACTIVE == 1) {
                        $action .= '<a href="'.route('project_cat_con_status', $item->CATE_CON_ID).'" data-isactive="1" class="label label-warning change_status">'.trans('common.table_inactive').'</a>';
                    } else {
                        $action .= '<a href="'.route('project_cat_con_status', $item->CATE_CON_ID).'" data-isactive="0" class="label label-success change_status">'.trans('common.table_active').'</a>';
                    }
                }
                if ($access->UPDATE == 1) {
                    $action .= '<a href="#" class="label label-default" data-toggle="modal" data-target="#modal_big_content" data-form="'.route('project_cat_con_edit_form', $item->CATE_CON_ID).'"><i class="fa fa-pencil"></i></a>';
                }
                if ($access->DELETE == 1) {
                    $action .= '<a href="'.route('project_cat_con_delete', $item->CATE_CON_ID).'" class="label label-danger delete_post"><i class="fa fa-times"></i></a>';
                }
                $row[] = $action;
                $data[] = $row;
                $sl++;
            }
        }

        $output = [
            'draw'            => $input['draw'],
            'recordsTotal'    => $collection['total'],
            'recordsFiltered' => $collection['filtered'],
            'data'            => $data,
        ];

        return $output;
    }

    /**
     * Lease Subcategory datatable Output.
     *
     * @return array
     */
    public static function leasesubCategoryOutput($input, $collection)
    {
        $sl = 1;
        $data = [];
        if ($collection) {
            foreach ($collection['data'] as $key => $item) {
                $row = [];
                $row[] = $sl;
                $category = LeaseCategory::where('id', $item->parent_id)->first();
                $row[] = !empty($category->bn_name) ? $category->bn_name : '';
                //$row[]      = $category->bn_name;
                $row[] = $item->name;
                $row[] = $item->bn_name;
                $dt = date_create($item->created_at);
                $dt = date_format($dt, 'd/m/Y');
                $row[] = $dt;

                $action = '';
                if ($item->is_active == 1) {
                    $action .= '<a href="'.route('lease_subcategory_status', $item->id).'" data-isactive="1" class="label label-warning change_status">'.trans('common.table_inactive').'</a>';
                } else {
                    $action .= '<a href="'.route('lease_subcategory_status', $item->id).'" data-isactive="0" class="label label-success change_status">'.trans('common.table_active').'</a>';
                }
                $action .= '<a href="#" class="label label-default" data-toggle="modal" data-target="#modal_add_content" data-form="'.route('lease_subcategory_edit_form', $item->id).'"><i class="fa fa-pencil"></i></a>';
                $action .= '<a href="'.route('lease_subcategory_delete', $item->id).'" class="label label-danger delete_post"><i class="fa fa-times"></i></a>';

                $row[] = $action;
                $data[] = $row;
                $sl++;
            }
        }

        return [
            'draw'            => $input['draw'],
            'recordsTotal'    => $collection['total'],
            'recordsFiltered' => $collection['filtered'],
            'data'            => $data,
        ];
    }

    /**
     * Permission datatable Output.
     *
     * @return array
     */
    public static function permissionOutput($input, $collection)
    {
        $sl = 1;
        $data = [];
        if ($collection) {
            foreach ($collection['data'] as $key => $item) {
                $row = [];
                $row[] = $sl;
                $row[] = $item->NAME;
                $row[] = $item->DISPLAY_NAME;
                $row[] = $item->DESCRIPTION;
                $row[] = date_format(date_create($item->CREATED_AT), 'd/m/Y');

                $action = '';
                $action .= '<a href="#" class="label label-default" data-toggle="modal" data-target="#modal_add_content" data-form="'.route('permission_edit_form', $item->PERM_ID).'"><i class="fa fa-pencil"></i></a>';
                $action .= '<a href="'.route('permission_delete', $item->PERM_ID).'" class="label label-danger delete_post"><i class="fa fa-times"></i></a>';

                $row[] = $action;
                $data[] = $row;
                $sl++;
            }
        }

        return [
            'draw'              => $input['draw'],
            'recordsTotal'      => $collection['total'],
            'recordsFiltered'   => $collection['filtered'],
            'data'              => $data,
        ];
    }

    /**
     * Role datatable Output.
     *
     * @return array
     */
    public static function roleOutput($input, $collection)
    {
        $data = [];
        if ($collection) {
            foreach ($collection['data'] as $key => $item) {
                $row = [];
                $row[] = $key + 1;
                $row[] = $item->NAME;
                $row[] = $item->DISPLAY_NAME;
                $row[] = $item->DESCRIPTION;
                $row[] = date_format(date_create($item->CREATED_AT), 'd/m/Y');

                $action = '';
                $action .= '<a href="#" class="label label-default" data-toggle="modal" data-target="#modal_add_content" data-form="'.route('role_edit_form', $item->ROLE_ID).'"><i class="fa fa-pencil"></i></a>';
                $action .= '<a href="'.route('role_delete', $item->ROLE_ID).'" class="label label-danger delete_post"><i class="fa fa-times"></i></a>';

                $row[] = $action;
                $data[] = $row;
            }
        }

        return [
            'draw'              => $input['draw'],
            'recordsTotal'      => $collection['total'],
            'recordsFiltered'   => $collection['filtered'],
            'data'              => $data,
        ];
    }

    /**
     * Mouza datatable Output.
     *
     * @author      Nurullah <nurul@atilimited.net>
     *
     * @return array
     */
    public static function mouzaOutput($input, $collection)
    {
        $data = [];
        $link_url = 'mouza_list'; // module url
        $access = Module::checkPrevilege($link_url); // get access info by module url
        if ($collection) {
            foreach ($collection['data'] as $key => $item) {
                $row = [];
                $row[] = $key + 1;
                $zone = Zone::where('WARD_ID', $item->getWard->PARENT_ID)->pluck('WARD_NAME_BN');
                $row[] = $item->MOUZA_NAME;
                $row[] = $item->MOUZA_NAME_BN;
                $row[] = $item->getWard->WARD_NAME_BN;
                $row[] = $zone;

                $action = '';
                if ($access->STATUS == 1) {
                    if ($item->IS_ACTIVE == 1) {
                        $action .= '<a href="'.route('mouza_status', $item->MOUZA_ID).'" data-isactive="1" class="label label-warning change_status">'.trans('common.table_inactive').'</a>';
                    } else {
                        $action .= '<a href="'.route('mouza_status', $item->MOUZA_ID).'" data-isactive="0" class="label label-success change_status">'.trans('common.table_active').'</a>';
                    }
                }
                if ($access->UPDATE == 1) {
                    $action .= '<a href="#" class="label label-default" data-toggle="modal" data-target="#modal_add_content" data-form="'.route('mouza_edit_form', $item->MOUZA_ID).'"><i class="fa fa-pencil"></i></a>';
                }
                if ($access->DELETE == 1) {
                    $action .= '<a href="'.route('mouza_delete', $item->MOUZA_ID).'" class="label label-danger delete_post"><i class="fa fa-times"></i></a>';
                }
                $row[] = $action;
                $data[] = $row;
            }
        }

        return [
            'draw'            => $input['draw'],
            'recordsTotal'    => $collection['total'],
            'recordsFiltered' => $collection['filtered'],
            'data'            => $data,
        ];
    }

    /**
     * User datatable Output.
     *
     * @return array
     */
    public static function userOutput($input, $collection)
    {
        $sl = 1;
        $link_url = 'user_list'; // module url
        $access = Module::checkPrevilege($link_url); // get access info by module url
        $data = [];
        if ($collection) {
            foreach ($collection['data'] as $key => $item) {
                $group = ModuleGroup::where('USERGRP_ID', $item->USERGRP_ID)->first();
                $group_level = ModuleGroupLevel::where('UG_LEVEL_ID', $item->USERLVL_ID)->first();
                $row = [];
                $row[] = $sl;
                $row[] = $item->FULL_NAME;
                $row[] = $item->USERNAME;
                $row[] = $item->EMAIL;
                if (count($item->roles) > 0) {
                    $role_names = '';
                    foreach ($item->roles as $key => $role) {
                        $role_names .= '<p style="margin:0;">'.$role->DISPLAY_NAME.'</p>';
                    }
                    $row[] = $role_names;
                } else {
                    $row[] = '<p style="margin:0;color:red;">No role</p>';
                }
                $row[] = $group->USERGRP_NAME;
                $row[] = $group_level->UGLEVE_NAME;
                //$row[] = date_format(date_create($item->CREATED_AT), 'd/m/Y');

                $action = '';
                if ($access->STATUS == 1) {
                    if ($item->IS_ACTIVE == 1) {
                        $action .= '<a href="'.route('user_status', $item->USER_ID).'" data-isactive="1" class="label label-warning change_status">'.trans('common.table_inactive').'</a>';
                    } else {
                        $action .= '<a href="'.route('user_status', $item->USER_ID).'" data-isactive="0" class="label label-success change_status">'.trans('common.table_active').'</a>';
                    }
                }
                if ($access->UPDATE == 1) {
                    $action .= '<a href="#" class="label label-default" data-toggle="modal" data-target="#modal_add_content" data-form="'.route('user_edit_form', $item->USER_ID).'"><i class="fa fa-pencil"></i></a>';
                }
                if ($access->DELETE == 1) {
                    $action .= '<a href="'.route('delete_user', $item->USER_ID).'" class="label label-danger delete_post"><i class="fa fa-times"></i></a>';
                }
                if ($access->READ == 1) {
                    $action .= '<a href="'.route('user_profile', $item->USER_ID).'" class="label label-primary"><i class="fa fa-eye"></i></a>';
                }
                $row[] = $action;
                $data[] = $row;
                $sl++;
            }
        }

        return [
            'draw'            => $input['draw'],
            'recordsTotal'    => $collection['total'],
            'recordsFiltered' => $collection['filtered'],
            'data'            => $data,
        ];
    }

    /**
     * Particular datatable Output.
     *
     * @author      Nurullah <nurul@atilimited.net>
     *
     * @return array
     */
    public static function particularOutput($input, $collection)
    {
        $sl = 1;
        $data = [];
        $link_url = 'particular_list'; // module url
        $access = Module::checkPrevilege($link_url); // get access info by module url
        if ($collection) {
            foreach ($collection['data'] as $key => $item) {
                $row = [];
                $row[] = $sl;
                $row[] = $item->PARTICULAR_NAME;
                $row[] = $item->PARTICULAR_DESC;

                $action = '';
                if ($access->STATUS == 1) {
                    if ($item->IS_ACTIVE == 1) {
                        $action .= '<a href="'.route('particular_status', $item->PARTICULAR_ID).'" data-isactive="1" class="label label-warning change_status">'.trans('common.table_inactive').'</a>';
                    } else {
                        $action .= '<a href="'.route('particular_status', $item->PARTICULAR_ID).'" data-isactive="0" class="label label-success change_status">'.trans('common.table_active').'</a>';
                    }
                }
                if ($access->UPDATE) {
                    $action .= '<a href="#" class="label label-default" data-toggle="modal" data-target="#modal_add_content" data-form="'.route('particular_edit_form', $item->PARTICULAR_ID).'"><i class="fa fa-pencil"></i></a>';
                }
                //$action .= '<a href="' . route('particular_delete', $item->PARTICULAR_ID) . '" class="label label-danger delete_post"><i class="fa fa-times"></i></a>';

                $row[] = $action;
                $data[] = $row;
                $sl++;
            }
        }

        return [
            'draw'            => $input['draw'],
            'recordsTotal'    => $collection['total'],
            'recordsFiltered' => $collection['filtered'],
            'data'            => $data,
        ];
    }

    /**
     * Bank Information datatable Output.
     *
     * @author      Jahid Hasan <jahid@atilimited.net>
     *
     * @param array $input
     * @param array $collection defining Bank row information
     *
     * @return array
     */
    public static function bankOutput($input, $collection)
    {
        $sl = 1;
        $data = [];
        $link_url = 'bank_list'; // module url
        $access = Module::checkPrevilege($link_url); // get access info by module url
        if ($collection) {
            foreach ($collection['data'] as $key => $item) {
                $row = [];
                $row[] = $sl;
                $row[] = $item->BANK_NAME;
                //$row[] = $item->B_PARENT_ID;
                $bank_list = Bank::where('BANK_ID', $item->B_PARENT_ID)->first();
                $row[] = !empty($bank_list->BANK_NAME) ? $bank_list->BANK_NAME : '';
                $row[] = $item->ADDRESS;

                $action = '';
                if ($access->STATUS == 1) {
                    if ($item->IS_ACTIVE == 1) {
                        $action .= '<a href="'.route('bank_status', $item->BANK_ID).'" data-isactive="1" class="label label-warning change_status">'.trans('common.table_inactive').'</a>';
                    } else {
                        $action .= '<a href="'.route('bank_status', $item->BANK_ID).'" data-isactive="0" class="label label-success change_status">'.trans('common.table_active').'</a>';
                    }
                }
                if ($access->UPDATE == 1) {
                    $action .= '<a href="#" class="label label-default" data-toggle="modal" data-target="#modal_add_content" data-form="'.route('bank_edit_form', $item->BANK_ID).'"><i class="fa fa-pencil"></i></a>';
                }
                if ($access->DELETE == 1) {
                    $action .= '<a href="'.route('bank_delete', $item->BANK_ID).'" class="label label-danger delete_post"><i class="fa fa-times"></i></a>';
                }
                $row[] = $action;
                $data[] = $row;
                $sl++;
            }
        }

        return [
            'draw'            => $input['draw'],
            'recordsTotal'    => $collection['total'],
            'recordsFiltered' => $collection['filtered'],
            'data'            => $data,
        ];
    }

    /**
     * Tender inapproved payment Information datatable Output.
     *
     * @param array $input
     * @param array $collection
     *
     * @return array
     */
    public static function tenderApplicantDataList($input, $collection)
    {
        $data = [];
        if (count($collection) > 0) {
            foreach ($collection['data'] as $key => $item) {
                if ($item->IS_CITIZEN == 0) {
                    $installment = TenderInstallment::where('TE_APP_ID', $item->TE_APP_ID)->select('TE_APP_ID')->first();
                } else {
                    $installment = TenderInstallment::where('CITIZEN_ID', $item->CITIZEN_ID)->select('CITIZEN_ID')->first();
                }
                if (!empty($installment)) {
                    $row = [];
                    $row[] = $key + 1;
                    if ($item->IS_CITIZEN == 0) {
                        if (!empty($item->getApplicant->getThana->THANA_ENAME)) {
                            $thana = $item->getApplicant->getThana->THANA_ENAME.', ';
                            $adrs = $thana.$item->getApplicant->ROAD_NO.', '.$item->getApplicant->HOLDING_NO;
                        } else {
                            $thana = '';
                        }
                    } else {
                        if (!empty($item->getCitizen->getThana->THANA_ENAME)) {
                            $thana = $item->getCitizen->getThana->THANA_ENAME.', ';
                            $adrs = $thana.$item->getCitizen->ROAD_NO.', '.$item->getCitizen->HOLDING_NO;
                        } else {
                            $thana = '';
                        }
                    }

                    if ($item->IS_CITIZEN == 0) {
                        $row[] = '<a href="#" title="View Applicant Details" data-toggle="modal" data-target="#modal_big_content" data-form="'.route('tender_applicant_view', ['applicant', $item->TE_APP_ID, $item->SCHEDULE_ID]).'">'
                             .$item->getApplicant->APPLICANT_NAME.'</a><br>'.$adrs;
                    } else {
                        $row[] = '<a href="#" title="View Applicant Details" data-toggle="modal" data-target="#modal_big_content" data-form="'.route('tender_applicant_view', ['citizen', $item->CITIZEN_ID, $item->SCHEDULE_ID]).'">'
                              .$item->getCitizen->getTenderCitizen->FULL_NAME.'</a><br>'.$adrs;
                    }
                    $row[] = '<a href="#" title="Tender Info" data-toggle="modal" data-target="#modal_big_content" data-form="'.route($item->IS_LEASE == 1 ? 'lease_tender_view' : 'tender_view', $item->TE_APP_ID).'">'.$item->getTender->TENDER_NO.'</a>';
                    $row[] = $item->IS_CITIZEN == 0 ? $item->getApplicant->APPLICANT_PHONE : $item->getCitizen->getTenderCitizen->MOBILE_NO;
                    $row[] = $item->getProject->PR_NAME_BN;
                    $row[] = '<a href="#" class="label label-success" data-toggle="modal" data-target="#modal_add_content" data-form="'.route('installment_list_view', [$item->IS_CITIZEN == 0 ? 'applicant/'.$item->TE_APP_ID : 'citizen/'.$item->CITIZEN_ID]).'"><i class="fa fa-check-square-o"></i></a>';
                    $data[] = $row;
                }
            }
        }

        return [
            'draw'              => $input['draw'],
            'recordsTotal'      => $collection['total'],
            'recordsFiltered'   => $collection['filtered'],
            'data'              => $data,
        ];
    }

    /**
     * Revenue report datatable Output.
     *
     * @param array $input
     * @param array $collection
     *
     * @return array
     */
    public static function revenueDataList($input, $collection)
    {
        $data = [];
        if (count($collection) > 0) {
            foreach ($collection['data'] as $key => $item) {
                $row = [];
                $row[] = $key + 1;
                if (!empty($item->getCitizen)) {
                    $row[] = $item->getCitizen->getTenderCitizen->FULL_NAME;
                } elseif (!empty($item->getApplicant)) {
                    $row[] = $item->getApplicant->APPLICANT_NAME;
                } elseif (!empty($item->getLeaseDemesne)) {
                    $row[] = $item->getLeaseDemesne->NAME;
                } else {
                    $row[] = '';
                }
                $row[] = $item->VOUCHER_NO.'<br/>'.date('d/m/Y', strtotime($item->TRX_TRAN_DT));
                $row[] = $item->getParticular->PARTICULAR_NAME.'&#40;'.$item->getCategory->CATE_NAME.'&#41;';
                $row[] = date('F', strtotime($item->TRX_TRAN_DT));
                $row[] = date('Y', strtotime($item->TRX_TRAN_DT));
                $row[] = number_format($item->TOTAL_PENDING, 2);
                $row[] = number_format($item->TOTAL_PAID, 2);
                $total_due = $item->TOTAL_PENDING - $item->TOTAL_PAID;
                $row[] = $total_due < 0 ? number_format(0, 2) : number_format($total_due, 2);
                //$row[]  = '<a href="'. route('revenue_data_print', $item->VOUCHER_NO) .'" target="_blank" class="label label-default"><i class="fa fa-print"></i></a>';
                $data[] = $row;
            }
        }

        return [
            'draw'              => $input['draw'],
            'recordsTotal'      => $collection['total'],
            'recordsFiltered'   => $collection['filtered'],
            'data'              => $data,
        ];
    }

    /**
     * Citizen ledger report datatable Output.
     *
     * @param array $input
     * @param array $collection
     *
     * @return array
     */
    public static function citizenLedgerDataList($input, $collection)
    {
        $data = [];
        if (count($collection) > 0) {
            foreach ($collection['data'] as $key => $item) {
                $row = [];
                $row[] = $key + 1;
                if (!empty($item->getCitizen)) {
                    $row[] = $item->getCitizen->getTenderCitizen->FULL_NAME;
                } elseif (!empty($item->getApplicant)) {
                    $row[] = $item->getApplicant->APPLICANT_NAME;
                } elseif (!empty($item->getLeaseDemesne)) {
                    $row[] = $item->getLeaseDemesne->NAME;
                } else {
                    $row[] = '';
                }
                $row[] = $item->VOUCHER_NO.'<br/>'.date('d/m/Y', strtotime($item->TRX_TRAN_DT));
                $row[] = $item->getParticular->PARTICULAR_NAME.'&#40;'.$item->getCategory->CATE_NAME.'&#41;';
                $row[] = date('F', strtotime($item->TRX_TRAN_DT));
                $row[] = date('Y', strtotime($item->TRX_TRAN_DT));
                $row[] = number_format($item->TOTAL_PENDING, 2);
                $row[] = number_format($item->TOTAL_PAID, 2);
                $total_due = $item->TOTAL_PENDING - $item->TOTAL_PAID;
                $row[] = $total_due < 0 ? number_format(0, 2) : number_format($total_due, 2);
                //$row[]  = '<a href="'. route('revenue_data_print', $item->VOUCHER_NO) .'" target="_blank" class="label label-default"><i class="fa fa-print"></i></a>';
                $data[] = $row;
            }
        }

        return [
            'draw'              => $input['draw'],
            'recordsTotal'      => $collection['total'],
            'recordsFiltered'   => $collection['filtered'],
            'data'              => $data,
        ];
    }

    /**
     * Lease Demesne Output.
     *
     * @author      Nurullah <nurul@atilimited.net>
     *
     * @return array
     */
    public static function leaseDemesneOutput($input, $collection)
    {
        $sl = 1;
        $data = [];
        $link_url = 'lease_demesne'; // module url
        $access = Module::checkPrevilege($link_url); // get access info by module url
        if ($collection) {
            foreach ($collection['data'] as $key => $item) {
                $schedule = TenderSchedule::with('getProject')->where('SCHEDULE_ID', $item->SCHEDULE_ID)->first();
                $row = [];
                $row[] = $sl;
                $row[] = $schedule->getProject->PR_NAME_BN;
                $row[] = $item->NAME;
                $row[] = $item->PHONE;
                $row[] = $item->ADDRESS;
                $row[] = date_format(date_create($item->getValidity->DATE_FROM), 'd/m/Y').' তারিখ হতে <br/>'
                        .date_format(date_create($item->getValidity->DATE_TO), 'd/m/Y').' তারিখ পর্যন্ত';
                $action = '';

                if ($item->IS_CANCEL == 1) {
                    $action .= '<label class="label label-danger">বাতিল</label>';
                } else {
                    if ($access->DELETE == 1) {
                        $action .= '<a href="#" title="Cancle Demesne" class="label label-success" data-toggle="modal" data-target="#modal_add_content" data-form="'.route('lease_demesne_cancel_form', $item->LEASE_DE_ID).'">'.trans('বাতিলকরণ').'</a>';
                    }
                    if ($access->UPDATE == 1) {
                        $action .= '<a href="#" title="Edit Demesne" class="label label-default" data-toggle="modal" data-target="#modal_add_content" data-form="'.route('lease_demesne_edit', $item->LEASE_DE_ID).'"><i class="fa fa-pencil"></i></a>';
                    }
                }

                $row[] = $action;
                $data[] = $row;
                $sl++;
            }
        }

        $output = [
            'draw'              => $input['draw'],
            'recordsTotal'      => $collection['total'],
            'recordsFiltered'   => $collection['filtered'],
            'data'              => $data,
        ];

        return $output;
    }

    /**
     * Account Pending receive amount list.
     *
     * @author      Nurullah <nurul@atilimited.net>
     *
     * @return array
     */
    public static function pendingPaymentOutput($input, $collection)
    {
        $data = [];
        $link_url = 'payment_pending_list'; // module url
        $access = Module::checkPrevilege($link_url); // get access info by module url
        if (count($collection) > 0) {
            foreach ($collection['data'] as $key => $item) {
                $row = [];
                $row[] = $key + 1;
                $particular = Particular::where('PARTICULAR_ID', $item->getVoucherChd->PARTICULAR_ID)->pluck('PARTICULAR_NAME');
                if ($item->getVoucher->CITIZEN_ID == null && $item->getVoucher->LEASE_DE_ID == null) {
                    $row[] = '<a href="#" title="View Applicant Details" data-toggle="modal" data-target="#modal_big_content" data-form="'.route('tender_applicant_view', ['applicant', $item->getVoucher->TE_APP_ID, $item->getVoucherChd->SCHEDULE_ID]).'">'
                             .$item->getVoucher->getApplicant->APPLICANT_NAME.'</a>';
                    $row[] = '<a href="#" title="View payment" data-toggle="modal" data-target="#modal_add_content" data-form="'.route('payment_info', ['applicant', $item->getVoucher->TE_APP_ID, $item->getVoucherChd->SCHEDULE_ID, $item->getVoucherChd->TRX_TRAN_NO]).'">'.Helpers::numberConvert($item->VOUCHER_NO).'</a>';
                } elseif ($item->getVoucher->TE_APP_ID == null && $item->getVoucher->LEASE_DE_ID == null) {
                    $row[] = '<a href="#" title="View Applicant Details" data-toggle="modal" data-target="#modal_big_content" data-form="'.route('tender_applicant_view', ['citizen', $item->getVoucher->CITIZEN_ID, $item->getVoucherChd->SCHEDULE_ID]).'">'
                             .$item->getVoucher->getCitizen->getTenderCitizen->FULL_NAME.'</a>';
                    $row[] = '<a href="#" title="View payment" data-toggle="modal" data-target="#modal_add_content" data-form="'.route('payment_info', ['citizen', $item->getVoucher->CITIZEN_ID, $item->getVoucherChd->SCHEDULE_ID, $item->getVoucherChd->TRX_TRAN_NO]).'">'.Helpers::numberConvert($item->VOUCHER_NO).'</a>';
                } elseif ($item->getVoucher->TE_APP_ID == null && $item->getVoucher->CITIZEN_ID == null) {
                    $row[] = '<a href="#" title="View Applicant Details" data-toggle="modal" data-target="#modal_big_content" data-form="'.route('tender_applicant_view', ['lease_demesne', $item->getVoucher->getLeaseDemesne->LEASE_DE_ID, $item->getVoucherChd->SCHEDULE_ID]).'">'
                             .$item->getVoucher->getLeaseDemesne->NAME.'</a>';
                    $row[] = '<a href="#" title="View payment" data-toggle="modal" data-target="#modal_add_content" data-form="'.route('payment_info', ['lease_demesne', $item->getVoucher->getLeaseDemesne->LEASE_DE_ID, $item->getVoucherChd->SCHEDULE_ID, $item->getVoucherChd->TRX_TRAN_NO]).'">'.Helpers::numberConvert($item->VOUCHER_NO).'</a>';
                }
                $row[] = Helpers::numberConvert(date_format(date_create($item->VLEDGER_DT), 'd/m/Y'));
                $row[] = $particular;
                $row[] = Helpers::numberConvert(number_format($item->DR_AMT, 2));
                $action = '';
                if ($access->CREATE == 1) {
                    $action .= '<a href="'.route('payment_approve', $item->VLEDGER_NO).'" class="label label-success approve_post"><i class="fa fa-check-square-o"></i></a>';
                }
                $row[] = $action;
                $data[] = $row;
            }
        }

        return [
            'draw'              => $input['draw'],
            'recordsTotal'      => $collection['total'],
            'recordsFiltered'   => $collection['filtered'],
            'data'              => $data,
        ];
    }

    /**
     * Lease Demesne money collection Output.
     *
     * @author      Nurullah <nurul@atilimited.net>
     *
     * @return array
     */
    public static function demesneMoneyOutput($input, $collection)
    {
        $sl = 1;
        $data = [];
        if ($collection) {
            foreach ($collection['data'] as $key => $item) {
                $schedule = TenderSchedule::with('getProject')->where('SCHEDULE_ID', $item->SCHEDULE_ID)->first();
                $demesne = LeaseDemesne::where('SCHEDULE_ID', $item->SCHEDULE_ID)->first();
                $demesne_validity = LeaseValidity::where('SCHEDULE_ID', $item->SCHEDULE_ID)->first();
                $row = [];
                $row[] = $sl;
                $row[] = $schedule->getProject->PR_NAME_BN;
                $row[] = $demesne->NAME;
                $row[] = $demesne->PHONE;
                //$row[]  = $demesne->ADDRESS;
                $row[] = Helpers::numberConvert(date_format(date_create($demesne_validity->DATE_FROM), 'd/m/Y')).' তারিখ হতে <br/>'
                        .Helpers::numberConvert(date_format(date_create($demesne_validity->DATE_TO), 'd/m/Y')).' তারিখ পর্যন্ত';
                $row[] = Helpers::numberConvert(number_format($item->getVnLeager->DR_AMT, 2).'৳ <br/>'.date_format(date_create($item->getVnLeager->TRX_TRAN_DT), 'd/m/Y'));
                $data[] = $row;
                $sl++;
            }
        }

        $output = [
            'draw'              => $input['draw'],
            'recordsTotal'      => $collection['total'],
            'recordsFiltered'   => $collection['filtered'],
            'data'              => $data,
        ];

        return $output;
    }

    /**
     * Tender bid comparison datatable Output.
     *
     * @param $input
     * @param $collection
     *
     * @return array
     */
    public static function leaseFinalOutput($input, $collection)
    {
        $sl = 1;
        $data = [];
        $link_url = 'lease_final_selected_applicant_list'; // module url
        $access = Module::checkPrevilege($link_url); // get access info by module url
        if ($collection) {
            foreach ($collection['data'] as $key => $item) {
                $row = [];
                $row[] = $sl;
                if ($item->CITIZEN_ID == null) {
                    $applicant = ApplicantProperty::with('getApplicant')->where('TE_APP_ID', $item->TE_APP_ID)->first();
                    $row[] = '<a href="#" title="View Applicant Details" data-toggle="modal" data-target="#modal_big_content" data-form="'.route('tender_applicant_view', ['applicant', $item->TE_APP_ID, $item->SCHEDULE_ID]).'">'
                             .$applicant->getApplicant->APPLICANT_NAME.'</a>';
                } else {
                    $applicant = Citizen::with('getTenderCitizen')->where('CITIZEN_ID', $item->CITIZEN_ID)->first();
                    $row[] = '<a href="#" title="View Applicant Details" data-toggle="modal" data-target="#modal_big_content" data-form="'.route('tender_applicant_view', ['citizen', $item->CITIZEN_ID, $item->SCHEDULE_ID]).'">'
                             .$applicant->getTenderCitizen->FULL_NAME.'</a>';
                }
                $tender_no = Tender::where('TENDER_ID', $item->TENDER_ID)->pluck('TENDER_NO');
                $row[] = '<a href="#" title="Lease Info" data-toggle="modal" data-target="#modal_big_content" data-form="'.route('lease_tender_view', $item->TENDER_ID).'">
                         '.$tender_no.'</a>';
                $project_name = TenderSchedule::with('getProject')->where('SCHEDULE_ID', $item->SCHEDULE_ID)->first();
                $row[] = $project_name->getProject->PR_NAME_BN;
                $row[] = number_format($item->CR_AMT, 2);
                $row[] = number_format($item->DR_AMT, 2);
                $row[] = number_format($item->Due_Amount, 2);

                $action = '';
                if ($access->CREATE == 1) {
                    $action .= '<a href="#" title="Select Final Applicant" class="label label-success" data-toggle="modal" data-target="#modal_big_content" data-form="'.route('lease_final_select_form', [$item->SCHEDULE_ID, $item->CITIZEN_ID == null ? 'applicant/'.$item->TE_APP_ID : 'citizen/'.$item->CITIZEN_ID]).'"><i class="fa fa-check"></i></a>';
                }
                $row[] = $action;

                $data[] = $row;
                $sl++;
            }
        }

        return [
            'draw'              => $input['draw'],
            'recordsTotal'      => $collection['total'],
            'recordsFiltered'   => $collection['filtered'],
            'data'              => $data,
        ];
    }

    /**
     * Tender bid comparison datatable Output.
     *
     * @param $input
     * @param $collection
     *
     * @return array
     */
    public static function tenderFinalOutput($input, $collection)
    {
        $sl = 1;
        $data = [];
        $link_url = 'tender_final_selected_applicant_list'; // module url
        $access = Module::checkPrevilege($link_url); // get access info by module url
        if ($collection) {
            foreach ($collection['data'] as $key => $item) {
                $row = [];
                $row[] = $sl;
                if ($item->CITIZEN_ID == null) {
                    $applicant = ApplicantProperty::with('getApplicant')->where('TE_APP_ID', $item->TE_APP_ID)->first();
                    $row[] = '<a href="#" title="View Applicant Details" data-toggle="modal" data-target="#modal_big_content" data-form="'.route('tender_applicant_view', ['applicant', $item->TE_APP_ID, $item->SCHEDULE_ID]).'">'
                             .$applicant->getApplicant->APPLICANT_NAME.'</a>';
                } else {
                    $applicant = Citizen::with('getTenderCitizen')->where('CITIZEN_ID', $item->CITIZEN_ID)->first();
                    $row[] = '<a href="#" title="View Applicant Details" data-toggle="modal" data-target="#modal_big_content" data-form="'.route('tender_applicant_view', ['citizen', $item->CITIZEN_ID, $item->SCHEDULE_ID]).'">'
                             .$applicant->getTenderCitizen->FULL_NAME.'</a>';
                }
                $tender_no = Tender::where('TENDER_ID', $item->TENDER_ID)->pluck('TENDER_NO');
                $row[] = '<a href="#" title="Tender Info" data-toggle="modal" data-target="#modal_big_content" data-form="'.route('tender_view', $item->TENDER_ID).'">
                         '.$tender_no.'</a>';
                $project_name = ProjectDetails::with('getCategory', 'getProject')->where('PR_DETAIL_ID', $item->PR_DETAIL_ID)->first();
                $row[] = $project_name->getProject->PR_NAME_BN.'<br/>'.$project_name->getCategory->CATE_NAME.' নাম্বার '.$project_name->PR_SSF_NO;
                $row[] = number_format($item->CR_AMT, 2);
                $row[] = number_format($item->DR_AMT, 2);
                $row[] = number_format($item->Due_Amount, 2);

                $action = '';
                if ($access->CREATE == 1) {
                    $action .= '<a href="#" title="Select Final Applicant" class="label label-success" data-toggle="modal" data-target="#modal_big_content" data-form="'.route('tender_final_select_form', [$item->SCHEDULE_ID, $item->CITIZEN_ID == null ? 'applicant/'.$item->TE_APP_ID : 'citizen/'.$item->CITIZEN_ID]).'"><i class="fa fa-check"></i></a>';
                }
                $row[] = $action;

                $data[] = $row;
                $sl++;
            }
        }

        return [
            'draw'              => $input['draw'],
            'recordsTotal'      => $collection['total'],
            'recordsFiltered'   => $collection['filtered'],
            'data'              => $data,
        ];
    }

    /**
     * Tender security money datatable Output.
     *
     * @param $input
     * @param $collection
     *
     * @return array
     */
    public static function tenderSecurityMoneyOutput($input, $collection)
    {
        $sl = 1;
        $data = [];
        if ($collection) {
            foreach ($collection['data'] as $key => $item) {
                $row = [];
                $row[] = $sl;
                if (!empty($item->TE_APP_ID)) {
                    $row[] = '<a href="#" title="View Applicant Details" data-toggle="modal" data-target="#modal_big_content" data-form="'.route('tender_applicant_view', ['applicant', $item->TE_APP_ID, $item->SCHEDULE_ID]).'">'.$item->getApplicant->APPLICANT_NAME.'</a>';
                } else {
                    $row[] = '<a href="#" title="View Applicant Details" data-toggle="modal" data-target="#modal_big_content" data-form="'.route('tender_applicant_view', ['citizen', $item->CITIZEN_ID, $item->SCHEDULE_ID]).'">'.$item->getCitizen->getTenderCitizen->FULL_NAME.'</a>';
                }
                $row[] = '<a href="#" title="Tender Info" data-toggle="modal" data-target="#modal_big_content" data-form="'.route('tender_view', $item->TENDER_ID).'">'.$item->getTender->TENDER_NO.'</a>';
                $row[] = $item->getProject->PR_NAME_BN.'<br/>'.$item->getCategory->CATE_NAME.' নাম্বার '.$item->getProject->PR_SSF_NO;
                $row[] = number_format($item->AMOUNT, 2);

                if (!empty($item->TE_APP_ID)) {
                    $row[] = '<a href="#" title="Return money" data-toggle="modal" data-target="#modal_big_content" data-form="'.route('return_security_money', [$item->SCHEDULE_ID, 'applicant', $item->TE_APP_ID]).'"class="label label-success return_money"><i class="fa fa-recycle"></i></a>';
                } else {
                    $row[] = '<a href="#" title="Return money" data-toggle="modal" data-target="#modal_big_content" data-form="'.route('return_security_money', [$item->SCHEDULE_ID, 'citizen', $item->CITIZEN_ID]).'"class="label label-success return_money"><i class="fa fa-recycle"></i></a>';
                }

                $data[] = $row;
                $sl++;
            }
        }

        return [
            'draw'              => $input['draw'],
            'recordsTotal'      => $collection['total'],
            'recordsFiltered'   => $collection['filtered'],
            'data'              => $data,
        ];
    }

    /**
     * Citizen rent histody.
     *
     * @param array $input
     * @param array $collection
     *
     * @return array
     */
    public static function rentMonthOutput($input, $collection)
    {
        $data = [];
        if ($collection) {
            foreach ($collection['data'] as $key => $item) {
                $row = [];
                $row[] = $key + 1;
                $row[] = date('M Y', strtotime($item->VOUCHER_DT));
                $row[] = $item->VOUCHER_NO;
                $row[] = date('d/m/Y', strtotime($item->TRX_TRAN_DT));
                $row[] = $item->credit_amount;

                if ($item->debit_amount != $item->credit_amount) {
                    $row[] = '<span class="label label-danger"><i class="glyphicon glyphicon-remove-circle"></i></span>';
                } else {
                    $row[] = '<span class="label label-success"><i class="glyphicon glyphicon-ok-sign"></i></span>';
                }

                $data[] = $row;
            }
        }

        return [
            'draw'              => $input['draw'],
            'recordsTotal'      => $collection['total'],
            'recordsFiltered'   => $collection['filtered'],
            'data'              => $data,
        ];
    }

    /**
     * Citizen services.
     *
     * @param array $input
     * @param array $collection
     *
     * @return array
     */
    public static function citizenServiceOutput($input, $collection)
    {
        $data = [];
        $colm = [];
        if (count($collection['data']) > 0) {
            foreach ($collection['data'] as $key => $item) {
                $row = [];
                $row[] = $key + 1;
                if ($item->IS_LEASE == 0 && !empty($item->projectDetails)) {
                    $row[] = $item->getProject->PROJECT_LOCATION;
                    $row[] = $item->getProjectType->TYPE_NAME;
                    $row[] = $item->projectDetails->PR_MEASURMENT.' বর্গফুট';
                    $row[] = $item->tenderRentParticular->PARTICULAR_AMT;
                } else {
                    $row[] = $item->getProject->PR_NAME_BN;
                    $row[] = $item->getProject->PROJECT_LOCATION;
                    $row[] = date('d/m/Y', strtotime($item->leaseValidity->DATE_FROM)).' '.trans('common.to').' '.date('d/m/Y', strtotime($item->leaseValidity->DATE_TO));
                }
                $row[] = '<a href="#" data-toggle="modal" data-target="#modal_big_content" data-form="'.route('tender_view', $item->TENDER_ID).'" class="btn btn-default btn-xs" title="বিস্তারিত"><i class="fa fa-eye"></i></a>';

                $data[] = $row;
            }
        }
        if (isset($input['group']) && !empty($input['group'])) {
            $property = CitizenProperty::where('PR_CATEGORY', $input['group'])->pluck('IS_LEASE');
            if ($property == 1) {
                $colm = [trans('common.table_sl'), trans('lease.mahal_bn_name'), trans('lease.mahal_address'), trans('lease.lease_duration'), trans('common.details')];
            } else {
                $colm = [trans('common.table_sl'), 'অবস্থান', 'ধরন', 'পরিমান', 'ভাড়া &#40;প্রতি বর্গফুট&#41;', trans('common.details')];
            }
        }

        return [
            'draw'              => isset($input['draw']) ? $input['draw'] : 0,
            'columns'           => $colm,
            'recordsTotal'      => $collection['total'],
            'recordsFiltered'   => $collection['filtered'],
            'data'              => $data,
        ];
    }

    /**
     * Revenue report datatable Output.
     *
     * @author Nurullah <nurul@atilimited.net>
     *
     * @param array $input
     * @param array $collection
     *
     * @return array
     */
    public static function leaseRevenueDataList($input, $collection)
    {
        $data = [];
        if (count($collection) > 0) {
            foreach ($collection['data'] as $key => $item) {
                $row = [];
                $row[] = $key + 1;
                $row[] = $item->getSchedule->getProject->PR_NAME_BN;

                if (!empty($item->TE_APP_ID)) {
                    $row[] = $item->getApplicant->APPLICANT_NAME;
                } elseif ($item->CITIZEN_ID) {
                    $row[] = $item->getCitizen->getTenderCitizen->FULL_NAME;
                } elseif ($item->LEASE_DE_ID) {
                    $row[] = $item->getLeaseDemesne->NAME;
                }

                foreach ($item->voucherSchedule as $index => $vsc) {
                    if ($vsc->PUNIT_PRICE != '') {
                        $row[] = Helpers::numberConvert(number_format($vsc->PUNIT_PRICE, 2));
                    } else {
                        $row[] = '';
                    }
                }

                // Row balance
                if (count($row) <= 8) {
                    for ($i = 0; $i <= (8 - count($row)); $i++) {
                        $row[] = '--';
                        if (count($row) < 8) {
                            $row[] = '--';
                        }
                    }
                }

                // $row[]  = '<a href="'. route('revenue_data_print', $item->VOUCHER_NO) .'" target="_blank" class="label label-default"><i class="fa fa-print"></i></a>';
                $data[] = $row;
            }
        }

        return [
            'draw'              => $input['draw'],
            'recordsTotal'      => $collection['total'],
            'recordsFiltered'   => $collection['filtered'],
            'data'              => $data,
        ];
    }

    /**
     * Tender bid comparison datatable Output.
     *
     * @param $input
     * @param $collection
     *
     * @return array
     */
    public static function tenderallListOutput($input, $collection)//nurullah
    {
        $sl = 1;
        $data = [];
        $link_url = 'applicant_security_money'; // module url
        $access = Module::checkPrevilege($link_url); // get access info by module url
        if ($collection) {
            foreach ($collection['data'] as $key => $item) {
                $row = [];
                $row[] = $sl;
                if ($item->IS_CITIZEN == 0) {
                    if (!empty($item->getApplicant->getThana->THANA_ENAME)) {
                        $thana = $item->getApplicant->getThana->THANA_ENAME.', ';
                        $adrs = $thana.$item->getApplicant->ROAD_NO.', '.$item->getApplicant->HOLDING_NO;
                    } else {
                        $thana = '';
                    }
                } else {
                    if (!empty($item->getCitizen->getThana->THANA_ENAME)) {
                        $thana = $item->getCitizen->getThana->THANA_ENAME.', ';
                        $adrs = $thana.$item->getCitizen->ROAD_NO.', '.$item->getCitizen->HOLDING_NO;
                    } else {
                        $thana = '';
                    }
                }
                if ($item->IS_CITIZEN == 0) {
                    $row[] = '<a href="#" title="View Applicant Details" data-toggle="modal" data-target="#modal_big_content" data-form="'.route('tender_applicant_view', ['applicant', $item->TE_APP_ID, $item->SCHEDULE_ID]).'">'
                             .$item->getApplicant->APPLICANT_NAME.'</a><br>'.$item->getApplicant->APPLICANT_PHONE.'<br>'.$adrs;
                } else {
                    $row[] = '<a href="#" title="View Applicant Details" data-toggle="modal" data-target="#modal_big_content" data-form="'.route('tender_applicant_view', ['citizen', $item->CITIZEN_ID, $item->SCHEDULE_ID]).'">'
                             .$item->getCitizen->getTenderCitizen->FULL_NAME.'</a><br>'.$item->getCitizen->getTenderCitizen->MOBILE_NO.'<br>'.$adrs;
                }
                $row[] = '<a href="#" title="Tender Info" data-toggle="modal" data-target="#modal_big_content" data-form="'.route($item->IS_LEASE == 1 ? 'lease_tender_view' : 'tender_view', $item->TENDER_ID).'">
                         '.$item->getTender->TENDER_NO.'</a>';
                $row[] = $item->getProject->PR_NAME_BN;
                $row[] = $item->BG_AMOUNT;
                $action = '';
                if (!empty($item->TE_APP_ID)) {
                    if ($access->DELETE == 1) {
                        $action .= '<a href="#" title="Return money" data-toggle="modal" data-target="#modal_add_content" data-form="'.route('return_security_money', [$item->SCHEDULE_ID, 'applicant', $item->TE_APP_ID]).'"class="label label-success return_money"><i class="fa fa-recycle"></i></a>';
                    }
                } else {
                    if ($access->DELETE == 1) {
                        $action .= '<a href="#" title="Return money" data-toggle="modal" data-target="#modal_add_content" data-form="'.route('return_security_money', [$item->SCHEDULE_ID, 'citizen', $item->CITIZEN_ID]).'"class="label label-success return_money"><i class="fa fa-recycle"></i></a>';
                    }
                }
                $row[] = $action;
                $data[] = $row;
                $sl++;
            }
        }

        return [
            'draw'              => $input['draw'],
            'recordsTotal'      => $collection['total'],
            'recordsFiltered'   => $collection['filtered'],
            'data'              => $data,
        ];
    }

    //********************************

    /**
     * Module datatable Output.
     *
     * @author  Nurullah <nurul@atilimited.net>
     *
     *@param array $input
     * @param string $collection
     *
     * @return array
     */
    public static function moduleList($input, $collection)
    {
        $sl = 1;
        $data = [];
        $link_url = 'module_list'; // Module url
        $access = Module::checkPrevilege($link_url); // get access info by Module url
        if ($collection) {
            foreach ($collection['data'] as $key => $item) {
                $row = [];
                $row[] = $sl;
                $row[] = $item->MODULE_NAME;
                $row[] = $item->MODULE_NAME_BN;
                $row[] = $item->MODULE_ICON;

                $action = '';
                if ($access->STATUS == 1) {
                    if ($item->IS_ACTIVE == 1) {
                        $action .= '<a href="'.route('module_status', $item->MODULE_ID).'" data-isactive="1" class="label label-warning change_status">'.trans('common.table_inactive').'</a>';
                    } else {
                        $action .= '<a href="'.route('module_status', $item->MODULE_ID).'" data-isactive="0" class="label label-success change_status">'.trans('common.table_active').'</a>';
                    }
                }
                if ($access->UPDATE == 1) {
                    $action .= '<a href="#" class="label label-default" data-toggle="modal" data-target="#modal_add_content" data-form="'.route('module_edit_form', $item->MODULE_ID).'"><i class="fa fa-pencil"></i></a>';
                }
                if ($access->DELETE == 1) {
                    $action .= '<a href="'.route('module_delete', $item->MODULE_ID).'" class="label label-danger delete_post"><i class="fa fa-times"></i></a>';
                }

                $row[] = $action;
                $data[] = $row;
                $sl++;
            }
        }

        $output = [
            'draw'              => $input['draw'],
            'recordsTotal'      => $collection['total'],
            'recordsFiltered'   => $collection['filtered'],
            'data'              => $data,
        ];

        return $output;
    }

    //********************************

    /**
     * Module datatable Output.
     *
     * @author  Nurullah <nurul@atilimited.net>
     *
     *@param array $input
     * @param string $collection
     *
     * @return array
     */
    public static function moduleLinkList($input, $collection)
    {
        $sl = 1;
        $data = [];
        $link_url = 'module_link_list'; // Module Link url
        $access = Module::checkPrevilege($link_url); // get access info by Module link url
        if ($collection) {
            foreach ($collection['data'] as $key => $item) {
                $module = Module::where('MODULE_ID', $item->MODULE_ID)->first();
                $row = [];
                $row[] = $sl;
                $row[] = $module->MODULE_NAME;
                $row[] = $item->LINK_NAME;
                $row[] = $item->LINK_NAME_BN;
                $row[] = $item->LINK_URI;

                $action = '';
                if ($access->STATUS == 1) {
                    if ($item->IS_ACTIVE == 1) {
                        $action .= '<a href="'.route('link_status', $item->LINK_ID).'" data-isactive="1" class="label label-warning change_status">'.trans('common.table_inactive').'</a>';
                    } else {
                        $action .= '<a href="'.route('link_status', $item->LINK_ID).'" data-isactive="0" class="label label-success change_status">'.trans('common.table_active').'</a>';
                    }
                }
                if ($access->UPDATE == 1) {
                    $action .= '<a href="#" class="label label-default" data-toggle="modal" data-target="#modal_add_content" data-form="'.route('module_link_edit_form', $item->LINK_ID).'"><i class="fa fa-pencil"></i></a>';
                }
                if ($access->DELETE == 1) {
                    $action .= '<a href="'.route('link_delete', $item->LINK_ID).'" class="label label-danger delete_post"><i class="fa fa-times"></i></a>';
                }
                $row[] = $action;
                $data[] = $row;
                $sl++;
            }
        }

        $output = [
            'draw'              => $input['draw'],
            'recordsTotal'      => $collection['total'],
            'recordsFiltered'   => $collection['filtered'],
            'data'              => $data,
        ];

        return $output;
    }
}
