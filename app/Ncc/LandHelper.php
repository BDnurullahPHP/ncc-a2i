<?php

namespace App\Ncc;

/**
 * Class     LandHelper.
 *
 * @author   Abdul Awal <abdulawal@atilimited.net>
 */
class LandHelper
{
    /**
     * Generate land number.
     *
     * @param string $area
     * @param string $ward
     * @param string $mouza
     * @param string $jl_no
     *
     * @return string
     */
    public static function landNumber($area, $ward, $mouza, $jl_no, $user)
    {
        return $area.'-'.$ward.'-'.$mouza.'-'.$jl_no.'-'.$user.'-'.mt_rand(10000, 99999);
    }
}
