<?php

namespace App\Models;

use App\Ncc\Helpers;
use Illuminate\Database\Eloquent\Model;

class Tender extends Model
{
    protected $table = 'tender';
    protected $primaryKey = 'TENDER_ID';
    const CREATED_AT = 'CREATED_AT';
    const UPDATED_AT = 'UPDATED_AT';
    protected $fillable = [
                            'TENDER_NO',
                            'PRE_TENDER_ID',
                            'TENDER_METHOD',
                            'TENDER_TITLE',
                            'TENDER_DESC',
                            'TENDER_DT',
                            'TENDER_PUBLISH_DT',
                            'SC_LAST_SELLING_DT',
                            'TE_LAST_RECIEVE_DT',
                            'TE_OPENING_DT',
                            'IS_ACTIVE',
                            'CREATED_AT',
                            'CREATED_BY',
                            'UPDATED_AT',
                            'UPDATED_BY',
                        ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = ['TENDER_ID'];

    /**
     * Create or update tender data.
     *
     * @param $data array
     * @param $user string
     *
     * @return string
     */
    public static function createData($data, $user)
    {
        if (isset($data['tender_id'])) {
            $tender = self::find($data['tender_id']);
            $tender->UPDATED_BY = $user;
        } else {
            $tender = new self();
            $tender->CREATED_BY = $user;
        }
        if (isset($data['re_tender']) && !empty($data['re_tender'])) {
            $tender->PRE_TENDER_ID = $data['re_tender'];
        }

        $tender->TENDER_NO = $data['tender_no'];
        $tender->TENDER_METHOD = $data['tender_method'];
        $tender->TENDER_TITLE = $data['tender_title'];
        $tender->TENDER_DESC = $data['tender_desc'];
        $tender->TENDER_DT = date('Y-m-d', strtotime(str_replace('/', '-', $data['tender_dt'])));
        $tender->TENDER_PUBLISH_DT = date('Y-m-d', strtotime(str_replace('/', '-', $data['tender_publish_dt'])));
        $tender->IS_LEASE = 0;
        $tender->IS_ACTIVE = 1;
        $tender->save();

        $tenderId = $tender->TENDER_ID;

        $location_array = array_merge($data['tender_location'], $data['tender_copy_location']);

        // Create tender location data
        foreach ($location_array as $key => $loc) {
            if (isset($loc['location_id'])) {
                $tender_location = TenderLocation::find($loc['location_id']);
                $tender_location->UPDATED_BY = $user;
            } else {
                $tender_location = new TenderLocation();
                $tender_location->CREATED_BY = $user;
            }

            $tender_location->TENDER_ID = $tenderId;
            $tender_location->LOCATION_ID = $loc['location'];
            $tender_location->TYPE = $loc['type'];
            $tender_location->IS_ACTIVE = 1;
            $tender_location->save();
        }

        // Create tender rules
        foreach ($data['tender_rules'] as $key => $trl) {
            if (isset($trl['con_id'])) {
                $tender_condition = TenderConditions::find($trl['con_id']);
                $tender_condition->UPDATED_BY = $user;
            } else {
                $tender_condition = new TenderConditions();
                $tender_condition->CREATED_BY = $user;
            }
            $tender_condition->TENDER_ID = $tenderId;
            $tender_condition->PR_CATEGORY = $trl['category'];
            $tender_condition->CON_DESC = $trl['rules'];
            $tender_condition->IS_ACTIVE = 1;
            $tender_condition->save();
        }

        // Create tender date data
        if (isset($data['date_id'])) {
            $tender_date_time = TenderDateTime::find($data['date_id']);
            $tender_date_time->UPDATED_BY = $user;
        } else {
            $tender_date_time = new TenderDateTime();
            $tender_date_time->CREATED_BY = $user;
        }

        //$lst_sdt_arr = explode(' - ', $data['sc_last_selling_date']);
        $lst_sdt_from = $data['sc_last_selling_date'].' '.$data['sc_last_selling_tfrom'];
        $lst_sdt_to = $data['sc_last_selling_date'].' '.$data['sc_last_selling_tto'];

        $tender_date_time->TENDER_ID = $tenderId;
        $tender_date_time->LAST_SELLING_DT_FROM = date('Y-m-d', strtotime(Helpers::full_time_format($lst_sdt_from)));
        $tender_date_time->LAST_SELLING_DT_TO = date('Y-m-d', strtotime(Helpers::full_time_format($lst_sdt_to)));
        $tender_date_time->TE_LAST_RECIEVE_DT = date('Y-m-d', strtotime(Helpers::full_time_format($data['te_last_receive_dt'])));
        $tender_date_time->TE_OPENING_DT = date('Y-m-d', strtotime(Helpers::full_time_format($data['te_opening_dt'])));
        $tender_date_time->IS_ACTIVE = 1;
        $tender_date_time->save();

        // Create tender schedule and tender particular data
        if (count($data['tender_schedule']) > 0) {
            foreach ($data['tender_schedule'] as $key => $project_cat) {
                if (count($project_cat) > 0) {
                    foreach ($project_cat as $index => $ts) {
                        $projectID = explode(',', $ts['pr_details']); // [0] PROJECT_ID and [1] PR_DETAIL_ID
                        if (isset($ts['schedule_id'])) {
                            $tender_schedule = TenderSchedule::find($ts['schedule_id']);
                            $tender_schedule->UPDATED_BY = $user;
                        } else {
                            $tender_schedule = new TenderSchedule();
                            $tender_schedule->SC_SERIAL = TenderSchedule::max('SC_SERIAL') + 1;
                            $tender_schedule->CREATED_BY = $user;
                        }
                        $tender_schedule->TENDER_ID = $tenderId;
                        $tender_schedule->PROJECT_ID = $projectID[0];
                        $tender_schedule->PR_DETAIL_ID = $projectID[1];
                        $tender_schedule->PR_CATEGORY = $ts['pr_category'];
                        $tender_schedule->PR_TYPE = $ts['pr_type'];
                        $tender_schedule->LOWEST_TENDER_MONEY = $ts['lt_money'];
                        $tender_schedule->IS_CURRENT = 1;
                        $tender_schedule->IS_ACTIVE = 1;
                        $tender_schedule->save();

                        $schedule_id = $tender_schedule->SCHEDULE_ID;
                        //var_dump(count($ts['tender_particular']));exit;
                        if (count($ts['tender_particular']) > 0) {
                            foreach ($ts['tender_particular'] as $inx => $tp) {
                                var_dump($tp);
                                /*$tender_particular = array(
                                    'UPDATED_BY' => $user,
                                    'PARTICULAR_SL' => $max_sl+1,
                                    'CREATED_BY' => $user,
                                    'SCHEDULE_ID' => $schedule_id,
                                    'PARTICULAR_ID' => $tp['particular_id'],
                                    'PARTICULAR_AMT' => $tp['particular_amt'],
                                    'UOM' => $tp['uom'],
                                    'IS_ACTIVE' => 1,
                                );
                                $inserUpdate[] = $tender_particular;
                                var_dump($inserUpdate);exit;*/
                                //var_dump($inx);//exit;
                                $max_sl = TenderParticulars::max('PARTICULAR_SL');

                                if (isset($tp['tp_id'])) {
                                    $tender_particular = TenderParticulars::find($tp['tp_id']);
                                    $tender_particular->UPDATED_BY = $user;
                                } else {
                                    $tender_particular = new TenderParticulars();
                                    $tender_particular->PARTICULAR_SL = $max_sl + 1;
                                    $tender_particular->CREATED_BY = $user;
                                }
                                $tender_particular->SCHEDULE_ID = $schedule_id;
                                $tender_particular->PARTICULAR_ID = $tp['particular_id'];
                                $tender_particular->PARTICULAR_AMT = $tp['particular_amt'];
                                $tender_particular->UOM = $tp['uom'];
                                $tender_particular->IS_ACTIVE = 1;
                                $tender_particular->save();
                            }
                            var_dump($tender_particular);
                            exit;
                        }
                    }
                }
            }
        }

        return $tenderId;
    }

    /**
     * Create or update lease tender data.
     *
     * @param $data array
     * @param $user string
     *
     * @return string
     */
    public static function leaseTenderData($data, $user)
    {
        if (isset($data['tender_id'])) {
            $tender = self::find($data['tender_id']);
            $tender->UPDATED_BY = $user;
        } else {
            $tender = new self();
            $tender->CREATED_BY = $user;
        }

        if (isset($data['re_tender_id']) && !empty($data['re_tender_id'])) {
            $tender->PRE_TENDER_ID = $data['re_tender_id'];

            TenderDateTime::where('TENDER_ID', $data['re_tender_id'])->update(['IS_ACTIVE' => 0, 'UPDATED_BY' => $user]);
        }

        $tender->TENDER_NO = $data['tender_no'];
        $tender->TENDER_METHOD = $data['tender_method'];
        $tender->TENDER_TITLE = $data['tender_title'];
        $tender->TENDER_DESC = $data['tender_desc'];
        $tender->TENDER_DT = date('Y-m-d', strtotime(str_replace('/', '-', $data['tender_dt'])));
        $tender->TENDER_PUBLISH_DT = date('Y-m-d', strtotime(str_replace('/', '-', $data['tender_publish_dt'])));
        $tender->IS_LEASE = 1;
        $tender->IS_ACTIVE = $data['is_active'];
        $tender->save();

        $tenderId = $tender->TENDER_ID;
        $location_array = array_merge($data['tender_location'], $data['tender_copy_location']);

        // Create tender location data
        foreach ($location_array as $key => $loc) {
            if (isset($loc['location_id'])) {
                $tender_location = TenderLocation::find($loc['location_id']);
                $tender_location->UPDATED_BY = $user;
            } else {
                $tender_location = new TenderLocation();
                $tender_location->CREATED_BY = $user;
            }

            $tender_location->TENDER_ID = $tenderId;
            $tender_location->LOCATION_ID = $loc['location'];
            $tender_location->TYPE = $loc['type'];
            $tender_location->IS_ACTIVE = 1;
            $tender_location->save();
        }

        // Create tender rules
        if (isset($data['rules_con_id'])) {
            $tender_condition = TenderConditions::find($data['rules_con_id']);
            $tender_condition->UPDATED_BY = $user;
        } else {
            $tender_condition = new TenderConditions();
            $tender_condition->CREATED_BY = $user;
        }
        $tender_condition->TENDER_ID = $tenderId;
        $tender_condition->PR_CATEGORY = $data['rules_category'];
        $tender_condition->CON_DESC = $data['rules_data'];
        $tender_condition->IS_ACTIVE = 1;
        $tender_condition->save();

        // Create tender date data
        if (count($data['tender_date']) > 0) {
            foreach ($data['tender_date'] as $key => $tender_dt) {
                if (isset($tender_dt['date_id'])) {
                    $tender_date_time = TenderDateTime::find($tender_dt['date_id']);
                    $tender_date_time->UPDATED_BY = $user;
                } else {
                    $tender_date_time = new TenderDateTime();
                    $tender_date_time->CREATED_BY = $user;
                }

                //$lst_sdt_arr = explode(' - ', $tender_dt['last_sale_date']);
                $lst_st_arr = explode(' - ', $tender_dt['last_sale_time']);
                $lst_sdt_from = $tender_dt['last_sale_date'].' '.$lst_st_arr[0];
                $lst_sdt_to = $tender_dt['last_sale_date'].' '.$lst_st_arr[1];

                $tender_date_time->TENDER_ID = $tenderId;
                $tender_date_time->LAST_SELLING_DT_FROM = date('Y-m-d', strtotime(Helpers::full_time_format($lst_sdt_from)));
                $tender_date_time->LAST_SELLING_DT_TO = date('Y-m-d', strtotime(Helpers::full_time_format($lst_sdt_to)));
                $tender_date_time->TE_LAST_RECIEVE_DT = date('Y-m-d', strtotime(Helpers::full_time_format($tender_dt['receive_date'])));
                $tender_date_time->TE_OPENING_DT = date('Y-m-d', strtotime(Helpers::full_time_format($tender_dt['opening_date'])));
                $tender_date_time->CLAUSE = $tender_dt['clause'];
                $tender_date_time->IS_ACTIVE = 1;
                $tender_date_time->save();
            }
        }

        // Create tender schedule and tender particular data
        if (count($data['tender_schedule']) > 0) {
            foreach ($data['tender_schedule'] as $key => $ts) {
                if (isset($ts['schedule_id'])) {
                    $tender_schedule = TenderSchedule::find($ts['schedule_id']);
                    $tender_schedule->UPDATED_BY = $user;
                } else {
                    $tender_schedule = new TenderSchedule();
                    $tender_schedule->SC_SERIAL = TenderSchedule::max('SC_SERIAL') + 1;
                    $tender_schedule->CREATED_BY = $user;
                }
                $tender_schedule->TENDER_ID = $tenderId;
                $tender_schedule->PROJECT_ID = $ts['project_id'];
                $tender_schedule->PR_CATEGORY = $ts['pr_category'];
                $tender_schedule->IS_CURRENT = 1;
                $tender_schedule->IS_ACTIVE = 1;
                $tender_schedule->save();

                $schedule_id = $tender_schedule->SCHEDULE_ID;

                if (count($ts['tender_particular']) > 0) {
                    foreach ($ts['tender_particular'] as $inx => $tp) {
                        $max_sl = TenderParticulars::max('PARTICULAR_SL');

                        if (isset($tp['tp_id'])) {
                            $tender_particular = TenderParticulars::find($tp['tp_id']);
                            $tender_particular->UPDATED_BY = $user;
                        } else {
                            $tender_particular = new TenderParticulars();
                            $tender_particular->PARTICULAR_SL = $max_sl + 1;
                            $tender_particular->CREATED_BY = $user;
                        }
                        $tender_particular->SCHEDULE_ID = $schedule_id;
                        $tender_particular->PARTICULAR_ID = $tp['particular_id'];
                        $tender_particular->PARTICULAR_AMT = $tp['particular_amt'];
                        $tender_particular->UOM = $tp['uom'];
                        $tender_particular->IS_ACTIVE = 1;
                        $tender_particular->save();
                    }
                }
            }
        }

        return $tenderId;
    }

    public static function getData($input)
    {
        $data = self::leftJoin('tender_date_time', 'tender_date_time.TENDER_ID', '=', 'tender.TENDER_ID')
                        ->where('IS_LEASE', 0)
                        ->orderBy('tender.TENDER_ID', 'desc')
                        ->take($input['length'])
                        ->skip($input['start'])
                        ->get();

        $total = self::where('IS_LEASE', 0)->count();

        return ['data' => $data, 'total' => $total, 'filtered' => $total];
    }

    public static function getLeaseData($input)
    {
        $data = self::where('IS_LEASE', 1)
                        ->orderBy('TENDER_ID', 'desc')
                        ->take($input['length'])
                        ->skip($input['start'])
                        ->get();

        $total = self::where('IS_LEASE', 1)->count();

        return ['data' => $data, 'total' => $total, 'filtered' => $total];
    }

    /**
     * Get current tender count.
     *
     * @return data count
     */
    public static function getCurrentTenderCount()
    {
        return TenderSchedule::leftJoin('tender', 'tender.TENDER_ID', '=', 'tender_schedule.TENDER_ID')
                    ->leftJoin('tender_date_time', 'tender_date_time.TENDER_ID', '=', 'tender_schedule.TENDER_ID')
                    ->select('tender_schedule.*', 'tender.*', 'tender_date_time.*')
                    ->where('tender.IS_LEASE', 0)
                    ->where('tender.IS_ACTIVE', 1)
                    ->where('tender_date_time.IS_ACTIVE', 1)
                    ->where('tender.TENDER_PUBLISH_DT', '<=', date('Y-m-d'))
                    ->where('tender_date_time.LAST_SELLING_DT_FROM', '>=', date('Y-m-d'))
                    ->count();
    }

    /**
     * Get current lease count.
     *
     * @return data count
     */
    public static function getCurrentLeaseCount()
    {
        return TenderSchedule::leftJoin('tender', 'tender.TENDER_ID', '=', 'tender_schedule.TENDER_ID')
                    ->leftJoin('tender_date_time', 'tender_date_time.TENDER_ID', '=', 'tender_schedule.TENDER_ID')
                    ->select('tender_schedule.*', 'tender.*', 'tender_date_time.*')
                    ->where('tender.IS_LEASE', 1)
                    ->where('tender.IS_ACTIVE', 1)
                    ->where('tender_date_time.IS_ACTIVE', 1)
                    ->where('tender.TENDER_PUBLISH_DT', '<=', date('Y-m-d'))
                    ->where('tender_date_time.LAST_SELLING_DT_FROM', '>=', date('Y-m-d'))
                    ->count();
    }

    /**
     * Get tender schedule.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function getTenderSchedule()
    {
        return $this->belongsTo('App\Models\TenderSchedule', 'TENDER_ID', 'TENDER_ID');
    }
}
