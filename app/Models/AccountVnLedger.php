<?php

namespace App\Models;

use DB;
use Illuminate\Database\Eloquent\Model;

class AccountVnLedger extends Model
{
    protected $table = 'ac_vn_ledgers';
    protected $primaryKey = 'VLEDGER_NO';
    const CREATED_AT = 'CREATED_AT';
    const UPDATED_AT = 'UPDATED_AT';
    protected $fillable = [
                                'VLEDGER_DT',
                                'TRX_CODE_ID',
                                'TRX_TRAN_NO',
                                'VOUCHER_NO',
                                'CR_AMT',
                                'DR_AMT',
                                'PITEM_TQTY',
                                'REMARKS',
                                'IS_ACTIVE',
                                'CREATED_AT',
                                'CREATED_BY',
                                'UPDATED_AT',
                                'UPDATED_BY',
                            ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = ['VLEDGER_NO'];

    /**
     * Get ledger data.
     *
     * @param $citizen
     * @param $schedule
     *
     * @return collections
     */
    public static function rentPendingMonth($citizen, $schedule)
    {
        return self::join('ac_voucherchd', 'ac_voucherchd.TRX_TRAN_NO', '=', 'ac_vn_ledgers.TRX_TRAN_NO')
                                ->leftJoin('ac_vouchermst', 'ac_vouchermst.VOUCHER_NO', '=', 'ac_voucherchd.VOUCHER_NO')
                                ->select('*', DB::raw('SUM(CASE WHEN ac_vn_ledgers.TRX_CODE_ID = 5 THEN ac_vn_ledgers.DR_AMT END) AS debit_amount, SUM(CASE WHEN ac_vn_ledgers.TRX_CODE_ID = 4 THEN ac_vn_ledgers.CR_AMT END) AS credit_amount'))
                                ->where('ac_vouchermst.CITIZEN_ID', $citizen)
                                ->where('ac_voucherchd.SCHEDULE_ID', $schedule)
                                ->where('ac_voucherchd.PARTICULAR_ID', 5)
                                ->groupBy('ac_vn_ledgers.TRX_TRAN_NO')
                                ->get();
    }

    /**
     * Get rent ledger data.
     *
     * @param $citizen
     * @param $input
     *
     * @return collections
     */
    public static function rentMonthData($citizen, $input)
    {
        $query = self::join('ac_voucherchd', 'ac_voucherchd.TRX_TRAN_NO', '=', 'ac_vn_ledgers.TRX_TRAN_NO')
                                ->leftJoin('ac_vouchermst', 'ac_vouchermst.VOUCHER_NO', '=', 'ac_voucherchd.VOUCHER_NO')
                                ->select('*', DB::raw('SUM(CASE WHEN ac_vn_ledgers.TRX_CODE_ID = 5 THEN ac_vn_ledgers.DR_AMT END) AS debit_amount, SUM(CASE WHEN ac_vn_ledgers.TRX_CODE_ID = 4 THEN ac_vn_ledgers.CR_AMT END) AS credit_amount'))
                                ->where('ac_vouchermst.CITIZEN_ID', $citizen)
                                ->where('ac_voucherchd.PARTICULAR_ID', 5);

        if (!empty($input['schedule'])) {
            $query->where('ac_voucherchd.SCHEDULE_ID', $input['schedule']);
        }
        if (!empty($input['year'])) {
            $year_range = explode('/', $input['year']);
            $from_date = date('Y-m-d', strtotime($year_range[0])).' 00:00:00';
            $to_date = date('Y-m-d', strtotime($year_range[1])).' 23:59:59';

            $query->whereBetween('ac_vouchermst.VOUCHER_DT', [$from_date, $to_date]);
        }

        $total = $query->count();
        $data_list = $query->groupBy('ac_vn_ledgers.TRX_TRAN_NO')
                            ->take($input['length'])
                            ->skip($input['start'])
                            ->get();

        return ['data' => $data_list, 'total' => $total, 'filtered' => $total];
    }

    /**
     * Get AccountVnLedger data with.
     *
     * @author      Nurullah <nurul@atilimited.net>
     *
     * @return collection
     */
    public static function getPendingPaymentData($input)// Pending Payment
    {
        $data = self::with(['getVoucherChd', 'getVoucher'=>  function ($query) {
            $query->with(['getApplicant', 'getLeaseDemesne', 'getApplicantProperty', 'getCitizen'=> function ($query) {
                $query->with(['getTenderCitizen', 'getCitizenApplication']);
            },
                                        ]);
        },
                                ])
                            ->where('TRX_CODE_ID', 5)
                            ->where('IS_VERIFIED', 0)
                            ->orderBy('VOUCHER_NO', 'desc')
                            ->take($input['length'])
                            ->skip($input['start'])
                            ->get();

        $total = self::where('TRX_CODE_ID', 5)->where('IS_VERIFIED', 0)->count();

        return ['data' => $data, 'total' => $total, 'filtered' => $total];
    }

    /**
     * approve payment.
     *
     * @author      Nurullah <nurul@atilimited.net>
     *
     * @param $type
     * @param $id
     * @param $user
     *
     * @return void
     */
    public static function approvePayment($id, $user)// Pending Payment
    {
        //$particuler = AccountVaucherChd::where('VOUCHER_NO', $no)->pluck('PARTICULAR_ID');
        // if ($particuler == 4 || $particuler == 7 ){
        //     if ($type == 'applicant'){
        //         $applicant = ApplicantProperty::where('TE_APP_ID', $id)->first();
        //         $applicant->IS_TE_SC_BOUGHT = 2;
        //         $applicant->UPDATED_BY = $user;
        //         $applicant->save();
        //     }  else {
        //         $applicant = CitizenApplication::where('CITIZEN_ID', $id)->first();
        //         $applicant->IS_TE_SC_BOUGHT = 2;
        //         $applicant->UPDATED_BY = $user;
        //         $applicant->save();
        //     }
        // }
        $leadger = self::where('VLEDGER_NO', $id)->first();
        $leadger->IS_VERIFIED = 1;
        $leadger->UPDATED_BY = $user;
        $leadger->save();
    }

    /**
     * Applicant payment history data collection.
     *
     * @param array      $input
     * @param collection $voucher
     *
     * @return Collection
     */
    public static function getHistoryData($input, $voucher)
    {
        $voucher_no = [];
        foreach ($voucher as $key => $v) {
            $voucher_no[$key] = $v->VOUCHER_NO;
        }
        $data = self::with(['getPaymentReturn', 'getVoucher', 'getVoucherChd' => function ($query) {
            $query->with('getParticular');
        }])
                                    ->whereIn('VOUCHER_NO', $voucher_no)
                                    ->orderBy('VLEDGER_NO', 'desc')
                                    //->groupBy('TRX_CODE_ID')
                                    ->take($input['length'])
                                    ->skip($input['start'])
                                    ->get();

        $total = self::whereIn('VOUCHER_NO', $voucher_no)->count();

        return ['data' => $data, 'total' => $total, 'filtered' => $total];
    }

    /**
     * Category wise total revenue.
     *
     * @return collection
     */
    public static function categoryRevenue()
    {
        return self::with('getCategory')
                                ->leftJoin('ac_voucherchd', 'ac_voucherchd.TRX_TRAN_NO', '=', 'ac_vn_ledgers.TRX_TRAN_NO')
                                ->leftJoin('tender_schedule', 'tender_schedule.SCHEDULE_ID', '=', 'ac_voucherchd.SCHEDULE_ID')
                                ->select(
                                        DB::raw('coalesce(SUM(CR_AMT), 0) as CR_AMT'),
                                        DB::raw('coalesce(SUM(DR_AMT), 0) as DR_AMT'),
                                        'tender_schedule.PR_CATEGORY'
                                    )
                                ->groupBy('tender_schedule.PR_CATEGORY')
                                ->orderBy('tender_schedule.PR_CATEGORY', 'asc')
                                ->get();
    }

    /**
     * Category wise total revenue.
     *
     * @return json
     */
    public static function categoryRevenuePie()
    {
        $data = [];
        $revenue = self::with('getCategory')
                                ->leftJoin('ac_voucherchd', 'ac_voucherchd.TRX_TRAN_NO', '=', 'ac_vn_ledgers.TRX_TRAN_NO')
                                ->leftJoin('tender_schedule', 'tender_schedule.SCHEDULE_ID', '=', 'ac_voucherchd.SCHEDULE_ID')
                                ->select(
                                        DB::raw('coalesce(SUM(CR_AMT), 0) as CR_AMT'),
                                        DB::raw('coalesce(SUM(DR_AMT), 0) as DR_AMT'),
                                        'tender_schedule.PR_CATEGORY'
                                    )
                                ->whereYear('VLEDGER_DT', '=', 'YEAR(CURDATE())')
                                ->groupBy('tender_schedule.PR_CATEGORY')
                                ->orderBy('tender_schedule.PR_CATEGORY', 'asc')
                                ->get();

        foreach ($revenue as $key => $rev) {
            $data[] = [$rev->getCategory->CATE_NAME => $rev->DR_AMT];
        }
        if (count($data) == 0) {
            $category = ProjectCategory::where('IS_ACTIVE', 1)->get();
            foreach ($category as $k => $cat) {
                $data[] = [$cat->CATE_NAME => 0];
            }
        }

        $data = json_encode($data, JSON_UNESCAPED_UNICODE);
        $data = str_replace('{', '[', $data);
        $data = str_replace('}', ']', $data);
        $data = str_replace(':', ',', $data);

        return $data;
    }

    /**
     * Get parent voucher child.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function getVoucherChd()
    {
        return $this->belongsTo('App\Models\AccountVaucherChd', 'TRX_TRAN_NO', 'TRX_TRAN_NO');
    }

    /**
     * Get payment data.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function getPaymentReturn()
    {
        return $this->hasOne('App\Models\PaymentResponse', 'TRX_TRAN_NO', 'TRX_TRAN_NO');
    }

    /**
     * Get voucher data.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function getVoucher()
    {
        return $this->belongsTo('App\Models\AccountVaucherMst', 'VOUCHER_NO', 'VOUCHER_NO');
    }

    /**
     * Get voucher chd data.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function getVoucherChdData()
    {
        return $this->hasOne('App\Models\AccountVaucherChd', 'VOUCHER_NO', 'VOUCHER_NO');
    }

    /**
     * Get category.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function getCategory()
    {
        return $this->belongsTo('App\Models\ProjectCategory', 'PR_CATEGORY', 'PR_CATEGORY');
    }
}
