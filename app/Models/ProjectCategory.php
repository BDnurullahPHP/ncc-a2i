<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProjectCategory extends Model
{
    protected $table = 'sa_category';
    protected $primaryKey = 'PR_CATEGORY';
    const CREATED_AT = 'CREATED_AT';
    const UPDATED_AT = 'UPDATED_AT';

    protected $fillable = [
                                'CATE_NAME',
                                'CATE_DESC',
                                'IS_ACTIVE',
                                'CREATED_BY',
                                'UPDATED_BY',
                            ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = ['PR_CATEGORY'];

    /**
     * Project Category data collection.
     *
     * @param $input array
     *
     * @return Collection
     */
    public static function getDataProjectCategory($input)
    {
        $data = self::where('PR_CATEGORY', '!=', 0)
                                ->select(['PR_CATEGORY', 'CATE_NAME', 'CATE_DESC', 'IS_ACTIVE', 'CREATED_AT'])
                                ->orderBy('PR_CATEGORY', 'desc')
                                ->take($input['length'])
                                ->skip($input['start'])
                                ->get();

        $total = self::count();

        return ['data' => $data, 'total' => $total, 'filtered' => $total];
    }

    /**
     * Get all category published tender schedule data.
     *
     * @return collection
     */
    public static function categorySchedule()
    {
        return self::with(['getSchedule' => function ($query) {
            $query->with(['getProject', 'getCategory', 'getProjectType', 'getProjectDetail'])
                              ->leftJoin('tender', 'tender.TENDER_ID', '=', 'tender_schedule.TENDER_ID')
                              ->leftJoin('tender_date_time', 'tender_date_time.TENDER_ID', '=', 'tender_schedule.TENDER_ID')
                              ->where('tender.TENDER_PUBLISH_DT', '<=', date('Y-m-d'))
                              ->where('tender_date_time.LAST_SELLING_DT_FROM', '>=', date('Y-m-d'))
                              ->where('tender.IS_ACTIVE', 1)
                              ->orderBy('tender_schedule.SCHEDULE_ID', 'desc');
        }])
                    ->where('IS_ACTIVE', 1)
                    ->get();
    }

    /**
     * Get tender schedule data by category id.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function getSchedule()
    {
        return $this->hasMany('App\Models\TenderSchedule', 'PR_CATEGORY', 'PR_CATEGORY');
    }
}
