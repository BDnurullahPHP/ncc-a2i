<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ApplicantProperty extends Model
{
    protected $table = 'applicant_property';
    protected $primaryKey = 'APP_PRO_ID';
    const CREATED_AT = 'CREATED_AT';
    const UPDATED_AT = 'UPDATED_AT';
    protected $fillable = [
                                'TE_APP_ID',
                                'TENDER_ID',
                                'SCHEDULE_ID',
                                'PROJECT_ID',
                                'PR_DETAIL_ID',
                                'PR_CATEGORY',
                                'PR_TYPE',
                                'IS_TE_SC_BOUGHT',
                                'TE_SC_AMOUNT',
                                'TE_SC_DATE',
                                'BID_AMOUNT',
                                'BID_AMOUNT_TEXT',
                                'BG_AMOUNT',
                                'BG_AMOUNT_TEXT',
                                'BANK_ID',
                                'B_DRAFT_NO',
                                'B_DRAFT_DATE',
                                'B_DRAFT_ATTACHMENT',
                                'REMARKS',
                                'INITIAL_REMARKS',
                                'CANCEL_REMARKS',
                                'IS_SELECTED',
                                'IS_LEASE',
                                'IS_CANCEL',
                                'IS_CITIZEN',
                                'IS_VIEW',
                                'IS_ACTIVE',
                                'CREATED_BY',
                                'UPDATED_BY',
                            ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = ['APP_PRO_ID'];

    /**
     * Get parent applicant.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function getApplicant()
    {
        return $this->belongsTo('App\Models\TenderApplicant', 'TE_APP_ID', 'TE_APP_ID');
    }

    /**
     * Get Tender No.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function getTender()
    {
        return $this->belongsTo('App\Models\Tender', 'TENDER_ID', 'TENDER_ID');
    }

    /**
     * Get tender schedule.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function getSchedule()
    {
        return $this->belongsTo('App\Models\TenderSchedule', 'SCHEDULE_ID', 'SCHEDULE_ID');
    }

    /**
     * Get parent schedule.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function cityCorporationRate()
    {
        return $this->belongsTo('App\Models\TenderSchedule', 'SCHEDULE_ID', 'SCHEDULE_ID');
    }

    /**
     * Get parent Project.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function getProject()
    {
        return $this->belongsTo('App\Models\Project', 'PROJECT_ID', 'PROJECT_ID');
    }

    /**
     * Get parent bank.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function getBank()
    {
        return $this->belongsTo('App\Models\Bank', 'BANK_ID', 'BANK_ID');
    }
}
