<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LandCase extends Model
{
    protected $table = 'land_case';
    protected $primaryKey = 'LAND_CASE_ID';
    const CREATED_AT = 'CREATED_AT';
    const UPDATED_AT = 'UPDATED_AT';
    protected $fillable = [
                                'LAND_ID',
                                'CASE_NUMBER',
                                'COMPLAINANT',
                                'DEFENDANT',
                                'COURT_DOCUMENT',
                                'SETTLEMENT_DATE',
                                'REMARKS',
                                'IS_ACTIVE',
                                'CREATED_BY',
                                'UPDATED_BY',
                            ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = ['LAND_CASE_ID'];
}
