<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class District extends Model
{
    protected $table = 'sa_districts';
    protected $primaryKey = 'DISTRICT_ID';
    const CREATED_AT = 'CREATED_AT';
    const UPDATED_AT = 'UPDATED_AT';
    protected $fillable = [
                                'DIVISION_ID',
                                'DISTRICT_ENAME',
                                'DISTRICT_LNAME',
                                'UD_DISTRICT_CODE',
                                'ORDER_SL',
                                'IS_ACTIVE',
                                'CREATED_BY',
                                'UPDATED_BY',
                            ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = ['DISTRICT_ID'];

    /**
     * Mouza data collection.
     *
     * @param $input array
     *
     * @return Collection
     */
    public static function getData($input)
    {
        $data = Mouza::with(['getWard'])
                ->select(['MOUZA_ID', 'WARD_ID', 'MOUZA_NAME', 'MOUZA_NAME_BN', 'JL_NO', 'IS_ACTIVE', 'CREATED_AT'])
                ->take($input['length'])
                ->skip($input['start'])
                ->get();

        $total = Mouza::count();

        return ['data' => $data, 'total' => $total, 'filtered' => $total];
    }

    /**
     * Get parent category.
     */
    public function getWard()
    {
        return $this->belongsTo('App\Models\Zone', 'WARD_ID', 'WARD_ID');
    }
}
