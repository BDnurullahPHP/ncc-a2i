<?php

namespace App\Models;

use DB;
use Illuminate\Database\Eloquent\Model;

class ModuleAssignORG extends Model
{
    protected $table = 'sa_org_modules';
    protected $primaryKey = 'ORG_MODULE_ID';

    const CREATED_AT = 'CREATED_AT';
    const UPDATED_AT = 'UPDATED_AT';

    protected $fillable = ['MODULE_ID', 'MODULE_NAME', 'ORG_ID', 'DEFAULT_FLAG', 'IS_ACTIVE', 'CREATED_BY', 'UPDATED_BY'];

    /**
     * Save or update Module.
     *
     *@author  Nurullah <nurul@atilimited.net>
     *
     * @param array       $data
     * @param string      $user
     * @param string|null $id
     *
     * @return string
     */
    public static function createModule($data, $user, $id)
    {
        $maxSlNo = DB::table('sa_modules')->max('SL_NO');
        if (!empty($id)) {
            $module = Module::find($id);
            $module->IS_ACTIVE = isset($data['IS_ACTIVE']) ? $data['IS_ACTIVE'] : 0;
            $module->UPDATED_BY = $user;
        } else {
            $module = new Module();
            $module->SL_NO = $maxSlNo + 1;
            $module->CREATED_BY = $user;
        }

        $module->MODULE_NAME = $data['MODULE_NAME'];
        $module->MODULE_NAME_BN = $data['MODULE_NAME_BN'];
        $module->MODULE_ICON = $data['MODULE_ICON'];

        $module->save();
    }

    /**
     * Module data collection.
     *
     *@author  Nurullah <nurul@atilimited.net>
     *
     * @param $input array
     *
     * @return Collection
     */
    public static function getData($input)
    {
        $data = Module::orderBy('SL_NO', 'desc')
                    ->take($input['length'])
                    ->skip($input['start'])
                    ->get();

        $total = Module::count();

        return ['data' => $data, 'total' => $total, 'filtered' => $total];
    }
}
