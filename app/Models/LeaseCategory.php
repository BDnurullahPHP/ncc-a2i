<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LeaseCategory extends Model
{
    protected $table = 'lease_category';
    protected $fillable = ['parent_id', 'name', 'bn_name', 'status', 'created_by', 'updated_by'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * Category data collection.
     *
     * @param $input array
     *
     * @return Collection
     */
    public static function getDatalease($input)
    {
        $data = self::where('parent_id', '=', 0)->select(['id', 'name', 'bn_name', 'status', 'created_at'])
                             ->orderBy('id', 'desc')
                             ->take($input['length'])
                             ->skip($input['start'])
                             ->get();

        $total = self::count();

        return ['data' => $data, 'total' => $total, 'filtered' => $total];
    }

    /**
     * Lease Subcategory data collection.
     *
     * @param $input array
     *
     * @return Collection
     */
    public static function leasegetSubcategoryData($input)
    {
        $data = self::where('parent_id', '!=', 0)
                             ->select(['id', 'parent_id', 'name', 'bn_name', 'status', 'created_at'])
                             ->orderBy('id', 'desc')
                             ->take($input['length'])
                             ->skip($input['start'])
                             ->get();

        $total = self::count();

        return ['data' => $data, 'total' => $total, 'filtered' => $total];
    }

    /**
     * Get parent category.
     */
    public function getCategory()
    {
        return $this->hasOne('App\Models\LeaseCategory', 'parent_id', 'id');
    }
}
