<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TenderDateTime extends Model
{
    protected $table = 'tender_date_time';
    protected $primaryKey = 'TENDER_DT_ID';
    const CREATED_AT = 'CREATED_AT';
    const UPDATED_AT = 'UPDATED_AT';

    protected $fillable = [
                            'TENDER_ID',
                            'LAST_SELLING_DT_FROM',
                            'LAST_SELLING_DT_TO',
                            'TE_LAST_RECIEVE_DT',
                            'TE_OPENING_DT',
                            'CLAUSE',
                            'IS_ACTIVE',
                            'CREATED_BY',
                            'UPDATED_BY',
                        ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = ['TENDER_DT_ID'];

    /**
     * Get category.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function getCategory()
    {
        return $this->belongsTo('App\Models\ProjectCategory', 'PR_CATEGORY', 'PR_CATEGORY');
    }

    /**
     * Get Project Type.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function getProjectType()
    {
        return $this->belongsTo('App\Models\ProjectType', 'PR_TYPE', 'PR_TYPE');
    }

    /**
     * Get Project Details.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function getProjectDetail()
    {
        return $this->belongsTo('App\Models\ProjectDetails', 'PR_DETAIL_ID', 'PR_DETAIL_ID');
    }
}
