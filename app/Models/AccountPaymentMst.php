<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AccountPaymentMst extends Model
{
    protected $table = 'ac_paymentmst';
    protected $primaryKey = 'TRX_TRAN_NO';
    const CREATED_AT = 'CREATED_AT';
    const UPDATED_AT = 'UPDATED_AT';
    protected $fillable = [
                                'TRX_TRAN_DT',
                                'TRAN_AMT',
                                'VOUCHER_NO',
                                'TRX_CODE_ID',
                                'COLLECTED_BY',
                                'TCANCEL_FG',
                                'REMARKS',
                                'IS_ACTIVE',
                                'CREATED_BY',
                                'UPDATED_BY',
                            ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = ['TRX_TRAN_NO'];
}
