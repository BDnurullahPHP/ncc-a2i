<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProjectFloor extends Model
{
    protected $table = 'sa_floor';
    protected $primaryKey = 'FLOOR_ID';
    const CREATED_AT = 'CREATED_AT';
    const UPDATED_AT = 'UPDATED_AT';
    //const CREATED_BY = 'CREATED_BY';

    protected $fillable = ['FLOOR_NUMBER', 'FLOOR_NUMBER_BN', 'DETAILS', 'IS_ACTIVE', 'CREATED_BY', 'UPDATED_BY'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = ['FLOOR_ID'];

    /**
     * Project Type data collection.
     *
     * @param $input array
     *
     * @return Collection
     */
    public static function getDataProjectFloor($input)
    {
        $data = self::where('FLOOR_ID', '!=', 0)->select(['FLOOR_ID', 'FLOOR_NUMBER', 'FLOOR_NUMBER_BN', 'DETAILS', 'IS_ACTIVE', 'CREATED_AT'])
                            ->orderBy('FLOOR_ID', 'desc')
                            ->take($input['length'])
                            ->skip($input['start'])
                            ->get();

        $total = self::count();

        return ['data' => $data, 'total' => $total, 'filtered' => $total];
    }
}
