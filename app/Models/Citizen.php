<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Citizen extends Model
{
    protected $table = 'sa_citizen';
    protected $primaryKey = 'CITIZEN_ID';
    const CREATED_AT = 'CREATED_AT';
    const UPDATED_AT = 'UPDATED_AT';
    protected $fillable = [
                                'USER_ID',
                                'TE_APP_ID',
                                'SPOUSE_NAME',
                                'NID',
                                'DIVISION_ID',
                                'DISTRICT_ID',
                                'THANA_ID',
                                'PO_ID',
                                'ROAD_NO',
                                'HOLDING_NO',
                                'IS_DEFAULTER',
                                'IS_ACTIVE',
                                'CREATED_BY',
                                'UPDATED_BY',
                            ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = ['CITIZEN_ID'];

    /**
     * Get citizen voucher.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function getCitizenVoucher()
    {
        return $this->hasMany('App\Models\AccountVaucherMst', 'CITIZEN_ID', 'CITIZEN_ID');
    }

    /**
     * Get parent user ifno.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function getTenderCitizen()
    {
        return $this->belongsTo('App\User', 'USER_ID', 'USER_ID');
    }

    /**
     * Get parent thana.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function getThana()
    {
        return $this->belongsTo('App\Models\Thana', 'THANA_ID', 'THANA_ID');
    }

    /**
     * Get parent bank.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function getBank()
    {
        return $this->belongsTo('App\Models\Bank', 'BANK_ID', 'BANK_ID');
    }

    /**
     * Get applicant data.
     *
     * @author      Nurullah <nurul@atilimited.net>
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function getCitizenApplication()
    {
        return $this->belongsTo('App\Models\CitizenApplication', 'CITIZEN_ID', 'CITIZEN_ID');
    }
}
