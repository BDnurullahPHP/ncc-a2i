<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ModuleLinkAddORG extends Model
{
    protected $table = 'sa_org_mlink';
    protected $primaryKey = 'ORG_MLINKS_ID';

    const CREATED_AT = 'CREATED_AT';
    const UPDATED_AT = 'UPDATED_AT';

    protected $fillable = ['MODULE_ID', 'LINK_ID', 'LINK_NAME', 'LINK_PAGES', 'LINK_URI', 'ORG_ID', 'CREATE', 'READ', 'UPDATE', 'DELETE', 'STATUS', 'IS_ACTIVE', 'CREATED_BY', 'UPDATED_BY'];

    /*
     * Save or update Module
     *@author  Nurullah <nurul@atilimited.net>
     * @param array $data
     * @param string $user
     * @param string|null $id
     * @return string
     */
}
