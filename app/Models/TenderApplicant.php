<?php

namespace App\Models;

use App\Ncc\Helpers;
use App\Role;
use App\User;
use DB;
use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Session;
use Zizaco\Entrust\Traits\EntrustUserTrait;

class TenderApplicant extends Model implements
    AuthenticatableContract,
                                    AuthorizableContract,
                                    CanResetPasswordContract
{
    use Authenticatable, Authorizable, CanResetPassword,
                EntrustUserTrait {
                    EntrustUserTrait::can insteadof Authorizable;
                }

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'tender_applicant';
    protected $primaryKey = 'TE_APP_ID';
    const CREATED_AT = 'CREATED_AT';
    const UPDATED_AT = 'UPDATED_AT';

    protected $fillable = [
                            'FIRST_NAME',
                            'LAST_NAME',
                            'APPLICANT_NAME',
                            'APPLICANT_PHONE',
                            'NID',
                            'PASSWORD',
                            'SPOUSE_NAME',
                            'EMAIL',
                            'REMEMBER_TOKEN',
                            'RESET_TOKEN',
                            'DIVISION_ID',
                            'DISTRICT_ID',
                            'THANA_ID',
                            'ROAD_NO',
                            'HOLDING_NO',
                            'IS_ACTIVE',
                            'CREATED_BY',
                            'UPDATED_BY',
                        ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['PASSWORD', 'REMEMBER_TOKEN'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = ['TE_APP_ID'];

    /**
     * Password need to be all time encrypted.
     *
     * @param string $password
     */
    public function setPasswordAttribute($password)
    {
        $this->attributes['PASSWORD'] = bcrypt($password);
    }

    // Override required, otherwise existing Authentication system will not match credentials
    public function getAuthPassword()
    {
        return $this->PASSWORD;
    }

    /**
     * Tender application form validation.
     *
     * @return array
     */
    public static function tenderApplicantRules()
    {
        return [
                'FIRST_NAME'        => 'required|min:3',
                'LAST_NAME'         => 'required|min:3',
                'SPOUSE_NAME'       => 'required|min:3',
                'APPLICANT_PHONE'   => 'required|min:11|max:14',
                'APPLICANT_EMAIL'   => 'email',
                'TE_APP_PASSWORD'   => 'required|min:6',
                'RE_PASSWORD'       => 'required|min:6',
                'NID'               => 'required',
                'division'          => 'required',
                'district'          => 'required',
                'thana'             => 'required',
                'ROAD_NO'           => 'required',
                'HOLDING_NO'        => 'required',
                'BID_AMOUNT'        => 'required|numeric',
                'BG_AMOUNT'         => 'required|numeric',
                'BG_AMOUNT_TEXT'    => 'required',
                'BID_AMOUNT_TEXT'   => 'required',

                // 'PAYMENT_METHOD'    => 'required',
                // 'B_DRAFT_NO'        => 'required_if:PAYMENT_METHOD,==,1',
                // 'BANK_ID'           => 'required_if:PAYMENT_METHOD,==,1',
                // 'B_DRAFT_DATE'      => 'required_if:PAYMENT_METHOD,==,1',
                // 'ATTACHMENT'        => 'required_if:PAYMENT_METHOD,==,1|max:1048576|mimes:jpg,jpeg,png',
            ];
    }

    /**
     * Create tender applicant data.
     *
     * @param $data array
     * @param $user string
     *
     * @return string
     */
    public static function tenderapplicantData($data)
    {
        $tender_schedule = TenderSchedule::where('SCHEDULE_ID', $data['SCHEDULE_ID'])->first();
        $type = $data['TYPE'] == 'tender' ? 0 : 1;

        if (isset($data['CITIZEN_ID']) && !empty($data['CITIZEN_ID'])) {

            // Create citizen applicant property data
            $citizen_application = self::storeCitizenApplication($tender_schedule, $data['CITIZEN_ID'], $data);
            // Create notification
            $notification = Notification::tenderApplication($data, null, $data['CITIZEN_ID'], $data['SCHEDULE_ID'], $data['TYPE']);
            // Create voucher data
            $acc_voucher = self::createVoucherData($data, $data['CITIZEN_ID'], $applicant = null, $type);

            $login_type = ['name' => 'citizen', 'type' => 2];
        } elseif (isset($data['TE_APPLICANT_ID']) && !empty($data['TE_APPLICANT_ID'])) {

            // Create tender applicant property data
            $app_property = self::storeAppProperty($tender_schedule, $data['TE_APPLICANT_ID'], $data);
            // Create notification
            $notification = Notification::tenderApplication($data, $data['TE_APPLICANT_ID'], null, $data['SCHEDULE_ID'], $data['TYPE']);
            // Create voucher data
            $acc_voucher = self::createVoucherData($data, $citizen = null, $data['TE_APPLICANT_ID'], $type);

            $login_type = ['name' => 'applicant_exist', 'type' => 1];
        } else {
            $te_app_id = self::createTenderApplicant($data);
            // Create tender applicant property data
            $app_property = self::storeAppProperty($tender_schedule, $te_app_id, $data);
            // Create notification
            $notification = Notification::tenderApplication($data, $te_app_id, null, $data['SCHEDULE_ID'], $data['TYPE']);
            // Create voucher data
            $acc_voucher = self::createVoucherData($data, $citizen = null, $te_app_id, $type);

            $login_type = ['name' => 'applicant', 'type' => 1];
        }

        $login_data = ['email' => $data['APPLICANT_EMAIL'], 'password' => $data['TE_APP_PASSWORD']];

        Session::forget('user_data');
        Session::put($login_type['name'], $login_data);

        return $login_type['type'];
    }

    /**
     * Create tender applicant data.
     *
     * @param $data array
     * @param $user string
     *
     * @return string
     */
    public static function saveTenderApplication($data, $user)
    {
        $tender_schedule = TenderSchedule::where('SCHEDULE_ID', $data['SCHEDULE_ID'])->first();
        $type = $data['TYPE'] == 'tender' ? 0 : 1;

        $te_app_id = self::createTenderApplicant($data);
        // Create tender applicant property data
        $app_property = self::storeAppProperty($tender_schedule, $te_app_id, $data);

        if ($data['PAYMENT_METHOD'] == 1 || $data['PAYMENT_METHOD'] == 3) {
            // Create voucher data
            $acc_voucher = self::createPaidVoucherData($data, $citizen = null, $te_app_id, $type);
            // Create notification
            $notification = Notification::tenderApplication($data, $te_app_id, null, $data['SCHEDULE_ID'], $data['TYPE'], true);
            // Send SMS
            $sms = Notification::generateApplySMS($data, $type, true);
            $send_sms = Helpers::sendSMS($data['APPLICANT_PHONE'], $sms);
        } else {
            // Create voucher data
            $acc_voucher = self::createVoucherData($data, $citizen = null, $te_app_id, $type);
            // Create notification
            $notification = Notification::tenderApplication($data, $te_app_id, null, $data['SCHEDULE_ID'], $data['TYPE'], false);
            // Send SMS
            $sms = Notification::generateApplySMS($data, $type, false);
            $send_sms = Helpers::sendSMS($data['APPLICANT_PHONE'], $sms);
        }
    }

    /**
     * Store citizen applicant property data.
     *
     * @param object $tender_schedule
     * @param string $citizen_id
     * @param array  $data
     *
     * @return string CITIZEN_APP_ID
     */
    private static function storeCitizenApplication($tender_schedule, $citizen_id, $data)
    {
        $citizen_applicnt = new CitizenApplication();
        $citizen_applicnt->CITIZEN_ID = $data['CITIZEN_ID'];
        $citizen_applicnt->TENDER_ID = $tender_schedule->TENDER_ID;
        $citizen_applicnt->SCHEDULE_ID = $data['SCHEDULE_ID'];
        $citizen_applicnt->PROJECT_ID = $tender_schedule->PROJECT_ID;
        $citizen_applicnt->PR_CATEGORY = $tender_schedule->PR_CATEGORY;

        if ($data['TYPE'] == 'tender') {
            $citizen_applicnt->PR_DETAIL_ID = $tender_schedule->PR_DETAIL_ID;
            $citizen_applicnt->PR_TYPE = $tender_schedule->PR_TYPE;
        }

        $citizen_applicnt->IS_TE_SC_BOUGHT = 0;
        $citizen_applicnt->TE_SC_AMOUNT = $data['TOTAL_AMOUNT'];
        $citizen_applicnt->BID_AMOUNT = $data['BID_AMOUNT'];
        $citizen_applicnt->BID_AMOUNT_TEXT = $data['BID_AMOUNT_TEXT'];
        $citizen_applicnt->BG_AMOUNT = $data['BG_AMOUNT'];
        $citizen_applicnt->BG_AMOUNT_TEXT = $data['BG_AMOUNT_TEXT'];
        $citizen_applicnt->IS_LEASE = $data['TYPE'] == 'tender' ? 0 : 1;
        $citizen_applicnt->IS_ACTIVE = 1;
        $citizen_applicnt->save();

        return $citizen_applicnt->CITIZEN_APP_ID;
    }

    /**
     * create tender applicant data.
     *
     * @param array $data
     *
     * @return string TE_APP_ID
     */
    private static function createTenderApplicant($data)
    {
        $tenderapplicant = new self();
        $tenderapplicant->FIRST_NAME = $data['FIRST_NAME'];
        $tenderapplicant->LAST_NAME = $data['LAST_NAME'];
        $tenderapplicant->APPLICANT_NAME = $data['FIRST_NAME'].' '.$data['LAST_NAME'];
        $tenderapplicant->EMAIL = isset($data['APPLICANT_EMAIL']) ? $data['APPLICANT_EMAIL'] : null;
        $tenderapplicant->SPOUSE_NAME = $data['SPOUSE_NAME'];
        $tenderapplicant->APPLICANT_PHONE = $data['APPLICANT_PHONE'];
        $tenderapplicant->NID = $data['NID'];
        $tenderapplicant->PASSWORD = $data['TE_APP_PASSWORD'];
        $tenderapplicant->DIVISION_ID = $data['division'];
        $tenderapplicant->DISTRICT_ID = $data['district'];
        $tenderapplicant->THANA_ID = $data['thana'];
        $tenderapplicant->ROAD_NO = $data['ROAD_NO'];
        $tenderapplicant->HOLDING_NO = $data['HOLDING_NO'];
        $tenderapplicant->IS_ACTIVE = 1;
        $tenderapplicant->save();

        return $tenderapplicant->TE_APP_ID;
    }

    /**
     * Store tender applicant property data.
     *
     * @param object $tender_schedule
     * @param string $te_app_id
     * @param array  $data
     *
     * @return string APP_PRO_ID
     */
    private static function storeAppProperty($tender_schedule, $te_app_id, $data)
    {
        $applicant_property = new ApplicantProperty();
        $applicant_property->TE_APP_ID = $te_app_id;
        $applicant_property->TENDER_ID = $tender_schedule->TENDER_ID;
        $applicant_property->SCHEDULE_ID = $data['SCHEDULE_ID'];
        $applicant_property->PROJECT_ID = $tender_schedule->PROJECT_ID;
        $applicant_property->PR_CATEGORY = $tender_schedule->PR_CATEGORY;

        if ($data['TYPE'] == 'tender') {
            $applicant_property->PR_DETAIL_ID = $tender_schedule->PR_DETAIL_ID;
            $applicant_property->PR_TYPE = $tender_schedule->PR_TYPE;
        }

        $applicant_property->IS_TE_SC_BOUGHT = 0;
        $applicant_property->TE_SC_AMOUNT = $data['TOTAL_AMOUNT'];
        $applicant_property->BID_AMOUNT = $data['BID_AMOUNT'];
        $applicant_property->BID_AMOUNT_TEXT = $data['BID_AMOUNT_TEXT'];
        $applicant_property->BG_AMOUNT = $data['BG_AMOUNT'];
        $applicant_property->BG_AMOUNT_TEXT = $data['BG_AMOUNT_TEXT'];
        $applicant_property->IS_LEASE = $data['TYPE'] == 'tender' ? 0 : 1;
        $applicant_property->IS_ACTIVE = 1;
        $applicant_property->save();

        return $applicant_property->APP_PRO_ID;
    }

    /**
     * Create voucher data.
     *
     * @param array  $data
     * @param string $citizen
     * @param string $applicant
     * @param string $type
     *
     * @return string VOUCHER_NO
     */
    private static function createVoucherData($data, $citizen, $applicant, $type)
    {
        $ac_vaucher_mst = new AccountVaucherMst();
        $ac_vaucher_mst->VOUCHER_DT = date('Y-m-d H:i:s');

        if (!empty($citizen)) {
            $ac_vaucher_mst->CITIZEN_ID = $citizen;
        } else {
            $ac_vaucher_mst->TE_APP_ID = $applicant;
        }

        $ac_vaucher_mst->IS_ACTIVE = 1;
        $ac_vaucher_mst->save();

        $vaucher_mst_id = $ac_vaucher_mst->VOUCHER_NO;
        $particular_id = Particular::where('TYPE', 1)->where('IS_LEASE', $type)->pluck('PARTICULAR_ID');
        $particular_amt = TenderParticulars::where('PARTICULAR_ID', $particular_id)
                                            ->where('SCHEDULE_ID', $data['SCHEDULE_ID'])
                                            ->pluck('PARTICULAR_AMT');

        // for tender or schedule money
        $ac_vaucher_chd = new AccountVaucherChd();
        $ac_vaucher_chd->TRX_TRAN_DT = date('Y-m-d H:i:s');
        $ac_vaucher_chd->VOUCHER_NO = $vaucher_mst_id;
        $ac_vaucher_chd->SCHEDULE_ID = $data['SCHEDULE_ID'];
        $ac_vaucher_chd->PARTICULAR_ID = $particular_id;
        $ac_vaucher_chd->PUNIT_PRICE = $particular_amt;
        $ac_vaucher_chd->IS_ACTIVE = 1;
        $ac_vaucher_chd->save();

        $vn_ledger = new AccountVnLedger();
        $vn_ledger->VLEDGER_DT = date('Y-m-d H:i:s');
        $vn_ledger->TRX_CODE_ID = 4;
        $vn_ledger->TRX_TRAN_NO = $ac_vaucher_chd->TRX_TRAN_NO;
        $vn_ledger->VOUCHER_NO = $vaucher_mst_id;
        $vn_ledger->CR_AMT = $particular_amt;
        $vn_ledger->IS_ACTIVE = 1;
        $vn_ledger->save();
        //=====================================

        // For security money
        $ac_vaucher_chd = new AccountVaucherChd();
        $ac_vaucher_chd->TRX_TRAN_DT = date('Y-m-d H:i:s');
        $ac_vaucher_chd->VOUCHER_NO = $vaucher_mst_id;
        $ac_vaucher_chd->SCHEDULE_ID = $data['SCHEDULE_ID'];
        $ac_vaucher_chd->PARTICULAR_ID = $type == 0 ? 3 : 6;
        $ac_vaucher_chd->PUNIT_PRICE = $data['BG_AMOUNT'];
        $ac_vaucher_chd->IS_ACTIVE = 1;
        $ac_vaucher_chd->save();

        $vn_ledger = new AccountVnLedger();
        $vn_ledger->VLEDGER_DT = date('Y-m-d H:i:s');
        $vn_ledger->TRX_CODE_ID = 4;
        $vn_ledger->TRX_TRAN_NO = $ac_vaucher_chd->TRX_TRAN_NO;
        $vn_ledger->VOUCHER_NO = $vaucher_mst_id;
        $vn_ledger->CR_AMT = $data['BG_AMOUNT'];
        $vn_ledger->IS_ACTIVE = 1;
        $vn_ledger->save();

        //======================================

        return $ac_vaucher_mst->VOUCHER_NO;
    }

    /**
     * Create voucher data.
     *
     * @param array  $data
     * @param string $citizen
     * @param string $applicant
     * @param string $type
     *
     * @return string VOUCHER_NO
     */
    private static function createPaidVoucherData($data, $citizen, $applicant, $type)
    {
        $ac_vaucher_mst = new AccountVaucherMst();
        $ac_vaucher_mst->VOUCHER_DT = date('Y-m-d H:i:s');

        if (!empty($citizen)) {
            $ac_vaucher_mst->CITIZEN_ID = $citizen;
        } else {
            $ac_vaucher_mst->TE_APP_ID = $applicant;
        }

        $ac_vaucher_mst->IS_ACTIVE = 1;
        $ac_vaucher_mst->save();

        $vaucher_mst_id = $ac_vaucher_mst->VOUCHER_NO;
        $particular_id = Particular::where('TYPE', 1)->where('IS_LEASE', $type)->pluck('PARTICULAR_ID');
        $particular_amt = TenderParticulars::where('PARTICULAR_ID', $particular_id)
                                            ->where('SCHEDULE_ID', $data['SCHEDULE_ID'])
                                            ->pluck('PARTICULAR_AMT');

        // for tender or schedule money
        $ac_vaucher_chd = new AccountVaucherChd();
        $ac_vaucher_chd->TRX_TRAN_DT = date('Y-m-d H:i:s');
        $ac_vaucher_chd->VOUCHER_NO = $vaucher_mst_id;
        $ac_vaucher_chd->SCHEDULE_ID = $data['SCHEDULE_ID'];
        $ac_vaucher_chd->PARTICULAR_ID = $particular_id;
        $ac_vaucher_chd->PUNIT_PRICE = $particular_amt;
        $ac_vaucher_chd->IS_ACTIVE = 1;
        $ac_vaucher_chd->save();

        $vn_ledger = new AccountVnLedger();
        $vn_ledger->VLEDGER_DT = date('Y-m-d H:i:s');
        $vn_ledger->TRX_CODE_ID = 4;
        $vn_ledger->TRX_TRAN_NO = $ac_vaucher_chd->TRX_TRAN_NO;
        $vn_ledger->VOUCHER_NO = $vaucher_mst_id;
        $vn_ledger->CR_AMT = $particular_amt;
        $vn_ledger->IS_ACTIVE = 1;
        $vn_ledger->save();
        //=====================================

        // For security money
        $ac_vaucher_chd = new AccountVaucherChd();
        $ac_vaucher_chd->TRX_TRAN_DT = date('Y-m-d H:i:s');
        $ac_vaucher_chd->VOUCHER_NO = $vaucher_mst_id;
        $ac_vaucher_chd->SCHEDULE_ID = $data['SCHEDULE_ID'];
        $ac_vaucher_chd->PARTICULAR_ID = $type == 0 ? 3 : 6;
        $ac_vaucher_chd->PUNIT_PRICE = $data['BG_AMOUNT'];
        $ac_vaucher_chd->IS_ACTIVE = 1;
        $ac_vaucher_chd->save();

        $vn_ledger = new AccountVnLedger();
        $vn_ledger->VLEDGER_DT = date('Y-m-d H:i:s');
        $vn_ledger->TRX_CODE_ID = 4;
        $vn_ledger->TRX_TRAN_NO = $ac_vaucher_chd->TRX_TRAN_NO;
        $vn_ledger->VOUCHER_NO = $vaucher_mst_id;
        $vn_ledger->CR_AMT = $data['BG_AMOUNT'];
        $vn_ledger->IS_ACTIVE = 1;
        $vn_ledger->save();

        //======================================

        return $ac_vaucher_mst->VOUCHER_NO;
    }

    /**
     * Tender applicant comparison data collection.
     *
     * @param $input array
     *
     * @return Collection
     */
    public static function getComparisonData($input)
    {
        $tender_ap = ApplicantProperty::with(['getProject', 'getTender', 'getBank', 'getApplicant'=>function ($query) {
            $query->with(['getThana']);
        },
                        ])
                        ->where('IS_SELECTED', 0)
                        ->where('IS_LEASE', 0);

        if (isset($input['group']) && !empty($input['group'])) {
            $tender_ap->where('TENDER_ID', $input['group']);
        } //$tender_ap->where('SCHEDULE_ID', $input['group']);

        $applicant_total = $tender_ap->count();
        $applicant_data = $tender_ap->orderBy('BID_AMOUNT', 'desc')
                                    ->take($input['length'])
                                    ->skip($input['start'])
                                    ->get();

        $applicant_data = collect($applicant_data);

        $ctz_ap = CitizenApplication::with(['getProject', 'getTender', 'getBank', 'getCitizen'=>function ($query) {
            $query->with(['getTenderCitizen', 'getThana']);
        },
                        ])
                        ->where('IS_SELECTED', 0)
                        ->where('IS_LEASE', 0);

        if (isset($input['group']) && !empty($input['group'])) {
            $ctz_ap->where('TENDER_ID', $input['group']);
        } //$ctz_ap->where('SCHEDULE_ID', $input['group']);

        $citizen_total = $ctz_ap->count();
        $citizen_data = $ctz_ap->orderBy('BID_AMOUNT', 'desc')
                                ->take($input['length'])
                                ->skip($input['start'])
                                ->get();

        $citizen_data = collect($citizen_data);

        $total = (int) $applicant_total + (int) $citizen_total;
        $data_list = $applicant_data->merge($citizen_data);

        return ['data' => $data_list, 'total' => $total, 'filtered' => $total];
    }

    /**
     * Tender applicant data collection.
     *
     * @param $input array
     *
     * @return Collection
     */
    public static function getLeaseListData($input) // Applicant data
    {
        $tender_ap = ApplicantProperty::with(['getProject', 'getTender', 'getBank', 'getApplicant'=>function ($query) {
            $query->with(['getThana']);
        },
                        ])
                        ->where('IS_SELECTED', 0)
                        ->where('IS_LEASE', 1);

        if (isset($input['group']) && !empty($input['group'])) {
            $tender_ap->where('TENDER_ID', $input['group']);
        }

        $applicant_total = $tender_ap->count();
        $applicant_data = $tender_ap->orderBy('TE_APP_ID', 'desc')
                                    ->take($input['length'])
                                    ->skip($input['start'])
                                    ->get();

        $applicant_data = collect($applicant_data);

        $ctz_ap = CitizenApplication::with(['getProject', 'getTender', 'getBank', 'getCitizen'=>function ($query) {
            $query->with(['getTenderCitizen', 'getThana']);
        },
                        ])
                        ->where('IS_SELECTED', 0)
                        ->where('IS_LEASE', 1);

        if (isset($input['group']) && !empty($input['group'])) {
            $ctz_ap->where('TENDER_ID', $input['group']);
        }

        $citizen_total = $ctz_ap->count();
        $citizen_data = $ctz_ap->orderBy('CITIZEN_ID', 'desc')
                                ->take($input['length'])
                                ->skip($input['start'])
                                ->get();

        $citizen_data = collect($citizen_data);

        $total = (int) $applicant_total + (int) $citizen_total;
        $data_list = $applicant_data->merge($citizen_data);

        return ['data' => $data_list, 'total' => $total, 'filtered' => $total];
    }

    /**
     * lease applicant comparison data collection.
     *
     * @param $input array
     *
     * @return Collection
     */
    public static function getLeaseComparisonData($input) // Comparison data
    {
        $tender_ap = ApplicantProperty::with(['getProject', 'getTender', 'getBank', 'getApplicant'=>function ($query) {
            $query->with(['getThana']);
        },
                        ])
                        ->where('IS_SELECTED', 0)
                        ->where('IS_LEASE', 1);

        if (isset($input['group']) && !empty($input['group'])) {
            $tender_ap->where('TENDER_ID', $input['group']);
        }

        $applicant_total = $tender_ap->count();
        $applicant_data = $tender_ap->orderBy('BID_AMOUNT', 'desc')
                                    ->take($input['length'])
                                    ->skip($input['start'])
                                    ->get();

        $applicant_data = collect($applicant_data);

        $ctz_ap = CitizenApplication::with(['getProject', 'getTender', 'getBank', 'getCitizen'=>function ($query) {
            $query->with(['getTenderCitizen', 'getThana']);
        },
                        ])
                        ->where('IS_SELECTED', 0)
                        ->where('IS_LEASE', 1);

        if (isset($input['group']) && !empty($input['group'])) {
            $ctz_ap->where('TENDER_ID', $input['group']);
        }

        $citizen_total = $ctz_ap->count();
        $citizen_data = $ctz_ap->orderBy('BID_AMOUNT', 'desc')
                                ->take($input['length'])
                                ->skip($input['start'])
                                ->get();

        $citizen_data = collect($citizen_data);

        $total = (int) $applicant_total + (int) $citizen_total;
        $data_list = $applicant_data->merge($citizen_data);

        return ['data' => $data_list, 'total' => $total, 'filtered' => $total];
    }

    /**
     * Tender applicant data collection.
     *
     * @param $input array
     *
     * @return Collection
     */
    public static function getListData($input)
    {
        $tender_ap = ApplicantProperty::with(['getProject', 'getTender', 'getBank', 'getApplicant'=>function ($query) {
            $query->with(['getThana']);
        },
                        ])
                        ->where('IS_SELECTED', 0)
                        ->where('IS_LEASE', 0);

        if (isset($input['group']) && !empty($input['group'])) {
            $tender_ap->where('TENDER_ID', $input['group']);
        }

        $applicant_total = $tender_ap->count();
        $applicant_data = $tender_ap->orderBy('TE_APP_ID', 'desc')
                                    ->take($input['length'])
                                    ->skip($input['start'])
                                    ->get();

        $applicant_data = collect($applicant_data);

        $ctz_ap = CitizenApplication::with(['getProject', 'getTender', 'getBank', 'getCitizen'=>function ($query) {
            $query->with(['getTenderCitizen', 'getThana']);
        },
                        ])
                        ->where('IS_SELECTED', 0)
                        ->where('IS_LEASE', 0);

        if (isset($input['group']) && !empty($input['group'])) {
            $ctz_ap->where('TENDER_ID', $input['group']);
        }

        $citizen_total = $ctz_ap->count();
        $citizen_data = $ctz_ap->orderBy('CITIZEN_ID', 'desc')
                                ->take($input['length'])
                                ->skip($input['start'])
                                ->get();

        $citizen_data = collect($citizen_data);

        $total = (int) $applicant_total + (int) $citizen_total;
        $data_list = $applicant_data->merge($citizen_data);

        return ['data' => $data_list, 'total' => $total, 'filtered' => $total];
    }

    /**
     * Tender applicant data collection.
     *
     * @param $input array
     *
     * @return Collection
     */
    public static function getTenderListData($input)
    {
        $tender_ap = ApplicantProperty::with(['getProject', 'getTender', 'getBank', 'getApplicant'=>function ($query) {
            $query->with(['getThana']);
        },
                        ])
                        ->where('IS_SELECTED', 1)
                        ->where('IS_ACTIVE', 1);

        if (isset($input['group']) && !empty($input['group'])) {
            $tender_ap->where('TENDER_ID', $input['group']);
        }

        $applicant_total = $tender_ap->count();
        $applicant_data = $tender_ap->orderBy('TE_APP_ID', 'desc')
                                    ->take($input['length'])
                                    ->skip($input['start'])
                                    ->get();

        $applicant_data = collect($applicant_data);

        $ctz_ap = CitizenApplication::with(['getProject', 'getTender', 'getBank', 'getCitizen'=>function ($query) {
            $query->with(['getTenderCitizen', 'getThana']);
        },
                        ])
                        ->where('IS_SELECTED', 1)
                        ->where('IS_ACTIVE', 1);

        if (isset($input['group']) && !empty($input['group'])) {
            $ctz_ap->where('TENDER_ID', $input['group']);
        }

        $citizen_total = $ctz_ap->count();
        $citizen_data = $ctz_ap->orderBy('CITIZEN_ID', 'desc')
                                ->take($input['length'])
                                ->skip($input['start'])
                                ->get();

        $citizen_data = collect($citizen_data);

        $total = (int) $applicant_total + (int) $citizen_total;
        $data_list = $applicant_data->merge($citizen_data);

        return ['data' => $data_list, 'total' => $total, 'filtered' => $total];
    }

    /**
     * Tender applicant data collection.
     *
     * @param $input array
     *
     * @return Collection
     */
    public static function getPrimaryListData($input)
    {
        $tender_ap = ApplicantProperty::with(['getProject', 'getTender', 'getBank', 'getApplicant'=>function ($query) {
            $query->with(['getThana']);
        },
                        ])
                        ->where('IS_SELECTED', 1)
                        ->where('IS_LEASE', 0);

        if (isset($input['group']) && !empty($input['group'])) {
            $tender_ap->where('TENDER_ID', $input['group']);
        }

        $applicant_total = $tender_ap->count();
        $applicant_data = $tender_ap->orderBy('TE_APP_ID', 'desc')
                                    ->take($input['length'])
                                    ->skip($input['start'])
                                    ->get();

        $applicant_data = collect($applicant_data);

        $ctz_ap = CitizenApplication::with(['getProject', 'getTender', 'getBank', 'getCitizen'=>function ($query) {
            $query->with(['getTenderCitizen', 'getThana']);
        },
                        ])
                        ->where('IS_SELECTED', 0)
                        ->where('IS_LEASE', 1);

        if (isset($input['group']) && !empty($input['group'])) {
            $ctz_ap->where('TENDER_ID', $input['group']);
        }

        $citizen_total = $ctz_ap->count();
        $citizen_data = $ctz_ap->orderBy('CITIZEN_ID', 'desc')
                                ->take($input['length'])
                                ->skip($input['start'])
                                ->get();

        $citizen_data = collect($citizen_data);

        $total = (int) $applicant_total + (int) $citizen_total;
        $data_list = $applicant_data->merge($citizen_data);

        return ['data' => $data_list, 'total' => $total, 'filtered' => $total];
    }

    /**
     * Lease applicant data collection.
     *
     * @param $input array
     *
     * @return Collection
     */
    public static function getLeasePrimaryListData($input)
    {
        $tender_ap = ApplicantProperty::with(['getProject', 'getTender', 'getBank', 'getApplicant'=>function ($query) {
            $query->with(['getThana']);
        },
                        ])
                        ->where('IS_SELECTED', 1)
                        ->where('IS_LEASE', 1);

        if (isset($input['group']) && !empty($input['group'])) {
            $tender_ap->where('TENDER_ID', $input['group']);
        }

        $applicant_total = $tender_ap->count();
        $applicant_data = $tender_ap->orderBy('TE_APP_ID', 'desc')
                                    ->take($input['length'])
                                    ->skip($input['start'])
                                    ->get();

        $applicant_data = collect($applicant_data);

        $ctz_ap = CitizenApplication::with(['getProject', 'getTender', 'getBank', 'getCitizen'=>function ($query) {
            $query->with(['getTenderCitizen', 'getThana']);
        },
                        ])
                        ->where('IS_SELECTED', 1)
                        ->where('IS_LEASE', 1);

        if (isset($input['group']) && !empty($input['group'])) {
            $ctz_ap->where('TENDER_ID', $input['group']);
        }

        $citizen_total = $ctz_ap->count();
        $citizen_data = $ctz_ap->orderBy('CITIZEN_ID', 'desc')
                                ->take($input['length'])
                                ->skip($input['start'])
                                ->get();

        $citizen_data = collect($citizen_data);

        $total = (int) $applicant_total + (int) $citizen_total;
        $data_list = $applicant_data->merge($citizen_data);

        return ['data' => $data_list, 'total' => $total, 'filtered' => $total];
    }

    /**
     * Lease Final Selected applicant data collection.
     *
     *@author      Nurullah <nurul@atilimited.net>
     *
     * @param $input array
     *
     * @return Collection
     */
    public static function getLeaseFinalListData($input)
    {
        if (isset($input['group'])) {
            $tender = 'and ts.TENDER_ID = '.$input['group'];
        } else {
            $tender = '';
        }
        $data_list = DB::select(DB::raw("select d.*,ts.TENDER_ID,ts.PROJECT_ID,ts.PR_DETAIL_ID
                                from((select vm.TE_APP_ID, vm.CITIZEN_ID, vc.SCHEDULE_ID, sum(ifnull(al.CR_AMT, 0))CR_AMT, SUM(ifnull(al.DR_AMT, 0))DR_AMT, (sum(ifnull(al.CR_AMT, 0)) - SUM(ifnull(al.DR_AMT, 0)))Due_Amount
                                from ac_vn_ledgers al
                                left join ac_vouchermst vm on al.VOUCHER_NO = vm.VOUCHER_NO 
                                left join ac_voucherchd vc on al.TRX_TRAN_NO = vc.TRX_TRAN_NO
                                where vm.TE_APP_ID In ( select ap.TE_APP_ID from applicant_property ap where vm.TE_APP_ID = ap.TE_APP_ID and ap.IS_SELECTED = 1 and ap.IS_LEASE = 1)
                                group by vc.SCHEDULE_ID, vm.TE_APP_ID)
                                union
                                (select vm.TE_APP_ID, vm.CITIZEN_ID, vc.SCHEDULE_ID, sum(ifnull(al.CR_AMT, 0))CR_AMT, SUM(ifnull(al.DR_AMT, 0))DR_AMT, (sum(ifnull(al.CR_AMT, 0)) - SUM(ifnull(al.DR_AMT, 0)))Due_Amount
                                from ac_vn_ledgers al
                                left join ac_vouchermst vm on al.VOUCHER_NO = vm.VOUCHER_NO 
                                left join ac_voucherchd vc on al.TRX_TRAN_NO = vc.TRX_TRAN_NO
                                where vm.CITIZEN_ID In ( select cp.CITIZEN_ID from sa_citizen_application cp where vm.CITIZEN_ID = cp.CITIZEN_ID and cp.IS_SELECTED = 1  and cp.IS_LEASE = 1)
                                group by vc.SCHEDULE_ID, vm.CITIZEN_ID))d
                                left join tender_schedule ts on d.SCHEDULE_ID = ts.SCHEDULE_ID
                                where d.Due_Amount = 0 $tender
                                "));

        $applicant_total = count($data_list);

        $applicant_data = collect($data_list);

        $total = (int) $applicant_total;
        $data_list = $applicant_data;

        return ['data' => $data_list, 'total' => $total, 'filtered' => $total];
    }

    /**
     * Tender Final Selected applicant data collection.
     *
     *@author      Nurullah <nurul@atilimited.net>
     *
     * @param $input array
     *
     * @return Collection
     */
    public static function getTenderFinalListData($input)
    {
        if (isset($input['group'])) {
            $tender = 'and ts.TENDER_ID = '.$input['group'];
        } else {
            $tender = '';
        }
        $data_list = DB::select(DB::raw("select d.*,ts.TENDER_ID,ts.PROJECT_ID,ts.PR_DETAIL_ID
                                from((select vm.TE_APP_ID, vm.CITIZEN_ID, vc.SCHEDULE_ID, sum(ifnull(al.CR_AMT, 0))CR_AMT, SUM(ifnull(al.DR_AMT, 0))DR_AMT, (sum(ifnull(al.CR_AMT, 0)) - SUM(ifnull(al.DR_AMT, 0)))Due_Amount
                                from ac_vn_ledgers al
                                left join ac_vouchermst vm on al.VOUCHER_NO = vm.VOUCHER_NO 
                                left join ac_voucherchd vc on al.TRX_TRAN_NO = vc.TRX_TRAN_NO
                                where vm.TE_APP_ID In ( select ap.TE_APP_ID from applicant_property ap where vm.TE_APP_ID = ap.TE_APP_ID and ap.IS_SELECTED = 1 and ap.IS_LEASE = 0)
                                group by vc.SCHEDULE_ID, vm.TE_APP_ID)
                                union
                                (select vm.TE_APP_ID, vm.CITIZEN_ID, vc.SCHEDULE_ID, sum(ifnull(al.CR_AMT, 0))CR_AMT, SUM(ifnull(al.DR_AMT, 0))DR_AMT, (sum(ifnull(al.CR_AMT, 0)) - SUM(ifnull(al.DR_AMT, 0)))Due_Amount
                                from ac_vn_ledgers al
                                left join ac_vouchermst vm on al.VOUCHER_NO = vm.VOUCHER_NO 
                                left join ac_voucherchd vc on al.TRX_TRAN_NO = vc.TRX_TRAN_NO
                                where vm.CITIZEN_ID In ( select cp.CITIZEN_ID from sa_citizen_application cp where vm.CITIZEN_ID = cp.CITIZEN_ID and cp.IS_SELECTED = 1  and cp.IS_LEASE = 0)
                                group by vc.SCHEDULE_ID, vm.CITIZEN_ID))d
                                left join tender_schedule ts on d.SCHEDULE_ID = ts.SCHEDULE_ID
                                where d.Due_Amount = 0 and ts.PR_DETAIL_ID is not null $tender
                                "));

        $applicant_total = count($data_list);

        $applicant_data = collect($data_list);

        $total = (int) $applicant_total;
        $data_list = $applicant_data;

        return ['data' => $data_list, 'total' => $total, 'filtered' => $total];
    }

    /**
     * Save final letter data.
     *
     * @param array  $data
     * @param string $ts   Tender schedule id
     * @param string $type Lease applicant type(Applicant/citizen)
     * @param string $id   Applicant id(Applicant/citizen)
     * @param string $user Admin user id
     *
     * @return bool
     */
    public static function leaseFinalSave($data, $ts, $type, $id, $user)
    {
        // Make tender letter
        $letter = self::makeTenderLetter($data, 1, 1, $user);

        // User type select
        if ($type == 'applicant') {
            $lease_applicant = ApplicantProperty::where('TE_APP_ID', $id)->where('SCHEDULE_ID', $ts)->first();

            // Make final select citizen
            $final_select = self::makeFinalSelectCitizen($data, $ts, $type, $id, $user, $lease_applicant);
        } else {
            $lease_applicant = CitizenApplication::where('CITIZEN_ID', $id)->where('SCHEDULE_ID', $ts)->first();
            $tender_schedule = TenderSchedule::where('SCHEDULE_ID', $ts)->first();

            // Make citizen property
            $property = self::makeCitizenProperty($id, $lease_applicant, $tender_schedule, $user);
        }
        $lease_applicant->IS_SELECTED = 2;
        $lease_applicant->UPDATED_BY = $user;
        $lease_applicant->IS_ACTIVE = 0;
        $lease_applicant->save();

        if ($type == 'applicant') {
            // Check if others application exist
            $app_set = ApplicantProperty::where('TE_APP_ID', $id)
                                        ->where('SCHEDULE_ID', $ts)
                                        ->where('IS_ACTIVE', 1)
                                        ->get();
            if (count($app_set) > 0) {
                $ctz_app = self::makeCitizenApplication($final_select, $app_set);
            }
        }

        return true;
    }

    /**
     * Save final letter data.
     *
     * @param array  $data
     * @param string $ts   Tender schedule id
     * @param string $type applicant/citizen
     * @param string $user authonticated user id
     * @param string $id   applicant or citizen id
     *
     * @return void
     */
    public static function tenderFinalSave($data, $ts, $type, $user, $id)
    {
        // Make tender letter
        $letter = self::makeTenderLetter($data, 1, 0, $user);

        // User type select
        if ($type == 'applicant') {
            $tender_applicant = ApplicantProperty::where('TE_APP_ID', $id)->where('SCHEDULE_ID', $ts)->first();

            // Return security money
            $money_return = self::securityMoneyReturn($data, $ts, $type, $user, $id);

            // Make final select citizen
            $final_select = self::makeFinalSelectCitizen($data, $ts, $type, $id, $user, $tender_applicant);
        } else {
            $tender_applicant = CitizenApplication::where('CITIZEN_ID', $id)->where('SCHEDULE_ID', $ts)->first();
            $tender_schedule = TenderSchedule::where('SCHEDULE_ID', $ts)->first();

            // Return security money
            $money_return = self::securityMoneyReturn($data, $ts, $type, $user, $id);

            // Make citizen property
            $property = self::makeCitizenProperty($id, $tender_applicant, $tender_schedule, $user);
        }
        $tender_applicant->IS_SELECTED = 2;
        $tender_applicant->UPDATED_BY = $user;
        $tender_applicant->IS_ACTIVE = 0;
        $tender_applicant->save();

        if ($type == 'applicant') {
            // Check if others application exist
            $app_set = ApplicantProperty::where('TE_APP_ID', $id)
                                        ->where('SCHEDULE_ID', $ts)
                                        ->where('IS_ACTIVE', 1)
                                        ->get();
            if (count($app_set) > 0) {
                $ctz_app = self::makeCitizenApplication($final_select, $app_set);
            }
        }

        // Make first month rent for citizen property
        $rent_voucher = self::makeRentVoucher($id, $ts);
    }

    /**
     * Save security money returnable applicant data.
     *
     * @param array  $data
     * @param string $ts   Tender schedule id
     * @param string $type applicant/citizen
     * @param string $user authonticated user id
     * @param string $id   applicant or citizen id
     *
     * @return void
     */
    private static function securityMoneyReturn($data, $ts, $type, $user, $id)
    {
        if ($type == 'applicant') {
            $applicant_property = ApplicantProperty::where('SCHEDULE_ID', $ts)
                                                    ->whereNotIn('TE_APP_ID', [$id])
                                                    ->get();
            foreach ($applicant_property as $key => $ap) {
                $voucher_chd = AccountVaucherMst::leftJoin('ac_voucherchd', 'ac_voucherchd.VOUCHER_NO', '=', 'ac_vouchermst.VOUCHER_NO')
                                                ->where('ac_vouchermst.TE_APP_ID', $ap->TE_APP_ID)
                                                ->where('ac_voucherchd.PARTICULAR_ID', 3)
                                                ->select('ac_voucherchd.TRX_TRAN_NO', 'ac_voucherchd.PUNIT_PRICE')
                                                ->first();
                if (!empty($voucher_chd)) {
                    $refund = new SecurityRefund();
                    $refund->TE_APP_ID = $id;
                    $refund->TRX_TRAN_NO = $voucher_chd->TRX_TRAN_NO;
                    $refund->TENDER_ID = $ap->TENDER_ID;
                    $refund->SCHEDULE_ID = $ap->SCHEDULE_ID;
                    $refund->PROJECT_ID = $ap->PROJECT_ID;
                    $refund->PR_DETAIL_ID = $ap->PR_DETAIL_ID;
                    $refund->PR_CATEGORY = $ap->PR_CATEGORY;
                    $refund->PR_TYPE = $ap->PR_TYPE;
                    $refund->IS_LEASE = $ap->IS_LEASE;
                    $refund->AMOUNT = $voucher_chd->PUNIT_PRICE;
                    $refund->CREATED_BY = $user;
                    $refund->save();
                }
            }
        } else {
            $applicant_property = CitizenApplication::where('SCHEDULE_ID', $ts)
                                                    ->whereNotIn('CITIZEN_ID', [$id])
                                                    ->get();
            foreach ($applicant_property as $key => $ap) {
                $voucher_chd = AccountVaucherMst::leftJoin('ac_voucherchd', 'ac_voucherchd.VOUCHER_NO', '=', 'ac_vouchermst.VOUCHER_NO')
                                                ->where('ac_vouchermst.CITIZEN_ID', $ap->CITIZEN_ID)
                                                ->where('ac_voucherchd.PARTICULAR_ID', 3)
                                                ->select('ac_voucherchd.TRX_TRAN_NO', 'ac_voucherchd.PUNIT_PRICE')
                                                ->first();
                if (!empty($voucher_chd)) {
                    $refund = new SecurityRefund();
                    $refund->CITIZEN_ID = $id;
                    $refund->TRX_TRAN_NO = $voucher_chd->TRX_TRAN_NO;
                    $refund->TENDER_ID = $ap->TENDER_ID;
                    $refund->SCHEDULE_ID = $ap->SCHEDULE_ID;
                    $refund->PROJECT_ID = $ap->PROJECT_ID;
                    $refund->PR_DETAIL_ID = $ap->PR_DETAIL_ID;
                    $refund->PR_CATEGORY = $ap->PR_CATEGORY;
                    $refund->PR_TYPE = $ap->PR_TYPE;
                    $refund->IS_LEASE = $ap->IS_LEASE;
                    $refund->BID_AMOUNT = $voucher_chd->PUNIT_PRICE;
                    $refund->CREATED_BY = $user;
                    $refund->save();
                }
            }
        }
    }

    /**
     * Make tender letter.
     *
     * @param array  $data
     * @param string $type
     * @param string $is_lease
     * @param string $user
     *
     * @return void
     */
    private static function makeTenderLetter($data, $type, $is_lease, $user)
    {
        $letter = new TenderLetter();
        if ($type == 'applicant') {
            $letter->TE_APP_ID = $data['applicant'];
        } else {
            $letter->CITIZEN_ID = $data['applicant'];
        }
        $letter->SCHEDULE_ID = $data['schedule'];
        $letter->LATTER_NUMBER = $data['sharok_no'];
        $letter->DATE = date('Y-m-d', strtotime(str_replace('/', '-', $data['sharok_date'])));
        $letter->SUBJECT = $data['letter_subject'];
        $letter->BODY = $data['letter_body'];
        $letter->CONDITIONS = $data['letter_conditions'];
        $letter->APPLICANT_INFO = $data['applicant_info'];
        $letter->OFFICER_INFO = $data['officer_info'];
        $letter->DISTRIBUTION = $data['distribution'];
        $letter->TYPE = $type;
        $letter->IS_LEASE = $is_lease;
        $letter->CREATED_BY = $user;
        $letter->save();
    }

    /**
     * Make final select citizen.
     *
     * @param array  $data
     * @param string $ts              Tender schedule id
     * @param string $type            Lease applicant applicant
     * @param string $id              Applicant applicant
     * @param string $user            Admin user id
     * @param object $lease_applicant Applicant application data
     *
     * @return string CITIZEN_ID        Citizen id
     */
    private static function makeFinalSelectCitizen($data, $ts, $type, $id, $user, $lease_applicant)
    {
        $tender_applicant = self::where('TE_APP_ID', $id)->first();
        $tender_schedule = TenderSchedule::where('SCHEDULE_ID', $ts)->first();

        $user_name = explode('@', $tender_applicant->EMAIL);
        $email = $tender_applicant->EMAIL;
        $user_exist = User::where('USERNAME', $user_name[0])->first();
        $email_exist = User::where('EMAIL', $email)->first();

        if (!empty($email_exist)) {
            $user_name[0] = $user_name[0].'_'.Helpers::random_str(5);
        }
        if (!empty($user_exist)) {
            $email = $user_name[0].'_'.Helpers::random_str(5).'@'.$user_name[1];
        }

        $role = Role::where('NAME', 'citizen')->pluck('ROLE_ID');

        $user = new User();
        $user->USER_TYPE = 'C';
        $user->FIRST_NAME = $tender_applicant->FIRST_NAME;
        $user->LAST_NAME = $tender_applicant->LAST_NAME;
        $user->FULL_NAME = $tender_applicant->APPLICANT_NAME;
        $user->GENDER = 'm';
        $user->USERNAME = $user_name[0];
        $user->EMAIL = $email;
        $user->PASSWORD = $tender_applicant->PASSWORD;
        $user->MOBILE_NO = $tender_applicant->APPLICANT_PHONE;
        $user->IS_ACTIVE = 1;
        $user->save();
        $user->attachRole($role);

        $user_id = $user->USER_ID;

        $citizen = new Citizen();
        $citizen->USER_ID = $user_id;
        $citizen->TE_APP_ID = $id;
        $citizen->SPOUSE_NAME = $tender_applicant->SPOUSE_NAME;
        $citizen->NID = $tender_applicant->NID;
        $citizen->DIVISION_ID = $tender_applicant->DIVISION_ID;
        $citizen->DISTRICT_ID = $tender_applicant->DISTRICT_ID;
        $citizen->THANA_ID = $tender_applicant->THANA_ID;
        $citizen->ROAD_NO = $tender_applicant->ROAD_NO;
        $citizen->HOLDING_NO = $tender_applicant->HOLDING_NO;
        $citizen->CREATED_BY = $user;
        $citizen->IS_ACTIVE = 1;
        $citizen->save();

        // Make citizen property
        $property = self::makeCitizenProperty($citizen->CITIZEN_ID, $lease_applicant, $tender_schedule, $user);

        return $citizen->CITIZEN_ID;
    }

    /**
     * Make citizen property.
     *
     * @param string $citizen_id      Citizen id
     * @param object $lease_applicant Tender applicant data
     * @param string $tender_schedule Tender schedule data
     * @param string $user            Admin user id
     *
     * @return stsring CIT_PRO_ID        citizen proprty id
     */
    private static function makeCitizenProperty($citizen_id, $lease_applicant, $tender_schedule, $user)
    {
        $citizen_property = new CitizenProperty();
        $citizen_property->CITIZEN_ID = $citizen_id;
        $citizen_property->TENDER_ID = $lease_applicant->TENDER_ID;
        $citizen_property->SCHEDULE_ID = $tender_schedule->SCHEDULE_ID;
        $citizen_property->PROJECT_ID = $tender_schedule->PROJECT_ID;
        $citizen_property->PR_DETAIL_ID = $lease_applicant->PR_DETAIL_ID;
        $citizen_property->PR_CATEGORY = $lease_applicant->PR_CATEGORY;
        $citizen_property->PR_TYPE = $lease_applicant->PR_TYPE;
        $citizen_property->IS_LEASE = $lease_applicant->IS_LEASE;
        $citizen_property->CREATED_BY = $user;
        $citizen_property->IS_ACTIVE = 1;
        $citizen_property->save();

        return $citizen_property->CIT_PRO_ID;
    }

    /**
     * Make citizen application.
     *
     * @param string      $citizen_id Citizen id
     * @param collections $app_set    Tender application property data
     *
     * @return void
     */
    private static function makeCitizenApplication($citizen_id, $app_set)
    {
        foreach ($app_set as $key => $app) {
            $citizen_application = new CitizenApplication();
            $citizen_application->CITIZEN_ID = $citizen_id;
            $citizen_application->TENDER_ID = $app->TENDER_ID;
            $citizen_application->SCHEDULE_ID = $app->SCHEDULE_ID;
            $citizen_application->PROJECT_ID = $app->PROJECT_ID;
            $citizen_application->PR_DETAIL_ID = $app->PR_DETAIL_ID;
            $citizen_application->PR_CATEGORY = $app->PR_CATEGORY;
            $citizen_application->PR_TYPE = $app->PR_TYPE;
            $citizen_application->IS_TE_SC_BOUGHT = $app->IS_TE_SC_BOUGHT;
            $citizen_application->TE_SC_AMOUNT = $app->TE_SC_AMOUNT;
            $citizen_application->TE_SC_DATE = $app->TE_SC_DATE;
            $citizen_application->BID_AMOUNT = $app->BID_AMOUNT;
            $citizen_application->BID_AMOUNT_TEXT = $app->BID_AMOUNT_TEXT;
            $citizen_application->BG_AMOUNT = $app->BG_AMOUNT;
            $citizen_application->BG_AMOUNT_TEXT = $app->BG_AMOUNT_TEXT;
            $citizen_application->BG_PAYMENT_TYPE = $app->BG_PAYMENT_TYPE;
            $citizen_application->BG_PAYMENT_STATUS = $app->BG_PAYMENT_STATUS;
            $citizen_application->BANK_ID = $app->BANK_ID;
            $citizen_application->B_DRAFT_NO = $app->B_DRAFT_NO;
            $citizen_application->B_DRAFT_DATE = $app->B_DRAFT_DATE;
            $citizen_application->B_DRAFT_ATTACHMENT = $app->B_DRAFT_ATTACHMENT;
            $citizen_application->REMARKS = $app->REMARKS;
            $citizen_application->INITIAL_REMARKS = $app->INITIAL_REMARKS;
            $citizen_application->CANCEL_REMARKS = $app->CANCEL_REMARKS;
            $citizen_application->IS_SELECTED = $app->IS_SELECTED;
            $citizen_application->IS_LEASE = $app->IS_LEASE;
            $citizen_application->IS_CANCEL = $app->IS_CANCEL;
            $citizen_application->IS_ACTIVE = $app->IS_ACTIVE;
            $citizen_application->CREATED_BY = $app->CREATED_BY;
            $citizen_application->UPDATED_BY = $app->UPDATED_BY;
            $citizen_application->save();
        }
    }

    /**
     * Make citizen rent voucher for first month.
     *
     * @param string $id Citizen id
     * @param string $ts Tender schedule id
     *
     * @return string TRX_TRAN_NO voucher child id
     */
    private static function makeRentVoucher($id, $ts)
    {
        $citizen = CitizenProperty::where('CITIZEN_ID')
                                    ->where('SCHEDULE_ID', $ts)
                                    ->where('IS_LEASE', 0)
                                    ->where('IS_ACTIVE', 1)
                                    ->first();

        $ac_vaucher_mst = new AccountVaucherMst();
        $ac_vaucher_mst->VOUCHER_DT = date('Y-m-d H:i:s');
        $ac_vaucher_mst->CITIZEN_ID = $id;
        $ac_vaucher_mst->IS_ACTIVE = 1;
        $ac_vaucher_mst->save();

        $vaucher_mst_id = $ac_vaucher_mst->VOUCHER_NO;
        $particular_id = Particular::where('TYPE', 2)->pluck('PARTICULAR_ID');
        $particular_amt = CategoryParticular::where('PARTICULAR_ID', $particular_id)->pluck('PARTICULAR_AMT');

        $ac_vaucher_chd = new AccountVaucherChd();
        $ac_vaucher_chd->TRX_TRAN_DT = date('Y-m-d H:i:s');
        $ac_vaucher_chd->VOUCHER_NO = $vaucher_mst_id;
        $ac_vaucher_chd->SCHEDULE_ID = $ts;
        $ac_vaucher_chd->PARTICULAR_ID = $particular_id;
        $ac_vaucher_chd->PUNIT_PRICE = $particular_amt;
        $ac_vaucher_chd->IS_ACTIVE = 1;
        $ac_vaucher_chd->save();

        $vn_ledger = new AccountVnLedger();
        $vn_ledger->VLEDGER_DT = date('Y-m-d H:i:s');
        $vn_ledger->TRX_CODE_ID = 4;
        $vn_ledger->TRX_TRAN_NO = $ac_vaucher_chd->TRX_TRAN_NO;
        $vn_ledger->VOUCHER_NO = $vaucher_mst_id;
        $vn_ledger->CR_AMT = $particular_amt;
        $vn_ledger->IS_ACTIVE = 1;
        $vn_ledger->save();

        return $ac_vaucher_chd->TRX_TRAN_NO;
    }

    /**
     * Tender applicant data collection.
     *
     * @param $input array
     *
     * @return Collection
     */
    public static function getallListData($input)//nurullah
    {
        $tender_ap = ApplicantProperty::with(['getProject', 'getTender', 'getBank', 'getApplicant'=>function ($query) {
            $query->with(['getThana']);
        },
                        ])
                        ->where('IS_SELECTED', 0);

        if (isset($input['group']) && !empty($input['group'])) {
            $tender_ap->where('TENDER_ID', $input['group']);
        }

        $applicant_total = $tender_ap->count();
        $applicant_data = $tender_ap->orderBy('TE_APP_ID', 'desc')
                                    ->take($input['length'])
                                    ->skip($input['start'])
                                    ->get();

        $applicant_data = collect($applicant_data);

        $ctz_ap = CitizenApplication::with(['getProject', 'getTender', 'getBank', 'getCitizen'=>function ($query) {
            $query->with(['getTenderCitizen', 'getThana']);
        },
                        ])
                        ->where('IS_SELECTED', 0);

        if (isset($input['group']) && !empty($input['group'])) {
            $ctz_ap->where('TENDER_ID', $input['group']);
        }

        $citizen_total = $ctz_ap->count();
        $citizen_data = $ctz_ap->orderBy('CITIZEN_ID', 'desc')
                                ->take($input['length'])
                                ->skip($input['start'])
                                ->get();

        $citizen_data = collect($citizen_data);

        $total = (int) $applicant_total + (int) $citizen_total;
        $data_list = $applicant_data->merge($citizen_data);

        return ['data' => $data_list, 'total' => $total, 'filtered' => $total];
    }

    /**
     * Get applicant application.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function getApplication()
    {
        return $this->hasMany('App\Models\ApplicantProperty', 'TE_APP_ID', 'TE_APP_ID');
    }

    /**
     * Get parent thana.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function getThana()
    {
        return $this->belongsTo('App\Models\Thana', 'THANA_ID', 'THANA_ID');
    }

    /**
     * Get parent project category.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function getProjectCategory()
    {
        return $this->belongsTo('App\Models\ProjectCategory', 'PR_CATEGORY', 'PR_CATEGORY');
    }

    /**
     * Get parent project type.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function getProjectType()
    {
        return $this->belongsTo('App\Models\ProjectType', 'PR_TYPE', 'PR_TYPE');
    }

    /**
     * Get parent bank.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function getBank()
    {
        return $this->belongsTo('App\Models\Bank', 'BANK_ID', 'BANK_ID');
    }
}
