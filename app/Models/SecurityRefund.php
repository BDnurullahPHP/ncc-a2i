<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SecurityRefund extends Model
{
    protected $table = 'security_refund';
    protected $primaryKey = 'SF_ID';
    const CREATED_AT = 'CREATED_AT';
    const UPDATED_AT = 'UPDATED_AT';

    protected $fillable = [
                            'TE_APP_ID',
                            'CITIZEN_ID',
                            'TRX_TRAN_NO',
                            'TENDER_ID',
                            'PROJECT_ID',
                            'SCHEDULE_ID',
                            'PR_DETAIL_ID',
                            'PR_CATEGORY',
                            'PR_TYPE',
                            'IS_LEASE',
                            'AMOUNT',
                            'REMARKS',
                            'IS_PAID',
                            'IS_ACTIVE',
                            'CREATED_BY',
                            'UPDATED_BY',
                        ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = ['SF_ID'];

    /**
     * Security refund data.
     *
     * @param array $input
     *
     * @return collection
     */
    public static function getSecurityRefundData($input)
    {
        $data = self::with(['getCitizen' => function ($query) {
            $query->with('getTenderCitizen');
        }, 'getApplicant', 'getProject', 'getCategory', 'getTender'])
                                ->where('IS_PAID', 0)
                                ->where('IS_ACTIVE', 1)
                                ->orderBy('SF_ID', 'desc')
                                ->take($input['length'])
                                ->skip($input['start'])
                                ->get();

        $total = self::where('IS_ACTIVE', 1)->where('IS_PAID', 0)->count();

        return ['data' => $data, 'total' => $total, 'filtered' => $total];
    }

    /**
     * Applicant security refund.
     *
     * @param array  $data
     * @param string $uid
     * @param string $utype
     * @param string $sc
     * @param string $user
     *
     * @return array
     */
    public static function refundUpdate($data, $uid, $utype, $sc, $user)
    {
        if ($utype == 'citizen') {
            self::where('CITIZEN_ID', $uid)->where('SCHEDULE_ID', $sc)->update(['IS_PAID' => 1]);

            $voucher = self::refundVoucher($data, $sc, $uid, null, $user);
            $letter = self::refundLetter($data, $sc, $uid, null, $user);
            $citizen_user = Citizen::with('getTenderCitizen')->where('CITIZEN_ID', $uid)->first();
            $mobile_no = $citizen_user->getTenderCitizen->MOBILE_NO;
        } else {
            self::where('TE_APP_ID', $uid)->where('SCHEDULE_ID', $sc)->update(['IS_PAID' => 1]);

            $voucher = self::refundVoucher($data, $sc, null, $uid, $user);
            $letter = self::refundLetter($data, $sc, null, $uid, $user);
            $mobile_no = TenderApplicant::where('TE_APP_ID', $uid)->pluck('APPLICANT_PHONE');
        }

        $notification = Notification::securityRefundSMS($data, $uid, $utype, $sc);

        return ['number' => $mobile_no, 'sms' => $notification];
    }

    /**
     * Applicant security refund voucher information.
     *
     * @param array       $data
     * @param string      $sc
     * @param string|null $citizen_id
     * @param string|null $applicant_id
     * @param string      $user
     *
     * @return void
     */
    private static function refundVoucher($data, $sc, $citizen_id, $applicant_id, $user)
    {
        $ac_vaucher_mst = new AccountVaucherMst();
        $ac_vaucher_mst->VOUCHER_DT = date('Y-m-d H:i:s');
        $ac_vaucher_mst->CITIZEN_ID = $citizen_id;
        $ac_vaucher_mst->TE_APP_ID = $applicant_id;
        $ac_vaucher_mst->IS_ACTIVE = 1;
        $ac_vaucher_mst->save();

        $vaucher_mst_id = $ac_vaucher_mst->VOUCHER_NO;

        $ac_vaucher_chd = new AccountVaucherChd();
        $ac_vaucher_chd->TRX_TRAN_DT = date('Y-m-d H:i:s');
        $ac_vaucher_chd->VOUCHER_NO = $vaucher_mst_id;
        $ac_vaucher_chd->SCHEDULE_ID = $sc;
        $ac_vaucher_chd->PARTICULAR_ID = 15;
        $ac_vaucher_chd->PUNIT_PRICE = $data['security_money'];
        $ac_vaucher_chd->IS_ACTIVE = 1;
        $ac_vaucher_chd->save();

        $vn_ledger = new AccountVnLedger();
        $vn_ledger->VLEDGER_DT = date('Y-m-d H:i:s');
        $vn_ledger->TRX_CODE_ID = 6;
        $vn_ledger->TRX_TRAN_NO = $ac_vaucher_chd->TRX_TRAN_NO;
        $vn_ledger->VOUCHER_NO = $vaucher_mst_id;
        $vn_ledger->CR_AMT = $data['security_money'];
        $vn_ledger->IS_ACTIVE = 1;
        $vn_ledger->save();
    }

    /**
     * Applicant security refund letter.
     *
     * @param array  $data
     * @param string $uid
     * @param string $utype
     * @param string $sc
     * @param string $type
     * @param string $user
     *
     * @return void
     */
    private static function refundLetter($data, $sc, $citizen_id, $applicant_id, $user)
    {
        $letter = new TenderLetter();
        $letter->CITIZEN_ID = $citizen_id;
        $letter->TE_APP_ID = $applicant_id;
        $letter->SCHEDULE_ID = $sc;
        $letter->PARTICULAR_ID = 15;
        $letter->LATTER_NUMBER = $data['sharok_no'];
        $letter->DATE = date('Y-m-d', strtotime(str_replace('/', '-', $data['sharok_date'])));
        $letter->SUBJECT = $data['letter_subject'];
        $letter->BODY = $data['letter_body'];
        $letter->OFFICER_INFO = $data['officer_info'];
        $letter->CREATED_BY = $user;
        $letter->save();
    }

    /**
     * Get parent applicant.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function getCitizen()
    {
        return $this->belongsTo('App\Models\Citizen', 'CITIZEN_ID', 'CITIZEN_ID');
    }

    /**
     * Get parent applicant.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function getApplicant()
    {
        return $this->belongsTo('App\Models\TenderApplicant', 'TE_APP_ID', 'TE_APP_ID');
    }

    /**
     * Get Tender No.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function getTender()
    {
        return $this->belongsTo('App\Models\Tender', 'TENDER_ID', 'TENDER_ID');
    }

    /**
     * Get tender schedule.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function getSchedule()
    {
        return $this->belongsTo('App\Models\TenderSchedule', 'SCHEDULE_ID', 'SCHEDULE_ID');
    }

    /**
     * Get Project.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function getProject()
    {
        return $this->belongsTo('App\Models\Project', 'PROJECT_ID', 'PROJECT_ID');
    }

    /**
     * Get category.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function getCategory()
    {
        return $this->belongsTo('App\Models\ProjectCategory', 'PR_CATEGORY', 'PR_CATEGORY');
    }

    /**
     * Get Project Type.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function getProjectType()
    {
        return $this->belongsTo('App\Models\ProjectType', 'PR_TYPE', 'PR_TYPE');
    }

    /**
     * Get Project Details.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function getProjectDetail()
    {
        return $this->belongsTo('App\Models\ProjectDetails', 'PR_DETAIL_ID', 'PR_DETAIL_ID');
    }

    /**
     * Get Tender Particulars.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function getTenderParticulars()
    {
        return $this->hasMany('App\Models\TenderParticulars', 'SCHEDULE_ID', 'SCHEDULE_ID');
    }

    /**
     * Get category Particulars.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function getCategoryParticulars()
    {
        return $this->belongsTo('App\Models\CategoryParticular', 'PR_CATEGORY', 'PR_CATEGORY');
    }
}
