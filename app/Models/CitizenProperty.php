<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CitizenProperty extends Model
{
    protected $table = 'sa_citizen_property';
    protected $primaryKey = 'CIT_PRO_ID';
    const CREATED_AT = 'CREATED_AT';
    const UPDATED_AT = 'UPDATED_AT';
    protected $fillable = [
                                'CITIZEN_ID',
                                'TENDER_ID',
                                'SCHEDULE_ID',
                                'PROJECT_ID',
                                'PR_DETAIL_ID',
                                'PR_CATEGORY',
                                'PR_TYPE',
                                'IS_LEASE',
                                'DEFAULTER',
                                'IS_ACTIVE',
                                'CREATED_BY',
                                'UPDATED_BY',
                            ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = ['CIT_PRO_ID'];

    /**
     * Get citizen property.
     *
     * @param string $citizen_id
     *
     * @return collections
     */
    public static function getPropertyData($citizen_id)
    {
        return self::where('CITIZEN_ID', $citizen_id)
                                ->with(['getSchedule' => function ($query) {
                                    $query->with(['getProject', 'getCategory', 'getProjectType', 'getProjectDetail']);
                                }])
                                ->where('IS_ACTIVE', 1)
                                ->where('IS_LEASE', 0)
                                ->get();
    }

    /**
     * Get citizen property.
     *
     * @param string $citizen_id
     *
     * @return bool
     */
    public static function isTenderProperty($citizen_id)
    {
        $property = self::where('CITIZEN_ID', $citizen_id)
                                    ->where('IS_ACTIVE', 1)
                                    ->where('IS_LEASE', 0)
                                    ->count();

        return $property > 0 ? true : false;
    }

    /**
     * Get citizen services.
     *
     * @param string $citizen_id
     *
     * @return string
     */
    public static function citizenServices($citizen_id)
    {
        return self::where('CITIZEN_ID', $citizen_id)
                                    ->where('IS_ACTIVE', 1)
                                    ->groupBy('PR_CATEGORY')
                                    ->count();
    }

    /**
     * Get category based citizen property.
     *
     * @param string $citizen_id
     *
     * @return string
     */
    public static function categoryProperty($citizen_id)
    {
        $data = [];
        $category = self::with('projectCategory')
                                    ->where('CITIZEN_ID', $citizen_id)
                                    ->where('IS_ACTIVE', 1)
                                    ->where('IS_LEASE', 0)
                                    ->groupBy('PR_CATEGORY')
                                    ->get();

        foreach ($category as $key => $cat) {
            $obj = new \stdClass();
            $obj->PR_CATEGORY = $cat->PR_CATEGORY;
            $obj->CATEGORY_NAME = $cat->projectCategory->CATE_NAME;
            $obj->ITEMS = self::with(['tenderRentParticular' => function ($query) {
                $query->where('PARTICULAR_ID', 5);
            },
                                            'getProject', 'getProjectType', 'projectDetails',
                                        ])
                                        ->where('PR_CATEGORY', $cat->PR_CATEGORY)
                                        ->where('CITIZEN_ID', $citizen_id)
                                        ->where('IS_ACTIVE', 1)
                                        ->take(3)
                                        ->get();

            $data[] = $obj;
        }

        return $data;
    }

    /**
     * Get citizen service data.
     *
     * @param string $citizen_id
     * @param array  $input
     *
     * @return array
     */
    public static function citizenServiceData($citizen_id, $input)
    {
        if (isset($input['length']) && isset($input['start'])) {
            $query = self::with(['leaseValidity' => function ($query) use ($citizen_id) {
                $query->where('CITIZEN_ID', $citizen_id);
            },
                                            'tenderRentParticular' => function ($query) {
                                                $query->where('PARTICULAR_ID', 5);
                                            },
                                            'getProject',
                                            'getProjectType',
                                            'projectDetails',
                                        ])
                                        ->where('CITIZEN_ID', $citizen_id)
                                        ->where('PR_CATEGORY', $input['group'])
                                        ->where('IS_ACTIVE', 1);
            $total = $query->count();
            $data = $query->take($input['length'])
                            ->skip($input['start'])
                            ->get();
        } else {
            $data = [];
            $total = 0;
        }

        return ['data' => $data, 'total' => $total, 'filtered' => $total];
    }

    /**
     * Get citizen.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function getCitizen()
    {
        return $this->belongsTo('App\Models\Citizen', 'CITIZEN_ID', 'CITIZEN_ID');
    }

    /**
     * Get tender schedule.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function getSchedule()
    {
        return $this->belongsTo('App\Models\TenderSchedule', 'SCHEDULE_ID', 'SCHEDULE_ID');
    }

    /**
     * Get project.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function getProject()
    {
        return $this->belongsTo('App\Models\Project', 'PROJECT_ID', 'PROJECT_ID');
    }

    /**
     * Get Project Type.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function getProjectType()
    {
        return $this->belongsTo('App\Models\ProjectType', 'PR_TYPE', 'PR_TYPE');
    }

    /**
     * Get project category.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function projectCategory()
    {
        return $this->belongsTo('App\Models\ProjectCategory', 'PR_CATEGORY', 'PR_CATEGORY');
    }

    /**
     * Get project details.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function projectDetails()
    {
        return $this->belongsTo('App\Models\ProjectDetails', 'PR_DETAIL_ID', 'PR_DETAIL_ID');
    }

    /**
     * Get Tender Rent Particular.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function tenderRentParticular()
    {
        return $this->hasOne('App\Models\TenderParticulars', 'SCHEDULE_ID', 'SCHEDULE_ID');
    }

    /**
     * Get Lease validity.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function leaseValidity()
    {
        return $this->hasOne('App\Models\LeaseValidity', 'SCHEDULE_ID', 'SCHEDULE_ID');
    }
}
