<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProjectLocation extends Model
{
    protected $table = 'sa_location';
    protected $primaryKey = 'LOCATION_ID';
    const CREATED_AT = 'CREATED_AT';
    const UPDATED_AT = 'UPDATED_AT';

    protected $fillable = ['LOCATION_NAME', 'LOCATION_DESC', 'IS_ACTIVE', 'CREATED_BY', 'UPDATED_BY'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = ['LOCATION_ID'];

    /**
     * Project Location data collection.
     *
     * @param $input array
     *
     * @return Collection
     */
    public static function getDataProjectLocation($input)
    {
        $data = self::where('LOCATION_ID', '!=', 0)->select(['LOCATION_ID', 'LOCATION_NAME', 'LOCATION_DESC', 'IS_ACTIVE', 'CREATED_AT'])
                               ->orderBy('LOCATION_ID', 'desc')
                               ->take($input['length'])
                               ->skip($input['start'])
                               ->get();

        $total = self::count();

        return ['data' => $data, 'total' => $total, 'filtered' => $total];
    }

    /**
     * Get tender location.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function getTenderLocation()
    {
        return $this->hasOne('App\Models\TenderLocation', 'LOCATION_ID', 'LOCATION_ID');
    }
}
