<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Zone extends Model
{
    protected $table = 'sa_ward';
    protected $primaryKey = 'WARD_ID';
    const CREATED_AT = 'CREATED_AT';
    const UPDATED_AT = 'UPDATED_AT';

    protected $fillable = ['PARENT_ID', 'WARD_NAME', 'WARD_NAME_BN', 'IS_ACTIVE', 'CREATED_BY', 'UPDATED_BY'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = ['WARD_ID'];

    /**
     * Zone data collection.
     *
     * @param $input array
     *
     * @return Collection
     */
    public static function getzoneData($input)
    {
        $data = self::where('PARENT_ID', 0)
                            ->select(['WARD_ID', 'WARD_NAME', 'WARD_NAME_BN', 'IS_ACTIVE', 'CREATED_AT'])
                    ->orderBy('WARD_ID', 'desc')
                            ->take($input['length'])
                            ->skip($input['start'])
                            ->get();

        $total = self::where('PARENT_ID', 0)->count();

        return ['data' => $data, 'total' => $total, 'filtered' => $total];
    }

    /**
     * Subcategory data collection.
     *
     * @param $input array
     *
     * @return Collection
     */
    public static function getWardData($input)
    {
        $data = self::where('PARENT_ID', '!=', 0)
                    ->select(['WARD_ID', 'PARENT_ID', 'WARD_NAME', 'WARD_NAME_BN', 'IS_ACTIVE', 'CREATED_AT'])
                    ->orderBy('WARD_ID', 'desc')
                    ->take($input['length'])
                    ->skip($input['start'])
                    ->get();

        $total = self::where('PARENT_ID', '!=', 0)->count();

        return ['data' => $data, 'total' => $total, 'filtered' => $total];
    }

    /**
     * Get parent category.
     */
    public function getZone()
    {
        return $this->hasOne('App\Models\Zone', 'PARENT_ID', 'WARD_ID');
    }
}
