<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TenderLocation extends Model
{
    protected $table = 'tender_location';
    protected $primaryKey = 'TE_LOC_ID';
    const CREATED_AT = 'CREATED_AT';
    const UPDATED_AT = 'UPDATED_AT';

    protected $fillable = [
                            'TENDER_ID',
                            'LOCATION_ID',
                            'TYPE',
                            'IS_ACTIVE',
                            'CREATED_BY',
                            'UPDATED_BY',
                        ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = ['TE_LOC_ID'];

    /**
     * Get parent location.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function getLocation()
    {
        return $this->belongsTo('App\Models\ProjectLocation', 'LOCATION_ID', 'LOCATION_ID');
    }
}
