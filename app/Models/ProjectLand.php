<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProjectLand extends Model
{
    protected $table = 'project_land';
    protected $primaryKey = 'PROJECT_LN_ID';
    const CREATED_AT = 'CREATED_AT';
    const UPDATED_AT = 'UPDATED_AT';

    protected $fillable = [
                            'PROJECT_ID',
                            'LAND_ID',
                            'IS_ACTIVE',
                            'CREATED_BY',
                            'UPDATED_BY',
                        ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = ['PROJECT_LN_ID'];

    /**
     * Get parent land.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function getLand()
    {
        return $this->belongsTo('App\Models\Land', 'LAND_ID', 'LAND_ID');
    }
}
