<?php

namespace App\Models;

use App\User;
use DB;
use Illuminate\Database\Eloquent\Model;

class AccountVaucherChd extends Model
{
    protected $table = 'ac_voucherchd';
    protected $primaryKey = 'TRX_TRAN_NO';
    const CREATED_AT = 'CREATED_AT';
    const UPDATED_AT = 'UPDATED_AT';
    protected $fillable = [
                                'TRX_TRAN_DT',
                                'VOUCHER_NO',
                                'PROJECT_ID',
                                'PR_DETAIL_ID',
                                'PR_CATEGORY',
                                'PARTICULAR_ID',
                                'PUNIT_PRICE',
                                'SEND_MAIL',
                                'SEND_SMS',
                                'IS_VIEW',
                                'TCANCEL_FG',
                                'REMARKS',
                                'IS_ACTIVE',
                                'CREATED_AT',
                                'CREATED_BY',
                                'UPDATED_AT',
                                'UPDATED_BY',
                            ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = ['TRX_TRAN_NO'];

    /**
     * Get tender voucher child particulars by voucher id and schedule id.
     *
     * @param array  $app_voucher
     * @param string $schedule_id
     *
     * @return collection
     */
    public static function tenderVoucherChd($app_voucher, $schedule_id)
    {
        return self::leftJoin('ac_vn_ledgers', 'ac_vn_ledgers.TRX_TRAN_NO', '=', 'ac_voucherchd.TRX_TRAN_NO')
                                    ->with('getParticular')
                                    ->select('ac_voucherchd.*', 'ac_vn_ledgers.*')
                                    ->whereIn('ac_voucherchd.VOUCHER_NO', $app_voucher)
                                    ->where('ac_voucherchd.SCHEDULE_ID', $schedule_id)
                                    ->where('ac_vn_ledgers.TRX_CODE_ID', 4)
                                    ->whereNotIn('ac_voucherchd.PARTICULAR_ID', [8])
                                    ->groupBy('ac_voucherchd.PARTICULAR_ID')
                                    ->get();
    }

    /**
     * Get tender voucher child particulars by voucher id and schedule id.
     *
     * @param array  $app_voucher
     * @param string $schedule_id
     *
     * @return collection
     */
    public static function leaseVoucherChd($app_voucher, $schedule_id)
    {
        return self::leftJoin('ac_vn_ledgers', 'ac_vn_ledgers.TRX_TRAN_NO', '=', 'ac_voucherchd.TRX_TRAN_NO')
                                    ->with('getParticular')
                                    ->select('ac_voucherchd.*', 'ac_vn_ledgers.*')
                                    ->whereIn('ac_voucherchd.VOUCHER_NO', $app_voucher)
                                    ->where('ac_voucherchd.SCHEDULE_ID', $schedule_id)
                                    ->where('ac_vn_ledgers.TRX_CODE_ID', 4)
                                    ->groupBy('ac_voucherchd.PARTICULAR_ID')
                                    ->get();
    }

    /**
     * Get tender voucher child particulars by voucher id, schedule id and particular id.
     *
     * @param array  $app_voucher
     * @param string $schedule_id
     * @param array  $data
     *
     * @return collection
     */
    public static function tenderVChdParticular($app_voucher, $schedule_id, $data)
    {
        $query = self::leftJoin('ac_vn_ledgers', 'ac_vn_ledgers.TRX_TRAN_NO', '=', 'ac_voucherchd.TRX_TRAN_NO')
                                    ->with('getParticular')
                                    ->select('ac_voucherchd.*', 'ac_vn_ledgers.*')
                                    ->whereIn('ac_voucherchd.VOUCHER_NO', $app_voucher)
                                    ->where('ac_voucherchd.SCHEDULE_ID', $schedule_id)
                                    ->whereNotIn('ac_voucherchd.PARTICULAR_ID', [8])
                                    ->where('ac_vn_ledgers.TRX_CODE_ID', 4);

        if (isset($data['particular']) && !empty($data['particular'])) {
            $query->where('ac_voucherchd.PARTICULAR_ID', $data['particular']);
        }

        return $query->groupBy('ac_voucherchd.PARTICULAR_ID')
                    ->get();
    }

    /**
     * Get tender voucher child particulars by voucher id, schedule id and particular id.
     *
     * @param array  $app_voucher
     * @param string $schedule_id
     * @param array  $data
     *
     * @return collection
     */
    // public static function leaseVChdParticular($app_voucher, $schedule_id, $data)
    // {
    //     $query = AccountVaucherChd::leftJoin('ac_vn_ledgers', 'ac_vn_ledgers.TRX_TRAN_NO', '=', 'ac_voucherchd.TRX_TRAN_NO')
    //                                 ->with('getParticular')
    //                                 ->select('ac_voucherchd.*', 'ac_vn_ledgers.*')
    //                                 ->whereIn('ac_voucherchd.VOUCHER_NO', $app_voucher)
    //                                 ->where('ac_voucherchd.SCHEDULE_ID', $schedule_id)
    //                                 ->where('ac_vn_ledgers.TRX_CODE_ID', 4);

    //     if ( isset($data['particular']) && !empty($data['particular']) ) {
    //         $query->where('ac_voucherchd.PARTICULAR_ID', $data['particular']);
    //     }

    //     return $query->groupBy('ac_voucherchd.PARTICULAR_ID')
    //                 ->get();
    // }

    /**
     * Get lease revenue data.
     *
     * @author Nurullah <nurul@atilimited.net>
     *
     * @param $input
     *
     * @return collection
     */
    public static function leaseRevenueData($input)
    {
        $getData = json_decode($input['search']);
        $datas = self::with(['voucherSchedule'=> function ($query) {
            $query->whereIn('PARTICULAR_ID', [6, 7, 9, 10, 11])
                                                    ->select('*', DB::raw('SUM(PUNIT_PRICE) as TOTAL_VALUE'))
                                                    ->groupBy(['SCHEDULE_ID', 'PARTICULAR_ID'])
                                                    ->orderBy('PARTICULAR_ID', 'asc');
        }, 'getSchedule'=>function ($query) {
            $query->with('getProject');
        }, 'getCitizen'=>function ($query) {
            $query->with('getTenderCitizen');
        }])
                                        ->leftJoin('ac_vouchermst', 'ac_vouchermst.VOUCHER_NO', '=', 'ac_voucherchd.VOUCHER_NO')
                                        ->join('ac_vn_ledgers', 'ac_vn_ledgers.TRX_TRAN_NO', '=', 'ac_voucherchd.TRX_TRAN_NO')
                                        ->whereIn('ac_voucherchd.PARTICULAR_ID', [6, 7, 9, 10, 11])
                                        ->select('*', DB::raw('SUM(DR_AMT) as TOTAL_PAID, SUM(CR_AMT) as TOTAL_PENDING'));

        if (!empty($getData->year)) {
            $datas->whereYear('ac_voucherchd.TRX_TRAN_DT', '=', $getData->year);
        }
        if (!empty($getData->month)) {
            $datas->whereMonth('ac_voucherchd.TRX_TRAN_DT', '=', $getData->month);
        }
        if (!empty($getData->SCHEDULE_ID)) {
            $datas->where('ac_voucherchd.SCHEDULE_ID', $getData->SCHEDULE_ID);
        }
        if (!empty($getData->citizen_name)) {
            $user = explode('/', $getData->citizen_name);
            if ($user[0] == 'applicant') {
                $datas->where('ac_vouchermst.TE_APP_ID', $user[1]);
            }
            if ($user[0] == 'citizen') {
                $datas->where('ac_vouchermst.CITIZEN_ID', $user[1]);
            }
        }

        $report_data = $datas->groupBy('ac_voucherchd.SCHEDULE_ID')->get();
        $total = count($report_data);

        return ['data' => $report_data, 'total' => $total, 'filtered' => $total];
    }

    /**
     * Lease revenue data collection.
     *
     * @param $input
     *
     * @return collection
     */
    public static function leaseRevenuePrintData($input)
    {
        $datas = self::with(['voucherSchedule'=> function ($query) {
            $query->whereIn('PARTICULAR_ID', [6, 7, 9, 10, 11])
                                                   ->select('*', DB::raw('SUM(PUNIT_PRICE) as TOTAL_VALUE'))
                                                   ->groupBy(['SCHEDULE_ID', 'PARTICULAR_ID'])
                                                   ->orderBy('PARTICULAR_ID', 'asc');
        }, 'getSchedule'=>function ($query) {
            $query->with('getProject');
        }, 'getCitizen'=>function ($query) {
            $query->with('getTenderCitizen');
        }])
                                       ->leftJoin('ac_vouchermst', 'ac_vouchermst.VOUCHER_NO', '=', 'ac_voucherchd.VOUCHER_NO')
                                       ->join('ac_vn_ledgers', 'ac_vn_ledgers.TRX_TRAN_NO', '=', 'ac_voucherchd.TRX_TRAN_NO')
                                       ->whereIn('ac_voucherchd.PARTICULAR_ID', [6, 7, 9, 10, 11])
                                       ->select('*', DB::raw('SUM(DR_AMT) as TOTAL_PAID, SUM(CR_AMT) as TOTAL_PENDING'));

        if (!empty($input->year)) {
            $datas->whereYear('ac_voucherchd.TRX_TRAN_DT', '=', $input->year);
        }
        if (!empty($input->month)) {
            $datas->whereMonth('ac_voucherchd.TRX_TRAN_DT', '=', $input->month);
        }
        if (!empty($input->SCHEDULE_ID)) {
            $datas->where('ac_voucherchd.SCHEDULE_ID', $input->SCHEDULE_ID);
        }
        if (!empty($input->citizen_name)) {
            $user = explode('/', $input->citizen_name);
            if ($user[0] == 'applicant') {
                $datas->where('ac_vouchermst.TE_APP_ID', $user[1]);
            }
            if ($user[0] == 'citizen') {
                $datas->where('ac_vouchermst.CITIZEN_ID', $user[1]);
            }
        }

        return $datas->groupBy('ac_voucherchd.SCHEDULE_ID')->get();
    }

    /**
     * Get admin yearly revenue data.
     *
     * @param string $year
     *
     * @return array
     */
    public static function yearlyRevenue($year)
    {
        // Month as collum
        // $revenue = DB::select(
        //                 DB::raw("SELECT
        //                             SUM(IF(month = 'Jan', total, 0)) AS 'January',
        //                             SUM(IF(month = 'Feb', total, 0)) AS 'Feburary',
        //                             SUM(IF(month = 'Mar', total, 0)) AS 'March',
        //                             SUM(IF(month = 'Apr', total, 0)) AS 'April',
        //                             SUM(IF(month = 'May', total, 0)) AS 'May',
        //                             SUM(IF(month = 'Jun', total, 0)) AS 'June',
        //                             SUM(IF(month = 'Jul', total, 0)) AS 'July',
        //                             SUM(IF(month = 'Aug', total, 0)) AS 'August',
        //                             SUM(IF(month = 'Sep', total, 0)) AS 'September',
        //                             SUM(IF(month = 'Oct', total, 0)) AS 'October',
        //                             SUM(IF(month = 'Nov', total, 0)) AS 'November',
        //                             SUM(IF(month = 'Dec', total, 0)) AS 'December',
        //                             SUM(total) AS total_yearly
        //                             FROM (
        //                         SELECT DATE_FORMAT(TRX_TRAN_DT, "%b") AS month, SUM(PUNIT_PRICE) as total
        //                         FROM ac_voucherchd
        //                         #WHERE TRX_TRAN_DT <= NOW() and TRX_TRAN_DT >= Date_add(Now(),interval - 12 month)
        //                         GROUP BY DATE_FORMAT(TRX_TRAN_DT, "%m-%Y")) as sub")
        //                     );

        // Month as row
        // $revenue = DB::select(
        //                 DB::raw("SELECT
        //                         t1.month,
        //                         t1.md,
        //                         coalesce(SUM(t1.amount+t2.amount), 0) AS total
        //                         from
        //                         (
        //                           select DATE_FORMAT(a.tdate,'%b') as month,
        //                           DATE_FORMAT(a.tdate, '%m-%Y') as md,
        //                           '0' as  amount
        //                           from (
        //                             select curdate() - INTERVAL (a.a + (10 * b.a) + (100 * c.a)) DAY as tdate
        //                             from (select 0 as a union all select 1 union all select 2 union all select 3 union all select 4 union all select 5 union all select 6 union all select 7 union all select 8 union all select 9) as a
        //                             cross join (select 0 as a union all select 1 union all select 2 union all select 3 union all select 4 union all select 5 union all select 6 union all select 7 union all select 8 union all select 9) as b
        //                             cross join (select 0 as a union all select 1 union all select 2 union all select 3 union all select 4 union all select 5 union all select 6 union all select 7 union all select 8 union all select 9) as c
        //                           ) a
        //                           where a.tdate <= NOW() and a.tdate >= Date_add(Now(),interval - 12 month)
        //                           group by md
        //                         )t1
        //                         left join
        //                         (
        //                           SELECT DATE_FORMAT(TRX_TRAN_DT, '%b') AS month, SUM(PUNIT_PRICE) as amount ,DATE_FORMAT(TRX_TRAN_DT, '%m-%Y') as md
        //                           FROM ac_voucherchd
        //                           where TRX_TRAN_DT <= NOW() and TRX_TRAN_DT >= Date_add(Now(),interval - 12 month)
        //                           GROUP BY md
        //                         )t2
        //                         on t2.md = t1.md
        //                         group by t1.month
        //                         order by t1.md")
        //                     );

        $data = [];
        $revenue = self::leftJoin('ac_vn_ledgers', 'ac_vn_ledgers.TRX_TRAN_NO', '=', 'ac_voucherchd.TRX_TRAN_NO')
                                    ->select('ac_voucherchd.*', 'ac_vn_ledgers.*', DB::raw('COALESCE(SUM(ac_vn_ledgers.DR_AMT), 0) as TOTAL_PAID, SUM(ac_vn_ledgers.CR_AMT) as TOTAL_PENDING'))
                                    ->whereYear('ac_voucherchd.TRX_TRAN_DT', '=', $year)
                                    ->groupBy(DB::raw("DATE_FORMAT(ac_voucherchd.TRX_TRAN_DT,'%Y-%m')"))
                                    ->orderBy(DB::raw("DATE_FORMAT(ac_voucherchd.TRX_TRAN_DT,'%Y-%m')"), 'desc')
                                    ->get();

        foreach ($revenue as $key => $rev) {
            $data[] = [
                        'y' => date('F Y', strtotime($rev->TRX_TRAN_DT)),
                        'a' => $rev->TOTAL_PENDING,
                        'b' => $rev->TOTAL_PAID,
                    ];
        }
        if (count($data) == 0) {
            $data[] = [
                        'y' => '',
                        'a' => number_format(0),
                        'b' => number_format(0),
                    ];
        }

        return $data;
    }

    /**
     * Get citizen yearly rent data.
     *
     * @param string $year
     * @param string $$citizen
     *
     * @return array
     */
    public static function citizenRentData($year, $citizen)
    {
        $data = [];
        $rent = self::leftJoin('ac_vn_ledgers', 'ac_vn_ledgers.TRX_TRAN_NO', '=', 'ac_voucherchd.TRX_TRAN_NO')
                                    ->leftJoin('ac_vouchermst', 'ac_vouchermst.VOUCHER_NO', '=', 'ac_voucherchd.VOUCHER_NO')
                                    ->select('ac_voucherchd.*', 'ac_vn_ledgers.*', DB::raw('COALESCE(SUM(ac_vn_ledgers.DR_AMT), 0) as TOTAL_PAID, SUM(ac_vn_ledgers.CR_AMT) as TOTAL_PENDING'))
                                    ->where('ac_vouchermst.CITIZEN_ID', $citizen)
                                    ->where('ac_voucherchd.PARTICULAR_ID', 5)
                                    ->whereYear('ac_voucherchd.TRX_TRAN_DT', '=', $year)
                                    ->groupBy(DB::raw("DATE_FORMAT(ac_voucherchd.TRX_TRAN_DT,'%Y-%m')"))
                                    ->orderBy(DB::raw("DATE_FORMAT(ac_voucherchd.TRX_TRAN_DT,'%Y-%m')"), 'desc')
                                    ->get();

        foreach ($rent as $key => $rev) {
            $data[] = [
                        'y' => date('F Y', strtotime($rev->TRX_TRAN_DT)),
                        'a' => $rev->TOTAL_PENDING,
                        'b' => $rev->TOTAL_PAID,
                    ];
        }
        if (count($data) == 0) {
            $data[] = [
                        'y' => '',
                        'a' => number_format(0),
                        'b' => number_format(0),
                    ];
        }

        return $data;
    }

    /**
     * Get rent payment and due amount data.
     *
     * @param string $$citizen
     *
     * @return object
     */
    public static function citizenRentPaymentData($citizen)
    {
        return self::leftJoin('ac_vn_ledgers', 'ac_vn_ledgers.TRX_TRAN_NO', '=', 'ac_voucherchd.TRX_TRAN_NO')
                                    ->leftJoin('ac_vouchermst', 'ac_vouchermst.VOUCHER_NO', '=', 'ac_voucherchd.VOUCHER_NO')
                                    ->select(DB::raw('COALESCE(SUM(ac_vn_ledgers.DR_AMT), 0) as TOTAL_PAID, SUM(ac_vn_ledgers.CR_AMT) as TOTAL_PENDING'))
                                    ->where('ac_vouchermst.CITIZEN_ID', $citizen)
                                    ->where('ac_voucherchd.PARTICULAR_ID', 5)
                                    ->first();
    }

    /**
     * Get accounts monthly revenue data.
     *
     * @param string $month
     *
     * @return array
     */
    public static function monthlyRevenue($month)
    {
        $data = [];
        $date = explode('-', $month);
        $revenue = self::leftJoin('ac_vn_ledgers', 'ac_vn_ledgers.TRX_TRAN_NO', '=', 'ac_voucherchd.TRX_TRAN_NO')
                                    ->select('ac_voucherchd.*', 'ac_vn_ledgers.*', DB::raw('COALESCE(SUM(ac_vn_ledgers.DR_AMT), 0) as TOTAL_PAID, SUM(ac_vn_ledgers.CR_AMT) as TOTAL_PENDING'))
                                    ->whereYear('ac_voucherchd.TRX_TRAN_DT', '=', $date[0])
                                    ->whereMonth('ac_voucherchd.TRX_TRAN_DT', '=', $date[1])
                                    ->groupBy('ac_voucherchd.TRX_TRAN_DT')
                                    ->orderBy('ac_voucherchd.TRX_TRAN_DT', 'desc')
                                    ->get();

        foreach ($revenue as $key => $rev) {
            $data[] = [
                        'y' => date('d', strtotime($rev->TRX_TRAN_DT)),
                        'a' => $rev->TOTAL_PENDING,
                        'b' => $rev->TOTAL_PAID,
                    ];
        }
        if (count($data) == 0) {
            $data[] = [
                        'y' => '',
                        'a' => number_format(0),
                        'b' => number_format(0),
                    ];
        }

        return $data;
    }

    /**
     * Get revenue search data for data table.
     *
     * @param object $input
     *
     * @return array
     */
    public static function revenueSearchDataTable($input)
    {
        $search = json_decode($input['search']);

        $revenue = self::with(['getCitizen'=> function ($query) {
            $query->with(['getTenderCitizen']);
        }, 'getApplicant', 'getLeaseDemesne', 'getParticular', 'getCategory',
                                    ])
                                    ->leftJoin('ac_vn_ledgers', 'ac_vn_ledgers.TRX_TRAN_NO', '=', 'ac_voucherchd.TRX_TRAN_NO')
                                    ->leftJoin('ac_vouchermst', 'ac_vouchermst.VOUCHER_NO', '=', 'ac_voucherchd.VOUCHER_NO')
                                    ->leftJoin('tender_schedule', 'tender_schedule.SCHEDULE_ID', '=', 'ac_voucherchd.SCHEDULE_ID')
                                    ->select('*',
                                                DB::raw('COALESCE(SUM(ac_vn_ledgers.DR_AMT), 0) as TOTAL_PAID, 
                                                        COALESCE(SUM(ac_vn_ledgers.CR_AMT), 0) as TOTAL_PENDING')
                                            );

        if (!empty($search->year)) {
            $revenue->whereYear('ac_voucherchd.TRX_TRAN_DT', '=', $search->year);
        }
        if (!empty($search->month)) {
            $revenue->whereMonth('ac_voucherchd.TRX_TRAN_DT', '=', $search->month);
        }
        if (!empty($search->CATEGORY)) {
            $revenue->where('tender_schedule.PR_CATEGORY', $search->CATEGORY);
        }
        if (!empty($search->PARTICULAR_ID)) {
            $revenue->where('ac_voucherchd.PARTICULAR_ID', $search->PARTICULAR_ID);
        }
        if (!empty($search->APPLICANT_ID)) {
            $app = explode('_', $search->APPLICANT_ID);
            if ($app[0] == 'C') {
                $revenue->where('ac_vouchermst.CITIZEN_ID', $app[1]);
            } elseif ($app[0] == 'T') {
                $revenue->where('ac_vouchermst.TE_APP_ID', $app[1]);
            } elseif ($app[0] == 'L') {
                $revenue->where('ac_vouchermst.LEASE_DE_ID', $app[1]);
            }
        }

        $revenue->groupBy(['ac_vn_ledgers.TRX_TRAN_NO']);

        $total = count($revenue->get());
        $data = $revenue->take($input['length'])
                            ->skip($input['start'])
                            ->get();

        return ['data' => $data, 'total' => $total, 'filtered' => $total];
    }

    /**
     * Get revenue search data.
     *
     * @param array $input
     *
     * @return collection
     */
    public static function revenueSearchData($input)
    {
        $revenue = self::with(['getCitizen'=> function ($query) {
            $query->with(['getTenderCitizen']);
        }, 'getApplicant', 'getLeaseDemesne', 'getParticular', 'getCategory',
                                    ])
                                    ->leftJoin('ac_vn_ledgers', 'ac_vn_ledgers.TRX_TRAN_NO', '=', 'ac_voucherchd.TRX_TRAN_NO')
                                    ->leftJoin('ac_vouchermst', 'ac_vouchermst.VOUCHER_NO', '=', 'ac_voucherchd.VOUCHER_NO')
                                    ->leftJoin('tender_schedule', 'tender_schedule.SCHEDULE_ID', '=', 'ac_voucherchd.SCHEDULE_ID')
                                    ->select('*',
                                                DB::raw('COALESCE(SUM(ac_vn_ledgers.DR_AMT), 0) as TOTAL_PAID, 
                                                        COALESCE(SUM(ac_vn_ledgers.CR_AMT), 0) as TOTAL_PENDING')
                                            );

        if (!empty($input['year'])) {
            $revenue->whereYear('ac_voucherchd.TRX_TRAN_DT', '=', $input['year']);
        }
        if (!empty($input['month'])) {
            $revenue->whereMonth('ac_voucherchd.TRX_TRAN_DT', '=', $input['month']);
        }
        if (!empty($input['CATEGORY'])) {
            $revenue->where('tender_schedule.PR_CATEGORY', $input['CATEGORY']);
        }
        if (!empty($input['PARTICULAR_ID'])) {
            $revenue->where('ac_voucherchd.PARTICULAR_ID', $input['PARTICULAR_ID']);
        }
        if (!empty($input['APPLICANT_ID'])) {
            $app = explode('_', $input['APPLICANT_ID']);
            if ($app[0] == 'C') {
                $revenue->where('ac_vouchermst.CITIZEN_ID', $app[1]);
            } elseif ($app[0] == 'T') {
                $revenue->where('ac_vouchermst.TE_APP_ID', $app[1]);
            } elseif ($app[0] == 'L') {
                $revenue->where('ac_vouchermst.LEASE_DE_ID', $app[1]);
            }
        }

        return $revenue->groupBy(['ac_vn_ledgers.TRX_TRAN_NO'])->get();
    }

    /**
     * Get citizen ledger search data for data table.
     *
     * @param object $input
     * @param bool   $print
     *
     * @return array
     */
    public static function citizenLedgerDataTable($input)
    {
        $search = json_decode($input['search']);

        $revenue = self::with(['getCitizen'=> function ($query) {
            $query->with(['getTenderCitizen']);
        }, 'getApplicant', 'getLeaseDemesne', 'getParticular', 'getCategory',
                                    ])
                                    ->leftJoin('ac_vn_ledgers', 'ac_vn_ledgers.TRX_TRAN_NO', '=', 'ac_voucherchd.TRX_TRAN_NO')
                                    ->leftJoin('ac_vouchermst', 'ac_vouchermst.VOUCHER_NO', '=', 'ac_voucherchd.VOUCHER_NO')
                                    ->leftJoin('tender_schedule', 'tender_schedule.SCHEDULE_ID', '=', 'ac_voucherchd.SCHEDULE_ID')
                                    ->select('*',
                                                DB::raw('COALESCE(SUM(ac_vn_ledgers.DR_AMT), 0) as TOTAL_PAID, 
                                                        COALESCE(SUM(ac_vn_ledgers.CR_AMT), 0) as TOTAL_PENDING')
                                            );

        if (!empty($search->START_DATE) && !empty($search->END_DATE)) {
            $from_date = date('Y-m-d', strtotime(str_replace('/', '-', $search->START_DATE))).' 00:00:00';
            $to_date = date('Y-m-d', strtotime(str_replace('/', '-', $search->END_DATE))).' 23:59:59';
            $revenue->whereBetween('ac_voucherchd.TRX_TRAN_DT', [$from_date, $to_date]);
        }
        if (!empty($search->CATEGORY)) {
            $revenue->where('tender_schedule.PR_CATEGORY', $search->CATEGORY);
        }
        if (!empty($search->PARTICULAR_ID)) {
            $revenue->where('ac_voucherchd.PARTICULAR_ID', $search->PARTICULAR_ID);
        }
        if (!empty($search->SCHEDULE_ID)) {
            $sc = explode('_', $search->SCHEDULE_ID);
            if ($sc[0] == 'C') {
                $revenue->where('ac_voucherchd.SCHEDULE_ID', $sc[2]);
            }
        }
        if (!empty($search->APPLICANT_USER)) {
            $app = explode('_', $search->APPLICANT_USER);
            if ($app[0] == 'C') {
                $revenue->where('ac_vouchermst.CITIZEN_ID', $app[1]);
            } elseif ($app[0] == 'T') {
                $revenue->where('ac_vouchermst.TE_APP_ID', $app[1]);
            } elseif ($app[0] == 'L') {
                $revenue->where('ac_vouchermst.LEASE_DE_ID', $app[1]);
            }
        }

        $revenue->groupBy(['ac_vn_ledgers.TRX_TRAN_NO']);

        $total = count($revenue->get());
        $data = $revenue->take($input['length'])
                            ->skip($input['start'])
                            ->get();

        return ['data' => $data, 'total' => $total, 'filtered' => $total];
    }

    /**
     * Get citizen ledger search data for data print.
     *
     * @param object $input
     * @param bool   $print
     *
     * @return collection
     */
    public static function citizenLedgerDataPrint($input)
    {
        $revenue = self::with(['getCitizen'=> function ($query) {
            $query->with(['getTenderCitizen']);
        }, 'getApplicant', 'getLeaseDemesne', 'getParticular', 'getCategory',
                                    ])
                                    ->leftJoin('ac_vn_ledgers', 'ac_vn_ledgers.TRX_TRAN_NO', '=', 'ac_voucherchd.TRX_TRAN_NO')
                                    ->leftJoin('ac_vouchermst', 'ac_vouchermst.VOUCHER_NO', '=', 'ac_voucherchd.VOUCHER_NO')
                                    ->leftJoin('tender_schedule', 'tender_schedule.SCHEDULE_ID', '=', 'ac_voucherchd.SCHEDULE_ID')
                                    ->select('*',
                                                DB::raw('COALESCE(SUM(ac_vn_ledgers.DR_AMT), 0) as TOTAL_PAID, 
                                                        COALESCE(SUM(ac_vn_ledgers.CR_AMT), 0) as TOTAL_PENDING')
                                            );

        if (!empty($input['START_DATE']) && !empty($input['END_DATE'])) {
            $from_date = date('Y-m-d', strtotime(str_replace('/', '-', $input['START_DATE']))).' 00:00:00';
            $to_date = date('Y-m-d', strtotime(str_replace('/', '-', $input['END_DATE']))).' 23:59:59';
            $revenue->whereBetween('ac_voucherchd.TRX_TRAN_DT', [$from_date, $to_date]);
        }
        if (!empty($input['CATEGORY'])) {
            $revenue->where('tender_schedule.PR_CATEGORY', $input['CATEGORY']);
        }
        if (!empty($input['PARTICULAR_ID'])) {
            $revenue->where('ac_voucherchd.PARTICULAR_ID', $input['PARTICULAR_ID']);
        }
        if (!empty($input['SCHEDULE_ID'])) {
            $sc = explode('_', $input['SCHEDULE_ID']);
            if ($sc[0] == 'C') {
                $revenue->where('ac_voucherchd.SCHEDULE_ID', $sc[2]);
            }
        }
        if (!empty($input['APPLICANT_USER'])) {
            $app = explode('_', $input['APPLICANT_USER']);
            if ($app[0] == 'C') {
                $revenue->where('ac_vouchermst.CITIZEN_ID', $app[1]);
            } elseif ($app[0] == 'T') {
                $revenue->where('ac_vouchermst.TE_APP_ID', $app[1]);
            } elseif ($app[0] == 'L') {
                $revenue->where('ac_vouchermst.LEASE_DE_ID', $app[1]);
            }
        }

        return $revenue->groupBy(['ac_vn_ledgers.TRX_TRAN_NO'])->get();
    }

    /**
     * Ownership change.
     *
     * @param array  $data
     * @param string $uid
     * @param string $utype
     * @param string $sc
     * @param string $type
     * @param string $user
     *
     * @return void
     */
    public static function userOwnershipChange($data, $uid, $utype, $sc, $type, $user)
    {
        if ($utype == 'citizen') {
            //$citizen_application = CitizenApplication::where('CITIZEN_ID', $uid)->where('SCHEDULE_ID', $sc)->where('IS_ACTIVE', 1)->first();
            $log_data = self::logUserData($data, $uid, null, $user);
            $update_user = self::userUpdate($data, $uid, null, $user);
            $voucher = self::userVoucher($data, $sc, $uid, null, $user);
        } else {
            $log_data = self::logUserData($data, null, $uid, $user);
            $update_user = self::userUpdate($data, null, $uid, $user);
            $voucher = self::userVoucher($data, $sc, null, $uid, $user);
        }
    }

    /**
     * Ownership cancel.
     *
     * @param array  $data
     * @param string $uid
     * @param string $utype
     * @param string $sc
     * @param string $type
     * @param string $user
     *
     * @return void
     */
    public static function userOwnershipCancel($data, $uid, $utype, $sc, $type, $user)
    {
        if ($utype == 'citizen') {
            $citizen_application = CitizenApplication::where('CITIZEN_ID', $uid)->where('SCHEDULE_ID', $sc)->where('IS_ACTIVE', 1)->first();
            $citizen_property = CitizenProperty::where('CITIZEN_ID', $uid)->where('SCHEDULE_ID', $sc)->where('IS_ACTIVE', 1)->first();
            if (!empty($citizen_application)) {
                $ca = CitizenApplication::find($citizen_application->CITIZEN_APP_ID);
                $ca->IS_ACTIVE = 0;
                $ca->UPDATED_BY = $user;
                $ca->save();
            } elseif (!empty($citizen_property)) {
                $cp = CitizenProperty::find($citizen_property->CIT_PRO_ID);
                $cp->IS_ACTIVE = 0;
                $cp->UPDATED_BY = $user;
                $cp->save();
            }
            $voucher = self::userCanclPropertyVoucher($data, $sc, $uid, null, $user);
        } else {
            $applicant_property = ApplicantProperty::where('TE_APP_ID', $uid)->where('SCHEDULE_ID', $sc)->where('IS_ACTIVE', 1)->first();
            if (!empty($applicant_property)) {
                $ca = ApplicantProperty::find($applicant_property->APP_PRO_ID);
                $ca->IS_ACTIVE = 0;
                $ca->UPDATED_BY = $user;
                $ca->save();
            }
            $voucher = self::userCanclPropertyVoucher($data, $sc, null, $uid, $user);
        }
    }

    /**
     * User log data.
     *
     * @param array       $data
     * @param string|null $citizen_id
     * @param string|null $applicant_id
     * @param string      $user
     *
     * @return void
     */
    private static function logUserData($data, $citizen_id, $applicant_id, $user)
    {
        $user = new UserLog();

        if (!empty($citizen_id)) {
            $user_data = Citizen::with('getTenderCitizen')->where('CITIZEN_ID', $citizen_id)->first();

            $user->CITIZEN_ID = $citizen_id;
            $user->FULL_NAME = $user_data->getTenderCitizen->FULL_NAME;
            $user->FULL_NAME_BN = $user_data->getTenderCitizen->FULL_NAME_BN;
            $user->GENDER = $user_data->getTenderCitizen->GENDER;
            $user->SPOUSE_NAME = $user_data->SPOUSE_NAME;
            $user->MOBILE = $user_data->getTenderCitizen->MOBILE_NO;
            $user->NID = $user_data->NID;
            $user->EMAIL = $user_data->getTenderCitizen->EMAIL;
            $user->DIVISION_ID = $user_data->DIVISION_ID;
            $user->DISTRICT_ID = $user_data->DISTRICT_ID;
            $user->THANA_ID = $user_data->THANA_ID;
            $user->PO_ID = $user_data->PO_ID;
            $user->ROAD_NO = $user_data->ROAD_NO;
            $user->HOLDING_NO = $user_data->HOLDING_NO;
            $user->IS_ACTIVE = 1;
            $user->CREATED_BY = $user;
            $user->save();
        } else {
            $user_data = TenderApplicant::where('TE_APP_ID', $applicant_id)->first();

            $user->TE_APP_ID = $applicant_id;
            $user->FULL_NAME = $user_data->APPLICANT_NAME;
            $user->SPOUSE_NAME = $user_data->SPOUSE_NAME;
            $user->MOBILE = $user_data->APPLICANT_PHONE;
            $user->NID = $user_data->NID;
            $user->EMAIL = $user_data->EMAIL;
            $user->DIVISION_ID = $user_data->DIVISION_ID;
            $user->DISTRICT_ID = $user_data->DISTRICT_ID;
            $user->THANA_ID = $user_data->THANA_ID;
            $user->PO_ID = $user_data->PO_ID;
            $user->ROAD_NO = $user_data->ROAD_NO;
            $user->HOLDING_NO = $user_data->HOLDING_NO;
            $user->IS_ACTIVE = 1;
            $user->CREATED_BY = $user;
            $user->save();
        }
    }

    /**
     * Update user information.
     *
     * @param array       $data
     * @param string|null $citizen_id
     * @param string|null $applicant_id
     * @param string      $user
     *
     * @return void
     */
    private static function userUpdate($data, $citizen_id, $applicant_id, $user)
    {
        if (!empty($citizen_id)) {
            $citizen = Citizen::find($citizen_id);
            $citizen->SPOUSE_NAME = $data['SPOUSE_NAME'];
            $citizen->NID = $data['NID'];
            $citizen->DIVISION_ID = $data['division'];
            $citizen->DISTRICT_ID = $data['district'];
            $citizen->THANA_ID = $data['thana'];
            $citizen->ROAD_NO = $data['ROAD_NO'];
            $citizen->HOLDING_NO = $data['HOLDING_NO'];
            $citizen->UPDATED_BY = $user;
            $citizen->save();

            $user = User::find($citizen->USER_ID);
            $user->FIRST_NAME = $data['FIRST_NAME'];
            $user->LAST_NAME = $data['LAST_NAME'];
            $user->FULL_NAME = $data['FIRST_NAME'].' '.$data['LAST_NAME'];
            $user->EMAIL = $data['APPLICANT_EMAIL'];
            $user->MOBILE_NO = $data['APPLICANT_PHONE'];
            $user->PASSWORD = bcrypt($data['TE_APP_PASSWORD']);
            $user->UPDATED_BY = $user;
            $user->save();
        } else {
            $applicant = TenderApplicant::find($applicant_id);
            $applicant->FIRST_NAME = $data['FIRST_NAME'];
            $applicant->LAST_NAME = $data['LAST_NAME'];
            $applicant->APPLICANT_PHONE = $data['APPLICANT_PHONE'];
            $applicant->NID = $data['NID'];
            $applicant->PASSWORD = $data['TE_APP_PASSWORD'];
            $applicant->SPOUSE_NAME = $data['SPOUSE_NAME'];
            $applicant->EMAIL = $data['APPLICANT_EMAIL'];
            $applicant->DIVISION_ID = $data['division'];
            $applicant->DISTRICT_ID = $data['district'];
            $applicant->THANA_ID = $data['thana'];
            $applicant->ROAD_NO = $data['ROAD_NO'];
            $applicant->HOLDING_NO = $data['HOLDING_NO'];
            $applicant->UPDATED_BY = $user;
            $applicant->save();
        }
    }

    /**
     * User ownership change voucher information.
     *
     * @param array       $data
     * @param string      $sc
     * @param string|null $citizen_id
     * @param string|null $applicant_id
     * @param string      $user
     *
     * @return void
     */
    private static function userVoucher($data, $sc, $citizen_id, $applicant_id, $user)
    {
        $ac_vaucher_mst = new AccountVaucherMst();
        $ac_vaucher_mst->VOUCHER_DT = date('Y-m-d H:i:s');
        $ac_vaucher_mst->CITIZEN_ID = $citizen_id;
        $ac_vaucher_mst->TE_APP_ID = $applicant_id;
        $ac_vaucher_mst->IS_ACTIVE = 1;
        $ac_vaucher_mst->save();

        $vaucher_mst_id = $ac_vaucher_mst->VOUCHER_NO;

        $ac_vaucher_chd = new self();
        $ac_vaucher_chd->TRX_TRAN_DT = date('Y-m-d H:i:s');
        $ac_vaucher_chd->VOUCHER_NO = $vaucher_mst_id;
        $ac_vaucher_chd->SCHEDULE_ID = $sc;
        $ac_vaucher_chd->PARTICULAR_ID = 13;
        $ac_vaucher_chd->PUNIT_PRICE = $data['returnable_amount'];
        $ac_vaucher_chd->IS_ACTIVE = 1;
        $ac_vaucher_chd->save();

        $vn_ledger = new AccountVnLedger();
        $vn_ledger->VLEDGER_DT = date('Y-m-d H:i:s');
        $vn_ledger->TRX_CODE_ID = 6;
        $vn_ledger->TRX_TRAN_NO = $ac_vaucher_chd->TRX_TRAN_NO;
        $vn_ledger->VOUCHER_NO = $vaucher_mst_id;
        $vn_ledger->CR_AMT = $data['returnable_amount'];
        $vn_ledger->IS_ACTIVE = 1;
        $vn_ledger->save();
    }

    /**
     * User property cancel voucher information.
     *
     * @param array       $data
     * @param string      $sc
     * @param string|null $citizen_id
     * @param string|null $applicant_id
     * @param string      $user
     *
     * @return void
     */
    private static function userCanclPropertyVoucher($data, $sc, $citizen_id, $applicant_id, $user)
    {
        $ac_vaucher_mst = new AccountVaucherMst();
        $ac_vaucher_mst->VOUCHER_DT = date('Y-m-d H:i:s');
        $ac_vaucher_mst->CITIZEN_ID = $citizen_id;
        $ac_vaucher_mst->TE_APP_ID = $applicant_id;
        $ac_vaucher_mst->IS_ACTIVE = 1;
        $ac_vaucher_mst->save();

        $vaucher_mst_id = $ac_vaucher_mst->VOUCHER_NO;

        $ac_vaucher_chd = new self();
        $ac_vaucher_chd->TRX_TRAN_DT = date('Y-m-d H:i:s');
        $ac_vaucher_chd->VOUCHER_NO = $vaucher_mst_id;
        $ac_vaucher_chd->SCHEDULE_ID = $sc;
        $ac_vaucher_chd->PARTICULAR_ID = 14;
        $ac_vaucher_chd->PUNIT_PRICE = $data['returnable_amount'];
        $ac_vaucher_chd->IS_ACTIVE = 1;
        $ac_vaucher_chd->save();

        $vn_ledger = new AccountVnLedger();
        $vn_ledger->VLEDGER_DT = date('Y-m-d H:i:s');
        $vn_ledger->TRX_CODE_ID = 6;
        $vn_ledger->TRX_TRAN_NO = $ac_vaucher_chd->TRX_TRAN_NO;
        $vn_ledger->VOUCHER_NO = $vaucher_mst_id;
        $vn_ledger->CR_AMT = $data['returnable_amount'];
        $vn_ledger->IS_ACTIVE = 1;
        $vn_ledger->save();
    }

    /**
     * Get particular data.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function getParticular()
    {
        return $this->belongsTo('App\Models\Particular', 'PARTICULAR_ID', 'PARTICULAR_ID');
    }

    /**
     * Get Project Category Data.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function getCategory()
    {
        return $this->belongsTo('App\Models\ProjectCategory', 'PR_CATEGORY', 'PR_CATEGORY');
    }

    /**
     * Get voucher child ledger data by VOUCHER_NO.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function getVnLeager()
    {
        return $this->belongsTo('App\Models\AccountVnLedger', 'VOUCHER_NO', 'VOUCHER_NO');
    }

    /**
     * Get voucher child ledger data by TRX_TRAN_NO.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function voucherLeager()
    {
        return $this->hasOne('App\Models\AccountVnLedger', 'TRX_TRAN_NO', 'TRX_TRAN_NO');
    }

    /**
     * Get voucher child schedule.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function voucherSchedule()
    {
        return $this->hasMany('App\Models\AccountVaucherChd', 'SCHEDULE_ID', 'SCHEDULE_ID');
    }

    /**
     * Get parent vaucher.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function getVaucher()
    {
        return $this->belongsTo('App\Models\AccountVaucherMst', 'VOUCHER_NO', 'VOUCHER_NO');
    }

    /**
     * Get tender schedule.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function getSchedule()
    {
        return $this->belongsTo('App\Models\TenderSchedule', 'SCHEDULE_ID', 'SCHEDULE_ID');
    }

    /**
     * Get tender applicant.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function getApplicant()
    {
        return $this->belongsTo('App\Models\TenderApplicant', 'TE_APP_ID', 'TE_APP_ID');
    }

    /**
     * Get tender citizen.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function getCitizen()
    {
        return $this->belongsTo('App\Models\Citizen', 'CITIZEN_ID', 'CITIZEN_ID');
    }

    /**
     * Get lease demesne.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function getLeaseDemesne()
    {
        return $this->belongsTo('App\Models\LeaseDemesne', 'LEASE_DE_ID', 'LEASE_DE_ID');
    }
}
