<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Particular extends Model
{
    protected $table = 'ac_particular';
    protected $primaryKey = 'PARTICULAR_ID';
    protected $fillable = ['PARTICULAR_NAME', 'PARTICULAR_DESC', 'IS_ACTIVE', 'CREATED_BY', 'UPDATED_BY'];

    /**
     * Get datatable source data of ac_particular.
     *
     * @author      Nurullah <nurul@atilimited.net>
     *
     * @param $input
     *
     * @return response
     */
    public static function getData($input)
    {
        $data = self::select(['PARTICULAR_ID', 'PARTICULAR_NAME', 'PARTICULAR_DESC', 'IS_ACTIVE', 'CREATED_AT'])
                          ->orderBy('PARTICULAR_ID', 'desc')
                          ->take($input['length'])
                          ->skip($input['start'])
                          ->get();

        $total = self::count();

        return ['data' => $data, 'total' => $total, 'filtered' => $total];
    }
}
