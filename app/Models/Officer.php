<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Officer extends Model
{
    protected $table = 'sa_officer';
    protected $primaryKey = 'OFFICER_ID';
    const CREATED_AT = 'CREATED_AT';
    const UPDATED_AT = 'UPDATED_AT';

    protected $fillable = [
                            'NAME',
                            'DESIGNATION',
                            'OFFICE',
                            'PHONE',
                            'EMAIL',
                            'TYPE',
                            'IS_ACTIVE',
                            'CREATED_BY',
                            'UPDATED_BY',
                        ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = ['OFFICER_ID'];
}
