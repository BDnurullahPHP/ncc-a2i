<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LandUse extends Model
{
    protected $table = 'land_uses';
    protected $primaryKey = 'LU_ID';
    const CREATED_AT = 'CREATED_AT';
    const UPDATED_AT = 'UPDATED_AT';
    protected $fillable = [
                                'LAND_ID',
                                'LAND_CATEGORY',
                                'LAND_SUBCATEGORY',
                                'USES',
                                'CREATED_BY',
                                'UPDATED_BY',
                            ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = ['LU_ID'];

    /**
     * Get parent category.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function getCaregory()
    {
        return $this->belongsTo('App\Models\LandCategory', 'LAND_CATEGORY', 'CAT_ID');
    }

    /**
     * Get parent subcategory.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function getSubCaregory()
    {
        return $this->belongsTo('App\Models\LandCategory', 'LAND_SUBCATEGORY', 'CAT_ID');
    }
}
