<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class PaymentResponse extends Model
{
    protected $table = 'ac_pgw_return';
    protected $primaryKey = 'AC_PGW_ID';
    const CREATED_AT = 'CREATED_AT';
    const UPDATED_AT = 'UPDATED_AT';
    protected $fillable = [
                                'VOUCHER_NO',
                                'TRX_TRAN_NO',
                                'PAYMENT_TYPE',
                                'STORE_ID',
                                'TRAN_ID',
                                'VAL_ID',
                                'AMOUNT',
                                'STORE_AMOUNT',
                                'CARD_TYPE',
                                'CARD_NO',
                                'CARD_ISSUER',
                                'CARD_BRAND',
                                'CARD_ISSUER_COUNTRY',
                                'CARD_ISSUER_COUNTRY_CODE',
                                'BANK_TRAN_ID',
                                'BANK_ID',
                                'B_DRAFT_NO',
                                'ATTACHMENT',
                                'STATUS',
                                'TRAN_DATE',
                                'CURRENCY',
                                'CURRENCY_TYPE',
                                'CURRENCY_AMOUNT',
                                'CURRENCY_RATE',
                                'VERIFY_SIGN',
                                'VERIFY_KEY',
                                'BASE_FAIR',
                                'RISK_LEVEL',
                                'RISK_TITLE',
                                'ERROR',
                                'REMARKS',
                                'IS_ACTIVE',
                                'CREATED_BY',
                                'UPDATED_BY',
                            ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = ['AC_PGW_ID'];

    /**
     * Insert payment response data.
     *
     * @param array       $data
     * @param string      $type
     * @param string      $part
     * @param string      $vchd
     * @param string      $sc
     * @param string|null $user
     *
     * @return array
     */
    public static function bankOrCashPayment($data, $type, $part, $vchd, $sc, $user)
    {
        //echo'<pre>';print_r($data);exit;
        $data['tran_date'] = isset($data['bank_date']) ? $data['bank_date'] : $data['cash_date'];
        $data['amount'] = $data['total_amount'];
        $voucher_chd = AccountVaucherChd::where('TRX_TRAN_NO', $vchd)->first();
        $payment_data = self::storePaymentData($data, null, $voucher_chd->VOUCHER_NO, $voucher_chd->TRX_TRAN_NO);
        $ledger = self::storeSuccessResponse($data, $voucher_chd->VOUCHER_NO, $vchd);

        if ($type == 'citizen') {
            $citizen_user = Citizen::with('getTenderCitizen')->where('CITIZEN_ID', $user)->first();
            $mobile_no = $citizen_user->getTenderCitizen->MOBILE_NO;
        } else {
            $mobile_no = TenderApplicant::where('TE_APP_ID', $user)->pluck('APPLICANT_PHONE');
        }
        $notification = Notification::paymentNotification($data, $type, $part, $vchd, $sc, $user);

        return ['payment_data' => $payment_data, 'mobile_no' => $mobile_no, 'sms' => $notification];
    }

    /**
     * Insert payment response data.
     *
     * @param array  $data
     * @param string $type
     * @param string $part
     * @param string $vchd
     * @param string $sc
     * @param string $user
     *
     * @return number
     */
    public static function makePayment($data, $type, $part, $vchd, $sc, $user)
    {
        $voucher_chd = AccountVaucherChd::where('TRX_TRAN_NO', $vchd)->first();

        // Store return data by payment getway
        $payment_response = self::storeResponse($data, $user, $voucher_chd->VOUCHER_NO, $voucher_chd->TRX_TRAN_NO);

        // If payment success then
        // Store data to 'ac_paymentmst', 'ac_paymodeamt' and 'ac_vn_ledgers' table
        // Update tender applicant table 'IS_TE_SC_BOUGHT' and 'TE_SC_DATE' collum
        if (!isset($data['error']) && $data['status'] == 'VALID') {
            self::storeSuccessResponse($data, $voucher_chd->VOUCHER_NO, $vchd);

            // $update_app = ['IS_TE_SC_BOUGHT' => 1, 'TE_SC_DATE' => date('Y-m-d H:i:s', strtotime($data['tran_date']))];

            // if ($type == 'applicant') {
            //     $applicant_property = ApplicantProperty::where('TE_APP_ID', $user)->where('SCHEDULE_ID', $sc)->update($update_app);
            // } else {
            //     $citizen_app = CitizenApplication::where('CITIZEN_ID', $user)->where('SCHEDULE_ID', $sc)->update($update_app);
            // }
        }

        return $payment_response;
    }

    /**
     * Store installment data response.
     *
     * @param $data
     * @param $type
     * @param $id
     * @param $vchd
     * @param $sc
     * @param $user
     *
     * @return number
     */
    public static function installmentPayResponse($data, $type, $id, $vchd, $sc, $user)
    {
        $voucher_chd = AccountVaucherChd::where('TRX_TRAN_NO', $vchd)->first();

        // Store return data by payment getway
        $payment_response = self::storeResponse($data, $user, $voucher_chd->VOUCHER_NO, $voucher_chd->TRX_TRAN_NO);

        // If payment success then
        // Store data to 'ac_paymentmst', 'ac_paymodeamt' and 'ac_vn_ledgers' table
        // Update tender applicant table 'IS_TE_SC_BOUGHT' and 'TE_SC_DATE' collum
        if (!isset($data['error']) && $data['status'] == 'VALID') {
            self::storeSuccessResponse($data, $voucher_chd->VOUCHER_NO, $vchd);

            $tender_installment = TenderInstallment::find($id);
            $tender_installment->IS_PAID = 1;
            $tender_installment->save();
        }

        return $payment_response;
    }

    /**
     * Insert payment response data for rent payment.
     *
     * @param array  $data Request data
     * @param string $vc   Voucher child number
     * @param string $sc   Tender schedule id
     * @param string $sc   Tender schedule id
     * @param string $user Authonticated user
     *
     * @return string $payment_response payment response id
     */
    public static function rentPayment($data, $vc, $sc, $ctz, $user)
    {
        $vaucher_no = AccountVaucherChd::where('TRX_TRAN_NO', $vc)->pluck('VOUCHER_NO');

        // Store return data by payment getway
        $payment_response = self::storeResponse($data, $user, $vaucher_no, $vc);

        // If payment success then
        // Store data to 'ac_paymentmst', 'ac_paymodeamt' and 'ac_vn_ledgers' table
        // Update tender applicant table 'IS_TE_SC_BOUGHT' and 'TE_SC_DATE' collum
        if (!isset($data['error']) && $data['status'] == 'VALID') {
            self::storeSuccessResponse($data, $vaucher_no, $vc);
        }

        return $payment_response;
    }

    /**
     * Save payment response data.
     *
     * @param $data
     * @param $user
     * @param $vaucher_no
     * @param $voucher_chd
     *
     * @return number
     */
    private static function storeResponse($data, $user, $vaucher_no, $voucher_chd)
    {
        $payment = new self();
        $payment->VOUCHER_NO = $vaucher_no;
        $payment->TRX_TRAN_NO = $voucher_chd;
        $payment->PAYMENT_TYPE = 2;
        $payment->TRAN_ID = isset($data['tran_id']) && !empty($data['tran_id']) ? $data['tran_id'] : null;
        $payment->VAL_ID = isset($data['val_id']) && !empty($data['val_id']) ? $data['val_id'] : null;
        $payment->AMOUNT = isset($data['amount']) && !empty($data['amount']) ? $data['amount'] : null;
        $payment->CARD_TYPE = isset($data['card_type']) && !empty($data['card_type']) ? $data['card_type'] : null;
        $payment->STORE_AMOUNT = isset($data['store_amount']) && !empty($data['store_amount']) ? $data['store_amount'] : null;
        $payment->CARD_NO = isset($data['card_no']) && !empty($data['card_no']) ? $data['card_no'] : null;
        $payment->BANK_TRAN_ID = isset($data['bank_tran_id']) && !empty($data['bank_tran_id']) ? $data['bank_tran_id'] : null;
        $payment->STATUS = isset($data['status']) && !empty($data['status']) ? $data['status'] : null;
        $payment->TRAN_DATE = isset($data['tran_date']) && !empty($data['tran_date']) ? date('Y-m-d H:i:s', strtotime($data['tran_date'])) : null;
        $payment->CURRENCY = isset($data['currency']) && !empty($data['currency']) ? $data['currency'] : null;
        $payment->CARD_ISSUER = isset($data['card_issuer']) && !empty($data['card_issuer']) ? $data['card_issuer'] : null;
        $payment->CARD_BRAND = isset($data['card_brand']) && !empty($data['card_brand']) ? $data['card_brand'] : null;
        $payment->CARD_ISSUER_COUNTRY = isset($data['card_issuer_country']) && !empty($data['card_issuer_country']) ? $data['card_issuer_country'] : null;
        $payment->CARD_ISSUER_COUNTRY_CODE = isset($data['card_issuer_country_code']) && !empty($data['card_issuer_country_code']) ? $data['card_issuer_country_code'] : null;
        $payment->STORE_ID = isset($data['store_id']) && !empty($data['store_id']) ? $data['store_id'] : $data['store_id'];
        $payment->VERIFY_SIGN = isset($data['verify_sign']) && !empty($data['verify_sign']) ? $data['verify_sign'] : $data['verify_sign'];
        $payment->VERIFY_KEY = isset($data['verify_key']) && !empty($data['verify_key']) ? $data['verify_key'] : $data['verify_key'];
        $payment->CURRENCY_TYPE = isset($data['currency_type']) && !empty($data['currency_type']) ? $data['currency_type'] : $data['currency_type'];
        $payment->CURRENCY_AMOUNT = isset($data['currency_amount']) && !empty($data['currency_amount']) ? $data['currency_amount'] : $data['currency_amount'];
        $payment->CURRENCY_RATE = isset($data['currency_rate']) && !empty($data['currency_rate']) ? $data['currency_rate'] : $data['currency_rate'];
        $payment->BASE_FAIR = isset($data['base_fair']) && !empty($data['base_fair']) ? $data['base_fair'] : $data['base_fair'];
        $payment->RISK_LEVEL = isset($data['risk_level']) && !empty($data['risk_level']) ? $data['risk_level'] : null;
        $payment->RISK_TITLE = isset($data['risk_title']) && !empty($data['risk_title']) ? $data['risk_title'] : null;
        $payment->ERROR = isset($data['error']) && !empty($data['error']) ? $data['error'] : null;
        $payment->REMARKS = isset($data['value_a']) && !empty($data['value_a']) ? $data['value_a'] : null;
        $payment->IS_ACTIVE = 1;
        $payment->CREATED_BY = $user;
        $payment->save();

        return $payment->AC_PGW_ID;
    }

    /**
     * Save payment data.
     *
     * @param $data
     * @param $user
     * @param $vaucher_no
     * @param $voucher_chd
     *
     * @return number
     */
    private static function storePaymentData($data, $user, $vaucher_no, $voucher_chd)
    {
        $payment = new self();
        $payment->VOUCHER_NO = $vaucher_no;
        $payment->TRX_TRAN_NO = $voucher_chd;
        $payment->PAYMENT_TYPE = $data['payment_method'];
        $payment->AMOUNT = $data['total_amount'];
        $payment->BANK_ID = isset($data['bank_id']) && !empty($data['bank_id']) ? $data['bank_id'] : null;
        if (isset($data['bank_druft']) && !empty($data['bank_druft'])) {
            $payment->B_DRAFT_NO = $data['bank_druft'];
            $payment->TRAN_DATE = date('Y-m-d H:i:s', strtotime($data['bank_date']));
        } elseif (isset($data['challan_no']) && !empty($data['challan_no'])) {
            $payment->B_DRAFT_NO = $data['challan_no'];
            $payment->TRAN_DATE = date('Y-m-d H:i:s', strtotime($data['cash_date']));
        } else {
            $payment->B_DRAFT_NO = null;
            $payment->TRAN_DATE = null;
        }
        if (isset($data['bank_attachment'])) {
            $porder = $data['bank_attachment'];
            $porder_name = md5(uniqid(rand(), true));
            $porder_ext = $porder->getClientOriginalExtension();
            $porder_ext = strtolower($porder_ext);
            $porder_fullname = $porder_name.'.'.$porder_ext;
            $payment->ATTACHMENT = $porder_fullname;
            $porder->move(base_path().'/public/upload/bank/', $porder_fullname);
        } elseif (isset($data['cash_attachment'])) {
            $porder = $data['cash_attachment'];
            $porder_name = md5(uniqid(rand(), true));
            $porder_ext = $porder->getClientOriginalExtension();
            $porder_ext = strtolower($porder_ext);
            $porder_fullname = $porder_name.'.'.$porder_ext;
            $payment->ATTACHMENT = $porder_fullname;
            $porder->move(base_path().'/public/upload/bank/', $porder_fullname);
        }
        $payment->REMARKS = isset($data['comment']) && !empty($data['comment']) ? $data['comment'] : null;
        $payment->IS_ACTIVE = 1;
        $payment->CREATED_BY = $user;
        $payment->save();

        return $payment->AC_PGW_ID;
    }

    /**
     * Create account vaoucher.
     *
     * @param $data
     * @param $vaucher_no
     * @param $vchd
     *
     * @return void
     */
    private static function storeSuccessResponse($data, $vaucher_no, $vchd)
    {
        $vn_ledger = new AccountVnLedger();
        $vn_ledger->VLEDGER_DT = date('Y-m-d H:i:s', strtotime($data['tran_date']));
        $vn_ledger->TRX_CODE_ID = 5;
        $vn_ledger->VOUCHER_NO = $vaucher_no;
        $vn_ledger->TRX_TRAN_NO = $vchd;
        $vn_ledger->DR_AMT = $data['amount'];
        $vn_ledger->IS_ACTIVE = 1;
        $vn_ledger->save();
        if (isset($data['particular_id']) && $data['particular_id'] == 8) {
            $tender_installment = TenderInstallment::where('TRX_TRAN_NO', $vchd)->first();
            $tender_installment->IS_PAID = 1;
            $tender_installment->IS_APPROVED = 1;
            $tender_installment->save();
        }
    }

    /**
     * Applicant payment history data collection.
     *
     * @param array      $input
     * @param collection $voucher
     *
     * @return Collection
     */
    public static function getHistoryData($input, $voucher)
    {
        $voucher_no = [];
        foreach ($voucher as $key => $v) {
            $voucher_no[$key] = $v->VOUCHER_NO;
        }
        $data = self::with(['getVoucherChd' => function ($query) {
            $query->with('getParticular');
        }])
                                ->whereIn('VOUCHER_NO', $voucher_no)
                                ->orderBy('AC_PGW_ID', 'desc')
                                ->take($input['length'])
                                ->skip($input['start'])
                                ->get();

        $total = self::whereIn('VOUCHER_NO', $voucher_no)->count();

        return ['data' => $data, 'total' => $total, 'filtered' => $total];
    }

    /**
     * Get parent voucher child.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function getVoucherChd()
    {
        return $this->belongsTo('App\Models\AccountVaucherChd', 'TRX_TRAN_NO', 'TRX_TRAN_NO');
    }
}

/*
 * Demo response data by SSL
 */

// Success response
/*
Array
(
    [tran_id] => tranjXgn3EpzhQ
    [val_id] => 1609081750160oPsC5WirxUeDUw
    [amount] => 2500
    [card_type] => AMEX-City Bank
    [store_amount] => 2412.5
    [card_no] =>
    [bank_tran_id] => 1609081750161rpOi0zfRumYv5W
    [status] => VALID
    [tran_date] => 2016-09-08 17:50:13
    [currency] => BDT
    [card_issuer] =>
    [card_brand] =>
    [card_issuer_country] =>
    [card_issuer_country_code] =>
    [store_id] => testbox
    [verify_sign] => e20f3994c655027676aba6caaf688af1
    [verify_key] => amount,bank_tran_id,base_fair,card_brand,card_issuer,card_issuer_country,card_issuer_country_code,card_no,card_type,currency,currency_amount,currency_rate,currency_type,risk_level,risk_title,status,store_amount,store_id,tran_date,tran_id,val_id,value_a,value_b,value_c,value_d
    [currency_type] => BDT
    [currency_amount] => 2500.00
    [currency_rate] => 1.0000
    [base_fair] => 0.00
    [value_a] =>
    [value_b] =>
    [value_c] =>
    [value_d] =>
    [risk_level] => 0
    [risk_title] => Safe
)*/

// Error response
/*
Array
(
    [tran_id] => tranjXgn3EpzhQ
    [error] => system error: (unable to process transaction request)
    [status] => FAILED
    [bank_tran_id] => 16090817443809fgiZYqQ6fxGPN
    [currency] => BDT
    [tran_date] => 2016-09-08 17:44:35
    [amount] => 2500
    [store_id] => testbox
    [card_type] =>
    [card_no] => 100001*********1610
    [card_issuer] => Dutch Bangla Bank Limited
    [card_brand] => NEXUS
    [card_issuer_country] => Bangladesh
    [card_issuer_country_code] => BD
    [currency_type] => BDT
    [currency_amount] => 2500.00
    [currency_rate] => 1.0000
    [base_fair] => 0.00
    [value_a] =>
    [value_b] =>
    [value_c] =>
    [value_d] =>
    [verify_sign] => 587c8b6f09a09bf1c76c142b311dda1a
    [verify_key] => amount,bank_tran_id,base_fair,card_brand,card_issuer,card_issuer_country,card_issuer_country_code,card_no,card_type,currency,currency_amount,currency_rate,currency_type,error,status,store_id,tran_date,tran_id,value_a,value_b,value_c,value_d
)*/

// Cancel response
/*
Array
(
    [status] => CANCELLED
    [tran_id] => tranrCzl1DPL0E
    [error] => Cancelled by User
    [bank_tran_id] =>
    [currency] => BDT
    [tran_date] => 2016-09-18 15:00:00
    [amount] => 2500
    [store_id] => testbox
    [currency_type] => BDT
    [currency_amount] => 2500.00
    [currency_rate] => 1.0000
    [base_fair] => 0.00
    [value_a] =>
    [value_b] =>
    [value_c] =>
    [value_d] =>
    [verify_sign] => ff11a18c7ed4b7d4579fa73b0bf39775
    [verify_key] => amount,bank_tran_id,base_fair,currency,currency_amount,currency_rate,currency_type,error,status,store_id,tran_date,tran_id,value_a,value_b,value_c,value_d
)
*/
