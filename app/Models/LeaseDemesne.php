<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LeaseDemesne extends Model
{
    protected $table = 'lease_demesne';
    protected $primaryKey = 'LEASE_DE_ID';
    const CREATED_AT = 'CREATED_AT';
    const UPDATED_AT = 'UPDATED_AT';

    protected $fillable = [
                            'SCHEDULE_ID',
                            'NAME',
                            'PHONE',
                            'ADDRESS',
                            'REMARKS',
                            'IS_CANCEL',
                            'CANCEL_REMARKS',
                            'IS_ACTIVE',
                            'CREATED_BY',
                            'UPDATED_BY',
                        ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = ['LEASE_DE_ID'];

    /**
     * Demesne data collection.
     *
     * @param $input array
     *
     * @return Collection
     */
    public static function getData($input)
    {
        $data = self::with('getValidity')
                        ->orderBy('LEASE_DE_ID', 'desc')
                        ->take($input['length'])
                        ->skip($input['start'])
                        ->get();

        $total = self::count();

        return ['data' => $data, 'total' => $total, 'filtered' => $total];
    }

    /**
     * Demesne Money collection Data.
     *
     * @param $input array
     *
     * @return Collection
     */
    public static function getMoneyData($input)
    {
        $data = AccountVaucherChd::with('getVnLeager')
                        ->where('PARTICULAR_ID', 11)
                        ->orderBy('VOUCHER_NO', 'desc')
                        ->take($input['length'])
                        ->skip($input['start'])
                        ->get();

        $total = $data->count();

        return ['data' => $data, 'total' => $total, 'filtered' => $total];
    }

    /**
     * Inser Demesne data
     * Nurullah<nurul@atilimited.net>.
     *
     * @param $data array
     * @param $user string
     * @param $id string
     *
     * @return string
     */
    public static function createDemesne($data, $user)
    {
        $demesne = new self();
        $demesne->SCHEDULE_ID = $data['SCHEDULE_ID'];
        $demesne->NAME = $data['NAME'];
        $demesne->PHONE = $data['PHONE'];
        $demesne->ADDRESS = $data['ADDRESS'];
        $demesne->IS_ACTIVE = isset($data['IS_ACTIVE']) ? $data['IS_ACTIVE'] : 0;
        $demesne->CREATED_BY = $user;
        $demesne->save();
        $demesneid = $demesne->LEASE_DE_ID;

        $validity = new LeaseValidity();
        $validity->LEASE_DE_ID = $demesneid;
        $validity->SCHEDULE_ID = $data['SCHEDULE_ID'];
        $validity->DATE_FROM = date('Y-m-d', strtotime(str_replace('/', '-', $data['DATE_FROM'])));
        $validity->DATE_TO = date('Y-m-d', strtotime(str_replace('/', '-', $data['DATE_TO'])));
        $validity->CREATED_BY = $user;
        $validity->save();

        return $demesneid;
    }

    /**
     * Update demesne data
     * Nurullah<nurul@atilimited.net>.
     *
     * @param $data array
     * @param $user string
     * @param $id string
     *
     * @return string
     */
    public static function updateDemesne($data, $user, $id)
    {
        $demesne = self::find($id);
        $demesne->SCHEDULE_ID = $data['SCHEDULE_ID'];
        $demesne->NAME = $data['NAME'];
        $demesne->PHONE = $data['PHONE'];
        $demesne->ADDRESS = $data['ADDRESS'];
        $demesne->IS_ACTIVE = isset($data['IS_ACTIVE']) ? $data['IS_ACTIVE'] : 0;
        $demesne->UPDATED_BY = $user;
        $demesne->save();
        $demesneid = $demesne->LEASE_DE_ID;

        $validity = LeaseValidity::where('LEASE_DE_ID', $id)->first();
        $validity->SCHEDULE_ID = $data['SCHEDULE_ID'];
        $validity->DATE_FROM = date('Y-m-d', strtotime(str_replace('/', '-', $data['DATE_FROM'])));
        $validity->DATE_TO = date('Y-m-d', strtotime(str_replace('/', '-', $data['DATE_TO'])));
        $validity->UPDATED_BY = $user;
        $validity->save();

        return $demesneid;
    }

    /**
     * Get Lease Demesne Validity.
     *
     * @author      Nurullah <nurul@atilimited.net>
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function getValidity()
    {
        return $this->belongsTo('App\Models\LeaseValidity', 'LEASE_DE_ID', 'LEASE_DE_ID');
    }

    /**
     * Get tender schedule.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function getSchedule()
    {
        return $this->belongsTo('App\Models\TenderSchedule', 'SCHEDULE_ID', 'SCHEDULE_ID');
    }
}
