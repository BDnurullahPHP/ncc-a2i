<?php

namespace App\Models;

use DB;
use Illuminate\Database\Eloquent\Model;

class TenderSchedule extends Model
{
    protected $table = 'tender_schedule';
    protected $primaryKey = 'SCHEDULE_ID';
    const CREATED_AT = 'CREATED_AT';
    const UPDATED_AT = 'UPDATED_AT';

    protected $fillable = [
                            'TENDER_ID',
                            'PROJECT_ID',
                            'PR_DETAIL_ID',
                            'PR_CATEGORY',
                            'PR_TYPE',
                            'LOWEST_TENDER_MONEY',
                            'SC_SERIAL',
                            'IS_CURRENT',
                            'NO_OF_INSTALLMENT',
                            'DAYS_BETWEEN_INSTALLMENT',
                            'IS_ACTIVE',
                            'CREATED_BY',
                            'UPDATED_BY',
                        ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = ['SCHEDULE_ID'];

    /**
     * Tender data collection.
     *
     * @param $input array
     *
     * @return Collection
     */
    public static function getData($input, $type)
    {
        $query = self::with(['getProject', 'getProjectType', 'getProjectDetail', 'getTenderParticulars', 'getCategoryParticulars'])
                    ->leftJoin('tender', 'tender.TENDER_ID', '=', 'tender_schedule.TENDER_ID')
                    ->leftJoin('tender_date_time', 'tender_date_time.TENDER_ID', '=', 'tender_schedule.TENDER_ID')
                    ->select('tender_schedule.*', 'tender.*', 'tender_date_time.*')
                    ->where('tender.IS_LEASE', 0)
                    ->where('tender.IS_ACTIVE', 1)
                    ->where('tender_date_time.IS_ACTIVE', 1)
                    ->where('tender_schedule.PR_CATEGORY', $type)
                    ->where('tender.TENDER_PUBLISH_DT', '<=', date('Y-m-d'))
                    ->where('tender_date_time.LAST_SELLING_DT_FROM', '>=', date('Y-m-d'))
                    ->orderBy('tender_schedule.SCHEDULE_ID', 'desc');

        $data = $query->take($input['length'])->skip($input['start'])->get();
        $total = $query->count();

        return ['data' => $data, 'total' => $total, 'filtered' => $total];
    }

    /**
     * Tender application data collection.
     *
     * @param $input array
     *
     * @return Collection
     */
    public static function getTenderData($input)
    {
        $query = self::with(['getProject', 'getProjectType', 'getProjectDetail', 'getTenderParticulars', 'getCategoryParticulars', 'getCategory'])
                    ->leftJoin('tender', 'tender.TENDER_ID', '=', 'tender_schedule.TENDER_ID')
                    ->leftJoin('tender_date_time', 'tender_date_time.TENDER_ID', '=', 'tender_schedule.TENDER_ID')
                    ->select('tender_schedule.*', 'tender.*', 'tender_date_time.*')
                    ->where('tender.IS_LEASE', 0)
                    ->where('tender.IS_ACTIVE', 1)
                    ->where('tender_date_time.IS_ACTIVE', 1)
                    ->whereIn('tender_schedule.PR_CATEGORY', [21, 22, 23])
                    ->where('tender.TENDER_PUBLISH_DT', '<=', date('Y-m-d'))
                    ->where('tender_date_time.LAST_SELLING_DT_FROM', '>=', date('Y-m-d'))
                    ->orderBy('tender_schedule.PR_CATEGORY', 'asc');

        $data = $query->take($input['length'])->skip($input['start'])->get();
        $total = $query->count();

        return ['data' => $data, 'total' => $total, 'filtered' => $total];
    }

    /**
     * Lease tender data collection.
     *
     * @param $input array
     *
     * @return Collection
     */
    public static function getLeaseData($input)
    {
        $query = self::with(['getProject', 'getProjectType', 'getProjectDetail', 'getTenderParticulars', 'getCategoryParticulars', 'getCategory'])
                    ->leftJoin('tender', 'tender.TENDER_ID', '=', 'tender_schedule.TENDER_ID')
                    ->leftJoin('tender_date_time', 'tender_date_time.TENDER_ID', '=', 'tender_schedule.TENDER_ID')
                    ->select('tender_schedule.*', 'tender.*', 'tender_date_time.*')
                    ->where('tender.IS_LEASE', 1)
                    ->where('tender.IS_ACTIVE', 1)
                    ->where('tender_date_time.IS_ACTIVE', 1)
                    ->where('tender.TENDER_PUBLISH_DT', '<=', date('Y-m-d'))
                    ->where('tender_date_time.LAST_SELLING_DT_FROM', '>=', date('Y-m-d'))
                    ->orderBy('tender_schedule.SCHEDULE_ID', 'desc');

        $data = $query->take($input['length'])->skip($input['start'])->get();
        $total = $query->count();

        return ['data' => $data, 'total' => $total, 'filtered' => $total];
    }

    /**
     * Get project category count.
     *
     * @return collection
     */
    public static function categoryCount()
    {
        return self::with('getCategory')
                                ->select('*', DB::raw('count(SCHEDULE_ID) as total'))
                                ->groupBy('PR_CATEGORY')
                                ->get();
    }

    /**
     * Get Project.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function getProject()
    {
        return $this->belongsTo('App\Models\Project', 'PROJECT_ID', 'PROJECT_ID');
    }

    /**
     * Get category.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function getCategory()
    {
        return $this->belongsTo('App\Models\ProjectCategory', 'PR_CATEGORY', 'PR_CATEGORY');
    }

    /**
     * Get Project Type.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function getProjectType()
    {
        return $this->belongsTo('App\Models\ProjectType', 'PR_TYPE', 'PR_TYPE');
    }

    /**
     * Get Project Details.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function getProjectDetail()
    {
        return $this->belongsTo('App\Models\ProjectDetails', 'PR_DETAIL_ID', 'PR_DETAIL_ID');
    }

    /**
     * Get Tender Particulars.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function getTenderParticulars()
    {
        return $this->hasMany('App\Models\TenderParticulars', 'SCHEDULE_ID', 'SCHEDULE_ID');
    }

    /**
     * Get category Particulars.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function getCategoryParticulars()
    {
        return $this->belongsTo('App\Models\CategoryParticular', 'PR_CATEGORY', 'PR_CATEGORY');
    }

    /**
     * Get parent ward.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function getWord()
    {
        return $this->belongsTo('App\Models\Zone', 'WARD_ID', 'WARD_ID');
    }

    /**
     * Get parent mouza.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function getMouza()
    {
        return $this->belongsTo('App\Models\Mouza', 'MOUZA_ID', 'MOUZA_ID');
    }
}
