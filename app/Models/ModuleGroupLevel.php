<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ModuleGroupLevel extends Model
{
    protected $table = 'sa_ug_level';
    protected $primaryKey = 'UG_LEVEL_ID';

    const CREATED_AT = 'CREATED_AT';
    const UPDATED_AT = 'UPDATED_AT';

    protected $fillable = ['USERGRP_ID', 'ORG_ID', 'UGLEVE_NAME', 'SL_NO', 'IS_ACTIVE', 'CREATED_BY', 'UPDATED_BY'];

    /**
     * Save or update Module.
     *
     *@author  Nurullah <nurul@atilimited.net>
     *
     * @param array       $data
     * @param string      $user
     * @param string|null $id
     *
     * @return string
     */
    public static function createGroupLevel($data, $user, $id)
    {
        $group = new self();
        $group->USERGRP_ID = $id;
        $group->ORG_ID = 1;
        $group->UGLEVE_NAME = $data['UGLEVE_NAME'];
        $group->CREATED_BY = $user;

        $group->save();
    }

    /*
    * Module data collection
    *@author  Nurullah <nurul@atilimited.net>
    * @param $input array
    * @return Collection
    */
//    public static function getData($input)
//    {
//        $data = Module::orderBy('SL_NO','desc')
//                    ->take($input['length'])
//                    ->skip($input['start'])
//                    ->get();
//
//        $total = Module::count();
//
//        return array('data' => $data, 'total' => $total, 'filtered' => $total);
//    }
}
