<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TenderConditions extends Model
{
    protected $table = 'tender_conditions';
    protected $primaryKey = 'TE_CON_ID';
    const CREATED_AT = 'CREATED_AT';
    const UPDATED_AT = 'UPDATED_AT';
    protected $fillable = [
                                'TENDER_ID',
                                'PR_CATEGORY',
                                'CON_DESC',
                                'IS_ACTIVE',
                                'CREATED_AT',
                                'CREATED_BY',
                                'UPDATED_AT',
                                'UPDATED_BY',
                            ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = ['TE_CON_ID'];

    /**
     * Get project category name.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function getCategory()
    {
        return $this->belongsTo('App\Models\ProjectCategory', 'PR_CATEGORY', 'PR_CATEGORY');
    }
}
