<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LandCategory extends Model
{
    protected $table = 'land_category';
    protected $primaryKey = 'CAT_ID';
    const CREATED_AT = 'CREATED_AT';
    const UPDATED_AT = 'UPDATED_AT';

    protected $fillable = ['PARENT_ID', 'CAT_NAME', 'CAT_NAME_BN', 'IS_ACTIVE', 'CREATED_BY', 'UPDATED_BY'];

    /**
     * Category data collection.
     *
     * @param $input array
     *
     * @return Collection
     */
    public static function getData($input)
    {
        $data = self::where('PARENT_ID', 0)
                            ->select(['CAT_ID', 'CAT_NAME', 'CAT_NAME_BN', 'IS_ACTIVE', 'CREATED_AT'])
                            ->orderBy('CAT_ID', 'desc')
                            ->take($input['length'])
                            ->skip($input['start'])
                            ->get();

        $total = self::where('PARENT_ID', 0)->count();

        return ['data' => $data, 'total' => $total, 'filtered' => $total];
    }

    /**
     * Subcategory data collection.
     *
     * @param $input array
     *
     * @return Collection
     */
    public static function getSubcategoryData($input)
    {
        $data = self::where('parent_id', '!=', 0)
                            ->select(['CAT_ID', 'PARENT_ID', 'CAT_NAME', 'CAT_NAME_BN', 'IS_ACTIVE', 'CREATED_AT'])
                            ->orderBy('CAT_ID', 'desc')
                            ->take($input['length'])
                            ->skip($input['start'])
                            ->get();

        $total = self::where('parent_id', '!=', 0)->count();

        return ['data' => $data, 'total' => $total, 'filtered' => $total];
    }

    /**
     * Save or update land category.
     *
     * @param array       $data
     * @param string      $user
     * @param string|null $id
     *
     * @return string
     */
    public static function saveCategory($data, $user, $id = null)
    {
        if (!empty($id)) {
            $category = self::find($id);
            $category->UPDATED_BY = $user;
        } else {
            $category = new self();
            $category->CREATED_BY = $user;
        }
        $category->PARENT_ID = $data['category'];
        $category->CAT_NAME = $data['CAT_NAME'];
        $category->CAT_NAME_BN = $data['CAT_NAME_BN'];
        $category->IS_ACTIVE = isset($data['IS_ACTIVE']) ? $data['IS_ACTIVE'] : 0;
        $category->save();
    }

    /**
     * Get parent category.
     */
    public function getCategory()
    {
        return $this->hasOne('App\Models\LandCategory', 'PARENT_ID', 'CAT_ID');
    }
}
