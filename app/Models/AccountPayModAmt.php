<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AccountPayModAmt extends Model
{
    protected $table = 'ac_paymodeamt';
    protected $primaryKey = 'MR_TRAN_NO';
    const CREATED_AT = 'CREATED_AT';
    const UPDATED_AT = 'UPDATED_AT';
    protected $fillable = [
                                'MR_TRAN_DT',
                                'TRX_TRAN_NO',
                                'MR_TRAN_AMT',
                                'VOUCHER_NO',
                                'TRX_CODE_ID',
                                'PAYMENT_MODE_ID',
                                'B_MOBILE_NO',
                                'B_TRX_ID',
                                'B_PAYMENT_DT',
                                'CHEQUE_NO',
                                'C_PAYMENT_DT',
                                'AC_NO',
                                'AC_NAME',
                                'PAYMENT_OPTION',
                                'BANK_TRANS_ID',
                                'BANK_STATUS',
                                'BANK_ID',
                                'BRANCH_ID',
                                'REMARKS',
                                'IS_ACTIVE',
                                'CREATED_BY',
                                'UPDATED_BY',
                            ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = ['MR_TRAN_NO'];
}
