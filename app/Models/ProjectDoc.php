<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProjectDoc extends Model
{
    protected $table = 'project_docs';
    protected $primaryKey = 'PR_DOC_ID';
    const CREATED_AT = 'CREATED_AT';
    const UPDATED_AT = 'UPDATED_AT';

    protected $fillable = [
                            'PROJECT_ID',
                            'ATTACHMENT',
                            'TYPE',
                            'IS_ACTIVE',
                            'CREATED_BY',
                            'UPDATED_BY',
                        ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = ['PR_DOC_ID'];
}
