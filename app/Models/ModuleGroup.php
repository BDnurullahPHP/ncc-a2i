<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ModuleGroup extends Model
{
    protected $table = 'sa_user_group';
    protected $primaryKey = 'USERGRP_ID';

    const CREATED_AT = 'CREATED_AT';
    const UPDATED_AT = 'UPDATED_AT';

    protected $fillable = ['ORG_ID', 'USERGRP_NAME', 'IS_ACTIVE', 'CREATED_BY', 'UPDATED_BY'];

    /**
     * Save or update Module.
     *
     *@author  Nurullah <nurul@atilimited.net>
     *
     * @param array       $data
     * @param string      $user
     * @param string|null $id
     *
     * @return string
     */
    public static function createGroup($data, $user, $id)
    {
        $group = new self();
        $group->ORG_ID = 1;
        $group->USERGRP_NAME = $data['USERGRP_NAME'];
        $group->CREATED_BY = $user;

        $group->save();
    }

    /*
    * Module data collection
    *@author  Nurullah <nurul@atilimited.net>
    * @param $input array
    * @return Collection
    */
//    public static function getData($input)
//    {
//        $data = Module::orderBy('SL_NO','desc')
//                    ->take($input['length'])
//                    ->skip($input['start'])
//                    ->get();
//
//        $total = Module::count();
//
//        return array('data' => $data, 'total' => $total, 'filtered' => $total);
//    }
}
