<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AccountVaucherMst extends Model
{
    protected $table = 'ac_vouchermst';
    protected $primaryKey = 'VOUCHER_NO';
    const CREATED_AT = 'CREATED_AT';
    const UPDATED_AT = 'UPDATED_AT';
    protected $fillable = [
                                'VOUCHER_DT',
                                'USER_ID',
                                'TE_APP_ID',
                                'VCANCEL_FG',
                                'REMARKS',
                                'IS_ACTIVE',
                                'CREATED_AT',
                                'CREATED_BY',
                                'UPDATED_AT',
                                'UPDATED_BY',
                            ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = ['VOUCHER_NO'];

    /**
     * Get year range by citizen id.
     *
     * @param $citizen_id
     *
     * @return string
     */
    public static function getCitizenYearRange($citizen_id)
    {
        $str = '<option value="">'.trans('common.form_select').'</option>';
        $voucher_date = self::where('CITIZEN_ID', $citizen_id)->min('VOUCHER_DT');
        $voucher_date = date('Y', strtotime($voucher_date));

        for ($i = $voucher_date; $i <= date('Y') + 1; $i++) {
            $j = $i - 1;
            $str .= '<option value="'.$j.'-06-30/'.$i.'-07-01'.'">'.$j.'-'.$i.'</option>';
        }

        return $str;
    }

    /**
     * Get year range.
     *
     * @return string
     */
    public static function getYearRange()
    {
        $str = '';
        $voucher_date = self::min('VOUCHER_DT');
        $voucher_date = date('Y', strtotime($voucher_date));

        for ($i = $voucher_date - 1; $i <= date('Y') + 1; $i++) {
            $str .= '<option value="'.$i.'"';
            if ($i == date('Y')) {
                $str .= 'selected="selected"';
            }
            $str .= '>'.$i.'</option>';
        }

        return $str;
    }

    /**
     * Get citizen year range.
     *
     * @param string $citizen_id
     *
     * @return string
     */
    public static function getRentYearRange($citizen_id)
    {
        $str = '';
        $voucher_date = self::where('CITIZEN_ID', $citizen_id)->min('VOUCHER_DT');
        $voucher_date = date('Y', strtotime($voucher_date));

        for ($i = $voucher_date - 1; $i <= date('Y') + 1; $i++) {
            $str .= '<option value="'.$i.'"';
            if ($i == date('Y')) {
                $str .= 'selected="selected"';
            }
            $str .= '>'.$i.'</option>';
        }

        return $str;
    }

    /**
     * insert Demesne Amount Data.
     *
     * @author      Nurullah <nurul@atilimited.net>
     *
     * @param $user
     * @param $data
     *
     * @return number
     */
    public static function createVaucher($data, $user)
    {
        // voucher create
        $vaucher = new self();
        $vaucher->VOUCHER_DT = date('Y-m-d', strtotime(str_replace('/', '-', $data['VOUCHER_DT'])));
        $vaucher->LEASE_DE_ID = $data['LEASE_DE_ID'];
        $vaucher->IS_ACTIVE = 1;
        $vaucher->CREATED_BY = $user;
        $vaucher->save();

        $insert_vaucher = $vaucher->VOUCHER_NO;
        // voucher child create
        $vaucher_chd = new AccountVaucherChd();
        $vaucher_chd->TRX_TRAN_DT = date('Y-m-d', strtotime(str_replace('/', '-', $data['VOUCHER_DT'])));
        $vaucher_chd->VOUCHER_NO = $insert_vaucher;
        $vaucher_chd->SCHEDULE_ID = $data['SCHEDULE_ID'];
        $vaucher_chd->PARTICULAR_ID = 11;
        $vaucher_chd->PUNIT_PRICE = $data['amount'];
        $vaucher_chd->IS_ACTIVE = 1;
        $vaucher_chd->CREATED_BY = $user;
        $vaucher_chd->save();

        $insert_voucher_chd = $vaucher_chd->TRX_TRAN_NO;
        // leager a cr amount create 4
        $vaucher_ledgers = new AccountVnLedger();
        $vaucher_ledgers->VLEDGER_DT = date('Y-m-d', strtotime(str_replace('/', '-', $data['VOUCHER_DT'])));
        $vaucher_ledgers->TRX_CODE_ID = 4;
        $vaucher_ledgers->TRX_TRAN_NO = $insert_voucher_chd;
        $vaucher_ledgers->VOUCHER_NO = $insert_vaucher;
        $vaucher_ledgers->CR_AMT = $data['amount'];
        $vaucher_ledgers->IS_ACTIVE = 1;
        $vaucher_ledgers->CREATED_BY = $user;
        $vaucher_ledgers->save();
        // Account payment mst
        $ac_payment = new AccountPaymentMst();
        $ac_payment->TRX_TRAN_DT = date('Y-m-d', strtotime(str_replace('/', '-', $data['VOUCHER_DT'])));
        $ac_payment->TRAN_AMT = $data['amount'];
        $ac_payment->VOUCHER_NO = $insert_vaucher;
        $ac_payment->TRX_CODE_ID = 4;
        $ac_payment->IS_ACTIVE = 1;
        $ac_payment->CREATED_BY = $user;
        $ac_payment->save();

        $insert_payment = $ac_payment->TRX_TRAN_NO;

        // Account payment mst
        $ac_pay_mod = new AccountPayModAmt();
        $ac_pay_mod->MR_TRAN_DT = date('Y-m-d', strtotime(str_replace('/', '-', $data['VOUCHER_DT'])));
        $ac_pay_mod->TRX_TRAN_NO = $insert_payment;
        $ac_pay_mod->MR_TRAN_AMT = $data['amount'];
        $ac_pay_mod->VOUCHER_NO = $insert_vaucher;
        $ac_pay_mod->PAYMENT_MODE_ID = 1;
        $ac_pay_mod->TRX_CODE_ID = 4;
        $ac_pay_mod->IS_ACTIVE = 1;
        $ac_pay_mod->CREATED_BY = $user;
        $ac_pay_mod->save();

        // leager a DR amount create 5
        $ac_vn_ledgers = new AccountVnLedger();
        $ac_vn_ledgers->VLEDGER_DT = date('Y-m-d', strtotime(str_replace('/', '-', $data['VOUCHER_DT'])));
        $ac_vn_ledgers->TRX_CODE_ID = 5;
        $ac_vn_ledgers->TRX_TRAN_NO = $insert_voucher_chd;
        $ac_vn_ledgers->VOUCHER_NO = $insert_vaucher;
        $ac_vn_ledgers->DR_AMT = $data['amount'];
        $ac_vn_ledgers->IS_ACTIVE = 1;
        $ac_vn_ledgers->CREATED_BY = $user;
        $ac_vn_ledgers->save();

        return $ac_vn_ledgers->VLEDGER_NO;
    }

    /**
     * Get citizen ledgers.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function getLedgers()
    {
        return $this->hasMany('App\Models\AccountVnLedger', 'VOUCHER_NO', 'VOUCHER_NO');
    }

    /**
     * Get applicant data.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function getApplicant()
    {
        return $this->belongsTo('App\Models\TenderApplicant', 'TE_APP_ID', 'TE_APP_ID');
    }

    /**
     * Get applicant data.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function getLeaseDemesne()
    {
        return $this->belongsTo('App\Models\LeaseDemesne', 'LEASE_DE_ID', 'LEASE_DE_ID');
    }

    /**
     * Get applicant Property.
     *
     * @author      Nurullah <nurul@atilimited.net>
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function getApplicantProperty()
    {
        return $this->belongsTo('App\Models\ApplicantProperty', 'TE_APP_ID', 'TE_APP_ID');
    }

    /**
     * Get citizen data.
     *
     * @author      Nurullah <nurul@atilimited.net>
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function getCitizen()
    {
        return $this->belongsTo('App\Models\Citizen', 'CITIZEN_ID', 'CITIZEN_ID');
    }
}
