<?php

namespace App\Models;

use DB;
use Illuminate\Database\Eloquent\Model;

class CitizenApplication extends Model
{
    protected $table = 'sa_citizen_application';
    protected $primaryKey = 'CITIZEN_APP_ID';
    const CREATED_AT = 'CREATED_AT';
    const UPDATED_AT = 'UPDATED_AT';
    protected $fillable = [
                                'CITIZEN_ID',
                                'TENDER_ID',
                                'SCHEDULE_ID',
                                'PROJECT_ID',
                                'PR_DETAIL_ID',
                                'PR_CATEGORY',
                                'PR_TYPE',
                                'IS_TE_SC_BOUGHT',
                                'TE_SC_AMOUNT',
                                'TE_SC_DATE',
                                'BID_AMOUNT',
                                'BID_AMOUNT_TEXT',
                                'BG_AMOUNT',
                                'BG_AMOUNT_TEXT',
                                'BG_PAYMENT_TYPE',
                                'BG_PAYMENT_STATUS',
                                'BANK_ID',
                                'B_DRAFT_NO',
                                'B_DRAFT_DATE',
                                'B_DRAFT_ATTACHMENT',
                                'REMARKS',
                                'INITIAL_REMARKS',
                                'CANCEL_REMARKS',
                                'IS_SELECTED',
                                'IS_LEASE',
                                'IS_CANCEL',
                                'IS_CITIZEN',
                                'IS_VIEW',
                                'IS_ACTIVE',
                                'CREATED_BY',
                                'UPDATED_BY',
                            ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = ['CITIZEN_APP_ID'];

    /**
     * Get tender application data.
     *
     * @param string $month
     * @param string $is_lease
     *
     * @return array
     */
    public static function tenderApplication($month, $is_lease)
    {
        $data = [];
        $date = explode('-', $month);
        $applicant = ApplicantProperty::where('IS_LEASE', $is_lease)
                                    ->whereYear('CREATED_AT', '=', $date[0])
                                    ->whereMonth('CREATED_AT', '=', $date[1])
                                    ->select(DB::raw("DATE_FORMAT(CREATED_AT, '%d') as MD, count(APP_PRO_ID) as TOTAL"))
                                    ->groupBy(DB::raw("DATE_FORMAT(CREATED_AT, '%d-%m-%Y')"))
                                    ->get();
        $applicant = collect($applicant);
        $citizen = self::where('IS_LEASE', $is_lease)
                                    ->whereYear('CREATED_AT', '=', $date[0])
                                    ->whereMonth('CREATED_AT', '=', $date[1])
                                    ->select(DB::raw("DATE_FORMAT(CREATED_AT, '%d') as MD, count(CITIZEN_APP_ID) as TOTAL"))
                                    ->groupBy(DB::raw("DATE_FORMAT(CREATED_AT, '%d-%m-%Y')"))
                                    ->get();
        $citizen = collect($citizen);
        $applications = $applicant->merge($citizen);

        foreach ($applications as $key => $app) {
            $data[] = [
                        'y' => $app->MD,
                        'a' => $app->TOTAL,
                    ];
        }
        if (count($data) == 0) {
            $data[] = [
                        'y' => '',
                        'a' => 0,
                    ];
        }

        return $data;
    }

    /**
     * Get parent applicant.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function getCitizen()
    {
        return $this->belongsTo('App\Models\Citizen', 'CITIZEN_ID', 'CITIZEN_ID');
    }

    /**
     * Get Tender No.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function getTender()
    {
        return $this->belongsTo('App\Models\Tender', 'TENDER_ID', 'TENDER_ID');
    }

    /**
     * Get parent schedule.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function cityCorporationRate()
    {
        return $this->belongsTo('App\Models\TenderSchedule', 'SCHEDULE_ID', 'SCHEDULE_ID');
    }

    /**
     * Get tender schedule.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function getSchedule()
    {
        return $this->belongsTo('App\Models\TenderSchedule', 'SCHEDULE_ID', 'SCHEDULE_ID');
    }

    /**
     * Get parent Project.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function getProject()
    {
        return $this->belongsTo('App\Models\Project', 'PROJECT_ID', 'PROJECT_ID');
    }

    /**
     * Get parent bank.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function getBank()
    {
        return $this->belongsTo('App\Models\Bank', 'BANK_ID', 'BANK_ID');
    }
}
