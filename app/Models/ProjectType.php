<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProjectType extends Model
{
    protected $table = 'sa_type';
    protected $primaryKey = 'PR_TYPE';
    const CREATED_AT = 'CREATED_AT';
    const UPDATED_AT = 'UPDATED_AT';
    //const CREATED_BY = 'CREATED_BY';

    protected $fillable = ['TYPE_NAME', 'TYPE_DESC', 'IS_ACTIVE', 'CREATED_BY', 'UPDATED_BY'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = ['PR_TYPE'];

    /**
     * Project Type data collection.
     *
     * @param $input array
     *
     * @return Collection
     */
    public static function getDataProjectType($input)
    {
        $data = self::where('PR_TYPE', '!=', 0)->select(['PR_TYPE', 'TYPE_NAME', 'TYPE_DESC', 'IS_ACTIVE', 'CREATED_AT'])
                            ->orderBy('PR_TYPE', 'desc')
                            ->take($input['length'])
                            ->skip($input['start'])
                            ->get();

        $total = self::count();

        return ['data' => $data, 'total' => $total, 'filtered' => $total];
    }
}
