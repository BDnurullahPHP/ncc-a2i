<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LeaseValidity extends Model
{
    protected $table = 'lease_validity';
    protected $primaryKey = 'LEASE_VL_ID';
    const CREATED_AT = 'CREATED_AT';
    const UPDATED_AT = 'UPDATED_AT';

    protected $fillable = [
                            'TE_APP_ID',
                            'USER_ID',
                            'SCHEDULE_ID',
                            'DATE_FROM',
                            'DATE_TO',
                            'IS_CALCEL',
                            'CANCEL_REMARKS',
                            'IS_ACTIVE',
                            'CREATED_BY',
                            'UPDATED_BY',
                        ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = ['LEASE_VL_ID'];

    /**
     * Save installment data.
     *
     * @param $data
     * @param $user
     *
     * @return $installment id
     */
    public static function leaseValiditySave($data, $ts, $type, $user, $id)
    {
        $validity = new self();
        if ($type == 'applicant') {
            $validity->TE_APP_ID = $id;
        } else {
            $validity->CITIZEN_ID = $id;
        }
        $validity->SCHEDULE_ID = $data['schedule'];
        $validity->DATE_FROM = date('Y-m-d', strtotime(str_replace('/', '-', $data['from_date'])));
        $validity->DATE_TO = date('Y-m-d', strtotime(str_replace('/', '-', $data['to_date'])));
        $validity->CREATED_BY = $user;
        $validity->save();
        //=======Letter=========
        $letter = new TenderLetter();
        if ($type == 'applicant') {
            $letter->TE_APP_ID = $data['applicant'];
        } else {
            $letter->CITIZEN_ID = $data['applicant'];
        }
        $letter->SCHEDULE_ID = $data['schedule'];
        $letter->LATTER_NUMBER = $data['sharok_no'];
        $letter->DATE = date('Y-m-d', strtotime(str_replace('/', '-', $data['sharok_date'])));
        $letter->SUBJECT = $data['letter_subject'];
        $letter->LETTER_SOURCE = $data['letter_sources'];
        $letter->BODY = $data['letter_body'];
        $letter->APPLICANT_INFO = $data['applicant_info'];
        $letter->OFFICER_INFO = $data['officer_info'];
        $letter->DISTRIBUTION = $data['distribution'];
        $letter->IS_LEASE = 1;
        $letter->CREATED_BY = $user;
        $letter->save();

        //=======Applicant Select=========
        if ($type == 'applicant') {
            $tender_applicant = ApplicantProperty::where('TE_APP_ID', $id)->where('SCHEDULE_ID', $ts)->first();
        } else {
            $tender_applicant = CitizenApplication::where('CITIZEN_ID', $id)->where('SCHEDULE_ID', $ts)->first();
        }
        $tender_applicant->IS_SELECTED = 1;
        $tender_applicant->INITIAL_REMARKS = $data['remarks'];
        $tender_applicant->UPDATED_BY = $user;
        $tender_applicant->save();

        // Create voucher data
        $ac_vaucher_mst = new AccountVaucherMst();
        $ac_vaucher_mst->VOUCHER_DT = date('Y-m-d H:i:s');
        if ($type == 'applicant') {
            $ac_vaucher_mst->TE_APP_ID = $id;
        } else {
            $ac_vaucher_mst->CITIZEN_ID = $id;
        }
        $ac_vaucher_mst->IS_ACTIVE = 1;
        $ac_vaucher_mst->CREATED_BY = $user;
        $ac_vaucher_mst->save();

        $vaucher_mst_id = $ac_vaucher_mst->VOUCHER_NO;
        // start voucher chd
        if ($type == 'applicant') {
            $bid_amount = ApplicantProperty::where('TE_APP_ID', $id)->where('SCHEDULE_ID', $ts)->pluck('BID_AMOUNT');
            $particular_id = TenderParticulars::where('SCHEDULE_ID', $ts)->whereNotIn('PARTICULAR_ID', [6, 7])->get();
        } else {
            $bid_amount = CitizenApplication::where('CITIZEN_ID', $id)->where('SCHEDULE_ID', $ts)->pluck('BID_AMOUNT');
            $particular_id = TenderParticulars::where('SCHEDULE_ID', $ts)->whereNotIn('PARTICULAR_ID', [6, 7])->get();
        }
        foreach ($particular_id as $key => $value) {
            $ac_vaucher_chd = new AccountVaucherChd();
            $ac_vaucher_chd->TRX_TRAN_DT = date('Y-m-d H:i:s');
            $ac_vaucher_chd->VOUCHER_NO = $vaucher_mst_id;
            $ac_vaucher_chd->SCHEDULE_ID = $ts;
            $ac_vaucher_chd->PARTICULAR_ID = $value->PARTICULAR_ID;
            $ac_vaucher_chd->PUNIT_PRICE = $value->UOM == 1 ? $value->PARTICULAR_AMT * $bid_amount / 100 : $value->PARTICULAR_AMT;
            $ac_vaucher_chd->IS_ACTIVE = 1;
            $ac_vaucher_chd->CREATED_BY = $user;
            $ac_vaucher_chd->save();
            $voucher_chd = $ac_vaucher_chd->TRX_TRAN_NO;
            // start ledger
            $vn_ledger = new AccountVnLedger();
            $vn_ledger->VLEDGER_DT = date('Y-m-d H:i:s');
            $vn_ledger->TRX_CODE_ID = 4;
            $vn_ledger->TRX_TRAN_NO = $voucher_chd;
            $vn_ledger->VOUCHER_NO = $vaucher_mst_id;
            $vn_ledger->CR_AMT = $value->UOM == 1 ? $value->PARTICULAR_AMT * $bid_amount / 100 : $value->PARTICULAR_AMT;
            $vn_ledger->IS_ACTIVE = 1;
            $vn_ledger->CREATED_BY = $user;
            $vn_ledger->save();
        }

        return $validity->LEASE_VL_ID;
    }

    /**
     * Create payment voucher data.
     *
     * @param $data
     *
     * @return void
     */
    private static function paymentVoucher($data)
    {
        $ac_vaucher_mst = new AccountVaucherMst();
        $ac_vaucher_mst->VOUCHER_DT = date('Y-m-d H:i:s');
        if (isset($data['user_id'])) {
            $ac_vaucher_mst->USER_ID = $data['user_id'];
        } else {
            $ac_vaucher_mst->TE_APP_ID = $data['applicant_id'];
        }
        $ac_vaucher_mst->IS_ACTIVE = 1;
        $ac_vaucher_mst->save();

        $vaucher_mst_id = $ac_vaucher_mst->VOUCHER_NO;
        $particular_id = Particular::where('TYPE', 3)->pluck('PARTICULAR_ID');

        $ac_vaucher_chd = new AccountVaucherChd();
        $ac_vaucher_chd->TRX_TRAN_DT = date('Y-m-d H:i:s');
        $ac_vaucher_chd->VOUCHER_NO = $vaucher_mst_id;
        $ac_vaucher_chd->PARTICULAR_ID = $particular_id;
        $ac_vaucher_chd->PUNIT_PRICE = $data['amount'];
        $ac_vaucher_chd->IS_ACTIVE = 1;
        $ac_vaucher_chd->save();

        $vn_ledger = new AccountVnLedger();
        $vn_ledger->VLEDGER_DT = date('Y-m-d H:i:s');
        $vn_ledger->TRX_CODE_ID = 4;
        $vn_ledger->VOUCHER_NO = $vaucher_mst_id;
        $vn_ledger->CR_AMT = $data['amount'];
        $vn_ledger->IS_ACTIVE = 1;
        $vn_ledger->save();
    }

    /**
     * Get Schedule.
     *
     * @author      Nurullah <nurul@atilimited.net>
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function getSchedule()
    {
        return $this->belongsTo('App\Models\TenderSchedule', 'SCHEDULE_ID', 'SCHEDULE_ID');
    }
}
