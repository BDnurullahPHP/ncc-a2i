<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Division extends Model
{
    protected $table = 'sa_divisions';
    protected $primaryKey = 'DIVISION_ID';
    const CREATED_AT = 'CREATED_AT';
    const UPDATED_AT = 'UPDATED_AT';
    protected $fillable = [
                                'DIVISION_ENAME',
                                'DIVISION_LNAME',
                                'ORDER_SL',
                                'UD_DIVISION_CODE',
                                'IS_ACTIVE',
                                'CREATED_BY',
                                'UPDATED_BY',
                            ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = ['DIVISION_ID'];

    /**
     * Mouza data collection.
     *
     * @param $input array
     *
     * @return Collection
     */
    public static function getData($input)
    {
        $data = Mouza::with(['getWard'])
                ->select(['MOUZA_ID', 'WARD_ID', 'MOUZA_NAME', 'MOUZA_NAME_BN', 'JL_NO', 'IS_ACTIVE', 'CREATED_AT'])
                ->take($input['length'])
                ->skip($input['start'])
                ->get();

        $total = Mouza::count();

        return ['data' => $data, 'total' => $total, 'filtered' => $total];
    }

    /**
     * Get parent category.
     */
    public function getWard()
    {
        return $this->belongsTo('App\Models\Zone', 'WARD_ID', 'WARD_ID');
    }
}
