<?php

namespace App\Models;

use DB;
use Illuminate\Database\Eloquent\Model;

class Land extends Model
{
    protected $table = 'land';
    protected $primaryKey = 'LAND_ID';
    const CREATED_AT = 'CREATED_AT';
    const UPDATED_AT = 'UPDATED_AT';

    protected $fillable = [
                            'LAND_NUMBER',
                            'LAND_TITLE',
                            'LAND_OWNER',
                            'AREA',
                            'WARD_ID',
                            'MOUZA_ID',
                            'OUSTED_LAND',
                            'CASE_DETAILS',
                            'POSSESSION_LAND',
                            'TOTAL_LAND',
                            'DETAILS',
                            'REMARKS',
                            'IS_ACTIVE',
                            'CREATED_BY',
                            'UPDATED_BY',
                        ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = ['LAND_ID'];

    /**
     * Create land data.
     *
     * @param $data array
     * @param $user string
     *
     * @return string
     */
    public static function createData($data, $user, $id)
    {
        if (!empty($id)) {
            $land = self::find($id);
        } else {
            $land = new self();
        }
        $land->LAND_NUMBER = $data['land_no'];
        $land->LAND_OWNER = $data['land_owner'];
        $land->AREA = $data['area'];
        $land->WARD_ID = $data['ward'];
        $land->MOUZA_ID = $data['mouza'];
        $land->OUSTED_LAND = $data['ousted_land'];
        $land->POSSESSION_LAND = $data['possession_land'];
        $land->TOTAL_LAND = $data['total_land'];
        $land->DETAILS = $data['land_details'];
        $land->REMARKS = $data['remarks'];
        $land->IS_ACTIVE = $data['is_active'];
        $land->CREATED_BY = $user;
        $land->save();

        $landId = $land->LAND_ID;

        // Create land dag no table data
        if (isset($data['land_rs_cs']) && count($data['land_rs_cs']) > 0) {
            foreach ($data['land_rs_cs'] as $key => $land_rs_cs) {
                if (count($land_rs_cs) > 0) {
                    foreach ($land_rs_cs as $index => $val) {
                        if (isset($val['dag_id']) && !empty($val['dag_id'])) {
                            $land_dag = LandDag::find($val['dag_id']);
                        } else {
                            $land_dag = new LandDag();
                        }
                        $land_dag->LAND_ID = $landId;
                        $land_dag->TYPE = $val['type'];
                        if (isset($val['rs']) && !empty($val['rs'])) {
                            $land_dag->RS = $val['rs'];
                        }
                        if (isset($val['cs']) && !empty($val['cs'])) {
                            $land_dag->CS = $val['cs'];
                        }
                        if (isset($val['sa']) && !empty($val['sa'])) {
                            $land_dag->SA = $val['sa'];
                        }

                        $land_dag->CREATED_BY = $user;
                        $land_dag->save();
                    }
                }
            }
        }

        // Create land use table data
        if (count($data['land_use']) > 0) {
            foreach ($data['land_use'] as $key => $use) {
                if (isset($use['use_id']) && !empty($use['use_id'])) {
                    $land_use = LandUse::find($use['use_id']);
                } else {
                    $land_use = new LandUse();
                }
                $land_use->LAND_ID = $landId;
                $land_use->LAND_CATEGORY = $use['category'];
                $land_use->LAND_SUBCATEGORY = $use['subcategory'];
                $land_use->USES = $use['total'];
                $land_use->CREATED_BY = $user;
                $land_use->save();
            }
        }

        // Create land document table data and insert document to filesystem
        if (isset($data['document_row']) && count($data['document_row']) > 0) {
            foreach ($data['document_row'] as $key => $document) {
                $land_docs = new LandDocs();

                if (isset($document['bahia'])) {
                    $bahiadolil = $document['bahia'];
                    $bahiadolil_name = uniqid('bahia-');
                    $bahiadolil_ext = $bahiadolil->getClientOriginalExtension();
                    $bahiadolil_ext = strtolower($bahiadolil_ext);
                    $bahiadolil_fullname = $bahiadolil_name.'.'.$bahiadolil_ext;
                    $land_docs->BAYA_DOLIL = $bahiadolil_fullname;
                    $bahiadolil->move(base_path().'/public/upload/land/', $bahiadolil_fullname);
                }

                if (isset($document['dolil'])) {
                    $dolil = $document['dolil'];
                    $dolil_name = uniqid('dolil-');
                    $dolil_ext = $dolil->getClientOriginalExtension();
                    $dolil_ext = strtolower($dolil_ext);
                    $dolil_fullname = $dolil_name.'.'.$dolil_ext;
                    $land_docs->DOLIL = $dolil_fullname;
                    $dolil->move(base_path().'/public/upload/land/', $dolil_fullname);
                }
                if (isset($document['dcr'])) {
                    $dcr = $document['dcr'];
                    $dcr_name = uniqid('dcr-');
                    $dcr_ext = $dcr->getClientOriginalExtension();
                    $dcr_ext = strtolower($dcr_ext);
                    $dcr_fullname = $dcr_name.'.'.$dcr_ext;
                    $land_docs->DCR = $dcr_fullname;
                    $dcr->move(base_path().'/public/upload/land/', $dcr_fullname);
                }

                if (isset($document['ledger'])) {
                    $ledger = $document['ledger'];
                    $ledger_name = uniqid('ledger-');
                    $ledger_ext = $ledger->getClientOriginalExtension();
                    $ledger_ext = strtolower($ledger_ext);
                    $ledger_fullname = $ledger_name.'.'.$ledger_ext;
                    $land_docs->LEDGER = $ledger_fullname;
                    $ledger->move(base_path().'/public/upload/land/', $ledger_fullname);
                }

                $land_docs->LAND_ID = $landId;
                $land_docs->CREATED_BY = $user;
                $ldocs = $land_docs->save();
            }
        }

        // Create land tax table data and insert tax receipt to filesystem
        if (isset($data['land_tax']) && count($data['land_tax']) > 0) {
            foreach ($data['land_tax'] as $key => $tax) {
                if (isset($tax['tax_id']) && $tax['tax_id'] != '') {
                    $land_tax = LandTax::find($tax['tax_id']);
                    $land_tax->UPDATED_BY = $user;
                } else {
                    $land_tax = new LandTax();
                    $land_tax->CREATED_BY = $user;
                }

                if (isset($tax['doc']) && isset($tax['date'])) {
                    $land_tax_doc = $tax['doc'];
                    $land_tax_doc_name = uniqid('tax-');
                    $land_tax_doc_ext = $land_tax_doc->getClientOriginalExtension();
                    $land_tax_doc_ext = strtolower($land_tax_doc_ext);
                    $land_tax_doc_fullname = $land_tax_doc_name.'.'.$land_tax_doc_ext;
                    $land_tax_doc->move(base_path().'/public/upload/land/', $land_tax_doc_fullname);

                    $land_tax->RECEIPT = $land_tax_doc_name;
                    $land_tax->FILE_TYPE = $land_tax_doc_ext;

                    $land_tax->LAND_ID = $landId;
                    $land_tax->DATE = date('Y-m-d', strtotime(str_replace('/', '-', $tax['date'])));
                    $ltax = $land_tax->save();
                }
            }
        }

        // Create land tax table data and insert tax receipt to filesystem
        if ($data['is_case'] != 0) {
            if (isset($data['case_id']) && !empty($data['case_id'])) {
                $land_case = LandCase::find($data['case_id']);
            } else {
                $land_case = new LandCase();
            }
            $land_case->LAND_ID = $landId;
            $land_case->CASE_NUMBER = $data['case_number'];
            $land_case->COMPLAINANT = $data['case_complainant'];
            $land_case->DEFENDANT = $data['case_defendant'];
            $land_case->SETTLEMENT_DATE = date('Y-m-d', strtotime(str_replace('/', '-', $data['settlement_date'])));
            $land_case->REMARKS = $data['case_remarks'];
            $land_case->IS_ACTIVE = 1;
            $land_case->CREATED_BY = $user;

            if (isset($data['court_document'])) {
                $case_file = $data['court_document'];
                $case_file_name = uniqid('court-');
                $case_file_ext = $case_file->getClientOriginalExtension();
                $case_file_ext = strtolower($case_file_ext);
                $case_file_fullname = $case_file_name.'.'.$case_file_ext;
                $case_file->move(base_path().'/public/upload/land/', $case_file_fullname);

                $land_case->COURT_DOCUMENT = $case_file_fullname;
            }

            $land_case->save();
        } else {
            //if ( isset($data['case_id']) && !empty($data['case_id']) ) {
            $land_case = LandCase::where('LAND_ID', $id);
            if (!empty($land_case)) {
                if (!empty($land_case->COURT_DOCUMENT)) {
                    if (file_exists(public_path().'/upload/land/'.$land_case->COURT_DOCUMENT)) {
                        unlink(public_path().'/upload/land/'.$land_case->COURT_DOCUMENT);
                    }
                }
                $land_case->delete();
            }
        }

        return $landId;
    }

    /**
     * Get Project edit land.
     *
     * @param string $id
     *
     * @return Collection
     */
    public static function getProjectEditLands($id)
    {
        return DB::select(DB::raw("SELECT l.LAND_ID, l.LAND_NUMBER FROM land AS l LEFT JOIN project_land AS pl ON l.LAND_ID = pl.LAND_ID WHERE (pl.LAND_ID is null or  pl.PROJECT_ID = $id)"));
    }

    /**
     * Get Lease edit land.
     *
     * @param string $id
     *
     * @return Collection
     */
    public static function getLeaseEditLands($id)
    {
        return DB::select(DB::raw("SELECT l.LAND_ID, l.LAND_NUMBER FROM land AS l LEFT JOIN project_land AS pl ON l.LAND_ID = pl.LAND_ID WHERE (pl.LAND_ID is null or  pl.PROJECT_ID = $id)"));
    }

    /**
     * land data collection.
     *
     * @param array $input
     *
     * @return Collection
     */
    public static function getData($input)
    {
        $data = self::with(['getArea', 'getWord', 'getMouza'])
                    ->orderBy('LAND_ID', 'desc')
                    ->take($input['length'])
                    ->skip($input['start'])
                    ->get();

        $total = self::count();

        return ['data' => $data, 'total' => $total, 'filtered' => $total];
    }

    /**
     * Get land report data.
     *
     * @param object $input
     *
     * @return array
     */
    public static function reportData($input)
    {
        $search = json_decode($input['search']);

        $land = self::with(['getArea', 'getWord', 'getMouza'])
                    ->leftJoin('land_uses', 'land_uses.LAND_ID', '=', 'land.LAND_ID');

        if (!empty($search->is_case)) {
            if ($search->is_case == 'yes') {
                $land->join('land_case', 'land_case.LAND_ID', '=', 'land.LAND_ID');
            } else {
                $land->leftJoin('land_case', 'land_case.LAND_ID', '=', 'land.LAND_ID');
            }
        }

        $land->select('land.*');

        if (!empty($search->area)) {
            $land->where('land.AREA', '=', $search->area);
        }
        if (!empty($search->ward)) {
            $land->where('land.WARD_ID', '=', $search->ward);
        }
        if (!empty($search->mouza)) {
            $land->where('land.MOUZA_ID', $search->mouza);
        }
        if (!empty($search->land_owner)) {
            $land->where('land.LAND_OWNER', $search->land_owner);
        }
        if (!empty($search->land_number)) {
            $land->where('land.LAND_NUMBER', 'like', '%'.$search->land_number.'%');
        }
        if (!empty($search->category)) {
            $land->where('land_uses.LAND_CATEGORY', $search->category);
        }
        if (!empty($search->subcategory)) {
            $land->where('land_uses.LAND_SUBCATEGORY', $search->subcategory);
        }
        if (!empty($search->is_case) && $search->is_case == 'no') {
            $land->whereNull('land_case.LAND_ID');
        }

        $land->groupBy(['land.LAND_ID']);

        $total = count($land->get());
        $data = $land->take($input['length'])
                        ->skip($input['start'])
                        ->get();

        return ['data' => $data, 'total' => $total, 'filtered' => $total];
    }

    /**
     * Get land report data.
     *
     *@Nurullah
     *
     * @param object $input
     *
     * @return array
     */
    public static function reportDataPrint($input)
    {
        $land = self::with(['getArea', 'getWord', 'getMouza'])
                    ->leftJoin('land_uses', 'land_uses.LAND_ID', '=', 'land.LAND_ID');

        if (!empty($input['is_case'])) {
            if ($input['is_case'] == 'yes') {
                $land->join('land_case', 'land_case.LAND_ID', '=', 'land.LAND_ID');
            } else {
                $land->leftJoin('land_case', 'land_case.LAND_ID', '=', 'land.LAND_ID');
            }
        }
        $land->select('land.*');

        if (!empty($input['area'])) {
            $land->where('land.AREA', '=', $input['area']);
        }
        if (!empty($input['ward'])) {
            $land->where('land.WARD_ID', '=', $input['ward']);
        }
        if (!empty($input['mouza'])) {
            $land->where('land.MOUZA_ID', $input['mouza']);
        }
        if (!empty($input['land_owner'])) {
            $land->where('land.LAND_OWNER', $input['land_owner']);
        }
        if (!empty($input['land_number'])) {
            $land->where('land.LAND_NUMBER', 'like', '%'.$input['land_number'].'%');
        }
        if (!empty($input['category'])) {
            $land->where('land_uses.LAND_CATEGORY', $input['category']);
        }
        if (!empty($input['subcategory'])) {
            $land->where('land_uses.LAND_SUBCATEGORY', $input['subcategory']);
        }
        if (!empty($input['is_case']) && $input['is_case'] == 'no') {
            $land->whereNull('land_case.LAND_ID');
        }

        return $land->groupBy(['land.LAND_ID'])->get();
    }

    /**
     * Get land case data.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function landDagNo()
    {
        return $this->hasMany('App\Models\LandDag', 'LAND_ID', 'LAND_ID');
    }

    /**
     * Get land case data.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function landCase()
    {
        return $this->belongsTo('App\Models\LandCase', 'LAND_ID', 'LAND_ID');
    }

    /**
     * Get land document data.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function landDoc()
    {
        return $this->belongsTo('App\Models\LandDocs', 'LAND_ID', 'LAND_ID');
    }

    /**
     * Get land tax data.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function landTax()
    {
        return $this->belongsTo('App\Models\LandTax', 'LAND_ID', 'LAND_ID');
    }

    /**
     * Get land use data.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function landUses()
    {
        return $this->belongsTo('App\Models\LandUse', 'LAND_ID', 'LAND_ID');
    }

    /**
     * Get parent area.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function getArea()
    {
        return $this->belongsTo('App\Models\Zone', 'AREA', 'WARD_ID');
    }

    /**
     * Get parent ward.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function getWord()
    {
        return $this->belongsTo('App\Models\Zone', 'WARD_ID', 'WARD_ID');
    }

    /**
     * Get parent mouza.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function getMouza()
    {
        return $this->belongsTo('App\Models\Mouza', 'MOUZA_ID', 'MOUZA_ID');
    }

    /**
     * Get land parent category.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function getCaregory()
    {
        return $this->belongsTo('App\Models\LandCategory', 'LAND_CATEGORY', 'CAT_ID');
    }

    /**
     * Get parent subcategory.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function getSubCaregory()
    {
        return $this->belongsTo('App\Models\LandCategory', 'LAND_SUBCATEGORY', 'CAT_ID');
    }
}
