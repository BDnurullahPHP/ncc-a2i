<?php

namespace App\Models;

use App\Ncc\Helpers;
use App\Role;
use App\User;
use Illuminate\Database\Eloquent\Model;

class TenderInstallment extends Model
{
    protected $table = 'tender_installment';
    protected $primaryKey = 'TENDER_INS_ID';
    const CREATED_AT = 'CREATED_AT';
    const UPDATED_AT = 'UPDATED_AT';

    protected $fillable = [
                            'TE_APP_ID',
                            'CITIZEN_ID',
                            'TRX_TRAN_NO',
                            'SCHEDULE_ID',
                            'INSTALLMENT',
                            'AMOUNT',
                            'PUNISHMENT',
                            'DATE',
                            'IS_PAID',
                            'IS_APPROVED',
                            'REMARKS',
                            'IS_ACTIVE',
                            'CREATED_BY',
                            'UPDATED_BY',
                        ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = ['TENDER_INS_ID'];

    /**
     * Save installment data and create voucher.
     *
     * @param $data
     * @param $user
     *
     * @return $installment id
     */
    public static function saveData($data, $user, $ts, $type, $id)
    {
        // Update applicant / citizen property
        if ($type == 'applicant') {
            $tender_applicant = ApplicantProperty::where('TE_APP_ID', $id)->where('SCHEDULE_ID', $ts)->first();
        } else {
            $tender_applicant = CitizenApplication::where('CITIZEN_ID', $id)->where('SCHEDULE_ID', $ts)->first();
        }
        $tender_applicant->IS_SELECTED = 1;
        $tender_applicant->INITIAL_REMARKS = $data['remarks'];
        $tender_applicant->UPDATED_BY = $user;
        $tender_applicant->save();

        // Create voucher data
        $ac_vaucher_mst = new AccountVaucherMst();
        $ac_vaucher_mst->VOUCHER_DT = date('Y-m-d H:i:s');
        if ($type == 'applicant') {
            $ac_vaucher_mst->TE_APP_ID = $id;
        } else {
            $ac_vaucher_mst->CITIZEN_ID = $id;
        }
        $ac_vaucher_mst->IS_ACTIVE = 1;
        $ac_vaucher_mst->CREATED_BY = $user;
        $ac_vaucher_mst->save();

        $vaucher_mst_id = $ac_vaucher_mst->VOUCHER_NO;

        // Save Installment
        foreach ($data['installment'] as $key => $value) {
            $ac_vaucher_chd = new AccountVaucherChd();
            $ac_vaucher_chd->TRX_TRAN_DT = date('Y-m-d H:i:s');
            $ac_vaucher_chd->VOUCHER_NO = $vaucher_mst_id;
            $ac_vaucher_chd->SCHEDULE_ID = $ts;
            $ac_vaucher_chd->PARTICULAR_ID = 8;
            $ac_vaucher_chd->PUNIT_PRICE = $value['amount'];
            $ac_vaucher_chd->IS_ACTIVE = 1;
            $ac_vaucher_chd->CREATED_BY = $user;
            $ac_vaucher_chd->save();

            $voucher_chd = $ac_vaucher_chd->TRX_TRAN_NO;
            // start ledger
            $vn_ledger = new AccountVnLedger();
            $vn_ledger->VLEDGER_DT = date('Y-m-d H:i:s');
            $vn_ledger->TRX_CODE_ID = 4;
            $vn_ledger->TRX_TRAN_NO = $voucher_chd;
            $vn_ledger->VOUCHER_NO = $vaucher_mst_id;
            $vn_ledger->CR_AMT = $value['amount'];
            $vn_ledger->IS_ACTIVE = 1;
            $vn_ledger->CREATED_BY = $user;
            $vn_ledger->save();

            // Save installment
            $installment = new self();
            if ($type == 'applicant') {
                $installment->TE_APP_ID = $id;
            } else {
                $installment->CITIZEN_ID = $id;
            }
            $installment->TRX_TRAN_NO = $voucher_chd;
            $installment->SCHEDULE_ID = $ts;
            $installment->INSTALLMENT = $value['number'];
            $installment->AMOUNT = $value['amount'];
            $installment->PUNISHMENT = $data['punishment'];
            $installment->DATE = date('Y-m-d', strtotime(str_replace('/', '-', $value['date'])));
            $installment->CREATED_BY = $user;
            $installment->save();
        }

        //Save Primary select Letter
        $letter = new TenderLetter();
        if ($type == 'applicant') {
            $letter->TE_APP_ID = $id;
        } else {
            $letter->CITIZEN_ID = $id;
        }
        $letter->SCHEDULE_ID = $ts;
        $letter->LATTER_NUMBER = $data['sharok_no'];
        $letter->DATE = date('Y-m-d', strtotime(str_replace('/', '-', $data['sharok_date'])));
        $letter->SUBJECT = $data['letter_subject'];
        $letter->LETTER_SOURCE = $data['letter_sources'];
        $letter->BODY = $data['letter_body'];
        $letter->APPLICANT_INFO = $data['applicant_info'];
        $letter->OFFICER_INFO = $data['officer_info'];
        $letter->DISTRIBUTION = $data['distribution'];
        $letter->save();

        return $installment->TENDER_INS_ID;
    }

    /**
     * Save installment data.
     *
     * @param $data
     * @param $user
     *
     * @return $installment id
     */
    public static function leaseSaveData($data, $user, $id)
    {
        $installment = new self();
        $installment->TE_APP_ID = $data['applicant_id'];
        $installment->SCHEDULE_ID = $data['schedule_id'];
        $installment->INSTALLMENT = $data['installment'];
        $installment->AMOUNT = $data['amount'];
        $installment->PUNISHMENT = $data['punishment'];
        $installment->DATE = date('Y-m-d', strtotime(str_replace('/', '-', $data['date'])));
        $installment->IS_ACTIVE = 1;
        $installment->CREATED_BY = $user;
        $installment->save();

        $tender_applicant = TenderApplicant::find($id);
        $tender_applicant->IS_SELECTED = 1;
        $tender_applicant->INITIAL_REMARKS = $data['REMARKS'];
        $tender_applicant->UPDATED_BY = $user;
        $tender_applicant->save();

        // Create voucher data
        self::paymentVoucher($data);

        return $installment->TENDER_INS_ID;
    }

    /**
     * Update installment data.
     *
     * @param $data
     * @param $user
     *
     * @return $installment id
     */
    public static function updateData($data, $user, $id)
    {
        foreach ($data['installment'] as $key => $value) {
            $installment = self::find($value['ins_id']);
            //$installment->PUNISHMENT   = $value['penalty'];
            $installment->REMARKS = $value['remarks'];
            $installment->UPDATED_BY = $user;
            $installment->save();
        }

        return $installment->TENDER_INS_ID;
    }

    /**
     * Save Primary applicant selected letters.
     *
     * @param $data
     * @param $user
     *
     * @return $installment id
     */
//    public static function primaryLetterSave($data, $user, $id)
//    {
//        $tender_letter = TenderLetter::where('TE_APP_ID', $id)->first();
//        if (!empty($tender_letter)) {
//            $letter                 = TenderLetter::find($tender_letter->TENDER_LETTER_ID);
//            $letter->UPDATED_BY     = $user;
//        } else {
//            $letter                 = new TenderLetter();
//            $letter->CREATED_BY     = $user;
//        }
//        $letter->TE_APP_ID      = $data['te_app_id'];
//        $letter->SCHEDULE_ID    = $data['schedule_id'];
//        $letter->LATTER_NUMBER  = $data['sharok_no'];
//        $letter->DATE           = date('Y-m-d', strtotime(str_replace('/','-',$data['sharok_date'])));
//        $letter->SUBJECT        = $data['letter_subject'];
//        $letter->BODY           = $data['letter_body'];
//        $letter->APPLICANT_INFO = $data['applicant_info'];
//        $letter->OFFICER_INFO   = $data['officer_info'];
//        $letter->DISTRIBUTION   = $data['distribution'];
//        $letter->save();
//
//        return $letter->TENDER_LETTER_ID;
//    }

    /**
     * Approved installment amount and create a permanent user.
     *
     * @param $id
     * @param $user
     *
     * @return number
     */
    public static function approveInstallment($type, $id, $user)
    {
        $installment = self::find($id);
        $installment->IS_APPROVED = 1;
        $installment->UPDATED_AT = $user;
        $installment->save();

        $installment_exist = self::where('TE_APP_ID', $installment->TE_APP_ID)
                                              ->where('IS_PAID', 1)
                                              ->where('IS_APPROVED', 0)
                                              ->first();

        if (empty($installment_exist)) {
            $tender_applicant = TenderApplicant::where('TE_APP_ID', $installment->TE_APP_ID)->first();
            $tender_schedule = TenderSchedule::where('SCHEDULE_ID', $tender_applicant->SCHEDULE_ID)->first();

            $user_name = explode('@', $tender_applicant->EMAIL);
            $email = $tender_applicant->EMAIL;
            $user_exist = User::where('USERNAME', $user_name[0])->first();
            $email_exist = User::where('EMAIL', $email)->first();

            if (!empty($email_exist)) {
                $user_name[0] = $user_name[0].'_'.Helpers::random_str(5);
            }
            if (!empty($user_exist)) {
                $email = $user_name[0].'_'.Helpers::random_str(5).'@'.$user_name[1];
            }

            $role = Role::where('NAME', 'citizen')->pluck('ROLE_ID');

            $user = new User();
            $user->USER_TYPE = 'C';
            $user->FIRST_NAME = $tender_applicant->FIRST_NAME;
            $user->LAST_NAME = $tender_applicant->LAST_NAME;
            $user->FULL_NAME = $tender_applicant->APPLICANT_NAME;
            $user->GENDER = 'm';
            $user->USERNAME = $user_name[0];
            $user->EMAIL = $email;
            $user->PASSWORD = $tender_applicant->PASSWORD;
            $user->MOBILE_NO = $tender_applicant->APPLICANT_PHONE;
            $user->IS_ACTIVE = 1;
            $user->save();
            $user->attachRole($role);

            $user_id = $user->USER_ID;

            $citizen = new Citizen();
            $citizen->USER_ID = $user_id;
            $citizen->TE_APP_ID = $installment->TE_APP_ID;
            $citizen->SPOUSE_NAME = $tender_applicant->SPOUSE_NAME;
            $citizen->NID = $tender_applicant->NID;
            $citizen->DISTRICT_ID = $tender_applicant->DISTRICT_ID;
            $citizen->THANA_ID = $tender_applicant->THANA_ID;
            $citizen->ROAD_NO = $tender_applicant->ROAD_NO;
            $citizen->HOLDING_NO = $tender_applicant->HOLDING_NO;
            $citizen->CREATED_BY = $user;
            $citizen->IS_ACTIVE = 1;
            $citizen->save();

            $citizen_property = new CitizenProperty();
            $citizen_property->CITIZEN_ID = $citizen->CITIZEN_ID;
            $citizen_property->TENDER_ID = $tender_applicant->TENDER_ID;
            $citizen_property->SCHEDULE_ID = $tender_applicant->SCHEDULE_ID;
            $citizen_property->PROJECT_ID = $tender_schedule->PROJECT_ID;
            $citizen_property->PR_DETAIL_ID = $tender_applicant->PR_DETAIL_ID;
            $citizen_property->PR_CATEGORY = $tender_applicant->PR_CATEGORY;
            $citizen_property->PR_TYPE = $tender_applicant->PR_TYPE;
            $citizen_property->IS_LEASE = $tender_applicant->IS_LEASE;
            $citizen_property->CREATED_BY = $user;
            $citizen_property->IS_ACTIVE = 1;
            $citizen_property->save();

            if ($tender_applicant->IS_LEASE == 0) {
                // Create voucher data
                $project_id = ProjectDetails::where('PR_DETAIL_ID', $tender_applicant->PR_DETAIL_ID)
                                            ->pluck('PROJECT_ID');
                $data['user_id'] = $user->USER_ID;
                $data['amount'] = TenderParticulars::where('SCHEDULE_ID', $tender_applicant->SCHEDULE_ID)
                                                        ->where('PARTICULAR_ID', 5)
                                                        ->pluck('PARTICULAR_AMT');
                // Create voucher data
                $ac_vaucher_mst = new AccountVaucherMst();
                $ac_vaucher_mst->VOUCHER_DT = date('Y-m-d H:i:s');
                if (isset($data['user_id'])) {
                    $ac_vaucher_mst->USER_ID = $data['user_id'];
                } else {
                    $ac_vaucher_mst->TE_APP_ID = $data['applicant_id'];
                }
                $ac_vaucher_mst->IS_ACTIVE = 1;
                $ac_vaucher_mst->save();

                $vaucher_mst_id = $ac_vaucher_mst->VOUCHER_NO;
                $particular_id = Particular::where('TYPE', 3)->pluck('PARTICULAR_ID');

                $ac_vaucher_chd = new AccountVaucherChd();
                $ac_vaucher_chd->TRX_TRAN_DT = date('Y-m-d H:i:s');
                $ac_vaucher_chd->VOUCHER_NO = $vaucher_mst_id;
                $ac_vaucher_chd->PROJECT_ID = $project_id;
                $ac_vaucher_chd->PR_DETAIL_ID = $tender_applicant->PR_DETAIL_ID;
                $ac_vaucher_chd->PR_CATEGORY = $tender_applicant->PR_CATEGORY;
                $ac_vaucher_chd->PARTICULAR_ID = $particular_id;
                $ac_vaucher_chd->PUNIT_PRICE = $data['amount'];
                $ac_vaucher_chd->IS_ACTIVE = 1;
                $ac_vaucher_chd->save();

                $vn_ledger = new AccountVnLedger();
                $vn_ledger->VLEDGER_DT = date('Y-m-d H:i:s');
                $vn_ledger->TRX_CODE_ID = 4;
                $vn_ledger->VOUCHER_NO = $vaucher_mst_id;
                $vn_ledger->CR_AMT = $data['amount'];
                $vn_ledger->IS_ACTIVE = 1;
                $vn_ledger->save();
            }

            $applicant = TenderApplicant::find($installment->TE_APP_ID);
            $applicant->IS_SELECTED = 2;
            $applicant->IS_ACTIVE = 0;
            $applicant->save();
        }

        return $installment->TENDER_INS_ID;
    }

    /**
     * Create payment voucher data.
     *
     * @param $data
     *
     * @return void
     */
    private static function paymentVoucher($data)
    {
        $ac_vaucher_mst = new AccountVaucherMst();
        $ac_vaucher_mst->VOUCHER_DT = date('Y-m-d H:i:s');
        if (isset($data['user_id'])) {
            $ac_vaucher_mst->USER_ID = $data['user_id'];
        } else {
            $ac_vaucher_mst->TE_APP_ID = $data['applicant_id'];
        }
        $ac_vaucher_mst->IS_ACTIVE = 1;
        $ac_vaucher_mst->save();

        $vaucher_mst_id = $ac_vaucher_mst->VOUCHER_NO;
        $particular_id = Particular::where('TYPE', 3)->pluck('PARTICULAR_ID');

        $ac_vaucher_chd = new AccountVaucherChd();
        $ac_vaucher_chd->TRX_TRAN_DT = date('Y-m-d H:i:s');
        $ac_vaucher_chd->VOUCHER_NO = $vaucher_mst_id;
        $ac_vaucher_chd->PARTICULAR_ID = $particular_id;
        $ac_vaucher_chd->PUNIT_PRICE = $data['amount'];
        $ac_vaucher_chd->IS_ACTIVE = 1;
        $ac_vaucher_chd->save();

        $vn_ledger = new AccountVnLedger();
        $vn_ledger->VLEDGER_DT = date('Y-m-d H:i:s');
        $vn_ledger->TRX_CODE_ID = 4;
        $vn_ledger->VOUCHER_NO = $vaucher_mst_id;
        $vn_ledger->CR_AMT = $data['amount'];
        $vn_ledger->IS_ACTIVE = 1;
        $vn_ledger->save();
    }

    /**
     * Tender installment data collection.
     *
     * @param $input array
     *
     * @return Collection
     */
    public static function getPendingData($input)
    {
        $data = self::with(['getApplicant', 'getSchedule'])
                            ->where('IS_PAID', 1)
                            ->where('IS_APPROVED', 0)
                            ->orderBy('TENDER_INS_ID', 'desc')
                            ->take($input['length'])
                            ->skip($input['start'])
                            ->get();

        $total = self::where('IS_PAID', 1)->where('IS_APPROVED', 0)->count();

        return ['data' => $data, 'total' => $total, 'filtered' => $total];
    }

    /**
     * Get tender applicant.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function getApplicant()
    {
        return $this->belongsTo('App\Models\TenderApplicant', 'TE_APP_ID', 'TE_APP_ID');
    }

    /**
     * Get tender schedule.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function getSchedule()
    {
        return $this->belongsTo('App\Models\TenderSchedule', 'SCHEDULE_ID', 'SCHEDULE_ID');
    }

    /**
     * Get voucher child.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function getVoucherChd()
    {
        return $this->belongsTo('App\Models\AccountVaucherChd', 'TRX_TRAN_NO', 'TRX_TRAN_NO');
    }
}
