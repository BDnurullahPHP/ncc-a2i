<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TenderParticulars extends Model
{
    protected $table = 'tender_particulars';
    protected $primaryKey = 'TE_PART_ID';
    const CREATED_AT = 'CREATED_AT';
    const UPDATED_AT = 'UPDATED_AT';

    protected $fillable = [
                                'SCHEDULE_ID',
                                'PARTICULAR_ID',
                                'PARTICULAR_AMT',
                                'UOM',
                                'PARTICULAR_SL',
                                'CURRENT_SL',
                                'REMARKS',
                                'IS_ACTIVE',
                                'CREATED_BY',
                                'UPDATED_BY',
                            ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = ['TE_PART_ID'];

    /**
     * Get parent particular.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function getParticular()
    {
        return $this->belongsTo('App\Models\Particular', 'PARTICULAR_ID', 'PARTICULAR_ID');
    }

    /**
     * Get parent schedule.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function getSchedule()
    {
        return $this->belongsTo('App\Models\TenderSchedule', 'SCHEDULE_ID', 'SCHEDULE_ID');
    }
}
