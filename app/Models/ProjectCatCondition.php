<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProjectCatCondition extends Model
{
    protected $table = 'sa_category_conditions';
    protected $primaryKey = 'CATE_CON_ID';
    const CREATED_AT = 'CREATED_AT';
    const UPDATED_AT = 'UPDATED_AT';
    //const CREATED_BY = 'CREATED_BY';

    protected $fillable = ['CATE_CON_ID', 'PR_CATEGORY', 'CON_TITLE', 'CON_DESC', 'IS_ACTIVE', 'CREATED_BY', 'UPDATED_BY'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = ['CATE_CON_ID'];

    /**
     * Project category condition data collection.
     *
     * @param $input array
     *
     * @return Collection
     */
    public static function getDataProjectCatCondition1($input)
    {
        $data = ProjectType::where('PR_TYPE', '!=', 0)->select(['PR_TYPE', 'TYPE_NAME', 'TYPE_DESC', 'IS_ACTIVE', 'CREATED_AT'])
                           ->orderBy('PR_TYPE', 'desc')
                           ->take($input['length'])
                           ->skip($input['start'])
                           ->get();

        $total = ProjectType::where('PR_TYPE', '!=', 0)->count();

        return ['data' => $data, 'total' => $total, 'filtered' => $total];
    }

    public static function getDataProjectCatCondition($input)
    {
        $data = self::with(['getCategory'])
                                  ->select(['CATE_CON_ID', 'PR_CATEGORY', 'CON_TITLE', 'CON_DESC', 'IS_ACTIVE', 'CREATED_AT'])
                                  ->orderBy('CATE_CON_ID', 'desc')
                                  ->take($input['length'])
                                  ->skip($input['start'])
                                  ->get();

        $total = self::count();

        return ['data' => $data, 'total' => $total, 'filtered' => $total];
    }

    /**
     * Get project category name.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function getCategory()
    {
        return $this->belongsTo('App\Models\ProjectCategory', 'PR_CATEGORY', 'PR_CATEGORY');
    }
}
