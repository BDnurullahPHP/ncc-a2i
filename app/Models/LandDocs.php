<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LandDocs extends Model
{
    protected $table = 'land_docs';
    protected $primaryKey = 'DOC_ID';
    const CREATED_AT = 'CREATED_AT';
    const UPDATED_AT = 'UPDATED_AT';
    protected $fillable = [
                                'LAND_ID',
                                'BAYA_DOLIL',
                                'DOLIL',
                                'DCR',
                                'LEDGER',
                                'CREATED_BY',
                                'UPDATED_BY',
                            ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = ['DOC_ID'];

    /**
     * Get land land document data by land id.
     *
     * @param $id string
     *
     * @return array
     */
    public static function docTypedata($id)
    {
        $dolil_cat = [];
        $land_dolil = self::where('LAND_ID', $id)->get();

        if (count($land_dolil) > 0) {
            foreach ($land_dolil as $key => $ld) {
                $dolil_cat['dolil']['NAME'] = 'dolil';
                $dolil_cat['dolil']['ATTR_ID'] = 'dolil_multiple';
                $dolil_cat['dolil']['LABEL'] = trans('land.land_form_document');
                $dolil_cat['dolil'][$key]['DOC_ID'] = $ld->DOC_ID;
                $dolil_cat['dolil'][$key]['DOC'] = $ld->DOLIL;

                $dolil_cat['dcr']['NAME'] = 'dcr';
                $dolil_cat['dcr']['ATTR_ID'] = 'dcr_multiple';
                $dolil_cat['dcr']['LABEL'] = trans('land.land_form_dcr');
                $dolil_cat['dcr'][$key]['DOC_ID'] = $ld->DOC_ID;
                $dolil_cat['dcr'][$key]['DOC'] = $ld->DCR;

                $dolil_cat['ledger']['NAME'] = 'ledger';
                $dolil_cat['ledger']['ATTR_ID'] = 'ledger_multiple';
                $dolil_cat['ledger']['LABEL'] = trans('land.land_form_ledger');
                $dolil_cat['ledger'][$key]['DOC_ID'] = $ld->DOC_ID;
                $dolil_cat['ledger'][$key]['DOC'] = $ld->LEDGER;

                $dolil_cat['bahia']['NAME'] = 'bahia';
                $dolil_cat['bahia']['ATTR_ID'] = 'bahia_multiple';
                $dolil_cat['bahia']['LABEL'] = trans('land.land_form_bahia');
                $dolil_cat['bahia'][$key]['DOC_ID'] = $ld->DOC_ID;
                $dolil_cat['bahia'][$key]['DOC'] = $ld->BAYA_DOLIL;
            }
        } else {
            $dolil_cat['dolil']['NAME'] = 'dolil';
            $dolil_cat['dolil']['ATTR_ID'] = 'dolil_multiple';
            $dolil_cat['dolil']['LABEL'] = trans('land.land_form_document');

            $dolil_cat['dcr']['NAME'] = 'dcr';
            $dolil_cat['dcr']['ATTR_ID'] = 'dcr_multiple';
            $dolil_cat['dcr']['LABEL'] = trans('land.land_form_dcr');

            $dolil_cat['ledger']['NAME'] = 'ledger';
            $dolil_cat['ledger']['ATTR_ID'] = 'ledger_multiple';
            $dolil_cat['ledger']['LABEL'] = trans('land.land_form_ledger');

            $dolil_cat['bahia']['NAME'] = 'bahia';
            $dolil_cat['bahia']['ATTR_ID'] = 'bahia_multiple';
            $dolil_cat['bahia']['LABEL'] = trans('land.land_form_bahia');
        }

        return $dolil_cat;
    }
}
