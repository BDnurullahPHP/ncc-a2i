<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Bank extends Model
{
    protected $table = 'sa_bank';
    protected $primaryKey = 'BANK_ID';
    const CREATED_AT = 'CREATED_AT';
    const UPDATED_AT = 'UPDATED_AT';
    protected $fillable = [
                                'B_PARENT_ID',
                                'BANK_NAME',
                                'ADDRESS',
                                'IS_ACTIVE',
                                'CREATED_AT',
                                'CREATED_BY',
                                'UPDATED_AT',
                                'UPDATED_BY',
                            ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = ['BANK_ID'];

    /**
     * Bank Information.
     *
     * @author Jahid Hasan
     *
     * @param $input array
     *
     * @return Collection
     */
    public static function getData($input)
    {
        $data = self::with('parentBank')
                    ->orderBy('BANK_NAME', 'asc')
                    ->take($input['length'])
                    ->skip($input['start'])
                    ->get();
        $total = self::count();

        return ['data' => $data, 'total' => $total, 'filtered' => $total];
    }

    /**
     * Parent Bank Information.
     *
     * @author Jahid Hasan
     *
     * @return Collection
     */
    public function parentBank()
    {
        return $this->hasOne('App\Models\Bank', 'B_PARENT_ID', 'BANK_ID');
    }
}
