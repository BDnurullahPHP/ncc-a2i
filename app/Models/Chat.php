<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Chat extends Model
{
    protected $table = 'sa_chat';
    protected $primaryKey = 'CHAT_ID';
    const CREATED_AT = 'CREATED_AT';
    const UPDATED_AT = 'UPDATED_AT';
    protected $fillable = ['USER_ID', 'MESSAGE', 'IS_ACTIVE', 'CREATED_AT', 'UPDATED_AT'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = ['CHAT_ID'];
}
