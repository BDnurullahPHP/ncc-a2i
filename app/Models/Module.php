<?php

namespace App\Models;

use Auth;
use DB;
use Illuminate\Database\Eloquent\Model;

class Module extends Model
{
    protected $table = 'sa_modules';
    protected $primaryKey = 'MODULE_ID';

    const CREATED_AT = 'CREATED_AT';
    const UPDATED_AT = 'UPDATED_AT';

    protected $fillable = ['MODULE_NAME', 'MODULE_NAME_BN', 'MODULE_ICON', 'SL_NO', 'IS_ACTIVE', 'CREATED_BY', 'UPDATED_BY'];

    /**
     * Save or update Module.
     *
     *@author  Nurullah <nurul@atilimited.net>
     *
     * @param array       $data
     * @param string      $user
     * @param string|null $id
     *
     * @return string
     */
    public static function createModule($data, $user, $id)
    {
        $maxSlNo = DB::table('sa_modules')->max('SL_NO');
        if (!empty($id)) {
            $module = self::find($id);
            $module->IS_ACTIVE = isset($data['IS_ACTIVE']) ? $data['IS_ACTIVE'] : 0;
            $module->UPDATED_BY = $user;
        } else {
            $module = new self();
            $module->SL_NO = $maxSlNo + 1;
            $module->CREATED_BY = $user;
        }

        $module->MODULE_NAME = $data['MODULE_NAME'];
        $module->MODULE_NAME_BN = $data['MODULE_NAME_BN'];
        $module->MODULE_ICON = $data['MODULE_ICON'];

        $module->save();
    }

    /**
     * Module data collection.
     *
     *@author  Nurullah <nurul@atilimited.net>
     *
     * @param $input array
     *
     * @return Collection
     */
    public static function getData($input)
    {
        $data = self::orderBy('SL_NO', 'desc')
                    ->take($input['length'])
                    ->skip($input['start'])
                    ->get();

        $total = self::count();

        return ['data' => $data, 'total' => $total, 'filtered' => $total];
    }

    public static function checkPrevilege($link_url)
    {
        $session_info = Auth::user();
        $org_id = $session_info['ORG_ID'];
        $user_id = $session_info['USER_ID'];
        $org_group = $session_info['USERGRP_ID'];
        $org_group_level = $session_info['USERLVL_ID'];

        $access = ModuleLinkAssignUGLW::where('LINK_URI', $link_url)
                                                                    ->where('ORG_ID', $org_id)
                                                                    ->where('USERGRP_ID', $org_group)
                                                                    ->where('UG_LEVEL_ID', $org_group_level)
                                                                    ->where('USER_ID', $user_id)
                                                                    ->first(['CREATE', 'READ', 'UPDATE', 'DELETE', 'STATUS']);

        return $access;
    }
}
