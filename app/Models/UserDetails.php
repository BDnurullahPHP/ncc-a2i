<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserDetails extends Model
{
    protected $table = 'sa_user_details';
    protected $primaryKey = 'UD_ID';
    const CREATED_AT = 'CREATED_AT';
    const UPDATED_AT = 'UPDATED_AT';
    //public $timestamps    = false;
    protected $fillable = ['USER_ID', 'DESIGNATION', 'ADDRESS', 'IMAGE', 'CONTENT_TYPE'];

    /**
     * User profile information update validation rules.
     *
     * @return array
     */
    public static function profileInfoRules()
    {
        return [
                'first_name'    => 'required|min:2',
                'last_name'     => 'required|min:2',
                'user_name'     => 'required|min:3',
                'phone_no'      => 'min:11',
                'designation'   => 'min:4',
                'address'       => 'min:6',
                'image'         => 'mimes:jpeg,bmp,png',
            ];
    }
}
