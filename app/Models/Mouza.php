<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Mouza extends Model
{
    protected $table = 'sa_mouza';
    protected $primaryKey = 'MOUZA_ID';
    const CREATED_AT = 'CREATED_AT';
    const UPDATED_AT = 'UPDATED_AT';
    protected $fillable = [
                                'WARD_ID',
                                'MOUZA_NAME',
                                'MOUZA_NAME_BN',
                                'JL_NO',
                                'IS_ACTIVE',
                                'CREATED_BY',
                                'UPDATED_BY',
                            ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = ['MOUZA_ID'];

    /**
     * Mouza data collection.
     *
     * @param $input array
     *
     * @return Collection
     */
    public static function getData($input)
    {
        $data = self::with(['getWard'])
                    ->orderBy('MOUZA_ID', 'desc')
                    ->take($input['length'])
                    ->skip($input['start'])
                    ->get();

        $total = self::count();

        return ['data' => $data, 'total' => $total, 'filtered' => $total];
    }

    /**
     * Save or update mouza.
     *
     * @param array       $data
     * @param string      $user
     * @param string|null $id
     *
     * @return string
     */
    public static function saveMouza($data, $user, $id = null)
    {
        if (!empty($id)) {
            $mouza = self::find($id);
            $mouza->UPDATED_BY = $user;
        } else {
            $mouza = new self();
            $mouza->CREATED_BY = $user;
        }
        $mouza->WARD_ID = $data['WARD_ID'];
        $mouza->MOUZA_NAME = $data['MOUZA_NAME'];
        $mouza->MOUZA_NAME_BN = $data['MOUZA_NAME_BN'];
        $mouza->IS_ACTIVE = isset($data['IS_ACTIVE']) ? $data['IS_ACTIVE'] : 0;
        $mouza->save();
    }

    /**
     * Get parent category.
     */
    public function getWard()
    {
        return $this->belongsTo('App\Models\Zone', 'WARD_ID', 'WARD_ID');
    }
}
