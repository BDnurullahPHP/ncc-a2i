<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ModuleLinkAssignUGLW extends Model
{
    protected $table = 'sa_uglw_mlink';
    protected $primaryKey = 'UGLWM_LINK';

    const CREATED_AT = 'CREATED_AT';
    const UPDATED_AT = 'UPDATED_AT';

    protected $fillable = ['ORG_MLINKS_ID', 'ORG_ID', 'USER_ID', 'USERGRP_ID', 'UG_LEVEL_ID', 'MODULE_ID', 'LINK_ID', 'LINK_URI', 'CREATE', 'READ', 'UPDATE', 'DELETE', 'STATUS', 'IS_ACTIVE', 'CREATED_BY', 'UPDATED_BY'];
}
