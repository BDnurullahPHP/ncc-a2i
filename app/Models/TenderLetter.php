<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TenderLetter extends Model
{
    protected $table = 'tender_latter';
    protected $primaryKey = 'TENDER_LETTER_ID';
    const CREATED_AT = 'CREATED_AT';
    const UPDATED_AT = 'UPDATED_AT';

    protected $fillable = [
                            'TE_APP_ID',
                            'CITIZEN_ID',
                            'SCHEDULE_ID',
                            'PARTICULAR_ID',
                            'LATTER_NUMBER',
                            'DATE',
                            'SUBJECT',
                            'LETTER_SOURCE',
                            'BODY',
                            'APPLICANT_INFO',
                            'OFFICER_INFO',
                            'DISTRIBUTION',
                            'TYPE',
                            'IS_LEASE',
                            'IS_ACTIVE',
                            'CREATED_BY',
                            'UPDATED_BY',
                        ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = ['TENDER_LETTER_ID'];

    /**
     * User ownership letter.
     *
     * @param array  $data
     * @param string $uid
     * @param string $utype
     * @param string $sc
     * @param string $type
     * @param string $user
     *
     * @return void
     */
    public static function ownershipLetter($data, $uid, $utype, $sc, $type, $user)
    {
        $letter = new self();
        if ($utype == 'citizen') {
            $letter->CITIZEN_ID = $uid;
        } else {
            $letter->TE_APP_ID = $uid;
        }
        $letter->SCHEDULE_ID = $sc;
        $letter->PARTICULAR_ID = $type == 1 ? 13 : 14;
        $letter->LATTER_NUMBER = $data['sharok_no'];
        $letter->DATE = date('Y-m-d', strtotime(str_replace('/', '-', $data['sharok_date'])));
        $letter->SUBJECT = $data['letter_subject'];
        $letter->BODY = $data['letter_body'];
        $letter->OFFICER_INFO = $data['officer_info'];
        $letter->CREATED_BY = $user;
        $letter->save();
    }
}
