<?php

namespace App\Models;

use App\Ncc\Helpers;
use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    protected $table = 'project';
    protected $primaryKey = 'PROJECT_ID';
    const CREATED_AT = 'CREATED_AT';
    const UPDATED_AT = 'UPDATED_AT';

    protected $fillable = [
                            'PR_UD_ID',
                            'PR_NAME',
                            'PR_NAME_BN',
                            'PR_DESC',
                            'PR_START_DT',
                            'PR_END_DT',
                            'PROJECT_LOCATION',
                            'IS_LEASE',
                            'IS_ACTIVE',
                            'CREATED_BY',
                            'UPDATED_BY',
                        ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = ['PROJECT_ID'];

    /**
     * Project data collection.
     *
     *@author      Nurullah <nurul@atilimited.net>
     *
     * @param $input array
     *
     * @return Collection
     */
    public static function getMarketData($input)
    {
        $data = self::where('IS_LEASE', 0)->orderBy('PROJECT_ID', 'desc')
                        ->take($input['length'])
                        ->skip($input['start'])
                        ->get();

        $total = self::where('IS_LEASE', 0)->count();

        return ['data' => $data, 'total' => $total, 'filtered' => $total];
    }

    /**
     * Create project data.
     *
     * @author      Nurullah <nurul@atilimited.net>
     *
     * @param $data array
     * @param $user string
     *
     * @return string
     */
    public static function createMarketData($data, $user, $type)
    {
        $project_land = explode(',', $data['land']);
        // echo '<pre>'; print_r($project_land);exit;
        $project = new self();
        $project->IS_LEASE = $type;
        $project->PR_UD_ID = $data['number'];
        $project->PR_NAME = $data['name'];
        $project->PR_NAME_BN = $data['name_bn'];
        $project->PR_DESC = $data['details'];
        $project->PR_START_DT = date('Y-m-d', strtotime(str_replace('/', '-', $data['start_date'])));
        $project->PR_END_DT = date('Y-m-d', strtotime(str_replace('/', '-', $data['end_date'])));
        $project->PROJECT_LOCATION = $data['location'];
        $project->IS_ACTIVE = isset($data['is_active']) ? $data['is_active'] : 0;
        $project->CREATED_BY = $user;
        $project->save();

        $projectId = $project->PROJECT_ID;

        // Create project documents
        if (isset($data['project_doc']) && count($data['project_doc']) > 0) {
            foreach ($data['project_doc'] as $key => $doc) {
                $pr_docs = new ProjectDoc();
                $pr_docs->PROJECT_ID = $projectId;
                $project_file = $doc['doc'];
                $project_file_name = uniqid('pro-');
                $project_file_ext = $project_file->getClientOriginalExtension();
                $project_file_ext = strtolower($project_file_ext);
                $project_file_fullname = $project_file_name.'.'.$project_file_ext;
                $pr_docs->ATTACHMENT = $project_file_fullname;
                $project_file->move(base_path().'/public/upload/project/', $project_file_fullname);
                $pr_docs->CREATED_BY = $user;
                $pr_docs->save();
            }
        }

        // Create project Land table data(Project land)
        if ($data['land'] != 'null') {
            foreach ($project_land as $key => $land) {
                $projectLand = new ProjectLand();
                $projectLand->PROJECT_ID = $projectId;
                $projectLand->LAND_ID = $land;
                $projectLand->IS_ACTIVE = 1;
                $projectLand->CREATED_BY = $user;
                $projectLand->save();
            }
        }

        return $projectId;
    }

    /**
     * Update project data.
     *
     * @author      Nurullah <nurul@atilimited.net>
     *
     * @param $data array
     * @param $user string
     * @param $id string
     *
     * @return string
     */
    public static function updateMarketData($data, $user, $id)
    {
        $project_land = explode(',', $data['land']);
        //echo '<pre>'; print_r($project_land);exit;
        $project = self::find($id);
        $project->PR_UD_ID = $data['number'];
        $project->PR_NAME = $data['name'];
        $project->PR_NAME_BN = $data['name_bn'];
        $project->PR_DESC = $data['details'];
        $project->PR_START_DT = date('Y-m-d', strtotime(str_replace('/', '-', $data['start_date'])));
        $project->PR_END_DT = date('Y-m-d', strtotime(str_replace('/', '-', $data['end_date'])));
        $project->PROJECT_LOCATION = $data['location'];
        $project->IS_ACTIVE = $data['is_active'];
        $project->UPDATED_BY = $user;
        $project->save();

        $projectId = $project->PROJECT_ID;

        // Create project documents
        if (isset($data['project_doc']) && count($data['project_doc']) > 0) {
            foreach ($data['project_doc'] as $key => $doc) {
                $pr_docs = new ProjectDoc();
                $pr_docs->PROJECT_ID = $projectId;
                $project_file = $doc['doc'];
                $project_file_name = uniqid('pro-');
                $project_file_ext = $project_file->getClientOriginalExtension();
                $project_file_ext = strtolower($project_file_ext);
                $project_file_fullname = $project_file_name.'.'.$project_file_ext;
                $pr_docs->ATTACHMENT = $project_file_fullname;
                $project_file->move(base_path().'/public/upload/project/', $project_file_fullname);
                $pr_docs->CREATED_BY = $user;
                $pr_docs->save();
            }
        }

        // Update project Land table data(Project land)
        if ($data['land'] != 'null') {
            ProjectLand::where('PROJECT_ID', $id)->delete();
            foreach ($project_land as $key => $land) {
                $projectLand = new ProjectLand();
                $projectLand->PROJECT_ID = $projectId;
                $projectLand->LAND_ID = $land;
                $projectLand->IS_ACTIVE = 1;
                $projectLand->CREATED_BY = $user;
                $projectLand->save();
            }
        }

        return $projectId;
    }

    /**
     * Create project data.
     *
     * @author      Nurullah <nurul@atilimited.net>
     *
     * @param $data array
     * @param $user string
     *
     * @return string
     */
    public static function createMarketDetailsData($data, $user, $type)
    {
        $project_details_number = $data['PR_SSF_NO'];

        // Create project Land table data(Project land)
        foreach ($project_details_number as $key => $number) {
            $projectDetails = new ProjectDetails();
            $projectDetails->PROJECT_ID = $data['project'];
            $projectDetails->PR_CATEGORY = $data['category'];
            $projectDetails->POSITION = $data['POSITION'][$key];
            $projectDetails->PR_TYPE = $data['PR_TYPE'][$key] != '' ? $data['PR_TYPE'][$key] : 10;
            $projectDetails->PR_SSF_NO = Helpers::bn2en($data['PR_SSF_NO'][$key]);
            $projectDetails->PR_MEASURMENT = Helpers::bn2en($data['PR_MEASURMENT'][$key]);
            $projectDetails->UOM = 1;
            $projectDetails->IS_RESERVED = isset($data['IS_RESERVED'][$key]) ? 1 : 0;
            $projectDetails->IS_BUILT = 1;
            $projectDetails->IS_ACTIVE = 1;
            $projectDetails->CREATED_BY = $user;
            $projectDetails->save();
        }

        return $data['project'];
    }

    /**
     * Project data collection.
     *
     *@author      Nurullah <nurul@atilimited.net>
     *
     * @param $input array
     *
     * @return Collection
     */
    public static function getMarketDetailsData($input)
    {
        $data = ProjectDetails::with('getProject', 'getCategory')
                        ->orderBy('PROJECT_ID', 'desc')
                        ->take($input['length'])
                        ->skip($input['start'])
                        ->get();

        $total = ProjectDetails::count();

        return ['data' => $data, 'total' => $total, 'filtered' => $total];
    }

    ///--------------------**********************

    /**
     * Project data collection.
     *
     * @param $input array
     *
     * @return Collection
     */
    public static function getData($input)
    {
        $data = self::where('IS_LEASE', 0)->orderBy('PROJECT_ID', 'desc')
                        ->take($input['length'])
                        ->skip($input['start'])
                        ->get();

        $total = self::where('IS_LEASE', 0)->count();

        return ['data' => $data, 'total' => $total, 'filtered' => $total];
    }

    /**
     * Lease data collection.
     *
     * @param $input array
     *
     * @return Collection
     */
    public static function getDataLease($input)
    {
        $data = self::where('IS_LEASE', 1)
                        ->orderBy('PROJECT_ID', 'desc')
                        ->take($input['length'])
                        ->skip($input['start'])
                        ->get();
        $total = self::where('IS_LEASE', 1)->count();

        return ['data' => $data, 'total' => $total, 'filtered' => $total];
    }

    /**
     * Create project data.
     *
     * @param $data array
     * @param $user string
     *
     * @return string
     */
    public static function createData($data, $user, $type)
    {
        $project_land = explode(',', $data['land']);

        $project = new self();
        $project->IS_LEASE = $type;
        $project->PR_UD_ID = $data['number'];
        $project->PR_NAME = $data['name'];
        $project->PR_NAME_BN = $data['name_bn'];
        $project->PR_DESC = $data['details'];
        $project->PR_START_DT = date('Y-m-d', strtotime(str_replace('/', '-', $data['start_date'])));
        $project->PR_END_DT = date('Y-m-d', strtotime(str_replace('/', '-', $data['end_date'])));
        $project->PROJECT_LOCATION = $data['location'];
        $project->IS_ACTIVE = isset($data['is_active']) ? $data['is_active'] : 0;
        $project->CREATED_BY = $user;
        $project->save();

        $projectId = $project->PROJECT_ID;

        // Create project documents
        if (isset($data['project_doc']) && count($data['project_doc']) > 0) {
            foreach ($data['project_doc'] as $key => $doc) {
                $pr_docs = new ProjectDoc();
                $pr_docs->PROJECT_ID = $projectId;
                $project_file = $doc['doc'];
                $project_file_name = uniqid('pro-');
                $project_file_ext = $project_file->getClientOriginalExtension();
                $project_file_ext = strtolower($project_file_ext);
                $project_file_fullname = $project_file_name.'.'.$project_file_ext;
                $pr_docs->ATTACHMENT = $project_file_fullname;
                $project_file->move(base_path().'/public/upload/project/', $project_file_fullname);
                $pr_docs->CREATED_BY = $user;
                $pr_docs->save();
            }
        }

        // Create project Land table data(Project land)
        foreach ($project_land as $key => $land) {
            $projectLand = new ProjectLand();
            $projectLand->PROJECT_ID = $projectId;
            $projectLand->LAND_ID = $land;
            $projectLand->IS_ACTIVE = 1;
            $projectLand->CREATED_BY = $user;
            $projectLand->save();
        }
        // Create project details table data(Project shop)
        if (isset($data['project_shop']) and count($data['project_shop']) > 0) {
            foreach ($data['project_shop'] as $key => $pr_shop) {
                if (!empty($pr_shop['type']) && !empty($pr_shop['position'])) {
                    $projectShop = new ProjectDetails();
                    $projectShop->PROJECT_ID = $projectId;
                    $projectShop->PR_CATEGORY = $pr_shop['category'];
                    $projectShop->PR_TYPE = $pr_shop['type'];
                    $projectShop->POSITION = $pr_shop['position'];
                    $projectShop->PR_SSF_NO = $pr_shop['ssf_no'];
                    $projectShop->PR_MEASURMENT = Helpers::bn2en($pr_shop['measurement']);
                    $projectShop->UOM = $pr_shop['uom'];
                    $projectShop->IS_RESERVED = $pr_shop['is_reserved'];
                    $projectShop->IS_BUILT = $pr_shop['is_build'];
                    $projectShop->IS_ACTIVE = $pr_shop['is_active'];
                    $projectShop->CREATED_BY = $user;
                    $projectShop->save();
                }
            }
        }
        // Create project details table data(Project flat)
        if (isset($data['project_flat']) and count($data['project_flat']) > 0) {
            foreach ($data['project_flat'] as $key => $pr_flat) {
                if (!empty($pr_flat['type']) && !empty($pr_flat['position'])) {
                    $projectFlat = new ProjectDetails();
                    $projectFlat->PROJECT_ID = $projectId;
                    $projectFlat->PR_CATEGORY = $pr_flat['category'];
                    $projectFlat->PR_TYPE = $pr_flat['type'];
                    $projectFlat->POSITION = $pr_flat['position'];
                    $projectFlat->PR_SSF_NO = $pr_flat['ssf_no'];
                    $projectFlat->PR_MEASURMENT = Helpers::bn2en($pr_flat['measurement']);
                    $projectFlat->UOM = $pr_flat['uom'];
                    $projectFlat->IS_RESERVED = $pr_flat['is_reserved'];
                    $projectFlat->IS_BUILT = $pr_flat['is_build'];
                    $projectFlat->IS_ACTIVE = $pr_flat['is_active'];
                    $projectFlat->CREATED_BY = $user;
                    $projectFlat->save();
                }
            }
        }
        // Create project details table data(Project space)
        if (isset($data['project_space']) and count($data['project_space']) > 0) {
            foreach ($data['project_space'] as $key => $pr_space) {
                if (!empty($pr_space['type']) && !empty($pr_space['position'])) {
                    $projectSpace = new ProjectDetails();
                    $projectSpace->PROJECT_ID = $projectId;
                    $projectSpace->PR_CATEGORY = $pr_space['category'];
                    $projectSpace->PR_TYPE = $pr_space['type'];
                    $projectSpace->POSITION = $pr_space['position'];
                    $projectSpace->PR_SSF_NO = $pr_space['ssf_no'];
                    $projectSpace->PR_MEASURMENT = Helpers::bn2en($pr_space['measurement']);
                    $projectSpace->UOM = $pr_space['uom'];
                    $projectSpace->IS_RESERVED = $pr_space['is_reserved'];
                    $projectSpace->IS_BUILT = $pr_space['is_build'];
                    $projectSpace->IS_ACTIVE = $pr_space['is_active'];
                    $projectSpace->CREATED_BY = $user;
                    $projectSpace->save();
                }
            }
        }

        return $projectId;
    }

    /**
     * Update project data.
     *
     * @param $data array
     * @param $user string
     * @param $id string
     *
     * @return string
     */
    public static function updateData($data, $user, $id)
    {
        $project_land = explode(',', $data['land']);

        $project = self::find($id);
        $project->PR_UD_ID = $data['number'];
        $project->PR_NAME = $data['name'];
        $project->PR_NAME_BN = $data['name_bn'];
        $project->PR_DESC = $data['details'];
        $project->PR_START_DT = date('Y-m-d', strtotime(str_replace('/', '-', $data['start_date'])));
        $project->PR_END_DT = date('Y-m-d', strtotime(str_replace('/', '-', $data['end_date'])));
        $project->PROJECT_LOCATION = $data['location'];
        $project->IS_ACTIVE = $data['is_active'];
        $project->UPDATED_BY = $user;
        $project->save();

        $projectId = $project->PROJECT_ID;

        // Create project documents
        if (isset($data['project_doc']) && count($data['project_doc']) > 0) {
            foreach ($data['project_doc'] as $key => $doc) {
                $pr_docs = new ProjectDoc();
                $pr_docs->PROJECT_ID = $projectId;
                $project_file = $doc['doc'];
                $project_file_name = uniqid('pro-');
                $project_file_ext = $project_file->getClientOriginalExtension();
                $project_file_ext = strtolower($project_file_ext);
                $project_file_fullname = $project_file_name.'.'.$project_file_ext;
                $pr_docs->ATTACHMENT = $project_file_fullname;
                $project_file->move(base_path().'/public/upload/project/', $project_file_fullname);
                $pr_docs->CREATED_BY = $user;
                $pr_docs->save();
            }
        }

        // Update project Land table data(Project land)
        ProjectLand::where('PROJECT_ID', $id)->delete();
        foreach ($project_land as $key => $land) {
            $projectLand = new ProjectLand();
            $projectLand->PROJECT_ID = $projectId;
            $projectLand->LAND_ID = $land;
            $projectLand->IS_ACTIVE = 1;
            $projectLand->CREATED_BY = $user;
            $projectLand->save();
        }

        // Create/update project details table data(Project shop)
        if (isset($data['project_shop']) and count($data['project_shop']) > 0) {
            foreach ($data['project_shop'] as $key => $pr_shop) {
                if (!empty($pr_shop['type']) && !empty($pr_shop['position'])) {
                    if (isset($pr_shop['details_id']) && !empty($pr_shop['details_id'])) {
                        $projectShop = ProjectDetails::find($pr_shop['details_id']);
                    } else {
                        $projectShop = new ProjectDetails();
                    }

                    $projectShop->PROJECT_ID = $projectId;
                    $projectShop->PR_CATEGORY = $pr_shop['category'];
                    $projectShop->PR_TYPE = $pr_shop['type'];
                    $projectShop->POSITION = $pr_shop['position'];
                    $projectShop->PR_SSF_NO = $pr_shop['ssf_no'];
                    $projectShop->PR_MEASURMENT = Helpers::bn2en($pr_shop['measurement']);
                    $projectShop->UOM = $pr_shop['uom'];
                    $projectShop->IS_RESERVED = $pr_shop['is_reserved'];
                    $projectShop->IS_BUILT = $pr_shop['is_build'];
                    $projectShop->IS_ACTIVE = $pr_shop['is_active'];
                    $projectShop->CREATED_BY = $user;
                    $projectShop->UPDATED_BY = $user;
                    $projectShop->save();
                }
            }
        }
        // Create/update project details table data(Project flat)
        if (isset($data['project_flat']) and count($data['project_flat']) > 0) {
            foreach ($data['project_flat'] as $key => $pr_flat) {
                if (!empty($pr_flat['type']) && !empty($pr_flat['position'])) {
                    if (isset($pr_flat['details_id']) && !empty($pr_flat['details_id'])) {
                        $projectFlat = ProjectDetails::find($pr_flat['details_id']);
                    } else {
                        $projectFlat = new ProjectDetails();
                    }

                    $projectFlat->PROJECT_ID = $projectId;
                    $projectFlat->PR_CATEGORY = $pr_flat['category'];
                    $projectFlat->PR_TYPE = $pr_flat['type'];
                    $projectFlat->POSITION = $pr_flat['position'];
                    $projectFlat->PR_SSF_NO = $pr_flat['ssf_no'];
                    $projectFlat->PR_MEASURMENT = Helpers::bn2en($pr_flat['measurement']);
                    $projectFlat->UOM = $pr_flat['uom'];
                    $projectFlat->IS_RESERVED = $pr_flat['is_reserved'];
                    $projectFlat->IS_BUILT = $pr_flat['is_build'];
                    $projectFlat->IS_ACTIVE = $pr_flat['is_active'];
                    $projectFlat->CREATED_BY = $user;
                    $projectFlat->UPDATED_BY = $user;
                    $projectFlat->save();
                }
            }
        }
        // Create/update project details table data(Project space)
        if (isset($data['project_space']) and count($data['project_space']) > 0) {
            foreach ($data['project_space'] as $key => $pr_space) {
                if (!empty($pr_space['type']) && !empty($pr_space['position'])) {
                    if (isset($pr_space['details_id']) && !empty($pr_space['details_id'])) {
                        $projectSpace = ProjectDetails::find($pr_space['details_id']);
                    } else {
                        $projectSpace = new ProjectDetails();
                    }

                    $projectSpace->PROJECT_ID = $projectId;
                    $projectSpace->PR_CATEGORY = $pr_space['category'];
                    $projectSpace->PR_TYPE = $pr_space['type'];
                    $projectSpace->POSITION = $pr_space['position'];
                    $projectSpace->PR_SSF_NO = $pr_space['ssf_no'];
                    $projectSpace->PR_MEASURMENT = Helpers::bn2en($pr_space['measurement']);
                    $projectSpace->UOM = $pr_space['uom'];
                    $projectSpace->IS_RESERVED = $pr_space['is_reserved'];
                    $projectSpace->IS_BUILT = $pr_space['is_build'];
                    $projectSpace->IS_ACTIVE = $pr_space['is_active'];
                    $projectSpace->CREATED_BY = $user;
                    $projectSpace->UPDATED_BY = $user;
                    $projectSpace->save();
                }
            }
        }

        return $projectId;
    }

    /**
     * Inser Mahal data
     * Nurullah<nurul@atilimited.net>.
     *
     * @param $data array
     * @param $user string
     * @param $id string
     *
     * @return string
     */
    public static function createMahal($data, $user)
    {
        $project = new self();
        $project->IS_LEASE = 1;
        $project->PR_UD_ID = $data['PR_UD_ID'];
        $project->PR_NAME_BN = $data['PR_NAME_BN'];
        $project->PR_NAME = $data['PR_NAME'];
        $project->PROJECT_LOCATION = $data['PROJECT_LOCATION'];
        $project->IS_ACTIVE = isset($data['IS_ACTIVE']) ? $data['IS_ACTIVE'] : 0;
        $project->CREATED_BY = $user;
        $project->save();

        $projectId = $project->PROJECT_ID;

        // Create project Land table data(Project land)
        foreach ($data['LAND_ID'] as $key => $land) {
            $projectLand = new ProjectLand();
            $projectLand->PROJECT_ID = $projectId;
            $projectLand->LAND_ID = $land;
            $projectLand->IS_ACTIVE = 1;
            $projectLand->CREATED_BY = $user;
            $projectLand->save();
        }

        return $projectId;
    }

    /**
     * Update Mohal data
     * Nurullah<nurul@atilimited.net>.
     *
     * @param $data array
     * @param $user string
     * @param $id string
     *
     * @return string
     */
    public static function updateDataMahal($data, $user, $id)
    {
        $project = self::find($id);
        $project->PR_UD_ID = $data['PR_UD_ID'];
        $project->PR_NAME = $data['PR_NAME'];
        $project->PR_NAME_BN = $data['PR_NAME_BN'];
        $project->PROJECT_LOCATION = $data['PROJECT_LOCATION'];
        $project->IS_ACTIVE = isset($data['IS_ACTIVE']) ? $data['IS_ACTIVE'] : 0;
        $project->UPDATED_BY = $user;
        $project->save();
        $projectId = $project->PROJECT_ID;

        // Update Mahal Land table data(Lease land)
        ProjectLand::where('PROJECT_ID', $id)->delete();
        foreach ($data['LAND_ID'] as $key => $land) {
            $projectLand = new ProjectLand();
            $projectLand->PROJECT_ID = $projectId;
            $projectLand->LAND_ID = $land;
            $projectLand->IS_ACTIVE = 1;
            $projectLand->CREATED_BY = $user;
            $projectLand->save();
        }

        return $projectId;
    }

    /**
     * Get Lease Mahal List.
     *
     * @return Collection
     */
    public static function getLeaseProject()
    {
        $project = [];
        $apro = [];
        $cpro = [];
        $capp = [];
        $tmp = [];
        $output = [];
        $appplicant_pro = ApplicantProperty::where('IS_LEASE', 1)
                                            ->where('IS_ACTIVE', 1)
                                            ->select('PROJECT_ID')
                                            ->get();
        $ctz_application = CitizenApplication::where('IS_LEASE', 1)
                                            ->where('IS_ACTIVE', 1)
                                            ->select('PROJECT_ID')
                                            ->get();
        $ctz_property = CitizenProperty::where('IS_LEASE', 1)
                                        ->where('IS_ACTIVE', 1)
                                        ->select('PROJECT_ID')
                                        ->get();
        foreach ($appplicant_pro as $key => $lapro) {
            $apro[] = $lapro->PROJECT_ID;
        }
        foreach ($ctz_application as $inx => $lcapp) {
            $capp[] = $lcapp->PROJECT_ID;
        }
        foreach ($ctz_property as $indx => $lcpro) {
            $cpro[] = $lcpro->PROJECT_ID;
        }

        $project = array_merge($apro, $capp, $cpro);

        foreach ($project as $k => $pr) {
            $tmp[$pr] = $k;
        }
        foreach ($tmp as $i => $arr) {
            $output[] = $i;
        }

        return count($output) > 0 ? self::where('IS_ACTIVE', 1)->where('IS_LEASE', 1)->whereNotIn('PROJECT_ID', $output)->get()
                : self::where('IS_ACTIVE', 1)->where('IS_LEASE', 1)->get();
    }

    /**
     * Get Tender categorty List.
     *
     * @return Collection
     */
    public static function getReTenderCategory()
    {
        $project = [];
        $apro = [];
        $cpro = [];
        $capp = [];
        $tmp = [];
        $output = [];
        $appplicant_pro = ApplicantProperty::where('IS_LEASE', 1)
                                            ->where('IS_ACTIVE', 1)
                                            ->select('PROJECT_ID')
                                            ->get();
        $ctz_application = CitizenApplication::where('IS_LEASE', 1)
                                            ->where('IS_ACTIVE', 1)
                                            ->select('PROJECT_ID')
                                            ->get();
        $ctz_property = CitizenProperty::where('IS_LEASE', 1)
                                        ->where('IS_ACTIVE', 1)
                                        ->select('PROJECT_ID')
                                        ->get();
        foreach ($appplicant_pro as $key => $lapro) {
            $apro[] = $lapro->PROJECT_ID;
        }
        foreach ($ctz_application as $inx => $lcapp) {
            $capp[] = $lcapp->PROJECT_ID;
        }
        foreach ($ctz_property as $indx => $lcpro) {
            $cpro[] = $lcpro->PROJECT_ID;
        }

        $project = array_merge($apro, $capp, $cpro);

        foreach ($project as $k => $pr) {
            $tmp[$pr] = $k;
        }
        foreach ($tmp as $i => $arr) {
            $output[] = $i;
        }

        return count($output) > 0 ? self::where('IS_ACTIVE', 1)->where('IS_LEASE', 1)->whereNotIn('PROJECT_ID', $output)->get()
                : self::where('IS_ACTIVE', 1)->where('IS_LEASE', 1)->get();
    }

    /**
     * Get parent land.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function getLand()
    {
        return $this->belongsTo('App\Models\Land', 'LAND_ID', 'LAND_ID');
    }

    /**
     * Get project details.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function getDocuments()
    {
        return $this->hasMany('App\Models\ProjectDoc', 'PROJECT_ID', 'PROJECT_ID');
    }

    /**
     * Get project details.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function getProjectDetails()
    {
        return $this->hasMany('App\Models\ProjectDetails', 'PROJECT_ID', 'PROJECT_ID');
    }
}
