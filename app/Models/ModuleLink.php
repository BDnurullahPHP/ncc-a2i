<?php

namespace App\Models;

use DB;
use Illuminate\Database\Eloquent\Model;

class ModuleLink extends Model
{
    protected $table = 'sa_module_links';
    protected $primaryKey = 'LINK_ID';

    const CREATED_AT = 'CREATED_AT';
    const UPDATED_AT = 'UPDATED_AT';

    protected $fillable = ['LINK_NAME', 'LINK_NAME_BN', 'LINK_PAGES', 'MODULE_ID', 'LINK_URI', 'LINK_DESC', 'SL_NO', 'CREATE', 'READ', 'UPDATE', 'DELETE', 'STATUS', 'IS_ACTIVE', 'CREATED_BY', 'UPDATED_BY'];

    /**
     * Save or update Module.
     *
     *@author  Nurullah <nurul@atilimited.net>
     *
     * @param array       $data
     * @param string      $user
     * @param string|null $id
     *
     * @return string
     */
    public static function createLink($data, $user, $id)
    {
        $maxSlNo = DB::table('sa_module_links')->max('SL_NO');
        if (!empty($id)) {
            $link = self::find($id);
            $link->IS_ACTIVE = isset($data['IS_ACTIVE']) ? $data['IS_ACTIVE'] : 0;
            $link->UPDATED_BY = $user;
        } else {
            $link = new self();
            $link->LINK_PAGES = 'I,V,U,D,S';
            $link->CREATE = 1;
            $link->READ = 1;
            $link->UPDATE = 1;
            $link->DELETE = 1;
            $link->STATUS = 1;
            $link->SL_NO = $maxSlNo + 1;
            $link->CREATED_BY = $user;
        }
        $link->MODULE_ID = $data['MODULE_ID'];
        $link->LINK_NAME = $data['LINK_NAME'];
        $link->LINK_NAME_BN = $data['LINK_NAME_BN'];
        $link->LINK_URI = $data['LINK_URI'];

        $link->save();
    }

    /**
     * Module data collection.
     *
     *@author  Nurullah <nurul@atilimited.net>
     *
     * @param $input array
     *
     * @return Collection
     */
    public static function getData($input)
    {
        $data = self::orderBy('SL_NO', 'desc')
                    ->take($input['length'])
                    ->skip($input['start'])
                    ->get();

        $total = self::count();

        return ['data' => $data, 'total' => $total, 'filtered' => $total];
    }
}
