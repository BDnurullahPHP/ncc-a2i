<?php

namespace App\Models;

use App\Ncc\Helpers;
use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    protected $table = 'sa_notifications';
    protected $primaryKey = 'NOTIFICATION_ID';
    const CREATED_AT = 'CREATED_AT';
    const UPDATED_AT = 'UPDATED_AT';
    protected $fillable = [
                                'TE_APP_ID',
                                'USER_ID',
                                'USER_ID',
                                'CITIZEN_ID',
                                'LEASE_DE_ID',
                                'SCHEDULE_ID',
                                'PARTICULAR_ID',
                                'TRX_TRAN_NO',
                                'TITLE',
                                'DETAILS',
                                'SEND_MAIL',
                                'SEND_SMS',
                                'IS_VIEW',
                                'IS_ACTIVE',
                                'CREATED_AT',
                                'UPDATED_AT',
                            ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = ['NOTIFICATION_ID'];

    /**
     * Notification for tender application.
     *
     * @param array  $data
     * @param string $applicant
     * @param string $citizen
     * @param string $schedule
     * @param string $type
     * @param bool   $paid
     *
     * @return string
     */
    public static function tenderApplication($data, $applicant, $citizen, $schedule, $type, $paid = false)
    {
        $title = $data['TYPE'] == 'tender' ? 'সিডিউলের মূল্য ' : 'দরপত্রের মূল্য ';

        $notification = new self();
        $notification->TE_APP_ID = $applicant;
        $notification->CITIZEN_ID = $citizen;
        $notification->SCHEDULE_ID = $schedule;
        $notification->TITLE = $paid ? $title.'পরিশোধিত।' : $title.'পরিশোধ করুন।';
        $notification->DETAILS = self::generateApplySMS($data, $type, $paid);
        $notification->SEND_MAIL = 1;
        $notification->SEND_SMS = 1;
        $notification->IS_ACTIVE = 1;
        $notification->save();
    }

    /**
     * Generate tender apply SMS.
     *
     * @param array  $data Application data
     * @param string $type tender/lease
     * @param bool   $paid
     *
     * @return string
     */
    public static function generateApplySMS($data, $type, $paid)
    {
        $type = $type == 'tender' ? 0 : 1;
        $schedule = TenderSchedule::with('getProject', 'getCategory', 'getProjectType', 'getProjectDetail')
                                    ->where('SCHEDULE_ID', $data['SCHEDULE_ID'])
                                    ->first();
        $sms = 'আপনার আবেদনকৃত ';
        $sms .= $schedule->getProject->PR_NAME_BN;
        if (!empty($schedule->getProjectDetail)) {
            $sms .= ' এর ';
            $sms .= Helpers::en2bn($schedule->getProjectDetail->POSITION).' তলার ';
            $sms .= $schedule->getProjectDetail->PR_SSF_NO.' নং ';
            $sms .= $schedule->getCategory->CATE_NAME;
        }
        if ($type == 0) {
            $sms .= ' এর জন্য সিডিউলের মূল্য ';
        } else {
            $sms .= ' এর জন্য দরপত্রের মূল্য ';
        }

        $particular = Particular::where('TYPE', 1)->where('IS_LEASE', $type)->pluck('PARTICULAR_ID');
        $tender_price = TenderParticulars::where('PARTICULAR_ID', $particular)
                                            ->where('SCHEDULE_ID', $data['SCHEDULE_ID'])
                                            ->pluck('PARTICULAR_AMT');

        $sms .= Helpers::en2bn(number_format($tender_price, 2)).' টাকা এবং জামানত ';
        $sms .= Helpers::en2bn(number_format($data['BG_AMOUNT'], 2)).' টাকা ';
        $sms .= $paid ? 'পরিশোধিত।' : 'পরিশোধ করুন।';

        if (!isset($data['CITIZEN_ID']) && !isset($data['TE_APPLICANT_ID'])) {
            $sms .= 'আপনার লগইন আইডিঃ '.$data['APPLICANT_PHONE'];
            $sms .= ' এবং পাসওয়ার্ডঃ '.$data['TE_APP_PASSWORD'];
            $sms .= ' ব্যবহার করুন';
        }

        return $sms;
    }

    /**
     * Notification for security refund.
     *
     * @param array  $data
     * @param string $uid
     * @param string $utype
     * @param string $sc
     *
     * @return string
     */
    public static function securityRefundSMS($data, $uid, $utype, $sc)
    {
        $message = self::generatesecurityRefundSMS($sc, $data['security_money']);

        $notification = new self();
        $notification->TE_APP_ID = $utype != 'citizen' ? $uid : null;
        $notification->CITIZEN_ID = $utype == 'citizen' ? $uid : null;
        $notification->SCHEDULE_ID = $sc;
        $notification->TITLE = 'জামানতের টাকা ফেরত';
        $notification->DETAILS = $message;
        $notification->SEND_SMS = 1;
        $notification->IS_ACTIVE = 1;
        $notification->save();

        return $message;
    }

    /**
     * Generate security refund SMS.
     *
     * @param array $sc     Schedule id
     * @param array $amount Total amount
     *
     * @return string
     */
    public static function generatesecurityRefundSMS($sc, $amount)
    {
        $schedule = TenderSchedule::with('getProject', 'getCategory', 'getProjectType', 'getProjectDetail')
                                    ->where('SCHEDULE_ID', $sc)
                                    ->first();
        $sms = 'আপনার আবেদনকৃত ';
        $sms .= $schedule->getProject->PR_NAME_BN;
        if (!empty($schedule->getProjectDetail)) {
            $sms .= ' এর ';
            $sms .= Helpers::en2bn($schedule->getProjectDetail->POSITION).' তলার ';
            $sms .= $schedule->getProjectDetail->PR_SSF_NO.' নং ';
            $sms .= $schedule->getCategory->CATE_NAME;
        }

        $sms .= ' এর জন্য আপনি নির্বাচিত না হওয়ায় আপনার জামানতের ';
        $sms .= Helpers::en2bn($amount).' টাকা ফেরত দেওয়া হলো।';

        return $sms;
    }

    /**
     * Notification for payment.
     *
     * @param array  $data
     * @param string $type
     * @param string $part
     * @param string $vchd
     * @param string $sc
     * @param string $user
     *
     * @return string
     */
    public static function paymentNotification($data, $type, $part, $vchd, $sc, $user)
    {
        $message = self::generatePaymentNotificationSMS($sc, $part, $data['total_amount']);

        $notification = new self();
        $notification->TE_APP_ID = $type != 'citizen' ? $user : null;
        $notification->CITIZEN_ID = $type == 'citizen' ? $user : null;
        $notification->SCHEDULE_ID = $sc;
        $notification->TITLE = $message['particular'];
        $notification->DETAILS = $message['sms'];
        $notification->SEND_SMS = 1;
        $notification->IS_ACTIVE = 1;
        $notification->save();

        return $message['sms'];
    }

    /**
     * Generate payment SMS.
     *
     * @param array $sc     Schedule id
     * @param array $part   Particular id
     * @param array $amount Total amount
     *
     * @return array
     */
    public static function generatePaymentNotificationSMS($sc, $part, $amount)
    {
        $particular = Particular::where('PARTICULAR_ID', $part)->pluck('PARTICULAR_NAME');
        $schedule = TenderSchedule::with('getProject', 'getCategory', 'getProjectType', 'getProjectDetail')
                                    ->where('SCHEDULE_ID', $sc)
                                    ->first();
        $sms = 'আপনার আবেদনকৃত ';
        $sms .= $schedule->getProject->PR_NAME_BN;
        if (!empty($schedule->getProjectDetail)) {
            $sms .= ' এর ';
            $sms .= Helpers::en2bn($schedule->getProjectDetail->POSITION).' তলার ';
            $sms .= $schedule->getProjectDetail->PR_SSF_NO.' নং ';
            $sms .= $schedule->getCategory->CATE_NAME;
        }

        $sms .= ' এর জন্য '.$particular.' ';
        $sms .= Helpers::en2bn($amount).' টাকা পরিশোধ হয়েছে।';

        return ['sms' => $sms, 'particular' => $particular];
    }
}
