<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LandTax extends Model
{
    protected $table = 'land_tax';
    protected $primaryKey = 'TAX_ID';
    const CREATED_AT = 'CREATED_AT';
    const UPDATED_AT = 'UPDATED_AT';
    protected $fillable = [
                                'LAND_ID',
                                'RECEIPT',
                                'FILE_TYPE',
                                'DATE',
                                'CREATED_BY',
                                'UPDATED_AT',
                            ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = ['TAX_ID'];
}
