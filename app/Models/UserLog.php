<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserLog extends Model
{
    protected $table = 'user_account_log';
    protected $primaryKey = 'USER_LG_ID';
    const CREATED_AT = 'CREATED_AT';
    const UPDATED_AT = 'UPDATED_AT';
    protected $fillable = [
                                'CITIZEN_ID',
                                'TE_APP_ID',
                                'FULL_NAME',
                                'FULL_NAME_BN',
                                'MOBILE',
                                'SPOUSE_NAME',
                                'NID',
                                'EMAIL',
                                'DIVISION_ID',
                                'DISTRICT_ID',
                                'THANA_ID',
                                'PO_ID',
                                'ROAD_NO',
                                'HOLDING_NO',
                                'IS_ACTIVE',
                                'CREATED_BY',
                                'UPDATED_BY',
                            ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = ['USER_LG_ID'];
}
