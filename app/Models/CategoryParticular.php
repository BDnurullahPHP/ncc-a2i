<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CategoryParticular extends Model
{
    protected $table = 'sa_category_particulars';
    protected $primaryKey = 'CAT_PART_ID';
    const CREATED_AT = 'CREATED_AT';
    const UPDATED_AT = 'UPDATED_AT';

    protected $fillable = [
                            'PR_CATEGORY',
                            'PARTICULAR_ID',
                            'PARTICULAR_AMT',
                            'UOM',
                            'REMARKS',
                            'IS_ACTIVE',
                            'CREATED_BY',
                            'UPDATED_BY',
                        ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = ['CAT_PART_ID'];

    /**
     * CategoryParticular data collection.
     *
     * @param $input array
     *
     * @return Collection
     */
    public static function getData($input)
    {
        $data = self::orderBy('CAT_PART_ID', 'desc')
                                    ->take($input['length'])
                                    ->skip($input['start'])
                                    ->get();

        $total = self::count();

        return ['data' => $data, 'total' => $total, 'filtered' => $total];
    }

    /**
     * Get parent category.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function getCategory()
    {
        return $this->belongsTo('App\Models\ProjectCategory', 'PR_CATEGORY', 'PR_CATEGORY');
    }

    /**
     * Get parent particular.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function getParticular()
    {
        return $this->belongsTo('App\Models\Particular', 'PARTICULAR_ID', 'PARTICULAR_ID');
    }
}
