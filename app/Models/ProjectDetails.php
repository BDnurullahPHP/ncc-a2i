<?php

namespace App\Models;

use DB;
use Illuminate\Database\Eloquent\Model;

class ProjectDetails extends Model
{
    protected $table = 'project_details';
    protected $primaryKey = 'PR_DETAIL_ID';
    const CREATED_AT = 'CREATED_AT';
    const UPDATED_AT = 'UPDATED_AT';

    protected $fillable = [
                            'PROJECT_ID',
                            'PR_CATEGORY',
                            'PR_TYPE',
                            'POSITION',
                            'PR_SSF_NO',
                            'PR_MEASURMENT',
                            'UOM',
                            'IS_RESERVED',
                            'IS_BUILT',
                            'IS_ACTIVE',
                            'CREATED_BY',
                            'UPDATED_BY',
                        ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = ['PR_DETAIL_ID'];

    public static function getCategoryParticular()
    {
        $data = [];
        $particular = DB::table('sa_category_particulars')
                        ->join('project_details', 'sa_category_particulars.PR_CATEGORY', '=', 'project_details.PR_CATEGORY')
                        ->select('sa_category_particulars.*', 'project_details.*')
                        ->where('sa_category_particulars.PR_CATEGORY', 22)
                        ->groupBy('sa_category_particulars.CAT_PART_ID')
                        ->get();

        //$cat_particular     = DB::select(DB::raw("SELECT  `sa_category_particulars` . * ,  `sa_category_particulars`.`PR_CATEGORY` AS  `pivot_PR_CATEGORY` ,  `project_details`.`PR_CATEGORY` AS `pivot_PR_CATEGORY` FROM  `sa_category_particulars` INNER JOIN  `project_details` ON  `sa_category_particulars`.`PR_CATEGORY` =  `project_details`.`PR_CATEGORY` GROUP BY  `sa_category_particulars`.`CAT_PART_ID`"));

        return $particular;
    }

    /**
     * Get Tender project details List.
     *
     * @param $project_id
     * @param $category_id
     *
     * @return Collection
     */
    public static function getTenderProjectDetails($project_id, $category_id)
    {
        $projects = explode(',', $project_id);
        $project = [];
        $apro = [];
        $cpro = [];
        $capp = [];
        $tmp = [];
        $output = [];
        $appplicant_pro = ApplicantProperty::where('IS_LEASE', 0)
                                            ->where('IS_ACTIVE', 1)
                                            ->whereIn('PROJECT_ID', $projects)
                                            ->where('PR_CATEGORY', $category_id)
                                            ->select('PR_DETAIL_ID')
                                            ->get();
        $ctz_application = CitizenApplication::where('IS_LEASE', 0)
                                            ->where('IS_ACTIVE', 1)
                                            ->whereIn('PROJECT_ID', $projects)
                                            ->where('PR_CATEGORY', $category_id)
                                            ->select('PR_DETAIL_ID')
                                            ->get();
        $ctz_property = CitizenProperty::where('IS_LEASE', 0)
                                        ->where('IS_ACTIVE', 1)
                                        ->whereIn('PROJECT_ID', $projects)
                                        ->where('PR_CATEGORY', $category_id)
                                        ->select('PR_DETAIL_ID')
                                        ->get();
        foreach ($appplicant_pro as $key => $lapro) {
            $apro[] = $lapro->PR_DETAIL_ID;
        }
        foreach ($ctz_application as $inx => $lcapp) {
            $capp[] = $lcapp->PR_DETAIL_ID;
        }
        foreach ($ctz_property as $indx => $lcpro) {
            $cpro[] = $lcpro->PR_DETAIL_ID;
        }

        $project = array_merge($apro, $capp, $cpro);

        foreach ($project as $k => $pr) {
            $tmp[$pr] = $k;
        }
        foreach ($tmp as $i => $arr) {
            $output[] = $i;
        }

        return count($output) > 0 ? self::with('getProject', 'getFloor')
                                    ->where('IS_ACTIVE', 1)
                                    ->whereIn('PROJECT_ID', $projects)
                                    ->where('PR_CATEGORY', $category_id)
                                    ->whereNotIn('PR_DETAIL_ID', $output)
                                    ->orderBy('PR_DETAIL_ID', 'desc')->get()
                                    : self::with('getProject', 'getFloor')
                                    ->where('IS_ACTIVE', 1)
                                    ->whereIn('PROJECT_ID', $projects)
                                    ->where('PR_CATEGORY', $category_id)
                                    ->orderBy('PR_DETAIL_ID', 'desc')
                                    ->get();
    }

    /**
     * Get category details view.
     *
     * @param $id
     *
     * @return collection
     */
    public static function getCategoryDetails($id)
    {
        return self::with(['getCategory'])
                                ->where('PROJECT_ID', $id)
                                ->groupBy('PR_CATEGORY')
                                ->get();
    }

    /**
     * Get parent category.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function getCategory()
    {
        return $this->belongsTo('App\Models\ProjectCategory', 'PR_CATEGORY', 'PR_CATEGORY');
    }

    /**
     * Get parent type.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function getType()
    {
        return $this->belongsTo('App\Models\ProjectType', 'PR_TYPE', 'PR_TYPE');
    }

    /**
     * Get parent project.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function getProject()
    {
        return $this->belongsTo('App\Models\Project', 'PROJECT_ID', 'PROJECT_ID');
    }

    /**
     * Get parent category details.
     *
     * @return \Illuminate\Database\Eloquent\Relations\hasMany
     */
    public function getCategorydata()
    {
        return $this->hasMany('App\Models\ProjectDetails', 'PROJECT_ID');
    }

    /**
     * Get parent type.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function getFloor()
    {
        return $this->belongsTo('App\Models\ProjectFloor', 'POSITION', 'FLOOR_ID');
    }
}
