<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
//use App\Models\Lease;

class Lease extends Model
{
    protected $table = 'lease';
    protected $fillable = [
                            'category_id',
                            'subcategory_id',
                            'own_land',
                            'owner_info',
                            'name',
                            'bn_name',
                            'address',
                            'comments',
                            'status',
                            'created_by',
                            'updated_by',
                        ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * Get parent category.
     */
    public function getCategory1()
    {
        return $this->hasOne('App\Models\LeaseCategory', 'parent_id', 'id');
    }

    public function getCategory()
    {
        return $this->belongsTo('App\Models\LeaseCategory', 'category', 'id');
    }

    public function getWord()
    {
        return $this->belongsTo('App\Models\Word', 'ward', 'id');
    }

    public static function getData($input)
    {
        $data = self::with(['getCategory'])
                    ->select(['id', 'category_id', 'subcategory_id', 'own_land', 'owner_info', 'name', 'address', 'comments', 'status', 'created_at'])
                    ->orderBy('id', 'desc')
                    ->take($input['length'])
                    ->skip($input['start'])
                    ->get();

        $total = self::count();

        return ['data' => $data, 'total' => $total, 'filtered' => $total];
    }

    /**
     * Create lease data.
     *
     * @param $data array
     * @param $user string
     *
     * @return string
     */
    public static function leasecreateData($data, $user)
    {
        $lease = new self();
        $lease->category_id = $data['category_id'];
        $lease->subcategory_id = $data['subcategory_id'];
        $lease->own_land = $data['own_land'];
        $lease->owner_info = $data['owner_info'];
        $lease->name = $data['name'];
        $lease->bn_name = $data['bn_name'];
        $lease->address = $data['address'];
        $lease->comments = $data['comments'];
        $lease->status = $data['is_active'];
        $lease->created_by = $user;
        $lease->save();
        $leaseId = $lease->id;

        return $leaseId;
    }
}
