<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LandDag extends Model
{
    protected $table = 'land_dag_no';
    protected $primaryKey = 'DAG_ID';
    const CREATED_AT = 'CREATED_AT';
    const UPDATED_AT = 'UPDATED_AT';
    protected $fillable = [
                                'LAND_ID',
                                'RS',
                                'CS',
                                'SA',
                                'BS',
                                'TYPE',
                                'CREATED_BY',
                                'UPDATED_BY',
                            ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = ['DAG_ID'];
}
