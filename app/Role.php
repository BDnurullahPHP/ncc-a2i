<?php

namespace App;

use Zizaco\Entrust\EntrustRole;

class Role extends EntrustRole
{
    protected $primaryKey = 'ROLE_ID';

    /**
     * Role data collection.
     *
     * @param $input array
     *
     * @return Collection
     */
    public static function getData($input)
    {
        $data = self::orderBy('ROLE_ID', 'desc')
                    ->take($input['length'])
                    ->skip($input['start'])
                    ->get();

        $total = self::count();

        return ['data' => $data, 'total' => $total, 'filtered' => $total];
    }
}
