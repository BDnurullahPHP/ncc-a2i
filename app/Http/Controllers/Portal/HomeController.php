<?php

namespace App\Http\Controllers\Portal;

use App\Http\Controllers\Controller;
use App\Models\ApplicantProperty;
use App\Models\Bank;
use App\Models\CategoryParticular;
use App\Models\Citizen;
use App\Models\CitizenApplication;
use App\Models\CitizenProperty;
use App\Models\District;
use App\Models\Division;
use App\Models\Land;
use App\Models\LandCategory;
use App\Models\LeaseCategory;
use App\Models\LeaseDemesne;
use App\Models\Mouza;
use App\Models\Notification;
use App\Models\Officer;
use App\Models\Particular;
use App\Models\PaymentResponse;
use App\Models\Project;
use App\Models\ProjectCategory;
use App\Models\ProjectDetails;
use App\Models\Tender;
use App\Models\TenderApplicant;
use App\Models\TenderConditions;
use App\Models\TenderDateTime;
use App\Models\TenderLocation;
use App\Models\TenderParticulars;
use App\Models\TenderSchedule;
use App\Models\Thana;
use App\Models\Zone;
use App\Ncc\Datatablehelper;
use App\Ncc\Helpers;
use App\User;
use Auth;
use Config;
use DB;
use Illuminate\Http\Request;
use Mail;
use Redirect;
use Response;
use Session;
use Validator;
use View;

class HomeController extends Controller
{
    /**
     * Show the public portal page.
     *
     * @return \Illuminate\View\View
     */
    public function getIndexUrl($token = '')
    {//Start API token login eksheba
        $return = json_decode(file_get_contents('http://sandbox.eksheba.gov.bd/api/login/'.$token), true);
        if (reset($return)) {
            $new_token = $return['data']['token'];
            session(['new_token' => $new_token]);

            return redirect()->route('home');
        } else {
            return redirect()->to('http://sandbox.eksheba.gov.bd/');
        }
        //End API token login eksheba
    }

    public function getIndex()
    {
        //Start API token add in session eksheba
        if (empty(Session::get('new_token'))) {
            session(['new_token' => '']);
        }
        //Start API token add in session eksheba
        $tender_category = ProjectCategory::categorySchedule();

        return view('portal.home', compact('tender_category'));
    }

    /**
     * Show the public register page.
     *
     * @return \Illuminate\View\View
     */
    public function getRegister()
    {
        return view('portal.register');
    }

    /**
     * Show the Tender page.
     *
     * @return \Illuminate\View\View
     */
    public function getTender()
    {
        $project_category = ProjectCategory::where('IS_LEASE', 0)
                                            ->orderBy('PR_CATEGORY', 'desc')
                                            ->get();

        return view('portal.tender', compact('project_category'));
    }

    /**
     * Get tender schedule data.
     *
     * @param \Illuminate\Http\Request $request
     * @param string                   $type
     *
     * @return json
     */
    public function tenderscheduleData(Request $request, $type)
    {
        $input = $request->all();
        $collection = TenderSchedule::getData($input, $type);
        $output = Datatablehelper::tenderscheduleOutput($input, $collection);

        return response()->json($output);
    }

    /**
     * Get tender conditions.
     *
     * @param \Illuminate\Http\Request $request
     * @param string                   $type
     * @param string                   $id
     * @param string                   $sc
     *
     * @return \Illuminate\View\View
     */
    public function getTenderConditions(Request $request, $type, $id, $sc)
    {
        $data['t_type'] = $type == 24 ? 'lease' : 'tender';
        $data['tender'] = Tender::where('TENDER_ID', $id)->first();
        $data['officer'] = Officer::where('TYPE', 1)->first();
        $data['conditions'] = TenderConditions::with('getCategory')
                                                ->where('TENDER_ID', $id)
                                                ->where('PR_CATEGORY', $type)
                                                ->first();
        $data['onulipy_location'] = TenderLocation::with('getLocation')
                                                ->where('TENDER_ID', $id)
                                                ->where('TYPE', 2)
                                                ->get();

        return view('portal.condition', compact('sc'))->with($data);
    }

    /**
     * Show tender application form.
     *
     * @param \Illuminate\Http\Request $request
     * @param string                   $type
     * @param string                   $id
     *
     * @return \Illuminate\View\View
     */
    public function getTenderApplicationForm(Request $request, $type, $id)
    {
        $schedule_data = TenderSchedule::find($id);
        $val['bank_list'] = Bank::lists('BANK_NAME', 'BANK_ID');
        $val['division'] = Division::lists('DIVISION_ENAME', 'DIVISION_ID');
        $val['app_info'] = $this->applicationInfo($id, $type);
        if ($type == 'tender') {
            $val['schedule_price'] = CategoryParticular::where('PR_CATEGORY', $schedule_data->PR_CATEGORY)
                                                        ->where('PARTICULAR_ID', 4)
                                                        ->pluck('PARTICULAR_AMT');
            $val['guarantee'] = CategoryParticular::where('PR_CATEGORY', $schedule_data->PR_CATEGORY)
                                                        ->where('PARTICULAR_ID', 3)
                                                        ->pluck('PARTICULAR_AMT');
        } else {
            $val['schedule_price'] = TenderParticulars::where('SCHEDULE_ID', $schedule_data->SCHEDULE_ID)
                                                        ->where('PARTICULAR_ID', 7)
                                                        ->pluck('PARTICULAR_AMT');
            $val['guarantee'] = TenderParticulars::where('SCHEDULE_ID', $schedule_data->SCHEDULE_ID)
                                                        ->where('PARTICULAR_ID', 6)
                                                        ->pluck('PARTICULAR_AMT');
        }
        if (Auth::check()) {
            $user_data = Auth::user();
            $val['citizen_data'] = Auth::user()->load('getCitizen');
            if (!empty($val['citizen_data']->getCitizen)) {
                $val['district'] = District::where('DIVISION_ID', $val['citizen_data']->getCitizen->DIVISION_ID)
                                        ->lists('DISTRICT_ENAME', 'DISTRICT_ID');
                $val['thana'] = Thana::where('DISTRICT_ID', $val['citizen_data']->getCitizen->DISTRICT_ID)
                                    ->lists('THANA_ENAME', 'THANA_ID');
                // Check if already apply
                $citizen_application = CitizenApplication::where('CITIZEN_ID', $val['citizen_data']->getCitizen->CITIZEN_ID)
                                                        ->where('SCHEDULE_ID', $id)
                                                        ->where('IS_SELECTED', 0)
                                                        ->where('IS_ACTIVE', 1)
                                                        ->first();
                if (!empty($citizen_application)) {
                    Session::flash('app_exist', trans('common.already_apply'));

                    return redirect()->back();
                    exit;
                }
            }
        } elseif (!empty(Auth::user('user'))) {
            $val['applicant_data'] = Auth::user('user');
            $val['district'] = District::where('DIVISION_ID', $val['applicant_data']->DIVISION_ID)
                                        ->lists('DISTRICT_ENAME', 'DISTRICT_ID');
            $val['thana'] = Thana::where('DISTRICT_ID', $val['applicant_data']->DISTRICT_ID)
                                    ->lists('THANA_ENAME', 'THANA_ID');
            // Check if already apply
            $app_application = ApplicantProperty::where('TE_APP_ID', $val['applicant_data']->TE_APP_ID)
                                                ->where('SCHEDULE_ID', $id)
                                                ->where('IS_SELECTED', 0)
                                                ->where('IS_ACTIVE', 1)
                                                ->first();
            if (!empty($app_application)) {
                Session::flash('app_exist', trans('common.already_apply'));

                return redirect()->back();
                exit;
            }
        }
        //Start API token refresh eksheba
        $new_token = Session::get('new_token');
        $refresh = json_decode(file_get_contents('http://sandbox.eksheba.gov.bd/api/refresh/'.$new_token), true);
        //End API token refresh
        return View::make('portal.tender_application_form', compact('schedule_data'))->with($val);
    }

    /**
     * Create tender applicant.
     *
     * @param \Illuminate\Http\Request $request
     * @param string                   $type
     * @param string                   $id
     *
     * @return response
     */
    public function tenderapplicantCreate(Request $request, $type, $id)
    {
        $data = $request->all();
        $validator = Validator::make($request->all(), TenderApplicant::tenderApplicantRules());

        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput();
        } else {
            $division = Division::where('DIVISION_ID', $data['division'])->pluck('DIVISION_ENAME');
            $district = District::where('DISTRICT_ID', $data['district'])->pluck('DISTRICT_ENAME');
            $thana = Thana::where('THANA_ID', $data['thana'])->pluck('THANA_ENAME');
            $data['DIVISION_ENAME'] = $division;
            $data['DISTRICT_ENAME'] = $district;
            $data['THANA_ENAME'] = $thana;
            $data['TYPE'] = $type;

            Session::put('user_data', $data);

            return redirect()->route('get_tender_applicant_preview', $type);
        }
    }

    /**
     * Preview tender application.
     *
     * @param \Illuminate\Http\Request $request
     * @param string                   $type
     *
     * @return \Illuminate\View\View
     */
    public function tenderapplicantPreview(Request $request, $type)
    {
        $user_details = [];
        if (Session::has('user_data')) {
            $user_details = Session::get('user_data');
        }
        $app_info = $this->applicationInfo($user_details['SCHEDULE_ID'], $type);

        return view('portal.tenderapplicantPreview', compact('user_details', 'app_info'));
    }

    /**
     * Create tender application.
     *
     * @param \Illuminate\Http\Request $request
     * @param string                   $type
     *
     * @return response
     */
    public function tenderapplicantSave(Request $request, $type)
    {
        $data = Session::get('user_data');
        $tenderapplicant = TenderApplicant::tenderapplicantData($data);
        $sms = Notification::generateApplySMS($data, $type, false);
        $send_sms = Helpers::sendSMS($data['APPLICANT_PHONE'], $sms);
        //Start API token Save eksheba
        $new_token = Session::get('new_token');
        if ($new_token) {
            $data1 = [
                'name'      => $data['FIRST_NAME'].' '.$data['LAST_NAME'],
                'gender'    => 1,
                'amount'    => '0',
                'is_tribe'  => false,
                'is_disable'=> false,
            ];
            $data = [
                'token'=> $new_token,
                'date' => date('Y-m-d'),
                'data' => $data1,
            ];
            //API Url
            $url = 'http://sandbox.eksheba.gov.bd/api/save';
            //Initiate cURL.
            $ch = curl_init($url);
            //The JSON data.
            $jsonData = $data;
            //Encode the array into JSON.
            $jsonDataEncoded = json_encode($jsonData);
            //Tell cURL that we want to send a POST request.
            curl_setopt($ch, CURLOPT_POST, 1);
            //Attach our encoded JSON string to the POST fields.
            curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonDataEncoded);
            //Set the content type to application/json
            curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
            //Execute the request
            $result = curl_exec($ch);

            return redirect()->to($result['data']['redirect_url']);
        }
        //End API token Save eksheba
        return redirect()->route('get_tender_applicant_success', $data['TYPE']);
    }

    /**
     * Genarate tender application form with data.
     *
     * @param \Illuminate\Http\Request $request
     * @param string                   $type
     * @param string                   $id
     *
     * @return PDF
     */
    public function applyFormDownload(Request $request, $type, $id)
    {
        $data['schedule_data'] = TenderSchedule::with(['getCategory', 'getProject', 'getProjectType', 'getProjectDetail'])
                                                    ->where('SCHEDULE_ID', $id)->first();
        $data['tender_particular'] = TenderParticulars::with(['getParticular'])
                                                        ->where('SCHEDULE_ID', $id)
                                                        ->get();
        $data['conditions'] = TenderConditions::with('getCategory')
                                                        ->where('TENDER_ID', $data['schedule_data']->TENDER_ID)
                                                        ->where('PR_CATEGORY', $data['schedule_data']->PR_CATEGORY)
                                                        ->first();
        if ($type == 'tender') {
            $data['schedule_price'] = CategoryParticular::where('PR_CATEGORY', $data['schedule_data']->PR_CATEGORY)
                                                        ->where('PARTICULAR_ID', 4)
                                                        ->pluck('PARTICULAR_AMT');
            $data['guarantee'] = CategoryParticular::where('PR_CATEGORY', $data['schedule_data']->PR_CATEGORY)
                                                        ->where('PARTICULAR_ID', 3)
                                                        ->pluck('PARTICULAR_AMT');
        } else {
            $data['schedule_price'] = TenderParticulars::where('SCHEDULE_ID', $data['schedule_data']->SCHEDULE_ID)
                                                        ->where('PARTICULAR_ID', 7)
                                                        ->pluck('PARTICULAR_AMT');
            $data['guarantee'] = TenderParticulars::where('SCHEDULE_ID', $data['schedule_data']->SCHEDULE_ID)
                                                        ->where('PARTICULAR_ID', 6)
                                                        ->pluck('PARTICULAR_AMT');
        }

        //return View::make('portal.form_download', compact('type'))->with($data);exit;
        $pdf_data = View::make('portal.form_download', compact('type'))->with($data);
        $file_name = 'NCC-'.'tender-form'.'.pdf';

        return Helpers::makePDF($file_name, $pdf_data);
    }

    /**
     * Tender application success page.
     *
     * @return \Illuminate\View\View
     */
    public function tenderapplicantSuccess()
    {
        return view('portal.tenderapplicantSuccess');
    }

    /**
     * Render tender view page.
     *
     * @param \Illuminate\Http\Request $request
     * @param string                   $id
     *
     * @return \Illuminate\View\View
     */
    public function tenderView(Request $request, $id)
    {
        $data['tender'] = Tender::where('TENDER_ID', $id)->first();
        $data['schedule'] = TenderSchedule::with('getProject')->where('TENDER_ID', $id)->first();
        $data['tender_location'] = TenderLocation::with('getLocation')->where('TENDER_ID', $id)->where('TYPE', 1)->get();
        $data['onulipy_location'] = TenderLocation::with('getLocation')->where('TENDER_ID', $id)->where('TYPE', 2)->get();
        $data['conditions'] = TenderConditions::with('getCategory')->where('TENDER_ID', $id)->get();
        $data['location_type'] = TenderLocation::where('TENDER_ID', $id)->groupBy('TYPE')->get();
        $data['tender_date'] = TenderDateTime::where('TENDER_ID', $id)->first();
        $data['project_category'] = TenderSchedule::with('getCategory')
                                                    ->where('TENDER_ID', $id)
                                                    ->groupBy('PR_CATEGORY')
                                                    ->get();
        $data['project_details'] = TenderSchedule::where('TENDER_ID', $id)->get();

        return view('admin_access.tender.view', $data)->render();
    }

    /**
     * Render lease tender view page.
     *
     * @param \Illuminate\Http\Request $request
     * @param string                   $id
     *
     * @return \Illuminate\View\View
     */
    public function leaseTenderView(Request $request, $id)
    {
        $data['tender'] = Tender::where('TENDER_ID', $id)->first();
        $data['schedule'] = TenderSchedule::with('getProject')->where('TENDER_ID', $id)->first();
        $data['lease_location'] = TenderLocation::with('getLocation')->where('TENDER_ID', $id)->where('TYPE', 1)->get();
        $data['onulipy_location'] = TenderLocation::with('getLocation')->where('TENDER_ID', $id)->where('TYPE', 2)->get();
        $data['conditions'] = TenderConditions::with('getCategory')->where('TENDER_ID', $id)->where('PR_CATEGORY', 24)->first();
        $data['tender_date'] = TenderDateTime::where('TENDER_ID', $id)->get();
        $data['project_category'] = TenderSchedule::with('getCategory')
                                                    ->where('TENDER_ID', $id)
                                                    ->first();
        $data['particular'] = CategoryParticular::with('getParticular')
                                                ->where('PR_CATEGORY', $data['project_category']->PR_CATEGORY)
                                                ->orderBy('PARTICULAR_ID', 'asc')
                                                ->get();
        $data['tender_schedule'] = TenderSchedule::with(['getTenderParticulars', 'getProject'])
                                                    ->where('TENDER_ID', $id)
                                                    ->get();

        return view('admin_access.tender_lease.view', $data)->render();
    }

    /**
     * Show the tender applicant Details.
     *
     * @return \Illuminate\View\View
     */
    public function applicantDetails(Request $request, $type, $id, $sc)
    {
        if ($type == 'applicant') {
            $data['applicant'] = TenderApplicant::where('TE_APP_ID', $id)->first();
            $data['applicant_property'] = ApplicantProperty::where('TE_APP_ID', $id)->where('SCHEDULE_ID', $sc)->first();
        } elseif ($type == 'citizen') {
            $data['applicant'] = Citizen::with('getTenderCitizen')->where('CITIZEN_ID', $id)->first();
            $data['applicant_property'] = CitizenApplication::where('CITIZEN_ID', $id)->where('SCHEDULE_ID', $sc)->first();
        } else {
            $data['applicant_property'] = LeaseDemesne::with(['getSchedule' => function ($query) {
                $query->with('getProject');
            }, 'getValidity'])->where('SCHEDULE_ID', $sc)->first();
        }

        $data['schedule_info'] = TenderSchedule::where('SCHEDULE_ID', $data['applicant_property']->SCHEDULE_ID)->first();
        $data['project_name'] = Project::where('PROJECT_ID', $data['schedule_info']->PROJECT_ID)->first();
        $data['tender_particular'] = TenderParticulars::with('getParticular')
                                                            ->where('SCHEDULE_ID', $data['applicant_property']->SCHEDULE_ID)
                                                            ->get();

        if ($type == 'applicant' || $type == 'citizen') {
            $data['applicant_district'] = District::where('DISTRICT_ID', $data['applicant']->DISTRICT_ID)->pluck('DISTRICT_ENAME');
            $data['applicant_thana'] = Thana::where('THANA_ID', $data['applicant']->THANA_ID)->pluck('THANA_ENAME');
            $data['applicant_category'] = ProjectCategory::where('PR_CATEGORY', $data['applicant_property']->PR_CATEGORY)->pluck('CATE_NAME');
            $data['project_details'] = ProjectDetails::where('PR_DETAIL_ID', $data['applicant_property']->PR_DETAIL_ID)->first();
            $data['applicant_bank'] = Bank::where('BANK_ID', $data['applicant_property']->BANK_ID)->pluck('BANK_NAME');
        }

        return view('admin_access.tender_applicant.view', compact('type'))->with($data)->render();
    }

    /**
     * Save payment getway return data.
     *
     * @param string                   $type    User type
     * @param string                   $part    Particular id
     * @param string                   $vchd    Voucher child id
     * @param string                   $sc      Schedule id
     * @param \Illuminate\Http\Request $request
     *
     * @return response
     */
    public function makePayment(Request $request, $type, $part, $vchd, $sc)
    {
        $data = $request->all();
        $user = $type == 'applicant' ? Auth::user('user')->TE_APP_ID : Auth::user()->load('getCitizen')->CITIZEN_ID;
        $payment = PaymentResponse::makePayment($data, $type, $part, $vchd, $sc, $user);

        if (isset($data['error'])) {
            Session::flash('error', trans('common.payment_error'));
        } else {
            Session::flash('success', trans('common.payment_success'));

            return redirect()->route('payment_invoice', [$payment, $part, $sc, $type, $user]);
        }

        return $type == 'applicant' ? redirect()->route('te_app_notification') : redirect()->route('citizen_app_notification');
    }

    /**
     * Installment payment response.
     *
     * @param $request Request
     * @param string $id   Tender installment id
     * @param string $vchd Voucher child id
     * @param string $sc   Tender schedule id
     *
     * @return response
     */
    public function installmentPay(Request $request, $type, $id, $vchd, $sc)
    {
        $data = $request->all();
        $user = $type == 'applicant' ? Auth::user('user')->TE_APP_ID : Auth::user()->load('getCitizen')->CITIZEN_ID;
        $payment = PaymentResponse::installmentPayResponse($data, $type, $id, $vchd, $sc, $user);

        if (isset($data['error'])) {
            Session::flash('error', trans('common.payment_error'));
        } else {
            Session::flash('success', trans('common.payment_success'));

            return redirect()->route('payment_invoice', [$payment, 8, $sc, $type, $user]); // Tender installment particular id 8
        }

        return $type == 'applicant' ? redirect()->route('te_app_notification') : redirect()->route('citizen_app_notification');
    }

    /**
     * Show applicant payment invoice.
     *
     * @param \Illuminate\Http\Request $request
     * @param string                   $id      Payment response id
     * @param string                   $part    Particular id
     * @param string                   $sc      Schedule id
     * @param string                   $type    User type
     * @param string                   $uid     User id
     *
     * @return \Illuminate\View\View
     */
    public function paymentInvoice(Request $request, $id, $part, $sc, $type, $uid)
    {
        $data['user_type'] = $type;
        $data['particular'] = Particular::where('PARTICULAR_ID', $part)->pluck('PARTICULAR_NAME');
        $data['payment'] = PaymentResponse::find($id);

        if ($type == 'applicant') {
            $data['user'] = TenderApplicant::where('TE_APP_ID', $uid)->first();
        } else {
            $data['user'] = Citizen::with('getTenderCitizen')->where('CITIZEN_ID', $uid)->first();
        }

        if ($type == 'applicant') {
            $application = ApplicantProperty::where('TE_APP_ID', $data['user']->TE_APP_ID)
                                                        ->where('SCHEDULE_ID', $sc)
                                                        ->first();
            $data['project'] = Project::where('PROJECT_ID', $application->PROJECT_ID)->first();
            $data['tender_name'] = Tender::where('TENDER_ID', $application->TENDER_ID)->pluck('TENDER_TITLE');
            $data['user_district'] = District::where('DISTRICT_ID', $data['user']->DISTRICT_ID)->pluck('DISTRICT_ENAME');
            $data['user_thana'] = Thana::where('THANA_ID', $data['user']->THANA_ID)->pluck('THANA_ENAME');
            $tender_schedule = TenderSchedule::where('SCHEDULE_ID', $sc)->first();
        } else {
            if ($part == 5) {
                $citizen_pro = CitizenProperty::where('CITIZEN_ID', $data['user']->CITIZEN_ID)
                                                        ->where('SCHEDULE_ID', $sc)
                                                        ->first();
            } else {
                $citizen_pro = CitizenApplication::where('CITIZEN_ID', $data['user']->CITIZEN_ID)
                                                        ->where('SCHEDULE_ID', $sc)
                                                        ->first();
            }

            $data['project'] = Project::where('PROJECT_ID', $citizen_pro->PROJECT_ID)->first();
            $data['tender_name'] = Tender::where('TENDER_ID', $citizen_pro->TENDER_ID)->pluck('TENDER_TITLE');
            $data['user_district'] = District::where('DISTRICT_ID', $data['user']->DISTRICT_ID)->pluck('DISTRICT_ENAME');
            $data['user_thana'] = Thana::where('THANA_ID', $data['user']->THANA_ID)->pluck('THANA_ENAME');
            $tender_schedule = TenderSchedule::where('SCHEDULE_ID', $sc)->first();
        }

        if (!empty($tender_schedule->PR_DETAIL_ID)) {
            $data['project_details'] = ProjectDetails::with('getType')
                                                    ->where('PR_DETAIL_ID', $tender_schedule->PR_DETAIL_ID)
                                                    ->first();
            $data['project_category'] = ProjectCategory::where('PR_CATEGORY', $data['project_details']->PR_CATEGORY)
                                                ->where('IS_LEASE', 0)
                                                ->pluck('CATE_NAME');
        }

        return view('applicant.invoice', $data);
    }

    /**
     * Show the Lease Tender page.
     *
     * @return \Illuminate\View\View
     */
    public function getLease()
    {
        return view('portal.lease');
    }

    /**
     * Get lease tender schedule data.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return json
     */
    public function leaseTenderData(Request $request)
    {
        $input = $request->all();
        $collection = TenderSchedule::getLeaseData($input);
        $output = Datatablehelper::leaseTenderscheduleOutput($input, $collection);

        return response()->json($output);
    }

    /**
     * Show the Lease Tender page.
     *
     * @return Request               $request
     * @return string                $id
     * @return \Illuminate\View\View
     */
    public function getLeaseTender(Request $request, $id)
    {
        $lease_clause = TenderDateTime::where('TENDER_ID', $id)->get();

        return view('portal.lease_tender', compact('lease_clause'));
    }

    /**
     * Tender application edit form.
     *
     * @return Request               $request
     * @return string                $id
     * @return \Illuminate\View\View
     */
    public function getTenderApplicationFormEdit(Request $request, $id)
    {
        $bank_list = Bank::lists('BANK_NAME', 'BANK_ID');
        $division = Division::lists('DIVISION_ENAME', 'DIVISION_ID');

        return View::make('portal.tender_application_form_edit', compact('bank_list', 'division', 'id'));
    }

    /**
     * Show payment selection form.
     *
     * @return Request               $request
     * @return \Illuminate\View\View
     */
    public function paymentSelection(Request $request)
    {
        $data['payment_data'] = json_decode($request->get('up_data'));
        $data['payment_api'] = Config::get('payment');
        $data['bank_list'] = Bank::lists('BANK_NAME', 'BANK_ID');

        return View::make('applicant.payment_selection')->with($data)->render();
    }

    /**
     * Post payment data.
     *
     * @param string $type User type
     * @param string $part Particular id
     * @param string $vchd Voucher child id
     * @param string $sc   Schedule id
     * @param string $uid  User id
     *
     * @return Request $request
     * @return json
     */
    public function postPayment(Request $request, $type, $part, $vchd, $sc, $uid)
    {
        $data = $request->all();
        $payment = PaymentResponse::bankOrCashPayment($data, $type, $part, $vchd, $sc, $uid);
        $sms = Helpers::sendSMS($payment['mobile_no'], $payment['sms']);

        if (Auth::check() && !Auth::user()->hasRole('citizen')) {
            return response()->json(['flug' => 1, 'message' => trans('common.save_success')]);
        } else {
            return response()->json(['flug' => 1, 'message' => trans('common.save_success'), 'url' => route('payment_invoice', [$payment['payment_data'], $part, $sc, $type, $uid])]);
        }
    }

    /**
     * Get tender sticky note application info.
     *
     * @param string $schedule_id
     * @param string $type
     *
     * @return \Illuminate\View\View
     */
    private function applicationInfo($schedule_id, $type)
    {
        $val['schedule_data'] = TenderSchedule::with(['getCategory', 'getProject', 'getProjectType', 'getProjectDetail'])
                                                    ->where('SCHEDULE_ID', $schedule_id)->first();
        $val['tender_particular'] = TenderParticulars::with(['getParticular'])
                                                        ->where('SCHEDULE_ID', $schedule_id)
                                                        ->get();

        return View::make('portal.sticky_panel', compact('type'))->with($val)->render();
    }

    /**
     * Tender application view page.
     *
     * @return \Illuminate\View\View
     */
    public function applyOnline()
    {
        return view('portal.apply_online');
    }

    /**
     * Process tender form and return to application payment page.
     *
     * @return response
     */
    public function postApplication()
    {
        return redirect()->route('application_payment');
    }

    /**
     * Application payment view page.
     *
     * @return \Illuminate\View\View
     */
    public function applicationPayment()
    {
        return view('portal.application_payment');
    }

    /**
     * Contact us view page.
     *
     * @return \Illuminate\View\View
     */
    public function contact()
    {
        return view('portal.contact');
    }

    /**
     * Post contact.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return response
     */
    public function submitContact(Request $request)
    {
        $validator = Validator::make($request->all(), [
                                    'name'      => 'required|min:3',
                                    'email'     => 'required|email',
                                    'mobile'    => 'required|min:11',
                                    'message'   => 'required|min:3',
                                    'captcha'   => 'required|captcha',
                                ],
                                ['captcha' => 'Captcha not match']
                            );

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        } else {
            $mail_conf = Config::get('mail.from');
            $from['address'] = $request->get('email');
            $from['name'] = $request->get('name');
            $data['to_email'] = $mail_conf['address'];
            $data['to_name'] = $mail_conf['name'];
            $data['template'] = 'contact';
            $data['subject'] = 'Contact request';
            $data['name'] = $request->get('name');
            $data['mobile'] = $request->get('mobile');
            $data['text'] = $request->get('message');

            $send_email = Helpers::sendEmail($data, $from);

            Session::flash('success', 'সফলভাবে বার্তা পাঠানো হয়েছে।');

            return redirect()->back();
        }
    }

    /**
     * Show the public feedback form.
     *
     * @return \Illuminate\View\View
     */
    public function publicFeedback()
    {
        return View::make('portal.feedback');
    }

    /**
     * Post public feedback.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return response
     */
    public function postFeedback(Request $request)
    {
        $validator = Validator::make($request->all(), [
                                    'query'     => 'required',
                                    'name'      => 'required|min:3',
                                    'email'     => 'required|email',
                                    'mobile'    => 'required|min:11',
                                    'address'   => 'required|min:3',
                                    'message'   => 'required|min:3',
                                    'captcha'   => 'required|captcha',
                                ],
                                ['captcha' => 'Captcha not match']
                            );

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        } else {
            $mail_conf = Config::get('mail.from');
            $from['address'] = $request->get('email');
            $from['name'] = $request->get('name');
            $data['to_email'] = $mail_conf['address'];
            $data['to_name'] = $mail_conf['name'];
            $data['template'] = 'contact';
            $data['subject'] = 'Citizen feedback';
            $data['name'] = $request->get('name');
            $data['mobile'] = $request->get('mobile');
            $data['text'] = $request->get('message');

            $data['query'] = $request->get('query');
            $data['address'] = $request->get('address');

            $send_email = Helpers::sendEmail($data, $from);

            Session::flash('success', 'সফলভাবে বার্তা পাঠানো হয়েছে।');

            return redirect()->back();
        }
    }

    /**
     * Show the login form.
     *
     * @return \Illuminate\View\View
     */
    public function showLogin()
    {
        return View::make('portal.admin_login');
    }

    /**
     * Show tender applicant login form.
     *
     * @return \Illuminate\View\View
     */
    public function applicantLogin()
    {
        return View::make('portal.applicant_login');
    }

    /**
     * Show tender applicant password recovery form.
     *
     * @return \Illuminate\View\View
     */
    public function applicantRecovery()
    {
        return View::make('portal.app_forgot_pass');
    }

    /**
     * Check email for recovery.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return response
     */
    public function checkAccount(Request $request)
    {
        $email = $request->get('email');
        $data['code'] = md5($email);
        $applicant = TenderApplicant::where('EMAIL', $email)->where('IS_ACTIVE', 1)->first();
        $user = User::where('EMAIL', $email)->where('IS_ACTIVE', 1)->first();

        if (!empty($applicant)) {
            $data['link'] = route('get_reset_token', [$applicant->TE_APP_ID, 'applicant', $data['code']]);
            $data['to_email'] = $applicant->EMAIL;
            $data['to_name'] = $applicant->APPLICANT_NAME;
            $data['subject'] = 'Password reset';
            $data['template'] = 'reset';

            $send_mail = Helpers::sendEmail($data);

            $tender_applicant = TenderApplicant::find($applicant->TE_APP_ID);
            $tender_applicant->RESET_TOKEN = $data['code'];
            $tender_applicant->save();

            Session::flash('success', 'Check your email for reset password');
        } elseif (!empty($user)) {
            $data['link'] = route('get_reset_token', [$user->USER_ID, 'user', $data['code']]);
            $data['to_email'] = $user->EMAIL;
            $data['to_name'] = $user->FULL_NAME;
            $data['subject'] = 'Password reset';
            $data['template'] = 'reset';

            $send_mail = Helpers::sendEmail($data);

            $tender_user = User::find($user->USER_ID);
            $tender_user->RESET_TOKEN = $data['code'];
            $tender_user->save();

            Session::flash('success', 'Check your email for reset password');
        } else {
            Session::flash('error', 'Your email not found in the system!!');
        }

        return redirect()->back();
    }

    /**
     * Show password reset form.
     *
     * @param \Illuminate\Http\Request $request
     * @param string                   $id
     * @param string                   $type
     * @param string                   $token
     *
     * @return response
     */
    public function getResetToken(Request $request, $id, $type, $token)
    {
        $applicant = TenderApplicant::where('TE_APP_ID', $id)->where('RESET_TOKEN', $token)->first();
        $user = User::where('USER_ID', $id)->where('RESET_TOKEN', $token)->first();
        if (!empty($user) || !empty($applicant)) {
            return View::make('portal.app_recovery_pass', compact('id', 'type', 'token'));
            exit;
        }
        Session::flash('error', 'Your have lost your session!!');

        return redirect()->route('applicant_recovery');
    }

    /**
     * Change applicant password.
     *
     * @param \Illuminate\Http\Request $request
     * @param string                   $id
     * @param string                   $token
     *
     * @return response
     */
    public function changeAppPassword(Request $request, $id, $type, $token)
    {
        $applicant = TenderApplicant::where('TE_APP_ID', $id)->where('RESET_TOKEN', $token)->first();
        $user = User::where('USER_ID', $id)->where('RESET_TOKEN', $token)->first();

        $validator = Validator::make($request->all(), $this->passwordResetRules());
        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput();
            exit;
        } else {
            if (!empty($applicant)) {
                $tender_applicant = TenderApplicant::find($id);
                $tender_applicant->PASSWORD = $request->get('password');
                $tender_applicant->RESET_TOKEN = null;
                $tender_applicant->save();

                Session::flash('success', 'Successfully changed password');

                return redirect()->route('user_login');
                exit;
            } elseif (!empty($user)) {
                $tender_user = User::find($id);
                $tender_user->PASSWORD = bcrypt($request->get('password'));
                $tender_user->RESET_TOKEN = null;
                $tender_user->save();

                Session::flash('success', 'Successfully changed password');

                return redirect()->route('user_login');
                exit;
            }
            Session::flash('error', 'Oops! Something went wrong. Please try again later!!');
        }

        return redirect()->back();
    }

    /**
     * Tender applicant password reset validation rules.
     *
     * @return array
     */
    private function passwordResetRules()
    {
        return [
                'password'              => 'required|min:6|confirmed',
                'password_confirmation' => 'required|min:6',
            ];
    }

    /**
     * Switch language.
     *
     * @param \Illuminate\Http\Request $request
     * @param string                   $lang
     *
     * @return Response
     */
    public function languageSwitch(Request $request, $lang)
    {
        Session::put('locale', $lang);

        return redirect()->back();
    }

    /**
     * Check unique phone.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return json
     */
    public function checkUniquePhone(Request $request)
    {
        $mobile_exist = TenderApplicant::where('APPLICANT_PHONE', $request->phone)->where('IS_ACTIVE', 1)->first();
        $mobile_user_exist = User::where('MOBILE_NO', $request->phone)->where('IS_ACTIVE', 1)->first();
        if (!empty($mobile_exist)) {
            return response()->json([
                                        'flug'      => 0,
                                        'status'    => 1,
                                        'url'       => route('user_login'),
                                        'msg'       => [
                                                            'yes'       => trans('common.yes'),
                                                            'no'        => trans('common.no'),
                                                            'login_msg' => trans('common.login_your_account'),
                                                            'message'   => trans('common.error_mobile_number'),
                                                        ],
                                    ]);
        } elseif (!empty($mobile_user_exist)) {
            return response()->json([
                                        'flug'      => 0,
                                        'status'    => 2,
                                        'url'       => route('user_login'),
                                        'msg'       => [
                                                            'yes'       => trans('common.yes'),
                                                            'no'        => trans('common.no'),
                                                            'login_msg' => trans('common.login_your_account'),
                                                            'message'   => trans('user.applicant_exist'),
                                                        ],
                                    ]);
        } else {
            return response()->json(['flug' => 1, 'status' => 0, 'url' => '', 'message' => '']);
        }
    }

    /**
     * Check unique email.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return json
     */
    public function checkUniqueEmail(Request $request)
    {
        $email_exist = TenderApplicant::where('EMAIL', $request->email)->where('IS_ACTIVE', 1)->first();
        $email_user_exist = User::where('EMAIL', $request->email)->where('IS_ACTIVE', 1)->first();
        if (!empty($email_exist)) {
            return response()->json([
                                        'flug'      => 0,
                                        'status'    => 1,
                                        'url'       => route('user_login'),
                                        'msg'       => [
                                                            'yes'       => trans('common.yes'),
                                                            'no'        => trans('common.no'),
                                                            'login_msg' => trans('common.login_your_account'),
                                                            'message'   => trans('user.email_exists'),
                                                        ],
                                    ]);
        } elseif (!empty($email_user_exist)) {
            return response()->json([
                                        'flug'      => 0,
                                        'status'    => 2,
                                        'url'       => route('user_login'),
                                        'msg'       => [
                                                            'yes'       => trans('common.yes'),
                                                            'no'        => trans('common.no'),
                                                            'login_msg' => trans('common.login_your_account'),
                                                            'message'   => trans('user.applicant_exist'),
                                                        ],
                                    ]);
        } else {
            return response()->json(['flug' => 1, 'status' => 0, 'message' => '']);
        }
    }

    /**
     * Check unique NID.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return json
     */
    public function checkUniqueNid(Request $request)
    {
        $nid_exist = TenderApplicant::where('NID', $request->nid)->where('IS_ACTIVE', 1)->first();
        $nid_user_exist = Citizen::where('NID', $request->nid)->where('IS_ACTIVE', 1)->first();
        if (!empty($nid_exist)) {
            return response()->json([
                                        'flug'      => 0,
                                        'status'    => 1,
                                        'url'       => route('user_login'),
                                        'msg'       => [
                                                            'yes'       => trans('common.yes'),
                                                            'no'        => trans('common.no'),
                                                            'login_msg' => trans('common.login_your_account'),
                                                            'message'   => trans('user.nid_exists'),
                                                        ],
                                    ]);
        } elseif (!empty($nid_user_exist)) {
            return response()->json([
                                        'flug'      => 0,
                                        'status'    => 2,
                                        'url'       => route('user_login'),
                                        'msg'       => [
                                                            'yes'       => trans('common.yes'),
                                                            'no'        => trans('common.no'),
                                                            'login_msg' => trans('common.login_your_account'),
                                                            'message'   => trans('user.applicant_exist'),
                                                        ],
                                    ]);
        } else {
            return response()->json(['flug' => 1, 'status' => 0, 'message' => '']);
        }
    }

    /**
     * Get ward by area id.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return string
     */
    public function getWard(Request $request)
    {
        $zone_id = $request->get('zone');
        $ward = Zone::where('PARENT_ID', $zone_id)->where('IS_ACTIVE', 1)->get();

        $data = '<option value="">'.trans('common.form_select').'</option>';
        if (count($ward) > 0) {
            foreach ($ward as $value) {
                $data .= '<option value="'.$value->WARD_ID.'">'.$value->WARD_NAME_BN.'</option>';
            }
        }

        return $data;
    }

    /**
     * Get ward land by area id.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return json
     */
    public function getAreaLand(Request $request)
    {
        $zone_id = $request->get('zone');
        $ward = Zone::where('PARENT_ID', $zone_id)->where('IS_ACTIVE', 1)->get();
        $land = DB::table('land')
                        ->where('land.AREA', $zone_id)
                        ->where('land.IS_ACTIVE', 1)
                        ->whereNull('project_land.LAND_ID')
                        ->leftJoin('project_land', 'project_land.LAND_ID', '=', 'land.LAND_ID')
                        ->select('land.LAND_ID', 'land.LAND_NUMBER')
                        ->orderBy('land.LAND_ID', 'desc')
                        ->get();

        $data = '<option value="">'.trans('common.form_select').'</option>';
        $str = '<option value="">'.trans('common.form_select').'</option>';
        if (count($ward) > 0) {
            foreach ($ward as $value) {
                $data .= '<option value="'.$value->WARD_ID.'">'.$value->WARD_NAME_BN.'</option>';
            }
        }
        if (count($land) > 0) {
            foreach ($land as $val) {
                $str .= '<option value="'.$val->LAND_ID.'">'.$val->LAND_NUMBER.'</option>';
            }
        }

        return response()->json(['flag' => 1, 'ward' => $data, 'land' => $str]);
    }

    /**
     * Get mouza land by mouza id.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return string
     */
    public function getWardLand(Request $request)
    {
        $ward_id = $request->get('ward');
        $zone_id = $request->get('zone');
        $mouza = Mouza::where('WARD_ID', $ward_id)->where('IS_ACTIVE', 1)->get();
        $land = DB::table('land')
                        ->where('land.AREA', $zone_id)
                        ->where('land.WARD_ID', $ward_id)
                        ->where('land.IS_ACTIVE', 1)
                        ->whereNull('project_land.LAND_ID')
                        ->leftJoin('project_land', 'project_land.LAND_ID', '=', 'land.LAND_ID')
                        ->select('land.LAND_ID', 'land.LAND_NUMBER')
                        ->orderBy('land.LAND_ID', 'desc')
                        ->get();

        $data = '<option value="">'.trans('common.form_select').'</option>';
        $str = '<option value="">'.trans('common.form_select').'</option>';
        if (count($mouza) > 0) {
            foreach ($mouza as $value) {
                $data .= '<option value="'.$value->MOUZA_ID.'">'.$value->MOUZA_NAME_BN.'</option>';
            }
        }
        if (count($land) > 0) {
            foreach ($land as $val) {
                $str .= '<option value="'.$val->LAND_ID.'">'.$val->LAND_NUMBER.'</option>';
            }
        }

        return response()->json(['flag' => 1, 'mouza' => $data, 'land' => $str]);
    }

    /**
     * Get mouza land by mouza id.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return string
     */
    public function getMouzaLand(Request $request)
    {
        $zone_id = $request->get('zone');
        $ward_id = $request->get('ward');
        $mouza_id = $request->get('mouza');
        $land = Land::where('WARD_ID', $ward_id)
                            ->where('AREA', $zone_id)
                            ->where('MOUZA_ID', $mouza_id)
                            ->where('IS_ACTIVE', 1)
                            ->orderBy('LAND_ID', 'desc')
                            ->get();
        $land = DB::table('land')
                        ->where('land.AREA', $zone_id)
                        ->where('land.WARD_ID', $ward_id)
                        ->where('land.MOUZA_ID', $mouza_id)
                        ->where('land.IS_ACTIVE', 1)
                        ->whereNull('project_land.LAND_ID')
                        ->leftJoin('project_land', 'project_land.LAND_ID', '=', 'land.LAND_ID')
                        ->select('land.LAND_ID', 'land.LAND_NUMBER')
                        ->orderBy('land.LAND_ID', 'desc')
                        ->get();

        $str = '<option value="">'.trans('common.form_select').'</option>';
        if (count($land) > 0) {
            foreach ($land as $val) {
                $str .= '<option value="'.$val->LAND_ID.'">'.$val->LAND_NUMBER.'</option>';
            }
        }

        return response()->json(['flag' => 1, 'land' => $str]);
    }

    /**
     * Get district by division id.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return string
     */
    public function getdistrict(Request $request)
    {
        $division_id = $request->get('division');
        $district = District::where('DIVISION_ID', $division_id)->where('IS_ACTIVE', 1)->get();

        $data = '<option value="">'.trans('common.form_select').'</option>';
        if (count($district) > 0) {
            foreach ($district as $value) {
                $data .= '<option value="'.$value->DISTRICT_ID.'">'.$value->DISTRICT_ENAME.'</option>';
            }
        }

        return $data;
    }

    /**
     * Get mouza by ward id.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return string
     */
    public function getMouza(Request $request)
    {
        $ward_id = $request->get('ward');
        $mouza = Mouza::where('WARD_ID', $ward_id)->where('IS_ACTIVE', 1)->get();

        $data = '<option value="">'.trans('common.form_select').'</option>';
        if (count($mouza) > 0) {
            foreach ($mouza as $value) {
                $data .= '<option value="'.$value->MOUZA_ID.'">'.$value->MOUZA_NAME_BN.'</option>';
            }
        }

        return $data;
    }

    /**
     * Get thana by district id.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return string
     */
    public function getThana(Request $request)
    {
        $district_id = $request->get('district');
        $thana = Thana::where('DISTRICT_ID', $district_id)->where('IS_ACTIVE', 1)->get();

        $data = '<option value="">'.trans('common.form_select').'</option>';
        if (count($thana) > 0) {
            foreach ($thana as $value) {
                $data .= '<option value="'.$value->THANA_ID.'">'.$value->THANA_ENAME.'</option>';
            }
        }

        return $data;
    }

    /**
     * Get bank branch by branch id.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return string
     */
    public function getBankBranch(Request $request)
    {
        $bank = $request->get('bank');
        $branch = Bank::where('B_PARENT_ID', $bank)->where('IS_ACTIVE', 1)->get();

        $data = '<option value="">'.trans('common.form_select').'</option>';
        if (count($branch) > 0) {
            foreach ($branch as $value) {
                $data .= '<option value="'.$value->BANK_ID.'">'.$value->BANK_NAME.'</option>';
            }
        }

        return $data;
    }

    /**
     * Get land no by mouza.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return string
     */
    public function getLandno(Request $request)
    {
        $mouza_id = $request->get('mouza');
        $land = Land::where('id', $mouza_id)->where('IS_ACTIVE', 1)->orderBy('id', 'desc')->get();

        $data = '<option value="">'.trans('common.land_form_select').'</option>';
        if (count($land) > 0) {
            foreach ($land as $value) {
                $data .= '<option value="'.$value->id.'">'.$value->land_number.'</option>';
            }
        }

        return $data;
    }

    /**
     * Get land subcategory by category id.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return string
     */
    public function getLandSubcategory(Request $request)
    {
        $category = $request->get('category');
        $subcategory = LandCategory::where('PARENT_ID', $category)->where('IS_ACTIVE', 1)->get();

        $data = '<option value="">'.trans('common.form_select').'</option>';
        if (count($subcategory) > 0) {
            foreach ($subcategory as $value) {
                $data .= '<option value="'.$value->CAT_ID.'">'.$value->CAT_NAME_BN.'</option>';
            }
        }

        return $data;
    }

    /**
     * Get lease subcategory.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return string
     */
    public function getLeaseSubcategory(Request $request)
    {
        $category = $request->get('category');
        $subcategory = LeaseCategory::where('parent_id', $category)->where('IS_ACTIVE', 1)->orderBy('id', 'desc')->get();

        $data = '<option value="">'.trans('common.lease_form_select').'</option>';
        if (count($subcategory) > 0) {
            foreach ($subcategory as $value) {
                $data .= '<option value="'.$value->id.'">'.$value->bn_name.'</option>';
            }
        }

        return $data;
    }

    /**
     * Get user tender schedule by mobile no and category id.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return string
     */
    public function userTenderSchedule(Request $request)
    {
        $input = $request->all();
        $data = '<option value="">'.trans('common.form_select').'</option>';
        $user = User::where('MOBILE_NO', $input['mobile'])->select('USER_ID')->first();
        $applicant = TenderApplicant::where('APPLICANT_PHONE', $input['mobile'])->select('TE_APP_ID')->first();

        if (!empty($user)) {
            $citizen = Citizen::where('USER_ID', $user->USER_ID)->pluck('CITIZEN_ID');

            $data = '<option value="">'.trans('common.form_select').'</option>';
            $ctz_application = CitizenApplication::with(['getSchedule' => function ($query) {
                $query->with(['getProject', 'getCategory', 'getProjectType', 'getProjectDetail']);
            }])
                                                ->where('CITIZEN_ID', $citizen)
                                                ->where('PR_CATEGORY', $input['category'])
                                                ->where('IS_ACTIVE', 1)
                                                ->get();
            $ctz_property = CitizenProperty::with(['getSchedule' => function ($query) {
                $query->with(['getProject', 'getCategory', 'getProjectType', 'getProjectDetail']);
            }])
                                                ->where('CITIZEN_ID', $citizen)
                                                ->where('PR_CATEGORY', $input['category'])
                                                ->where('IS_ACTIVE', 1)
                                                ->get();

            if (count($ctz_application) > 0) {
                foreach ($ctz_application as $ca) {
                    $data .= '<option value="'.$citizen.'_CA_'.$ca->SCHEDULE_ID.'">';
                    $data .= $ca->getSchedule->getProject->PR_NAME_BN.' / ';
                    $data .= $ca->getSchedule->getCategory->CATE_NAME;
                    if (!empty($ca->getSchedule->getProjectDetail)) {
                        $data .= ' / '.$ca->getSchedule->getProjectType->TYPE_NAME.' / ';
                        $data .= $ca->getSchedule->getProjectDetail->PR_SSF_NO;
                    }
                    $data .= '</option>';
                }
            }
            if (count($ctz_property) > 0) {
                foreach ($ctz_property as $cp) {
                    $data .= '<option value="'.$citizen.'_CP_'.$cp->SCHEDULE_ID.'">';
                    $data .= $cp->getSchedule->getProject->PR_NAME_BN.' / ';
                    $data .= $cp->getSchedule->getCategory->CATE_NAME;
                    if (!empty($cp->getSchedule->getProjectDetail)) {
                        $data .= ' / '.$cp->getSchedule->getProjectType->TYPE_NAME.' / ';
                        $data .= $cp->getSchedule->getProjectDetail->PR_SSF_NO;
                    }
                    $data .= '</option>';
                }
            }
        } elseif (!empty($applicant)) {
            $data = '<option value="">'.trans('common.form_select').'</option>';
            $t_application = ApplicantProperty::with(['getSchedule' => function ($query) {
                $query->with(['getProject', 'getCategory', 'getProjectType', 'getProjectDetail']);
            }])
                                                ->where('TE_APP_ID', $applicant->TE_APP_ID)
                                                ->where('PR_CATEGORY', $input['category'])
                                                ->where('IS_ACTIVE', 1)
                                                ->get();

            if (count($t_application) > 0) {
                foreach ($t_application as $ta) {
                    $data .= '<option value="'.$applicant->TE_APP_ID.'_TA_'.$ta->SCHEDULE_ID.'">';
                    $data .= $ta->getSchedule->getProject->PR_NAME_BN.' / ';
                    $data .= $ta->getSchedule->getCategory->CATE_NAME;
                    if (!empty($ta->getSchedule->getProjectDetail)) {
                        $data .= ' / '.$ta->getSchedule->getProjectType->TYPE_NAME.' / ';
                        $data .= $ta->getSchedule->getProjectDetail->PR_SSF_NO;
                    }
                    $data .= '</option>';
                }
            }
        }

        return $data;
    }

    /**
     * Citizen number search.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return string
     */
    public function citizenNumberSearch(Request $request)
    {
        $mobile = $request->get('mobile_no');
        $data_user = $data_applicant = [];
        $user = User::with('getCitizen')
                    ->where('MOBILE_NO', 'like', '%'.$mobile.'%')
                    ->where('IS_ACTIVE', 1)
                    ->get();
        $applicant = TenderApplicant::where('APPLICANT_PHONE', 'like', '%'.$mobile.'%')
                                    ->where('IS_ACTIVE', 1)
                                    ->select('APPLICANT_PHONE', 'APPLICANT_NAME', 'TE_APP_ID')
                                    ->get();

        foreach ($user as $key => $u) {
            if (!empty($u->MOBILE_NO)) {
                $data_user[] = ['name' => $u->FULL_NAME, 'mobile' => $u->MOBILE_NO, 'value' => !empty($u->getCitizen) ? 'C_'.$u->getCitizen->CITIZEN_ID : ''];
            }
        }
        foreach ($applicant as $index => $app) {
            if (!empty($app->APPLICANT_PHONE)) {
                $data_applicant[] = ['name' => $app->APPLICANT_NAME, 'mobile' => $app->APPLICANT_PHONE, 'value' => 'T_'.$app->TE_APP_ID];
            }
        }

        return array_merge($data_user, $data_applicant);
        //echo '<pre>'; print_r( $user );
    }

    public function getUserManualDownload()
    {
        //PDF file is stored under project/public/download/info.pdf
        $file = public_path().'/user_manual/citizen_user_manual.pdf';

        return Response::download($file);
    }
}
