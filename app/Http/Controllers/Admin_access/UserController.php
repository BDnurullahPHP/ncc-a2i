<?php

namespace App\Http\Controllers\Admin_access;

use App\Http\Controllers\Controller;
use App\Models\Module;
use App\Models\ModuleGroup;
use App\Models\ModuleGroupLevel;
use App\Models\UserDetails;
use App\Ncc\Datatablehelper;
use App\Permission;
use App\Role;
use App\User;
use Auth;
use DB;
use Hash;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
use Response;
use View;

/**
 * Class     UserController.
 *
 * @author   Abdul awal <abdulawal@atilimited.net>
 */
class UserController extends Controller
{
    /**
     * Show all users.
     *
     * @return \Illuminate\View\View
     */
    public function userIndex()
    {
        $link_url = 'user_list'; // module url
        $data['access'] = Module::checkPrevilege($link_url); // get access info by module url
        return view('admin_access.user.index', $data);
    }

    /**
     * Get user source data.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return json
     */
    public function userData(Request $request)
    {
        $input = $request->all();
        $collection = User::getData($input);
        $output = Datatablehelper::userOutput($input, $collection);

        return response()->json($output);
    }

    /**
     * Render user create form.
     *
     * @return \Illuminate\View\View
     */
    public function createForm()
    {
        $data['role'] = Role::orderBy('ROLE_ID', 'desc')->lists('DISPLAY_NAME', 'ROLE_ID');
        $data['group'] = ModuleGroup::where('ORG_ID', 1)->lists('USERGRP_NAME', 'USERGRP_ID');

        return View::make('admin_access.user.create', $data)->render();
    }

    /**
     * Create user email.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return json
     */
    public function createUser(Request $request)
    {
        $data = $request->all();

        // Check email before insert
        $check_existing_email = User::where('EMAIL', $data['EMAIL'])->first();
        if (!empty($check_existing_email)) {
            return response()->json(['flug' => 0, 'message' => trans('user.email_exists')]);
            exit;
        }
        // Check username before insert
        $user_name_exist = User::where('USERNAME', $data['USERNAME'])->first();
        if (!empty($user_name_exist)) {
            return response()->json(['flug' => 0, 'message' => trans('user.username_exists')]);
            exit;
        }
        // Check mobile no before insert
        $mobile_exist = User::where('MOBILE_NO', $data['MOBILE_NO'])->first();
        if (!empty($mobile_exist)) {
            return response()->json(['flug' => 0, 'message' => trans('user.mobile_exists')]);
            exit;
        }

        $user = new User();
        $user->USER_TYPE = $data['USER_TYPE'];
        $user->USERGRP_ID = $data['group_id'];
        $user->USERLVL_ID = $data['groupLevel'];
        $user->FIRST_NAME = $data['FIRST_NAME'];
        $user->LAST_NAME = $data['LAST_NAME'];
        $user->FULL_NAME = $data['FIRST_NAME'].' '.$data['LAST_NAME'];
        $user->GENDER = $data['GENDER'];
        $user->USERNAME = $data['USERNAME'];
        $user->EMAIL = $data['EMAIL'];
        $user->PASSWORD = bcrypt($data['PASSWORD']);
        $user->MOBILE_NO = $data['MOBILE_NO'];
        $user->IS_ACTIVE = $data['IS_ACTIVE'];
        $user->CREATED_BY = Auth::user()->USER_ID;
        $user->save();

        if (!empty($data['role_id'])) {
            $user->attachRole($data['role_id']);
        }
        $user->save();

        return response()->json(['flug' => 1, 'message' => trans('user.user_create_message')]);
    }

    /**
     * Get user edit form.
     *
     * @param \Illuminate\Http\Request $request
     * @param string                   $id
     *
     * @return \Illuminate\View\View
     */
    public function userEditForm(Request $request, $id)
    {
        $data['user'] = User::find($id);
        $data['role'] = Role::orderBy('ROLE_ID', 'desc')->lists('DISPLAY_NAME', 'ROLE_ID');
        $data['role_user'] = DB::table('sa_role_user')->where('USER_ID', $id)->first();
        $data['group'] = ModuleGroup::where('ORG_ID', 1)->get();
        $data['group_level'] = ModuleGroupLevel::where('USERGRP_ID', $data['user']->USERGRP_ID)->get();

        return View::make('admin_access.user.edit')->with($data)->render();
    }

    /**
     * Update user data.
     *
     * @param \Illuminate\Http\Request $request
     * @param string                   $id
     *
     * @return json
     */
    public function updateUser(Request $request, $id)
    {
        $flug = 0;
        $message = '';
        $data = $request->all(); //print_r($data);exit;
        $user = User::find($id);
        $user->USERGRP_ID = $data['group_id'];
        $user->USERLVL_ID = $data['groupLevel'];
        $user->FIRST_NAME = $data['FIRST_NAME'];
        $user->LAST_NAME = $data['LAST_NAME'];
        $user->FULL_NAME = $data['FIRST_NAME'].' '.$data['LAST_NAME'];
        $user->GENDER = $data['GENDER'];
        $user->USER_TYPE = $data['USER_TYPE'];
        $user->IS_ACTIVE = $data['IS_ACTIVE'];

        // Check mobile no before update
        $mobile_exist = User::whereNotIn('USER_ID', [$id])->where('MOBILE_NO', $data['MOBILE_NO'])->first();
        if (!empty($mobile_exist)) {
            return response()->json(['flug' => 0, 'message' => trans('user.mobile_exists')]);
            exit;
        } else {
            $user->MOBILE_NO = $data['MOBILE_NO'];
        }
        // Check email no before update
        $check_existing_email = User::whereNotIn('USER_ID', [$id])->where('EMAIL', $data['EMAIL'])->first();
        if (!empty($check_existing_email)) {
            return response()->json(['flug' => 0, 'message' => trans('user.email_exists')]);
            exit;
        } else {
            $user->EMAIL = $data['EMAIL'];
        }
        // Check username no before update
        $user_name_exist = User::whereNotIn('USER_ID', [$id])->where('USERNAME', $data['USERNAME'])->first();
        if (!empty($user_name_exist)) {
            return response()->json(['flug' => 0, 'message' => trans('user.username_exists')]);
            exit;
        } else {
            $user->USERNAME = $data['USERNAME'];
        }
        // Check password before update
        if (!empty($data['PASSWORD'])) {
            $user->PASSWORD = bcrypt($data['PASSWORD']);
        } else {
            unset($data['PASSWORD']);
        }
        // End check password

        $user->save();

        $user_role = DB::table('sa_role_user')->where('USER_ID', $id)->get();
        if (!empty($user_role) && count($user_role) > 0) {
            DB::table('sa_role_user')->where('USER_ID', $id)->delete();
        }
        if (!empty($data['role_id'])) {
            $user->roles()->attach($data['role_id']);
        }

        return response()->json(['flug' => 1, 'message' => trans('common.update_success')]);
    }

    /**
     * Change user status.
     *
     * @param \Illuminate\Http\Request $request
     * @param string                   $id
     *
     * @return string
     */
    public function changeStatus(Request $request, $id)
    {
        $user = User::find($id);

        if ($user->IS_ACTIVE == 1) {
            $user->IS_ACTIVE = 0;
        } else {
            $user->IS_ACTIVE = 1;
        }
        $user->save();

        return trans('common.status_change');
    }

    /**
     * Delete user data.
     *
     * @param \Illuminate\Http\Request $request
     * @param string                   $id
     *
     * @return string
     */
    public function deleteUser(Request $request, $id)
    {
        $user = User::find($id);
        $user_details = UserDetails::where('USER_ID', '=', $user->USER_ID)->first();

        if (!empty($user_details) && !empty($user_details->IMAGE)) {
            $user_image = $user_details->IMAGE.'.'.$user_details->CONTENT_TYPE;
            if (file_exists(public_path().'/upload/user/'.$user_image)) {
                //chmod(public_path().'/upload/user/'.$user_image, 0755);
                unlink(public_path().'/upload/user/'.$user_image);
            }
        }
        $user->delete();

        return trans('common.delete_data');
    }

    /**
     * Show user profile.
     *
     * @param \Illuminate\Http\Request $request
     * @param string                   $id
     *
     * @return \Illuminate\View\View
     */
    public function userProfile(Request $request, $id)
    {
        $title = 'User prifile';
        if (!Auth::user()->hasRole('admin')) {
            if (Auth::user()->USER_ID != $id) {
                abort(404);
            }
        }
        $user_info = User::where('USER_ID', $id)->with('getDetails')->first();

        return view('admin_access.user.profile', compact('title', 'user_info'));
    }

    /**
     * Get profile overview data.
     *
     * @param \Illuminate\Http\Request $request
     * @param string                   $id
     *
     * @return \Illuminate\View\View
     */
    public function profileOverview(Request $request, $id)
    {
        $user_info = User::where('USER_ID', $id)->with('getDetails')->first();

        return View::make('admin_access.user.profile_overview', compact('user_info'))->render();
    }

    /**
     * Update user password.
     *
     * @param \Illuminate\Http\Request $request
     * @param string                   $id
     *
     * @return json
     */
    public function updatePassword(Request $request, $id)
    {
        $flug = 0;
        $message = '';
        $data = $request->all();
        $hash = User::where('USER_ID', $id)->pluck('PASSWORD');

        $current_pass = $data['current_password'];
        if (Hash::check($current_pass, $hash)) {
            $user = User::find($id);
            $user->PASSWORD = bcrypt($data['new_password']);
            $user->save();
            $flug = 1;
            $message = trans('user.password_update_success');
        } else {
            $flug = 0;
            $message = trans('user.current_password_not_match');
        }

        return response()->json(['flug' => $flug, 'message' => $message]);
    }

    /**
     * Update user profile information.
     *
     * @param \Illuminate\Http\Request $request
     * @param string                   $id
     *
     * @return json
     */
    public function updateProfile(Request $request, $id)
    {
        $flug = 0;
        $message = '';
        $data = $request->all();
        $user = User::find($id);
        $user->FIRST_NAME = $data['FIRST_NAME'];
        $user->LAST_NAME = $data['LAST_NAME'];
        $user->FULL_NAME = $data['FIRST_NAME'].' '.$data['LAST_NAME'];

        // Check mobile no before update
        $mobile_exist = User::whereNotIn('USER_ID', [$id])->where('MOBILE_NO', $data['MOBILE_NO'])->first();
        if (!empty($mobile_exist)) {
            return response()->json(['flug' => 0, 'message' => trans('user.mobile_exists')]);
            exit;
        } else {
            $user->MOBILE_NO = $data['MOBILE_NO'];
        }
        // Check username before update
        $user_name_exist = User::whereNotIn('USER_ID', [$id])->where('USERNAME', $data['USERNAME'])->first();
        if (!empty($user_name_exist)) {
            return response()->json(['flug' => 0, 'message' => trans('user.username_exists')]);
            exit;
        } else {
            $user->USERNAME = $data['USERNAME'];
        }

        $user->save();

        $details_exists = UserDetails::where('USER_ID', $id)->first();
        if (!empty($details_exists)) {
            $user_details = UserDetails::find($details_exists->UD_ID);
        } else {
            $user_details = new UserDetails();
        }
        $user_details->USER_ID = $id;
        $user_details->DESIGNATION = $data['DESIGNATION'];
        $user_details->ADDRESS = $data['ADDRESS'];
        $user_details->save();

        return response()->json(['flug' => 1, 'message' => trans('common.update_success')]);
    }

    /**
     * Update user profile.
     *
     * @param \Illuminate\Http\Request $request
     * @param string                   $id
     *
     * @return json
     */
    public function updateProfilePicture(Request $request, $id)
    {
        $flug = 0;
        $message = '';
        $details_exists = UserDetails::where('USER_ID', $id)->first();
        if (!empty($details_exists)) {
            $details = UserDetails::find($details_exists->UD_ID);
            $user_image = $details->IMAGE.'.'.$details->CONTENT_TYPE;
            if (!empty($details->IMAGE) && file_exists(public_path().'/upload/user/'.$user_image)) {
                // Change permission before delete it
                //chmod(public_path().'/upload/user/'.$user_image, 0755);
                unlink(public_path().'/upload/user/'.$user_image);
            }
        } else {
            $details = new UserDetails();
            $details->USER_ID = $id;
        }

        $image = $request->file('image');
        $img_thumb = uniqid('user-');
        $img_ext = $image->getClientOriginalExtension();
        $img_ext = strtolower($img_ext);
        $img_link_name = $img_thumb.'.'.$img_ext;
        $details->IMAGE = $img_thumb;
        $details->CONTENT_TYPE = $img_ext;

        // Save image to filesystem
        $img = Image::make($image->getRealPath());
        $img->resize(160, 160)->save(public_path().'/upload/user/'.$img_link_name);
        // End save image to filesystem

        $details->save();
        $data = [
                        'flug'      => 1,
                        'message'   => trans('common.update_success'),
                        'image'     => $details->IMAGE.'.'.$details->CONTENT_TYPE,
                    ];

        return response()->json($data);
    }
}
