<?php

namespace App\Http\Controllers\Admin_access;

use App\Http\Controllers\Controller;
use App\Models\AccountVaucherChd;
use App\Models\AccountVaucherMst;
use App\Models\AccountVnLedger;
use App\Models\Bank;
use App\Models\Citizen;
use App\Models\CitizenApplication;
use App\Models\CitizenProperty;
use App\Models\District;
use App\Models\Division;
use App\Models\Notification;
use App\Models\Particular;
use App\Models\PaymentResponse;
use App\Models\ProjectCategory;
use App\Models\Tender;
use App\Models\TenderApplicant;
use App\Models\TenderInstallment;
use App\Models\TenderParticulars;
use App\Models\TenderSchedule;
use App\Models\Thana;
use App\Ncc\Datatablehelper;
use App\User;
use Auth;
use DB;
use Illuminate\Http\Request;
use Response;
use Session;
use View;

/**
 * Class     CitizenController.
 *
 * @author   Abdul awal <abdulawal@atilimited.net>
 */
class CitizenController extends Controller
{
    /**
     * Show citizen rent payment page.
     *
     * @return \Illuminate\View\View
     */
    public function rentPayment()
    {
        $data['citizen'] = Auth::user()->load('getCitizen');
        $data['thana'] = Thana::where('THANA_ID', $data['citizen']->getCitizen->THANA_ID)->pluck('THANA_ENAME');
        $data['citizen_property'] = CitizenProperty::getPropertyData($data['citizen']->getCitizen->CITIZEN_ID);

        return view('citizen.payment.index')->with($data);
    }

    /**
     * Show citizen rent pending data.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return string
     */
    public function rentPendingMonth(Request $request)
    {
        $citizen = Auth::user()->load('getCitizen');
        $schedule = $request->get('schedule_id');
        $voucher = AccountVnLedger::rentPendingMonth($citizen->getCitizen->CITIZEN_ID, $schedule);

        $voucher_dt = '<option value="">'.trans('common.form_select').'</option>';
        foreach ($voucher as $key => $vc) {
            if ($vc->credit_amount != $vc->debit_amount) {
                $voucher_dt .= '<option value="'.$vc->TRX_TRAN_NO.'">'.date('M-Y', strtotime($vc->VLEDGER_DT)).'</option>';
            }
        }

        return $voucher_dt;
    }

    /**
     * Show citizen rent pay form.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\View\View
     */
    public function rentPayForm(Request $request)
    {
        $vchild = $request->get('vchild');
        $schedule = $request->get('schedule_id');
        $data['citizen_user'] = Auth::user()->load('getCitizen');
        $data['te_schedule'] = TenderSchedule::where('SCHEDULE_ID', $schedule)
                                                ->with(['getProject', 'getCategory', 'getProjectType', 'getProjectDetail'])
                                                ->first();

        $amount = AccountVnLedger::where('TRX_TRAN_NO', $vchild)
                                    ->select('TRX_TRAN_NO', DB::raw('SUM(DR_AMT) as dr_amount'), DB::raw('SUM(CR_AMT) as cr_amount'))
                                    ->first();
        $data['total_amount'] = $amount->cr_amount - $amount->dr_amount;
        $data['particular'] = Particular::where('PARTICULAR_ID', 5)->pluck('PARTICULAR_NAME');

        return view('citizen.payment.rent_pay_form', compact('vchild', 'schedule'))->with($data)->render();
    }

    /**
     * Rent payment.
     *
     * @param \Illuminate\Http\Request $request
     * @param string                   $vc
     * @param string                   $sc
     * @param string                   $ctz
     *
     * @return response
     */
    public function rentPaymentSubmit(Request $request, $vc, $sc, $ctz)
    {
        $data = $request->all();
        $user = Auth::user()->USER_ID;
        $payment = PaymentResponse::rentPayment($data, $vc, $sc, $ctz, $user);

        if (isset($data['error'])) {
            Session::flash('error', trans('common.payment_error'));
        } else {
            Session::flash('success', trans('common.payment_success'));

            return redirect()->route('payment_invoice', [$payment, 5, $sc, 'citizen']);
        }

        return redirect()->route('citizen_rent_payment');
    }

    /**
     * Show citizen rent history page.
     *
     * @return \Illuminate\View\View
     */
    public function rentHistory()
    {
        $data['citizen_user'] = Auth::user()->load('getCitizen');
        $data['year_range'] = AccountVaucherMst::getCitizenYearRange($data['citizen_user']->getCitizen->CITIZEN_ID);
        $data['citizen_property'] = CitizenProperty::getPropertyData($data['citizen_user']->getCitizen->CITIZEN_ID);

        return view('citizen.payment.history', $data)->render();
    }

    /**
     * Rent history data.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return json
     */
    public function rentHistoryData(Request $request)
    {
        $input = $request->all();
        $citizen = Auth::user()->load('getCitizen');
        $collection = AccountVnLedger::rentMonthData($citizen->getCitizen->CITIZEN_ID, $input);
        $output = Datatablehelper::rentMonthOutput($input, $collection);

        return response()->json($output);
    }

    /**
     * Citizen services.
     *
     * @return \Illuminate\View\View
     */
    public function myServices()
    {
        $category = ProjectCategory::where('IS_ACTIVE', 1)->get();

        return view('citizen.myServices', compact('category'));
    }

    /**
     * Citizen service data.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return json
     */
    public function serviceData(Request $request)
    {
        $input = $request->all();
        $citizen = Auth::user()->load('getCitizen');
        $collection = CitizenProperty::citizenServiceData($citizen->getCitizen->CITIZEN_ID, $input);
        $output = Datatablehelper::citizenServiceOutput($input, $collection);

        return response()->json($output);
    }

    /**
     * Show applicant payment history.
     *
     * @return \Illuminate\View\View
     */
    public function paymentHistory()
    {
        return view('applicant.payment_history');
    }

    /**
     * Get tender applicant payment history data.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return json
     */
    public function paymentData(Request $request)
    {
        $input = $request->all();
        $voucher = AccountVaucherMst::where('CITIZEN_ID', Auth::user()->load('getCitizen')->CITIZEN_ID)->get();
        $collection = PaymentResponse::getHistoryData($input, $voucher);
        $output = Datatablehelper::applicantPaymentOutput($input, $collection);

        return response()->json($output);
    }

    /**
     * Show application form history.
     *
     * @return \Illuminate\View\View
     */
    public function applicationFormHistory()
    {
        $applicant_id = Auth::user()->load('getCitizen');
        $project = CitizenApplication::with(['getSchedule' => function ($query) {
            $query->with(['getProject', 'getCategory', 'getProjectType', 'getProjectDetail']);
        }])
                                    ->where('CITIZEN_ID', $applicant_id->getCitizen->CITIZEN_ID)
                                    ->where('IS_ACTIVE', 1)
                                    ->get();

        return view('applicant.form_history', compact('project'));
    }

    /**
     * Get application form data.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\View\View
     */
    public function getFormData(Request $request)
    {
        $schedule_id = $request->get('schedule_id');
        $data['citizen_user'] = Auth::user()->load('getCitizen');
        $data['app_project'] = TenderSchedule::with(['getProject'])
                                                ->where('SCHEDULE_ID', $schedule_id)
                                                ->first();
        $data['tender_name'] = Tender::where('TENDER_ID', $data['app_project']->TENDER_ID)
                                        ->pluck('TENDER_TITLE');
        $data['user_district'] = District::where('DISTRICT_ID', $data['citizen_user']->getCitizen->DISTRICT_ID)
                                            ->pluck('DISTRICT_ENAME');
        $data['user_thana'] = Thana::where('THANA_ID', $data['citizen_user']->getCitizen->THANA_ID)
                                            ->pluck('THANA_ENAME');

        return view('applicant.app_form_data')->with($data)->render();
    }

    /**
     * Show application notification history.
     *
     * @return \Illuminate\View\View
     */
    public function applicationNotification()
    {
        $citizen = Auth::user()->load('getCitizen');
        $project = CitizenApplication::with(['getSchedule' => function ($query) {
            $query->with(['getProject', 'getCategory', 'getProjectType', 'getProjectDetail']);
        }])
                                        ->where('CITIZEN_ID', $citizen->getCitizen->CITIZEN_ID)
                                        ->where('IS_ACTIVE', 1)
                                        ->get();

        return view('applicant.notification', compact('project'));
    }

    /**
     * Get applicant notification data.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\View\View
     */
    public function appNotificationData(Request $request)
    {
        $schedule_id = $request->get('schedule_id');
        $data['citizen'] = Auth::user()->load('getCitizen');
        $data['te_schedule'] = TenderSchedule::with('getProject')
                                                ->where('SCHEDULE_ID', $schedule_id)
                                                ->first();
        $data['app_property'] = CitizenApplication::where('CITIZEN_ID', $data['citizen']->getCitizen->CITIZEN_ID)
                                                    ->where('SCHEDULE_ID', $schedule_id)
                                                    ->where('IS_ACTIVE', 1)
                                                    ->first();
        $applicant_voucher = AccountVaucherMst::where('CITIZEN_ID', $data['citizen']->getCitizen->CITIZEN_ID)
                                                ->lists('VOUCHER_NO');
        foreach ($applicant_voucher as $key => $apv) {
            $app_voucher[$key] = $apv;
        }

        if ($data['app_property']->IS_LEASE == 1) {
            $data['app_voucher_chd'] = AccountVaucherChd::leaseVoucherChd($app_voucher, $schedule_id);
        } else {
            $data['app_voucher_chd'] = AccountVaucherChd::tenderVoucherChd($app_voucher, $schedule_id);
            $data['installment'] = TenderInstallment::where('CITIZEN_ID', $data['citizen']->getCitizen->CITIZEN_ID)
                                                    ->where('SCHEDULE_ID', $schedule_id)
                                                    ->get();
        }

        return view('applicant.notification_data', compact('schedule_id'))->with($data)->render();
    }

    /**
     * Show applicant installment history.
     *
     * @return \Illuminate\View\View
     */
    public function installmentHistory()
    {
        $citizen = Auth::user()->load('getCitizen');
        $schedule = TenderInstallment::with(['getSchedule' => function ($query) {
            $query->with(['getProject', 'getCategory', 'getProjectType', 'getProjectDetail']);
        }])
                                        ->where('CITIZEN_ID', $citizen->getCitizen->CITIZEN_ID)
                                        ->groupBy('SCHEDULE_ID')
                                        ->get();

        return view('applicant.installment_history', compact('schedule'))->render();
    }

    /**
     * Show installment data by schedule id.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\View\View
     */
    public function installmentData(Request $request)
    {
        $schedule_id = $request->get('schedule_id');
        $data['citizen'] = Auth::user()->load('getCitizen');
        $data['app_installment'] = TenderInstallment::where('CITIZEN_ID', $data['citizen']->getCitizen->CITIZEN_ID)
                                                    ->where('SCHEDULE_ID', $schedule_id)
                                                    ->orderBy('INSTALLMENT', 'asc')
                                                    ->get();

        return view('applicant.installment_data')->with($data)->render();
    }

    /**
     * Show citizen lease information.
     *
     * @return \Illuminate\View\View
     */
    public function leaseProperty()
    {
        $citizen = Auth::user()->load('getCitizen');
        $lease_project = CitizenProperty::with(['getProject'])
                                    ->leftJoin('lease_validity', 'lease_validity.SCHEDULE_ID', '=', 'sa_citizen_property.SCHEDULE_ID')
                                    ->where('sa_citizen_property.CITIZEN_ID', $citizen->getCitizen->CITIZEN_ID)
                                    ->where('lease_validity.CITIZEN_ID', $citizen->getCitizen->CITIZEN_ID)
                                    ->where('sa_citizen_property.IS_LEASE', 1)
                                    ->where('sa_citizen_property.IS_ACTIVE', 1)
                                    ->get();

        return view('citizen.lease_property', compact('lease_project'));
    }

    /**
     * Show type wise tender notice.
     *
     * @return \Illuminate\View\View
     */
    public function noticeList()
    {
        return view('admin_access.tender_apply.index');
    }

    /**
     * Tender notice table select.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\View\View
     */
    public function noticeTableSelect(Request $request)
    {
        $category = $request->get('category');

        return view('admin_access.tender_apply.table', compact('category'))->render();
    }

    /**
     * Get tender schedule or lease data.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return json
     */
    public function noticeTableData(Request $request)
    {
        $input = $request->all();
        $input['from_admin'] = true;

        if ($input['group'] == 'tender') {
            $collection = TenderSchedule::getTenderData($input);
            $output = Datatablehelper::tenderscheduleOutput($input, $collection);
        } else {
            $collection = TenderSchedule::getLeaseData($input);
            $output = Datatablehelper::leaseTenderscheduleOutput($input, $collection);
        }

        return response()->json($output);
    }

    /**
     * Tender apply form.
     *
     * @param \Illuminate\Http\Request $request
     * @param string                   $sc      Tender schedule id
     * @param string                   $type    Tender|Lease
     *
     * @return \Illuminate\View\View
     */
    public function tenderApplyForm(Request $request, $sc, $type)
    {
        $schedule_data = TenderSchedule::find($sc);
        $data['schedule_id'] = $sc;
        $data['division'] = Division::lists('DIVISION_ENAME', 'DIVISION_ID');
        $data['bank_list'] = Bank::lists('BANK_NAME', 'BANK_ID');
        if ($type == 'tender') {
            $data['schedule_price'] = TenderParticulars::where('SCHEDULE_ID', $schedule_data->SCHEDULE_ID)
                                                        ->where('PARTICULAR_ID', 4)
                                                        ->pluck('PARTICULAR_AMT');
            $data['guarantee'] = TenderParticulars::where('SCHEDULE_ID', $schedule_data->SCHEDULE_ID)
                                                        ->where('PARTICULAR_ID', 3)
                                                        ->pluck('PARTICULAR_AMT');
        } else {
            $data['schedule_price'] = TenderParticulars::where('SCHEDULE_ID', $schedule_data->SCHEDULE_ID)
                                                        ->where('PARTICULAR_ID', 7)
                                                        ->pluck('PARTICULAR_AMT');
            $data['guarantee'] = TenderParticulars::where('SCHEDULE_ID', $schedule_data->SCHEDULE_ID)
                                                        ->where('PARTICULAR_ID', 6)
                                                        ->pluck('PARTICULAR_AMT');
        }

        return view('admin_access.tender_apply.form', compact('type'))->with($data)->render();
    }

    /**
     * Save application data.
     *
     * @param \Illuminate\Http\Request $request
     * @param string                   $type
     * @param string                   $sc
     *
     * @return json
     */
    public function tenderApplyData(Request $request, $sc, $type)
    {
        $data = $request->all(); //echo '<pre>';print_r($data);exit;
        $data['TYPE'] = $type;
        $data['SCHEDULE_ID'] = $sc;
        $user = Auth::user()->USER_ID;
        $tenderapplicant = TenderApplicant::saveTenderApplication($data, $user);

        return response()->json(['flag' => 1, 'data' => trans('common.save_success')]);
    }
}
