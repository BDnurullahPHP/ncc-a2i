<?php

namespace App\Http\Controllers\Admin_access;

use App\Http\Controllers\Controller;
use App\Models\Notification;
use Illuminate\Http\Request;
use View;

/**
 * Class     NotificationController.
 *
 * @author   Abdul awal <abdulawal@atilimited.net>
 */
class NotificationController extends Controller
{
    /**
     * Show notification details.
     *
     * @param \Illuminate\Http\Request $request
     * @param string                   $id
     *
     * @return \Illuminate\View\View
     */
    public function getView(Request $request, $id)
    {
        $notification = Notification::where('NOTIFICATION_ID', $id)->where('IS_VIEW', 0)->first();
        if (!empty($notification)) {
            $notification = Notification::find($id);
            $notification->IS_VIEW = 1;
            $notification->save();
        }

        $details = Notification::where('NOTIFICATION_ID', $id)->pluck('DETAILS');

        return View::make('admin_access.notification.view', compact('details'))->render();
    }
}
