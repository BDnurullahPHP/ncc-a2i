<?php

namespace App\Http\Controllers\Admin_access;

use App\Http\Controllers\Controller;
use App\Models\AccountVaucherMst;
use App\Models\ApplicantProperty;
use App\Models\CategoryParticular;
use App\Models\Citizen;
use App\Models\CitizenApplication;
use App\Models\District;
use App\Models\Land;
use App\Models\Lease;
use App\Models\LeaseCategory;
use App\Models\LeaseDemesne;
use App\Models\LeaseValidity;
use App\Models\Module;
use App\Models\Officer;
use App\Models\Project;
use App\Models\ProjectCategory;
use App\Models\ProjectLocation;
use App\Models\Tender;
use App\Models\TenderApplicant;
use App\Models\TenderDateTime;
use App\Models\TenderLetter;
use App\Models\TenderLocation;
use App\Models\TenderParticulars;
use App\Models\TenderSchedule;
use App\Models\Thana;
use App\Models\Zone;
use App\Ncc\Datatablehelper;
use App\Ncc\Helpers;
use Auth;
use Illuminate\Http\Request;
use View;

class LeaseController extends Controller
{
    /**
     * Show all lease.
     *
     * @return \Illuminate\View\View
     */
    public function leaseIndex()
    {
        $link_url = 'lease_list'; // module url
        $data['access'] = Module::checkPrevilege($link_url); // get access info by module url
        return view('admin_access.lease.index', $data);
    }

    /**
     * Get lease data.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return json
     */
    public function leaseData(Request $request)
    {
        $input = $request->all();
        $collection = Project::getDataLease($input);
        $output = Datatablehelper::leaseOutput($input, $collection);

        return response()->json($output);
    }

    /**
     * Render lease create form.
     *
     * @return \Illuminate\View\View
     */
    public function leaseCreateForm()
    {
        $area = Zone::where('PARENT_ID', 0)
                    ->where('IS_ACTIVE', 1)
                    ->lists('WARD_NAME_BN', 'WARD_ID');

        return View::make('admin_access.lease.create', compact('area'))->render();
    }

    /**
     * Create lease.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return json
     */
    public function leaseCreate(Request $request)
    {
        $user = Auth::user()->USER_ID;
        $data = $request->all();

        // Check Mohal no before Insert
        $check = Project::where('PR_UD_ID', $data['PR_UD_ID'])->first();
        if (!empty($check)) {
            return response()->json(['flug' => 0, 'message' => trans('lease.mahal_exists')]);
            exit;
        } else {
            $project = Project::createMahal($data, $user);
        }

        return response()->json(['flug' => 1, 'message' => trans('common.save_success')]);
    }

    /**
     * Lease tender notice.
     *
     * @return \Illuminate\View\View
     */
    public function leaseNotice()
    {
        $link_url = 'lease_notice'; // module url
        $data['access'] = Module::checkPrevilege($link_url); // get access info by module url
        return view('admin_access.tender_lease.index', $data);
    }

    /**
     * Get lease tender notice data.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return json
     */
    public function leasePublishData(Request $request)
    {
        $input = $request->all();
        $collection = Tender::getLeaseData($input);
        $output = Datatablehelper::leaseTenderOutput($input, $collection);

        return response()->json($output);
    }

    /**
     * Render lease tender create form.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return string
     */
    public function leaseTenderCreateForm(Request $request)
    {
        $data['category'] = ProjectCategory::where('IS_LEASE', 1)->first();
        $data['location'] = ProjectLocation::where('IS_ACTIVE', 1)
                                                    ->lists('LOCATION_NAME', 'LOCATION_ID');
        $data['project_list'] = Project::getLeaseProject();
        $data['tender_list'] = Tender::where('IS_LEASE', 1)->where('IS_ACTIVE', 1)->get();
        $data['cat_particular'] = CategoryParticular::with('getParticular')
                                                    ->where('PR_CATEGORY', $data['category']->PR_CATEGORY)
                                                    ->orderBy('PARTICULAR_ID', 'asc')
                                                    ->get();

        return View::make('admin_access.tender_lease.create')->with($data)->render();
    }

    /**
     * Pre schedule data.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return json
     */
    public function tenderScheduleData(Request $request)
    {
        $data['tender_id'] = $request->get('tender_id');
        $data['category'] = ProjectCategory::where('IS_LEASE', 1)->first();
        $data['cat_particular'] = CategoryParticular::with('getParticular')
                                                    ->where('PR_CATEGORY', $data['category']->PR_CATEGORY)
                                                    ->orderBy('PARTICULAR_ID', 'asc')
                                                    ->get();

        if (!empty($data['tender_id'])) {
            $data['project_list'] = TenderSchedule::with('getProject')
                                                    ->Where('TENDER_ID', $data['tender_id'])->get();

            // Render tender form with data
            $data['tender_data'] = Tender::find($data['tender_id']);
            $data['location'] = ProjectLocation::with('getTenderLocation')->where('IS_ACTIVE', 1)->get();
            $data['tender_date'] = TenderDateTime::where('TENDER_ID', $data['tender_id'])->get();
            $data['te_max_clause'] = TenderDateTime::where('TENDER_ID', $data['tender_id'])->max('CLAUSE');

            $tender_location = TenderLocation::where('TENDER_ID', $data['tender_id'])
                                                    ->where('TYPE', 1)
                                                    ->select('TE_LOC_ID', 'LOCATION_ID', 'TYPE')
                                                    ->get();
            $tender_cp_location = TenderLocation::where('TENDER_ID', $data['tender_id'])
                                                    ->where('TYPE', 2)
                                                    ->select('TE_LOC_ID', 'LOCATION_ID', 'TYPE')
                                                    ->get();
            $data['tender_edit'] = false;
            $data['te_location'] = [];
            $data['te_copy_location'] = [];
            foreach ($tender_location as $inx => $tlc) {
                $data['te_location'][$inx] = $tlc->LOCATION_ID;
            }
            foreach ($tender_cp_location as $key => $tcl) {
                $data['te_copy_location'][$key] = $tcl->LOCATION_ID;
            }

            $project = View::make('admin_access.tender_lease.pre_schedule_list')->with($data)->render();
            $tender = View::make('admin_access.tender_lease.edit_tender_fields')->with($data)->render();
        } else {
            $data['project_list'] = Project::getLeaseProject();
            $data['location'] = ProjectLocation::where('IS_ACTIVE', 1)->lists('LOCATION_NAME', 'LOCATION_ID');

            $project = View::make('admin_access.tender_lease.pre_schedule_list')->with($data)->render();
            $tender = View::make('admin_access.tender_lease.tender_fields')->with($data)->render();
        }

        return response()->json(['flug' => 1, 'project' => $project, 'tender' => $tender]);
    }

    /**
     * Create lease tender data.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return json
     */
    public function leaseTenderCreate(Request $request)
    {
        $data = $request->all();
        $user = Auth::user()->USER_ID;
        $tender_no = $data['tender_no'];

        // Check tender no before insert
        $tender_exist = Tender::where('TENDER_NO', $tender_no)->first();
        if (!empty($tender_exist)) {
            return response()->json(['flug' => 0, 'message' => trans('tender.tender_exists')]);
            exit;
        } else {
            $tender = Tender::leaseTenderData($data, $user);
        }

        return response()->json(['flug' => 1, 'message' => trans('common.save_success')]);
    }

    /**
     * Change lease tender status.
     *
     * @param \Illuminate\Http\Request $request
     * @param string                   $id
     *
     * @return string
     */
    public function leaseTenderStatus(Request $request, $id)
    {
        $tender = Tender::find($id);

        if ($tender->IS_ACTIVE == 1) {
            $tender->IS_ACTIVE = 0;
        } else {
            $tender->IS_ACTIVE = 1;
        }
        $tender->save();

        return trans('common.status_change');
    }

    /**
     * Lease tender edit.
     *
     * @param \Illuminate\Http\Request $request
     * @param string                   $id
     *
     * @return \Illuminate\View\View
     */
    public function leaseTenderEdit(Request $request, $id)
    {
        $data['tender_edit'] = true;
        $data['tender_id'] = $id;
        $data['sc_project'] = [];
        $data['project_cat'] = [];
        $data['te_location'] = [];
        $data['te_copy_location'] = [];
        $data['tender_data'] = Tender::find($id);
        $data['schedule'] = TenderSchedule::where('TENDER_ID', $id)->first();
        $data['category'] = ProjectCategory::where('IS_LEASE', 1)->first();
        $data['location'] = ProjectLocation::with('getTenderLocation')->where('IS_ACTIVE', 1)->get();
        $data['cat_particular'] = CategoryParticular::with('getParticular')
                                                    ->where('PR_CATEGORY', $data['category']->PR_CATEGORY)
                                                    ->orderBy('PARTICULAR_ID', 'asc')
                                                    ->get();
        $data['project_list'] = Project::where('IS_LEASE', '=', 1)->get();
        $tender_location = TenderLocation::where('TENDER_ID', $id)
                                                ->where('TYPE', 1)
                                                ->select('TE_LOC_ID', 'LOCATION_ID', 'TYPE')
                                                ->get();
        $tender_cp_location = TenderLocation::where('TENDER_ID', $id)
                                                ->where('TYPE', 2)
                                                ->select('TE_LOC_ID', 'LOCATION_ID', 'TYPE')
                                                ->get();
        $data['tender_date'] = TenderDateTime::where('TENDER_ID', $id)->get();
        $data['te_max_clause'] = TenderDateTime::where('TENDER_ID', $id)->max('CLAUSE');
        $sc_project = TenderSchedule::where('TENDER_ID', $data['tender_id'])->get();

        foreach ($tender_location as $inx => $tlc) {
            $data['te_location'][$inx] = $tlc->LOCATION_ID;
        }
        foreach ($tender_cp_location as $key => $tcl) {
            $data['te_copy_location'][$key] = $tcl->LOCATION_ID;
        }
        foreach ($sc_project as $k => $ts) {
            $data['project_cat'][] = $ts->PR_CATEGORY;
        }
        foreach ($sc_project as $key => $scp) {
            $data['sc_project'][] = $scp->PROJECT_ID;
            $data['tender_schedule'][$scp->PROJECT_ID] = $scp->SCHEDULE_ID;

            $particular_obj = new \stdClass();
            $particular_obj->SCHEDULE_ID = $scp->SCHEDULE_ID;
            $particular_obj->PROJECT_ID = $scp->PROJECT_ID;
            $tender_particular[] = $particular_obj;
        }
        foreach ($tender_particular as $index => $tp) {
            $t_particular = TenderParticulars::where('SCHEDULE_ID', $tp->SCHEDULE_ID)
                                                    ->select('TE_PART_ID', 'SCHEDULE_ID')
                                                    ->get();
            foreach ($t_particular as $ky => $tenp) {
                $data['tendr_part'][$tp->PROJECT_ID][$ky] = $tenp->TE_PART_ID;
            }
        }

        return View::make('admin_access.tender_lease.edit')->with($data)->render();
    }

    /**
     * Update lease tender data.
     *
     * @param \Illuminate\Http\Request $request
     * @param string                   $id
     *
     * @return json
     */
    public function leaseTenderUpdate(Request $request, $id)
    {
        $data = $request->all();
        $user = Auth::user()->USER_ID;
        $tender_no = $data['tender_no'];

        // Check tender no before update
        $tender_exist = Tender::whereNotIn('TENDER_ID', [$id])->where('TENDER_NO', $tender_no)->first();
        if (!empty($tender_exist)) {
            return response()->json(['flug' => 0, 'message' => trans('tender.tender_exists')]);
            exit;
        } else {
            $tender = Tender::leaseTenderData($data, $user);
        }

        return response()->json(['flug' => 1, 'message' => trans('common.update_success')]);
    }

    /**
     * Delete lease tender date data.
     *
     * @param \Illuminate\Http\Request $request
     * @param string                   $id
     *
     * @return string
     */
    public function leaseTenderDateDelete(Request $request, $id)
    {
        TenderDateTime::where('TENDER_DT_ID', $id)->delete();

        return trans('common.delete_data');
    }

    /**
     * Delete lease tender schedule data.
     *
     * @param \Illuminate\Http\Request $request
     * @param string                   $id
     *
     * @return string
     */
    public function leaseTenderScheduleDelete(Request $request, $id)
    {
        TenderSchedule::where('PROJECT_ID', $id)->delete();

        return trans('common.delete_data');
    }

    /**
     * Delete lease tender data.
     *
     * @param \Illuminate\Http\Request $request
     * @param string                   $id
     *
     * @return string
     */
    public function leaseTenderDelete(Request $request, $id)
    {
        Tender::find($id)->delete();

        return trans('common.delete_data');
    }

    /**
     * Show all lease category.
     *
     * @return \Illuminate\View\View
     */
    public function leaseCategory()
    {
        return view('admin_access.lease_category.index');
    }

    /**
     * Render lease category create form.
     *
     * @return \Illuminate\View\View
     */
    public function leasecategoryCreateForm()
    {
        return View::make('admin_access.lease_category.create')->render();
    }

    /**
     * Lease category save.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return json
     */
    public function leaseCategorySave(Request $request)
    {
        $category = new LeaseCategory();
        $category->name = $request->get('name');
        $category->bn_name = $request->get('bn_name');
        $category->status = $request->get('is_active');
        $category->created_by = Auth::user()->id;
        $category->save();

        return response()->json(['flug' => 1, 'message' => trans('common.save_success')]);
    }

    /**
     * Get datatable source data.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return json
     */
    public function leaseCategoryData(Request $request)
    {
        $input = $request->all();
        $collection = LeaseCategory::getDatalease($input);
        $output = Datatablehelper::leasecatdatatableOutput($input, $collection);

        return response()->json($output);
    }

    /**
     * Get lease category edit form.
     *
     * @param \Illuminate\Http\Request $request
     * @param string                   $id
     *
     * @return \Illuminate\View\View
     */
    public function leasecategoryEditForm(Request $request, $id)
    {
        $category = LeaseCategory::find($id);

        return View::make('admin_access.lease_category.edit', compact('category'))->render();
    }

    /**
     * Lease category save.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return json
     */
    public function leaseCategoryUpdate(Request $request, $id)
    {
        $category = LeaseCategory::find($id);
        $category->name = $request->get('name');
        $category->bn_name = $request->get('bn_name');
        $category->status = $request->get('is_active');
        $category->updated_by = Auth::user()->id;
        $category->save();

        return response()->json(['flug' => 1, 'message' => trans('common.update_success')]);
    }

    /**
     * Change category status.
     *
     * @param \Illuminate\Http\Request $request
     * @param string                   $id
     *
     * @return string
     */
    public function leaseCategoryStatus(Request $request, $id)
    {
        $category = LeaseCategory::find($id);

        if ($category->status == 1) {
            $category->status = 0;
        } else {
            $category->status = 1;
        }

        $category->updated_by = Auth::user()->id;
        $category->save();

        return trans('common.status_change');
    }

    /**
     * Lease category delete.
     *
     * @param \Illuminate\Http\Request $request
     * @param string                   $id
     *
     * @return string
     */
    public function leaseCategoryDelete(Request $request, $id)
    {
        LeaseCategory::where('id', $id)->delete();

        return trans('common.delete_data');
    }

    /**
     * Show the land subcategory.
     *
     * @return \Illuminate\View\View
     */
    public function leaseSubcategory()
    {
        return view('admin_access.lease_subcategory.index', compact('subcategory'));
    }

    /**
     * Get land subcategory datatable source data.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return json
     */
    public function leaseSubCategoryData(Request $request)
    {
        $input = $request->all();
        $collection = LeaseCategory::leasegetSubcategoryData($input);
        $output = Datatablehelper::leasesubCategoryOutput($input, $collection);

        return response()->json($output);
    }

    /**
     * Render land subcategory create form.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\View\View
     */
    public function leasesubCategoryCreateForm(Request $request)
    {
        $category = LeaseCategory::where('parent_id', 0)
                                 ->orderBy('id', 'desc')
                                 ->lists('bn_name', 'id');

        return View::make('admin_access.lease_subcategory.create', compact('category'))->render();
    }

    /**
     * Land subcategory save.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return json
     */
    public function leaseSubCategoryCreate(Request $request)
    {
        $category = new LeaseCategory();
        $category->parent_id = $request->get('category');
        $category->name = $request->get('name');
        $category->bn_name = $request->get('bn_name');
        $category->status = $request->get('is_active');
        $category->created_by = Auth::user()->id;
        $category->save();

        return response()->json(['flug' => 1, 'message' => trans('common.save_success')]);
    }

    /**
     * Get land subcategory edit form.
     *
     * @param \Illuminate\Http\Request $request
     * @param string                   $id
     *
     * @return \Illuminate\View\View
     */
    public function leasesubcategoryEditForm(Request $request, $id)
    {
        $subcategory = LeaseCategory::find($id);
        $category = LeaseCategory::where('parent_id', 0)
                                           ->orderBy('id', 'desc')
                                           ->select('bn_name', 'id')
                                           ->get();

        return View::make('admin_access.lease_subcategory.edit', compact('subcategory', 'category'))->render();
    }

    /**
     * Land subcategory update.
     *
     * @param \Illuminate\Http\Request $request
     * @param string                   $id
     *
     * @return json
     */
    public function leaseSubcategoryUpdate(Request $request, $id)
    {
        $category = LeaseCategory::find($id);
        $category->parent_id = $request->get('category');
        $category->name = $request->get('name');
        $category->bn_name = $request->get('bn_name');
        $category->status = $request->get('is_active');
        $category->updated_by = Auth::user()->id;
        $category->save();

        return response()->json(['flug' => 1, 'message' => trans('common.update_success')]);
    }

    /**
     * Lease subcategory status.
     *
     * @param \Illuminate\Http\Request $request
     * @param string                   $id
     *
     * @return string
     */
    public function leaseSubCategoryStatus(Request $request, $id)
    {
        $category = LeaseCategory::find($id);

        if ($category->status == 1) {
            $category->status = 0;
        } else {
            $category->status = 1;
        }

        $category->updated_by = Auth::user()->id;
        $category->save();

        return trans('common.status_change');
    }

    /**
     * Lease subcategory status delete.
     *
     * @param \Illuminate\Http\Request $request
     * @param string                   $id
     *
     * @return string
     */
    public function leaseSubCategoryDelete(Request $request, $id)
    {
        LeaseCategory::where('id', $id)->delete();

        return trans('common.delete_data');
    }

    /**
     * Show lease comparison list.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\View\View
     */
    public function leaseComparison(Request $request)
    {
        $tender = Tender::where('IS_LEASE', 1)->where('IS_ACTIVE', 1)->lists('TENDER_TITLE', 'TENDER_ID');

        return view('admin_access.lease_applicant.index', compact('tender'));
    }

    /**
     * Lease comparison data.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return json
     */
    public function leaseComparisonData(Request $request)
    {
        $input = $request->all();
        $collection = TenderApplicant::getLeaseComparisonData($input);
        $output = Datatablehelper::leaseComparisonOutput($input, $collection);

        return response()->json($output);
    }

    /**
     * Show lease applicant list.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\View\View
     */
    public function leaseApplicantList(Request $request)
    {
        $tender = Tender::where('IS_LEASE', 1)->where('IS_ACTIVE', 1)->lists('TENDER_TITLE', 'TENDER_ID');

        return view('admin_access.lease_applicant.list', compact('tender'));
    }

    /**
     * Lease applicant data.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return json
     */
    public function leaseListData(Request $request)
    {
        $input = $request->all();
        $collection = TenderApplicant::getLeaseListData($input);
        $output = Datatablehelper::tenderListOutput($input, $collection);

        return response()->json($output);
    }

    /**
     * Render lease Cancle form.
     *
     * @param \Illuminate\Http\Request $request
     * @param string                   $id
     * @param string                   $type
     *
     * @return \Illuminate\View\View
     */
    public function leaseCancelForm(Request $request, $type, $id)
    {
        if ($type == 'applicant') {
            $tender = ApplicantProperty::where('TE_APP_ID', $id)->first();
        } else {
            $tender = CitizenApplication::where('CITIZEN_ID', $id)->first();
        }

        return View::make('admin_access.lease_applicant.cancel', compact('tender', 'type'))->render();
    }

    /**
     * Update lease Cancle.
     *
     * @param \Illuminate\Http\Request $request
     * @param string                   $id
     * @param string                   $type
     *
     * @return json
     */
    public function leaseCancelUpdate(Request $request, $type, $id)
    {
        $data = $request->all();
        if ($type == 'applicant') {
            $tender = ApplicantProperty::where('TE_APP_ID', $id)->first();
        } else {
            $tender = CitizenApplication::where('CITIZEN_ID', $id)->first();
        }
        $tender->CANCEL_REMARKS = $data['CANCEL_REMARKS'];
        $tender->IS_CANCEL = $tender->IS_CANCEL == 1 ? 0 : 1;
        $tender->UPDATED_BY = Auth::user()->USER_ID;
        $tender->save();

        return response()->json(['flug' => 1, 'message' => trans('common.update_success')]);
    }

    /**
     * Render Lease remarks form.
     *
     * @param \Illuminate\Http\Request $request
     * @param string                   $id
     * @param string                   $type
     *
     * @return \Illuminate\View\View
     */
    public function leaseRemarksForm(Request $request, $type, $id)
    {
        if ($type == 'applicant') {
            $tender = ApplicantProperty::where('TE_APP_ID', $id)->first();
        } else {
            $tender = CitizenApplication::where('CITIZEN_ID', $id)->first();
        }

        return View::make('admin_access.lease_applicant.remarks', compact('tender', 'type'))->render();
    }

    /**
     * Update lease remarks.
     *
     * @param \Illuminate\Http\Request $request
     * @param string                   $id
     * @param string                   $type
     *
     * @return json
     */
    public function leaseRemarksUpdate(Request $request, $type, $id)
    {
        $data = $request->all();
        if ($type == 'applicant') {
            $tender = ApplicantProperty::where('TE_APP_ID', $id)->first();
        } else {
            $tender = CitizenApplication::where('CITIZEN_ID', $id)->first();
        }
        $tender->REMARKS = $data['REMARKS'];
        $tender->UPDATED_BY = Auth::user()->USER_ID;
        $tender->save();

        return response()->json(['flug' => 1, 'message' => trans('common.update_success')]);
    }

    /**
     * Render lease primary selected form.
     *
     * @param \Illuminate\Http\Request $request
     * @param string                   $id
     * @param string                   $ts
     * @param string                   $id
     *
     * @return \Illuminate\View\View
     */
    public function leasePrimaryApplicantSelectForm(Request $request, $ts, $type, $id)
    {
        $data['type'] = $type;
        if ($type == 'applicant') {
            $data['applicant'] = ApplicantProperty::with('getBank')->where('TE_APP_ID', $id)->where('SCHEDULE_ID', $ts)->first();
            $data['lease'] = TenderApplicant::find($id);
        } else {
            $data['applicant'] = CitizenApplication::where('CITIZEN_ID', $id)->where('SCHEDULE_ID', $ts)->first();
            $data['citizen'] = Citizen::where('CITIZEN_ID', $id)->first();
            $data['lease'] = TenderApplicant::find($data['citizen']->TE_APP_ID);
        }
        $data['schedule_particulars'] = TenderParticulars::with('getParticular')
                                            ->where('SCHEDULE_ID', $data['applicant']->SCHEDULE_ID)
                                            ->whereIn('PARTICULAR_ID', [3, 9, 10])->get();
        $data['project_name'] = Project::where('PROJECT_ID', $data['applicant']->PROJECT_ID)->pluck('PR_NAME_BN');

        $data['applicent_district'] = District::where('DISTRICT_ID', $data['lease']->DISTRICT_ID)
                                            ->pluck('DISTRICT_ENAME');
        $data['applicent_thana'] = Thana::where('THANA_ID', $data['lease']->THANA_ID)
                                            ->pluck('THANA_ENAME');
        $data['officer'] = Officer::where('TYPE', 1)->first();
        $data['location'] = ProjectLocation::where('IS_ACTIVE', 1)->get();

        return View::make('admin_access.lease_applicant.primary_selection', $data)->render();
    }

    /**
     * Update primary lease applicant insert.
     *
     * @param \Illuminate\Http\Request $request
     * @param $ts
     * @param $type
     * @param $id
     *
     * @return json
     */
    public function leasePrimaryApplicantSave(Request $request, $ts, $type, $id)
    {
        $data = $request->all();
        // Check name before insert
        $number_exist = TenderLetter::where('LATTER_NUMBER', $data['sharok_no'])->first();
        if (!empty($number_exist)) {
            return response()->json(['flug' => 0, 'message' => trans('lease.is_sharok_exist')]);
            exit;
        }
        $user = Auth::user()->USER_ID;
        $lease_validity = LeaseValidity::leaseValiditySave($data, $ts, $type, $user, $id);

        // Letter Email
        if ($type == 'applicant') {
            $valid = LeaseValidity::where('TE_APP_ID', $id)->where('SCHEDULE_ID', $ts)->first();
            $te_applicant = TenderApplicant::where('TE_APP_ID', $id)->first();
            $bid_rate = ApplicantProperty::where('TE_APP_ID', $id)->where('SCHEDULE_ID', $ts)->pluck('BID_AMOUNT');
            $data['to_name'] = $te_applicant->APPLICANT_NAME;
            $data['to_email'] = $te_applicant->EMAIL;
            $data['number'] = $te_applicant->APPLICANT_PHONE;
            $data['letter'] = TenderLetter::where('TE_APP_ID', $id)
                                            ->where('SCHEDULE_ID', $ts)
                                            ->where('IS_LEASE', 1)
                                            ->where('TYPE', 0)
                                            ->first();
        } else {
            $valid = LeaseValidity::where('CITIZEN_ID', $id)->where('SCHEDULE_ID', $ts)->first();
            $citizen = Citizen::where('CITIZEN_ID', $id)->with('getTenderCitizen')->first();
            $bid_rate = CitizenApplication::where('CITIZEN_ID', $id)->where('SCHEDULE_ID', $ts)->pluck('BID_AMOUNT');
            $data['to_name'] = $citizen->getTenderCitizen->FULL_NAME;
            $data['to_email'] = $citizen->getTenderCitizen->EMAIL;
            $data['number'] = $citizen->getTenderCitizen->MOBILE_NO;
            $data['letter'] = TenderLetter::where('CITIZEN_ID', $id)
                                            ->where('SCHEDULE_ID', $ts)
                                            ->where('IS_LEASE', 1)
                                            ->where('TYPE', 0)
                                            ->first();
        }
        if (!empty($data['to_email'])) {
            $data['template'] = 'primary_letter_lease';
            $data['subject'] = 'Lease primary selection';
            $send_email = Helpers::sendEmail($data);
        }

        // Send SMS
        $sms = $this->generatePrimarySMS($ts, $bid_rate, $valid);
        $send_sms = Helpers::sendSMS($data['number'], $sms);

        return response()->json(['flug' => 1, 'message' => trans('common.save_success')]);
    }

    /**
     * Generate lease primary select SMS.
     *
     * @param string $ts       Tender schedule id
     * @param string $bid_rate Applicant bid rate
     * @param object $valid    Lease validity data
     *
     * @return string
     */
    private function generatePrimarySMS($ts, $bid_rate, $valid)
    {
        $schedule = TenderSchedule::with('getProject', 'getCategory')
                                    ->where('SCHEDULE_ID', $ts)
                                    ->first();
        $sms = 'আপনার আবেদনকৃত ';
        $sms .= $schedule->getProject->PR_NAME_BN.' মহালে আপনার প্রস্তাবিত দর ';
        $sms .= Helpers::en2bn(number_format($bid_rate, 2)).' টাকা সর্বোচ্চ বিবেচিত হওয়ায় ';
        $sms .= Helpers::en2bn(date('d/m/Y', strtotime($valid->DATE_FROM))).' থেকে ';
        $sms .= Helpers::en2bn(date('d/m/Y', strtotime($valid->DATE_TO))).'  তারিখ এর জন্য ';
        $sms .= 'আপনি প্রাথমিকভাবে নির্বাচিত হয়েছেন।';

        return $sms;
    }

    /**
     * Show Lease primary applicant selected page.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\View\View
     */
    public function leasePrimaryApplicantList(Request $request)
    {
        $tender = Tender::where('IS_LEASE', 1)->where('IS_ACTIVE', 1)->lists('TENDER_TITLE', 'TENDER_ID');

        return view('admin_access.lease_applicant.primary_list', compact('tender'));
    }

    /**
     * Get tender primary selected applicant bid data.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return json
     */
    public function leasePrimaryListData(Request $request)
    {
        $input = $request->all();
        $collection = TenderApplicant::getLeasePrimaryListData($input);
        $output = Datatablehelper::leasePrimaryOutput($input, $collection);

        return response()->json($output);
    }

    /**
     * print lease primary applicant letter.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Illuminate\Http\Request $request
     * @param string                   $ts
     * @param string                   $type
     * @param string                   $id
     *
     * @return pdf
     */
    public function leasePrimaryLetterPrint(Request $request, $ts, $type, $id)
    {
        if ($type == 'applicant') {
            $data['letter'] = TenderLetter::where('TE_APP_ID', $id)->where('SCHEDULE_ID', $ts)->where('IS_LEASE', 1)->where('TYPE', 0)->first();
        } else {
            $data['letter'] = TenderLetter::where('CITIZEN_ID', $id)->where('SCHEDULE_ID', $ts)->where('IS_LEASE', 1)->where('TYPE', 0)->first();
        }
        $pdf_data = View::make('admin_access.lease_applicant.primary_letter_print', $data);
        $file_name = 'NCC-'.'lease-primary-letter'.'.pdf';

        return Helpers::makePDF($file_name, $pdf_data);
    }

    /**
     * Show all lease Demesne.
     *
     * @author Nurullah <nurul@atilimited.net>
     *
     * @return \Illuminate\View\View
     */
    public function leaseDemesne()
    {
        $link_url = 'lease_demesne'; // module url
        $data['access'] = Module::checkPrevilege($link_url); // get access info by module url
        return view('admin_access.lease_demesne.index', $data);
    }

    /**
     * Get lease Demesne data.
     *
     * @author Nurullah <nurul@atilimited.net>
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return json
     */
    public function leaseDemesneData(Request $request)
    {
        $input = $request->all();
        $collection = LeaseDemesne::getData($input);
        $output = Datatablehelper::leaseDemesneOutput($input, $collection);

        return response()->json($output);
    }

    /**
     * Render lease Demesne create form.
     *
     * @author Nurullah <nurul@atilimited.net>
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\View\View
     */
    public function leaseDemesneCreate(Request $request)
    {
        $data['tender'] = Tender::where('IS_ACTIVE', 1)->where('IS_LEASE', 1)->lists('TENDER_NO', 'TENDER_ID');

        return View::make('admin_access.lease_demesne.create', $data)->render();
    }

    /**
     * On change get data schedule id.
     *
     * @author Nurullah <nurul@atilimited.net>
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return string
     */
    public function getscheduleByTenderId(Request $request)
    {
        $tender_id = $request->get('tender_id');

        $mohal = TenderSchedule::with('getProject')->where('TENDER_ID', $tender_id)->get();

        $data = '<option value="">'.trans('common.form_select').'</option>';
        if (count($mohal) > 0) {
            foreach ($mohal as $value) {
                $data .= '<option value="'.$value->SCHEDULE_ID.'">'.$value->getProject->PR_NAME_BN.'</option>';
            }
        }

        return $data;
    }

    /**
     * Lease demesne data save.
     *
     * @author Nurullah <nurul@atilimited.net>
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return json
     */
    public function leaseDemesneSave(Request $request)
    {
        $data = $request->all();
        $user = Auth::user()->USER_ID;
        // Check name before insert
        $schedule_exist = LeaseValidity::where('SCHEDULE_ID', $data['SCHEDULE_ID'])
                                        ->where('IS_ACTIVE', 1)->first();
        if (!empty($schedule_exist)) {
            return response()->json(['flug' => 0, 'message' => trans('lease.is_schedule_exist')]);
            exit;
        }
        $demesne = LeaseDemesne::createDemesne($data, $user);

        return response()->json(['flug' => 1, 'message' => trans('common.save_success')]);
    }

    /**
     * Lease demesne Edit.
     *
     * @author Nurullah <nurul@atilimited.net>
     *
     * @param \Illuminate\Http\Request $request
     * @param string                   $id
     *
     * @return \Illuminate\View\View
     */
    public function leaseDemesneEdit(Request $request, $id)
    {
        $data['leaseDemesne'] = LeaseDemesne::with('getValidity')->where('LEASE_DE_ID', $id)->first();
        $data['tender'] = Tender::where('IS_ACTIVE', 1)->where('IS_LEASE', 1)->select('TENDER_NO', 'TENDER_ID')->get();
        $data['tender_id'] = TenderSchedule::where('SCHEDULE_ID', $data['leaseDemesne']->SCHEDULE_ID)->pluck('TENDER_ID');
        $data['mohal'] = TenderSchedule::with('getProject')->where('TENDER_ID', $data['tender_id'])->get();

        return View::make('admin_access.lease_demesne.edit', $data)->render();
    }

    /**
     * Lease demesne data update.
     *
     * @author Nurullah <nurul@atilimited.net>
     *
     * @param \Illuminate\Http\Request $request
     * @param string                   $id
     *
     * @return json
     */
    public function leaseDemesneUpdate(Request $request, $id)
    {
        $data = $request->all();
        $user = Auth::user()->USER_ID;
        // Check name before insert
        $schedule_exist = LeaseValidity::whereNotIn('LEASE_DE_ID', [$id])
                              ->where('SCHEDULE_ID', $data['SCHEDULE_ID'])
                              ->where('IS_ACTIVE', 1)->first();
        if (!empty($schedule_exist)) {
            return response()->json(['flug' => 0, 'message' => trans('lease.is_schedule_exist')]);
            exit;
        }
        $demesne = LeaseDemesne::updateDemesne($data, $user, $id);

        return response()->json(['flug' => 1, 'message' => trans('common.update_success')]);
    }

    /**
     * Render lease Demesne Cancle form.
     *
     * @author Nurullah <nurul@atilimited.net>
     *
     * @param \Illuminate\Http\Request $request
     * @param string                   $id
     *
     * @return \Illuminate\View\View
     */
    public function leaseDemesneCancelForm(Request $request, $id)
    {
        $lease_demesne = LeaseDemesne::find($id);

        return View::make('admin_access.lease_demesne.cancel', compact('lease_demesne'))->render();
    }

    /**
     * Update lease Demesne Cancel.
     *
     * @author Nurullah <nurul@atilimited.net>
     *
     * @param \Illuminate\Http\Request $request
     * @param string                   $id
     *
     * @return json
     */
    public function leaseDemesneCancelUpdate(Request $request, $id)
    {
        $data = $request->all();
        $user = Auth::user()->USER_ID;

        $lease_demesne = LeaseDemesne::find($id);
        $lease_demesne->CANCEL_REMARKS = $data['CANCEL_REMARKS'];
        $lease_demesne->IS_CANCEL = 1;
        $lease_demesne->IS_ACTIVE = 0;
        $lease_demesne->UPDATED_BY = $user;
        $lease_demesne->save();

        $validity = LeaseValidity::where('LEASE_DE_ID', $id)->first();
        $validity->IS_ACTIVE = 0;
        $validity->UPDATED_BY = $user;
        $validity->save();

        return response()->json(['flug' => 1, 'message' => trans('common.update_success')]);
    }

    /**
     * Show all lease Demesne.
     *
     * @author Nurullah <nurul@atilimited.net>
     *
     * @return \Illuminate\View\View
     */
    public function demesneMoney()
    {
        $link_url = 'demesne_money'; // module url
        $data['access'] = Module::checkPrevilege($link_url); // get access info by module url
        return view('admin_access.lease_demesne_money_collection.index', $data);
    }

    /**
     * Get lease Demesne data.
     *
     * @author Nurullah <nurul@atilimited.net>
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return json
     */
    public function demesneMoneyData(Request $request)
    {
        $input = $request->all();
        $collection = LeaseDemesne::getMoneyData($input);
        $output = Datatablehelper::demesneMoneyOutput($input, $collection);

        return response()->json($output);
    }

    /**
     * Render lease Demesne create form.
     *
     * @author Nurullah <nurul@atilimited.net>
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\View\View
     */
    public function DemesneMoneyCreate(Request $request)
    {
        $data['schedule'] = LeaseValidity::with(['getSchedule'=> function ($query) {
            $query->with(['getProject']);
        },
                                ])
                           ->where('DATE_TO', '>=', date('Y-m-d').' 00:00:00')
                           ->whereNotNull('LEASE_DE_ID')
                           ->where('IS_ACTIVE', 1)->get();

        return View::make('admin_access.lease_demesne_money_collection.create', $data)->render();
    }

    /**
     * On change get demesne info.
     *
     * @author Nurullah <nurul@atilimited.net>
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\View\View
     */
    public function getInfoByScheduleId(Request $request)
    {
        $schedule_id = $request->get('schedule_id');
        $data['demesne_info'] = LeaseDemesne::where('SCHEDULE_ID', $schedule_id)->first();

        return View::make('admin_access.lease_demesne_money_collection.info', $data)->render();
    }

    /**
     * Lease demesne data save.
     *
     * @author Nurullah <nurul@atilimited.net>
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return json
     */
    public function DemesneMoneySave(Request $request)
    {
        $data = $request->all();
        $user = Auth::user()->USER_ID;
        $demesne = AccountVaucherMst::createVaucher($data, $user);

        return response()->json(['flug' => 1, 'message' => trans('common.save_success')]);
    }

    /**
     * Show Lease primary applicant selected page.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\View\View
     */
    public function leaseFinalApplicantList(Request $request)
    {
        $tender = Tender::where('IS_ACTIVE', 1)->where('IS_LEASE', 1)->lists('TENDER_TITLE', 'TENDER_ID');

        return view('admin_access.lease_applicant.final_list', compact('tender'));
    }

    /**
     * Get tender primary selected applicant bid data.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return json
     */
    public function leasefinalListData(Request $request)
    {
        $input = $request->all();
        $collection = TenderApplicant::getLeaseFinalListData($input);
        $output = Datatablehelper::leaseFinalOutput($input, $collection);

        return response()->json($output);
    }

    /**
     * Render lease primary selected form.
     *
     * @param \Illuminate\Http\Request $request
     * @param string                   $ts
     * @param string                   $type
     * @param string                   $id
     *
     * @return \Illuminate\View\View
     */
    public function leaseFinalApplicantSelectForm(Request $request, $ts, $type, $id)
    {
        $data['type'] = $type;
        if ($type == 'applicant') {
            $data['applicant'] = ApplicantProperty::with('getBank')->where('TE_APP_ID', $id)->where('SCHEDULE_ID', $ts)->first();
            $data['lease'] = TenderApplicant::find($id);
            //
            $data['validity'] = LeaseValidity::where('TE_APP_ID', $id)->where('SCHEDULE_ID', $ts)->first();
        } else {
            $data['applicant'] = CitizenApplication::where('CITIZEN_ID', $id)->where('SCHEDULE_ID', $ts)->first();
            $data['citizen'] = Citizen::where('CITIZEN_ID', $id)->first();
            $data['lease'] = TenderApplicant::find($data['citizen']->TE_APP_ID);
            //
            $data['validity'] = LeaseValidity::where('CITIZEN_ID', $id)->where('SCHEDULE_ID', $ts)->first();
        }
        $data['schedule_particulars'] = TenderParticulars::with('getParticular')
                                            ->where('SCHEDULE_ID', $data['applicant']->SCHEDULE_ID)
                                            ->whereIn('PARTICULAR_ID', [3, 9, 10])->get();
        $data['project_name'] = Project::where('PROJECT_ID', $data['applicant']->PROJECT_ID)->pluck('PR_NAME_BN');

        $data['applicent_district'] = District::where('DISTRICT_ID', $data['lease']->DISTRICT_ID)
                                            ->pluck('DISTRICT_ENAME');
        $data['applicent_thana'] = Thana::where('THANA_ID', $data['lease']->THANA_ID)
                                            ->pluck('THANA_ENAME');
        $data['officer'] = Officer::where('TYPE', 1)->first();
        $data['location'] = ProjectLocation::where('IS_ACTIVE', 1)->get();

        return View::make('admin_access.lease_applicant.final_selection', $data)->render();
    }

    /**
     * Lease final select.
     *
     * @param \Illuminate\Http\Request $request
     * @param string                   $ts      Tender schedule id
     * @param string                   $type    Lease applicant type(Applicant/citizen)
     * @param string                   $id      Applicant id(Applicant/citizen)
     *
     * @return response
     */
    public function leaseFinalApplicantSave(Request $request, $ts, $type, $id)
    {
        $data = $request->all();
        // Check LATTER_NUMBER before insert
        $number_exist = TenderLetter::where('LATTER_NUMBER', $data['sharok_no'])->first();
        if (!empty($number_exist)) {
            return response()->json(['flug' => 0, 'message' => trans('lease.is_sharok_exist')]);
            exit;
        }
        $user = Auth::user()->USER_ID;
        $lease_final_select = TenderApplicant::leaseFinalSave($data, $ts, $type, $id, $user);

        // Letter Email
        if ($type == 'applicant') {
            $valid = LeaseValidity::where('TE_APP_ID', $id)->where('SCHEDULE_ID', $ts)->first();
            $te_applicant = TenderApplicant::where('TE_APP_ID', $id)->first();
            $data['to_name'] = $te_applicant->APPLICANT_NAME;
            $data['to_email'] = $te_applicant->EMAIL;
            $data['number'] = $te_applicant->APPLICANT_PHONE;
            $data['letter'] = TenderLetter::where('TE_APP_ID', $id)
                                            ->where('SCHEDULE_ID', $ts)
                                            ->where('IS_LEASE', 1)
                                            ->where('TYPE', 1)
                                            ->first();
        } else {
            $valid = LeaseValidity::where('CITIZEN_ID', $id)->where('SCHEDULE_ID', $ts)->first();
            $citizen = Citizen::where('CITIZEN_ID', $id)->with('getTenderCitizen')->first();
            $data['to_name'] = $citizen->getTenderCitizen->FULL_NAME;
            $data['to_email'] = $citizen->getTenderCitizen->EMAIL;
            $data['number'] = $citizen->getTenderCitizen->MOBILE_NO;
            $data['letter'] = TenderLetter::where('CITIZEN_ID', $id)
                                            ->where('SCHEDULE_ID', $ts)
                                            ->where('IS_LEASE', 1)
                                            ->where('TYPE', 1)
                                            ->first();
        }
        if (!empty($data['to_email'])) {
            $data['template'] = 'final_letter_lease';
            $data['subject'] = 'Lease final selection';
            $send_email = Helpers::sendEmail($data);
        }

        // Send SMS
        $sms = $this->generateFinalSMS($ts, $valid);
        $send_sms = Helpers::sendSMS($data['number'], $sms);

        return response()->json(['flug' => 1, 'message' => trans('common.save_success')]);
    }

    /**
     * Generate lease final select SMS.
     *
     * @param string $ts    Tender schedule id
     * @param object $valid Lease validity data
     *
     * @return string
     */
    private function generateFinalSMS($ts, $valid)
    {
        $schedule = TenderSchedule::with('getProject', 'getCategory')
                                    ->where('SCHEDULE_ID', $ts)
                                    ->first();
        $sms = 'আপনার আবেদনকৃত ';
        $sms .= $schedule->getProject->PR_NAME_BN.' মহালে ';
        $sms .= Helpers::en2bn(date('d/m/Y', strtotime($valid->DATE_FROM))).' থেকে ';
        $sms .= Helpers::en2bn(date('d/m/Y', strtotime($valid->DATE_TO))).'  তারিখ এর জন্য ';
        $sms .= 'আপনি চূড়ান্তভাবে নির্বাচিত হয়েছেন।';

        return $sms;
    }

    /**
     * Lease final letter.
     *
     * @param \Illuminate\Http\Request $request
     * @param string                   $type    Lease applicant type(Applicant/citizen)
     * @param string                   $id      Applicant id(Applicant/citizen)
     *
     * @return pdf output
     */
    public function leaseFinalLetterPrint(Request $request, $ts, $type, $id)
    {
        if ($type == 'applicant') {
            $data['letter'] = TenderLetter::where('TE_APP_ID', $id)->where('SCHEDULE_ID', $ts)->where('IS_LEASE', 1)->where('TYPE', 1)->first();
        } else {
            $data['letter'] = TenderLetter::where('CITIZEN_ID', $id)->where('SCHEDULE_ID', $ts)->where('IS_LEASE', 1)->where('TYPE', 1)->first();
        }

        $pdf_data = View::make('admin_access.lease_applicant.final_letter_print', $data);
        $file_name = 'NCC-'.'lease-Final-letter'.'.pdf';

        return Helpers::makePDF($file_name, $pdf_data, 'D');
    }
}
