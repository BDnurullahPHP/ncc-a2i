<?php

namespace App\Http\Controllers\Admin_access;

use App\Http\Controllers\Controller;
use App\Models\ApplicantProperty;
use App\Models\CategoryParticular;
use App\Models\Citizen;
use App\Models\CitizenApplication;
use App\Models\District;
use App\Models\Module;
use App\Models\Officer;
use App\Models\Project;
use App\Models\ProjectCatCondition;
use App\Models\ProjectCategory;
use App\Models\ProjectDetails;
use App\Models\ProjectLocation;
use App\Models\Tender;
use App\Models\TenderApplicant;
use App\Models\TenderConditions;
use App\Models\TenderDateTime;
use App\Models\TenderInstallment;
use App\Models\TenderLetter;
use App\Models\TenderLocation;
use App\Models\TenderParticulars;
use App\Models\TenderSchedule;
use App\Models\Thana;
use App\Ncc\Datatablehelper;
use App\Ncc\Helpers;
use Auth;
use Illuminate\Http\Request;
use Response;
use View;

/**
 * Class     TenderController.
 *
 * @author   Abdul Awal <abdulawal@atilimited.net>
 */
class TenderController extends Controller
{
    /**
     * Show Tender Template Create.
     *
     * @return \Illuminate\View\View
     */
    public function tenderIndex()
    {
        $link_url = 'tender_list'; // module url
        $data['access'] = Module::checkPrevilege($link_url); // get access info by module url
        return view('admin_access.tender.index', $data);
    }

    /**
     * Get tender data.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return json
     */
    public function tenderData(Request $request)
    {
        $input = $request->all();
        $collection = Tender::getData($input);
        $output = Datatablehelper::tenderOutput($input, $collection);

        return response()->json($output);
    }

    /**
     * Render tender create form.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\View\View
     */
    public function tenderCreateForm(Request $request)
    {
        $data['project_list'] = Project::where('IS_LEASE', '=', 0)->where('IS_ACTIVE', 1)
                                        ->lists('PR_NAME_BN', 'PROJECT_ID');
        $data['location'] = ProjectLocation::where('IS_ACTIVE', 1)
                                                ->lists('LOCATION_NAME', 'LOCATION_ID');
        $data['tender_list'] = Tender::where('IS_LEASE', 0)->where('IS_ACTIVE', 1)->get();

        return View::make('admin_access.tender.create')->with($data)->render();
    }

    /**
     * Render tender project category by project id.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return json
     */
    public function tenderProjectCategory(Request $request)
    {
        $project_id = $request->get('project');
        $tender_id = $request->get('tender');
        $re_tender = $request->get('re_tender');
        $data['projects'] = $request->get('projects');
        $data['project_cat'] = [];

        if (!empty($project_id)) {
            $data['project_category'] = ProjectDetails::with('getCategory')
                                                        ->whereIn('PROJECT_ID', $project_id)
                                                        ->groupBy('PR_CATEGORY')
                                                        ->get();
            if (!empty($tender_id)) {
                $tender_schedule = TenderSchedule::where('TENDER_ID', $tender_id)
                                                    ->select('SCHEDULE_ID', 'PR_CATEGORY')
                                                    ->get();
                foreach ($tender_schedule as $key => $ts) {
                    $data['project_cat'][] = $ts->PR_CATEGORY;
                }
            }
        } elseif (!empty($re_tender)) {
            $tender_schedule = TenderSchedule::where('TENDER_ID', $re_tender)
                                                ->select('SCHEDULE_ID', 'PR_CATEGORY', 'PROJECT_ID')
                                                ->get();
            $data['project_category'] = ProjectDetails::with('getCategory')
                                                        ->where('PROJECT_ID', $tender_schedule[0]->PROJECT_ID)
                                                        ->groupBy('PR_CATEGORY')
                                                        ->get();
            foreach ($tender_schedule as $key => $ts) {
                $data['project_cat'][] = $ts->PR_CATEGORY;
            }
        }

        $view = View::make('admin_access.tender.project_category')->with($data)->render();

        if (count($data['project_category']) > 0) {
            return response()->json(['flug' => 1, 'message' => '', 'data' => $view]);
        } else {
            return response()->json(['flug' => 0, 'message' => trans('common.no_data'), 'data' => $view]);
        }
    }

    /**
     * Render tender project details by project id and category id.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\View\View
     */
    public function tenderProjectDetails(Request $request)
    {
        $project_id = $request->get('project_id');
        $category_id = $request->get('category_id');
        $tender_id = $request->get('tender');

        $data['cat_particular'] = CategoryParticular::with('getParticular')
                                                    ->where('PR_CATEGORY', $category_id)
                                                    ->orderBy('PARTICULAR_ID', 'desc')
                                                    ->get();
        if (!empty($tender_id)) {
            $project_array = $projects = explode(',', $project_id);
            $data['project_details'] = ProjectDetails::whereIn('PROJECT_ID', $project_array)
                                                    ->where('PR_CATEGORY', $category_id)
                                                    ->get();
            $sc_project = TenderSchedule::where('TENDER_ID', $tender_id)->get();
            $data['sc_project'] = [];
            $data['bid_amount'] = [];
            $te_schedule = [];
            foreach ($sc_project as $key => $scp) {
                $data['sc_project'][] = $scp->PR_DETAIL_ID;
                $data['bid_amount'][$scp->PR_DETAIL_ID] = $scp->LOWEST_TENDER_MONEY;
                $data['tender_schedule'][$scp->PR_DETAIL_ID] = $scp->SCHEDULE_ID;

                $particular_obj = new \stdClass();
                $particular_obj->SCHEDULE_ID = $scp->SCHEDULE_ID;
                $particular_obj->PR_DETAIL_ID = $scp->PR_DETAIL_ID;
                $tender_particular[] = $particular_obj;
                $te_schedule[] = $scp->SCHEDULE_ID;
            }
            foreach ($tender_particular as $index => $tp) {
                $t_particular = TenderParticulars::where('SCHEDULE_ID', $tp->SCHEDULE_ID)
                                                        ->select('TE_PART_ID', 'SCHEDULE_ID')
                                                        ->get();
                foreach ($t_particular as $ky => $tenp) {
                    $data['tendr_part'][$tp->PR_DETAIL_ID][$ky] = $tenp->TE_PART_ID;
                }
            }
        } else {
            $data['project_details'] = ProjectDetails::getTenderProjectDetails($project_id, $category_id);
        }

        return View::make('admin_access.tender.project_details')->with($data)->render();
    }

    /**
     * Tender category conditions.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\View\View
     */
    public function categoryConditions(Request $request)
    {
        $category = $request->get('category');
        $data['cat_name'] = ProjectCategory::where('PR_CATEGORY', $category)->pluck('CATE_NAME');
        if ($request->has('tender_id')) {
            $data['tender_id'] = $request->get('tender_id');
            $data['conditions'] = TenderConditions::with('getCategory')
                                                    ->where('TENDER_ID', $data['tender_id'])
                                                    ->whereIn('PR_CATEGORY', $category)
                                                    ->get();
            if (count($data['conditions']) == 0) {
                $data['conditions'] = ProjectCatCondition::with('getCategory')
                                                    ->whereIn('PR_CATEGORY', $category)
                                                    ->get();
            }
        } else {
            $data['conditions'] = ProjectCatCondition::with('getCategory')
                                                    ->whereIn('PR_CATEGORY', $category)
                                                    ->get();
        }

        return View::make('admin_access.tender.conditions', compact('category'))->with($data)->render();
    }

    /**
     * Create tender data.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return json
     */
    public function tenderCreate(Request $request)
    {
        $data = $request->all();
        $user = Auth::user()->USER_ID;
        $tender_no = $data['tender_no'];

        // Check tender no before insert
        $tender_exist = Tender::where('TENDER_NO', $tender_no)->first();
        if (!empty($tender_exist)) {
            return response()->json(['flug' => 0, 'message' => trans('tender.tender_exists')]);
            exit;
        } else {
            $tender = Tender::createData($data, $user);
        }

        return response()->json(['flug' => 1, 'message' => trans('common.save_success')]);
    }

    /**
     * Tender edit.
     *
     * @param \Illuminate\Http\Request $request
     * @param string                   $id
     *
     * @return \Illuminate\View\View
     */
    public function tenderEdit(Request $request, $id)
    {
        $data['tender_id'] = $id;
        $data['te_location'] = [];
        $data['te_copy_location'] = [];
        $data['tender_data'] = Tender::find($id);
        $data['schedule'] = TenderSchedule::where('TENDER_ID', $id)->first();
        $data['location'] = ProjectLocation::with('getTenderLocation')->where('IS_ACTIVE', 1)->get();
        $data['tender_date'] = TenderDateTime::where('TENDER_ID', $id)->first();
        $data['project_list'] = Project::where('IS_LEASE', '=', 0)->get(); //lists('PR_NAME_BN', 'PROJECT_ID');
        $projectland = TenderSchedule::where('TENDER_ID', $id)->get();
        foreach ($projectland as $key => $pl) {
            $data['p_id'][] = $pl->PROJECT_ID;
        }
        $tender_location = TenderLocation::where('TENDER_ID', $id)
                                                ->where('TYPE', 1)
                                                ->select('TE_LOC_ID', 'LOCATION_ID', 'TYPE')
                                                ->get();
        $tender_cp_location = TenderLocation::where('TENDER_ID', $id)
                                                ->where('TYPE', 2)
                                                ->select('TE_LOC_ID', 'LOCATION_ID', 'TYPE')
                                                ->get();
        foreach ($tender_location as $inx => $tlc) {
            $data['te_location'][$inx] = $tlc->LOCATION_ID;
        }
        foreach ($tender_cp_location as $key => $tcl) {
            $data['te_copy_location'][$key] = $tcl->LOCATION_ID;
        }

        return View::make('admin_access.tender.edit')->with($data)->render();
    }

    /**
     * Update tender data.
     *
     * @param \Illuminate\Http\Request $request
     * @param string                   $id
     *
     * @return json
     */
    public function tenderUpdate(Request $request, $id)
    {
        $data = $request->all();
        $user = Auth::user()->USER_ID;
        $tender_no = $data['tender_no'];

        // Check tender no before update
        $tender_exist = Tender::whereNotIn('TENDER_ID', [$id])->where('TENDER_NO', $tender_no)->first();
        if (!empty($tender_exist)) {
            return response()->json(['flug' => 0, 'message' => trans('tender.tender_exists')]);
            exit;
        } else {
            $tender = Tender::createData($data, $user);
        }

        return response()->json(['flug' => 1, 'message' => trans('common.update_success')]);
    }

    /**
     * Print project view page.
     *
     *@author      Nurullah <nurul@atilimited.net>
     *
     * @param \Illuminate\Http\Request $request
     * @param string                   $id
     *
     * @return \Illuminate\View\View
     */
    public function tenderPrint(Request $request, $id)
    {
        //echo'<pre>';print_r($id);exit;
        $data['tender'] = Tender::where('TENDER_ID', $id)->first();
        $data['schedule'] = TenderSchedule::with('getProject')->where('TENDER_ID', $id)->first();
        $data['tender_location'] = TenderLocation::with('getLocation')->where('TENDER_ID', $id)->where('TYPE', 1)->get();
        $data['onulipy_location'] = TenderLocation::with('getLocation')->where('TENDER_ID', $id)->where('TYPE', 2)->get();
        $data['conditions'] = TenderConditions::with('getCategory')->where('TENDER_ID', $id)->get();
        $data['location_type'] = TenderLocation::where('TENDER_ID', $id)->groupBy('TYPE')->get();
        $data['tender_date'] = TenderDateTime::where('TENDER_ID', $id)->first();
        $data['project_category'] = TenderSchedule::with('getCategory')
                                                    ->where('TENDER_ID', $id)
                                                    ->groupBy('PR_CATEGORY')
                                                    ->get();
        $data['project_details'] = TenderSchedule::where('TENDER_ID', $id)->get();
        $pdf_data = View::make('admin_access.tender.print', $data)->render();
        $file_name = 'NCC-project-'.$data['tender']->TENDER_ID.'.pdf';

        return Helpers::makePDF($file_name, $pdf_data);

//        return view('admin_access.tender.view', $data)->render();
//
//
//
//
//        $data['project'] = Project::with('getDocuments')->where('PROJECT_ID', $id)->first();
//        $data['category'] = ProjectDetails::getCategoryDetails($id);
//        $data['projectLand'] = ProjectLand::where('PROJECT_ID', $id)
//                                            ->with(['getLand'])
//                                            ->get();
//
//        $pdf_data  = View::make('admin_access.market.print', $data)->render();
//        $file_name = 'NCC-project-'.$data['project']->PR_NAME.'.pdf';
//
//        return Helpers::makePDF($file_name, $pdf_data);
    }

    /**
     * Delete tender schedule data.
     *
     * @param \Illuminate\Http\Request $request
     * @param string                   $id
     *
     * @return string
     */
    public function tenderScheduleDelete(Request $request, $id)
    {
        TenderSchedule::where('PR_DETAIL_ID', $id)->delete();

        return trans('common.delete_data');
    }

    /**
     * Change tender status.
     *
     * @param \Illuminate\Http\Request $request
     * @param string                   $id
     *
     * @return string
     */
    public function tenderStatus(Request $request, $id)
    {
        $tender = Tender::find($id);

        if ($tender->IS_ACTIVE == 1) {
            $tender->IS_ACTIVE = 0;
        } else {
            $tender->IS_ACTIVE = 1;
        }
        $tender->save();

        return trans('common.status_change');
    }

    /**
     * Delete tender data.
     *
     * @param \Illuminate\Http\Request $request
     * @param string                   $id
     *
     * @return string
     */
    public function tenderDelete(Request $request, $id)
    {
        Tender::find($id)->delete();

        return trans('common.delete_data');
    }

    /**
     * Show Tender applicant bid comparison page.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\View\View
     */
    public function tenderApplicantComparison(Request $request)
    {
        $tender = Tender::where('IS_LEASE', 0)->where('IS_ACTIVE', 1)->lists('TENDER_TITLE', 'TENDER_ID');

        return view('admin_access.tender_applicant.index', compact('tender'));
    }

    /**
     * Get tender applicant bid comparison data.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return json
     */
    public function tenderComparisonData(Request $request)
    {
        $input = $request->all();
        //echo'<pre>';print_r($input);exit;
        $collection = TenderApplicant::getComparisonData($input);
        $output = Datatablehelper::tenderComparisonOutput($input, $collection);

        return response()->json($output);
    }

//    public function getScheduleListByTender(Request $request) {
//        $input      = $request->all();
//        echo '<pre>';print_r($input['list_val']);exit;
//    }

    /**
     * Show Tender applicant bid page.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\View\View
     */
    public function tenderApplicantList(Request $request)
    {
        $tender = Tender::where('IS_LEASE', 0)->where('IS_ACTIVE', 1)->lists('TENDER_TITLE', 'TENDER_ID');

        return view('admin_access.tender_applicant.list', compact('tender'));
    }

    /**
     * Get tender applicant bid data.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return json
     */
    public function tenderListData(Request $request)
    {
        $input = $request->all();
        $collection = TenderApplicant::getListData($input);
        $output = Datatablehelper::tenderListOutput($input, $collection);

        return response()->json($output);
    }

    /**
     * Render tender remarks form.
     *
     * @param \Illuminate\Http\Request $request
     * @param string                   $type
     * @param string                   $id
     *
     * @return \Illuminate\View\View
     */
    public function tenderRemarksForm(Request $request, $type, $id)
    {
        if ($type == 'applicant') {
            $tender = ApplicantProperty::where('TE_APP_ID', $id)->first();
        } else {
            $tender = CitizenProperty::where('CITIZEN_ID', $id)->first();
        }

        return View::make('admin_access.tender_applicant.remarks', compact('tender', 'type'))->render();
    }

    /**
     * Update tender remarks.
     *
     * @param \Illuminate\Http\Request $request
     * @param string                   $type
     * @param string                   $id
     *
     * @return json
     */
    public function tenderRemarksUpdate(Request $request, $type, $id)
    {
        $data = $request->all();
        if ($type == 'applicant') {
            $tender = ApplicantProperty::where('TE_APP_ID', $id)->first();
        } else {
            $tender = CitizenProperty::where('CITIZEN_ID', $id)->first();
        }
        $tender->REMARKS = $data['REMARKS'];
        $tender->UPDATED_BY = Auth::user()->USER_ID;
        $tender->save();

        return response()->json(['flug' => 1, 'message' => trans('common.update_success')]);
    }

    /**
     * Render tender Cancle form.
     *
     * @param \Illuminate\Http\Request $request
     * @param string                   $type
     * @param string                   $id
     *
     * @return \Illuminate\View\View
     */
    public function tenderCancelForm(Request $request, $type, $id)
    {
        if ($type == 'applicant') {
            $tender = ApplicantProperty::where('TE_APP_ID', $id)->first();
        } else {
            $tender = CitizenProperty::where('CITIZEN_ID', $id)->first();
        }

        return View::make('admin_access.tender_applicant.cancel', compact('tender', 'type'))->render();
    }

    /**
     * Update tender Cancle.
     *
     * @param \Illuminate\Http\Request $request
     * @param string                   $type
     * @param string                   $id
     *
     * @return json
     */
    public function tenderCancelUpdate(Request $request, $type, $id)
    {
        $data = $request->all();
        if ($type == 'applicant') {
            $tender = ApplicantProperty::where('TE_APP_ID', $id)->first();
        } else {
            $tender = CitizenProperty::where('CITIZEN_ID', $id)->first();
        }
        $tender->CANCEL_REMARKS = $data['CANCEL_REMARKS'];
        $tender->IS_CANCEL = $tender->IS_CANCEL == 1 ? 0 : 1;
        $tender->UPDATED_BY = Auth::user()->USER_ID;
        $tender->save();

        return response()->json(['flug' => 1, 'message' => trans('common.update_success')]);
    }

    /**
     * Render tender primary selected form.
     *
     * @param \Illuminate\Http\Request $request
     * @param string                   $ts
     * @param string                   $type
     * @param string                   $id
     *
     * @return \Illuminate\View\View
     */
    public function tenderPrimaryApplicantSelectForm(Request $request, $ts, $type, $id)
    {
        $data['type'] = $type;
        if ($type == 'applicant') {
            $data['tender'] = ApplicantProperty::where('TE_APP_ID', $id)->where('SCHEDULE_ID', $ts)->first();
            $data['applicant'] = TenderApplicant::find($id);
        } else {
            $data['tender'] = CitizenApplication::where('CITIZEN_ID', $id)->where('SCHEDULE_ID', $ts)->first();
            $data['citizen'] = Citizen::where('CITIZEN_ID', $id)->first();
            $data['applicant'] = TenderApplicant::find($data['citizen']->TE_APP_ID);
        }
        $data['applicent_district'] = District::where('DISTRICT_ID', $data['applicant']->DISTRICT_ID)
                                            ->pluck('DISTRICT_ENAME');
        $data['applicent_thana'] = Thana::where('THANA_ID', $data['applicant']->THANA_ID)
                                            ->pluck('THANA_ENAME');
        $data['officer'] = Officer::where('TYPE', 1)->first();
        $data['location'] = ProjectLocation::where('IS_ACTIVE', 1)->get();
        $data['project_name'] = Project::where('PROJECT_ID', $data['tender']->PROJECT_ID)
                                            ->pluck('PR_NAME_BN');

        return View::make('admin_access.tender_applicant.primary_selection', $data)->render();
    }

    /**
     * Update primary applicant insert.
     *
     * @param \Illuminate\Http\Request $request
     * @param string                   $ts
     * @param string                   $type
     * @param string                   $id
     *
     * @return json
     */
    public function tenderPrimaryApplicantSave(Request $request, $ts, $type, $id)
    {
        $data = $request->all();
        // Check name before insert
        $name_exist = TenderLetter::where('LATTER_NUMBER', $data['sharok_no'])->first();
        if (!empty($name_exist)) {
            return response()->json(['flug' => 0, 'message' => trans('lease.is_primary_exist')]);
            exit;
        }
        $user = Auth::user()->USER_ID;
        $installment = TenderInstallment::saveData($data, $user, $ts, $type, $id);

        //Email
        if ($type == 'applicant') {
            $applicant = TenderApplicant::where('TE_APP_ID', $id)->first();
            $data['letter'] = TenderLetter::where('TE_APP_ID', $id)->where('SCHEDULE_ID', $ts)->where('IS_LEASE', 0)->first();
            $data['to_name'] = $applicant->APPLICANT_NAME;
            $data['number'] = $applicant->APPLICANT_PHONE;
            $data['to_email'] = $applicant->EMAIL;
            $bid_rate = ApplicantProperty::where('TE_APP_ID', $id)->where('SCHEDULE_ID', $ts)->pluck('BID_AMOUNT');
            $max_inst = TenderInstallment::where('TE_APP_ID', $id)->where('SCHEDULE_ID', $ts)->max('INSTALLMENT');
        } else {
            $citizen = Citizen::where('CITIZEN_ID', $id)->with('getTenderCitizen')->first();
            $data['letter'] = TenderLetter::where('CITIZEN_ID', $id)->where('SCHEDULE_ID', $ts)->where('IS_LEASE', 0)->first();
            $data['to_email'] = $citizen->getTenderCitizen->EMAIL;
            $data['number'] = $citizen->getTenderCitizen->MOBILE_NO;
            $data['to_name'] = $citizen->getTenderCitizen->FULL_NAME;
            $bid_rate = CitizenApplication::where('CITIZEN_ID', $id)->where('SCHEDULE_ID', $ts)->pluck('BID_AMOUNT');
            $max_inst = TenderInstallment::where('CITIZEN_ID', $id)->where('SCHEDULE_ID', $ts)->max('INSTALLMENT');
        }
        if (!empty($data['to_email'])) {
            $data['template'] = 'primary_letter_tender';
            $data['subject'] = 'Select Primary Applicant';
            $send_email = Helpers::sendEmail($data);
        }

        // Send SMS
        $sms = $this->generatePrimarySMS($ts, $bid_rate, $max_inst);
        $send_sms = Helpers::sendSMS($data['number'], $sms);

        return response()->json(['flug' => 1, 'message' => trans('common.save_success')]);
    }

    /**
     * Generate lease primary select SMS.
     *
     * @param string $ts       Tender schedule id
     * @param string $bid_rate Applicant bid rate
     * @param string $max_inst maximum number of installment
     *
     * @return string
     */
    private function generatePrimarySMS($ts, $bid_rate, $max_inst)
    {
        $schedule = TenderSchedule::with('getProject', 'getCategory', 'getProjectType', 'getProjectDetail')
                                    ->where('SCHEDULE_ID', $ts)
                                    ->first();
        $sms = 'আপনার আবেদনকৃত ';
        $sms .= $schedule->getProject->PR_NAME_BN.' এর ';
        $sms .= Helpers::en2bn($schedule->getProjectDetail->POSITION).' তলার ';
        $sms .= $schedule->getProjectDetail->PR_SSF_NO.' নং ';
        $sms .= $schedule->getCategory->CATE_NAME.' এর জন্য আপনার প্রস্তাবিত দর ';
        $sms .= Helpers::en2bn(number_format($bid_rate, 2)).' টাকা সর্বোচ্চ বিবেচিত হওয়ায় ';
        $sms .= 'আপনি প্রাথমিকভাবে নির্বাচিত হয়েছেন। আপনি উক্ত টাকা ';
        $sms .= Helpers::en2bn($max_inst).' টি সমান কিস্তিতে পরিশোধ করবেন।';

        return $sms;
    }

    /**
     * Show Tender primary applicant selected page.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\View\View
     */
    public function tenderPrimaryApplicantList(Request $request)
    {
        $tender = Tender::where('IS_LEASE', 0)->where('IS_ACTIVE', 1)->lists('TENDER_TITLE', 'TENDER_ID');

        return view('admin_access.tender_applicant.primary_list', compact('tender'));
    }

    /**
     * Get tender primary selected applicant bid data.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return json
     */
    public function tenderPrimaryListData(Request $request)
    {
        $input = $request->all();
        $collection = TenderApplicant::getPrimaryListData($input);
        $output = Datatablehelper::tenderPrimaryOutput($input, $collection);

        return response()->json($output);
    }

    /**
     * Update tender primary applicant letter save.
     *
     * @param \Illuminate\Http\Request $request
     * @param string                   $ts
     * @param string                   $type
     * @param string                   $id
     *
     * @return PDF
     */
    public function tenderPrimaryLetterPrint(Request $request, $ts, $type, $id)
    {
        if ($type == 'applicant') {
            $data['letter'] = TenderLetter::where('TE_APP_ID', $id)->where('SCHEDULE_ID', $ts)->where('IS_LEASE', 0)->where('TYPE', 0)->first();
        } else {
            $data['letter'] = TenderLetter::where('CITIZEN_ID', $id)->where('SCHEDULE_ID', $ts)->where('IS_LEASE', 0)->where('TYPE', 0)->first();
        }
        $pdf_data = View::make('admin_access.tender_applicant.primary_letter_print', $data);
        $file_name = 'NCC-'.'tender-primary-letter'.'.pdf';

        return Helpers::makePDF($file_name, $pdf_data);
    }

    /**
     * Show tender primary applicant selected page.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\View\View
     */
    public function tenderFinalApplicantList(Request $request)
    {
        $tender = Tender::where('IS_ACTIVE', 1)->where('IS_LEASE', 0)->lists('TENDER_TITLE', 'TENDER_ID');

        return view('admin_access.tender_applicant.final_list', compact('tender'));
    }

    /**
     * Get tender final selected applicant bid data.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return json
     */
    public function tenderfinalListData(Request $request)
    {
        $input = $request->all();
        $collection = TenderApplicant::getTenderFinalListData($input);
        $output = Datatablehelper::tenderFinalOutput($input, $collection);

        return response()->json($output);
    }

    /**
     *  tender final selected form.
     *
     * @param \Illuminate\Http\Request $request
     * @param string                   $ts
     * @param string                   $type
     * @param string                   $id
     *
     * @return \Illuminate\View\View
     */
    public function tenderFinalApplicantSelectForm(Request $request, $ts, $type, $id)
    {
        $data['type'] = $type;
        $data['user_id'] = $id;
        if ($type == 'applicant') {
            $data['tender'] = ApplicantProperty::where('TE_APP_ID', $id)->first();
            $data['applicant'] = TenderApplicant::find($id);
            $data['applicent_district'] = District::where('DISTRICT_ID', $data['applicant']->DISTRICT_ID)->pluck('DISTRICT_ENAME');
            $data['applicent_thana'] = Thana::where('THANA_ID', $data['applicant']->THANA_ID)->pluck('THANA_ENAME');
        } else {
            $data['tender'] = CitizenApplication::where('CITIZEN_ID', $id)->first();
            $data['citizen'] = Citizen::where('CITIZEN_ID', $id)->with('getTenderCitizen')->first();
            $data['applicent_district'] = District::where('DISTRICT_ID', $data['citizen']->DISTRICT_ID)->pluck('DISTRICT_ENAME');
            $data['applicent_thana'] = Thana::where('THANA_ID', $data['citizen']->THANA_ID)->pluck('THANA_ENAME');
        }

        $data['officer'] = Officer::where('TYPE', 1)->first();
        $data['location'] = ProjectLocation::where('IS_ACTIVE', 1)->get();
        $data['project_name'] = Project::where('PROJECT_ID', $data['tender']->PROJECT_ID)->pluck('PR_NAME_BN');

        return View::make('admin_access.tender_applicant.final_selection', $data)->render();
    }

    /**
     * save tender final selected form.
     *
     * @param \Illuminate\Http\Request $request
     * @param string                   $ts
     * @param string                   $type
     * @param string                   $id
     *
     * @return json
     */
    public function tenderFinalApplicantSave(Request $request, $ts, $type, $id)
    {
        $data = $request->all();
        // Check LATTER_NUMBER before insert
        $number_exist = TenderLetter::where('LATTER_NUMBER', $data['sharok_no'])->first();
        if (!empty($number_exist)) {
            return response()->json(['flug' => 0, 'message' => trans('lease.is_sharok_exist')]);
            exit;
        }
        $user = Auth::user()->USER_ID;
        $tender_final_select = TenderApplicant::tenderFinalSave($data, $ts, $type, $user, $id);
        // Letter Email
        if ($type == 'applicant') {
            $te_applicant = TenderApplicant::where('TE_APP_ID', $id)->first();
            $data['to_name'] = $te_applicant->APPLICANT_NAME;
            $data['to_email'] = $te_applicant->EMAIL;
            $data['number'] = $te_applicant->APPLICANT_PHONE;
            $data['letter'] = TenderLetter::where('TE_APP_ID', $id)->where('SCHEDULE_ID', $ts)
                                            ->where('IS_LEASE', 0)
                                            ->where('TYPE', 1)
                                            ->first();
        } else {
            $citizen = Citizen::where('CITIZEN_ID', $id)->with('getTenderCitizen')->first();
            $data['to_name'] = $citizen->getTenderCitizen->FULL_NAME;
            $data['to_email'] = $citizen->getTenderCitizen->EMAIL;
            $data['number'] = $citizen->getTenderCitizen->MOBILE_NO;
            $data['letter'] = TenderLetter::where('CITIZEN_ID', $id)
                                            ->where('SCHEDULE_ID', $ts)
                                            ->where('IS_LEASE', 0)
                                            ->where('TYPE', 1)
                                            ->first();
        }
        if (!empty($data['to_email'])) {
            $data['template'] = 'final_letter_lease';
            $data['subject'] = 'Tender final select';
            $send_email = Helpers::sendEmail($data);
        }

        // Send SMS
        $sms = $this->generateFinalSMS($ts);
        $send_sms = Helpers::sendSMS($data['number'], $sms);

        return response()->json(['flug' => 1, 'message' => trans('common.save_success')]);
    }

    /**
     * Generate tender final select SMS.
     *
     * @param string $ts Tender schedule id
     *
     * @return string
     */
    private function generateFinalSMS($ts)
    {
        $schedule = TenderSchedule::with('getProject', 'getCategory', 'getProjectType', 'getProjectDetail')
                                    ->where('SCHEDULE_ID', $ts)
                                    ->first();
        $sms = 'আপনার আবেদনকৃত ';
        $sms .= $schedule->getProject->PR_NAME_BN.' এর ';
        $sms .= Helpers::en2bn($schedule->getProjectDetail->POSITION).' তলার ';
        $sms .= $schedule->getProjectDetail->PR_SSF_NO.' নং ';
        $sms .= $schedule->getCategory->CATE_NAME;
        $sms .= ' এর জন্য আপনি চূড়ান্তভাবে নির্বাচিত হয়েছেন।';

        return $sms;
    }

    /**
     * Tender final letter print.
     *
     * @param \Illuminate\Http\Request $request
     * @param string                   $ts      tender schedule id
     * @param string                   $type    tender applicant type(applicant/citizen)
     * @param string                   $id      tender Letter id
     *
     * @return PDF
     */
    public function tenderFinalLetterPrint(Request $request, $ts, $type, $id)
    {
        if ($type == 'applicant') {
            $data['letter'] = TenderLetter::where('TE_APP_ID', $id)->where('SCHEDULE_ID', $ts)->where('IS_LEASE', 0)->where('TYPE', 1)->first();
        } else {
            $data['letter'] = TenderLetter::where('CITIZEN_ID', $id)->where('SCHEDULE_ID', $ts)->where('IS_LEASE', 0)->where('TYPE', 1)->first();
        }
        $pdf_data = View::make('admin_access.tender_applicant.final_letter_print', $data);
        $file_name = 'NCC-'.'lease-Final-letter'.'.pdf';

        return Helpers::makePDF($file_name, $pdf_data);
    }
}
