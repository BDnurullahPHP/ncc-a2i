<?php

namespace App\Http\Controllers\Admin_access;

use App\Http\Controllers\Controller;
use App\Models\Land;
use App\Models\LandCase;
use App\Models\LandCategory;
use App\Models\LandDag;
use App\Models\LandDocs;
use App\Models\LandTax;
use App\Models\LandUse;
use App\Models\Module;
use App\Models\Mouza;
use App\Models\Ward;
use App\Models\Zone;
use App\Ncc\Datatablehelper;
use App\Ncc\Helpers;
use Auth;
use DB;
use Excel;
use Illuminate\Http\Request;
use Response;
use View;

/**
 * Class     LandController.
 *
 * @author   Abdul awal <abdulawal@atilimited.net>
 */
class LandController extends Controller
{
    /**
     * Show land page.
     *
     * @return \Illuminate\View\View
     */
    public function landIndex()
    {
        $link_url = 'land_list'; // module url
        $data['access'] = Module::checkPrevilege($link_url); // get access info by module url
        return view('admin_access.land.index', $data);
    }

    /**
     * Get land data.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return json
     */
    public function landData(Request $request)
    {
        $input = $request->all();
        $collection = Land::getData($input);
        $output = Datatablehelper::landOutput($input, $collection);

        return Response::json($output);
    }

    /**
     * Render land view page.
     *
     * @param \Illuminate\Http\Request $request
     * @param string                   $id
     *
     * @return \Illuminate\View\View
     */
    public function landView(Request $request, $id)
    {
        $data['land'] = Land::where('LAND_ID', $id)
                            ->with([
                                    'landDagNo',
                                    'getArea',
                                    'getWord',
                                    'getMouza',
                                    'landDoc',
                                    'landTax',
                                    'getCaregory',
                                    'getSubCaregory',
                                ])->first();
        $data['land_use'] = LandUse::with(['getCaregory', 'getSubCaregory'])
                                        ->where('LAND_ID', $id)
                                        ->get();
        $data['total_use'] = LandUse::where('LAND_ID', $id)
                                    ->select(DB::raw('SUM(USES) AS total_land'))
                                    ->first();
        $data['land_rs_cs'] = $this->landRsCsData($id);
        $data['land_dolil'] = LandDocs::docTypedata($id);
        $data['land_tax'] = LandTax::where('LAND_ID', $id)->get();
        $data['land_case'] = LandCase::where('LAND_ID', $id)->first();

        return view('admin_access.land.view', $data)->render();
    }

    /**
     * Render land PDF file.
     *
     * @param \Illuminate\Http\Request $request
     * @param string                   $id
     *
     * @return PDF
     */
    public function landPrint(Request $request, $id)
    {
        $data['land'] = Land::where('LAND_ID', $id)
                            ->with([
                                    'landDagNo',
                                    'getArea',
                                    'getWord',
                                    'getMouza',
                                    'landDoc',
                                    'landTax',
                                    'getCaregory',
                                    'getSubCaregory',
                                ])->first();
        $data['land_use'] = LandUse::with(['getCaregory', 'getSubCaregory'])
                                        ->where('LAND_ID', $id)
                                        ->get();
        $data['total_use'] = LandUse::where('LAND_ID', $id)
                                    ->select(DB::raw('SUM(USES) AS total_land'))
                                    ->first();
        $data['land_rs_cs'] = $this->landRsCsData($id);
        $data['land_dolil'] = LandDocs::docTypedata($id);
        $data['land_tax'] = LandTax::where('LAND_ID', $id)->get();
        $data['land_case'] = LandCase::where('LAND_ID', $id)->first();

        $pdf_data = View::make('admin_access.land.print', $data)->render();
        $file_name = 'NCC-land-'.$data['land']->LAND_NUMBER.'.pdf';

        return Helpers::makePDF($file_name, $pdf_data);
    }

    /**
     * Show land report page.
     *
     * @return \Illuminate\View\View
     */
    public function landReport()
    {
        $link_url = 'land_report'; // module url
        $access = Module::checkPrevilege($link_url); // get access info by module url
        $area = Zone::where('PARENT_ID', 0)->where('IS_ACTIVE', 1)->lists('WARD_NAME_BN', 'WARD_ID');
        $caregory = LandCategory::where('PARENT_ID', 0)->where('IS_ACTIVE', 1)->lists('CAT_NAME_BN', 'CAT_ID');

        return view('admin_access.land.report', compact('area', 'caregory', 'access'));
    }

    /**
     * Get land report data.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return json
     */
    public function landReportData(Request $request)
    {
        $input = $request->all();
        $collection = Land::reportData($input);
        $output = Datatablehelper::landReportOutput($input, $collection);

        return Response::json($output);
    }

    /**
     *@Nurullah /17-april-2017
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return PDF
     */
    public function landReportDataPrint(Request $request)
    {
        $input = $request->all();
        $land_data = Land::reportDataPrint($input);
        // Make PDF
        $pdf_data = View::make('admin_access.land.report_print', compact('land_data'));
        $file_name = 'NCC-land-report'.'.pdf';

        return Helpers::makePDF($file_name, $pdf_data);
    }

    /**
     *@Nurullah /29-July-2017
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return excel
     */
    public function landReportDataExcel(Request $request)
    {
        $input = $request->all();
        $land_data = Land::reportDataPrint($input);
        //------------ Export Data In Excel start
        $landArray = [];
        $landArray[] = ['জমির নাম্বার', 'ওয়ার্ড', 'মৌজা', 'মোট জমি(শতাংশ)', 'নির্মিত']; // Excel Header
        $totalLand = 0;
        foreach ($land_data as $key =>$value) {
            $landArray[] = [Helpers::en2bn($value->LAND_NUMBER), Helpers::en2bn($value->getWord->WARD_NAME_BN), Helpers::en2bn($value->getMouza->MOUZA_NAME_BN), Helpers::en2bn($value->TOTAL_LAND), Helpers::en2bn(date('d/m/Y', strtotime($value->CREATED_AT)))];
            $totalLand += $value->TOTAL_LAND;
        }
        $landArray[] = ['', '', 'মোট জমি', Helpers::en2bn($totalLand).' (শতাংশ)', ''];
        // Generate Excel
        Excel::create('NCC_Land_info', function ($excel) use ($landArray) {
            $excel->sheet('ncc_land', function ($sheet) use ($landArray) {
                $sheet->cells('A1:E1', function ($cells) {
                    $cells->setBackground('#c5c3c3');
                    $cells->setFontColor('#ffffff');
                    $cells->setFontWeight('bold');
                    $cells->setFontSize(16);
                });
                $sheet->freezeFirstRow();
                $sheet->fromArray($landArray, null, 'A1', false, false);
            });
        })->download('xls');
    }

    /**
     * Render land create form.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\View\View
     */
    public function landCreateForm(Request $request)
    {
        $area = Zone::where('PARENT_ID', 0)->where('IS_ACTIVE', 1)->lists('WARD_NAME_BN', 'WARD_ID');
        $caregory = LandCategory::where('PARENT_ID', 0)->where('IS_ACTIVE', 1)->lists('CAT_NAME_BN', 'CAT_ID');

        return View::make('admin_access.land.create', compact('area', 'caregory'))->render();
    }

    /**
     * Create land.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return json
     */
    public function landCreate(Request $request)
    {
        $data = $request->all();
        $user = Auth::user()->USER_ID;
        $land_no = $data['land_no'];

        // Check land no before insert
        // $land_exist = Land::where('LAND_NUMBER',$land_no)->first();
        // if (!empty($land_exist)) {
        //     return response()->json(['flug' => 0, 'message' => trans('land.land_exists')]);exit;
        //} else {
        $land = Land::createData($data, $user, $id = null);
        //}

        return response()->json(['flug' => 1, 'message' => trans('common.save_success')]);
    }

    /**
     * Render land edit form.
     *
     * @param \Illuminate\Http\Request $request
     * @param string                   $id
     *
     * @return \Illuminate\View\View
     */
    public function landEditForm(Request $request, $id)
    {
        $data['land_data'] = Land::where('LAND_ID', $id)->with(['landCase'])->first();
        $data['land_rs_cs'] = $this->landRsCsData($id);
        $data['area'] = Zone::where('PARENT_ID', 0)->where('IS_ACTIVE', 1)->lists('WARD_NAME_BN', 'WARD_ID');
        $data['ward'] = Zone::where('PARENT_ID', $data['land_data']->AREA)
                                    ->lists('WARD_NAME_BN', 'WARD_ID');
        $data['mouza'] = Mouza::where('WARD_ID', $data['land_data']->WARD_ID)
                                    ->lists('MOUZA_NAME_BN', 'MOUZA_ID');
        $data['caregory'] = LandCategory::where('PARENT_ID', 0)->where('IS_ACTIVE', 1)->lists('CAT_NAME_BN', 'CAT_ID');
        $data['land_use'] = LandUse::with(['getCaregory', 'getSubCaregory'])
                                        ->where('LAND_ID', $id)
                                        ->get();
        $data['total_use'] = LandUse::where('LAND_ID', $id)
                                    ->select(DB::raw('SUM(USES) AS total_land'))
                                    ->first();
        $data['land_dolil'] = LandDocs::docTypedata($id);
        $data['land_tax'] = LandTax::where('LAND_ID', $id)->get();

        return View::make('admin_access.land.edit', $data)->render();
    }

    /**
     * Update land.
     *
     * @param \Illuminate\Http\Request $request
     * @param string                   $id
     *
     * @return json
     */
    public function landUpdate(Request $request, $id)
    {
        $data = $request->all();
        $user = Auth::user()->USER_ID;
        $land_no = $data['land_no'];

        // Check land no before update
        // $land_exist = Land::whereNotIn('LAND_ID', [$id])->where('LAND_NUMBER',$land_no)->first();
        // if (!empty($land_exist)) {
        //     return response()->json(['flug' => 0, 'message' => trans('land.land_exists')]);exit;
        // } else {
        $land = Land::createData($data, $user, $id);
        //}

        return response()->json(['flug' => 1, 'message' => trans('common.update_success')]);
    }

    /**
     * Get land RS/CS/SA  by land id.
     *
     * @param string $id
     *
     * @return array
     */
    private function landRsCsData($id)
    {
        $data = [];
        $land_rs_cs = LandDag::where('LAND_ID', $id)->groupBy('TYPE')->get();

        foreach ($land_rs_cs as $key => $val) {
            $dag_type_data = LandDag::where('LAND_ID', $id)->where('TYPE', $val->TYPE)->get();

            foreach ($dag_type_data as $index => $td) {
                $data[$key]['TYPE'] = $val->TYPE;
                if ($val->TYPE == 1) {
                    $data[$key]['TYPE_NAME'] = trans('land.khatian_no_rs');
                    $data[$key]['ATTR_ID'] = 'khatian_multiple';
                } elseif ($val->TYPE == 2) {
                    $data[$key]['TYPE_NAME'] = trans('land.dag_no_rs');
                    $data[$key]['ATTR_ID'] = 'dag_multiple';
                } elseif ($val->TYPE == 3) {
                    $data[$key]['TYPE_NAME'] = trans('land.jl_no_rs');
                    $data[$key]['ATTR_ID'] = 'jl_multiple';
                } elseif ($val->TYPE == 4) {
                    $data[$key]['TYPE_NAME'] = trans('land.owner_rs');
                    $data[$key]['ATTR_ID'] = 'owner_multiple';
                }
                $data[$key]['LAND_DAG'][$index]['DAG_ID'] = $td->DAG_ID;
                $data[$key]['LAND_DAG'][$index]['LAND_ID'] = $td->LAND_ID;
                $data[$key]['LAND_DAG'][$index]['RS'] = $td->RS;
                $data[$key]['LAND_DAG'][$index]['CS'] = $td->CS;
                $data[$key]['LAND_DAG'][$index]['SA'] = $td->SA;
                $data[$key]['LAND_DAG'][$index]['TYPE'] = $td->TYPE;
            }
        }

        return $data;
    }

    /**
     * Delete land dag no.
     *
     * @param \Illuminate\Http\Request $request
     * @param string                   $id
     *
     * @return string
     */
    public function landDagDelete(Request $request, $id)
    {
        LandDag::find($id)->delete();

        return trans('common.delete_data');
    }

    /**
     * Delete land use.
     *
     * @param \Illuminate\Http\Request $request
     * @param string                   $id
     *
     * @return string
     */
    public function landUseDelete(Request $request, $id)
    {
        LandUse::find($id)->delete();

        return trans('common.delete_data');
    }

    /**
     * Delete land document.
     *
     * @param \Illuminate\Http\Request $request
     * @param string                   $id
     *
     * @return string
     */
    public function landDocumentDelete(Request $request, $id, $type)
    {
        $land_doc = LandDocs::find($id);
        $user = Auth::user()->USER_ID;

        if ($type == 'dolil') {
            if (!empty($land_doc->DOLIL)) {
                if (file_exists(public_path().'/upload/land/'.$land_doc->DOLIL)) {
                    unlink(public_path().'/upload/land/'.$land_doc->DOLIL);
                }
                $land_doc->DOLIL = null;
                $land_doc->UPDATED_BY = $user;
                $land_doc->save();
            }
        } elseif ($type == 'dcr') {
            if (!empty($land_doc->DCR)) {
                if (file_exists(public_path().'/upload/land/'.$land_doc->DCR)) {
                    unlink(public_path().'/upload/land/'.$land_doc->DCR);
                }
                $land_doc->DCR = null;
                $land_doc->UPDATED_BY = $user;
                $land_doc->save();
            }
        } elseif ($type == 'ledger') {
            if (!empty($land_doc->LEDGER)) {
                if (file_exists(public_path().'/upload/land/'.$land_doc->LEDGER)) {
                    unlink(public_path().'/upload/land/'.$land_doc->LEDGER);
                }
                $land_doc->LEDGER = null;
                $land_doc->UPDATED_BY = $user;
                $land_doc->save();
            }
        } elseif ($type == 'bahia') {
            if (!empty($land_doc->BAYA_DOLIL)) {
                if (file_exists(public_path().'/upload/land/'.$land_doc->BAYA_DOLIL)) {
                    unlink(public_path().'/upload/land/'.$land_doc->BAYA_DOLIL);
                }
                $land_doc->BAYA_DOLIL = null;
                $land_doc->UPDATED_BY = $user;
                $land_doc->save();
            }
        }

        if (empty($land_doc->DOLIL) && empty($land_doc->DCR) && empty($land_doc->LEDGER) && empty($land_doc->BAYA_DOLIL)) {
            $land_doc->delete();
        }

        return trans('common.delete_data');
    }

    /**
     * Delete land document.
     *
     * @param \Illuminate\Http\Request $request
     * @param string                   $id
     *
     * @return string
     */
    public function landTaxDelete(Request $request, $id)
    {
        $land_tax = LandTax::find($id);

        if (file_exists(public_path().'/upload/land/'.$land_tax->RECEIPT.'.'.$land_tax->FILE_TYPE)) {
            unlink(public_path().'/upload/land/'.$land_tax->RECEIPT.'.'.$land_tax->FILE_TYPE);
        }
        $land_tax->delete();

        return trans('common.delete_data');
    }

    /**
     * Delete land data.
     *
     * @param \Illuminate\Http\Request $request
     * @param string                   $id
     *
     * @return string
     */
    public function landDelete(Request $request, $id)
    {
        $land = Land::find($id);
        $land_doc = LandDocs::where('LAND_ID', '=', $land->LAND_ID)->get();
        $land_tax = LandTax::where('LAND_ID', '=', $land->LAND_ID)->get();

        // Delete land docs file
        if (!empty($land_doc) && count($land_doc) > 0) {
            foreach ($land_doc as $key => $docs) {
                if (!empty($docs->BAYA_DOLIL)) {
                    if (file_exists(public_path().'/upload/land/'.$docs->BAYA_DOLIL)) {
                        unlink(public_path().'/upload/land/'.$docs->BAYA_DOLIL);
                    }
                }
                if (!empty($docs->DOLIL)) {
                    if (file_exists(public_path().'/upload/land/'.$docs->DOLIL)) {
                        unlink(public_path().'/upload/land/'.$docs->DOLIL);
                    }
                }
                if (!empty($docs->DCR)) {
                    if (file_exists(public_path().'/upload/land/'.$docs->DCR)) {
                        unlink(public_path().'/upload/land/'.$docs->DCR);
                    }
                }
                if (!empty($docs->LEDGER)) {
                    if (file_exists(public_path().'/upload/land/'.$docs->LEDGER)) {
                        unlink(public_path().'/upload/land/'.$docs->LEDGER);
                    }
                }
            }
        }

        // Delete land tax file
        if (!empty($land_tax) && count($land_tax) > 0) {
            foreach ($land_tax as $key => $tax) {
                if (!empty($tax->RECEIPT)) {
                    $tax_file = $tax->RECEIPT.'.'.$tax->FILE_TYPE;
                    if (file_exists(public_path().'/upload/land/'.$tax_file)) {
                        unlink(public_path().'/upload/land/'.$tax_file);
                    }
                }
            }
        }

        $land->delete();

        return trans('common.delete_data');
    }

    /**
     * Change land status.
     *
     * @param \Illuminate\Http\Request $request
     * @param string                   $id
     *
     * @return string
     */
    public function landStatus(Request $request, $id)
    {
        $land = Land::find($id);

        if ($land->IS_ACTIVE == 1) {
            $land->IS_ACTIVE = 0;
        } else {
            $land->IS_ACTIVE = 1;
        }
        $land->save();

        return trans('common.status_change');
    }

    /**
     * Show the land category.
     *
     * @return \Illuminate\View\View
     */
    public function landCategory()
    {
        $link_url = 'land_category'; // module url
        $data['access'] = Module::checkPrevilege($link_url); // get access info by module url
        return view('admin_access.land_category.index', $data);
    }

    /**
     * Render land category create form.
     *
     * @return \Illuminate\View\View
     */
    public function categoryCreateForm()
    {
        return View::make('admin_access.land_category.create')->render();
    }

    /**
     * Land category save.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return json
     */
    public function landCategorySave(Request $request)
    {
        $data = $request->all();
        // Check name before insert
        $name_exist = LandCategory::where('CAT_NAME', $data['CAT_NAME'])->first();
        if (!empty($name_exist)) {
            return response()->json(['flug' => 0, 'message' => trans('land.cat_name_exists')]);
            exit;
        }
        // Check bangla name before insert
        $bn_name_exist = LandCategory::where('CAT_NAME_BN', $data['CAT_NAME_BN'])->first();
        if (!empty($bn_name_exist)) {
            return response()->json(['flug' => 0, 'message' => trans('land.cat_bn_name_exists')]);
            exit;
        }

        $category = new LandCategory();
        $category->CAT_NAME = $data['CAT_NAME'];
        $category->CAT_NAME_BN = $data['CAT_NAME_BN'];
        $category->IS_ACTIVE = isset($data['IS_ACTIVE']) ? $data['IS_ACTIVE'] : 0;
        $category->CREATED_BY = Auth::user()->USER_ID;
        $category->save();

        return response()->json(['flug' => 1, 'message' => trans('common.save_success')]);
    }

    /**
     * Get datatable source data.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return json
     */
    public function landCategoryData(Request $request)
    {
        $input = $request->all();
        $collection = LandCategory::getData($input);
        $output = Datatablehelper::datatableOutput($input, $collection);

        return Response::json($output);
    }

    /**
     * Get land category edit form.
     *
     * @param \Illuminate\Http\Request $request
     * @param string                   $id
     *
     * @return \Illuminate\View\View
     */
    public function categoryEditForm(Request $request, $id)
    {
        $category = LandCategory::find($id);

        return View::make('admin_access.land_category.edit', compact('category'))->render();
    }

    /**
     * Land category save.
     *
     * @param \Illuminate\Http\Request $request
     * @param string                   $id
     *
     * @return json
     */
    public function landCategoryUpdate(Request $request, $id)
    {
        $data = $request->all();
        // Check name before insert
        $name_exist = LandCategory::whereNotIn('CAT_ID', [$id])->where('CAT_NAME', $data['CAT_NAME'])->first();
        if (!empty($name_exist)) {
            return response()->json(['flug' => 0, 'message' => trans('land.cat_name_exists')]);
            exit;
        }
        // Check bangla name before insert
        $bn_name_exist = LandCategory::whereNotIn('CAT_ID', [$id])->where('CAT_NAME_BN', $data['CAT_NAME_BN'])->first();
        if (!empty($bn_name_exist)) {
            return response()->json(['flug' => 0, 'message' => trans('land.cat_bn_name_exists')]);
            exit;
        }

        $category = LandCategory::find($id);
        $category->CAT_NAME = $data['CAT_NAME'];
        $category->CAT_NAME_BN = $data['CAT_NAME_BN'];
        $category->IS_ACTIVE = isset($data['IS_ACTIVE']) ? $data['IS_ACTIVE'] : 0;
        $category->UPDATE_BY = Auth::user()->USER_ID;
        $category->save();

        return response()->json(['flug' => 1, 'message' => trans('common.update_success')]);
    }

    /**
     * Change category status.
     *
     * @param \Illuminate\Http\Request $request
     * @param string                   $id
     *
     * @return string
     */
    public function landCategoryStatus(Request $request, $id)
    {
        $category = LandCategory::find($id);

        if ($category->IS_ACTIVE == 1) {
            $category->IS_ACTIVE = 0;
        } else {
            $category->IS_ACTIVE = 1;
        }
        $category->UPDATE_BY = Auth::user()->USER_ID;
        $category->save();

        return trans('common.status_change');
    }

    /**
     * Land category delete.
     *
     * @param \Illuminate\Http\Request $request
     * @param string                   $id
     *
     * @return string
     */
    public function landCategoryDelete(Request $request, $id)
    {
        LandCategory::where('CAT_ID', $id)->delete();

        return trans('common.delete_data');
    }

    /**
     * Show the land subcategory.
     *
     * @return \Illuminate\View\View
     */
    public function landSubcategory()
    {
        $link_url = 'land_subcategory'; // module url
        $data['access'] = Module::checkPrevilege($link_url); // get access info by module url
        return view('admin_access.land_subcategory.index', $data);
    }

    /**
     * Get land subcategory datatable source data.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return json
     */
    public function landSubCategoryData(Request $request)
    {
        $input = $request->all();
        $collection = LandCategory::getSubcategoryData($input);
        $output = Datatablehelper::subCategoryOutput($input, $collection);

        return response()->json($output);
    }

    /**
     * Render land subcategory create form.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\View\View
     */
    public function subCategoryCreateForm(Request $request)
    {
        $category = LandCategory::where('PARENT_ID', 0)->where('IS_ACTIVE', 1)->lists('CAT_NAME_BN', 'CAT_ID');

        return View::make('admin_access.land_subcategory.create', compact('category'))->render();
    }

    /**
     * Land subcategory save.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return json
     */
    public function landSubCategoryCreate(Request $request)
    {
        $data = $request->all();
        $user = Auth::user()->USER_ID;
        // Check name before insert
        $name_exist = LandCategory::whereIn('PARENT_ID', [$data['category']])->where('CAT_NAME', $data['CAT_NAME'])->first();
        if (!empty($name_exist)) {
            return response()->json(['flug' => 0, 'message' => trans('land.subcat_name_exists')]);
            exit;
        }
        // Check bangla name before insert
        $bn_name_exist = LandCategory::whereIn('PARENT_ID', [$data['category']])->where('CAT_NAME_BN', $data['CAT_NAME_BN'])->first();
        if (!empty($bn_name_exist)) {
            return response()->json(['flug' => 0, 'message' => trans('land.subcat_bn_name_exists')]);
            exit;
        }
        $category = LandCategory::saveCategory($data, $user);

        return response()->json(['flug' => 1, 'message' => trans('common.save_success')]);
    }

    /**
     * Get land subcategory edit form.
     *
     * @param \Illuminate\Http\Request $request
     * @param string                   $id
     *
     * @return \Illuminate\View\View
     */
    public function subcategoryEditForm(Request $request, $id)
    {
        $subcategory = LandCategory::find($id);
        $category = LandCategory::where('PARENT_ID', 0)->where('IS_ACTIVE', 1)->select('CAT_NAME_BN', 'CAT_ID')->get();

        return View::make('admin_access.land_subcategory.edit', compact('subcategory', 'category'))->render();
    }

    /**
     * Land subcategory update.
     *
     * @param \Illuminate\Http\Request $request
     * @param string                   $id
     *
     * @return json
     */
    public function landSubcategoryUpdate(Request $request, $id)
    {
        $data = $request->all();
        $user = Auth::user()->USER_ID;
        // Check name before insert
        $name_exist = LandCategory::whereIn('PARENT_ID', [$data['category']])
                                    ->whereNotIn('CAT_ID', [$id])
                                    ->where('CAT_NAME', $data['CAT_NAME'])
                                    ->first();
        if (!empty($name_exist)) {
            return response()->json(['flug' => 0, 'message' => trans('land.subcat_name_exists')]);
            exit;
        }
        // Check bangla name before insert
        $bn_name_exist = LandCategory::whereIn('PARENT_ID', [$data['category']])
                                    ->whereNotIn('CAT_ID', [$id])
                                    ->where('CAT_NAME_BN', $data['CAT_NAME_BN'])
                                    ->first();
        if (!empty($bn_name_exist)) {
            return response()->json(['flug' => 0, 'message' => trans('land.subcat_bn_name_exists')]);
            exit;
        }

        $category = LandCategory::saveCategory($data, $user, $id);

        return response()->json(['flug' => 1, 'message' => trans('common.update_success')]);
    }

    /**
     * Show the Zone.
     *
     * @author sourov <rokibuzzaman@atilimited.net>
     *
     * @return \Illuminate\View\View
     */
    public function zone()
    {
        $link_url = 'zone_list'; // module url
        $data['access'] = Module::checkPrevilege($link_url); // get access info by module url
        return view('admin_access.zone.index', $data);
    }

    /**
     * Render zone create form.
     *
     * @return \Illuminate\View\View
     */
    public function zoneCreateForm()
    {
        return View::make('admin_access.zone.create')->render();
    }

    /**
     * zone save.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return json
     */
    public function zoneSave(Request $request)
    {
        $data = $request->all();
        // Check name before insert
        $name_exist = Zone::where('WARD_NAME', $data['WARD_NAME'])->first();
        if (!empty($name_exist)) {
            return response()->json(['flug' => 0, 'message' => trans('land.zone_name_exists')]);
            exit;
        }
        // Check bangla name before insert
        $bn_name_exist = Zone::where('WARD_NAME_BN', $data['WARD_NAME_BN'])->first();
        if (!empty($bn_name_exist)) {
            return response()->json(['flug' => 0, 'message' => trans('land.zone_bn_name_exists')]);
            exit;
        }

        $zone = new Zone();
        $zone->WARD_NAME = $data['WARD_NAME'];
        $zone->WARD_NAME_BN = $data['WARD_NAME_BN'];
        $zone->IS_ACTIVE = isset($data['IS_ACTIVE']) ? $data['IS_ACTIVE'] : 0;
        $zone->CREATED_BY = Auth::user()->USER_ID;
        $zone->save();

        return response()->json(['flug' => 1, 'message' => trans('common.save_success')]);
    }

    /**
     * Get datatable source data.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return json
     */
    public function zoneData(Request $request)
    {
        $input = $request->all();
        $collection = Zone::getzoneData($input);
        $output = Datatablehelper::zonedatatableOutput($input, $collection);

        return response()->json($output);
    }

    /**
     * Get zone edit form.
     *
     * @param \Illuminate\Http\Request $request
     * @param string                   $id
     *
     * @return \Illuminate\View\View
     */
    public function zoneEditForm(Request $request, $id)
    {
        $zone = Zone::find($id);

        return View::make('admin_access.zone.edit', compact('zone'))->render();
    }

    /**
     * zone update.
     *
     * @param \Illuminate\Http\Request $request
     * @param string                   $id
     *
     * @return json
     */
    public function zoneUpdate(Request $request, $id)
    {
        $data = $request->all();
        // Check name before insert
        $name_exist = Zone::whereNotIn('WARD_ID', [$id])->where('WARD_NAME', $data['WARD_NAME'])->first();
        if (!empty($name_exist)) {
            return response()->json(['flug' => 0, 'message' => trans('land.zone_name_exists')]);
            exit;
        }
        // Check bangla name before insert
        $bn_name_exist = Zone::whereNotIn('WARD_ID', [$id])->where('WARD_NAME_BN', $data['WARD_NAME_BN'])->first();
        if (!empty($bn_name_exist)) {
            return response()->json(['flug' => 0, 'message' => trans('land.zone_bn_name_exists')]);
            exit;
        }

        $zone = Zone::find($id);
        $zone->WARD_NAME = $data['WARD_NAME'];
        $zone->WARD_NAME_BN = $data['WARD_NAME_BN'];
        $zone->IS_ACTIVE = isset($data['IS_ACTIVE']) ? $data['IS_ACTIVE'] : 0;
        $zone->UPDATED_BY = Auth::user()->USER_ID;
        $zone->save();

        return response()->json(['flug' => 1, 'message' => trans('common.update_success')]);
    }

    /**
     * Change zone status.
     *
     * @param \Illuminate\Http\Request $request
     * @param string                   $id
     *
     * @return string
     */
    public function zoneStatus(Request $request, $id)
    {
        $zone = Zone::find($id);

        if ($zone->IS_ACTIVE == 1) {
            $zone->IS_ACTIVE = 0;
        } else {
            $zone->IS_ACTIVE = 1;
        }
        $zone->UPDATED_BY = Auth::user()->USER_ID;
        $zone->save();

        return trans('common.status_change');
    }

    /**
     * zone delete.
     *
     * @param \Illuminate\Http\Request $request
     * @param string                   $id
     *
     * @return string
     */
    public function zoneDelete(Request $request, $id)
    {
        Zone::where('WARD_ID', $id)->delete();

        return trans('common.delete_data');
    }

    /**
     * Show the ward.
     *
     * @author sourov <rokibuzzaman@atilimited.net>
     *
     * @return \Illuminate\View\View
     */
    public function ward()
    {
        $link_url = 'word_list'; // module url
        $data['access'] = Module::checkPrevilege($link_url); // get access info by module url
        return view('admin_access.ward.index', $data);
    }

    /**
     * Get ward datatable source data.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return json
     */
    public function wardData(Request $request)
    {
        $input = $request->all();
        $collection = Zone::getWardData($input);
        $output = Datatablehelper::wardOutput($input, $collection);

        return response()->json($output);
    }

    /**
     * Render ward create form.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\View\View
     */
    public function wardCreateForm(Request $request)
    {
        $zone = Zone::where('PARENT_ID', 0)->where('IS_ACTIVE', 1)->lists('WARD_NAME_BN', 'WARD_ID');

        return View::make('admin_access.ward.create', compact('zone'))->render();
    }

    /**
     * ward save.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return json
     */
    public function wardCreate(Request $request)
    {
        $data = $request->all();
        // Check name before insert
        $name_exist = Zone::where('WARD_NAME', $data['WARD_NAME'])->first();
        if (!empty($name_exist)) {
            return response()->json(['flug' => 0, 'message' => trans('land.ward_name_exists')]);
            exit;
        }
        // Check bangla name before insert
        $bn_name_exist = Zone::where('WARD_NAME_BN', $data['WARD_NAME_BN'])->first();
        if (!empty($bn_name_exist)) {
            return response()->json(['flug' => 0, 'message' => trans('land.ward_bn_name_exists')]);
            exit;
        }

        $zone = new Zone();
        $zone->PARENT_ID = $data['zone'];
        $zone->WARD_NAME = $data['WARD_NAME'];
        $zone->WARD_NAME_BN = $data['WARD_NAME_BN'];
        $zone->IS_ACTIVE = isset($data['IS_ACTIVE']) ? $data['IS_ACTIVE'] : 0;
        $zone->CREATED_BY = Auth::user()->USER_ID;
        $zone->save();

        return response()->json(['flug' => 1, 'message' => trans('common.save_success')]);
    }

    /**
     * Get ward edit form.
     *
     * @param \Illuminate\Http\Request $request
     * @param string                   $id
     *
     * @return \Illuminate\View\View
     */
    public function wardEditForm(Request $request, $id)
    {
        $ward = Zone::find($id);
        $zone = Zone::where('PARENT_ID', 0)->where('IS_ACTIVE', 1)->select('WARD_NAME_BN', 'WARD_ID')->get();

        return View::make('admin_access.ward.edit', compact('ward', 'zone'))->render();
    }

    /**
     * ward update.
     *
     * @param \Illuminate\Http\Request $request
     * @param string                   $id
     *
     * @return json
     */
    public function wardUpdate(Request $request, $id)
    {
        $data = $request->all();
        // Check name before insert
        $name_exist = Zone::whereNotIn('WARD_ID', [$id])->where('WARD_NAME', $data['WARD_NAME'])->first();
        if (!empty($name_exist)) {
            return response()->json(['flug' => 0, 'message' => trans('land.ward_name_exists')]);
            exit;
        }
        // Check bangla name before insert
        $bn_name_exist = Zone::whereNotIn('WARD_ID', [$id])->where('WARD_NAME_BN', $data['WARD_NAME_BN'])->first();
        if (!empty($bn_name_exist)) {
            return response()->json(['flug' => 0, 'message' => trans('land.ward_bn_name_exists')]);
            exit;
        }

        $zone = Zone::find($id);
        $zone->PARENT_ID = $data['zone'];
        $zone->WARD_NAME = $data['WARD_NAME'];
        $zone->WARD_NAME_BN = $data['WARD_NAME_BN'];
        $zone->IS_ACTIVE = isset($data['IS_ACTIVE']) ? $data['IS_ACTIVE'] : 0;
        $zone->UPDATED_BY = Auth::user()->USER_ID;
        $zone->save();

        return response()->json(['flug' => 1, 'message' => trans('common.update_success')]);
    }

    /**
     * Show the List of Mouza.
     *
     * @author Nurullah <nurul@atilimited.net>
     *
     * @return \Illuminate\View\View
     */
    public function mouza()
    {
        $link_url = 'mouza_list'; // module url
        $data['access'] = Module::checkPrevilege($link_url); // get access info by module url
        return view('admin_access.mouza.index', $data);
    }

    /**
     * Show the Mouza Insert Form.
     *
     * @author Nurullah <nurul@atilimited.net>
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\View\View
     */
    public function mouzaCreateForm(Request $request)
    {
        $data['zone'] = Zone::where('PARENT_ID', 0)->where('IS_ACTIVE', 1)->orderBy('WARD_ID', 'asc')->lists('WARD_NAME_BN', 'WARD_ID');

        return View::make('admin_access.mouza.create', $data)->render();
    }

    /**
     * Get datatable source data of Mouza.
     *
     * @author Nurullah <nurul@atilimited.net>
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return json
     */
    public function mouzaData(Request $request)
    {
        $input = $request->all();
        $collection = Mouza::getData($input);
        $output = Datatablehelper::mouzaOutput($input, $collection);

        return response()->json($output);
    }

    /**
     * New Mouza Insert.
     *
     * @author Nurullah <nurul@atilimited.net>
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return json
     */
    public function mouzaSave(Request $request)
    {
        $data = $request->all();
        $user = Auth::user()->USER_ID;
        $name_exist = Mouza::where('WARD_ID', $data['WARD_ID'])
                            ->where('MOUZA_NAME_BN', $data['MOUZA_NAME_BN'])
                            ->first();
        if (!empty($name_exist)) {
            return response()->json(['flug' => 0, 'message' => trans('land.mouza_name_exists')]);
            exit;
        }
        $mouza = Mouza::saveMouza($data, $user);

        return response()->json(['flug' => 1, 'message' => trans('common.save_success')]);
    }

    /**
     *  Mouza update.
     *
     * @author Nurullah <nurul@atilimited.net>
     *
     * @param \Illuminate\Http\Request $request
     * @param string                   $id
     *
     * @return json
     */
    public function mouzaUpdate(Request $request, $id)
    {
        $data = $request->all();
        $user = Auth::user()->USER_ID;
        $name_exist = Mouza::whereNotIn('MOUZA_ID', [$id])
                        ->where('WARD_ID', $data['WARD_ID'])
                        ->where('MOUZA_NAME_BN', $data['MOUZA_NAME_BN'])
                        ->first();
        if (!empty($name_exist)) {
            return response()->json(['flug' => 0, 'message' => trans('land.mouza_name_exists')]);
            exit;
        }
        $mouza = Mouza::saveMouza($data, $user, $id);

        return response()->json(['flug' => 1, 'message' => trans('common.update_success')]);
    }

    /**
     * Get  Mouza edit form.
     *
     * @author Nurullah <nurul@atilimited.net>
     *
     * @param \Illuminate\Http\Request $request
     * @param string                   $id
     *
     * @return \Illuminate\View\View
     */
    public function mouzaEditForm(Request $request, $id)
    {
        $mouza = Mouza::find($id);
        $zone = Zone::where('PARENT_ID', 0)
                    ->where('IS_ACTIVE', 1)
                    ->orderBy('WARD_ID', 'asc')
                    ->select('WARD_ID', 'WARD_NAME_BN')
                    ->get();
        $zone_id = Zone::where('WARD_ID', $mouza->WARD_ID)->pluck('PARENT_ID');
        $wards = Zone::where('PARENT_ID', $zone_id)->select('WARD_NAME_BN', 'WARD_ID')->get();

        return View::make('admin_access.mouza.edit', compact('zone', 'mouza', 'zone_id', 'wards'))->render();
    }

    /**
     * Mouza delete.
     *
     * @author Nurullah <nurul@atilimited.net>
     *
     * @param \Illuminate\Http\Request $request
     * @param string                   $id
     *
     * @return string
     */
    public function mouzaDelete(Request $request, $id)
    {
        Mouza::where('MOUZA_ID', $id)->delete();

        return trans('common.delete_data');
    }

    /**
     * Change Mouza status.
     *
     * @author Nurullah <nurul@atilimited.net>
     *
     * @param \Illuminate\Http\Request $request
     * @param string                   $id
     *
     * @return string
     */
    public function mouzaStatus(Request $request, $id)
    {
        $mouza = Mouza::find($id);
        if ($mouza->IS_ACTIVE == 1) {
            $mouza->IS_ACTIVE = 0;
        } else {
            $mouza->IS_ACTIVE = 1;
        }

        $mouza->UPDATED_BY = Auth::user()->USER_ID;
        $mouza->save();

        return trans('common.status_change');
    }

    /**
     * Check unique Mouza.
     *
     * @author Nurullah <nurul@atilimited.net>
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return string
     */
    public function mouzaUnique(Request $request)
    {
        $data = $request->all();
        $ward_id = $data['WARD_ID'];
        $mouza_name = trim($data['MOUZA_NAME_BN']);
        $jl_no = $data['JL_NO'];
        $mouza_name = str_replace(' ', '-', $mouza_name);
        $mouza = Mouza::where('WARD_ID', $ward_id)
                                ->where('MOUZA_NAME_BN', $mouza_name)
                                ->where('JL_NO', $jl_no)
                                ->first();

        return empty($mouza) ? 0 : 1;
    }

    /**
     * Check unique Mouza in Update.
     *
     * @author Nurullah <nurul@atilimited.net>
     *
     * @param \Illuminate\Http\Request $request
     * @param string                   $id
     *
     * @return string
     */
    public function mouzaUpdateUnique(Request $request, $id)
    {
        $data = $request->all();
        $ward_id = $data['WARD_ID'];
        $mouza_name = trim($data['MOUZA_NAME_BN']);
        $jl_no = $data['JL_NO'];
        $mouza_name = str_replace(' ', '-', $mouza_name);

        $curent = Mouza::where('MOUZA_ID', $id)->where('WARD_ID', $ward_id)->where('MOUZA_NAME_BN', $mouza_name)->where('JL_NO', $jl_no)->first();
        if (!empty($curent)) {
            return 0;
        } else {
            $mouza = Mouza::where('WARD_ID', $ward_id)->where('MOUZA_NAME_BN', $mouza_name)->where('JL_NO', $jl_no)->first();

            return empty($mouza) ? 0 : 1;
        }
    }
}
