<?php

namespace App\Http\Controllers\Admin_access;

use App\Http\Controllers\Controller;
use App\Models\Land;
use App\Models\Module;
use App\Models\Project;
use App\Models\ProjectCatCondition;
use App\Models\ProjectCategory;
use App\Models\ProjectDetails;
use App\Models\ProjectDoc;
use App\Models\ProjectFloor;
use App\Models\ProjectLand;
use App\Models\ProjectLocation;
use App\Models\ProjectType;
use App\Models\Zone;
use App\Ncc\Datatablehelper;
use Auth;
use Helpers;
use Illuminate\Http\Request;
use View;

class ProjectController extends Controller
{
    /**
     * Show project page.
     *
     *@author      Nurullah <nurul@atilimited.net>
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\View\View
     */
    public function marketIndex(Request $request)
    {
        $link_url = 'all_market'; // module url
        $data['access'] = Module::checkPrevilege($link_url); // get access info by module url
        return view('admin_access.market.index', $data);
    }

    /**
     * Get project data.
     *
     *@author      Nurullah <nurul@atilimited.net>
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return json
     */
    public function marketData(Request $request)
    {
        $input = $request->all();
        $collection = Project::getMarketData($input);
        $output = Datatablehelper::marketOutput($input, $collection);

        return response()->json($output);
    }

    /**
     * Render project create form.
     *
     *@author      Nurullah <nurul@atilimited.net>
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\View\View
     */
    public function marketCreateForm(Request $request)
    {
        $data['area'] = Zone::where('PARENT_ID', 0)
                                        ->where('IS_ACTIVE', 1)
                                        ->lists('WARD_NAME_BN', 'WARD_ID');
        $data['type_category'] = ProjectCategory::where('IS_ACTIVE', 1)
                                                    ->where('IS_LEASE', 0)
                                                    ->get();
        $data['project_floor'] = ProjectFloor::where('IS_ACTIVE', 1)
                                                ->lists('FLOOR_NUMBER_BN', 'FLOOR_ID');
        $data['project_type'] = ProjectType::where('IS_ACTIVE', 1)
                                                ->lists('TYPE_NAME', 'PR_TYPE');

        return View::make('admin_access.market.create', $data)->render();
    }

    /**
     * Create project.
     *
     *@author      Nurullah <nurul@atilimited.net>
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return json
     */
    public function createMarket(Request $request)
    {
        $data = $request->all();
        $user = Auth::user()->USER_ID;
        $pro_number = $data['number'];

        // Check project no before Insert
        $project_exist = Project::where('PR_UD_ID', $pro_number)->first();
        if (!empty($project_exist)) {
            return response()->json(['flug' => 0, 'message' => trans('project.project_exists')]);
            exit;
        } else {
            $project = Project::createMarketData($data, $user, $type = 0);
        }

        return response()->json(['flug' => 1, 'message' => trans('common.save_success')]);
    }

    /**
     * Change project status.
     *
     * @author      Nurullah <nurul@atilimited.net>
     *
     * @param \Illuminate\Http\Request $request
     * @param string                   $id
     *
     * @return string
     */
    public function marketStatus(Request $request, $id)
    {
        $project = Project::find($id);

        if ($project->IS_ACTIVE == 1) {
            $project->IS_ACTIVE = 0;
        } else {
            $project->IS_ACTIVE = 1;
        }
        $project->save();

        return trans('common.status_change');
    }

    /**
     * Render project edit form.
     *
     *@author      Nurullah <nurul@atilimited.net>
     *
     * @param \Illuminate\Http\Request $request
     * @param string                   $id
     *
     * @return \Illuminate\View\View
     */
    public function marketEditForm(Request $request, $id)
    {
        $data['area'] = Zone::where('PARENT_ID', 0)
                                        ->where('IS_ACTIVE', 1)
                                        ->lists('WARD_NAME_BN', 'WARD_ID');
        $data['project_land'] = [];
        $data['project'] = Project::with('getDocuments')->where('PROJECT_ID', $id)->first();
        $projectland = ProjectLand::where('PROJECT_ID', $id)->get();
        foreach ($projectland as $key => $pl) {
            $data['project_land'][] = $pl->LAND_ID;
        }
        $data['land'] = Land::getProjectEditLands($id);
        $data['type_category'] = ProjectCategory::where('IS_ACTIVE', 1)->where('IS_LEASE', 0)->get();
        $data['project_type'] = ProjectType::lists('TYPE_NAME', 'PR_TYPE');
        $data['project_floor'] = ProjectFloor::where('IS_ACTIVE', 1)->lists('FLOOR_NUMBER_BN', 'FLOOR_ID');

        return View::make('admin_access.market.edit', $data)->render();
    }

    /**
     * Update project.
     *
     *@author      Nurullah <nurul@atilimited.net>
     *
     * @param \Illuminate\Http\Request $request
     * @param string                   $request
     *
     * @return json
     */
    public function updateMarket(Request $request, $id)
    {
        $data = $request->all();
        $user = Auth::user()->USER_ID;
        $pro_number = $data['number'];

        // Check project no before update
        $project_exist = Project::whereNotIn('PROJECT_ID', [$id])->where('PR_UD_ID', $pro_number)->first();
        if (!empty($project_exist)) {
            return response()->json(['flug' => 0, 'message' => trans('project.project_exists')]);
            exit;
        } else {
            $project = Project::updateMarketData($data, $user, $id);
        }

        return response()->json(['flug' => 1, 'message' => trans('common.update_success')]);
    }

    /**
     * Render project view page.
     *
     *@author      Nurullah <nurul@atilimited.net>
     *
     * @param \Illuminate\Http\Request $request
     * @param string                   $id
     *
     * @return \Illuminate\View\View
     */
    public function marketView(Request $request, $id)
    {
        $data['project'] = Project::with('getDocuments')->where('PROJECT_ID', $id)->first();
        $data['category'] = ProjectDetails::getCategoryDetails($id);
        $data['projectLand'] = ProjectLand::where('PROJECT_ID', $id)
                                            ->with(['getLand'])
                                            ->get();

        return view('admin_access.market.view', $data)->render();
    }

    /**
     * Print project view page.
     *
     *@author      Nurullah <nurul@atilimited.net>
     *
     * @param \Illuminate\Http\Request $request
     * @param string                   $id
     *
     * @return \Illuminate\View\View
     */
    public function marketDetailPrint(Request $request, $id)
    {
        $data['project'] = Project::with('getDocuments')->where('PROJECT_ID', $id)->first();
        $data['category'] = ProjectDetails::getCategoryDetails($id);
        $data['projectLand'] = ProjectLand::where('PROJECT_ID', $id)
                                            ->with(['getLand'])
                                            ->get();

        $pdf_data = View::make('admin_access.market.print', $data)->render();
        $file_name = 'NCC-project-'.$data['project']->PR_NAME.'.pdf';

        return Helpers::makePDF($file_name, $pdf_data);
    }

    /**
     * Delete project data.
     *
     * @author      Nurullah <nurul@atilimited.net>
     *
     * @param \Illuminate\Http\Request $request
     * @param string                   $id
     *
     * @return string
     */
    public function marketDelete(Request $request, $id)
    {
        $project = Project::find($id);

        // Delete project file
        if (!empty($project->PROJECT_SKETCH)) {
            if (file_exists(public_path().'/upload/project/'.$project->PROJECT_SKETCH)) {
                //chmod(public_path().'/upload/project/'.$project->PROJECT_SKETCH, 0755);
                unlink(public_path().'/upload/project/'.$project->PROJECT_SKETCH);
            }
        }
        $project->delete();

        return trans('common.delete_data');
    }

    /**
     * Show project page.
     *
     *@author      Nurullah <nurul@atilimited.net>
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\View\View
     */
    public function marketDetailsIndex(Request $request)
    {
        $link_url = 'all_market_details'; // module url
        $data['access'] = Module::checkPrevilege($link_url); // get access info by module url
        return view('admin_access.market.market_details_list', $data);
    }

    /**
     * Get project data.
     *
     *@author      Nurullah <nurul@atilimited.net>
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return json
     */
    public function marketDetailsData(Request $request)
    {
        $input = $request->all();
        $collection = Project::getMarketDetailsData($input);
        $output = Datatablehelper::marketDetailsOutput($input, $collection);

        return response()->json($output);
    }

    /**
     * Render project create form.
     *
     *@author      Nurullah <nurul@atilimited.net>
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\View\View
     */
    public function marketDetailsCreateForm(Request $request)
    {
        $data['project'] = Project::where('IS_ACTIVE', 1)
                                        ->where('IS_LEASE', 0)
                                        ->lists('PR_NAME_BN', 'PROJECT_ID');
        $data['category'] = ProjectCategory::where('IS_ACTIVE', 1)
                                                    ->where('IS_LEASE', 0)
                                                    ->lists('CATE_NAME', 'PR_CATEGORY');
        $data['type_category'] = ProjectCategory::where('IS_ACTIVE', 1)
                                                    ->where('IS_LEASE', 0)
                                                    ->get();
        $data['project_floor'] = ProjectFloor::where('IS_ACTIVE', 1)
                                                ->lists('FLOOR_NUMBER_BN', 'FLOOR_ID');
        $data['project_type'] = ProjectType::where('IS_ACTIVE', 1)
                                                ->lists('TYPE_NAME', 'PR_TYPE');

        return View::make('admin_access.market.market_details_create', $data)->render();
    }

    public function getMarketNumber(Request $request)
    {
        $project_id = $request->get('project_id');
        $project_type = $request->get('project_type');
        $place = $request->get('place');
        $number = $request->get('number');
        $projectDetails = ProjectDetails::where('PROJECT_ID', $project_id)
                                                            ->where('PR_CATEGORY', $project_type)
                                                            ->where('POSITION', $place)
                                                            ->where('PR_SSF_NO', $number)
                                                            ->first();

        if (!empty($projectDetails)) {
            $data = 1;
        } else {
            $data = 0;
        }

        return $data;
    }

    /**
     * Change project status.
     *
     * @author      Nurullah <nurul@atilimited.net>
     *
     * @param \Illuminate\Http\Request $request
     * @param string                   $id
     *
     * @return string
     */
    public function marketDetailStatus(Request $request, $id)
    {
        $projectDetails = ProjectDetails::find($id);

        if ($projectDetails->IS_ACTIVE == 1) {
            $projectDetails->IS_ACTIVE = 0;
        } else {
            $projectDetails->IS_ACTIVE = 1;
        }
        $projectDetails->save();

        return trans('common.status_change');
    }

    /**
     * Render project edit form.
     *
     *@author      Nurullah <nurul@atilimited.net>
     *
     * @param \Illuminate\Http\Request $request
     * @param string                   $id
     *
     * @return \Illuminate\View\View
     */
    public function marketDetailsEditForm(Request $request, $id)
    {
        $data['project'] = Project::where('IS_ACTIVE', 1)
                                        ->where('IS_LEASE', 0)
                                        ->get();
        $data['category'] = ProjectCategory::where('IS_ACTIVE', 1)
                                                    ->where('IS_LEASE', 0)
                                                    ->get();
        $data['type_category'] = ProjectCategory::where('IS_ACTIVE', 1)
                                                    ->where('IS_LEASE', 0)
                                                    ->get();
        $data['project_floor'] = ProjectFloor::where('IS_ACTIVE', 1)
                                                ->get();
        $data['project_type'] = ProjectType::where('IS_ACTIVE', 1)
                                                ->get();
        $data['project_details'] = ProjectDetails::where('PR_DETAIL_ID', $id)->first();

        return View::make('admin_access.market.market_details_edit', $data)->render();
    }

    public function marketDetailsUpdate(Request $request, $id)
    {
        $data = $request->all();
        $user = Auth::user()->USER_ID;

        $projectDetails = Project::updateMarketDetailsData($data, $user, $id);

        return response()->json(['flug' => 1, 'message' => trans('common.update_success')]);
    }

    /**
     * Create project.
     *
     *@author      Nurullah <nurul@atilimited.net>
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return json
     */
    public function createMarketDetails(Request $request)
    {
        $data = $request->all();
        $user = Auth::user()->USER_ID;
        $project = Project::createMarketDetailsData($data, $user, $type = 0);

        return response()->json(['flug' => 1, 'message' => trans('common.save_success')]);
    }

    //////////////--------------------------------------------------------------*******************

    /**
     * Show project page.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\View\View
     */
    public function projectIndex(Request $request)
    {
        return view('admin_access.project.index');
    }

    /**
     * Get project data.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return json
     */
    public function projectData(Request $request)
    {
        $input = $request->all();
        $collection = Project::getData($input);
        $output = Datatablehelper::projectOutput($input, $collection);

        return response()->json($output);
    }

    /**
     * Render project create form.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\View\View
     */
    public function projectCreateForm(Request $request)
    {
        $data['area'] = Zone::where('PARENT_ID', 0)
                                        ->where('IS_ACTIVE', 1)
                                        ->lists('WARD_NAME_BN', 'WARD_ID');
        $data['type_category'] = ProjectCategory::where('IS_ACTIVE', 1)
                                                    ->where('IS_LEASE', 0)
                                                    ->get();
        $data['project_floor'] = ProjectFloor::where('IS_ACTIVE', 1)
                                                ->lists('FLOOR_NUMBER_BN', 'FLOOR_ID');
        $data['project_type'] = ProjectType::where('IS_ACTIVE', 1)
                                                ->lists('TYPE_NAME', 'PR_TYPE');

        return View::make('admin_access.project.create', $data)->render();
    }

    /**
     * Create project.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return json
     */
    public function createProject(Request $request)
    {
        $data = $request->all(); //echo '<pre>';print_r($data);exit;
        $user = Auth::user()->USER_ID;
        $pro_number = $data['number'];

        // Check project no before update
        $project_exist = Project::where('PR_UD_ID', $pro_number)->first();
        if (!empty($project_exist)) {
            return response()->json(['flug' => 0, 'message' => trans('project.project_exists')]);
            exit;
        } else {
            $project = Project::createData($data, $user, $type = 0);
        }

        return response()->json(['flug' => 1, 'message' => trans('common.save_success')]);
    }

    /**
     * Change project status.
     *
     * @param \Illuminate\Http\Request $request
     * @param string                   $id
     *
     * @return string
     */
    public function projectStatus(Request $request, $id)
    {
        $project = Project::find($id);

        if ($project->IS_ACTIVE == 1) {
            $project->IS_ACTIVE = 0;
        } else {
            $project->IS_ACTIVE = 1;
        }
        $project->save();

        return trans('common.status_change');
    }

    /**
     * Render project edit form.
     *
     * @param \Illuminate\Http\Request $request
     * @param string                   $id
     *
     * @return \Illuminate\View\View
     */
    public function projectEditForm(Request $request, $id)
    {
        $data['area'] = Zone::where('PARENT_ID', 0)
                                        ->where('IS_ACTIVE', 1)
                                        ->lists('WARD_NAME_BN', 'WARD_ID');
        $data['project_land'] = [];
        $data['project'] = Project::with('getDocuments')->where('PROJECT_ID', $id)->first();
        $projectland = ProjectLand::where('PROJECT_ID', $id)->get();
        foreach ($projectland as $key => $pl) {
            $data['project_land'][] = $pl->LAND_ID;
        }
        $data['land'] = Land::getProjectEditLands($id);
        $data['type_category'] = ProjectCategory::where('IS_ACTIVE', 1)->where('IS_LEASE', 0)->get();
        $data['project_type'] = ProjectType::lists('TYPE_NAME', 'PR_TYPE');
        $data['project_floor'] = ProjectFloor::where('IS_ACTIVE', 1)->lists('FLOOR_NUMBER_BN', 'FLOOR_ID');

        return View::make('admin_access.project.edit', $data)->render();
    }

    /**
     * Delete Project attachemnt.
     *
     * @param \Illuminate\Http\Request $request
     * @param string                   $id
     *
     * @return string
     */
    public function projectFileDelete(Request $request, $id)
    {
        $attachment = ProjectDoc::find($id);

        if (file_exists(public_path().'/upload/project/'.$attachment->ATTACHMENT)) {
            unlink(public_path().'/upload/project/'.$attachment->ATTACHMENT);
        }
        $attachment->delete();

        return trans('common.delete_data');
    }

    /**
     * Lease edit form.
     *
     * @param \Illuminate\Http\Request $request
     * @param string                   $id
     *
     * @return \Illuminate\View\View
     */
    public function leaseEditForm(Request $request, $id)
    {
        $data['project_land'] = [];
        $data['area'] = Zone::where('PARENT_ID', 0)
                                        ->where('IS_ACTIVE', 1)
                                        ->lists('WARD_NAME_BN', 'WARD_ID');
        $data['project'] = Project::where('PROJECT_ID', $id)->first();
        $projectland = ProjectLand::where('PROJECT_ID', $id)->get();
        foreach ($projectland as $key => $pl) {
            $data['project_land'][] = $pl->LAND_ID;
        }
        $data['land'] = Land::getLeaseEditLands($id);

        return View::make('admin_access.lease.edit', $data)->render();
    }

    /**
     * Update project.
     *
     * @param \Illuminate\Http\Request $request
     * @param string                   $request
     *
     * @return json
     */
    public function updateProject(Request $request, $id)
    {
        $data = $request->all();
        $user = Auth::user()->USER_ID;
        $pro_number = $data['number'];

        // Check project no before update
        $project_exist = Project::whereNotIn('PROJECT_ID', [$id])->where('PR_UD_ID', $pro_number)->first();
        if (!empty($project_exist)) {
            return response()->json(['flug' => 0, 'message' => trans('project.project_exists')]);
            exit;
        } else {
            $project = Project::updateData($data, $user, $id);
        }

        return response()->json(['flug' => 1, 'message' => trans('common.update_success')]);
    }

    /**
     * Update lease List.
     *
     * @param \Illuminate\Http\Request $request
     * @param string                   $id
     *
     * @return json
     */
    public function updateLease(Request $request, $id)
    {
        $data = $request->all();
        $user = Auth::user()->USER_ID;

        // Check Mohal no before update
        $check = Project::whereNotIn('PROJECT_ID', [$id])
                        ->where('PR_UD_ID', $data['PR_UD_ID'])->first();

        if (!empty($check)) {
            return response()->json(['flug' => 0, 'message' => trans('lease.mahal_exists')]);
            exit;
        } else {
            $project = Project::updateDataMahal($data, $user, $id);
        }

        return response()->json(['flug' => 1, 'message' => trans('common.update_success')]);
    }

    /**
     * Render project view page.
     *
     * @param \Illuminate\Http\Request $request
     * @param string                   $id
     *
     * @return \Illuminate\View\View
     */
    public function projectView(Request $request, $id)
    {
        $data['project'] = Project::with('getDocuments')->where('PROJECT_ID', $id)->first();
        $data['category'] = ProjectDetails::getCategoryDetails($id);
        $data['projectLand'] = ProjectLand::where('PROJECT_ID', $id)
                                            ->with(['getLand'])
                                            ->get();

        return view('admin_access.project.view', $data)->render();
    }

    /**
     * Render lease view page.
     *
     * @param \Illuminate\Http\Request $request
     * @param string                   $id
     *
     * @return \Illuminate\View\View
     */
    public function leaseView(Request $request, $id)
    {
        $data['project'] = Project::where('PROJECT_ID', $id)->first();
        $data['category'] = ProjectDetails::getCategoryDetails($id);
        $data['projectLand'] = ProjectLand::where('PROJECT_ID', $id)
                                            ->with(['getLand'])
                                            ->get();

        return view('admin_access.lease.view', $data)->render();
    }

    /**
     * Delete project data.
     *
     * @param \Illuminate\Http\Request $request
     * @param string                   $id
     *
     * @return string
     */
    public function projectDelete(Request $request, $id)
    {
        $project = Project::find($id);

        // Delete project file
        if (!empty($project->PROJECT_SKETCH)) {
            if (file_exists(public_path().'/upload/project/'.$project->PROJECT_SKETCH)) {
                //chmod(public_path().'/upload/project/'.$project->PROJECT_SKETCH, 0755);
                unlink(public_path().'/upload/project/'.$project->PROJECT_SKETCH);
            }
        }
        $project->delete();

        return trans('common.delete_data');
    }

    /**
     * Delete project details data.
     *
     * @param \Illuminate\Http\Request $request
     * @param string                   $id
     *
     * @return string
     */
    public function projectDetailsDelete(Request $request, $id)
    {
        ProjectDetails::find($id)->delete();

        return trans('common.delete_data');
    }

    /**
     * Show project general page.
     *
     * @return \Illuminate\View\View
     */
    public function generalProjects()
    {
        return view('admin_access.project.general_list');
    }

    /**
     * Render project general create form.
     *
     * @param $request
     *
     * @return \Illuminate\View\View
     */
    public function projectGeneralForm(Request $request)
    {
        $land = Land::lists('LAND_NUMBER', 'LAND_ID');

        return View::make('admin_access.project.general_create', compact('land'))->render();
    }

    /**
     * Show project type list page.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\View\View
     */
    public function projectTypeList(Request $request)
    {
        return view('admin_access.flat_shop.index');
    }

    /**
     * Render project type create form.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\View\View
     */
    public function projectTypeDataCreateForm(Request $request)
    {
        $data['project'] = Project::lists('PR_NAME_BN', 'PROJECT_ID');
        $data['project_cat'] = ProjectCategory::lists('CATE_NAME', 'PR_CATEGORY');
        $data['project_type'] = ProjectType::lists('TYPE_NAME', 'PR_TYPE');

        return View::make('admin_access.flat_shop.create')->with($data)->render();
    }

    /**
     * Project category.
     *
     * @return \Illuminate\View\View
     */
    public function projectCategory()
    {
        $link_url = 'project_category'; // module url
        $data['access'] = Module::checkPrevilege($link_url); // get access info by module url
        return view('admin_access.project_category.index', $data);
    }

    /**
     * Project category create form.
     *
     * @return \Illuminate\View\View
     */
    public function projectCategoryCreateForm()
    {
        return View::make('admin_access.project_category.create')->render();
    }

    /**
     * Project category save.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return json
     */
    public function projectCategorySave(Request $request)
    {
        $data = $request->all();
        $CATE_NAME = ProjectCategory::where('CATE_NAME', $data['CATE_NAME'])->first();
        if (!empty($CATE_NAME)) {
            return response()->json(['flug' => 0, 'message' => trans('project.project_category_exists')]);
            exit;
        }
        $category = new ProjectCategory();
        $category->CATE_NAME = $data['CATE_NAME'];
        $category->CATE_DESC = $data['CATE_DESC'];
        $category->IS_ACTIVE = isset($data['IS_ACTIVE']) ? 1 : 0;
        $category->IS_LEASE = isset($data['IS_LEASE']) ? 1 : 0;
        $category->CREATED_BY = Auth::user()->USER_ID;
        $category->save();

        return response()->json(['flug' => 1, 'message' => trans('common.save_success')]);
    }

    /**
     * Project category data.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return json
     */
    public function projectCategoryData(Request $request)
    {
        $input = $request->all();
        $collection = ProjectCategory::getDataProjectCategory($input);
        $output = Datatablehelper::projectcatdatatableOutput($input, $collection);

        return response()->json($output);
    }

    /**
     * Project category edit form.
     *
     * @param \Illuminate\Http\Request $request
     * @param string                   $id
     *
     * @return \Illuminate\View\View
     */
    public function projectcategoryEditForm(Request $request, $id)
    {
        $category = ProjectCategory::find($id);

        return View::make('admin_access.project_category.edit', compact('category'))->render();
    }

    /**
     * project category update.
     *
     * @param \Illuminate\Http\Request $request
     * @param string                   $id
     *
     * @return json
     */
    public function projectCategoryUpdate(Request $request, $id)
    {
        $flug = 0;
        $msg = '';
        $data = $request->all();

        $current = ProjectCategory::whereNotIn('PR_CATEGORY', [$id])->where('CATE_NAME', $data['CATE_NAME'])->first();
        if (!empty($current)) {
            $flug = 0;
            $msg = trans('project.project_category_exists');
        } else {
            $flug = 1;
        }

        if ($flug == 1) {
            $category = ProjectCategory::find($id);
            $category->CATE_NAME = $data['CATE_NAME'];
            $category->CATE_DESC = $data['CATE_DESC'];
            $category->IS_ACTIVE = isset($data['IS_ACTIVE']) ? 1 : 0;
            $category->IS_LEASE = isset($data['IS_LEASE']) ? 1 : 0;
            $category->UPDATED_BY = Auth::user()->USER_ID;
            $category->save();
            $msg = trans('common.update_success');
        }

        return response()->json(['flug' => $flug, 'message' => $msg]);
    }

    /**
     * Project category status change.
     *
     * @param \Illuminate\Http\Request $request
     * @param string                   $id
     *
     * @return string
     */
    public function projectCategoryStatus(Request $request, $id)
    {
        $category = ProjectCategory::find($id);

        if ($category->IS_ACTIVE == 1) {
            $category->IS_ACTIVE = 0;
        } else {
            $category->IS_ACTIVE = 1;
        }

        $category->UPDATED_BY = Auth::user()->USER_ID;
        $category->save();

        return trans('common.status_change');
    }

    /**
     * project category delete.
     *
     * @param \Illuminate\Http\Request $request
     * @param string                   $id
     *
     * @return string
     */
    public function projectCategoryDelete(Request $request, $id)
    {
        ProjectCategory::where('PR_CATEGORY', $id)->delete();

        return trans('common.delete_data');
    }

    /**
     * Project type.
     *
     * @return \Illuminate\View\View
     */
    public function projectType()
    {
        $link_url = 'project_type'; // module url
        $data['access'] = Module::checkPrevilege($link_url); // get access info by module url
        return view('admin_access.project_type.index', $data);
    }

    /**
     * Project create from.
     *
     * @return \Illuminate\View\View
     */
    public function projectTypeCreateForm()
    {
        return View::make('admin_access.project_type.create')->render();
    }

    /**
     * Project type save.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return json
     */
    public function projectTypeSave(Request $request)
    {
        $data = $request->all();
        $TYPE_NAME = ProjectType::where('TYPE_NAME', $data['TYPE_NAME'])->first();
        if (!empty($TYPE_NAME)) {
            return response()->json(['flug' => 0, 'message' => trans('project.project_type_exists')]);
            exit;
        }
        $type = new ProjectType();
        $type->TYPE_NAME = $request->get('TYPE_NAME');
        $type->TYPE_DESC = $request->get('TYPE_DESC');
        $type->IS_ACTIVE = $request->get('IS_ACTIVE');
        $type->CREATED_BY = Auth::user()->USER_ID;
        $type->save();

        return response()->json(['flug' => 1, 'message' => trans('common.save_success')]);
    }

    /**
     * Project type data.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return json
     */
    public function projectTypeData(Request $request)
    {
        $input = $request->all();
        $collection = ProjectType::getDataProjectType($input);
        $output = Datatablehelper::projecttypedatatableOutput($input, $collection);

        return response()->json($output);
    }

    /**
     * Project type edit form.
     *
     * @param \Illuminate\Http\Request $request
     * @param string                   $id
     *
     * @return \Illuminate\View\View
     */
    public function projecttypeEditForm(Request $request, $id)
    {
        $type = ProjectType::find($id);

        return View::make('admin_access.project_type.edit', compact('type'))->render();
    }

    /**
     * project type update.
     *
     * @param \Illuminate\Http\Request $request
     * @param string                   $id
     *
     * @return json
     */
    public function projectTypeUpdate(Request $request, $id)
    {
        $flug = 0;
        $msg = '';
        $data = $request->all();

        $current = ProjectType::whereNotIn('PR_TYPE', [$id])->where('TYPE_NAME', $data['TYPE_NAME'])->first();
        if (!empty($current)) {
            $flug = 0;
            $msg = trans('project.project_type_exists');
        } else {
            $flug = 1;
        }

        if ($flug == 1) {
            $type = ProjectType::find($id);
            $type->TYPE_NAME = $data['TYPE_NAME'];
            $type->TYPE_DESC = $data['TYPE_DESC'];
            $type->IS_ACTIVE = $data['IS_ACTIVE'];
            $type->UPDATED_BY = Auth::user()->USER_ID;
            $type->save();
            $msg = trans('common.update_success');
        }

        return response()->json(['flug' => $flug, 'message' => $msg]);
    }

    /**
     * Project type status change.
     *
     * @param \Illuminate\Http\Request $request
     * @param string                   $id
     *
     * @return string
     */
    public function projectTypeStatus(Request $request, $id)
    {
        $type = ProjectType::find($id);

        if ($type->IS_ACTIVE == 1) {
            $type->IS_ACTIVE = 0;
        } else {
            $type->IS_ACTIVE = 1;
        }

        $type->UPDATED_BY = Auth::user()->USER_ID;
        $type->save();

        return trans('common.status_change');
    }

    /**
     * project type delete.
     *
     * @param \Illuminate\Http\Request $request
     * @param string                   $id
     *
     * @return string
     */
    public function projectTypeDelete(Request $request, $id)
    {
        ProjectType::where('PR_TYPE', $id)->delete();

        return trans('common.delete_data');
    }

    /**
     * Locations.
     *
     * @return \Illuminate\View\View
     */
    public function projectLocation()
    {
        $link_url = 'project_location'; // module url
        $data['access'] = Module::checkPrevilege($link_url); // get access info by module url
        return view('admin_access.project_location.index', $data);
    }

    /**
     * Location create form.
     *
     * @return \Illuminate\View\View
     */
    public function projectLocationCreateForm()
    {
        return View::make('admin_access.project_location.create')->render();
    }

    /**
     * Location save.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return json
     */
    public function projectLocationSave(Request $request)
    {
        $data = $request->all();
        $LOCATION_NAME = ProjectLocation::where('LOCATION_NAME', $data['LOCATION_NAME'])->first();
        if (!empty($LOCATION_NAME)) {
            return response()->json(['flug' => 0, 'message' => trans('project.project_location_exist')]);
            exit;
        }
        $location = new ProjectLocation();
        $location->LOCATION_NAME = $request->get('LOCATION_NAME');
        $location->LOCATION_DESC = $request->get('LOCATION_DESC');
        $location->IS_ACTIVE = $request->get('IS_ACTIVE');
        $location->CREATED_BY = Auth::user()->USER_ID;
        $location->save();

        return response()->json(['flug' => 1, 'message' => trans('common.save_success')]);
    }

    /**
     * Locations data.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return json
     */
    public function projectLocationData(Request $request)
    {
        $input = $request->all();
        $collection = ProjectLocation::getDataProjectLocation($input);
        $output = Datatablehelper::projectlocationdatatableOutput($input, $collection);

        return response()->json($output);
    }

    /**
     * Location edit form.
     *
     * @param \Illuminate\Http\Request $request
     * @param string                   $id
     *
     * @return \Illuminate\View\View
     */
    public function projectlocationEditForm(Request $request, $id)
    {
        $location = ProjectLocation::find($id);

        return View::make('admin_access.project_location.edit', compact('location'))->render();
    }

    /**
     * Location update.
     *
     * @param \Illuminate\Http\Request $request
     * @param string                   $id
     *
     * @return json
     */
    public function projectLocationUpdate(Request $request, $id)
    {
        $flug = 0;
        $msg = '';
        $data = $request->all();

        $current = ProjectLocation::whereNotIn('LOCATION_ID', [$id])
                                    ->where('LOCATION_NAME', $data['LOCATION_NAME'])
                                    ->first();
        if (!empty($current)) {
            $flug = 0;
            $msg = trans('project.project_type_exists');
        } else {
            $flug = 1;
        }

        if ($flug == 1) {
            $location = ProjectLocation::find($id);
            $location->LOCATION_NAME = $data['LOCATION_NAME'];
            $location->LOCATION_DESC = $data['LOCATION_DESC'];
            $location->IS_ACTIVE = $data['IS_ACTIVE'];
            $location->UPDATED_BY = Auth::user()->USER_ID;
            $location->save();
            $msg = trans('common.update_success');
        }

        return response()->json(['flug' => $flug, 'message' => $msg]);
    }

    /**
     * Location status change.
     *
     * @param \Illuminate\Http\Request $request
     * @param string                   $id
     *
     * @return string
     */
    public function projectLocationStatus(Request $request, $id)
    {
        $location = ProjectLocation::find($id);

        if ($location->IS_ACTIVE == 1) {
            $location->IS_ACTIVE = 0;
        } else {
            $location->IS_ACTIVE = 1;
        }

        $location->UPDATED_BY = Auth::user()->USER_ID;
        $location->save();

        return trans('common.status_change');
    }

    /**
     * Location delete.
     *
     * @param \Illuminate\Http\Request $request
     * @param string                   $id
     *
     * @return string
     */
    public function projectLocationDelete(Request $request, $id)
    {
        ProjectLocation::where('LOCATION_ID', $id)->delete();

        return trans('common.delete_data');
    }

    /**
     * Category condition.
     *
     * @return \Illuminate\View\View
     */
    public function projectCatCondition()
    {
        $link_url = 'project_cat_con'; // module url
        $data['access'] = Module::checkPrevilege($link_url); // get access info by module url
        return view('admin_access.project_cat_condition.index', $data);
    }

    /**
     * Category condition create form.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\View\View
     */
    public function projectCatConditionCreateForm(Request $request)
    {
        $category = ProjectCategory::where('IS_ACTIVE', 1)->lists('CATE_NAME', 'PR_CATEGORY');

        return View::make('admin_access.project_cat_condition.create', compact('category'))->render();
    }

    /**
     * Category condition save.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return json
     */
    public function projectCatConditionSave(Request $request)
    {
        $data = $request->all();
        $check = ProjectCatCondition::where('PR_CATEGORY', $data['category'])->first();
        if (!empty($check)) {
            return response()->json(['flug' => 0, 'message' => trans('project.project_cat_con_exists')]);
            exit;
        }
        $condition = new ProjectCatCondition();
        $condition->PR_CATEGORY = $request->get('category');
        $condition->CON_TITLE = $request->get('CON_TITLE');
        $condition->CON_DESC = $request->get('CON_DESC');
        $condition->IS_ACTIVE = $request->get('IS_ACTIVE');
        $condition->CREATED_BY = Auth::user()->USER_ID;
        $condition->save();

        return response()->json(['flug' => 1, 'message' => trans('common.save_success')]);
    }

    /**
     * Category condition data.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return json
     */
    public function projectCatConditionData(Request $request)
    {
        $input = $request->all();
        $collection = ProjectCatCondition::getDataProjectCatCondition($input);
        $output = Datatablehelper::projectcatconditiondatatableOutput($input, $collection);

        return response()->json($output);
    }

    /**
     * Category condition edit form.
     *
     * @param \Illuminate\Http\Request $request
     * @param string                   $id
     *
     * @return json
     */
    public function projectcatconditionEditForm(Request $request, $id)
    {
        $condition = ProjectCatCondition::find($id);
        $category = ProjectCategory::where('IS_ACTIVE', 1)->select('CATE_NAME', 'PR_CATEGORY')->get();

        return View::make('admin_access.project_cat_condition.edit', compact('condition', 'category'))->render();
    }

    /**
     * Category condition update.
     *
     * @param \Illuminate\Http\Request $request
     * @param string                   $id
     *
     * @return json
     */
    public function projectCatConditionUpdate(Request $request, $id)
    {
        $flug = 0;
        $msg = '';
        $data = $request->all();
        $current = ProjectCatCondition::whereNotIn('CATE_CON_ID', [$id])->where('PR_CATEGORY', $data['PR_CATEGORY'])->first();
        if (!empty($current)) {
            $flug = 0;
            $msg = trans('project.project_cat_con_exists');
        } else {
            $flug = 1;
        }

        if ($flug == 1) {
            $condition = ProjectCatCondition::find($id);
            $condition->PR_CATEGORY = $data['PR_CATEGORY'];
            $condition->CON_TITLE = $data['CON_TITLE'];
            $condition->CON_DESC = $data['CON_DESC'];
            $condition->IS_ACTIVE = $data['IS_ACTIVE'];
            $condition->UPDATED_BY = Auth::user()->USER_ID;
            $condition->save();
            $msg = trans('common.update_success');
        }

        return response()->json(['flug' => $flug, 'message' => $msg]);
    }

    /**
     * Category condition status change.
     *
     * @param \Illuminate\Http\Request $request
     * @param string                   $id
     *
     * @return string
     */
    public function projectCatConditionStatus(Request $request, $id)
    {
        $condition = ProjectCatCondition::find($id);

        if ($condition->IS_ACTIVE == 1) {
            $condition->IS_ACTIVE = 0;
        } else {
            $condition->IS_ACTIVE = 1;
        }

        $condition->UPDATED_BY = Auth::user()->USER_ID;
        $condition->save();

        return trans('common.status_change');
    }

    /**
     * Category condition delete.
     *
     * @param \Illuminate\Http\Request $request
     * @param string                   $id
     *
     * @return delete
     */
    public function projectCatConditionDelete(Request $request, $id)
    {
        ProjectCatCondition::where('CATE_CON_ID', $id)->delete();

        return trans('common.delete_data');
    }

    /**
     * Project floor.
     *
     * @return \Illuminate\View\View
     */
    public function floor()
    {
        $link_url = 'floor'; // module url
        $data['access'] = Module::checkPrevilege($link_url); // get access info by module url
        return view('admin_access.project_floor.index', $data);
    }

    /**
     * Project floor create form.
     *
     * @return \Illuminate\View\View
     */
    public function projectFloorCreateForm()
    {
        return View::make('admin_access.project_floor.create')->render();
    }

    /**
     * Project floor save.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return json
     */
    public function projectFloorSave(Request $request)
    {
        $data = $request->all();
        $check = ProjectFloor::where('FLOOR_NUMBER', $data['FLOOR_NUMBER'])
                                ->where('FLOOR_NUMBER_BN', $data['FLOOR_NUMBER_BN'])
                                ->first();
        if (!empty($check)) {
            return response()->json(['flug' => 0, 'message' => trans('project.project_floor_exists')]);
            exit;
        }
        $type = new ProjectFloor();
        $type->FLOOR_NUMBER = $request->get('FLOOR_NUMBER');
        $type->FLOOR_NUMBER_BN = $request->get('FLOOR_NUMBER_BN');
        $type->DETAILS = $request->get('DETAILS');
        $type->IS_ACTIVE = isset($data['IS_ACTIVE']) ? $data['IS_ACTIVE'] : 0;
        $type->CREATED_BY = Auth::user()->USER_ID;
        $type->save();

        return response()->json(['flug' => 1, 'message' => trans('common.save_success')]);
    }

    /**
     * Project floor data.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return json
     */
    public function projectFloorData(Request $request)
    {
        $input = $request->all();
        $collection = ProjectFloor::getDataProjectFloor($input);
        $output = Datatablehelper::projectfloordatatableOutput($input, $collection);

        return response()->json($output);
    }

    /**
     * Project floor edit form.
     *
     * @param \Illuminate\Http\Request $request
     * @param string                   $id
     *
     * @return \Illuminate\View\View
     */
    public function projectfloorEditForm(Request $request, $id)
    {
        $floor = ProjectFloor::find($id);

        return View::make('admin_access.project_floor.edit', compact('floor'))->render();
    }

    /**
     * Project floor update.
     *
     * @param \Illuminate\Http\Request $request
     * @param string                   $id
     *
     * @return json
     */
    public function projectFloorUpdate(Request $request, $id)
    {
        $flug = 0;
        $msg = '';
        $data = $request->all();
        $current = ProjectFloor::whereNotIn('FLOOR_ID', [$id])
                                ->where('FLOOR_NUMBER', $data['FLOOR_NUMBER'])
                                ->where('FLOOR_NUMBER_BN', $data['FLOOR_NUMBER_BN'])
                                ->first();
        if (!empty($current)) {
            $flug = 0;
            $msg = trans('project.project_floor_exists');
        } else {
            $flug = 1;
        }

        if ($flug == 1) {
            $type = ProjectFloor::find($id);
            $type->FLOOR_NUMBER = $data['FLOOR_NUMBER'];
            $type->FLOOR_NUMBER_BN = $data['FLOOR_NUMBER_BN'];
            $type->DETAILS = $data['DETAILS'];
            $type->IS_ACTIVE = isset($data['IS_ACTIVE']) ? $data['IS_ACTIVE'] : 0;
            $type->UPDATED_BY = Auth::user()->USER_ID;
            $type->save();
            $msg = trans('common.update_success');
        }

        return response()->json(['flug' => $flug, 'message' => $msg]);
    }

    /**
     * Project floor status change.
     *
     * @param \Illuminate\Http\Request $request
     * @param string                   $id
     *
     * @return string
     */
    public function projectFloorStatus(Request $request, $id)
    {
        $type = ProjectFloor::find($id);

        if ($type->IS_ACTIVE == 1) {
            $type->IS_ACTIVE = 0;
        } else {
            $type->IS_ACTIVE = 1;
        }

        $type->UPDATED_BY = Auth::user()->USER_ID;
        $type->save();

        return trans('common.status_change');
    }

    /**
     * Project floor delete.
     *
     * @param \Illuminate\Http\Request $request
     * @param string                   $id
     *
     * @return string
     */
    public function projectFloorDelete(Request $request, $id)
    {
        ProjectFloor::where('FLOOR_ID', $id)->delete();

        return trans('common.delete_data');
    }
}
