<?php

namespace App\Http\Controllers\Admin_access;

use App\Http\Controllers\Controller;
use App\Models\Module;
use App\Models\ModuleAssignORG;
use App\Models\ModuleGroup;
use App\Models\ModuleGroupLevel;
use App\Models\ModuleLink;
use App\Models\ModuleLinkAddORG;
use App\Models\ModuleLinkAssignUGLW;
use App\Models\ModuleORG;
use App\Ncc\Datatablehelper;
use App\Permission;
use App\Role;
use App\User;
use Auth;
use DB;
use Illuminate\Http\Request;
use Response;
use View;

/**
 * Class     AccessController.
 *
 * @author   Abdul awal <abdulawal@atilimited.net>
 */
class AccessController extends Controller
{
    /**
     * Show all permission.
     *
     * @return \Illuminate\View\View
     */
    public function permissionIndex()
    {
        return view('admin_access.permission.index');
    }

    /**
     * Get permission source data.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return json
     */
    public function permissionData(Request $request)
    {
        $input = $request->all();
        $collection = Permission::getData($input);
        $output = Datatablehelper::permissionOutput($input, $collection);

        return response()->json($output);
    }

    /**
     * Render permission create form.
     *
     * @return \Illuminate\View\View
     */
    public function permissionCreateForm()
    {
        return View::make('admin_access.permission.create')->render();
    }

    /**
     * Create permission.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return json
     */
    public function permissionCreate(Request $request)
    {
        $data = $request->all();
        $perm_name = trim($data['NAME']);
        $perm_name = strtolower(str_replace(' ', '-', $perm_name));

        $perm_exist = Permission::where('NAME', $perm_name)->first();
        if (!empty($perm_exist)) {
            return response()->json(['flug' => 0, 'message' => trans('security.permission_exists')]);
            exit;
        }

        $admin = new Permission();
        $admin->NAME = $perm_name;
        $admin->DISPLAY_NAME = $data['DISPLAY_NAME'];
        $admin->DESCRIPTION = $data['DESCRIPTION'];
        $admin->save();

        return response()->json(['flug' => 1, 'message' => trans('security.permission_create_message')]);
    }

    /**
     * Get permission edit form.
     *
     * @param \Illuminate\Http\Request $request
     * @param string                   $id
     *
     * @return \Illuminate\View\View
     */
    public function permissionEditForm(Request $request, $id)
    {
        $permission = Permission::where('PERM_ID', $id)->first();

        return View::make('admin_access.permission.edit', compact('permission'))->render();
    }

    /**
     * Update permission.
     *
     * @param \Illuminate\Http\Request $request
     * @param string                   $id
     *
     * @return json
     */
    public function permissionUpdate(Request $request, $id)
    {
        $flug = 0;
        $msg = '';
        $data = $request->all();
        $perm_name = trim($data['NAME']);
        $perm_name = strtolower(str_replace(' ', '-', $perm_name));

        $curent = Permission::whereNotIn('PERM_ID', [$id])->where('NAME', $perm_name)->first();
        if (!empty($curent)) {
            $flug = 0;
            $msg = trans('security.permission_exists');
        } else {
            $flug = 1;
        }

        if ($flug == 1) {
            $admin = Permission::find($id);
            $admin->NAME = $perm_name;
            $admin->DISPLAY_NAME = $data['DISPLAY_NAME'];
            $admin->DESCRIPTION = $data['DESCRIPTION'];
            $admin->save();
            $msg = trans('common.update_success');
        }

        return response()->json(['flug' => $flug, 'message' => $msg]);
    }

    /**
     * Delete permission data.
     *
     * @param \Illuminate\Http\Request $request
     * @param string                   $id
     *
     * @return string
     */
    public function permissionDelete(Request $request, $id)
    {
        Permission::find($id)->delete();

        return trans('common.delete_data');
    }

    /**
     * Show all roles.
     *
     * @return \Illuminate\View\View
     */
    public function roleIndex()
    {
        return view('admin_access.role.index');
    }

    /**
     * Get role source data.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return json
     */
    public function roleData(Request $request)
    {
        $input = $request->all();
        $collection = Role::getData($input);
        $output = Datatablehelper::roleOutput($input, $collection);

        return response()->json($output);
    }

    /**
     * Render role create form.
     *
     * @return \Illuminate\View\View
     */
    public function roleCreateForm()
    {
        return View::make('admin_access.role.create')->render();
    }

    /**
     * Create role.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return json
     */
    public function roleCreate(Request $request)
    {
        $data = $request->all();
        $role_name = trim($data['NAME']);
        $role_name = strtolower(str_replace(' ', '-', $role_name));

        $perm_exist = Role::where('NAME', $role_name)->first();
        if (!empty($perm_exist)) {
            return response()->json(['flug' => 0, 'message' => trans('security.role_exists')]);
            exit;
        }

        $admin = new Role();
        $admin->NAME = $role_name;
        $admin->DISPLAY_NAME = $data['DISPLAY_NAME'];
        $admin->DESCRIPTION = $data['DESCRIPTION'];
        $admin->save();

        return response()->json(['flug' => 1, 'message' => trans('common.save_success')]);
    }

    /**
     * Get role edit form.
     *
     * @param \Illuminate\Http\Request $request
     * @param string                   $id
     *
     * @return \Illuminate\View\View
     */
    public function roleEditForm(Request $request, $id)
    {
        $role = Role::where('ROLE_ID', $id)->first();

        return View::make('admin_access.role.edit', compact('role'))->render();
    }

    /**
     * Update role.
     *
     * @param \Illuminate\Http\Request $request
     * @param string                   $id
     *
     * @return json
     */
    public function roleUpdate(Request $request, $id)
    {
        $flug = 0;
        $msg = '';
        $data = $request->all();
        $role_name = trim($data['NAME']);
        $role_name = strtolower(str_replace(' ', '-', $role_name));

        $curent = Role::whereNotIn('ROLE_ID', [$id])->where('NAME', $role_name)->first();
        if (!empty($curent)) {
            $flug = 0;
            $msg = trans('security.role_exists');
        } else {
            $flug = 1;
        }

        if ($flug == 1) {
            $admin = Role::find($id);
            $admin->NAME = $role_name;
            $admin->DISPLAY_NAME = $data['DISPLAY_NAME'];
            $admin->DESCRIPTION = $data['DESCRIPTION'];
            $admin->save();
            $msg = trans('common.update_success');
        }

        return response()->json(['flug' => $flug, 'message' => $msg]);
    }

    /**
     * Delete role with associated permissions.
     *
     * @param \Illuminate\Http\Request $request
     * @param string                   $id
     *
     * @return string
     */
    public function roleDelete(Request $request, $id)
    {
        Role::where('ROLE_ID', $id)->delete();

        return trans('common.delete_data');
    }

    /**
     * Show role permission page.
     *
     * @return \Illuminate\View\View
     */
    public function rolePermission()
    {
        $roles = Role::orderBy('ROLE_ID', 'desc')->lists('DISPLAY_NAME', 'ROLE_ID');

        return view('admin_access.role.permission_role', compact('roles'));
    }

    /**
     * Render all permissions.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\View\View
     */
    public function rolePermissions(Request $request)
    {
        $role = $request->get('role');
        $permission_role = DB::table('sa_permission_role')->where('ROLE_ID', $role)->lists('permission_id');
        $permissions = Permission::orderBy('PERM_ID', 'desc')->get();

        return View::make('admin_access.role.permissions', compact('permission_role', 'permissions'))->render();
    }

    /**
     * Assign or re-assign permissions to role.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return string
     */
    public function assignPermissions(Request $request)
    {
        $data = $request->all();
        $role = Role::find($data['role_id']);

        $permission_role = DB::table('sa_permission_role')->where('ROLE_ID', $data['role_id'])->get();
        if (!empty($permission_role) || count($permission_role) > 0) {
            DB::table('sa_permission_role')->where('ROLE_ID', $data['role_id'])->delete();
        }

        if (!empty($data['permission_id']) && count($data['permission_id']) > 0) {
            $role->perms()->sync($data['permission_id']);
        }

        return trans('common.update_success');
    }

    //********************************************************************************

    /**
     *Module List.
     *
     *@author  Nurullah <nurul@atilimited.net>
     *
     *@param \Illuminate\Http\Request  $request
     *
     * @return list of value
     */
    public function moduleSetup()
    {
        $link_url = 'module_list'; // module url
        $data['access'] = Module::checkPrevilege($link_url); // get access info by module url
        return view('security_access.modules.index', $data);
    }

    /**
     *Render New Module Create form.
     *
     *@author  Nurullah <nurul@atilimited.net>
     *
     *@param \Illuminate\view
     *
     * @return form
     */
    public function cretaeModule()
    {
        return view::make('security_access.modules.create')->render();
    }

    /**
     * Module save.
     *
     *@author  Nurullah <nurul@atilimited.net>
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return json
     */
    public function moduleSave(Request $request)
    {
        $data = $request->all();
        $user = Auth::user()->USER_ID;
        // Check name before insert
        $name_exist = Module::where('MODULE_NAME', $data['MODULE_NAME'])->first();
        if (!empty($name_exist)) {
            return response()->json(['flug' => 0, 'message' => trans('এই মডিউলের নামটি বিদ্যমান')]);
            exit;
        }
        $module = Module::createModule($data, $user, $id = null);

        return response()->json(['flug' => 1, 'message' => trans('common.save_success')]);
    }

    /**
     * Get datatable source data.
     *
     *@author  Nurullah <nurul@atilimited.net>
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return json
     */
    public function moduleData(Request $request)
    {
        $input = $request->all();
        $collection = Module::getData($input);
        $output = Datatablehelper::moduleList($input, $collection);

        return Response::json($output);
    }

    /**
     * Get Module edit form.
     *
     *@author  Nurullah <nurul@atilimited.net>
     *
     * @param \Illuminate\Http\Request $request
     * @param string                   $id
     *
     * @return \Illuminate\View\View
     */
    public function moduleEditForm(Request $request, $id)
    {
        $module = Module::find($id);

        return View::make('security_access.modules.edit', compact('module'))->render();
    }

    /**
     * Module Update.
     *
     *@author  Nurullah <nurul@atilimited.net>
     *
     * @param \Illuminate\Http\Request $request
     * @param string                   $id
     *
     * @return json
     */
    public function moduleUpdate(Request $request, $id)
    {
        $data = $request->all();
        $user = Auth::user()->USER_ID;
        // Check name before insert
        $name_exist = Module::whereNotIn('MODULE_ID', [$id])->where('MODULE_NAME', $data['MODULE_NAME'])->first();
        if (!empty($name_exist)) {
            return response()->json(['flug' => 0, 'message' => trans('এই মডিউলের নামটি বিদ্যমান')]);
            exit;
        }
        $module = Module::createModule($data, $user, $id);

        return response()->json(['flug' => 1, 'message' => trans('common.update_success')]);
    }

    /**
     * Module status.
     *
     * @author  Nurullah <nurul@atilimited.net>
     *
     * @param \Illuminate\Http\Request $request
     * @param string                   $id
     *
     * @return string
     */
    public function moduleStatus(Request $request, $id)
    {
        $category = Module::find($id);

        if ($category->IS_ACTIVE == 1) {
            $category->IS_ACTIVE = 0;
        } else {
            $category->IS_ACTIVE = 1;
        }
        $category->UPDATED_BY = Auth::user()->USER_ID;
        $category->save();

        return trans('common.status_change');
    }

    /**
     * Module delete.
     *
     *@author  Nurullah <nurul@atilimited.net>
     *
     * @param \Illuminate\Http\Request $request
     * @param string                   $id
     *
     * @return string
     */
    public function moduleDelete(Request $request, $id)
    {
        Module::where('MODULE_ID', $id)->delete();

        return trans('common.delete_data');
    }

    ////*****************************

    /**
     *All Module Link.
     *
     *@author  Nurullah <nurul@atilimited.net>
     *
     *@param \Illuminate\Http\Request  $request
     *
     * @return list of value
     */
    public function moduleLinkSetup()
    {
        $link_url = 'module_link_list'; // module Link url
        $data['access'] = Module::checkPrevilege($link_url); // get access info by module link url
        return view('security_access.modules_link.index', $data);
    }

    /**
     *Render New Link Create form.
     *
     *@author  Nurullah <nurul@atilimited.net>
     *
     *@param \Illuminate\view
     *
     * @return form
     */
    public function cretaeLink()
    {
        $data['module'] = Module::where('IS_ACTIVE', 1)->lists('MODULE_NAME', 'MODULE_ID');

        return view::make('security_access.modules_link.create', $data)->render();
    }

    /**
     * Link save.
     *
     *@author  Nurullah <nurul@atilimited.net>
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return json
     */
    public function linkSave(Request $request)
    {
        $data = $request->all();
        $user = Auth::user()->USER_ID;
        // Check name before insert
        $name_exist = ModuleLink::where('MODULE_ID', $data['MODULE_ID'])->where('LINK_NAME', $data['LINK_NAME'])->first();
        if (!empty($name_exist)) {
            return response()->json(['flug' => 0, 'message' => trans('এই মডিউলের লিংকটি বিদ্যমান')]);
            exit;
        }
        $module = ModuleLink::createLink($data, $user, $id = null);

        return response()->json(['flug' => 1, 'message' => trans('common.save_success')]);
    }

    /**
     * Get datatable source data.
     *
     *@author  Nurullah <nurul@atilimited.net>
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return json
     */
    public function linkData(Request $request)
    {
        $input = $request->all();
        $collection = ModuleLink::getData($input);
        $output = Datatablehelper::moduleLinkList($input, $collection);

        return Response::json($output);
    }

    /**
     * Get Module edit form.
     *
     *@author  Nurullah <nurul@atilimited.net>
     *
     * @param \Illuminate\Http\Request $request
     * @param string                   $id
     *
     * @return \Illuminate\View\View
     */
    public function linkEditForm(Request $request, $id)
    {
        $data['link'] = ModuleLink::find($id);
        $data['module'] = Module::where('IS_ACTIVE', 1)->get();

        return View::make('security_access.modules_link.edit', $data)->render();
    }

    /**
     * Module Update.
     *
     *@author  Nurullah <nurul@atilimited.net>
     *
     * @param \Illuminate\Http\Request $request
     * @param string                   $id
     *
     * @return json
     */
    public function linkUpdate(Request $request, $id)
    {
        $data = $request->all();
        $user = Auth::user()->USER_ID;
        // Check name before insert
        $name_exist = ModuleLink::whereNotIn('LINK_ID', [$id])->where('MODULE_ID', $data['MODULE_ID'])->where('LINK_NAME', $data['LINK_NAME'])->first();
        if (!empty($name_exist)) {
            return response()->json(['flug' => 0, 'message' => trans('এই মডিউলের নামটি বিদ্যমান')]);
            exit;
        }
        $module = ModuleLink::createLink($data, $user, $id);

        return response()->json(['flug' => 1, 'message' => trans('common.update_success')]);
    }

    /**
     * link status.
     *
     * @author  Nurullah <nurul@atilimited.net>
     *
     * @param \Illuminate\Http\Request $request
     * @param string                   $id
     *
     * @return string
     */
    public function linkStatus(Request $request, $id)
    {
        $link = ModuleLink::find($id);

        if ($link->IS_ACTIVE == 1) {
            $link->IS_ACTIVE = 0;
        } else {
            $link->IS_ACTIVE = 1;
        }
        $link->UPDATED_BY = Auth::user()->USER_ID;
        $link->save();

        return trans('common.status_change');
    }

    /**
     * Module delete.
     *
     *@author  Nurullah <nurul@atilimited.net>
     *
     * @param \Illuminate\Http\Request $request
     * @param string                   $id
     *
     * @return string
     */
    public function linkDelete(Request $request, $id)
    {
        ModuleLink::where('LINK_ID', $id)->delete();

        return trans('common.delete_data');
    }

    //==========================ORG

    /**
     *All Module Link.
     *
     *@author  Nurullah <nurul@atilimited.net>
     *
     *@param \Illuminate\Http\Request  $request
     *
     * @return list of value
     */
    public function orgSetup()
    {
        $link_url = 'module_org'; // module url
        $data['access'] = Module::checkPrevilege($link_url); // get access info by module url
        $data['org'] = ModuleORG::get();

        return view('security_access.org.index', $data);
    }

    public function assignModule()
    {
        $data['modules'] = Module::get();
        $data['active_modules'] = ModuleAssignORG::where('ORG_ID', 1)->get();

        return view::make('security_access.org.assign_module', $data)->render();
    }

    public function addModule(Request $request)
    {
        $data = $request->all();
        $user = Auth::user()->USER_ID;
        // Check name before insert
        $addModule = $data['module'];
        $module[] = '';
        $org_module = ModuleAssignORG:: get();
        foreach ($org_module as $key => $org) {
            $module[] = $org->MODULE_ID;
        }
        $result = array_diff($addModule, $module);

        if (isset($result) && count($result) > 0) {
            foreach ($result as $key => $value) {
                $exist = ModuleAssignORG::where('MODULE_ID', $value)->first();
                if (!empty($name_exist)) {
                    echo 0;
                //return response()->json(['flug' => 0, 'message' => trans('এই গ্রুপের নামটি বিদ্যমান')]);exit;
                } else {
                    $module_name = Module::where('MODULE_ID', $value)->first();
                    $module = new ModuleAssignORG();
                    $module->MODULE_ID = $value;
                    $module->MODULE_NAME = $module_name->MODULE_NAME;
                    $module->ORG_ID = 1;
                    $module->CREATED_BY = $user;
                    $module->save();
                }
            }
        }
        echo 1;
    }

    public function removeModule(Request $request)
    {
        $data = $request->all();
        $user = Auth::user()->USER_ID;
        // Check name before insert
        $removeModule = $data['module'];
        $module = [];
        $org_module = ModuleAssignORG:: get();
        foreach ($org_module as $key => $org) {
            array_push($module, $org->MODULE_ID);
        }
        $result = array_diff($module, $removeModule);

        if (isset($result) && count($result) > 0) {
            foreach ($result as $key => $value) {
                ModuleAssignORG::where('MODULE_ID', $value)->delete();
            }
        }
        //return response()->json(['flug' => 1, 'message' => trans('common.save_success')]);
        echo 1;
    }

    public function assignPages()
    {
        $data['active_modules'] = ModuleAssignORG::where('ORG_ID', 1)->get();
//        foreach ($data['active_modules'] as $key => $value) {
//            $data['org_module_links'] = ModuleLink:: where('MODULE_ID', $value->MODULE_ID)->get();
//        }
        return view::make('security_access.org.assign_page', $data)->render();
    }

    public function addRemovePages(Request $request)
    {
        $data = $request->all();
        $user = Auth::user()->USER_ID;

        $values = explode(',', $data['values']);
        //print_r($values); exit;
        $module_id = $values[0];
        $link_id = $values[1];
        $page_type = $values[2];
        $org_id = $values[3];
        $is_checked = $data['is_checked'];

        $link_Info = ModuleLink:: where('LINK_ID', $link_id)->first();
        $check_existance = ModuleLinkAddORG:: where('MODULE_ID', $module_id)->where('LINK_ID', $link_id)->where('ORG_ID', $org_id)->first();

        if (!empty($check_existance)) {
            $moduleLinkUpdate = ModuleLinkAddORG::find($check_existance->ORG_MLINKS_ID);
            $moduleLinkUpdate->CREATE = ($page_type == 'C') ? $is_checked : $check_existance->CREATE;
            $moduleLinkUpdate->READ = ($page_type == 'R') ? $is_checked : $check_existance->READ;
            $moduleLinkUpdate->UPDATE = ($page_type == 'U') ? $is_checked : $check_existance->UPDATE;
            $moduleLinkUpdate->DELETE = ($page_type == 'D') ? $is_checked : $check_existance->DELETE;
            $moduleLinkUpdate->STATUS = ($page_type == 'S') ? $is_checked : $check_existance->STATUS;
            $moduleLinkUpdate->UPDATED_BY = $user;
            $moduleLinkUpdate->save();

            echo 'updated';
        } else {
            $moduleLink = new ModuleLinkAddORG();
            $moduleLink->LINK_ID = $link_id;
            $moduleLink->LINK_NAME = $link_Info->LINK_NAME;
            $moduleLink->LINK_PAGES = $link_Info->LINK_PAGES;
            $moduleLink->LINK_URI = $link_Info->LINK_URI;
            $moduleLink->ORG_ID = $org_id;
            $moduleLink->MODULE_ID = $module_id;
            $moduleLink->CREATE = ($page_type == 'C') ? 1 : 0;
            $moduleLink->READ = ($page_type == 'R') ? 1 : 0;
            $moduleLink->UPDATE = ($page_type == 'U') ? 1 : 0;
            $moduleLink->DELETE = ($page_type == 'D') ? 1 : 0;
            $moduleLink->STATUS = ($page_type == 'S') ? 1 : 0;
            $moduleLink->CREATED_BY = $user;
            $moduleLink->save();

            echo 'inserted';
        }
    }

    /**
     *All Module Link.
     *
     *@author  Nurullah <nurul@atilimited.net>
     *
     *@param \Illuminate\Http\Request  $request
     *
     * @return list of value
     */
    public function allGroup()
    {
        $link_url = 'all_group'; // module url
        $data['access'] = Module::checkPrevilege($link_url); // get access info by module url
        $data['groups'] = ModuleGroup::get();
        foreach ($data['groups'] as $key => $value) {
            $data['group_levels'] = ModuleGroupLevel::where('USERGRP_ID', $value->USERGRP_ID)->get();
        }

        return view('security_access.user_group.all_groups', $data);
    }

    /**
     *Render New Link Create form.
     *
     *@author  Nurullah <nurul@atilimited.net>
     *
     *@param \Illuminate\view
     *
     * @return form
     */
    public function cretaeGroup()
    {
        return view::make('security_access.user_group.create_group')->render();
    }

    /**
     * Link save.
     *
     *@author  Nurullah <nurul@atilimited.net>
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return json
     */
    public function groupSave(Request $request)
    {
        $data = $request->all();
        $user = Auth::user()->USER_ID;
        // Check name before insert
        $name_exist = ModuleGroup::where('USERGRP_NAME', $data['USERGRP_NAME'])->first();
        if (!empty($name_exist)) {
            return response()->json(['flug' => 0, 'message' => trans('এই গ্রুপের নামটি বিদ্যমান')]);
            exit;
        }
        $group = ModuleGroup::createGroup($data, $user, $id = null);

        return response()->json(['flug' => 1, 'message' => trans('common.save_success')]);
    }

    /**
     *Render New Link Create form.
     *
     *@author  Nurullah <nurul@atilimited.net>
     *
     *@param \Illuminate\view
     *
     * @return form
     */
    public function cretaeGroupLevel(Request $request, $id)
    {
        $data['id'] = $id;

        return view::make('security_access.user_group.create_group_level', $data)->render();
    }

    /**
     * Link save.
     *
     *@author  Nurullah <nurul@atilimited.net>
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return json
     */
    public function groupLevelSave(Request $request, $id)
    {
        $data = $request->all();
        $user = Auth::user()->USER_ID;
        // Check name before insert
        $name_exist = ModuleGroupLevel::where('UGLEVE_NAME', $data['UGLEVE_NAME'])->first();
        if (!empty($name_exist)) {
            return response()->json(['flug' => 0, 'message' => trans('এই গ্রুপের নামটি বিদ্যমান')]);
            exit;
        }
        $groupLevel = ModuleGroupLevel::createGroupLevel($data, $user, $id);

        return response()->json(['flug' => 1, 'message' => trans('common.save_success')]);
    }

    /**
     *All Module Link.
     *
     *@author  Nurullah <nurul@atilimited.net>
     *
     *@param \Illuminate\Http\Request  $request
     *
     * @return list of value
     */
    public function assignModuleToGroup()
    {
        $link_url = 'assign_to_group'; // module url
        $data['access'] = Module::checkPrevilege($link_url); // get access info by module url
        $data['groups'] = ModuleGroup::get();
        $data['org_modules'] = ModuleAssignORG::get();

        return view('security_access.assign_module.assign_module', $data);
    }

    public function getLevelByGroup(Request $request)
    {
        $group = $request->get('groupID');
        $groupLevel = ModuleGroupLevel::where('USERGRP_ID', $group)->where('IS_ACTIVE', 1)->get();

        $data = '<option value="">'.trans('common.form_select').'</option>';
        if (count($groupLevel) > 0) {
            foreach ($groupLevel as $value) {
                $data .= '<option value="'.$value->UG_LEVEL_ID.'">'.$value->UGLEVE_NAME.'</option>';
            }
        }

        return $data;
    }

    public function getUserByLevel(Request $request)
    {
        $group = $request->get('group_id');
        $groupLevel = $request->get('level_id');
        $user = User::where('USERGRP_ID', $group)->where('USERLVL_ID', $groupLevel)->where('IS_ACTIVE', 1)->get();

        $data = '<option value="">'.trans('common.form_select').'</option>';
        if (count($user) > 0) {
            foreach ($user as $value) {
                $data .= '<option value="'.$value->USER_ID.'">'.$value->FULL_NAME.'</option>';
            }
        }

        return $data;
    }

    public function getModuleAcceesByGroup(Request $request)
    {
        $data = $request->all();
        $data['group'] = $data['group'];
        $data['org_modules'] = ModuleAssignORG:: where('ORG_ID', 1)->where('IS_ACTIVE', 1)->get();

        return view::make('security_access.assign_module.assign_module_by_group_id', $data)->render();
    }

    public function getModuleAcceesByGroupLevel(Request $request)
    {
        $data = $request->all();
        $data['group'] = $data['group'];
        $data['level'] = $data['level'];
        $data['org_modules'] = ModuleAssignORG:: where('ORG_ID', 1)->where('IS_ACTIVE', 1)->get();

        return view::make('security_access.assign_module.assign_module_by_group_level', $data)->render();
    }

    //User
    public function getModuleAcceesByUser(Request $request)
    {
        $data = $request->all();
        $data['group'] = $data['group_id'];
        $data['level'] = $data['level_id'];
        $data['user'] = $data['user_id'];
        $data['org_modules'] = ModuleAssignORG:: where('ORG_ID', 1)->where('IS_ACTIVE', 1)->get();

        return view::make('security_access.assign_module.assign_module_by_user', $data)->render();
    }

    public function assignModuleToGroupAction(Request $request)
    {
        $data = $request->all();
        $user = Auth::user()->USER_ID;
        $group = $data['group_id'];
        $level = $data['level_id'];
        $assign_user = $data['user'];
        $values = explode(',', $data['values']);
        $module_id = $values[0];
        $link_id = $values[1];
        $page_type = $values[2];
        $is_checked = $data['is_checked'];

        $org_link = ModuleLinkAddORG::where('ORG_MLINKS_ID', $link_id)->where('ORG_ID', 1)->first();
        $check_existance = ModuleLinkAssignUGLW::where('ORG_MLINKS_ID', $link_id)->where('USERGRP_ID', $group)->where('UG_LEVEL_ID', $level)->where('USER_ID', $assign_user)->where('ORG_ID', 1)->first();
        //var_dump($check_existance);exit;
        if (!empty($check_existance)) {
            $uglwUpdate = ModuleLinkAssignUGLW::find($check_existance->UGLWM_LINK);
            $uglwUpdate->CREATE = ($page_type == 'C') ? $is_checked : $check_existance->CREATE;
            $uglwUpdate->READ = ($page_type == 'R') ? $is_checked : $check_existance->READ;
            $uglwUpdate->UPDATE = ($page_type == 'U') ? $is_checked : $check_existance->UPDATE;
            $uglwUpdate->DELETE = ($page_type == 'D') ? $is_checked : $check_existance->DELETE;
            $uglwUpdate->STATUS = ($page_type == 'S') ? $is_checked : $check_existance->STATUS;
            $uglwUpdate->UPDATED_BY = $user;
            $uglwUpdate->save();

            echo 'Updated';
        } else {
            $uglwInsert = new ModuleLinkAssignUGLW();
            $uglwInsert->ORG_MLINKS_ID = $link_id;
            $uglwInsert->ORG_ID = 1;
            $uglwInsert->USER_ID = $assign_user;
            $uglwInsert->USERGRP_ID = $group;
            $uglwInsert->UG_LEVEL_ID = $level;
            $uglwInsert->MODULE_ID = $module_id;
            $uglwInsert->LINK_ID = $org_link->LINK_ID;
            $uglwInsert->LINK_URI = $org_link->LINK_URI;
            $uglwInsert->CREATE = ($page_type == 'C') ? 1 : 0;
            $uglwInsert->READ = ($page_type == 'R') ? 1 : 0;
            $uglwInsert->UPDATE = ($page_type == 'U') ? 1 : 0;
            $uglwInsert->DELETE = ($page_type == 'D') ? 1 : 0;
            $uglwInsert->STATUS = ($page_type == 'S') ? 1 : 0;
            $uglwInsert->CREATED_BY = $user;
            $uglwInsert->save();

            echo 'Inserted';
        }
    }

//
//     /**
//     *Render New Link Create form
//     *@author  Nurullah <nurul@atilimited.net>
//     *@param \Illuminate\view
//     * @return form
//     */
//    public function cretaeLink()
//    {
//        $data['module'] = Module::where('IS_ACTIVE', 1)->lists('MODULE_NAME', 'MODULE_ID');
//        return view::make('security_access.modules_link.create',$data)->render();
//    }
//
//
//    /**
//     * Link save
//     *@author  Nurullah <nurul@atilimited.net>
//     * @param \Illuminate\Http\Request  $request
//     * @return json
//     */
//    public function linkSave(Request $request)
//    {
//        $data       = $request->all();
//        $user       = Auth::user()->USER_ID;
//        // Check name before insert
//        $name_exist = ModuleLink::where('MODULE_ID', $data['MODULE_ID'])->where('LINK_NAME', $data['LINK_NAME'])->first();
//        if(!empty($name_exist)) {
//            return response()->json(['flug' => 0, 'message' => trans('এই মডিউলের লিংকটি বিদ্যমান')]);exit;
//        }
//        $module   = ModuleLink::createLink($data, $user, $id = null);
//
//        return response()->json(['flug' => 1, 'message' => trans('common.save_success')]);
//    }
//
//    /**
//     * Get datatable source data
//     *@author  Nurullah <nurul@atilimited.net>
//     * @param \Illuminate\Http\Request  $request
//     * @return json
//     */
//    public function linkData(Request $request)
//    {
//        $input      = $request->all();
//        $collection = ModuleLink::getData($input);
//        $output     = Datatablehelper::moduleLinkList($input, $collection);
//
//        return Response::json($output);
//    }
//
//    /**
//     * Get Module edit form
//     *@author  Nurullah <nurul@atilimited.net>
//     * @param \Illuminate\Http\Request  $request
//     * @param string $id
//     * @return \Illuminate\View\View
//     */
//    public function linkEditForm(Request $request, $id)
//    {
//        $data['link'] = ModuleLink::find($id);
//        $data['module'] = Module::where('IS_ACTIVE', 1)->get();
//
//        return View::make('security_access.modules_link.edit', $data)->render();
//    }
//
//    /**
//     * Module Update
//     *@author  Nurullah <nurul@atilimited.net>
//     * @param \Illuminate\Http\Request  $request
//     * @param string $id
//     * @return json
//     */
//    public function linkUpdate(Request $request, $id)
//    {
//        $data       = $request->all();
//         $user       = Auth::user()->USER_ID;
//        // Check name before insert
//        $name_exist = ModuleLink::whereNotIn('LINK_ID', [$id])->where('MODULE_ID', $data['MODULE_ID'])->where('LINK_NAME', $data['LINK_NAME'])->first();
//        if(!empty($name_exist)) {
//            return response()->json(['flug' => 0, 'message' => trans('এই মডিউলের নামটি বিদ্যমান')]);exit;
//        }
//        $module   = ModuleLink::createLink($data, $user, $id);
//
//        return response()->json(['flug' => 1, 'message' => trans('common.update_success')]);
//    }
//
//    /**
//     * link status
//     * @author  Nurullah <nurul@atilimited.net>
//     * @param \Illuminate\Http\Request  $request
//     * @param string $id
//     * @return string
//     */
//    public function linkStatus(Request $request, $id)
//    {
//        $link = ModuleLink::find($id);
//
//        if ($link->IS_ACTIVE == 1) {
//            $link->IS_ACTIVE = 0;
//        } else {
//            $link->IS_ACTIVE = 1;
//        }
//        $link->UPDATED_BY = Auth::user()->USER_ID;
//        $link->save();
//
//        return trans('common.status_change');
//    }
//
//    /**
//     * Module delete
//     *@author  Nurullah <nurul@atilimited.net>
//     * @param \Illuminate\Http\Request  $request
//     * @param string $id
//     * @return string
//     */
//    public function linkDelete(Request $request, $id)
//    {
//        ModuleLink::where('LINK_ID', $id)->delete();
//
//        return trans('common.delete_data');
//    }
}
