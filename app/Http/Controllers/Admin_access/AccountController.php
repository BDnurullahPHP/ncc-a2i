<?php

namespace App\Http\Controllers\Admin_access;

use App\Http\Controllers\Controller;
use App\Models\AccountVaucherChd;
use App\Models\AccountVnLedger;
use App\Models\ApplicantProperty;
use App\Models\Bank;
use App\Models\CategoryParticular;
use App\Models\Citizen;
use App\Models\CitizenApplication;
use App\Models\District;
use App\Models\Division;
use App\Models\LeaseDemesne;
use App\Models\Module;
use App\Models\Officer;
use App\Models\Particular;
use App\Models\PaymentResponse;
use App\Models\Project;
use App\Models\ProjectCategory;
use App\Models\ProjectLocation;
use App\Models\SecurityRefund;
use App\Models\Tender;
use App\Models\TenderApplicant;
use App\Models\TenderInstallment;
use App\Models\TenderLetter;
use App\Models\TenderSchedule;
use App\Models\Thana;
use App\Ncc\Datatablehelper;
use App\Ncc\Helpers;
use App\User;
use Auth;
use Illuminate\Http\Request;
use Response;
use View;

/**
 * Class      AccountController.
 *
 * @author   Nurullah <nurul@atilimited.net>
 */
class AccountController extends Controller
{
    /**
     * Category particular.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\View\View
     */
    public function categoryParticular(Request $request)
    {
        $link_url = 'category_particular'; // module url
        $data['access'] = Module::checkPrevilege($link_url); // get access info by module url
        return view('admin_access.category_particular.index', $data);
    }

    /**
     * Get category particular data.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return json
     */
    public function categoryParticularData(Request $request)
    {
        $input = $request->all();
        $collection = CategoryParticular::getData($input);
        $output = Datatablehelper::categoryParticularOutput($input, $collection);

        return response()->json($output);
    }

    /**
     * Render category particular create form.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\View\View
     */
    public function categoryParticularCreateForm(Request $request)
    {
        $data['category'] = ProjectCategory::where('IS_ACTIVE', 1)->lists('CATE_NAME', 'PR_CATEGORY');
        $data['particular'] = Particular::where('IS_ACTIVE', 1)->lists('PARTICULAR_NAME', 'PARTICULAR_ID');

        return View::make('admin_access.category_particular.create', $data)->render();
    }

    /**
     * Create category particular data.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return json
     */
    public function categoryParticularCreate(Request $request)
    {
        $data = $request->all();
        // Check dublicak before insert
        $name_exist = CategoryParticular::where('PR_CATEGORY', $data['PR_CATEGORY'])
                                ->where('PARTICULAR_ID', $data['PARTICULAR_ID'])
                                ->where('PARTICULAR_AMT', $data['PARTICULAR_AMT'])
                                ->where('UOM', $data['UOM'])->first();
        if (!empty($name_exist)) {
            return response()->json(['flug' => 0, 'message' => trans('account.particular_type_exists')]);
            exit;
        }
        $cat_particular = new CategoryParticular();
        $cat_particular->PR_CATEGORY = $data['PR_CATEGORY'];
        $cat_particular->PARTICULAR_ID = $data['PARTICULAR_ID'];
        $cat_particular->PARTICULAR_AMT = $data['PARTICULAR_AMT'];
        $cat_particular->UOM = $data['UOM'];
        $cat_particular->REMARKS = $data['REMARKS'];
        $cat_particular->IS_ACTIVE = isset($data['IS_ACTIVE']) ? $data['IS_ACTIVE'] : 0;
        $cat_particular->CREATED_BY = Auth::user()->USER_ID;
        $cat_particular->save();

        return response()->json(['flug' => 1, 'message' => trans('common.save_success')]);
    }

    /**
     * Change category particular status.
     *
     * @param \Illuminate\Http\Request $request
     * @param string                   $id
     *
     * @return string
     */
    public function categoryParticularStatus(Request $request, $id)
    {
        $cat_particular = CategoryParticular::find($id);

        if ($cat_particular->IS_ACTIVE == 1) {
            $cat_particular->IS_ACTIVE = 0;
        } else {
            $cat_particular->IS_ACTIVE = 1;
        }
        $cat_particular->save();

        return trans('common.status_change');
    }

    /**
     * Render category particular create form.
     *
     * @param \Illuminate\Http\Request $request
     * @param string                   $id
     *
     * @return \Illuminate\View\View
     */
    public function categoryParticularEditForm(Request $request, $id)
    {
        $data['cat_part'] = CategoryParticular::find($id);
        $data['category'] = ProjectCategory::where('IS_ACTIVE', 1)->lists('CATE_NAME', 'PR_CATEGORY');
        $data['particular'] = Particular::where('IS_ACTIVE', 1)->lists('PARTICULAR_NAME', 'PARTICULAR_ID');

        return View::make('admin_access.category_particular.edit', $data)->render();
    }

    /**
     * Update category particular data.
     *
     * @param \Illuminate\Http\Request $request
     * @param string                   $id
     *
     * @return json
     */
    public function categoryParticularUpdate(Request $request, $id)
    {
        $data = $request->all();
        $name_exist = CategoryParticular::whereNotIn('CAT_PART_ID', [$id])
                                ->where('PR_CATEGORY', $data['PR_CATEGORY'])
                                ->where('PARTICULAR_ID', $data['PARTICULAR_ID'])
                                ->where('PARTICULAR_AMT', $data['PARTICULAR_AMT'])
                                ->where('UOM', $data['UOM'])->first();
        if (!empty($name_exist)) {
            return response()->json(['flug' => 0, 'message' => trans('account.particular_type_exists')]);
            exit;
        }
        $cat_particular = CategoryParticular::find($id);
        $cat_particular->PR_CATEGORY = $data['PR_CATEGORY'];
        $cat_particular->PARTICULAR_ID = $data['PARTICULAR_ID'];
        $cat_particular->PARTICULAR_AMT = $data['PARTICULAR_AMT'];
        $cat_particular->UOM = $data['UOM'];
        $cat_particular->REMARKS = $data['REMARKS'];
        $cat_particular->IS_ACTIVE = isset($data['IS_ACTIVE']) ? $data['IS_ACTIVE'] : 0;
        $cat_particular->UPDATED_BY = Auth::user()->USER_ID;
        $cat_particular->save();

        return response()->json(['flug' => 1, 'message' => trans('common.update_success')]);
    }

    /**
     * Delete category particular data.
     *
     * @param \Illuminate\Http\Request $request
     * @param string                   $id
     *
     * @return string
     */
    public function categoryParticularDelete(Request $request, $id)
    {
        CategoryParticular::find($id)->delete();

        return trans('common.delete_data');
    }

    /**
     * Show the List of Particular.
     *
     * @author Nurullah <nurul@atilimited.net>
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\View\View
     */
    public function particular(Request $request)
    {
        $link_url = 'particular_list'; // module url
        $data['access'] = Module::checkPrevilege($link_url); // get access info by module url
        return view('admin_access.account_particular.index', $data);
    }

    /**
     * Get datatable source data of ac_particular.
     *
     * @author Nurullah <nurul@atilimited.net>
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return json
     */
    public function particularData(Request $request)
    {
        $input = $request->all();
        $collection = Particular::getData($input);
        $output = Datatablehelper::particularOutput($input, $collection);

        return response()->json($output);
    }

    /**
     * Show the Particular Insert Form.
     *
     * @author Nurullah <nurul@atilimited.net>
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\View\View
     */
    public function particularCreateForm(Request $request)
    {
        return View::make('admin_access.account_particular.create')->render();
    }

    /**
     * New Mouza Insert.
     *
     * @author Nurullah <nurul@atilimited.net>
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return json
     */
    public function particularSave(Request $request)
    {
        $data = $request->all();

        $particular_name_exist = Particular::where('PARTICULAR_NAME', $data['PARTICULAR_NAME'])->first();
        if (!empty($particular_name_exist)) {
            return response()->json(['flug' => 0, 'message' => trans('account.particular_exists')]);
            exit;
        }

        $particular = new Particular();
        $particular->PARTICULAR_NAME = $data['PARTICULAR_NAME'];
        $particular->PARTICULAR_DESC = $data['PARTICULAR_DESC'];
        $particular->IS_ACTIVE = isset($data['IS_ACTIVE']) ? $data['IS_ACTIVE'] : 0;
        $particular->CREATED_BY = Auth::user()->USER_ID;
        $particular->save();

        return response()->json(['flug' => 1, 'message' => trans('common.save_success')]);
    }

    /**
     * Change Particular status.
     *
     * @author Nurullah <nurul@atilimited.net>
     *
     * @param \Illuminate\Http\Request $request
     * @param string                   $id
     *
     * @return string
     */
    public function particularStatus(Request $request, $id)
    {
        $particular = Particular::find($id);

        if ($particular->IS_ACTIVE == 1) {
            $particular->IS_ACTIVE = 0;
        } else {
            $particular->IS_ACTIVE = 1;
        }
        $particular->UPDATED_BY = Auth::user()->USER_ID;
        $particular->save();

        return trans('common.status_change');
    }

    /**
     * Get  Particular edit form.
     *
     * @author Nurullah <nurul@atilimited.net>
     *
     * @param \Illuminate\Http\Request $request
     * @param string                   $id
     *
     * @return \Illuminate\View\View
     */
    public function particularEditForm(Request $request, $id)
    {
        $particular = Particular::where('PARTICULAR_ID', $id)->first();

        return View::make('admin_access.account_particular.edit', compact('particular'))->render();
    }

    /**
     *  Particular update.
     *
     * @author Nurullah <nurul@atilimited.net>
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return json
     */
    public function particularUpdate(Request $request, $id)
    {
        $flug = 0;
        $msg = '';
        $data = $request->all(); //print_r($data);exit;
        $perm_name = trim($data['PARTICULAR_NAME']);

        $curent = Particular::whereNotIn('PARTICULAR_ID', [$id])->where('PARTICULAR_NAME', $perm_name)->first();
        if (!empty($curent)) {
            $flug = 0;
            $msg = trans('account.particular_exists');
        } else {
            $flug = 1;
        }

        if ($flug == 1) {
            $particular = Particular::find($id);
            $particular->PARTICULAR_NAME = $perm_name;
            $particular->PARTICULAR_DESC = $data['PARTICULAR_DESC'];
            $particular->IS_ACTIVE = isset($data['IS_ACTIVE']) ? 1 : 0;
            $particular->UPDATED_BY = Auth::user()->USER_ID;
            $particular->save();
            $msg = trans('common.update_success');
        }

        return response()->json(['flug' => $flug, 'message' => $msg]);
    }

    /**
     * Particular delete.
     *
     * @author Nurullah <nurul@atilimited.net>
     *
     * @param \Illuminate\Http\Request $request
     * @param string                   $id
     *
     * @return string
     */
    public function particularDelete(Request $request, $id)
    {
        Particular::where('PARTICULAR_ID', $id)->delete();

        return trans('common.delete_data');
    }

    /**
     * Bank List.
     *
     * @author Jahid Hasan <jahid@atilimited.net>
     *
     * @return \Illuminate\View\View
     */
    public function bankList()
    {
        $link_url = 'bank_list'; // module url
        $data['access'] = Module::checkPrevilege($link_url); // get access info by module url
        return view('admin_access.bank.index', $data);
    }

    /**
     * Get bank data.
     *
     * @author Jahid Hasan <jahid@atilimited.net>
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return json
     */
    public function bankData(Request $request)
    {
        $input = $request->all();
        $collection = Bank::getData($input);
        $output = Datatablehelper::bankOutput($input, $collection);

        return response()->json($output);
    }

    /**
     * Render user create form.
     *
     * @author Jahid Hasan <jahid@atilimited.net>
     *
     * @return \Illuminate\View\View
     */
    public function bankCreateForm()
    {
        $bank_list = Bank::where('B_PARENT_ID', 0)->lists('BANK_NAME', 'BANK_ID');

        return View::make('admin_access.bank.create', compact('bank_list'))->render();
    }

    /**
     * Render Bank create form.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @author Jahid Hasan <jahid@atilimited.net>
     *
     * @return json
     */
    public function createBank(Request $request)
    {
        $data = $request->all();
        // Check Bank Name before insert
        $name_exist = Bank::where('BANK_NAME', $data['txtBankName'])
                            ->where('B_PARENT_ID', $data['cmbParentBank'])->first();
        if (!empty($name_exist)) {
            return response()->json(['flug' => 0, 'message' => trans('bank.bank_exits')]);
            exit;
        }
        $user = new Bank();
        $user->BANK_NAME = $data['txtBankName'];
        $user->ADDRESS = $data['txtBankAddress'];
        $user->B_PARENT_ID = ($data['cmbParentBank'] != '') ? $data['cmbParentBank'] : '';
        $user->IS_ACTIVE = isset($data['IS_ACTIVE']) ? $data['IS_ACTIVE'] : 0;
        $user->CREATED_BY = Auth::user()->USER_ID;
        $user->save();

        return response()->json(['flug' => 1, 'message' => trans('bank.bank_create_message')]);
    }

    /**
     * Update bank information.
     *
     * @param \Illuminate\Http\Request $request
     * @param string                   $id
     *
     * @return json
     */
    public function bankUpdate(Request $request, $id)
    {
        $data = $request->all();
        // Check name before insert
        $name_exist = Bank::whereNotIn('BANK_ID', [$id])->where('BANK_NAME', $data['BANK_NAME'])->first();
        if (!empty($name_exist)) {
            return response()->json(['flug' => 0, 'message' => trans('land.subcat_name_exists')]);
            exit;
        }
        // Check bangla name before insert
        $bn_name_exist = Bank::whereNotIn('BANK_ID', [$id])->where('BANK_NAME', $data['BANK_NAME'])->first();
        if (!empty($bn_name_exist)) {
            return response()->json(['flug' => 0, 'message' => trans('land.subcat_bn_name_exists')]);
            exit;
        }

        $bank_list_c = Bank::find($id);
        $bank_list_c->B_PARENT_ID = $data['B_PARENT_ID'];
        $bank_list_c->BANK_NAME = $data['BANK_NAME'];
        $bank_list_c->ADDRESS = $data['ADDRESS'];
        $bank_list_c->IS_ACTIVE = isset($data['IS_ACTIVE']) ? $data['IS_ACTIVE'] : 0;
        $bank_list_c->UPDATED_BY = Auth::user()->USER_ID;
        $bank_list_c->save();

        return response()->json(['flug' => 1, 'message' => trans('common.update_success')]);
    }

    /**
     * Get bank edit form.
     *
     * @param \Illuminate\Http\Request $request
     * @param string                   $id
     *
     * @return \Illuminate\View\View
     */
    public function bankEditForm(Request $request, $id)
    {
        $bank_list_s = Bank::find($id);
        $bank_list_c = Bank::where('B_PARENT_ID', 0)
                         ->whereNotIn('BANK_ID', [$id])
                         ->select('BANK_NAME', 'BANK_ID')->get();

        return View::make('admin_access.bank.edit', compact('bank_list_s', 'bank_list_c'))->render();
    }

    /**
     * Change Bank status.
     *
     * @param \Illuminate\Http\Request $request
     * @param string                   $id
     *
     * @return string
     */
    public function bankStatus(Request $request, $id)
    {
        $bank_list = Bank::find($id);

        if ($bank_list->IS_ACTIVE == 1) {
            $bank_list->IS_ACTIVE = 0;
        } else {
            $bank_list->IS_ACTIVE = 1;
        }
        $bank_list->UPDATED_BY = Auth::user()->USER_ID;
        $bank_list->save();

        return trans('common.status_change');
    }

    /**
     * Cashiar cashbook.
     *
     * @return \Illuminate\View\View
     */
    public function cashiarCashbook()
    {
        return view('admin_access.account_particular.cashiarCashbook');
    }

    /**
     * Cashiar cashbook.
     *
     * @return string
     */
    public function accountCashbook()
    {
        echo 'Under Construction';
    }

    /**
     * Bank delete.
     *
     * @param \Illuminate\Http\Request $request
     * @param string                   $id
     *
     * @return string
     */
    public function bankDelete(Request $request, $id)
    {
        Bank::where('BANK_ID', $id)->delete();

        return trans('common.delete_data');
    }

    /**
     * Show all revenue.
     *
     * @return \Illuminate\View\View
     */
    public function revenueList()
    {
        $link_url = 'revenue_list'; // module url
        $data['access'] = Module::checkPrevilege($link_url); // get access info by module url

        $data['category'] = ProjectCategory::where('IS_ACTIVE', 1)->lists('CATE_NAME', 'PR_CATEGORY');
        $data['particular'] = Particular::where('IS_ACTIVE', 1)->lists('PARTICULAR_NAME', 'PARTICULAR_ID');
        $data['te_applicant'] = User::getUserAll();

        return view('admin_access.revenue.index', $data)->render();
    }

    /**
     * Show revenue data.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return json
     */
    public function revenueData(Request $request)
    {
        $input = $request->all();
        $collection = AccountVaucherChd::revenueSearchDataTable($input);
        $output = Datatablehelper::revenueDataList($input, $collection);

        return response()->json($output);
    }

    /**
     * print user data by voucher no.
     *
     * @return PDF
     */
    public function revenueDataPrintByVoucher(Request $request)
    {
        $input = $request->all();
        $revenue_data = AccountVaucherChd::revenueSearchData($input);

        // Make PDF
        $pdf_data = View::make('admin_access.revenue.revenue_print', compact('revenue_data'));
        $file_name = 'NCC-Revenue'.'.pdf';

        return Helpers::makePDF($file_name, $pdf_data);
    }

    /**
     * Payment data info.
     *
     * @param \Illuminate\Http\Request $request
     * @param string                   $type
     * @param string                   $sc
     * @param string                   $vchd
     *
     * @return \Illuminate\View\View
     */
    public function paymentInfo(Request $request, $type, $ta, $sc, $vchd)
    {
        if ($type == 'applicant') {
            $data['applicant_property'] = ApplicantProperty::with('getBank')
                                                            ->where('TE_APP_ID', $ta)
                                                            ->where('SCHEDULE_ID', $sc)
                                                            ->first();
        } elseif ($type == 'citizen') {
            $data['applicant_property'] = CitizenApplication::with('getBank')
                                                            ->where('CITIZEN_ID', $ta)
                                                            ->where('SCHEDULE_ID', $sc)
                                                            ->first();
        } else {
            $data['applicant_property'] = LeaseDemesne::with(['getSchedule' => function ($query) {
                $query->with('getProject');
            }, 'getValidity'])->where('SCHEDULE_ID', $sc)->first();
        }
        $data['ledger'] = AccountVnLedger::where('TRX_TRAN_NO', $vchd)->where('TRX_CODE_ID', 5)->first();
        $data['payment_info'] = PaymentResponse::where('TRX_TRAN_NO', $vchd)->where('STATUS', 'VALID')->first();

        return View::make('admin_access.accounts.payment_info', compact('type'))->with($data)->render();
    }

    /**
     * All Pending Payment.
     *
     * @author Nurullah <nurul@atilimited.net>
     *
     * @return \Illuminate\View\View
     */
    public function paymentPendingList()
    {
        return view('admin_access.pending_payment.index');
    }

    /**
     * Get All Pending Payment data.
     *
     * @author Nurullah <nurul@atilimited.net>
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return json
     */
    public function pendingPaymentData(Request $request)
    {
        $input = $request->all();
        $collection = AccountVnLedger::getPendingPaymentData($input);
        $output = Datatablehelper::pendingPaymentOutput($input, $collection);

        return response()->json($output);
    }

    /**
     * approve payment.
     *
     * @author Nurullah <nurul@atilimited.net>
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return string
     */
    public function approvePayment(Request $request, $id)
    {
        $user = Auth::user()->USER_ID;
        $approve = AccountVnLedger::approvePayment($id, $user);

        return trans('account.installment_approved');
    }

    /**
     * Tender pending applicant list.
     *
     * @return \Illuminate\View\View
     */
    public function applicantPendingList()
    {
        $tender = Tender::where('IS_ACTIVE', 1)->lists('TENDER_TITLE', 'TENDER_ID');

        return view('admin_access.accounts.applicant_pending', compact('tender'));
    }

    /**
     * Get bank data.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return json
     */
    public function installmentPendingData(Request $request)
    {
        $input = $request->all();
        $collection = TenderApplicant::getTenderListData($input);
        $output = Datatablehelper::tenderApplicantDataList($input, $collection);

        return response()->json($output);
    }

    /**
     * Tender pending installment list.
     *
     * @param \Illuminate\Http\Request $request
     * @param string                   $id
     *
     * @return \Illuminate\View\View
     */
    public function installmentPendingList(Request $request, $type, $id)
    {
        if ($type == 'applicant') {
            $installment = TenderInstallment::where('TE_APP_ID', $id)->get();
        } else {
            $installment = TenderInstallment::where('CITIZEN_ID', $id)->get();
        }

        return View::make('admin_access.accounts.payment_pending', compact('installment'))->render();
    }

    /**
     * Approve installment.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return string
     */
    public function approveInstallment(Request $request, $type, $id)
    {
        $user = Auth::user()->USER_ID;
        $approve_ins = TenderInstallment::approveInstallment($type, $id, $user);

        return trans('account.installment_approved');
    }

    /**
     * Show all revenue.
     *
     * @return \Illuminate\View\View
     */
    public function leaseRevenueList()
    {
        $link_url = 'lease_revenue_list'; // module url
        $data['access'] = Module::checkPrevilege($link_url); // get access info by module url

        $data['project'] = TenderSchedule::with('getProject')
                                    ->leftJoin('tender', 'tender.TENDER_ID', '=', 'tender_schedule.TENDER_ID')
                                    ->where('tender.IS_LEASE', 1)->get();
        $data['applicant'] = TenderApplicant::where('IS_ACTIVE', 1)->lists('APPLICANT_NAME', 'TE_APP_ID');
        $data['lease_part'] = Particular::whereIn('PARTICULAR_ID', [6, 7, 9, 10, 11])
                                        ->orderBy('PARTICULAR_ID', 'asc')
                                        ->get();

        $applicant = ApplicantProperty::with('getApplicant')
                              ->where('IS_ACTIVE', 1)
                              ->where('IS_LEASE', 1)
                              ->get();
        $applicant = collect($applicant);

        $citizen = CitizenApplication::with(['getCitizen'=> function ($query) {
            $query->with('getTenderCitizen');
        },
                                    ])
                              ->where('IS_ACTIVE', 1)
                              ->where('IS_LEASE', 1)
                              ->get();
        $citizen = collect($citizen);
        $data['applicant_data'] = $applicant->merge($citizen);

        return view('admin_access.lease_report.index', $data)->render();
    }

    /**
     * print user data by voucher no.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return PDF
     */
    public function leaseRevenueDataPrint(Request $request)
    {
        $input = $request->all();
        $data['lease_part'] = Particular::whereIn('PARTICULAR_ID', [6, 7, 9, 10, 11])
                                        ->orderBy('PARTICULAR_ID', 'asc')
                                        ->get();
        $data['report_data'] = AccountVaucherChd::leaseRevenuePrintData($input);

        // Make PDF
        $pdf_data = View::make('admin_access.lease_report.revenue_print', $data);
        $file_name = 'NCC-'.'User'.'.pdf';

        return Helpers::makePDF($file_name, $pdf_data);
    }

    /**
     * Show data.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return json
     */
    public function leaseRevenueData(Request $request)
    {
        $input = $request->all();
        $collection = AccountVaucherChd::leaseRevenueData($input);
        $output = Datatablehelper::leaseRevenueDataList($input, $collection);

        return response()->json($output);
    }

    /**
     * Citizen money collection.
     *
     * @return \Illuminate\View\View
     */
    public function moneyCollection()
    {
        $link_url = 'money_collection'; // module url
        $data['access'] = Module::checkPrevilege($link_url); // get access info by module url

        $data['category'] = ProjectCategory::where('IS_ACTIVE', 1)->lists('CATE_NAME', 'PR_CATEGORY');
        $data['particular'] = Particular::where('IS_ACTIVE', 1)->lists('PARTICULAR_NAME', 'PARTICULAR_ID');

        return View::make('admin_access.money_collection.index')->with($data);
    }

    /**
     * Ownership change or cancel.
     *
     * @return \Illuminate\View\View
     */
    public function ownershipChange()
    {
        $link_url = 'ownership_change'; // module url
        $data['access'] = Module::checkPrevilege($link_url); // get access info by module url

        $data['category'] = ProjectCategory::where('IS_ACTIVE', 1)->lists('CATE_NAME', 'PR_CATEGORY');

        return View::make('admin_access.ownership.index')->with($data);
    }

    /**
     * Citizen ledger.
     *
     * @return \Illuminate\View\View
     */
    public function citizenLedger()
    {
        $link_url = 'citizen_ledger'; // module url
        $data['access'] = Module::checkPrevilege($link_url); // get access info by module url

        $data['category'] = ProjectCategory::where('IS_ACTIVE', 1)->lists('CATE_NAME', 'PR_CATEGORY');
        $data['particular'] = Particular::where('IS_ACTIVE', 1)->lists('PARTICULAR_NAME', 'PARTICULAR_ID');

        return View::make('admin_access.revenue.citizen_ledger')->with($data);
    }

    /**
     * Show citizen revenue data.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return json
     */
    public function citizenLedgerData(Request $request)
    {
        $input = $request->all();
        $collection = AccountVaucherChd::citizenLedgerDataTable($input);
        $output = Datatablehelper::citizenLedgerDataList($input, $collection);

        return response()->json($output);
    }

    /**
     * print citizen data by voucher no.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return PDF
     */
    public function citizenLedgerPrint(Request $request)
    {
        $input = $request->all();
        $revenue_data = AccountVaucherChd::citizenLedgerDataPrint($input);

        // Make PDF
        $pdf_data = View::make('admin_access.revenue.citizen_ledger_print', compact('revenue_data'));
        $file_name = 'NCC-Citizen-ledger'.'.pdf';

        return Helpers::makePDF($file_name, $pdf_data);
    }

    /**
     * Show application security money.
     *
     * @return \Illuminate\View\View
     */
    public function applicantSecurityMoney()
    {
        $tender = Tender::where('IS_ACTIVE', 1)->lists('TENDER_TITLE', 'TENDER_ID');

        return view('admin_access.accounts.security_refund', compact('tender'));
    }

    /**
     * Security money data.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return json
     */
    public function securityMoneyData(Request $request)
    {
        $input = $request->all();
        //$collection = SecurityRefund::getSecurityRefundData($input);
        //$output     = Datatablehelper::tenderSecurityMoneyOutput($input, $collection);

        $collection = TenderApplicant::getallListData($input);
        $output = Datatablehelper::tenderallListOutput($input, $collection);

        return response()->json($output);
    }

    /**
     * Applicant security money return.
     *
     * @param \Illuminate\Http\Request $request
     * @param string                   $ts      Tender schedule id
     * @param string                   $type    Tender applicant type(Applicant/citizen)
     * @param string                   $id      Applicant id(Applicant/citizen)
     *
     * @return json
     */
    public function returnSecurityMoney(Request $request, $ts, $type, $id)
    {
        $data['user_id'] = $id;
        $data['user_type'] = $type;
        $data['schedule_id'] = $ts;

        if ($type == 'applicant') {
            $data['applicant'] = TenderApplicant::where('TE_APP_ID', $id)->first();
            $data['applicent_district'] = District::where('DISTRICT_ID', $data['applicant']->DISTRICT_ID)->pluck('DISTRICT_ENAME');
            $data['applicent_thana'] = Thana::where('THANA_ID', $data['applicant']->THANA_ID)->pluck('THANA_ENAME');
            $data['applicant_property'] = SecurityRefund::with(['getSchedule' => function ($query) {
                $query->with(['getProject', 'getCategory', 'getProjectType', 'getProjectDetail']);
            }])->where('TE_APP_ID', $id)->where('SCHEDULE_ID', $ts)->first();
        } else {
            $data['applicant'] = Citizen::with('getTenderCitizen')->where('CITIZEN_ID', $id)->first();
            $data['applicent_district'] = District::where('DISTRICT_ID', $data['applicant']->DISTRICT_ID)->pluck('DISTRICT_ENAME');
            $data['applicent_thana'] = Thana::where('THANA_ID', $data['applicant']->THANA_ID)->pluck('THANA_ENAME');
            $data['applicant_property'] = SecurityRefund::with(['getSchedule' => function ($query) {
                $query->with(['getProject', 'getCategory', 'getProjectType', 'getProjectDetail']);
            }])->where('CITIZEN_ID', $id)->where('SCHEDULE_ID', $ts)->first();
        }
        //$data['payment_info']  = PaymentResponse::where('TRX_TRAN_NO', $data['applicant_property']->TRX_TRAN_NO)->get();
        $data['division'] = Division::lists('DIVISION_ENAME', 'DIVISION_ID');
        $data['officer'] = Officer::where('TYPE', 1)->first();
        $data['location'] = ProjectLocation::where('IS_ACTIVE', 1)->get();

        return view('admin_access.accounts.refund_letter')->with($data)->render();
    }

    /**
     * Applicant security money return.
     *
     * @param \Illuminate\Http\Request $request
     * @param string                   $uid     Applicant id(Applicant/citizen)
     * @param string                   $utype   Tender applicant type(Applicant/citizen)
     * @param string                   $sc      Tender schedule id
     *
     * @return json
     */
    public function securityMoneyUpdate(Request $request, $uid, $utype, $sc)
    {
        $data = $request->all();
        $user = Auth::user()->USER_ID;
        $refund = SecurityRefund::refundUpdate($data, $uid, $utype, $sc, $user);
        $sms = Helpers::sendSMS($refund['number'], $refund['sms']);

        return response()->json(['flag' => 1, 'message' => trans('common.save_success')]);
    }

    /**
     * Security money return letter print.
     *
     * @param \Illuminate\Http\Request $request
     * @param string                   $uid
     * @param string                   $utype
     * @param string                   $sc
     *
     * @return PDF
     */
    public function userOwnershipPrint(Request $request, $uid, $utype, $sc)
    {
        if ($utype == 'citizen') {
            $letter = TenderLetter::where('CITIZEN_ID', $uid)->where('SCHEDULE_ID', $sc)->where('PARTICULAR_ID', 15)->first();
        } else {
            $letter = TenderLetter::where('TE_APP_ID', $uid)->where('SCHEDULE_ID', $sc)->where('PARTICULAR_ID', 15)->first();
        }
        $pdf_data = view('admin_access.accounts.refund_print', compact('letter'))->render();
        $file_name = 'NCC-'.'refund-letter'.'.pdf';

        return Helpers::makePDF($file_name, $pdf_data);
    }
}
