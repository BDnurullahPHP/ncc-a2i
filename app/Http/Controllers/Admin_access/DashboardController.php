<?php

namespace App\Http\Controllers\Admin_access;

use App\Http\Controllers\Controller;
use App\Models\AccountVaucherChd;
use App\Models\AccountVaucherMst;
use App\Models\AccountVnLedger;
use App\Models\ApplicantProperty;
use App\Models\CitizenApplication;
use App\Models\CitizenProperty;
use App\Models\Land;
use App\Models\Project;
use App\Models\ProjectDetails;
use App\Models\Tender;
use App\Models\TenderSchedule;
use App\User;
use Auth;
//use Redis;
use DB;
use Entrust;
use Illuminate\Http\Request;
use Response;

/**
 * Class     DashboardController.
 *
 * @author   Abdul awal <abdulawal@atilimited.net>
 */
class DashboardController extends Controller
{
    /**
     * Show the admin dashboard.
     *
     * @return \Illuminate\View\View
     */
    public function getIndex()
    {
        $data['title'] = 'Dashboard';

        if (Entrust::hasRole('admin')) { // Admin dashboard
            $page = 'admin';
            $data = $this->roleAdmin();
        } elseif (Entrust::hasRole('project-manager')) { // Project dashboard
            $page = 'project';
            $data = $this->roleProjectManager();
        } elseif (Entrust::hasRole('tender-manager')) { // Tender dashboard
            $page = 'tender';
            $data = $this->roleTenderManager();
        } elseif (Entrust::hasRole('lease-manager')) { // Lease dashboard
            $page = 'lease';
            $data = $this->roleLeaseManager();
        } elseif (Entrust::hasRole('account-manager')) { // Accounts dashboard
            $page = 'accounts';
            $data = $this->roleAccountsManager();
        } elseif (Entrust::hasRole('citizen')) { // Citizen dashboard
            $page = 'citizen';
            $data = $this->roleCitizen();
        } else {
            $page = 'citizen';
        }
        //echo "<pre>";print_r($data['revenue_pie']);exit;

        return view('admin_access.dashboard.'.$page)->with($data);
    }

    /**
     * Admin dashboard operations.
     *
     * @return array
     */
    private function roleAdmin()
    {
        $data['user'] = Auth::user()->load(['getDetails', 'getCitizen']);
        $data['revenue_cr'] = AccountVnLedger::where('TRX_CODE_ID', 4)->sum('CR_AMT');
        $data['revenue_dr'] = AccountVnLedger::where('TRX_CODE_ID', 5)->sum('DR_AMT');
        //Start flat
        $data['ready_flat'] = ProjectDetails::with(['getProject', 'getType'])->where('PR_CATEGORY', 22)->where('IS_BUILT', 1)->take(5)->get();
        $data['construction_flat'] = ProjectDetails::with(['getProject', 'getType'])->where('PR_CATEGORY', 22)->where('IS_BUILT', 0)->take(5)->get();
        $data['allotment_flat'] = CitizenProperty::with(['getProject', 'getCitizen'=>function ($query) {
            $query->with('getTenderCitizen');
        },
                                            'projectDetails'=> function ($query) {
                                                $query->with('getType');
                                            },
                                            ])->where('PR_CATEGORY', 22)->where('IS_LEASE', 0)->take(5)->get();
        //End flat
        //Start shop
        $data['ready_shop'] = ProjectDetails::with(['getProject', 'getType'])->where('PR_CATEGORY', 21)->where('IS_BUILT', 1)->get();
        $data['construction_shop'] = ProjectDetails::with(['getProject', 'getType'])->where('PR_CATEGORY', 21)->where('IS_BUILT', 0)->get();
        $data['allotment_shop'] = CitizenProperty::with(['getProject', 'getCitizen'=>function ($query) {
            $query->with('getTenderCitizen');
        },
                                            'projectDetails'=> function ($query) {
                                                $query->with('getType');
                                            },
                                            ])->where('PR_CATEGORY', 21)->where('IS_LEASE', 0)->get();
        //End shop
        //Start space
        $data['ready_space'] = ProjectDetails::with(['getProject', 'getType'])->where('PR_CATEGORY', 23)->where('IS_BUILT', 1)->get();
        $data['construction_space'] = ProjectDetails::with(['getProject', 'getType'])->where('PR_CATEGORY', 23)->where('IS_BUILT', 0)->get();
        $data['allotment_space'] = CitizenProperty::with(['getProject', 'getCitizen'=>function ($query) {
            $query->with('getTenderCitizen');
        },
                                            'projectDetails'=> function ($query) {
                                                $query->with('getType');
                                            },
                                            ])->where('PR_CATEGORY', 23)->where('IS_LEASE', 0)->get();
        //End space
        //Start Lease
        $data['ready_lease'] = Project::where('IS_LEASE', 1)->get();
        $data['lease'] = CitizenProperty::with(['getProject', 'getCitizen'=>function ($query) {
            $query->with('getTenderCitizen');
        },
                                                ])
                                                ->where('PR_CATEGORY', 24)->where('IS_LEASE', 1)->get();
        //End Lease
        $data['land_ousted'] = Land::where('LAND_OWNER', 1)->sum('OUSTED_LAND');
        $data['land_possession'] = Land::where('LAND_OWNER', 1)->sum('POSSESSION_LAND');
        $data['rev_years'] = AccountVaucherMst::getYearRange();

        // Redis
        // $redis = Redis::connection();
        // $redis->publish('message', 'Test message');

        return $data;
    }

    /**
     * Project manager dashboard operations.
     *
     * @return array
     */
    private function roleProjectManager()
    {
        $data['project'] = Project::where('IS_ACTIVE', 1)->where('IS_LEASE', 0)->count('PROJECT_ID');
        $data['shop'] = ProjectDetails::where('IS_ACTIVE', 1)->where('PR_CATEGORY', 21)->count('PROJECT_ID');
        $data['flat'] = ProjectDetails::where('IS_ACTIVE', 1)->where('PR_CATEGORY', 22)->count('PROJECT_ID');
        $data['space'] = ProjectDetails::where('IS_ACTIVE', 1)->where('PR_CATEGORY', 23)->count('PROJECT_ID');

        //Start flat
        $data['ready_flat'] = ProjectDetails::with(['getProject', 'getType'])->where('PR_CATEGORY', 22)->where('IS_BUILT', 1)->take(5)->get();
        $data['construction_flat'] = ProjectDetails::with(['getProject', 'getType'])->where('PR_CATEGORY', 22)->where('IS_BUILT', 0)->take(5)->get();
        $data['allotment_flat'] = CitizenProperty::with(['getProject', 'getCitizen'=>function ($query) {
            $query->with('getTenderCitizen');
        },
                                            'projectDetails'=> function ($query) {
                                                $query->with('getType');
                                            },
                                            ])->where('PR_CATEGORY', 22)->where('IS_LEASE', 0)->take(5)->get();
        //End flat
        //Start shop
        $data['ready_shop'] = ProjectDetails::with(['getProject', 'getType'])->where('PR_CATEGORY', 21)->where('IS_BUILT', 1)->take(5)->get();
        $data['construction_shop'] = ProjectDetails::with(['getProject', 'getType'])->where('PR_CATEGORY', 21)->where('IS_BUILT', 0)->take(5)->get();
        $data['allotment_shop'] = CitizenProperty::with(['getProject', 'getCitizen'=>function ($query) {
            $query->with('getTenderCitizen');
        },
                                            'projectDetails'=> function ($query) {
                                                $query->with('getType');
                                            },
                                            ])->where('PR_CATEGORY', 21)->where('IS_LEASE', 0)->take(5)->get();
        //End shop
        //Start space
        $data['ready_space'] = ProjectDetails::with(['getProject', 'getType'])->where('PR_CATEGORY', 23)->where('IS_BUILT', 1)->take(5)->get();
        $data['construction_space'] = ProjectDetails::with(['getProject', 'getType'])->where('PR_CATEGORY', 23)->where('IS_BUILT', 0)->take(5)->get();
        $data['allotment_space'] = CitizenProperty::with(['getProject', 'getCitizen'=>function ($query) {
            $query->with('getTenderCitizen');
        },
                                            'projectDetails'=> function ($query) {
                                                $query->with('getType');
                                            },
                                            ])->where('PR_CATEGORY', 23)->where('IS_LEASE', 0)->take(5)->get();
        //End space
        $data['project_info'] = Project::with(['getProjectDetails'=> function ($query) {
            $query->select('*', DB::raw('count(PR_CATEGORY) AS total_category'))
                                                ->groupBy(['PR_CATEGORY', 'PROJECT_ID']);
        }])
                              ->where('IS_ACTIVE', 1)
                              ->where('IS_LEASE', 0)
                              ->get();

        return $data;
    }

    /**
     * Tender manager dashboard operations.
     *
     * @return array
     */
    private function roleTenderManager()
    {
        $data['tender'] = Tender::where('IS_ACTIVE', 1)->where('IS_LEASE', 0)->count('TENDER_ID');
        $data['current_tender'] = Tender::leftJoin('tender_date_time', 'tender_date_time.TENDER_ID', '=', 'tender.TENDER_ID')
                                ->where('tender_date_time.LAST_SELLING_DT_TO', '>=', date('Y-m-d').' 00:00:00')
                                ->where('tender.IS_LEASE', 0)->count('tender.TENDER_ID');
        //Current tender revenue by selling schedule
        $data['applicant'] = ApplicantProperty::where('IS_LEASE', 0)->where('IS_SELECTED', 1)->count('APP_PRO_ID');
        $data['citizen'] = CitizenApplication::where('IS_LEASE', 0)->where('IS_SELECTED', 1)->count('CITIZEN_APP_ID');

        $data['applicant_final'] = ApplicantProperty::where('IS_LEASE', 0)->where('IS_SELECTED', 2)->count('APP_PRO_ID');
        $data['citizen_final'] = CitizenApplication::where('IS_LEASE', 0)->where('IS_SELECTED', 2)->count('CITIZEN_APP_ID');
        // Tender Schedule List
        $data['schedule_info'] = TenderSchedule::with('getProject', 'getCategory', 'getProjectDetail', 'getCategoryParticulars', 'getProjectType', 'getTenderParticulars')
                                        ->leftJoin('tender_date_time', 'tender_date_time.TENDER_ID', '=', 'tender_schedule.TENDER_ID')
                                        ->leftJoin('tender', 'tender_schedule.TENDER_ID', '=', 'tender.TENDER_ID')
                                        ->where('tender_date_time.LAST_SELLING_DT_TO', '>=', date('Y-m-d').' 00:00:00')
                                        ->where('tender.IS_LEASE', 0)
                                        ->take(5)
                                        ->get();

        return $data;
    }

    /**
     * Lease manager dashboard operations.
     *
     * @return array
     */
    private function roleLeaseManager()
    {
        $data['lease'] = Tender::where('IS_ACTIVE', 1)->where('IS_LEASE', 1)->count('TENDER_ID');
        $data['current_lease'] = Tender::leftJoin('tender_date_time', 'tender_date_time.TENDER_ID', '=', 'tender.TENDER_ID')
                                ->where('tender_date_time.LAST_SELLING_DT_TO', '>=', date('Y-m-d').' 00:00:00')
                                ->where('tender.IS_LEASE', 1)->count('tender.TENDER_ID');
        //Current lease primary selected applicant
        $data['applicant'] = ApplicantProperty::where('IS_LEASE', 1)->where('IS_SELECTED', 1)->count('APP_PRO_ID');
        $data['citizen'] = CitizenApplication::where('IS_LEASE', 1)->where('IS_SELECTED', 1)->count('CITIZEN_APP_ID');

        $data['applicant_final'] = ApplicantProperty::where('IS_LEASE', 1)->where('IS_SELECTED', 2)->count('APP_PRO_ID');
        $data['citizen_final'] = CitizenApplication::where('IS_LEASE', 1)->where('IS_SELECTED', 2)->count('CITIZEN_APP_ID');
        // lease Schedule List
        $data['schedule_info'] = TenderSchedule::with('getProject', 'getCategory', 'getTenderParticulars')
                                        ->leftJoin('tender_date_time', 'tender_date_time.TENDER_ID', '=', 'tender_schedule.TENDER_ID')
                                        ->leftJoin('tender', 'tender_schedule.TENDER_ID', '=', 'tender.TENDER_ID')
                                        ->where('tender_date_time.LAST_SELLING_DT_TO', '>=', date('Y-m-d').' 00:00:00')
                                        ->where('tender.IS_LEASE', 1)
                                        ->take(5)
                                        ->get();

        return $data;
    }

    /**
     * Accounts manager dashboard operations.
     *
     * @return array
     */
    private function roleAccountsManager()
    {
        $data['total_revenue_cr'] = AccountVnLedger::where('TRX_CODE_ID', 4)->sum('CR_AMT');
        $data['total_revenue_dr'] = AccountVnLedger::where('TRX_CODE_ID', 5)->sum('DR_AMT');
        $data['category_total'] = TenderSchedule::categoryCount();
        $data['category_revenue'] = AccountVnLedger::categoryRevenue();
        $data['revenue_pie'] = AccountVnLedger::categoryRevenuePie();

        return $data;
    }

    /**
     * Citizen dashboard operations.
     *
     * @return array
     */
    private function roleCitizen()
    {
        $citizen_id = Auth::user()->load('getCitizen')->getCitizen->CITIZEN_ID;
        $data['current_tender'] = Tender::getCurrentTenderCount();
        $data['current_lease'] = Tender::getCurrentLeaseCount();
        $data['rent_years'] = AccountVaucherMst::getRentYearRange($citizen_id);
        $data['is_tender'] = CitizenProperty::isTenderProperty($citizen_id);
        $data['ctz_service'] = CitizenProperty::citizenServices($citizen_id);
        $data['rent_payment'] = AccountVaucherChd::citizenRentPaymentData($citizen_id);
        $data['cat_property'] = CitizenProperty::categoryProperty($citizen_id);
        //echo "<pre>";print_r($data['cat_property']);exit;

        return $data;
    }

    /**
     * Admin chart data.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return json
     */
    public function getChartData(Request $request)
    {
        $year = $request->get('year');
        $revenue = AccountVaucherChd::yearlyRevenue($year);

        return response()->json(['flug' => 1, 'data' => $revenue]);
    }

    /**
     * Citizen chart data.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return json
     */
    public function rentChartData(Request $request)
    {
        $year = $request->get('year');
        $citizen = Auth::user()->load('getCitizen')->getCitizen->CITIZEN_ID;
        $revenue = AccountVaucherChd::citizenRentData($year, $citizen);

        return response()->json(['flug' => 1, 'data' => $revenue]);
    }

    /**
     * Accounts revenue chart data.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return json
     */
    public function revenueChartData(Request $request)
    {
        $month = date('Y-m', strtotime($request->get('month')));
        $revenue = AccountVaucherChd::monthlyRevenue($month);

        return response()->json(['flug' => 1, 'data' => $revenue]);
    }

    /**
     * Tender application count data by month chart data.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return json
     */
    public function tenderApplicationData(Request $request)
    {
        $month = date('Y-m', strtotime($request->get('month')));
        $is_lease = $request->get('is_lease');
        $applicant = CitizenApplication::tenderApplication($month, $is_lease);

        return response()->json(['flug' => 1, 'data' => $applicant]);
    }
}
