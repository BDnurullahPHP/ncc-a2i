<?php

namespace App\Http\Controllers\Applicant;

use App\Http\Controllers\Controller;
use App\Models\AccountVaucherChd;
use App\Models\AccountVaucherMst;
use App\Models\ApplicantProperty;
use App\Models\Citizen;
use App\Models\CitizenApplication;
use App\Models\CitizenProperty;
use App\Models\District;
use App\Models\Division;
use App\Models\Officer;
use App\Models\PaymentResponse;
use App\Models\Project;
use App\Models\ProjectLocation;
use App\Models\Tender;
use App\Models\TenderApplicant;
use App\Models\TenderInstallment;
use App\Models\TenderLetter;
use App\Models\TenderSchedule;
use App\Models\Thana;
use App\Ncc\Datatablehelper;
use App\Ncc\Helpers;
use App\User;
use Auth;
use Illuminate\Http\Request;
use Response;

/**
 * Class     ApplicantController.
 *
 * @author   Abdul awal <abdulawal@atilimited.net>
 */
class ApplicantController extends Controller
{
    /**
     * Show the tender applicant dashboard.
     *
     * @return \Illuminate\View\View
     */
    public function getIndex()
    {
        $data['title'] = 'Dashboard';
        $data['user'] = Auth::user('user');
        $data['total_apply'] = ApplicantProperty::where('TE_APP_ID', $data['user']->TE_APP_ID)
                                                ->where('IS_ACTIVE', 1)
                                                ->count();
        $data['total_approve'] = ApplicantProperty::where('TE_APP_ID', $data['user']->TE_APP_ID)
                                                    ->where('IS_SELECTED', '!=', 0)
                                                    ->where('IS_ACTIVE', 1)
                                                    ->count();
        $data['current_tender'] = Tender::getCurrentTenderCount();
        $data['current_lease'] = Tender::getCurrentLeaseCount();

        return view('applicant.dashboard')->with($data);
    }

    /**
     * Show applicant payment history.
     *
     * @return \Illuminate\View\View
     */
    public function paymentHistory()
    {
        return view('applicant.payment_history');
    }

    /**
     * Get tender applicant payment history data.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return json
     */
    public function paymentData(Request $request)
    {
        $input = $request->all();
        $voucher = AccountVaucherMst::where('TE_APP_ID', Auth::user('user')->TE_APP_ID)->get();
        $collection = PaymentResponse::getHistoryData($input, $voucher);
        $output = Datatablehelper::applicantPaymentOutput($input, $collection);

        return response()->json($output);
    }

    /**
     * Show application form history.
     *
     * @return \Illuminate\View\View
     */
    public function applicationFormHistory()
    {
        $applicant_id = Auth::user('user')->TE_APP_ID;
        $project = ApplicantProperty::with(['getSchedule' => function ($query) {
            $query->with(['getProject', 'getCategory', 'getProjectType', 'getProjectDetail']);
        }])
                                    ->where('TE_APP_ID', $applicant_id)
                                    ->where('IS_ACTIVE', 1)
                                    ->get();

        return view('applicant.form_history', compact('project'));
    }

    /**
     * Get application form data.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\View\View
     */
    public function getFormData(Request $request)
    {
        $schedule_id = $request->get('schedule_id');
        $data['user'] = Auth::user('user');
        $data['app_project'] = TenderSchedule::with(['getProject'])
                                                ->where('SCHEDULE_ID', $schedule_id)
                                                ->first();
        $data['tender_name'] = Tender::where('TENDER_ID', $data['app_project']->TENDER_ID)
                                        ->pluck('TENDER_TITLE');
        $data['user_district'] = District::where('DISTRICT_ID', $data['user']->DISTRICT_ID)
                                            ->pluck('DISTRICT_ENAME');
        $data['user_thana'] = Thana::where('THANA_ID', $data['user']->THANA_ID)
                                            ->pluck('THANA_ENAME');

        return view('applicant.app_form_data')->with($data)->render();
    }

    /**
     * Show application notification history.
     *
     * @return \Illuminate\View\View
     */
    public function applicationNotification()
    {
        $data['user'] = Auth::user('user');
        $data['project'] = ApplicantProperty::with(['getSchedule' => function ($query) {
            $query->with(['getProject', 'getCategory', 'getProjectType', 'getProjectDetail']);
        }])
                                            ->where('TE_APP_ID', $data['user']->TE_APP_ID)
                                            ->where('IS_ACTIVE', 1)
                                            ->get();

        return view('applicant.notification')->with($data);
    }

    /**
     * Get applicant notification data.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\View\View
     */
    public function appNotificationData(Request $request)
    {
        $schedule_id = $request->get('schedule_id');
        $data['applicant'] = Auth::user('user');
        $data['te_schedule'] = TenderSchedule::with('getProject')
                                                ->where('SCHEDULE_ID', $schedule_id)
                                                ->first();
        $data['app_property'] = ApplicantProperty::where('TE_APP_ID', $data['applicant']->TE_APP_ID)
                                                    ->where('SCHEDULE_ID', $schedule_id)
                                                    ->where('IS_ACTIVE', 1)
                                                    ->first();
        $applicant_voucher = AccountVaucherMst::where('TE_APP_ID', $data['applicant']->TE_APP_ID)
                                                ->lists('VOUCHER_NO');
        foreach ($applicant_voucher as $key => $apv) {
            $app_voucher[$key] = $apv;
        }

        if ($data['app_property']->IS_LEASE == 1) {
            $data['app_voucher_chd'] = AccountVaucherChd::leaseVoucherChd($app_voucher, $schedule_id);
        } else {
            $data['app_voucher_chd'] = AccountVaucherChd::tenderVoucherChd($app_voucher, $schedule_id);
            $data['installment'] = TenderInstallment::where('TE_APP_ID', $data['applicant']->TE_APP_ID)
                                                    ->where('SCHEDULE_ID', $schedule_id)
                                                    ->get();
        }

        return view('applicant.notification_data', compact('schedule_id'))->with($data)->render();
    }

    /**
     * Get user payment data.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\View\View
     */
    public function userPaymentInfo(Request $request)
    {
        $input = $request->all();
        $type_data = explode('_', $input['schedule']);
        $schedule_id = $type_data[2];
        $data['user_id'] = $type_data[0];
        $data['te_schedule'] = TenderSchedule::with('getProject')
                                                ->where('SCHEDULE_ID', $schedule_id)
                                                ->where('PR_CATEGORY', $input['category'])
                                                ->first();
        if ($type_data[1] == 'CA' || $type_data[1] == 'CP') {
            $data['ctz_data'] = Citizen::with('getTenderCitizen')->where('CITIZEN_ID', $type_data[0])->first();
            $data['applicant_name'] = $data['ctz_data']->getTenderCitizen->FULL_NAME;
            $data['applicant_fname'] = $data['ctz_data']->SPOUSE_NAME;
            $division = Division::where('DIVISION_ID', $data['ctz_data']->DIVISION_ID)->pluck('DIVISION_ENAME');
            $district = District::where('DISTRICT_ID', $data['ctz_data']->DISTRICT_ID)->pluck('DISTRICT_ENAME');
            $thana = Thana::where('THANA_ID', $data['ctz_data']->THANA_ID)->pluck('THANA_ENAME');
            $data['applicant_address'] = 'হোল্ডিং: '.$data['ctz_data']->HOLDING_NO.', রোড: '.$data['ctz_data']->ROAD_NO.', থানা: '.$thana.', জেলা: '.$district.', বিভাগ: '.$division;
            if (empty($input['particular']) || $input['particular'] == 8) {
                $data['installment'] = TenderInstallment::where('CITIZEN_ID', $type_data[0])
                                                        ->where('SCHEDULE_ID', $schedule_id)
                                                        ->get();
            }

            if ($type_data[1] == 'CP') {
                $data['app_property'] = CitizenProperty::where('CITIZEN_ID', $type_data[0])
                                                    ->where('SCHEDULE_ID', $schedule_id)
                                                    ->where('PR_CATEGORY', $input['category'])
                                                    ->where('IS_ACTIVE', 1)
                                                    ->first();
            } else {
                $data['app_property'] = CitizenApplication::where('CITIZEN_ID', $type_data[0])
                                                    ->where('SCHEDULE_ID', $schedule_id)
                                                    ->where('PR_CATEGORY', $input['category'])
                                                    ->where('IS_ACTIVE', 1)
                                                    ->first();
            }
            $applicant_voucher = AccountVaucherMst::where('CITIZEN_ID', $type_data[0])->lists('VOUCHER_NO');
        } else {
            $data['applicant_data'] = TenderApplicant::where('TE_APP_ID', $type_data[0])->first();
            $data['applicant_name'] = $data['applicant_data']->APPLICANT_NAME;
            $data['applicant_fname'] = $data['applicant_data']->SPOUSE_NAME;
            $division = Division::where('DIVISION_ID', $data['applicant_data']->DIVISION_ID)->pluck('DIVISION_ENAME');
            $district = District::where('DISTRICT_ID', $data['applicant_data']->DISTRICT_ID)->pluck('DISTRICT_ENAME');
            $thana = Thana::where('THANA_ID', $data['applicant_data']->THANA_ID)->pluck('THANA_ENAME');
            $data['applicant_address'] = 'হোল্ডিং: '.$data['applicant_data']->HOLDING_NO.', রোড: '.$data['applicant_data']->ROAD_NO.', থানা: '.$thana.', জেলা: '.$district.', বিভাগ: '.$division;

            $data['app_property'] = ApplicantProperty::where('TE_APP_ID', $type_data[0])
                                                        ->where('SCHEDULE_ID', $schedule_id)
                                                        ->where('PR_CATEGORY', $input['category'])
                                                        ->where('IS_ACTIVE', 1)
                                                        ->first();
            if (empty($input['particular']) || $input['particular'] == 8) {
                $data['installment'] = TenderInstallment::where('TE_APP_ID', $type_data[0])
                                                        ->where('SCHEDULE_ID', $schedule_id)
                                                        ->get();
            }
            $applicant_voucher = AccountVaucherMst::where('TE_APP_ID', $type_data[0])->lists('VOUCHER_NO');
        }

        foreach ($applicant_voucher as $key => $apv) {
            $app_voucher[$key] = $apv;
        }

        $data['app_voucher_chd'] = AccountVaucherChd::tenderVChdParticular($app_voucher, $schedule_id, $input);

        return view('applicant.payment_information', compact('schedule_id'))->with($data)->render();
    }

    /**
     * Show applicant installment history.
     *
     * @return \Illuminate\View\View
     */
    public function installmentHistory()
    {
        $applicant = Auth::user('user');
        $data['schedule'] = TenderInstallment::with(['getSchedule' => function ($query) {
            $query->with(['getProject', 'getCategory', 'getProjectType', 'getProjectDetail']);
        }])
                                            ->where('TE_APP_ID', $applicant->TE_APP_ID)
                                            ->groupBy('SCHEDULE_ID')
                                            ->get();

        return view('applicant.installment_history', $data)->render();
    }

    /**
     * Show installment data by schedule id.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\View\View
     */
    public function installmentData(Request $request)
    {
        $schedule_id = $request->get('schedule_id');
        $data['applicant'] = Auth::user('user');
        $data['app_installment'] = TenderInstallment::where('TE_APP_ID', $data['applicant']->TE_APP_ID)
                                                    ->where('SCHEDULE_ID', $schedule_id)
                                                    ->orderBy('INSTALLMENT', 'asc')
                                                    ->get();

        return view('applicant.installment_data')->with($data)->render();
    }

    /**
     * Get user ownership data.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\View\View
     */
    public function userOwnershipInfo(Request $request)
    {
        $input = $request->all();
        $type_data = explode('_', $input['schedule']);
        $data['type'] = $input['type'];

        if ($type_data[1] == 'CA' || $type_data[1] == 'CP') {
            $data['ctz_data'] = Citizen::with('getTenderCitizen')->where('CITIZEN_ID', $type_data[0])->first();
            $data['applicant_name'] = $data['ctz_data']->getTenderCitizen->FULL_NAME;
            $data['applicant_fname'] = $data['ctz_data']->SPOUSE_NAME;
            $division = Division::where('DIVISION_ID', $data['ctz_data']->DIVISION_ID)->pluck('DIVISION_ENAME');
            $district = District::where('DISTRICT_ID', $data['ctz_data']->DISTRICT_ID)->pluck('DISTRICT_ENAME');
            $thana = Thana::where('THANA_ID', $data['ctz_data']->THANA_ID)->pluck('THANA_ENAME');
            $data['applicant_address'] = 'হোল্ডিং: '.$data['ctz_data']->HOLDING_NO.', রোড: '.$data['ctz_data']->ROAD_NO.', থানা: '.$thana.', জেলা: '.$district.', বিভাগ: '.$division;
            $data['parameters'] = [$type_data[0], 'citizen', $type_data[2], $input['type']];
        } else {
            $data['applicant_data'] = TenderApplicant::where('TE_APP_ID', $type_data[0])->first();
            $data['applicant_name'] = $data['applicant_data']->APPLICANT_NAME;
            $data['applicant_fname'] = $data['applicant_data']->SPOUSE_NAME;
            $division = Division::where('DIVISION_ID', $data['applicant_data']->DIVISION_ID)->pluck('DIVISION_ENAME');
            $district = District::where('DISTRICT_ID', $data['applicant_data']->DISTRICT_ID)->pluck('DISTRICT_ENAME');
            $thana = Thana::where('THANA_ID', $data['applicant_data']->THANA_ID)->pluck('THANA_ENAME');
            $data['applicant_address'] = 'হোল্ডিং: '.$data['applicant_data']->HOLDING_NO.', রোড: '.$data['applicant_data']->ROAD_NO.', থানা: '.$thana.', জেলা: '.$district.', বিভাগ: '.$division;
            $data['parameters'] = [$type_data[0], 'applicant', $type_data[2], $input['type']];
        }

        return view('applicant.owner_information', compact('input'))->with($data)->render();
    }

    /**
     * Get user ownership letter.
     *
     * @param \Illuminate\Http\Request $request
     * @param string                   $uid
     * @param string                   $utype
     * @param string                   $sc
     * @param string                   $type
     *
     * @return \Illuminate\View\View
     */
    public function userOwnershipLetter(Request $request, $uid, $utype, $sc, $type)
    {
        $data['user_id'] = $uid;
        $data['user_type'] = $utype;
        $data['schedule_id'] = $sc;
        $data['type'] = $type;

        if ($utype == 'applicant') {
            $data['applicant'] = TenderApplicant::where('TE_APP_ID', $uid)->first();
            $data['applicent_district'] = District::where('DISTRICT_ID', $data['applicant']->DISTRICT_ID)->pluck('DISTRICT_ENAME');
            $data['applicent_thana'] = Thana::where('THANA_ID', $data['applicant']->THANA_ID)->pluck('THANA_ENAME');
            $data['property_price'] = ApplicantProperty::where('TE_APP_ID', $uid)->where('SCHEDULE_ID', $sc)->pluck('BID_AMOUNT');
        } else {
            $data['applicant'] = Citizen::with('getTenderCitizen')->where('CITIZEN_ID', $uid)->first();
            $data['applicent_district'] = District::where('DISTRICT_ID', $data['applicant']->DISTRICT_ID)->pluck('DISTRICT_ENAME');
            $data['applicent_thana'] = Thana::where('THANA_ID', $data['applicant']->THANA_ID)->pluck('THANA_ENAME');
            $data['property_price'] = CitizenApplication::where('CITIZEN_ID', $uid)->where('SCHEDULE_ID', $sc)->pluck('BID_AMOUNT');
        }

        $data['division'] = Division::lists('DIVISION_ENAME', 'DIVISION_ID');
        $data['officer'] = Officer::where('TYPE', 1)->first();
        $data['location'] = ProjectLocation::where('IS_ACTIVE', 1)->get();

        return view('admin_access.ownership.letter')->with($data)->render();
    }

    /**
     * User ownership change or cancel.
     *
     * @param \Illuminate\Http\Request $request
     * @param string                   $uid
     * @param string                   $utype
     * @param string                   $sc
     * @param string                   $type
     *
     * @return \Illuminate\View\View
     */
    public function userOwnershipSave(Request $request, $uid, $utype, $sc, $type)
    {
        $data = $request->all();
        $user = Auth::user()->USER_ID;
        if ($type == 1) {
            $ownership = AccountVaucherChd::userOwnershipChange($data, $uid, $utype, $sc, $type, $user);
        } else {
            $ownership = AccountVaucherChd::userOwnershipCancel($data, $uid, $utype, $sc, $type, $user);
        }
        $letter = TenderLetter::ownershipLetter($data, $uid, $utype, $sc, $type, $user);

        return response()->json(['flag' => 1, 'message' => trans('common.save_success')]);
    }

    /**
     * User ownership change or cancel letter print.
     *
     * @param \Illuminate\Http\Request $request
     * @param string                   $uid
     * @param string                   $utype
     * @param string                   $sc
     * @param string                   $type
     *
     * @return PDF
     */
    public function userOwnershipPrint(Request $request, $uid, $utype, $sc, $type)
    {
        $particular = $type == 1 ? 13 : 14;
        if ($utype == 'citizen') {
            $letter = TenderLetter::where('CITIZEN_ID', $uid)->where('SCHEDULE_ID', $sc)->where('PARTICULAR_ID', $particular)->first();
        } else {
            $letter = TenderLetter::where('TE_APP_ID', $id)->where('SCHEDULE_ID', $ts)->where('PARTICULAR_ID', $particular)->first();
        }
        $pdf_data = view('admin_access.ownership.letter_print', compact('letter'))->render();
        $file_name = 'NCC-'.'ownership-letter'.'.pdf';

        return Helpers::makePDF($file_name, $pdf_data);
    }
}
