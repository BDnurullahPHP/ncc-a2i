<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Auth;
use Config;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Http\Request;
use Schema;
use Session;
use Validator;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'getLogout']);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param array $data
     *
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name'     => 'required|max:255',
            'email'    => 'required|email|max:255|unique:users',
            'password' => 'required|confirmed|min:6',
        ]);
    }

    /**
     * Get a validator for an incoming request.
     *
     * Tender applicant login operation
     *
     * @param array $request
     *
     * @return Response
     */
    public function userAuthentication(Request $request)
    {
        //Start API token refresh eksheba
        $new_token = Session::get('new_token');
        if ($new_token) {
            $refresh = json_decode(file_get_contents('http://sandbox.eksheba.gov.bd/api/refresh/'.$new_token), true);
        }
        //End API token refresh
        if (Session::has('applicant')) {
            $user_data = Session::get('applicant');
            $email = $user_data['email'];
            $password = $user_data['password'];
            Session::forget('applicant');
        } else {
            $email = $request->input('email');
            $password = $request->input('password');
            $this->validate($request, ['email' => 'required', 'password' => 'required']);
        }

        if (Session::has('citizen') || Session::has('applicant_exist')) {
            Session::forget('applicant');
        }

        if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $field = 'email';
        } elseif (preg_match("/^(?:\+?88)?01[15-9]\d{8}$/", $email)) {
            $field = 'mobile_no';
        } else {
            $field = 'username';
        }

        $remembe = !empty($request->get('remember')) ? true : false;
        $table_conf = Config::get('auth')['multi'];

        if (Auth::attempt('admin', [$field => $email, 'password' => $password, 'is_active' => 1], $remembe)) {
            return redirect()->route('admin_dashboard');
        } elseif (Schema::hasColumn($table_conf['user']['table'], $field) && Auth::attempt('user', [$field => $email, 'password' => $password, 'is_active' => 1], false)) {
            return redirect()->route('applicant_dashboard');
        } elseif (Auth::attempt('user', ['applicant_phone' => $email, 'password' => $password, 'is_active' => 1], false)) {
            return redirect()->route('applicant_dashboard');
        } else {
            Session::flash('error', 'Oops! Something went wrong. Please try again later!!');

            return redirect()->back();
        }
    }

    /**
     * End session for active user.
     *
     * @return Response
     */
    public function getLogout()
    {
        //Start API token logout eksheba
        $new_token = Session::get('new_token');
        if ($new_token) {
            $logout = json_decode(file_get_contents('http://sandbox.eksheba.gov.bd/api/logout/'.$new_token), true);
            if ($logout['status']) {
                Auth::logout();
                Session::flush();

                return redirect()->to($logout['data']['redirect_url']);
            } else {
                Auth::logout();
                Session::flush();

                return redirect()->route('home');
            }
        } else {//Else Part is Original
            Auth::logout();
            Session::flush();
            //End API token logout eksheba

            return redirect()->route('home');
        }
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param array $data
     *
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
            'name'     => $data['name'],
            'email'    => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }
}
