<?php

/*
  |--------------------------------------------------------------------------
  | Application Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register all of the routes for an application.
  | It's a breeze. Simply tell Laravel the URIs it should respond to
  | and give it the controller to call when that URI is requested.
  |
 */

Route::get('php-info', function () {
    echo phpinfo();
});

// Public portal pages @author  Abdul Awal <abdulawal@atilimited.net>
//This is API route eksheba
Route::get('api/{token}', ['as' => 'token_url', 'uses' => 'Portal\HomeController@getIndexUrl']);
Route::get('/', ['as' => 'home', 'uses' => 'Portal\HomeController@getIndex']);
Route::get('get-register', ['as' => 'get_register', 'uses' => 'Portal\HomeController@getRegister']);
Route::get('get-tender', ['as' => 'get_tender', 'uses' => 'Portal\HomeController@getTender']);
Route::get('{type}/tender-condition/{id}/{sc}', ['as' => 'get_tender_conditions', 'uses' => 'Portal\HomeController@getTenderConditions']);
Route::get('tender/tender-print/{id}', ['as' => 'tender_print_portal', 'uses' => 'Admin_access\TenderController@tenderPrint']);
Route::get('get-lease', ['as' => 'get_lease', 'uses' => 'Portal\HomeController@getLease']);
Route::get('lease-tender-data', ['as' => 'lease_tenderschedule_data', 'uses' => 'Portal\HomeController@leaseTenderData']);
Route::get('tender/tender-view/{id}', ['as' => 'tender_view', 'uses' => 'Portal\HomeController@tenderView']);
Route::get('lease/tender-view/{id}', ['as' => 'lease_tender_view', 'uses' => 'Portal\HomeController@leaseTenderView']);
Route::get('category-conditions/{id}', ['as' => 'category_conditions', 'uses' => 'Admin_access\TenderController@categoryConditions']);
Route::get('applicant-view/{type}/{id}/{sc}', ['as' => 'tender_applicant_view', 'uses' => 'Portal\HomeController@applicantDetails']);

//Tender Appplication Form @author Sourov <rokibuzzaman@atilimited.net>
Route::get('lease-tender-purchase-time/{id}', ['as' => 'get_lease_tender', 'uses' => 'Portal\HomeController@getLeaseTender']);

//Route::get('lease/application-form', array("as" => "get_lease_tender_application_form", 'uses' => 'Portal\HomeController@getTenderApplicationForm'));
//Route::get('tender/application-form-edit/{id}', array("as" => "get_tender_application_form_edit", 'uses' => 'Portal\HomeController@getTenderApplicationFormEdit'));
Route::get('tender/tender-information/{type}', ['as' => 'tenderschedule_data', 'uses' => 'Portal\HomeController@tenderscheduleData']);
Route::get('get-district', ['as' => 'get_district', 'uses' => 'Portal\HomeController@getdistrict']);
Route::get('get-thana', ['as' => 'get_thana', 'uses' => 'Portal\HomeController@getThana']);
Route::get('get-bank-branch', ['as' => 'get_bank_branch', 'uses' => 'Portal\HomeController@getBankBranch']);

Route::get('{type}/application-form/{id}', ['as' => 'get_tender_application_form', 'uses' => 'Portal\HomeController@getTenderApplicationForm']);
Route::post('{type}/tender-applicant-form/{id}', ['as' => 'tenderapplicant_create_form', 'uses' => 'Portal\HomeController@tenderapplicantCreate']);
Route::get('{type}/tender-applicant-preview', ['as' => 'get_tender_applicant_preview', 'uses' => 'Portal\HomeController@tenderapplicantPreview']);
Route::get('{type}/tender-applicant-success', ['as' => 'get_tender_applicant_success', 'uses' => 'Portal\HomeController@tenderapplicantSuccess']);
Route::post('{type}/tender-applicant-save', ['as' => 'tenderapplicant_save_form', 'uses' => 'Portal\HomeController@tenderapplicantSave']);
//Route::post('tender/tender-applicant-fail', array("as" => "tenderapplicant_fail_form", 'uses' => 'Portal\HomeController@tenderapplicantSave'));
Route::get('{type}/apply-form-download/{id}', ['as' => 'apply_form_download', 'uses' => 'Portal\HomeController@applyFormDownload']);
Route::get('download-user-manual', ['as' => 'download_user_manual', 'uses' => 'Portal\HomeController@getUserManualDownload']);

Route::get('apply-online', ['as' => 'apply_online', 'uses' => 'Portal\HomeController@applyOnline']);
Route::post('post-application', ['as' => 'post_application', 'uses' => 'Portal\HomeController@postApplication']);
Route::get('application-payment', ['as' => 'application_payment', 'uses' => 'Portal\HomeController@applicationPayment']);
Route::get('contact', ['as' => 'contact', 'uses' => 'Portal\HomeController@contact']);
Route::post('submit-contact', ['as' => 'submit_contact', 'uses' => 'Portal\HomeController@submitContact']);
Route::get('language-switch/{lang}', ['as' => 'language_switch', 'uses' => 'Portal\HomeController@languageSwitch']);
Route::get('get-ward', ['as' => 'get_ward', 'uses' => 'Portal\HomeController@getWard']);
Route::get('get-mouza', ['as' => 'get_mouza', 'uses' => 'Portal\HomeController@getMouza']);
Route::get('get-area-land', ['as' => 'get_area_land', 'uses' => 'Portal\HomeController@getAreaLand']);
Route::get('get-ward-land', ['as' => 'get_ward_land', 'uses' => 'Portal\HomeController@getWardLand']);
Route::get('get-mouza-land', ['as' => 'get_mouza_land', 'uses' => 'Portal\HomeController@getMouzaLand']);
Route::get('get-landno', ['as' => 'get-landno', 'uses' => 'Portal\HomeController@getLandno']);
Route::get('get-land-subcategory', ['as' => 'get_land_subcategory', 'uses' => 'Portal\HomeController@getLandSubcategory']);
Route::get('get-lease-subcategory', ['as' => 'get_lease_subcategory', 'uses' => 'Portal\HomeController@getLeaseSubcategory']);
Route::get('check-unique-phone', ['as' => 'check_unique_phone', 'uses' => 'Portal\HomeController@checkUniquePhone']);
Route::get('check-unique-email', ['as' => 'check_unique_email', 'uses' => 'Portal\HomeController@checkUniqueEmail']);
Route::get('check-unique-nid', ['as' => 'check_unique_nid', 'uses' => 'Portal\HomeController@checkUniqueNid']);
// End public portal pages

Route::post('make-payment/{type}/{part}/{vchd}/{sc}', ['as' => 'make_payment', 'uses' => 'Portal\HomeController@makePayment']);
Route::post('installment-pay/{type}/{id}/{vchd}/{sc}', ['as' => 'installment_pay', 'uses' => 'Portal\HomeController@installmentPay']);
Route::get('payment-invoice/{id}/{part}/{sc}/{type}/{uid}', ['as' => 'payment_invoice', 'uses' => 'Portal\HomeController@paymentInvoice']);
Route::get('payment-selection', ['as' => 'payment_selection', 'uses' => 'Portal\HomeController@paymentSelection']);
Route::post('post-payment/{type}/{part}/{vchd}/{sc}/{uid}', ['as' => 'post_payment', 'uses' => 'Portal\HomeController@postPayment']);

Route::get('citizen-number-search', ['as' => 'citizen_number_search', 'uses' => 'Portal\HomeController@citizenNumberSearch']);

// For registered citizen
Route::group(['middleware' => 'auth'], function () {
    Route::get('feedback', ['as' => 'public_feedback', 'uses' => 'Portal\HomeController@publicFeedback']);
    Route::post('post-feedback', ['as' => 'post_feedback', 'uses' => 'Portal\HomeController@postFeedback']);

    // Route for Notifications
    Route::get('notification-view/{id}', ['as' => 'notification_view', 'uses' => 'Admin_access\NotificationController@getView']);

    // Money collection
    Route::get('user-tender-schedule', ['as' => 'user_tender_schedule', 'uses' => 'Portal\HomeController@userTenderSchedule']);
    Route::get('user-payment-info', ['as' => 'user_payment_info', 'uses' => 'Applicant\ApplicantController@userPaymentInfo']);

    // Ownership change
    Route::get('user-ownership-info', ['as' => 'user_ownership_info', 'uses' => 'Applicant\ApplicantController@userOwnershipInfo']);
    Route::get('user-ownership-letter/{uid}/{utype}/{sc}/{type}', ['as' => 'user_ownership_letter', 'uses' => 'Applicant\ApplicantController@userOwnershipLetter']);
    Route::post('user-ownership-save/{uid}/{utype}/{sc}/{type}', ['as' => 'user_ownership_save', 'uses' => 'Applicant\ApplicantController@userOwnershipSave']);
    Route::get('user-ownership-print/{uid}/{utype}/{sc}/{type}', ['as' => 'user_ownership_print', 'uses' => 'Applicant\ApplicantController@userOwnershipPrint']);
});

// Admin login
Route::group(['middleware' => 'guest', 'prefix' => 'auth'], function () {
    Route::get('/', ['as' => 'user_login', 'uses' => 'Portal\HomeController@showLogin']);
    Route::post('user-authentication', ['as' => 'user_authentication', 'uses' => 'Auth\AuthController@userAuthentication']);
    Route::get('recovery', ['as' => 'applicant_recovery', 'uses' => 'Portal\HomeController@applicantRecovery']);
    Route::post('check-account', ['as' => 'app_check_account', 'uses' => 'Portal\HomeController@checkAccount']);
    Route::get('reset-password/{id}/{type}/{token}', ['as' => 'get_reset_token', 'uses' => 'Portal\HomeController@getResetToken']);
    Route::post('change-password/{id}/{type}/{token}', ['as' => 'change_app_password', 'uses' => 'Portal\HomeController@changeAppPassword']);
});

// Logout active session
Route::get('get-logout', ['as' => 'get_logout', 'uses' => 'Auth\AuthController@getLogout']);

// Tender applicant login
// Route::group(['middleware' => 'guest', 'prefix' => 'login'], function () {

//     Route::get('/', array("as" => "applicant_login", 'uses' => 'Portal\HomeController@applicantLogin'));
//     Route::post('applicant-auth', array("as" => "applicant_auth", 'uses' => 'Auth\AuthController@userAuth'));

// });

// Admin part start here
Route::group(['middleware' => 'auth', 'prefix' => 'admin'], function () {
    Route::get('dashboard', ['as' => 'admin_dashboard', 'uses' => 'Admin_access\DashboardController@getIndex']);
    Route::get('get-chart-data', ['as' => 'get_chart_data', 'uses' => 'Admin_access\DashboardController@getChartData']);
    Route::get('revenue-chart-data', ['as' => 'revenue_chart_data', 'uses' => 'Admin_access\DashboardController@revenueChartData']);
    Route::get('application-data', ['as' => 'tender_application_data', 'uses' => 'Admin_access\DashboardController@tenderApplicationData']);

    // Security and Access @author  Abdul Awal <abdulawal@atilimited.net>
    Route::get('permission/list', ['as' => 'permission_list', 'uses' => 'Admin_access\AccessController@permissionIndex']);
    Route::get('permission/data', ['as' => 'permission_data', 'uses' => 'Admin_access\AccessController@permissionData']);
    Route::get('permission/create-form', ['as' => 'permission_create_form', 'uses' => 'Admin_access\AccessController@permissionCreateForm']);
    Route::post('permission/create', ['as' => 'permission_create', 'uses' => 'Admin_access\AccessController@permissionCreate']);
    Route::get('permission/edit-form/{id}', ['as' => 'permission_edit_form', 'uses' => 'Admin_access\AccessController@permissionEditForm']);
    Route::post('permission/update/{id}', ['as' => 'permission_update', 'uses' => 'Admin_access\AccessController@permissionUpdate']);
    Route::get('permission/delete/{id}', ['as' => 'permission_delete', 'uses' => 'Admin_access\AccessController@permissionDelete']);
    Route::get('role/list', ['as' => 'role_list', 'uses' => 'Admin_access\AccessController@roleIndex']);
    Route::get('role/data', ['as' => 'role_data', 'uses' => 'Admin_access\AccessController@roleData']);
    Route::get('role/create-form', ['as' => 'role_create_form', 'uses' => 'Admin_access\AccessController@roleCreateForm']);
    Route::post('role/create', ['as' => 'role_create', 'uses' => 'Admin_access\AccessController@roleCreate']);
    Route::get('role/edit-form/{id}', ['as' => 'role_edit_form', 'uses' => 'Admin_access\AccessController@roleEditForm']);
    Route::post('role/update/{id}', ['as' => 'role_update', 'uses' => 'Admin_access\AccessController@roleUpdate']);
    Route::get('role/delete/{id}', ['as' => 'role_delete', 'uses' => 'Admin_access\AccessController@roleDelete']);
    Route::get('role/permission', ['as' => 'role_permission', 'uses' => 'Admin_access\AccessController@rolePermission']);
    Route::get('role/role-permissions', ['as' => 'role_permissions', 'uses' => 'Admin_access\AccessController@rolePermissions']);
    Route::post('role/assign-permissions', ['as' => 'assign_permissions', 'uses' => 'Admin_access\AccessController@assignPermissions']);
    Entrust::routeNeedsRole('admin/permission/*', 'admin', Redirect::to('admin/dashboard'));
    Entrust::routeNeedsRole('admin/role/*', 'admin', Redirect::to('admin/dashboard'));
    Entrust::routeNeedsRole('log-viewer', 'admin', Redirect::to('admin/dashboard'));
    Entrust::routeNeedsRole('log-viewer/*', 'admin', Redirect::to('admin/dashboard'));
    // Security end

    // User management @author  Abdul Awal <abdulawal@atilimited.net>
    Route::group(['middleware' => ['role:admin']], function () {
        Route::get('user/list', ['as' => 'user_list', 'uses' => 'Admin_access\UserController@userIndex']);
        Route::get('user/data', ['as' => 'user_data', 'uses' => 'Admin_access\UserController@userData']);
        Route::get('user/create-form', ['as' => 'user_create_form', 'uses' => 'Admin_access\UserController@createForm']);
        Route::post('user/create', ['as' => 'user_create', 'uses' => 'Admin_access\UserController@createUser']);
        Route::get('user/edit-form/{id}', ['as' => 'user_edit_form', 'uses' => 'Admin_access\UserController@userEditForm']);
        Route::post('user/update/{id}', ['as' => 'user_update', 'uses' => 'Admin_access\UserController@updateUser']);
        Route::get('user/delete/{id}', ['as' => 'delete_user', 'uses' => 'Admin_access\UserController@deleteUser']);
        Route::get('user/status/{id}', ['as' => 'user_status', 'uses' => 'Admin_access\UserController@changeStatus']);
    });

    Route::get('user/profile/{id}', ['as' => 'user_profile', 'uses' => 'Admin_access\UserController@userProfile']);
    Route::get('user/profile-overview/{id}', ['as' => 'profile_overview', 'uses' => 'Admin_access\UserController@profileOverview']);
    Route::post('user/update-password/{id}', ['as' => 'update_password', 'uses' => 'Admin_access\UserController@updatePassword']);
    Route::post('user/update-profile/{id}', ['as' => 'update_profile', 'uses' => 'Admin_access\UserController@updateProfile']);
    Route::post('user/update-picture/{id}', ['as' => 'update_profile_picture', 'uses' => 'Admin_access\UserController@updateProfilePicture']);
    // User end

    Route::get('service/payment-info/{type}/{ta}/{sc}/{vchd}', ['as' => 'payment_info', 'uses' => 'Admin_access\AccountController@paymentInfo']);
    Route::get('service/money-collection', ['as' => 'money_collection', 'uses' => 'Admin_access\AccountController@moneyCollection']);
    Route::get('service/ownership-change', ['as' => 'ownership_change', 'uses' => 'Admin_access\AccountController@ownershipChange']);
    Route::get('service/notice-list', ['as' => 'notice_list', 'uses' => 'Admin_access\CitizenController@noticeList']);
    Route::get('service/notice-table-select', ['as' => 'notice_table_select', 'uses' => 'Admin_access\CitizenController@noticeTableSelect']);
    Route::get('service/notice-table-data', ['as' => 'notice_table_data', 'uses' => 'Admin_access\CitizenController@noticeTableData']);
    Route::get('service/tender-apply-form/{sc}/{type}', ['as' => 'tender_apply_form', 'uses' => 'Admin_access\CitizenController@tenderApplyForm']);
    Route::post('service/tender-apply-data/{sc}/{type}', ['as' => 'tender_apply_data', 'uses' => 'Admin_access\CitizenController@tenderApplyData']);
    Entrust::routeNeedsRole('admin/service/*', ['admin', 'project-manager'], Redirect::to('admin/dashboard'), false);

    // Land management @author  Abdul Awal <abdulawal@atilimited.net>
    Route::get('land/category', ['as' => 'land_category', 'uses' => 'Admin_access\LandController@landCategory']);
    Route::get('land/category-data', ['as' => 'land_category_data', 'uses' => 'Admin_access\LandController@landCategoryData']);
    Route::get('land/category-create-form', ['as' => 'category_create_form', 'uses' => 'Admin_access\LandController@categoryCreateForm']);
    Route::post('land/category-save', ['as' => 'land_category_save', 'uses' => 'Admin_access\LandController@landCategorySave']);
    Route::get('land/category-edit-form/{id}', ['as' => 'category_edit_form', 'uses' => 'Admin_access\LandController@categoryEditForm']);
    Route::post('land/category-update/{id}', ['as' => 'land_category_update', 'uses' => 'Admin_access\LandController@landCategoryUpdate']);
    Route::get('land/category-status/{id}', ['as' => 'land_category_status', 'uses' => 'Admin_access\LandController@landCategoryStatus']);
    Route::get('land/category-delete/{id}', ['as' => 'land_category_delete', 'uses' => 'Admin_access\LandController@landCategoryDelete']);
    Route::get('land/subcategory', ['as' => 'land_subcategory', 'uses' => 'Admin_access\LandController@landSubcategory']);
    Route::get('land/subcategory-data', ['as' => 'land_subcategory_data', 'uses' => 'Admin_access\LandController@landSubCategoryData']);
    Route::get('land/subcategory-create-form', ['as' => 'subcategory_create_form', 'uses' => 'Admin_access\LandController@subCategoryCreateForm']);
    Route::post('land/subcategory-create', ['as' => 'land_subcategory_create', 'uses' => 'Admin_access\LandController@landSubCategoryCreate']);
    Route::get('land/subcategory-edit-form/{id}', ['as' => 'subcategory_edit_form', 'uses' => 'Admin_access\LandController@subcategoryEditForm']);
    Route::post('land/subcategory-update/{id}', ['as' => 'land_subcategory_update', 'uses' => 'Admin_access\LandController@landSubcategoryUpdate']);

    Route::get('land/land', ['as' => 'land_list', 'uses' => 'Admin_access\LandController@landIndex']);
    Route::get('land/land-data', ['as' => 'land_data', 'uses' => 'Admin_access\LandController@landData']);
    Route::get('land/land-view/{id}', ['as' => 'land_view', 'uses' => 'Admin_access\LandController@landView']);
    Route::get('land/land-print/{id}', ['as' => 'land_print', 'uses' => 'Admin_access\LandController@landPrint']);
    Route::get('land/land-report', ['as' => 'land_report', 'uses' => 'Admin_access\LandController@landReport']);
    Route::get('land/land-report-data', ['as' => 'land_report_data', 'uses' => 'Admin_access\LandController@landReportData']);
    Route::get('land/land-report-print', ['as' => 'land_report_print', 'uses' => 'Admin_access\LandController@landReportDataPrint']); //nurullah 17-April-2017
    Route::get('land/land-report-excel', ['as' => 'land_report_excel', 'uses' => 'Admin_access\LandController@landReportDataExcel']); //nurullah 29-July-2017
    Route::get('land/land-create-form', ['as' => 'land_create_form', 'uses' => 'Admin_access\LandController@landCreateForm']);
    Route::post('land/land-create', ['as' => 'land_create', 'uses' => 'Admin_access\LandController@landCreate']);
    Route::get('land/land-status/{id}', ['as' => 'land_status', 'uses' => 'Admin_access\LandController@landStatus']);
    Route::get('land/land-edit-form/{id}', ['as' => 'land_edit_form', 'uses' => 'Admin_access\LandController@landEditForm']);
    Route::get('land/land-delete/{id}', ['as' => 'land_delete', 'uses' => 'Admin_access\LandController@landDelete']);
    Route::get('land/dag-no-delete/{id}', ['as' => 'land_dag_delete', 'uses' => 'Admin_access\LandController@landDagDelete']);
    Route::get('land/land-use-delete/{id}', ['as' => 'land_use_delete', 'uses' => 'Admin_access\LandController@landUseDelete']);
    Route::get('land/document-delete/{id}/{type}', ['as' => 'land_document_delete', 'uses' => 'Admin_access\LandController@landDocumentDelete']);
    Route::get('land/tax-delete/{id}', ['as' => 'land_tax_delete', 'uses' => 'Admin_access\LandController@landTaxDelete']);
    Route::post('land/land-update/{id}', ['as' => 'land_update', 'uses' => 'Admin_access\LandController@landUpdate']);
    Entrust::routeNeedsRole('admin/land/*', 'admin', Redirect::to('admin/dashboard'));
    // Land end

    //Zone start  @author      sourov <rokibuzzaman@atilimited.net>
    Route::get('zone/zone', ['as' => 'zone_list', 'uses' => 'Admin_access\LandController@zone']);
    Route::get('zone/zone-data', ['as' => 'zone_data', 'uses' => 'Admin_access\LandController@zoneData']);
    Route::get('zone/zone-create-form', ['as' => 'zone_create_form', 'uses' => 'Admin_access\LandController@zoneCreateForm']);
    Route::post('zone/zone-save', ['as' => 'zone_save', 'uses' => 'Admin_access\LandController@zoneSave']);
    Route::get('zone/zone-edit-form/{id}', ['as' => 'zone_edit_form', 'uses' => 'Admin_access\LandController@zoneEditForm']);
    Route::post('zone/zone-update/{id}', ['as' => 'zone_update', 'uses' => 'Admin_access\LandController@zoneUpdate']);
    Route::get('zone/zone-status/{id}', ['as' => 'zone_status', 'uses' => 'Admin_access\LandController@zoneStatus']);
    Route::get('zone/zone-delete/{id}', ['as' => 'zone_delete', 'uses' => 'Admin_access\LandController@zoneDelete']);
    Entrust::routeNeedsRole('admin/zone/*', 'admin', Redirect::to('admin/dashboard'));
    // Zone end

    //Word start  @author      sourov <rokibuzzaman@atilimited.net>
    Route::get('ward/ward', ['as' => 'word_list', 'uses' => 'Admin_access\LandController@ward']);
    Route::get('ward/ward-data', ['as' => 'ward_data', 'uses' => 'Admin_access\LandController@wardData']);
    Route::get('ward/ward-create-form', ['as' => 'ward_create_form', 'uses' => 'Admin_access\LandController@wardCreateForm']);
    Route::post('ward/ward-create', ['as' => 'ward_create', 'uses' => 'Admin_access\LandController@wardCreate']);
    Route::get('ward/ward-edit-form/{id}', ['as' => 'ward_edit_form', 'uses' => 'Admin_access\LandController@wardEditForm']);
    Route::post('ward/ward-update/{id}', ['as' => 'ward_update', 'uses' => 'Admin_access\LandController@wardUpdate']);
    Entrust::routeNeedsRole('admin/ward/*', 'admin', Redirect::to('admin/dashboard'));

    /* Mouza start  @author      Nurullah <nurul@atilimited.net>*/
    Route::get('mouza/mouza', ['as' => 'mouza_list', 'uses' => 'Admin_access\LandController@mouza']);
    Route::get('mouza/mouza-data', ['as' => 'mouza_data', 'uses' => 'Admin_access\LandController@mouzaData']);
    Route::get('mouza/mouza-create-form', ['as' => 'mouza_create_form', 'uses' => 'Admin_access\LandController@mouzaCreateForm']);
    Route::get('mouza/check-unique', ['as' => 'mouza_unique', 'uses' => 'Admin_access\LandController@mouzaUnique']);
    Route::post('mouza/mouza-save', ['as' => 'mouza_save', 'uses' => 'Admin_access\LandController@mouzaSave']);
    Route::get('mouza/mouza-edit-form/{id}', ['as' => 'mouza_edit_form', 'uses' => 'Admin_access\LandController@mouzaEditForm']);
    Route::get('mouza/update-unique/{id}', ['as' => 'mouza_update_unique', 'uses' => 'Admin_access\LandController@mouzaUpdateUnique']);
    Route::post('mouza/mouza-update/{id}', ['as' => 'mouza_update', 'uses' => 'Admin_access\LandController@mouzaUpdate']);
    Route::get('mouza/mouza-status/{id}', ['as' => 'mouza_status', 'uses' => 'Admin_access\LandController@mouzaStatus']);
    Route::get('mouza/mouza-delete/{id}', ['as' => 'mouza_delete', 'uses' => 'Admin_access\LandController@mouzaDelete']);
    Entrust::routeNeedsRole('admin/mouza/*', 'admin', Redirect::to('admin/dashboard'));

    // Lease management
    Route::get('lease/category', ['as' => 'lease_category', 'uses' => 'Admin_access\LeaseController@leaseCategory']);
    Route::get('lease/category-data', ['as' => 'lease_category_data', 'uses' => 'Admin_access\LeaseController@leaseCategoryData']);
    Route::get('lease/category-create-form', ['as' => 'lease_category_form', 'uses' => 'Admin_access\LeaseController@leasecategoryCreateForm']);
    Route::post('lease/category-save', ['as' => 'lease_category_save', 'uses' => 'Admin_access\LeaseController@leaseCategorySave']);
    Route::get('lease/category-edit-form/{id}', ['as' => 'lease_edit_form', 'uses' => 'Admin_access\LeaseController@leasecategoryEditForm']);
    Route::post('lease/category-update/{id}', ['as' => 'lease_category_update', 'uses' => 'Admin_access\LeaseController@leaseCategoryUpdate']);
    Route::get('lease/category-status/{id}', ['as' => 'lease_category_status', 'uses' => 'Admin_access\LeaseController@leaseCategoryStatus']);
    Route::get('lease/category-delete/{id}', ['as' => 'lease_category_delete', 'uses' => 'Admin_access\LeaseController@leaseCategoryDelete']);
    Route::get('lease/lease', ['as' => 'lease_list', 'uses' => 'Admin_access\LeaseController@leaseIndex']);
    Route::get('lease/lease-create-form', ['as' => 'lease_create_form', 'uses' => 'Admin_access\LeaseController@leaseCreateForm']);
    Route::post('lease/lease-create', ['as' => 'lease_create', 'uses' => 'Admin_access\LeaseController@leaseCreate']);
    Route::get('lease/lease-data', ['as' => 'lease_data_table', 'uses' => 'Admin_access\LeaseController@leaseData']);
    Route::get('lease/subcategory', ['as' => 'lease_subcategory', 'uses' => 'Admin_access\LeaseController@leaseSubcategory']);
    Route::get('lease/subcategory-data', ['as' => 'lease_subcategory_data', 'uses' => 'Admin_access\LeaseController@leaseSubCategoryData']);
    Route::get('lease/subcategory-create-form', ['as' => 'lease_subcategory_create_form', 'uses' => 'Admin_access\LeaseController@leasesubCategoryCreateForm']);
    Route::post('lease/leasesubcategory-create', ['as' => 'lease_subcategory_create', 'uses' => 'Admin_access\LeaseController@leaseSubCategoryCreate']);
    Route::get('lease/subcategory-edit-form/{id}', ['as' => 'lease_subcategory_edit_form', 'uses' => 'Admin_access\LeaseController@leasesubcategoryEditForm']);
    Route::post('lease/subcategory-update/{id}', ['as' => 'lease_subcategory_update', 'uses' => 'Admin_access\LeaseController@leaseSubcategoryUpdate']);
    Route::get('lease/subcategory-status/{id}', ['as' => 'lease_subcategory_status', 'uses' => 'Admin_access\LeaseController@leaseSubCategoryStatus']);
    Route::get('lease/subcategory-delete/{id}', ['as' => 'lease_subcategory_delete', 'uses' => 'Admin_access\LeaseController@leaseSubCategoryDelete']);
    Route::get('lease/lease-comparison', ['as' => 'lease_comparison', 'uses' => 'Admin_access\LeaseController@leaseComparison']);
    Route::get('lease/comparison-data', ['as' => 'lease_comparison_data', 'uses' => 'Admin_access\LeaseController@leaseComparisonData']);
    Route::get('lease/applicant-list', ['as' => 'lease_applicant_list', 'uses' => 'Admin_access\LeaseController@leaseApplicantList']);
    Route::get('lease/applicant-data', ['as' => 'lease_applicant_data', 'uses' => 'Admin_access\LeaseController@leaseListData']);

    Route::get('lease/tender-create-form', ['as' => 'lease_tender_create_form', 'uses' => 'Admin_access\LeaseController@leaseTenderCreateForm']);
    Route::get('lease/tender-schedule-data', ['as' => 'tender_schedule_data', 'uses' => 'Admin_access\LeaseController@tenderScheduleData']);
    Route::get('lease/tender-notice', ['as' => 'lease_notice', 'uses' => 'Admin_access\LeaseController@leaseNotice']);
    Route::get('lease/tender-data', ['as' => 'lease_data', 'uses' => 'Admin_access\LeaseController@leasePublishData']);
    Route::post('lease/tender-create', ['as' => 'lease_tender_create', 'uses' => 'Admin_access\LeaseController@leaseTenderCreate']);
    Route::get('lease/tender-edit/{id}', ['as' => 'lease_tender_edit', 'uses' => 'Admin_access\LeaseController@leaseTenderEdit']);
    Route::get('lease/tender-status/{id}', ['as' => 'lease_tender_status', 'uses' => 'Admin_access\LeaseController@leaseTenderStatus']);
    Route::get('lease/schedule-delete/{id}', ['as' => 'lease_tender_schedule_delete', 'uses' => 'Admin_access\LeaseController@leaseTenderScheduleDelete']);
    Route::get('lease/date-time-delete/{id}', ['as' => 'lease_tender_date_delete', 'uses' => 'Admin_access\LeaseController@leaseTenderDateDelete']);
    Route::post('lease/tender-update/{id}', ['as' => 'lease_tender_update', 'uses' => 'Admin_access\LeaseController@leaseTenderUpdate']);
    Route::get('lease/tender-delete/{id}', ['as' => 'lease_tender_delete', 'uses' => 'Admin_access\LeaseController@leaseTenderDelete']);

    /* Lease Demesne (খাস) start  @author      Nurullah <nurul@atilimited.net>*/
    Route::get('lease/demesne', ['as' => 'lease_demesne', 'uses' => 'Admin_access\LeaseController@leaseDemesne']);
    Route::get('lease/lease-demesne-data', ['as' => 'lease_demesne_data', 'uses' => 'Admin_access\LeaseController@leaseDemesneData']);
    Route::get('lease/lease-demesne-create', ['as' => 'lease_demesne_create', 'uses' => 'Admin_access\LeaseController@leaseDemesneCreate']);
    Route::get('lease/schedule', ['as' => 'get_schedule', 'uses' => 'Admin_access\LeaseController@getscheduleByTenderId']);
    Route::post('lease/lease-demesne-save', ['as' => 'lease_demesne_save', 'uses' => 'Admin_access\LeaseController@leaseDemesneSave']);
    Route::get('lease/lease-demesne-edit/{id}', ['as' => 'lease_demesne_edit', 'uses' => 'Admin_access\LeaseController@leaseDemesneEdit']);
    Route::post('lease/lease-demesne-update/{id}', ['as' => 'lease_demesne_update', 'uses' => 'Admin_access\LeaseController@leaseDemesneUpdate']);
    Route::get('lease/demesne-cancel-form/{id}', ['as' => 'lease_demesne_cancel_form', 'uses' => 'Admin_access\LeaseController@leaseDemesneCancelForm']);
    Route::post('lease/demesne-update-cancel/{id}', ['as' => 'lease_demesne_cancel_update', 'uses' => 'Admin_access\LeaseController@leaseDemesneCancelUpdate']);

    /* Lease Demesne (খাসের টাকা সংগ্রহ) start  @author      Nurullah <nurul@atilimited.net>*/
    Route::get('lease/demesne-money', ['as' => 'demesne_money', 'uses' => 'Admin_access\LeaseController@demesneMoney']);
    Route::get('lease/demesne-money-data', ['as' => 'demesne_money_data', 'uses' => 'Admin_access\LeaseController@demesneMoneyData']);
    Route::get('lease/demesne-money-create', ['as' => 'demesne_money_create', 'uses' => 'Admin_access\LeaseController@DemesneMoneyCreate']);
    Route::get('lease/demesne-info', ['as' => 'get_demesne_info', 'uses' => 'Admin_access\LeaseController@getInfoByScheduleId']);
    Route::post('lease/demesne-money-save', ['as' => 'demesne_money_save', 'uses' => 'Admin_access\LeaseController@DemesneMoneySave']);
    // Route::get('lease/lease-demesne-edit/{id}', array("as" => "lease_demesne_edit", 'uses' => 'Admin_access\LeaseController@leaseDemesneEdit'));
    // Route::post('lease/lease-demesne-update/{id}', array("as" => "lease_demesne_update", 'uses' => 'Admin_access\LeaseController@leaseDemesneUpdate'));
    // Route::get('lease/demesne-cancel-form/{id}', array("as" => "lease_demesne_cancel_form", 'uses' => 'Admin_access\LeaseController@leaseDemesneCancelForm'));
    // Route::post('lease/demesne-update-cancel/{id}', array("as" => "lease_demesne_cancel_update", 'uses' => 'Admin_access\LeaseController@leaseDemesneCancelUpdate'));

    Route::get('lease/cancel-form/{type}/{id}', ['as' => 'lease_cancel_form', 'uses' => 'Admin_access\LeaseController@leaseCancelForm']);
    Route::post('lease/update-cancel/{type}/{id}', ['as' => 'lease_cancel_update', 'uses' => 'Admin_access\LeaseController@leaseCancelUpdate']);
    Route::get('lease/remarks-form/{type}/{id}', ['as' => 'lease_remarks_form', 'uses' => 'Admin_access\LeaseController@leaseRemarksForm']);
    Route::post('lease/update-remarks/{type}/{id}', ['as' => 'lease_remarks_update', 'uses' => 'Admin_access\LeaseController@leaseRemarksUpdate']);
    Route::get('lease/primary-select-form/{ts}/{type}/{id}', ['as' => 'lease_primary_select_form', 'uses' => 'Admin_access\LeaseController@leasePrimaryApplicantSelectForm']);
    Route::post('lease/primary-select-insert/{ts}/{type}/{id}', ['as' => 'lease_primary_select_insert', 'uses' => 'Admin_access\LeaseController@leasePrimaryApplicantSave']);

    Route::get('lease/primary-applicant-list', ['as' => 'lease_primary_selected_applicant_list', 'uses' => 'Admin_access\LeaseController@leasePrimaryApplicantList']);
    Route::get('lease/primary-applicant-data', ['as' => 'lease_primary_applicant_data', 'uses' => 'Admin_access\LeaseController@leasePrimaryListData']);
    Route::get('lease/update-primary-print/{ts}/{type}/{id}', ['as' => 'lease_primary_letter_print', 'uses' => 'Admin_access\LeaseController@leasePrimaryLetterPrint']);

    /* Lease Final Selection  @author      Nurullah <nurul@atilimited.net>*/
    Route::get('lease/final-applicant-list', ['as' => 'lease_final_selected_applicant_list', 'uses' => 'Admin_access\LeaseController@leaseFinalApplicantList']);
    Route::get('lease/final-applicant-data', ['as' => 'lease_final_applicant_data', 'uses' => 'Admin_access\LeaseController@leasefinalListData']);
    Route::get('lease/final-select-form/{ts}/{type}/{id}', ['as' => 'lease_final_select_form', 'uses' => 'Admin_access\LeaseController@leaseFinalApplicantSelectForm']);
    Route::post('lease/final-select-insert/{ts}/{type}/{id}', ['as' => 'lease_final_select_insert', 'uses' => 'Admin_access\LeaseController@leaseFinalApplicantSave']);
    Route::get('lease/final-print/{ts}/{type}/{id}', ['as' => 'lease_final_letter_print', 'uses' => 'Admin_access\LeaseController@leaseFinalLetterPrint']);

    /* Lease Final Selection  @author      Nurullah <nurul@atilimited.net>*/
    Route::get('lease/revenue-list', ['as' => 'lease_revenue_list', 'uses' => 'Admin_access\AccountController@leaseRevenueList']);
    Route::get('lease/revenue-data', ['as' => 'lease_revenue_data', 'uses' => 'Admin_access\AccountController@leaseRevenueData']);
    Route::get('lease/revenue-data-print', ['as' => 'lease_revenue_data_print', 'uses' => 'Admin_access\AccountController@leaseRevenueDataPrint']);

    Entrust::routeNeedsRole('admin/lease/*', ['admin', 'lease-manager'], Redirect::to('admin/dashboard'), false);

    /* Account Particular start  @author      Nurullah <nurul@atilimited.net>*/
    Route::get('particular', ['as' => 'particular_list', 'uses' => 'Admin_access\AccountController@particular']);
    Route::get('particular/particular-data', ['as' => 'particular_data', 'uses' => 'Admin_access\AccountController@particularData']);
    Route::get('particular/particular-create-form', ['as' => 'particular_create_form', 'uses' => 'Admin_access\AccountController@particularCreateForm']);
    Route::post('particular/particular-save', ['as' => 'particular_save', 'uses' => 'Admin_access\AccountController@particularSave']);
    Route::get('particular/particular-edit-form/{id}', ['as' => 'particular_edit_form', 'uses' => 'Admin_access\AccountController@particularEditForm']);
    Route::post('particular/particular-update/{id}', ['as' => 'particular_update', 'uses' => 'Admin_access\AccountController@particularUpdate']);
    Route::get('particular/particular-status/{id}', ['as' => 'particular_status', 'uses' => 'Admin_access\AccountController@particularStatus']);
    Route::get('particular/particular-delete/{id}', ['as' => 'particular_delete', 'uses' => 'Admin_access\AccountController@particularDelete']);
    Route::get('particular/account-cashbook', ['as' => 'account_cashboak', 'uses' => 'Admin_access\AccountController@accountCashbook']);
    Route::get('particular/cashiar-cashbook', ['as' => 'cashiar_cashboak', 'uses' => 'Admin_access\AccountController@cashiarCashbook']);

    // Category particulars  @author  Abdul Awal <abdulawal@atilimited.net>
    Route::get('particular/category', ['as' => 'category_particular', 'uses' => 'Admin_access\AccountController@categoryParticular']);
    Route::get('particular/category-data', ['as' => 'category_particular_data', 'uses' => 'Admin_access\AccountController@categoryParticularData']);
    Route::get('particular/category-create-form', ['as' => 'category_particular_create_form', 'uses' => 'Admin_access\AccountController@categoryParticularCreateForm']);
    Route::post('particular/category-create', ['as' => 'category_particular_create', 'uses' => 'Admin_access\AccountController@categoryParticularCreate']);
    Route::get('particular/category-status/{id}', ['as' => 'category_particular_status', 'uses' => 'Admin_access\AccountController@categoryParticularStatus']);
    Route::get('particular/category-edit-form/{id}', ['as' => 'category_particular_edit_form', 'uses' => 'Admin_access\AccountController@categoryParticularEditForm']);
    Route::post('particular/category-update/{id}', ['as' => 'category_particular_update', 'uses' => 'Admin_access\AccountController@categoryParticularUpdate']);
    Route::get('particular/category-delete/{id}', ['as' => 'category_particular_delete', 'uses' => 'Admin_access\AccountController@categoryParticularDelete']);

    Route::get('particular/revenue-list', ['as' => 'revenue_list', 'uses' => 'Admin_access\AccountController@revenueList']);
    Route::get('particular/revenue-data', ['as' => 'revenue_data', 'uses' => 'Admin_access\AccountController@revenueData']);
    Route::get('particular/revenue-data-print', ['as' => 'revenue_data_print', 'uses' => 'Admin_access\AccountController@revenueDataPrintByVoucher']);

    Route::get('particular/applicant-pending-list', ['as' => 'applicant_pending_list', 'uses' => 'Admin_access\AccountController@applicantPendingList']);
    Route::get('particular/installment-pending-data', ['as' => 'installment_pending_data', 'uses' => 'Admin_access\AccountController@installmentPendingData']);
    Route::get('particular/installment-list-view/{type}/{id}', ['as' => 'installment_list_view', 'uses' => 'Admin_access\AccountController@installmentPendingList']);
    Route::get('particular/approve-installment/{type}/{id}', ['as' => 'approve_installment', 'uses' => 'Admin_access\AccountController@approveInstallment']);

    /* Pending Payment  @author      Nurullah<nurul@atilimited.net>*/
    Route::get('account/payment-pending-list', ['as' => 'payment_pending_list', 'uses' => 'Admin_access\AccountController@paymentPendingList']);
    Route::get('account/payment-pending-data', ['as' => 'payment_pending_data', 'uses' => 'Admin_access\AccountController@pendingPaymentData']);
    Route::get('account/payment_approve/{id}', ['as' => 'payment_approve', 'uses' => 'Admin_access\AccountController@approvePayment']);
    Route::get('account/citizen-ledger', ['as' => 'citizen_ledger', 'uses' => 'Admin_access\AccountController@citizenLedger']);
    Route::get('account/citizen-ledger-data', ['as' => 'citizen_ledger_data', 'uses' => 'Admin_access\AccountController@citizenLedgerData']);
    Route::get('account/citizen-ledger-print', ['as' => 'citizen_ledger_print', 'uses' => 'Admin_access\AccountController@citizenLedgerPrint']);
    Route::get('account/security-money', ['as' => 'applicant_security_money', 'uses' => 'Admin_access\AccountController@applicantSecurityMoney']);
    Route::get('account/security-money-data', ['as' => 'security_money_data', 'uses' => 'Admin_access\AccountController@securityMoneyData']);
    Route::get('account/return-security-money/{ts}/{type}/{id}', ['as' => 'return_security_money', 'uses' => 'Admin_access\AccountController@returnSecurityMoney']);
    Route::post('account/security-money-update/{uid}/{utype}/{sc}', ['as' => 'security_money_update', 'uses' => 'Admin_access\AccountController@securityMoneyUpdate']);
    Route::get('account/security-money-print/{uid}/{utype}/{sc}', ['as' => 'security_money_letter_print', 'uses' => 'Admin_access\AccountController@userOwnershipPrint']);
    Entrust::routeNeedsRole('admin/particular/*', ['admin', 'account-manager'], Redirect::to('admin/dashboard'), false);

    //project category start  @author      sourov <rokibuzzaman@atilimited.net>
    Route::get('project/project-category', ['as' => 'project_category', 'uses' => 'Admin_access\ProjectController@projectCategory']);
    Route::get('project/project-category-create-form', ['as' => 'project_category_create_form', 'uses' => 'Admin_access\ProjectController@projectCategoryCreateForm']);
    Route::post('project/project-category-save', ['as' => 'project_category_save', 'uses' => 'Admin_access\ProjectController@projectCategorySave']);
    Route::get('project/project-category-data', ['as' => 'project_category_data', 'uses' => 'Admin_access\ProjectController@projectCategoryData']);
    Route::get('project/project-edit-form/{PR_CATEGORY}', ['as' => 'project_edit_form', 'uses' => 'Admin_access\ProjectController@projectcategoryEditForm']);
    Route::post('project/project-category-update/{PR_CATEGORY}', ['as' => 'project_category_update', 'uses' => 'Admin_access\ProjectController@projectCategoryUpdate']);
    Route::get('project/project_category-status/{USER_ID}', ['as' => 'project_category_status', 'uses' => 'Admin_access\ProjectController@projectCategoryStatus']);
    Route::get('project/project-category-delete/{PR_CATEGORY}', ['as' => 'project_category_delete', 'uses' => 'Admin_access\ProjectController@projectCategoryDelete']);
    //project type start  @author      sourov <rokibuzzaman@atilimited.net>
    Route::get('project/project-type', ['as' => 'project_type', 'uses' => 'Admin_access\ProjectController@projectType']);
    Route::get('project/project-type-create-form', ['as' => 'project_type_create_form', 'uses' => 'Admin_access\ProjectController@projectTypeCreateForm']);
    Route::post('project/project-type-save', ['as' => 'project_type_save', 'uses' => 'Admin_access\ProjectController@projectTypeSave']);
    Route::get('project/project-type-data', ['as' => 'project_type_data', 'uses' => 'Admin_access\ProjectController@projectTypeData']);
    Route::get('project/project-type-edit-form/{PR_TYPE}', ['as' => 'project_type_edit_form', 'uses' => 'Admin_access\ProjectController@projecttypeEditForm']);
    Route::post('project/project-type-update/{PR_TYPE}', ['as' => 'project_type_update', 'uses' => 'Admin_access\ProjectController@projectTypeUpdate']);
    Route::get('project/project-type-status/{USER_ID}', ['as' => 'project_type_status', 'uses' => 'Admin_access\ProjectController@projectTypeStatus']);
    Route::get('project/project-type-delete/{PR_TYPE}', ['as' => 'project_type_delete', 'uses' => 'Admin_access\ProjectController@projectTypeDelete']);
    Route::get('project/project-file-delete/{id}', ['as' => 'project_file_delete', 'uses' => 'Admin_access\ProjectController@projectFileDelete']);

    /* project floor start  @author      Nurullah<nurul@atilimited.net>*/
    Route::get('project/floor', ['as' => 'floor', 'uses' => 'Admin_access\ProjectController@floor']);
    Route::get('project/floor-create-form', ['as' => 'project_floor_create_form', 'uses' => 'Admin_access\ProjectController@projectFloorCreateForm']);
    Route::post('project/floor-save', ['as' => 'project_floor_save', 'uses' => 'Admin_access\ProjectController@projectFloorSave']);
    Route::get('project/floor-data', ['as' => 'project_floor_data', 'uses' => 'Admin_access\ProjectController@projectFloorData']);
    Route::get('project/floor-edit-form/{FLOOR_ID}', ['as' => 'project_floor_edit_form', 'uses' => 'Admin_access\ProjectController@projectfloorEditForm']);
    Route::post('project/floor-update/{FLOOR_ID}', ['as' => 'project_floor_update', 'uses' => 'Admin_access\ProjectController@projectFloorUpdate']);
    Route::get('project/floor-status/{USER_ID}', ['as' => 'project_floor_status', 'uses' => 'Admin_access\ProjectController@projectFloorStatus']);
    Route::get('project/floor-delete/{FLOOR_ID}', ['as' => 'project_floor_delete', 'uses' => 'Admin_access\ProjectController@projectFloorDelete']);

    //project Category Condition start  @author      sourov <rokibuzzaman@atilimited.net>
    Route::get('project/project-cat-con', ['as' => 'project_cat_con', 'uses' => 'Admin_access\ProjectController@projectCatCondition']);
    Route::get('project/project-cat-con-create-form', ['as' => 'project_cat_con_create_form', 'uses' => 'Admin_access\ProjectController@projectCatConditionCreateForm']);
    Route::post('project/project-cat-con-save', ['as' => 'project_cat_con_save', 'uses' => 'Admin_access\ProjectController@projectCatConditionSave']);
    Route::get('project/project-cat-con-data', ['as' => 'project_cat_con_data', 'uses' => 'Admin_access\ProjectController@projectCatConditionData']);
    Route::get('project/project-cat-con-edit-form/{CATE_CON_ID}', ['as' => 'project_cat_con_edit_form', 'uses' => 'Admin_access\ProjectController@projectcatconditionEditForm']);
    Route::post('project/project-cat-con-update/{CATE_CON_ID}', ['as' => 'project_cat_con_update', 'uses' => 'Admin_access\ProjectController@projectCatConditionUpdate']);
    Route::get('project/project-cat-con-status/{USER_ID}', ['as' => 'project_cat_con_status', 'uses' => 'Admin_access\ProjectController@projectCatConditionStatus']);
    Route::get('project/project-cat-con-delete/{CATE_CON_ID}', ['as' => 'project_cat_con_delete', 'uses' => 'Admin_access\ProjectController@projectCatConditionDelete']);
    // Project Location * @author      sourov <rokibuzzaman@atilimited.net>
    Route::get('project/project-location', ['as' => 'project_location', 'uses' => 'Admin_access\ProjectController@projectLocation']);
    Route::get('project/project-location-create-form', ['as' => 'project_location_create_form', 'uses' => 'Admin_access\ProjectController@projectLocationCreateForm']);
    Route::post('project/project-location-save', ['as' => 'project_location_save', 'uses' => 'Admin_access\ProjectController@projectLocationSave']);
    Route::get('project/project-location-data', ['as' => 'project_location_data', 'uses' => 'Admin_access\ProjectController@projectLocationData']);
    Route::get('project/project-location-edit-form/{LOCATION_ID}', ['as' => 'project_location_edit_form', 'uses' => 'Admin_access\ProjectController@projectlocationEditForm']);
    Route::post('project/project-location-update/{LOCATION_ID}', ['as' => 'project_location_update', 'uses' => 'Admin_access\ProjectController@projectLocationUpdate']);
    Route::get('project/project-location-status/{USER_ID}', ['as' => 'project_location_status', 'uses' => 'Admin_access\ProjectController@projectLocationStatus']);
    Route::get('project/project-location-delete/{LOCATION_ID}', ['as' => 'project_location_delete', 'uses' => 'Admin_access\ProjectController@projectLocationDelete']);

    /* market @author      Nurullah <nurul@atilimited.net>*/
    Route::get('market/all-market', ['as' => 'all_market', 'uses' => 'Admin_access\ProjectController@marketIndex']);
    Route::get('market/market-data', ['as' => 'market_data', 'uses' => 'Admin_access\ProjectController@marketData']);
    Route::get('market/market-create-form', ['as' => 'market_create_form', 'uses' => 'Admin_access\ProjectController@marketCreateForm']);
    Route::post('market/market-create', ['as' => 'create_market', 'uses' => 'Admin_access\ProjectController@createMarket']);
    Route::get('market/market-status/{id}', ['as' => 'market_status', 'uses' => 'Admin_access\ProjectController@marketStatus']);
    Route::get('market/market-list-edit-form/{id}', ['as' => 'market_list_edit_form', 'uses' => 'Admin_access\ProjectController@marketEditForm']);
    Route::post('market/market-update/{id}', ['as' => 'update_market', 'uses' => 'Admin_access\ProjectController@updateMarket']);
    Route::get('market/market-view/{id}', ['as' => 'market_view', 'uses' => 'Admin_access\ProjectController@marketView']);
    Route::get('market/market-print/{id}', ['as' => 'market_print', 'uses' => 'Admin_access\ProjectController@marketDetailPrint']);
    Route::get('market/market-delete/{id}', ['as' => 'market_delete', 'uses' => 'Admin_access\ProjectController@marketDelete']);
    /* marketDetails @author      Nurullah <nurul@atilimited.net>*/
    Route::get('market/all-market-details', ['as' => 'all_market_details', 'uses' => 'Admin_access\ProjectController@marketDetailsIndex']);
    Route::get('market/market-details-data', ['as' => 'market_details_data', 'uses' => 'Admin_access\ProjectController@marketDetailsData']);
    Route::get('market/market-details-create-form', ['as' => 'market_details_create_form', 'uses' => 'Admin_access\ProjectController@marketDetailsCreateForm']);
    Route::post('market/market-details-create', ['as' => 'create_details_market', 'uses' => 'Admin_access\ProjectController@createMarketDetails']);
    Route::get('get-market-numbe', ['as' => 'get_market_number', 'uses' => 'Admin_access\ProjectController@getMarketNumber']);
    Route::get('market/market-details-status/{id}', ['as' => 'market-details-status', 'uses' => 'Admin_access\ProjectController@marketDetailStatus']);
    Route::get('market/market-details-edit-form/{id}', ['as' => 'market_details_edit_form', 'uses' => 'Admin_access\ProjectController@marketDetailsEditForm']);
    Route::post('market/market-details-edit/{id}', ['as' => 'market_details_edit', 'uses' => 'Admin_access\ProjectController@marketDetailsUpdate']);

    //start newle
    // Projects  @author  Abdul Awal <abdulawal@atilimited.net>
    Route::get('project/all-projects', ['as' => 'all_projects', 'uses' => 'Admin_access\ProjectController@projectIndex']);
    Route::get('project/project-data', ['as' => 'project_data', 'uses' => 'Admin_access\ProjectController@projectData']);
    Route::get('project/project-create-form', ['as' => 'project_create_form', 'uses' => 'Admin_access\ProjectController@projectCreateForm']);
    Route::post('project/project-create', ['as' => 'create_project', 'uses' => 'Admin_access\ProjectController@createProject']);
    Route::get('project/project-status/{id}', ['as' => 'project_status', 'uses' => 'Admin_access\ProjectController@projectStatus']);
    Route::get('project/project-list-edit-form/{id}', ['as' => 'project_list_edit_form', 'uses' => 'Admin_access\ProjectController@projectEditForm']);

    Route::get('lease/lease-list-edit-form/{id}', ['as' => 'lease_list_edit_form', 'uses' => 'Admin_access\ProjectController@leaseEditForm']);
    Route::post('lease/lease-update/{id}', ['as' => 'update_lease', 'uses' => 'Admin_access\ProjectController@updateLease']);
    Route::post('project/project-update/{id}', ['as' => 'update_project', 'uses' => 'Admin_access\ProjectController@updateProject']);
    Route::get('project/project-view/{id}', ['as' => 'project_view', 'uses' => 'Admin_access\ProjectController@projectView']);
    Route::get('lease/lease-view/{id}', ['as' => 'lease_view', 'uses' => 'Admin_access\ProjectController@leaseView']);
    Route::get('project/project-delete/{id}', ['as' => 'project_delete', 'uses' => 'Admin_access\ProjectController@projectDelete']);
    Route::get('project/details-delete/{id}', ['as' => 'project_details_delete', 'uses' => 'Admin_access\ProjectController@projectDetailsDelete']);
    Route::get('project/general-projects', ['as' => 'general_project_list', 'uses' => 'Admin_access\ProjectController@generalProjects']);
    Route::get('project/project-general-form', ['as' => 'project_general_form', 'uses' => 'Admin_access\ProjectController@projectGeneralForm']);
    Route::get('project/type-list', ['as' => 'project_type_list', 'uses' => 'Admin_access\ProjectController@projectTypeList']);
    Route::get('project/type-data-create-form', ['as' => 'project_type_data_create_form', 'uses' => 'Admin_access\ProjectController@projectTypeDataCreateForm']);
    Entrust::routeNeedsRole('admin/project/*', ['admin', 'project-manager'], Redirect::to('admin/dashboard'), false);
    // End Project management

    // Tender management  @author  Abdul Awal <abdulawal@atilimited.net>
    Route::get('tender/tender-list', ['as' => 'tender_list', 'uses' => 'Admin_access\TenderController@tenderIndex']);
    Route::get('tender/tender-create-form', ['as' => 'tender_create_form', 'uses' => 'Admin_access\TenderController@tenderCreateForm']);
    Route::get('tender/tender-data', ['as' => 'tender_data', 'uses' => 'Admin_access\TenderController@tenderData']);
    Route::get('tender/project-category', ['as' => 'tender_project_category', 'uses' => 'Admin_access\TenderController@tenderProjectCategory']);
    Route::get('tender/project-details', ['as' => 'tender_project_details', 'uses' => 'Admin_access\TenderController@tenderProjectDetails']);
    Route::post('tender/tender-create', ['as' => 'tender_create', 'uses' => 'Admin_access\TenderController@tenderCreate']);
    Route::get('tender/tender-status/{id}', ['as' => 'tender_status', 'uses' => 'Admin_access\TenderController@tenderStatus']);
    Route::get('tender/tender-edit/{id}', ['as' => 'tender_edit', 'uses' => 'Admin_access\TenderController@tenderEdit']);
    Route::get('tender/schedule-delete/{id}', ['as' => 'tender_schedule_delete', 'uses' => 'Admin_access\TenderController@tenderScheduleDelete']);
    Route::post('tender/tender-update/{id}', ['as' => 'tender_update', 'uses' => 'Admin_access\TenderController@tenderUpdate']);
    Route::get('tender/tender-print/{id}', ['as' => 'tender_print', 'uses' => 'Admin_access\TenderController@tenderPrint']);
    Route::get('tender/tender-delete/{id}', ['as' => 'tender_delete', 'uses' => 'Admin_access\TenderController@tenderDelete']);

    Route::get('tender/applicants-comparison', ['as' => 'tender_applicant_comparison', 'uses' => 'Admin_access\TenderController@tenderApplicantComparison']);
    Route::get('tender/comparison-data', ['as' => 'tender_comparison_data', 'uses' => 'Admin_access\TenderController@tenderComparisonData']);
    Route::get('tender/schelude-list', ['as' => 'tender_schedule_list', 'uses' => 'Admin_access\TenderController@getScheduleListByTender']);
    Route::get('tender/remarks-form/{type}/{id}', ['as' => 'tender_remarks_form', 'uses' => 'Admin_access\TenderController@tenderRemarksForm']);
    Route::post('tender/update-remarks/{type}/{id}', ['as' => 'tender_remarks_update', 'uses' => 'Admin_access\TenderController@tenderRemarksUpdate']);
    Route::get('tender/cancel-form/{type}/{id}', ['as' => 'tender_cancel_form', 'uses' => 'Admin_access\TenderController@tenderCancelForm']);
    Route::post('tender/update-cancel/{type}/{id}', ['as' => 'tender_cancel_update', 'uses' => 'Admin_access\TenderController@tenderCancelUpdate']);
    Route::get('tender/primary-select-form/{ts}/{type}/{id}', ['as' => 'tender_primary_select_form', 'uses' => 'Admin_access\TenderController@tenderPrimaryApplicantSelectForm']);
    Route::post('tender/primary-select-insert/{ts}/{type}/{id}', ['as' => 'tender_primary_select_insert', 'uses' => 'Admin_access\TenderController@tenderPrimaryApplicantSave']);
    Route::get('tender/applicant-list', ['as' => 'tender_applicant_list', 'uses' => 'Admin_access\TenderController@tenderApplicantList']);
    Route::get('tender/applicant-data', ['as' => 'tender_applicant_data', 'uses' => 'Admin_access\TenderController@tenderListData']);
    Route::get('tender/primary-applicant-list', ['as' => 'tender_primary_selected_applicant_list', 'uses' => 'Admin_access\TenderController@tenderPrimaryApplicantList']);
    Route::get('tender/primary-applicant-data', ['as' => 'tender_primary_applicant_data', 'uses' => 'Admin_access\TenderController@tenderPrimaryListData']);
    //Route::get('tender/primary-applicant/{id}', array("as" => "tender_primary_applicant", 'uses' => 'Admin_access\TenderController@primaryApplicant'));
    //Route::post('tender/primary-applicant-update/{id}', array("as" => "tender_primary_applicant_update", 'uses' => 'Admin_access\TenderController@primaryApplicantUpdate'));

    //Route::get('tender/primary-letter-form/{id}', array("as" => "tender_primary_letter_form", 'uses' => 'Admin_access\TenderController@tenderPrimaryLetterForm'));
    //Route::post('tender/update-primary-letter/{id}', array("as" => "tender_primary_letter_update", 'uses' => 'Admin_access\TenderController@tenderPrimaryLetterUpdate'));
    Route::get('tender/update-primary-print/{ts}/{type}/{id}', ['as' => 'tender_primary_letter_print', 'uses' => 'Admin_access\TenderController@tenderPrimaryLetterPrint']);

    /* Lease Final Selection  @author      Nurullah <nurul@atilimited.net>*/
    Route::get('tender/final-applicant-list', ['as' => 'tender_final_selected_applicant_list', 'uses' => 'Admin_access\TenderController@tenderFinalApplicantList']);
    Route::get('tender/final-applicant-data', ['as' => 'tender_final_applicant_data', 'uses' => 'Admin_access\TenderController@tenderfinalListData']);
    Route::get('tender/final-select-form/{ts}/{type}/{id}', ['as' => 'tender_final_select_form', 'uses' => 'Admin_access\TenderController@tenderFinalApplicantSelectForm']);
    Route::post('tender/final-select-insert/{ts}/{type}/{id}', ['as' => 'tender_final_select_insert', 'uses' => 'Admin_access\TenderController@tenderFinalApplicantSave']);
    Route::get('tender/final-print/{ts}/{type}/{id}', ['as' => 'tender_final_letter_print', 'uses' => 'Admin_access\TenderController@tenderFinalLetterPrint']);

    Entrust::routeNeedsRole('admin/tender/*', ['admin', 'tender-manager'], Redirect::to('admin/dashboard'), false);
    // End Tender Management

    /* Account Bank Information  @author      Jahid Hasan <jahid@atilimited.net>*/
    Route::get('bank', ['as' => 'bank_list', 'uses' => "Admin_access\AccountController@bankList"]);
    Route::get('bank_data', ['as' => 'bank_data', 'uses' => "Admin_access\AccountController@bankData"]);
    Route::get('bank/bank-create-form', ['as' => 'bank_create_form', 'uses' => 'Admin_access\AccountController@bankCreateForm']);
    Route::post('bank/create-bank', ['as' => 'create_bank', 'uses' => 'Admin_access\AccountController@createBank']);
    Route::get('bank/bank-edit-form/{id}', ['as' => 'bank_edit_form', 'uses' => 'Admin_access\AccountController@bankEditForm']);
    Route::get('bank/bank-status/{id}', ['as' => 'bank_status', 'uses' => 'Admin_access\AccountController@bankStatus']);
    Route::post('bank/bank-update/{id}', ['as' => 'bank_update', 'uses' => 'Admin_access\AccountController@bankUpdate']);
    Route::get('bank/bank-delete/{id}', ['as' => 'bank_delete', 'uses' => 'Admin_access\AccountController@bankDelete']);
    Entrust::routeNeedsRole('admin/bank/*', ['admin', 'account-manager'], Redirect::to('admin/dashboard'), false);

    // Citizen management @author  Abdul Awal <abdulawal@atilimited.net>
    Route::get('citizen/rent-payment', ['as' => 'citizen_rent_payment', 'uses' => "Admin_access\CitizenController@rentPayment"]);
    Route::get('citizen/rent-pending-month', ['as' => 'rent_pending_month', 'uses' => "Admin_access\CitizenController@rentPendingMonth"]);
    Route::get('citizen/rent-pay-form', ['as' => 'rent_pay_form', 'uses' => "Admin_access\CitizenController@rentPayForm"]);
    Route::post('citizen/rent-payment/{vc}/{sc}/{ctz}', ['as' => 'rent_payment', 'uses' => "Admin_access\CitizenController@rentPaymentSubmit"]);
    //Route::get('citizen/invoice/{resid}/{voucher}', array("as" => "rent_payment", "uses" => "Admin_access\CitizenController@rentPaymentSubmit"));
    Route::get('citizen/rent-history', ['as' => 'citizen_rent_history', 'uses' => "Admin_access\CitizenController@rentHistory"]);
    Route::get('citizen/rent-history-data', ['as' => 'rent_history_data', 'uses' => "Admin_access\CitizenController@rentHistoryData"]);
    Route::get('citizen/my-services', ['as' => 'my_services', 'uses' => "Admin_access\CitizenController@myServices"]);
    Route::get('citizen/service-data', ['as' => 'service_data', 'uses' => "Admin_access\CitizenController@serviceData"]);

    //************************************
    Route::get('citizen/payment-history', ['as' => 'citizen_pay_history', 'uses' => 'Admin_access\CitizenController@paymentHistory']);
    Route::get('citizen/payment-data', ['as' => 'citizen_pay_data', 'uses' => 'Admin_access\CitizenController@paymentData']);
    Route::get('citizen/application-form-history', ['as' => 'citizen_form_history', 'uses' => 'Admin_access\CitizenController@applicationFormHistory']);
    Route::get('citizen/form-data', ['as' => 'citizen_form_data', 'uses' => 'Admin_access\CitizenController@getFormData']);
    Route::get('citizen/application-notification', ['as' => 'citizen_app_notification', 'uses' => 'Admin_access\CitizenController@applicationNotification']);
    Route::get('citizen/notification-data', ['as' => 'citizen_notification_data', 'uses' => 'Admin_access\CitizenController@appNotificationData']);
    Route::get('citizen/installmen-history', ['as' => 'citizen_installment_history', 'uses' => 'Admin_access\CitizenController@installmentHistory']);
    Route::get('citizen/installmen-data', ['as' => 'citizen_installment_data', 'uses' => 'Admin_access\CitizenController@installmentData']);
    Route::get('citizen/lease-property', ['as' => 'citizen_lease_property', 'uses' => 'Admin_access\CitizenController@leaseProperty']);
    Route::get('citizen/rent-chart-data', ['as' => 'rent_chart_data', 'uses' => 'Admin_access\DashboardController@rentChartData']);
    //************************************

    //----------------------------------------------------------------------

    // Security Access management @author  Nurullah <nurul@atilimited.net>
    Route::get('module/list', ['as' => 'module_list', 'uses' => 'Admin_access\AccessController@moduleSetup']);
    Route::get('module/data', ['as' => 'module_data', 'uses' => 'Admin_access\AccessController@moduleData']);
    Route::get('module/create', ['as' => 'module_create', 'uses' => 'Admin_access\AccessController@cretaeModule']);
    Route::post('module/save', ['as' => 'module_save', 'uses' => 'Admin_access\AccessController@moduleSave']);
    Route::get('module/edit-form/{id}', ['as' => 'module_edit_form', 'uses' => 'Admin_access\AccessController@moduleEditForm']);
    Route::post('module/edit-save/{id}', ['as' => 'module_edit_save', 'uses' => 'Admin_access\AccessController@moduleUpdate']);
    Route::get('module/module-status/{id}', ['as' => 'module_status', 'uses' => 'Admin_access\AccessController@moduleStatus']);
    Route::get('module/module-delete/{id}', ['as' => 'module_delete', 'uses' => 'Admin_access\AccessController@moduleDelete']);
    // Module_link

    Route::get('module/link-list', ['as' => 'module_link_list', 'uses' => 'Admin_access\AccessController@moduleLinkSetup']);
    Route::get('module/link-data', ['as' => 'module_data_link', 'uses' => 'Admin_access\AccessController@linkData']);
    Route::get('module/create-link', ['as' => 'module_create_link', 'uses' => 'Admin_access\AccessController@cretaeLink']);
    Route::post('module/link-save', ['as' => 'module_link_save', 'uses' => 'Admin_access\AccessController@linkSave']);
    Route::get('module/link-edit-form/{id}', ['as' => 'module_link_edit_form', 'uses' => 'Admin_access\AccessController@linkEditForm']);
    Route::post('module/link-edit-save/{id}', ['as' => 'link_edit_save', 'uses' => 'Admin_access\AccessController@linkUpdate']);
    Route::get('module/link-status/{id}', ['as' => 'link_status', 'uses' => 'Admin_access\AccessController@linkStatus']);
    Route::get('module/link-delete/{id}', ['as' => 'link_delete', 'uses' => 'Admin_access\AccessController@linkDelete']);

    // ORG

    Route::get('module/org', ['as' => 'module_org', 'uses'=> 'Admin_access\AccessController@orgSetup']);
    Route::get('org/assign-module', ['as' => 'assign_module', 'uses' => 'Admin_access\AccessController@assignModule']);
    Route::get('org/add-module', ['as' => 'add_module', 'uses' => 'Admin_access\AccessController@addModule']);
    Route::get('org/remove-module', ['as' => 'remove_module', 'uses' => 'Admin_access\AccessController@removeModule']);

    Route::get('org/add-pages', ['as' => 'add_pages', 'uses' => 'Admin_access\AccessController@assignPages']);
    Route::get('org/add-remove-pages', ['as' => 'add_remove_pages', 'uses' => 'Admin_access\AccessController@addRemovePages']);

    Route::get('module/group', ['as' => 'all_group', 'uses' => 'Admin_access\AccessController@allGroup']);
    Route::get('group/create', ['as' => 'create_group', 'uses' => 'Admin_access\AccessController@cretaeGroup']);
    Route::post('group/save', ['as' => 'group_save', 'uses' => 'Admin_access\AccessController@groupSave']);
    Route::get('group/level-create/{id}', ['as' => 'create_group_level', 'uses' => 'Admin_access\AccessController@cretaeGroupLevel']);
    Route::post('group/save-level/{id}', ['as' => 'group_level_save', 'uses' => 'Admin_access\AccessController@groupLevelSave']);

    Route::get('module/assign', ['as' => 'assign_to_group', 'uses' => 'Admin_access\AccessController@assignModuleToGroup']);
    Route::get('module/group-level', ['as' => 'get_group_to_level', 'uses' => 'Admin_access\AccessController@getLevelByGroup']);
    Route::get('module/module-by-group', ['as' => 'get_module_by_group', 'uses' => 'Admin_access\AccessController@getModuleAcceesByGroup']);
    Route::get('module/module-by-level', ['as' => 'get_module_by_g_level', 'uses' => 'Admin_access\AccessController@getModuleAcceesByGroupLevel']);
    Route::get('module/assign-module-group', ['as' => 'assign_module_to_group', 'uses' => 'Admin_access\AccessController@assignModuleToGroupAction']);

    Route::get('module/level-user', ['as' => 'get_user_by_level', 'uses' => 'Admin_access\AccessController@getUserByLevel']);
    Route::get('module/assign-module-user', ['as' => 'assign_module_by_group', 'uses' => 'Admin_access\AccessController@getModuleAcceesByUser']);
    ///----------------------------------------------------------------------

    Entrust::routeNeedsRole('admin/citizen/*', 'citizen', Redirect::to('admin/dashboard'));
});

// For registered citizen
Route::group(['prefix' => 'applicant-user'], function () {
    Route::get('dashboard', ['as' => 'applicant_dashboard', 'uses' => 'Applicant\ApplicantController@getIndex']);
    Route::get('payment-history', ['as' => 'applicant_pay_history', 'uses' => 'Applicant\ApplicantController@paymentHistory']);
    Route::get('payment-data', ['as' => 'applicant_pay_data', 'uses' => 'Applicant\ApplicantController@paymentData']);
    Route::get('application-form-history', ['as' => 'application_form_history', 'uses' => 'Applicant\ApplicantController@applicationFormHistory']);
    Route::get('get-form-data', ['as' => 'get_form_data', 'uses' => 'Applicant\ApplicantController@getFormData']);
    Route::get('application-notification', ['as' => 'te_app_notification', 'uses' => 'Applicant\ApplicantController@applicationNotification']);
    Route::get('app-notification-data', ['as' => 'app_notification_data', 'uses' => 'Applicant\ApplicantController@appNotificationData']);
    Route::get('installmen-history', ['as' => 'applicant_installment_history', 'uses' => 'Applicant\ApplicantController@installmentHistory']);
    Route::get('installmen-data', ['as' => 'applicant_installment_data', 'uses' => 'Applicant\ApplicantController@installmentData']);
});
