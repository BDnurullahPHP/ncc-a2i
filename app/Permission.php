<?php

namespace App;

use Zizaco\Entrust\EntrustPermission;

class Permission extends EntrustPermission
{
    protected $primaryKey = 'PERM_ID';

    /**
     * Permission data collection.
     *
     * @param $input array
     *
     * @return Collection
     */
    public static function getData($input)
    {
        $data = self::orderBy('PERM_ID', 'desc')
                            ->take($input['length'])
                            ->skip($input['start'])
                            ->get();

        $total = self::count();

        return ['data' => $data, 'total' => $total, 'filtered' => $total];
    }
}
